# DeltaCMF Platform

DeltaCMF Platform is a module CMF (or CMS) powered by Laravel framework.



## Tech

List of core open source projects used in this application:

* [Vue](https://vuejs.org/v2/guide/) - JS framework.
* [Vuex](https://vuex.vuejs.org/) - State management pattern + library for Vue.js.
* [axios](https://github.com/axios/axios) - Promise based HTTP client.
* [Tailwind CSS](https://tailwindcss.com/docs/what-is-tailwind/) - Tailwind is a utility-first CSS framework for rapidly building custom user interfaces.
* [Element](http://element.eleme.io/#/en-US) - a Vue based component frontend library
* [lodash](https://lodash.com) - Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc.
* [Moment](https://momentjs.com) - Parse, validate, manipulate, and display dates and times.
* etc.

All of packages used in this application lists in [PACKAGE LIST](PACKAGES.md).



## Code Style

This project uses [certain standards for writing code](CODESTYLE.md) that must be strictly observed!
[Watch this](CODESTYLE.md)



## Installation

Oh... it's simple - [check this out](INSTALLATION.md)



### ToDo

Some code fixes that need to be fixed (tested) in future - [todo](TODO.md)
