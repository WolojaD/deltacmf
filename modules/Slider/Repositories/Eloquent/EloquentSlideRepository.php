<?php

namespace Modules\Slider\Repositories\Eloquent;

use Modules\Slider\Entities\Slider;
use Modules\Slider\Repositories\SlideRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentSlideRepository extends EloquentCoreRepository implements SlideRepository
{
    public $model_config = 'modules/Slider/Config/models/slide.json';

    private $sliders;

    /**
     * @param \Modules\Slider\Entities\Slide $model
     */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->slider = new EloquentSliderRepository(new Slider());
    }

    public function sliders()
    {
        return $this->slider->all()->pluck('title', 'id')->toArray();
    }
}
