<?php

namespace Modules\Slider\Repositories\Eloquent;

use Modules\Slider\Repositories\SliderRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentSliderRepository extends EloquentCoreRepository implements SliderRepository
{
    public $model_config = 'modules/Slider/Config/models/slider.json';

    protected function getFieldTypeValues()
    {
        return config('application.slider.fields-type.field_type');
    }
}
