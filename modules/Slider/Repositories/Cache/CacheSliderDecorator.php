<?php

namespace Modules\Slider\Repositories\Cache;

use Modules\Slider\Repositories\SliderRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheSliderDecorator extends CoreCacheDecorator implements SliderRepository
{
    /**
    * @var SliderRepository
    */
    protected $repository;

    public function __construct(SliderRepository $slider)
    {
        parent::__construct();

        $this->entityName = 'sliders';
        $this->repository = $slider;
    }
}
