<?php

namespace Modules\Slider\Repositories\Cache;

use Modules\Slider\Repositories\SlideRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheSlideDecorator extends CoreCacheDecorator implements SlideRepository
{
    /**
    * @var SlideRepository
    */
    protected $repository;

    public function __construct(SlideRepository $slider)
    {
        parent::__construct();

        $this->entityName = 'slider_slides';
        $this->repository = $slider;
    }
}
