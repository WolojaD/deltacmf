<?php

namespace Modules\Slider\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Slide extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'slider_slides';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/slider';

    protected $file_path = 'modules/Slider/Config/models/slide.json';

    public $groupOrder = 'slider_id';

    public $breadcrumbs = true;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    // TODO can have problems
    public function getBackendBreadcrumbs($carry = [])
    {
        $modelConfig = $this->getJsonList();
        $slider = Slider::find(request()->ident);

        return
        [
            [
                'name' => trans('core::breadcrumbs.slider'),
                'link' => [
                    'name' => 'api.backend.slider_slider',
                ]
            ],
            [
                'name' => $slider->title ?? '',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function slider()
    {
        return $this->belongsTo(Slider::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
