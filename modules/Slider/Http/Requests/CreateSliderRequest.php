<?php

namespace Modules\Slider\Http\Requests;

use Modules\Slider\Entities\Slider;
use Modules\Core\Internationalisation\BaseFormRequest;

class CreateSliderRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Slider/Config/models/slider.json');
    }

    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->has('title')) {
            $request = $this;
            $max = Slider::where('short_code', 'like', str_slug($this->title) . '%')
                ->pluck('short_code')
                ->map(function ($short_code) use ($request) {
                    return (int) trim(str_replace(str_slug($request->title), '', $short_code), '-');
                })->max();

            if (!is_null($max)) {
                $this->merge(['short_code' => str_slug($this->title) . '-' . ($max + 1)]);
            } else {
                $this->merge(['short_code' => str_slug($this->title)]);
            }

            if (is_numeric($max)) {
                $this->merge(['slug' => $this->slug . '-' . ($max + 1)]);
            }
        }
    }
}
