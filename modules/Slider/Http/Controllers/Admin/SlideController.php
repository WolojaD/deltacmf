<?php

namespace Modules\Slider\Http\Controllers\Admin;

use Modules\Slider\Repositories\SlideRepository;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class SlideController extends CoreAdminController
{
    /**
     * @var SlideRepository
     */
    private $slide;

    public function __construct(SlideRepository $slide)
    {
        $this->slide = $slide;
    }

    /**
     * Display a listing of the resource.
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     * @return Illuminate\View\View
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Illuminate\View\View
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function destroy($id)
    {
        $this->slide->delete($id);

        return redirect()->route('backend.slide.index')
            ->withSuccess(trans('Slider deleted'));
    }
}
