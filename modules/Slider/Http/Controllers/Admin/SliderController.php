<?php

namespace Modules\Slider\Http\Controllers\Admin;

use Modules\Slider\Repositories\SliderRepository;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class SliderController extends CoreAdminController
{
    /**
     * @var SliderRepository
     */
    private $slider;

    public function __construct(SliderRepository $slider)
    {
        $this->slider = $slider;
    }

    /**
     * Display a listing of the resource.
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     * @return Illuminate\View\View
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Illuminate\View\View
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function destroy($id)
    {
        $this->slider->delete($id);

        return redirect()->route('backend.slider.index')
            ->withSuccess(trans('Slider deleted'));
    }
}
