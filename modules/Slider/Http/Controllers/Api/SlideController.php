<?php

namespace Modules\Slider\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Slider\Repositories\SlideRepository;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Slider\Transformers\SlideTransformer;
use Modules\Slider\Http\Requests\CreateSlideRequest;
use Modules\Slider\Http\Requests\UpdateSlideRequest;
use Modules\Slider\Transformers\FullSlideTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class SlideController extends CoreApiController
{
    /**
     * @var SlideRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    /**
     * @var Status
     */
    protected $status;

    protected $slider;

    public function __construct(SlideRepository $slide, SliderRepository $slider)
    {
        $this->repository = $slide;
        $this->modelInfo = $this->repository->getJsonList();
        $this->slider = $slider;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['slider_id' => $id]]);

            return SlideTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return SlideTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $slide = $this->repository->find($id);

        return new FullSlideTransformer($slide);
    }

    public function update($id, UpdateSlideRequest $request)
    {
        $slide = $this->repository->find($id);
        $this->repository->update($slide, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('slider::models.of.slide')]),
            'goto' => ['name' => 'api.backend.slider_slider.show', 'params' => ['ident' => $slide->slider_id]],
            'id' => $slide->id
        ]);
    }

    public function store(CreateSlideRequest $request)
    {
        $slide = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('slider::models.of.slide')]),
            'goto' => ['name' => 'api.backend.slider_slider.show', 'params' => ['ident' => $slide->slider_id]],
            'id' => $slide->id
        ]);
    }

    public function destroy($id)
    {
        $slide = $this->repository->find($id);

        $this->repository->destroy($slide);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('slider::models.of.slide')]),
        ]);
    }
}
