<?php

namespace Modules\Slider\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Slider\Repositories\SlideRepository;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Slider\Transformers\SlideTransformer;
use Modules\Slider\Transformers\SliderTransformer;
use Modules\Slider\Http\Requests\CreateSliderRequest;
use Modules\Slider\Http\Requests\UpdateSliderRequest;
use Modules\Slider\Transformers\FullSliderTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class SliderController extends CoreApiController
{
    /**
     * @var SliderRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    /**
     * @var Status
     */
    protected $status;

    protected $slide;

    public function __construct(SliderRepository $slider, SlideRepository $slide)
    {
        $this->repository = $slider;
        $this->modelInfo = $this->repository->getJsonList();
        $this->slide = $slide;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['slider_id' => $id]]);

            return SlideTransformer::collection($this->slide->serverPaginationFilteringFor($request));
        }

        return SliderTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $slider = $this->repository->find($id);

        return [
            'data' => new FullSliderTransformer($slider),
            'title_addition' => $slider->title
        ];
    }

    public function update($id, UpdateSliderRequest $request)
    {
        $slider = $this->repository->find($id);
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $this->repository->update($slider, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('slider::models.of.slider')]),
            'id' => $slider->id
        ]);
    }

    public function store(CreateSliderRequest $request)
    {
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $slider = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('slider::models.of.slider')]),
            'id' => $slider->id
        ]);
    }

    public function destroy($id)
    {
        $slider = $this->repository->find($id);

        $this->repository->destroy($slider);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('slider::models.of.slider')])
        ]);
    }
}
