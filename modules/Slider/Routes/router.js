import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/slider',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.slider_slider',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Sliders',
                    breadcrumb: [
                        { name: 'Sliders' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.slider_slider.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Slider Create',
                    breadcrumb: [
                        { name: 'Sliders', link: 'api.backend.slider_slider' },
                        { name: 'Slider Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.slider_slider.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Slider Edit',
                    breadcrumb: [
                        { name: 'Sliders', link: 'api.backend.slider_slider' },
                        { name: 'Slider Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.slider_slider.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Slider',
                    breadcrumb: [
                        { name: 'Sliders', link: 'api.backend.slider_slider' },
                        { name: 'Slider' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/slider/slides',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.slider_slide',
                redirect: { name: 'api.backend.slider_slider' }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.slider_slide.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Slide Create',
                    breadcrumb: [
                        { name: 'Sliders', link: 'api.backend.slider_slider' },
                        { name: 'Slide Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.slider_slide.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Slide Edit',
                    breadcrumb: [
                        { name: 'Sliders', link: 'api.backend.slider_slider' },
                        { name: 'Slide Edit' }
                    ]
                }
            }
        ]
    }
]
