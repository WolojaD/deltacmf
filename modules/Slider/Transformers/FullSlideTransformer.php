<?php

namespace Modules\Slider\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullSlideTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'mobile_image' => $this->mobile_image,
            'desktop_image' => $this->desktop_image,
            'slider_id' => $this->slider_id,
            'target_blank' => $this->target_blank,
            'url' => $this->url,
            'status' => $this->status
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
