<?php

return [
    'field_type' => [
        'random' => trans('slider::field-types.random'),
        'order' => trans('slider::field-types.order')
    ],
];
