<?php

namespace Modules\Slider\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Slider\Entities\Slide;
use Modules\Slider\Entities\Slider;

class SliderDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Slider::class, 3)->create();
            factory(Slide::class, 15)->create();
        }
    }
}
