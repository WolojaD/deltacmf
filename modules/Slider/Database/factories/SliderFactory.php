<?php

use Faker\Generator as Faker;
use Modules\Slider\Entities\Slide;
use Modules\Slider\Entities\Slider;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Slider::class, function (Faker $faker) {
    $data = [
        'title' => $faker->name,
        'short_code' => $faker->lexify('?????????'),
        'status' => $faker->numberBetween(0, 1),
    ];

    return $data;
});

$factory->define(Slide::class, function (Faker $faker) {
    $data = [
        'slider_id' => $faker->numberBetween(1, 3),
        'status' => $faker->numberBetween(0, 1),
        'url' => $faker->url,
        'target_blank' => $faker->numberBetween(0, 1),
        'desktop_image' => $faker->name,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'sub_title' => $faker->name,
            'description' => $faker->sentence,
            'url_name' => $faker->name,
        ];
    }

    return $data;
});
