<?php

namespace Modules\Slider\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Webwizo\Shortcodes\Facades\Shortcode;
use Modules\Core\Providers\ServiceProvider;
use Modules\Slider\Shortcodes\SliderShortcode;
use Modules\Core\Events\LoadingBackendTranslations;

class SliderServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        Shortcode::register('slider', SliderShortcode::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('slider_sidebar', array_dot(trans('slider::sidebar')));
            $event->load('slider_table-fields', array_dot(trans('slider::table-fields')));
            $event->load('slider_permissions', array_dot(trans('slider::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('slider', 'config');
        $this->publishConfig('slider', 'permissions');
        $this->publishConfig('slider', 'fields-type');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $folder = base_path('modules/Slider/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }
        foreach ($models as $model_name => $model) {
            $model_name = studly_case($model_name);
            $this->model = $model;

            $this->app->bind("Modules\Slider\Repositories\\{$model_name}Repository", function () use ($model_name, $model) {
                $eloquent_repository = "\Modules\Slider\Repositories\Eloquent\Eloquent{$model_name}Repository";
                $entity = "\Modules\Slider\Entities\\{$model_name}";
                $cache_decorator = "\Modules\Slider\Repositories\Cache\Cache{$model_name}Decorator";

                $repository = new $eloquent_repository(new $entity());

                if (!Config::get('app.cache') && !($model->no_cache ?? false)) {
                    return $repository;
                }

                return new $cache_decorator($repository);
            });
        }
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
