<?php

namespace Modules\Slider\Shortcodes;

use Modules\Slider\Entities\Slider;
use Illuminate\Support\Facades\View;

class SliderShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        $slider = Slider::when(!is_numeric($shortcode->code), function ($q) use ($shortcode) {
            $q->where('short_code', $shortcode->code);
        }, function ($q) use ($shortcode) {
            $q->where('id', $shortcode->code);
        })->with('slides')->first();

        if (null === $slider) {
            return '';
        }

        if ($slider) {
            if ($slider->sort_slides == 'random') {
                $slider->slides = $slider->slides->shuffle();
            }

            return $slider->slides && $slider->slides->count() ? view('slider::' . 'index', compact('slider')) : '';
        } else {
            return '';
        }
    }
}
