<?php

namespace Modules\Infoblocks\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Infoblocks\Repositories\InfoblockInterface;

class CacheInfoblockDecorator extends CoreCacheDecorator implements InfoblockInterface
{
    /**
     * @var InfoblockInterface
     */
    protected $repository;

    public function __construct(InfoblockInterface $infoblocks)
    {
        parent::__construct();

        $this->entityName = 'infoblocks';
        $this->repository = $infoblocks;
    }
}
