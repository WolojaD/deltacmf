<?php

namespace Modules\Infoblocks\Repositories\Cache;

use Modules\Infoblocks\Repositories\TemplateInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheTemplateDecorator extends CoreCacheDecorator implements TemplateInterface
{
    /**
     * @var TemplateInterface
     */
    protected $repository;

    public function __construct(TemplateInterface $infoblocks)
    {
        parent::__construct();

        $this->entityName = 'infoblocks_templates';
        $this->repository = $infoblocks;
    }

    public function getFields()
    {
        return $this->remember(function () {
            return $this->repository->getFields();
        });
    }
}
