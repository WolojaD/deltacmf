<?php

namespace Modules\Infoblocks\Repositories\Cache;

use Modules\Infoblocks\Repositories\GroupInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheGroupDecorator extends CoreCacheDecorator implements GroupInterface
{
    /**
     * @var GroupInterface
     */
    protected $repository;

    public function __construct(GroupInterface $infoblocks)
    {
        parent::__construct();

        $this->entityName = 'infoblocks_groups';
        $this->repository = $infoblocks;
    }
}
