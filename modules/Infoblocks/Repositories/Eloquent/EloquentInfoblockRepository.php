<?php

namespace Modules\Infoblocks\Repositories\Eloquent;

use Modules\Infoblocks\Entities\Group;
use Modules\Infoblocks\Repositories\InfoblockInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentInfoblockRepository extends EloquentCoreRepository implements InfoblockInterface
{
    public $model_config = 'modules/Infoblocks/Config/models/infoblock.json';

    /**
     * Generate fields components with parameters for create/update page.
     *
     * @return type
     */
    public function getFieldsFromTemplate($request, $type = 'create')
    {
        if ($type == 'create') {
            if ($template = Group::with('template')->find($request->get('ident'))->template) {
                $group_template = $template->fields;
            } else {
                $group_template = null;
            }
        } elseif ($type == 'edit') {
            $group_template = $this->find($request->get('ident'))->group->template->fields;
        }

        if (null === $group_template) {
            return '';
        }

        $fields = [];

        foreach ($group_template as $key => $field) {
            if (array_key_exists('translatable', config('application.infoblocks.fields')[$field]) && config('application.infoblocks.fields')[$field]['translatable']) {
                if ($key == 0) {
                    $required = config('application.infoblocks.fields')[$field];
                    $required['parameters']['required'] = true;

                    $fields['{"en":"General","ru":"Общие"}']['translatable'][$field] = $required;
                } else {
                    $fields['{"en":"General","ru":"Общие"}']['translatable'][$field] = config('application.infoblocks.fields')[$field];
                }
            } else {
                $fields['{"en":"General","ru":"Общие"}'][$field] = config('application.infoblocks.fields')[$field];
            }
        }

        return $fields;
    }
}
