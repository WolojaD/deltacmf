<?php

namespace Modules\Infoblocks\Repositories\Eloquent;

use Modules\Infoblocks\Repositories\TemplateInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentTemplateRepository extends EloquentCoreRepository implements TemplateInterface
{
    public $model_config = 'modules/Infoblocks/Config/models/template.json';

    public function create($data)
    {
        if (!method_exists($this->model, 'getJsonList') || !request()->get('ident', false)) {
            $this->generateTemplate($data['slug'], $data['fields']);

            return $this->model->create($data);
        }

        $config = $this->model->getJsonList();

        foreach ($config->templates->default ?? [] as $field_name => $field) {
            if (!request()->filled($field_name) && ($field->from_ident ?? false)) {
                $data[$field_name] = (int) request()->ident;
            }
        }

        $this->generateTemplate($data['slug'], $data['fields']);

        return $this->model->create($data);
    }

    public function getFields()
    {
        $fields = [];

        foreach (config('application.infoblocks.fields') as $field => $params) {
            $fields[$field] = trans('infoblocks::fields.' . $field);
        }

        return $fields;
    }

    private function generateTemplate($slug, $fields)
    {
        $file = fopen(get_application_frontend_theme_path() . '/views/infoblocks/' . $slug . '.blade.php', 'w');

        fwrite($file, $this->templateParser($fields));

        fclose($file);
    }

    private function templateParser($fields)
    {
        $template = '';

        $template .= '@if($infoblock_group->show_title)' . "\n";
        $template .= '{{ $infoblock_group->title ?? \'\' }}' . "\n";
        $template .= '@endif' . "\n\n";

        $template .= '{!! $infoblock_group->description ?? \'\' !!}' . "\n\n";

        $template .= '@if($infoblock_group->url)' . "\n";
        $template .= '{{ $infoblock_group->url ?? \'\' }}' . "\n";
        $template .= '{{ $infoblock_group->url_title ?? \'\' }}' . "\n";
        $template .= '@endif' . "\n\n";

        $template .= '@if($infoblock_group->desktop_image)' . "\n";
        $template .= '{{ \'/storage/i/infoblock-group-image\' . $infoblock_group->adaptive_image ?? \'\' }}' . "\n";
        $template .= '@endif' . "\n\n";

        $template .= '@foreach($infoblock_group->infoblocks as $infoblock)' . "\n";

        foreach ($fields as $field) {
            if (str_contains($field, 'image')) {
                $template .= '@if($infoblock->' . $field . ')' . "\n";
                $template .= '{{ \'/storage/i/infoblock-image\' . $infoblock->' . $field . ' }}' . "\n";
                $template .= '@endif' . "\n\n";
            } elseif (str_contains($field, 'file')) {
                $template .= '@if($infoblock->' . $field . ')' . "\n";
                $template .= '{{ \'/storage/origin\' . $infoblock->' . $field . ' }}' . "\n";
                $template .= '@endif' . "\n\n";
            } elseif (str_contains($field, 'video')) {
                $template .= '@if($infoblock->' . $field . ' && str_contains($infoblock->' . $field . ', \'/infoblocks/\'))' . "\n";
                $template .= '{{ \'/storage/origin\' . $infoblock->' . $field . ' }}' . "\n";
                $template .= '@elseif($infoblock->' . $field . ')' . "\n";
                $template .= '<iframe src="{{ \'https://www.youtube.com/embed/\' . json_decode($infoblock->' . $field . ')->youtube }}"></iframe>' . "\n";
                $template .= '@endif' . "\n\n";
            } else {
                $template .= '{!! $infoblock->' . $field . ' ?? \'\' !!}' . "\n";
            }
        }

        $template .= '@endforeach' . "\n";

        return $template;
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($model)
    {
        unlink(get_application_frontend_theme_path() . '/views/infoblocks/' . $model->slug . '.blade.php');

        return $model->delete();
    }
}
