<?php

namespace Modules\Infoblocks\Repositories\Eloquent;

use Modules\Infoblocks\Entities\Template;
use Modules\Infoblocks\Repositories\GroupInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentGroupRepository extends EloquentCoreRepository implements GroupInterface
{
    public $model_config = 'modules/Infoblocks/Config/models/group.json';

    public function getTemplates()
    {
        $data = [];

        foreach (Template::all() as $template) {
            $data[$template->id] = $template;
        }

        return $data;
    }
}
