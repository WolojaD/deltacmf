<?php

namespace Modules\Infoblocks\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface TemplateInterface extends CoreInterface
{
    public function getFields();
}
