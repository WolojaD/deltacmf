<?php

namespace Modules\Infoblocks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class GroupTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $template = [
            'title' => $this->template->title ?? '',
            'image' => $this->template->image ?? '',
        ];

        return [
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at->toRfc2822String(),
            'title' => $this->defaultTranslate()->title ?? '',
            'short_code' => $this->short_code,
            'comment' => $this->comment,
            'template_id' => $template,
            'children_count' => $this->infoblocks->count() ?? null,
        ];
    }
}
