<?php

namespace Modules\Infoblocks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class InfoblockTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at->toRfc2822String(),
            'order' => $this->order,
            'title' => $this->defaultTranslate()->title,
        ];
    }
}
