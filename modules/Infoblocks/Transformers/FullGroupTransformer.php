<?php

namespace Modules\Infoblocks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullGroupTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'status' => $this->status,
            'comment' => $this->comment,
            'template_id' => $this->template_id,
            'short_code' => $this->short_code,
            'desktop_image' => $this->desktop_image,
            'mobile_image' => $this->mobile_image,
            'url' => $this->url,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $data[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
