<?php

namespace Modules\Infoblocks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullTemplateTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'fields' => $this->fields,
            'image' => $this->image,
        ];

        return $data;
    }
}
