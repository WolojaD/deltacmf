<?php

namespace Modules\Infoblocks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullInfoblockTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'status' => $this->status,
            'group_id' => $this->group_id,
            'image' => $this->image ?? null,
            'desktop_image' => $this->desktop_image ?? null,
            'mobile_image' => $this->mobile_image ?? null,
            'url' => $this->url ?? null,
            'file' => $this->file ?? null,
            'video' => $this->video ?? null,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
