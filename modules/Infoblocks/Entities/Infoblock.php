<?php

namespace Modules\Infoblocks\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Infoblock extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'infoblocks';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/infoblocks';

    protected $file_path = 'modules/Infoblocks/Config/models/infoblock.json';

    public $groupOrder = 'group_id';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
