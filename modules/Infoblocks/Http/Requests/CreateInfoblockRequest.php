<?php

namespace Modules\Infoblocks\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateInfoblockRequest extends BaseFormRequest
{
    // protected $file;

    // public function __construct()
    // {
    //     $this->file = base_path('modules/Infoblocks/Config/models/infoblock.json');
    // }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
