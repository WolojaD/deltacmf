<?php

namespace Modules\Infoblocks\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateTemplateRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Infoblocks/Config/models/template.json');
    }

    public function authorize()
    {
        return true;
    }
}
