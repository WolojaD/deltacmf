<?php

namespace Modules\Infoblocks\Http\Requests;

use Modules\Infoblocks\Entities\Group;
use Modules\Core\Internationalisation\BaseFormRequest;

class CreateGroupRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Infoblocks/Config/models/group.json');
    }

    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $request = $this;

        $max = Group::where('short_code', 'like', str_slug($this[key(get_application_frontend_locales())]['title']) . '%')
                ->pluck('short_code')
                ->map(function ($short_code) use ($request) {
                    return (int) trim(str_replace(str_slug($request[key(get_application_frontend_locales())]['title']), '', $short_code), '-');
                })->max();

        if (!is_null($max)) {
            $this->merge(['short_code' => str_slug($this[key(get_application_frontend_locales())]['title']) . '-' . ($max + 1)]);
        } else {
            $this->merge(['short_code' => str_slug($this[key(get_application_frontend_locales())]['title'])]);
        }

        if (is_numeric($max)) {
            $this->merge(['slug' => $this->slug . '-' . ($max + 1)]);
        }
    }
}
