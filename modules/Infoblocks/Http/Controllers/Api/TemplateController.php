<?php

namespace Modules\Infoblocks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Infoblocks\Repositories\TemplateInterface;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Infoblocks\Transformers\TemplateTransformer;
use Modules\Infoblocks\Http\Requests\CreateTemplateRequest;
use Modules\Infoblocks\Http\Requests\UpdateTemplateRequest;
use Modules\Infoblocks\Transformers\FullTemplateTransformer;

class TemplateController extends CoreApiController
{
    /**
     * @var TemplateInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(TemplateInterface $template)
    {
        $this->repository = $template;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return TemplateTransformer
     */
    public function index(Request $request, $id = false)
    {
        return TemplateTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullTemplateTransformer
     */
    public function find($id)
    {
        $template = $this->repository->find($id);

        return new FullTemplateTransformer($template);
    }

    /**
     * @param int $id
     * @param UpdateTemplateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateTemplateRequest $request)
    {
        $template = $this->repository->find($id);

        $locales = array_keys(get_application_frontend_locales());
        $locales[] = 'ident';

        $this->repository->update($template, $request->except($locales));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('infoblocks::models.of.template')]),
            'id' => $template->id
        ]);
    }

    /**
     * @param CreateTemplateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTemplateRequest $request)
    {
        $request->merge(['slug' => str_slug($request->title)]);

        $locales = array_keys(get_application_frontend_locales());
        $locales[] = 'ident';

        $template = $this->repository->create($request->except($locales));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('infoblocks::models.of.template')]),
            'id' => $template->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template = $this->repository->find($id);

        if (count($template->groups)) {
            return response()->json([
                'errors' => true,
                'message' => trans('infoblocks::messages.api.template has groups'),
            ]);
        }

        $this->repository->destroy($template);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('infoblocks::models.of.template')]),
        ]);
    }
}
