<?php

namespace Modules\Infoblocks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Infoblocks\Repositories\GroupInterface;
use Modules\Infoblocks\Transformers\GroupTransformer;
use Modules\Infoblocks\Repositories\InfoblockInterface;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Infoblocks\Http\Requests\CreateGroupRequest;
use Modules\Infoblocks\Http\Requests\UpdateGroupRequest;
use Modules\Infoblocks\Transformers\FullGroupTransformer;
use Modules\Infoblocks\Transformers\InfoblockTransformer;

class GroupController extends CoreApiController
{
    /**
     * @var GroupInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $infoblock;

    public function __construct(GroupInterface $group, InfoblockInterface $infoblock)
    {
        $this->repository = $group;
        $this->modelInfo = $this->repository->getJsonList();
        $this->infoblock = $infoblock;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return GroupTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['group_id' => $id]]);

            return InfoblockTransformer::collection($this->infoblock->serverPaginationFilteringFor($request));
        }

        return GroupTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullGroupTransformer
     */
    public function find($id)
    {
        $group = $this->repository->find($id);

        return new FullGroupTransformer($group);
    }

    /**
     * @param int $id
     * @param UpdateGroupRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateGroupRequest $request)
    {
        $group = $this->repository->find($id);
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $this->repository->update($group, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('infoblocks::models.of.group')]),
            'id' => $group->id
        ]);
    }

    /**
     * @param CreateGroupRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroupRequest $request)
    {
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $group = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('infoblocks::models.of.group')]),
            'id' => $group->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = $this->repository->find($id);

        $this->repository->destroy($group);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('infoblocks::models.of.group')]),
        ]);
    }
}
