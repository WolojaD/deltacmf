<?php

namespace Modules\Infoblocks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Infoblocks\Repositories\InfoblockInterface;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Infoblocks\Transformers\InfoblockTransformer;
use Modules\Infoblocks\Http\Requests\CreateInfoblockRequest;
use Modules\Infoblocks\Http\Requests\UpdateInfoblockRequest;
use Modules\Infoblocks\Transformers\FullInfoblockTransformer;

class InfoblockController extends CoreApiController
{
    /**
     * @var InfoblockInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(InfoblockInterface $infoblock)
    {
        $this->repository = $infoblock;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return InfoblockTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['group_id' => $id]]);

            return InfoblockTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return InfoblockTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullInfoblockTransformer
     */
    public function find($id)
    {
        $infoblock = $this->repository->find($id);

        return new FullInfoblockTransformer($infoblock);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function findNew(Request $request)
    {
        return [
            'fields' => $this->repository->getFieldsFromTemplate($request, 'create'),
            'breadcrumbs' => $this->repository->getFormBreadcrumbs($request->get('ident', 0), 'create'),
        ];
    }

    /**
     * Show the form for editin a resource.
     *
     * @return
     */
    public function findEdit(Request $request)
    {
        return [
            'fields' => $this->repository->getFieldsFromTemplate($request, 'edit'),
            'breadcrumbs' => $this->repository->getFormBreadcrumbs($request->get('ident', 0), 'edit'),
        ];
    }

    /**
     * @param int $id
     * @param UpdateInfoblockRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateInfoblockRequest $request)
    {
        $infoblock = $this->repository->find($id);

        $this->repository->update($infoblock, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('infoblocks::models.of.infoblock')]),
            'id' => $infoblock->id
        ]);
    }

    /**
     * @param CreateInfoblockRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateInfoblockRequest $request)
    {
        // dd($request->all());
        $infoblock = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('infoblocks::models.of.infoblock')]),
            'id' => $infoblock->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $infoblock = $this->repository->find($id);

        $this->repository->destroy($infoblock);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('infoblocks::models.of.infoblock')]),
        ]);
    }
}
