<?php

use Faker\Generator as Faker;
use Modules\Infoblocks\Entities\Group;
use Modules\Infoblocks\Entities\Template;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Template::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'title' => $faker->name,
        'fields' => $faker->words($nb = 3, $asText = false),
        'image' => $faker->name,
    ];

    return $data;
});

$factory->define(Group::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'template_id' => $faker->numberBetween(1, 1),
        'show_title' => $faker->numberBetween(0, 1),
        'comment' => $faker->numberBetween(0, 1) == 1 ? $faker->sentence : null,
        'short_code' => $faker->lexify('?????????'),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'description' => $faker->sentence,
        ];
    }

    return $data;
});
