<?php

namespace Modules\Infoblocks\Shortcodes;

use Modules\Infoblocks\Entities\Group;

class InfoblocksShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        $infoblock_group = Group::where('short_code', $shortcode->code)
            ->with('infoblocks', 'template')->first();

        if (null === $infoblock_group) {
            return '';
        }

        if ($infoblock_group) {
            return $infoblock_group->infoblocks->count() ? view('infoblocks::' . $infoblock_group->template->slug ?? 'index', compact('infoblock_group')) : '';
        } else {
            return '';
        }
    }
}
