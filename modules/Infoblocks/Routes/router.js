import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/infoblocks/template',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.infoblocks_template',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Templates',
                    breadcrumb: [
                        { name: 'Templates' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.infoblocks_template.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Templates Create',
                    breadcrumb: [
                        { name: 'Templates', link: 'api.backend.infoblocks_template' },
                        { name: 'Templates Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.infoblocks_template.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Templates Edit',
                    breadcrumb: [
                        { name: 'Templates', link: 'api.backend.infoblocks_template' },
                        { name: 'Templates Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/infoblocks/group',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.infoblocks_group',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Infoblocks',
                    breadcrumb: [
                        { name: 'Infoblocks' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.infoblocks_group.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Infoblocks Create',
                    breadcrumb: [
                        { name: 'Infoblocks', link: 'api.backend.infoblocks_group' },
                        { name: 'Infoblocks Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.infoblocks_group.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Infoblocks Edit',
                    breadcrumb: [
                        { name: 'Infoblocks', link: 'api.backend.infoblocks_group' },
                        { name: 'Infoblocks Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.infoblocks_group.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Infoblocks',
                    breadcrumb: [
                        { name: 'Infoblocks', link: 'api.backend.infoblocks_group' },
                        { name: 'Infoblock' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/infoblocks/infoblock',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.infoblocks_infoblock',
                redirect: { name: 'api.backend.infoblocks_group' }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.infoblocks_infoblock.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Infoblock Create',
                    breadcrumb: [
                        { name: 'Infoblocks', link: 'api.backend.infoblocks_group' },
                        { name: 'Infoblock Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.infoblocks_infoblock.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Infoblock Edit',
                    breadcrumb: [
                        { name: 'Infoblocks', link: 'api.backend.infoblocks_group' },
                        { name: 'Infoblock Edit' }
                    ]
                }
            }
        ]
    }
]
