<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Infoblocks');

Route::group(['prefix' => '/infoblocks/infoblock', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::post('text-editor-upload', ['as' => 'api.backend.infoblocks_infoblock.textEditorUpload', 'uses' => 'InfoblockController@textEditorUpload', 'middleware' => 'token-can:infoblocks.infoblock.edit']);
});
