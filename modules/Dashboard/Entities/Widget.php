<?php

namespace Modules\Dashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $fillable = ['widgets', 'user_id'];
    protected $table = 'dashboard__widgets';

    public function user()
    {
        $driver = config('application.user.config.driver');

        return $this->belongsTo("Modules\\Users\\Entities\\{$driver}\\User");
    }
}
