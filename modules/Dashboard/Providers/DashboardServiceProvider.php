<?php

namespace Modules\Dashboard\Providers;

use Modules\Core\Providers\ServiceProvider;
use Modules\Core\Events\LoadingBackendTranslations;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('dashboard_dashboard', array_dot(trans('dashboard::dashboard')));
            $event->load('dashboard_permissions', array_dot(trans('dashboard::permissions')));
        });
    }

    /**
     * Boot the application events.
     */
    public function boot()
    {
        $this->publishConfig('dashboard', 'permissions');
        $this->publishConfig('dashboard', 'config');
        $this->publishConfig('dashboard', 'settings');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
