<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class DashboardController extends CoreAdminController
{
    /**
     * Display the dashboard with its widgets.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }

    public function redirectIntended()
    {
        if (!session()->get('backend_locale')) {
            session()->put('backend_locale', locale());
        }

        // TODO Need to test
        // session()->put('backend_locale', locale());
        // dd(redirect()->intended(route(config('application.users.config.redirect_route_after_login')))->getTargetUrl());

        if (null !== session()->get('url.intented')) {
            return redirect()->intended(route(config('application.users.config.redirect_route_after_login')));
        }

        if (Sentinel::check() && null !== Sentinel::getUser()->last_url) {
            if (str_contains(Sentinel::getUser()->last_url, 'backend/users') && !Sentinel::hasAccess('users.index')) {
                return redirect()->route('backend.dashboard.index');
            }

            return redirect()->to(Sentinel::getUser()->last_url);
        }

        return redirect()->route('backend.dashboard.index');
    }
}
