<?php

namespace Modules\Dashboard\Http\Controllers\Api;

use Illuminate\Session\Store;
use Modules\Users\Contracts\Authentication;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class DashboardController extends CoreApiController
{
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @var SessionManager
     */
    private $session;

    /**
     * @param Authentication $auth
     */
    public function __construct(Authentication $auth, Store $session)
    {
        $this->auth = $auth;
        $this->session = $session;
    }

    /**
     * Display the dashboard with its widgets.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }
}
