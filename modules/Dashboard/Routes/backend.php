<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'backend.dashboard.redirectIntended', 'uses' => 'DashboardController@redirectIntended', 'middleware' => 'can:dashboard.index']);

Route::get('/dashboard', ['as' => 'backend.dashboard.index', 'uses' => 'DashboardController@index', 'middleware' => 'can:dashboard.index']);
