<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::backendRouteList('Ecommerce');

/*
|--------------------------------------------------------------------------
| Ecommerce Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'ecommerce'], function () {
    Route::get('/', ['as' => 'backend.ecommerce.index', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.category.index']);
});

/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/product'], function () {
    Route::get('{id}/suggestions', ['as' => 'backend.ecommerce_product.suggestions', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.product.edit']);
});

/*
|--------------------------------------------------------------------------
| Order Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/order'], function () {
    Route::get('user/{id}', ['as' => 'backend.ecommerce_order_user.index', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.order.index']);
});

/*
|--------------------------------------------------------------------------
| Info Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/info'], function () {
    Route::get('/', ['as' => 'backend.ecommerce_info.index', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.parameter.index']);

    Route::get('{id}', ['as' => 'backend.ecommerce_info.show', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.parameter.index']);
});

/*
|--------------------------------------------------------------------------
| Log Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/stock_log'], function () {
    Route::get('full', ['as' => 'backend.ecommerce_stock_log.index', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.stock.index']);
    Route::get('current_day', ['as' => 'backend.ecommerce_stock_log.index', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.stock.index']);
    Route::get('total_by_dates', ['as' => 'backend.ecommerce_stock_log.index', 'uses' => 'EcommerceController@index', 'middleware' => 'can:ecommerce.stock.index']);
});
