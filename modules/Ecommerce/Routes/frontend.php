<?php

/*
|--------------------------------------------------------------------------
| FrontEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register FrontEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "frontend" middleware group. Now create something great!
|
*/
Route::resource('comparison', 'ComparisonController');
Route::get('search', 'SearchController@index');
Route::get('search/{page}', 'SearchController@index')->where(['page' => '^page-[\d]+$']);
