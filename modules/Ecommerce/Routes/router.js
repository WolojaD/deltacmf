import helpers from '@services/helpers'

const tabs = [
    {
        name: 'products',
        label: 'ecommerce_sidebar.submenu.products',
        permission: 'ecommerce.product.index',
        routeName: 'api.backend.ecommerce_product',
    },
    {
        name: 'categories',
        label: 'ecommerce_sidebar.submenu.categories',
        permission: 'ecommerce.category.index',
        routeName: 'api.backend.ecommerce_category'
    }
]

const tabs_log = [
    {
        name: 'full',
        label: 'ecommerce_sidebar.submenu.log.full',
        permission: 'ecommerce.stock.index',
        routeName: 'api.backend.ecommerce_stock_log.full',
    },
    {
        name: 'current_day',
        label: 'ecommerce_sidebar.submenu.log.current_day',
        permission: 'ecommerce.stock.index',
        routeName: 'api.backend.ecommerce_stock_log.current_day'
    },
    {
        name: 'total_by_dates',
        label: 'ecommerce_sidebar.submenu.log.total_by_dates',
        permission: 'ecommerce.stock.index',
        routeName: 'api.backend.ecommerce_stock_log.total_by_dates'
    }
]


export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        redirect: { name: 'api.backend.ecommerce_category' }
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/category',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_category',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'categories'
                },
                meta: {
                    pageTitle: 'Products',
                    breadcrumb: [
                        { name: 'Products' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_category.show',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'categories'
                },
                meta: {
                    pageTitle: 'Products',
                    breadcrumb: [
                        { name: 'Products' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_category.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Category Create',
                    breadcrumb: [
                        { name: 'Category', link: 'api.backend.ecommerce_category' },
                        { name: 'Category Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_category.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Category Edit',
                    breadcrumb: [
                        { name: 'Category', link: 'api.backend.ecommerce_category' },
                        { name: 'Category Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/brand',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_brand',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Brand',
                    breadcrumb: [
                        { name: 'Brand' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_brand.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Brand',
                    breadcrumb: [
                        { name: 'Brand' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_brand.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Brand Create',
                    breadcrumb: [
                        { name: 'Brand', link: 'api.backend.ecommerce_brand' },
                        { name: 'Brand Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_brand.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Brand Edit',
                    breadcrumb: [
                        { name: 'Brand', link: 'api.backend.ecommerce_brand' },
                        { name: 'Brand Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/product',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_product',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'products'
                },
                meta: {
                    pageTitle: 'Product',
                    breadcrumb: [
                        { name: 'Product' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_product.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Product Create',
                    breadcrumb: [
                        { name: 'Product', link: 'api.backend.ecommerce_product' },
                        { name: 'Product Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_product.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Product Edit',
                    breadcrumb: [
                        { name: 'Product', link: 'api.backend.ecommerce_product' },
                        { name: 'Product Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/suggestions',
                name: 'api.backend.ecommerce_product.suggestions',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Product suggestions',
                    breadcrumb: [
                        { name: 'Product', link: 'api.backend.ecommerce_product' },
                        { name: 'Product suggestions' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/parameter',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_info',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Info',
                    breadcrumb: [
                        { name: 'Info' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_info.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Info',
                    breadcrumb: [
                        { name: 'Info' , link: 'api.backend.ecommerce_info'},
                        { name: 'Info Show' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/parameter_data',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_parameter_data.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Parameter Data Create',
                    breadcrumb: [
                        { name: 'Parameter Data', link: 'api.backend.ecommerce_parameter_data' },
                        { name: 'Parameter Data Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_parameter_data.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Parameter Data Edit',
                    breadcrumb: [
                        { name: 'Parameter Data', link: 'api.backend.ecommerce_parameter_data' },
                        { name: 'Parameter Data Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/parameter',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_parameter',
                redirect: { name: 'api.backend.ecommerce_info' },
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Parameter',
                    breadcrumb: [
                        { name: 'Parameter' }
                    ]
                }
            },
            {
                path: ':ident(\\d)',
                name: 'api.backend.ecommerce_parameter.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Parameter data',
                    breadcrumb: [
                        { name: 'Infos', link: 'api.backend.ecommerce_info'},
                        { name: 'Parameter data' },
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_parameter.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Parameter Create',
                    breadcrumb: [
                        { name: 'Parameter', link: 'api.backend.ecommerce_parameter' },
                        { name: 'Parameter Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_parameter.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Parameter Edit',
                    breadcrumb: [
                        { name: 'Parameter', link: 'api.backend.ecommerce_parameter' },
                        { name: 'Parameter Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/delivery_pay',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_delivery_pay',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Delivery Pay',
                    breadcrumb: [
                        { name: 'Delivery Pay' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_delivery_pay.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Delivery Pay',
                    breadcrumb: [
                        { name: 'Delivery Pay' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_delivery_pay.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Delivery Pay Create',
                    breadcrumb: [
                        { name: 'Delivery Pay', link: 'api.backend.ecommerce_delivery_pay' },
                        { name: 'Delivery Pay Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_delivery_pay.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Delivery Pay Edit',
                    breadcrumb: [
                        { name: 'Delivery Pay', link: 'api.backend.ecommerce_delivery_pay' },
                        { name: 'Delivery Pay Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_delivery_pay.show.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Delivery Pay Edit',
                    breadcrumb: [
                        { name: 'Delivery Pay', link: 'api.backend.ecommerce_delivery_pay' },
                        { name: 'Delivery Pay Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/delivery_type',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_delivery_type',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Delivery Type',
                    breadcrumb: [
                        { name: 'Delivery Type' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_delivery_type.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Delivery Type',
                    breadcrumb: [
                        { name: 'Delivery Type' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_delivery_type.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Delivery Type Create',
                    breadcrumb: [
                        { name: 'Delivery Type', link: 'api.backend.ecommerce_delivery_type' },
                        { name: 'Delivery Type Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_delivery_type.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Delivery Type Edit',
                    breadcrumb: [
                        { name: 'Delivery Type', link: 'api.backend.ecommerce_delivery_type' },
                        { name: 'Delivery Type Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_delivery_type.show.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Delivery Type Edit',
                    breadcrumb: [
                        { name: 'Delivery Type', link: 'api.backend.ecommerce_delivery_type' },
                        { name: 'Delivery Type Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/order',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_order',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Order',
                    breadcrumb: [
                        { name: 'Order' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_order.show',
                component: require('@/views/ecommerce/order/show').default,
                meta: {
                    pageTitle: 'Order',
                    breadcrumb: [
                        { name: 'Order' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_order.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Order Edit',
                    breadcrumb: [
                        { name: 'Order', link: 'api.backend.ecommerce_order' },
                        { name: 'Order Edit' }
                    ]
                }
            },
            {
                path: 'user/:ident(\\d+)',
                name: 'api.backend.ecommerce_order_user.index',
                component: require('@/views/ecommerce/order/user').default,
                meta: {
                    pageTitle: 'User Edit',
                    breadcrumb: [
                        { name: 'Order', link: 'api.backend.ecommerce_order' },
                        { name: 'User Edit' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/sale',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_sale',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Sale',
                    breadcrumb: [
                        { name: 'Sale' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.ecommerce_sale.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Sale',
                    breadcrumb: [
                        { name: 'Sale' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.ecommerce_sale.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Sale Edit',
                    breadcrumb: [
                        { name: 'Sale', link: 'api.backend.ecommerce_sale' },
                        { name: 'Sale Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.ecommerce_sale.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Sale Create',
                    breadcrumb: [
                        { name: 'Sale', link: 'api.backend.ecommerce_sale' },
                        { name: 'Sale Create' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/stock',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_stock',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Stock',
                    breadcrumb: [
                        { name: 'Stock' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/ecommerce/stock_log',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.ecommerce_stock_log',
                component: require('@/views/crud/index').default,
                props: {
                    tabs: tabs_log,
                    activeName: 'default'
                },
                meta: {
                    pageTitle: 'Stock Log',
                    breadcrumb: [
                        { name: 'Stock Log' }
                    ]
                }
            },
            {
                path: 'full',
                name: 'api.backend.ecommerce_stock_log.full',
                component: require('@/views/ecommerce/log/show').default,
                meta: {
                    pageTitle: 'Stock Log Full',
                    breadcrumb: [
                        { name: 'Stock Log Full' }
                    ]
                }
            },
            {
                path: 'current_day',
                name: 'api.backend.ecommerce_stock_log.current_day',
                component: require('@/views/ecommerce/log/current_day').default,
                meta: {
                    pageTitle: 'Stock Log Current Day',
                    breadcrumb: [
                        { name: 'Stock Log Current Day' }
                    ]
                }
            },
            {
                path: 'total_by_dates',
                name: 'api.backend.ecommerce_stock_log.total_by_dates',
                component: require('@/views/ecommerce/log/total_by_dates').default,
                meta: {
                    pageTitle: 'Stock Log By dates',
                    breadcrumb: [
                        { name: 'Stock Log By dates' }
                    ]
                }
            }
        ]
    },
]
