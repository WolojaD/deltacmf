<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Ecommerce');

/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/product', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('relations-lists', ['as' => 'api.backend.ecommerce_product.relationsLists', 'uses' => 'ProductController@relationsLists', 'middleware' => 'token-can:ecommerce.product.index']);

    Route::get('versions-fields', ['as' => 'api.backend.ecommerce_product.versionsFields', 'uses' => 'ProductController@versionsFields', 'middleware' => 'token-can:ecommerce.product.index']);

    Route::get('{ident}/suggestions', ['as' => 'api.backend.ecommerce_product.suggestions', 'uses' => 'ProductController@suggestions', 'middleware' => 'token-can:ecommerce.product.edit']);

    Route::get('filter-product-list', ['as' => 'api.backend.ecommerce_product.suggestions.filterProductList', 'uses' => 'ProductController@filterProductList', 'middleware' => 'token-can:ecommerce.product.edit']);

    Route::post('add-products/{id}', ['as' => 'api.backend.ecommerce_product.suggestions.addProducts', 'uses' => 'ProductController@addProducts', 'middleware' => 'token-can:ecommerce.product.edit']);

    Route::delete('detach-product/{id}', ['as' => 'api.backend.ecommerce_product.suggestions.detach.ecommerce_product', 'uses' => 'ProductController@detachProduct', 'middleware' => 'token-can:ecommerce.product.edit']);

    Route::delete('detach-products/{id}', ['as' => 'api.backend.ecommerce_product.suggestions.detach_multiple.ecommerce_product', 'uses' => 'ProductController@detachProducts', 'middleware' => 'token-can:ecommerce.product.edit']);
});

/*
|--------------------------------------------------------------------------
| Order Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/order', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('user/{ident}/find', ['as' => 'api.backend.ecommerce_order_user.find', 'uses' => 'OrderController@userOrders', 'middleware' => 'token-can:ecommerce.order.index']);

    Route::get('filter-product-list', ['as' => 'api.backend.ecommerce_order.filterProductList', 'uses' => 'OrderController@filterProductList', 'middleware' => 'token-can:ecommerce.order.edit']);

    Route::post('add-products/{id}', ['as' => 'api.backend.ecommerce_order.addProducts', 'uses' => 'OrderController@addProducts', 'middleware' => 'token-can:ecommerce.order.edit']);

    Route::put('{ident}/change-quantity', ['as' => 'api.backend.ecommerce_order.changeQuantity.ecommerce_product_version', 'uses' => 'OrderController@changeQuantity', 'middleware' => 'token-can:ecommerce.order.edit']);

    Route::put('send-mail-order-data/{ident}/', ['as' => 'api.backend.ecommerce_order.sendMailOrderData', 'uses' => 'OrderController@sendMailOrderData', 'middleware' => 'token-can:ecommerce.order.edit']);

    Route::delete('detach-product/{id}', ['as' => 'api.backend.ecommerce_order.detach.ecommerce_product_version', 'uses' => 'OrderController@detachProduct', 'middleware' => 'token-can:ecommerce.order.edit']);

    Route::delete('detach-products/{id}', ['as' => 'api.backend.ecommerce_order.detach_multiple.ecommerce_product_version', 'uses' => 'OrderController@detachProducts', 'middleware' => 'token-can:ecommerce.order.edit']);
});

/*
|--------------------------------------------------------------------------
| Sale Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/sale', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('filter-product-list', ['as' => 'api.backend.ecommerce_sale.filterProductList', 'uses' => 'SaleController@filterProductList', 'middleware' => 'token-can:ecommerce.sale.edit']);

    Route::post('add-products/{id}', ['as' => 'api.backend.ecommerce_sale.addProducts', 'uses' => 'SaleController@addProducts', 'middleware' => 'token-can:ecommerce.sale.edit']);

    Route::post('text-editor-upload', ['as' => 'api.backend.ecommerce_sale.textEditorUpload', 'uses' => 'SaleController@textEditorUpload', 'middleware' => 'token-can:ecommerce.sale.edit']);

    Route::put('change-discount/{id}', ['as' => 'api.backend.ecommerce_sale.changeDiscount', 'uses' => 'SaleController@changeDiscount', 'middleware' => 'token-can:ecommerce.sale.edit']);

    Route::delete('detach-product/{id}', ['as' => 'api.backend.ecommerce_sale.detach.ecommerce_product', 'uses' => 'SaleController@detachProduct', 'middleware' => 'token-can:ecommerce.sale.edit']);

    Route::delete('detach-products/{id}', ['as' => 'api.backend.ecommerce_sale.detach_multiple.ecommerce_product', 'uses' => 'SaleController@detachProducts', 'middleware' => 'token-can:ecommerce.sale.edit']);
});

/*
|--------------------------------------------------------------------------
| Info Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/info', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.ecommerce_info.index', 'uses' => 'ParameterController@index', 'middleware' => 'token-can:ecommerce.parameter.index']);
    Route::get('show/{ident}', ['as' => 'api.backend.ecommerce_info.show', 'uses' => 'ParameterController@index', 'middleware' => 'token-can:ecommerce.parameter.index']);
});

/*
|--------------------------------------------------------------------------
| Brand Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/brand', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::delete('detach-product/{id}', ['as' => 'api.backend.ecommerce_brand.detach.ecommerce_product', 'uses' => 'BrandController@detachProduct', 'middleware' => 'token-can:ecommerce.brand.edit']);

    Route::delete('detach-products/{id}', ['as' => 'api.backend.ecommerce_brand.detach_multiple.ecommerce_product', 'uses' => 'BrandController@detachProducts', 'middleware' => 'token-can:ecommerce.brand.edit']);
});

/*
|--------------------------------------------------------------------------
| Stock Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/stock', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::put('{ident}/change-quantity', ['as' => 'api.backend.ecommerce_stock.changeQuantity.ecommerce_stock', 'uses' => 'StockController@changeQuantity', 'middleware' => 'token-can:ecommerce.stock.edit']);
    Route::get('filter-product-list', ['as' => 'api.backend.ecommerce_stock.filterProductList', 'uses' => 'StockController@filterProductList', 'middleware' => 'token-can:ecommerce.stock.edit']);
    Route::post('add-product/{id}', ['as' => 'api.backend.ecommerce_stock.addProduct', 'uses' => 'StockController@addProduct', 'middleware' => 'token-can:ecommerce.stock.edit']);
    Route::get('relations-lists', ['as' => 'api.backend.ecommerce_stock.relationsLists', 'uses' => 'StockController@relationsLists', 'middleware' => 'token-can:ecommerce.stock.index']);
});

/*
|--------------------------------------------------------------------------
| Stock Log Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => '/ecommerce/stock_log', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('full', ['as' => 'api.backend.ecommerce_stock_log.full', 'uses' => 'StockLogController@fullLog', 'middleware' => 'token-can:ecommerce.stock.index']);
    Route::get('full-export', ['as' => 'api.backend.ecommerce_stock_log.full.exportExcel', 'uses' => 'StockLogController@fullExportExcel', 'middleware' => 'token-can:ecommerce.stock.index']);
    Route::get('current_day', ['as' => 'api.backend.ecommerce_stock_log.current_day', 'uses' => 'StockLogController@currentDayLog', 'middleware' => 'token-can:ecommerce.stock.index']);
    Route::get('current_day-export', ['as' => 'api.backend.ecommerce_stock_log.current_day.exportExcel', 'uses' => 'StockLogController@currentDayExportExcel', 'middleware' => 'token-can:ecommerce.stock.index']);
    Route::get('total_by_dates', ['as' => 'api.backend.ecommerce_stock_log.total_by_dates', 'uses' => 'StockLogController@totalByDatesLog', 'middleware' => 'token-can:ecommerce.stock.index']);
    Route::get('total_by_dates-export', ['as' => 'api.backend.ecommerce_stock_log.total_by_dates.exportExcel', 'uses' => 'StockLogController@totalByDatesExportExcel', 'middleware' => 'token-can:ecommerce.stock.index']);
});
