<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Ecommerce\Entities\Sale;
use Modules\Ecommerce\Entities\Brand;
use Modules\Ecommerce\Entities\Order;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Entities\ProductValue;
use Modules\Ecommerce\Entities\ParameterData;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\Eloquent\EloquentOrderRepository;

class EcommerceDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            $orders = factory(Order::class, 10)->create(['user_id' => 1]);

            $main_category = factory(Category::class)->create([
                'ru' => ['title' => 'main category'],
                'en' => ['title' => 'main category'],
                'uk' => ['title' => 'main category']
            ]);

            factory(Parameter::class)->create();
            factory(Parameter::class)->states('multifkey')->create();
            factory(Parameter::class, 4)->states('fkey')->create();

            $keyed = Parameter::whereIn('data_type', [4, 5])->get();

            foreach ($keyed as $fkey) {
                factory(ParameterData::class, 3)->create(['parameter_id' => $fkey->id]);
            }

            $this->generateStore();

            $parameters = Parameter::all();

            $products = Product::all();

            foreach ($parameters as $parameter) {
                if ($parameter->key_type == 'multifkey' || !$parameter->field_type) {
                    continue;
                }

                if ($parameter->key_type == 'fkey') {
                    $p_min = $parameter->data()->min('id');
                    $p_max = $parameter->data()->max('id');
                }

                foreach ($products as $product) {
                    $data = $parameter->randomizeData();

                    if ($parameter->key_type == 'fkey') {
                        ProductValue::create([
                            'parameter_id' => $parameter->id,
                            'product_id' => $product->id,
                            'integer' => rand($p_min, $p_max),
                            'data_type' => $parameter->key_type
                        ]);
                    } elseif (is_array($data)) {
                        ProductValue::create(array_merge([
                            'parameter_id' => $parameter->id,
                            'product_id' => $product->id,
                            'data_type' => $parameter->key_type
                        ], $data));
                    } else {
                        ProductValue::create([
                            'parameter_id' => $parameter->id,
                            'product_id' => $product->id,
                            $parameter->field_type => $data,
                            'data_type' => $parameter->key_type
                        ]);
                    }
                }
            }

            $order_repository = new EloquentOrderRepository(new Order);

            foreach ($orders as $order) {
                $prod_versions = ProductVersion::inRandomOrder()->limit(4)->with('product')->get();

                $order_repository->attachProducts($prod_versions, $order->id);
            }

            $sale = factory(Sale::class)->create();
        }
    }

    public function generateStore()
    {
        factory(Brand::class, 2)->create();
        for ($i = 1; $i < 7; $i++) {
            $data = [
                'parent_id' => 1,
                'status' => 1,
                'slug' => 'kategoria-' . $i,
            ];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'категория ' . $i
                ];
            }

            $category = factory(Category::class)->create($data);

            unset($data);

            $j_max = rand(3, 6);

            for ($j = 1; $j < $j_max; $j++) {
                $data = [
                    'parent_id' => $category->id,
                    'status' => 1,
                    'slug' => 'podkategoria-' . $j . '-kategorii-' . $i,
                ];

                foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                    $data[$key] = [
                        'title' => 'подкатегория ' . $j . ' категории ' . $i,
                    ];
                }

                $sub_category = factory(Category::class)->create($data);
                unset($data);

                if (rand(0, 1)) {
                    continue;
                }

                $k_max = rand(2, 8);

                for ($k = 1; $k < $k_max; $k++) {
                    $data = [
                        'category_id' => $sub_category->id,
                        'status' => 1,
                        'brand_id' => rand(1, 2),
                        'slug' => 'product-' . $k . '-podkategorii-' . $j . '-kategorii-' . $i,
                    ];

                    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                        $data[$key] = [
                            'title' => 'товар ' . $k . ' подкатегории ' . $j . ' категории ' . $i
                        ];
                    }

                    $product = factory(Product::class)->create($data);
                    factory(ProductVersion::class, 2)->create(['product_id' => $product->id]);

                    unset($data);
                }
            }
        }
    }
}
