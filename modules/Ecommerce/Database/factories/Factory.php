<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Modules\Ecommerce\Entities\Sale;
use Modules\Ecommerce\Entities\Brand;
use Modules\Ecommerce\Entities\Order;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Entities\ParameterData;
use Modules\Ecommerce\Entities\ProductVersion;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'comment' => $faker->sentence,
        'delivery' => $faker->sentence,
        'payment' => $faker->sentence,
        'situation' => $faker->numberBetween(1, 4),
        'is_quick' => $faker->boolean,
        'ip' => $faker->ipv4,
        'user_id' => 1
    ];
});

$factory->define(Category::class, function (Faker $faker) {
    $data = [
        'slug' => $faker->slug
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
        ];
    }

    return $data;
});

$factory->state(Category::class, 'published', [
    'status' => 1,
]);

$factory->define(Brand::class, function (Faker $faker) {
    $data = [
        'slug' => $faker->slug
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
        ];
    }

    return $data;
});

$factory->define(Product::class, function (Faker $faker) {
    $data = [
        'slug' => $faker->slug
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
        ];
    }

    return $data;
});

$factory->state(Product::class, 'published', [
    'status' => 1,
]);

$factory->define(ProductVersion::class, function (Faker $faker) {
    $price = $faker->numberBetween(100, 10000);
    $discount = 0;//$faker->numberBetween(1, 100);
    $data = [
        'article' => $faker->slug,
        'quantity' => $faker->numberBetween(1, 100),
        'price' => $price,
        'discount' => $discount,
        'discount_price' => $price * (1 - $discount / 100),
    ];

    // foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
    //     $data[$key] = [
    //         'title' => $faker->sentence,
    //     ];
    // }

    return $data;
});

$factory->define(Sale::class, function (Faker $faker) {
    $data = [
        'date_from' => Carbon::now()->subDays(3),
        'date_to' => Carbon::now()->addDays(3),
        'slug' => $faker->slug,
        'discount' => $faker->numberBetween(1, 100),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
        ];
    }

    return $data;
});

$factory->define(Parameter::class, function (Faker $faker) {
    $data = [
        'data_type' => $faker->numberBetween(1, 9),
        'comment' => $faker->sentence,
        'slug' => $faker->slug,
        'required' => $faker->numberBetween(0, 1),
        'in_filter' => $faker->numberBetween(0, 1),
        'to_versions' => $faker->numberBetween(0, 1),
        'hidden_filter' => $faker->numberBetween(0, 1),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
        ];
    }

    return $data;
});

$factory->state(Parameter::class, 'fkey', [
    'data_type' => 4,
]);

$factory->state(Parameter::class, 'multifkey', [
    'data_type' => 5,
]);

$factory->define(ParameterData::class, function (Faker $faker) {
    $data = [
        'slug' => $faker->slug,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
        ];
    }

    return $data;
});
