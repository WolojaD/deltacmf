<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeigns extends Migration
{
    public function up()
    {
        Schema::table('ecommerce_products', function (Blueprint $table) {
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('SET NULL');
        });

        Schema::table('ecommerce_orders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }
}
