<?php

namespace Modules\Ecommerce\Tests\Feature;

use Modules\Page\Entities\Page;
use Modules\Core\Tests\CoreTestCase;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Entities\ProductValue;
use Modules\Ecommerce\Entities\ParameterData;
use Modules\Ecommerce\Entities\ProductVersion;

class EcommerceTest extends CoreTestCase
{
    public function presetStore()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1]);
        $created_products_page = factory(Page::class)->create([
            'slug' => 'fantom_storepage_u_cant_see',
            'page_template' => 'products',
            'status' => 1,
            // 'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $categories[0] = factory(Category::class)->states('published')->create([
            'parent_id' => null,
            'slug' => 'category-0',
        ]);
        $categories[1] = factory(Category::class)->states('published')->create([
            'parent_id' => 1,
            'slug' => 'category-1',
        ]);

        $categories[2] = factory(Category::class)->states('published')->create([
            'parent_id' => 1,
            'slug' => 'category-2',
        ]);
        $categories[11] = factory(Category::class)->states('published')->create([
            'parent_id' => $categories[1]->id,
            'slug' => 'category-3',
        ]);
        $products[0] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[0]->id,
            'slug' => 'products-0',
        ]);

        $products[1] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[1]->id,
            'slug' => 'products-1',
        ]);

        $products[2] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[11]->id,
            'slug' => 'products-2',
        ]);

        $products[3] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[2]->id,
            'slug' => 'products-3',
        ]);

        foreach ($products as $product) {
            factory(ProductVersion::class)->create(['product_id' => $product->id]);
        }

        $this->categories = $categories;
        $this->products = $products;
    }

    public function parameterGenerate()
    {
        if (!isset($this->products)) {
            $this->presetStore();
        }

        factory(Parameter::class, 3)->create(['data_type' => 4]);

        $keyed = Parameter::whereIn('data_type', [4, 5])->get();

        foreach ($keyed as $fkey) {
            factory(ParameterData::class, 3)->create(['parameter_id' => $fkey->id]);
        }

        $parameters = Parameter::all();

        foreach ($parameters as $parameter) {
            $p_min = $parameter->data()->min('id');
            $p_max = $parameter->data()->max('id');
            ProductValue::create([
                'parameter_id' => $parameter->id,
                'product_id' => $this->products[0]->id,
                $parameter->field_type => rand($p_min, $p_max),
                'data_type' => $parameter->key_type
            ]);
        }
    }

    /** @test */
    public function category_has_sub_categories_and_all_products_of_all_sub_categories()
    {
        $this->presetStore();

        $this->get('/' . $this->categories[0]->slug)
            ->assertStatus(200)
            ->assertSee($this->categories[1]->slug)
            ->assertSee($this->categories[2]->slug)
            ->assertSee($this->products[0]->slug)
            ->assertSee($this->products[1]->slug)
            ->assertSee($this->products[2]->slug)
            ->assertSee($this->products[3]->slug);
    }

    /** @test */
    public function products_can_be_paginated()
    {
        $this->presetStore();
        $this->app['application.settings']->set('ecommerce::products_per_page', 1);

        $this->get('/' . $this->categories[0]->slug . '/page-2')
            ->assertStatus(200)
            ->assertSee($this->categories[1]->slug)
            ->assertSee($this->categories[2]->slug)
            ->assertDontSee($this->products[0]->slug)
            ->assertSee($this->products[1]->slug)
            ->assertDontSee($this->products[2]->slug)
            ->assertDontSee($this->products[3]->slug);
    }

    /** @test */
    public function out_of_total_pagination_aborts_404()
    {
        $this->withExceptionHandling();
        $this->presetStore();
        $this->app['application.settings']->set('ecommerce::products_per_page', 1);

        $this->get('/' . $this->categories[0]->slug . '/page-5')
            ->assertStatus(404);

        $this->get('/' . $this->categories[0]->slug . '/page-0')
            ->assertStatus(404);
    }

    /** @test */
    public function there_is_no_page_1_for_pagination()
    {
        $this->withExceptionHandling();
        $this->presetStore();
        $this->app['application.settings']->set('ecommerce::products_per_page', 1);

        $this->get('/' . $this->categories[0]->slug . '/page-1')
            ->assertStatus(404);
    }

    /** @test */
    public function product_url_is_slug()
    {
        $this->presetStore();
        $this->get('/' . $this->products[0]->slug)->assertStatus(200);
        $this->get('/' . $this->products[1]->slug)->assertStatus(200);
        $this->get('/' . $this->products[2]->slug)->assertStatus(200);
        $this->get('/' . $this->products[3]->slug)->assertStatus(200);
        // $response->assertDontSee($categories[11]->slug);
    }

    /** @test */
    public function there_is_top_products_works_like_categories()
    {
        $this->presetStore();

        $created_products_page = factory(Page::class)->create([
            'slug' => 'top_products',
            'page_template' => 'top_products',
            'status' => 1,
            // 'relation' => 1,
            'parent_id' => 1,
        ]);

        $this->products[1]->update(['is_top' => 1]);

        $this->get('/top_products/' . $this->categories[0]->slug)
            ->assertStatus(200)
            ->assertSee($this->categories[1]->slug)
            ->assertSee($this->categories[2]->slug)
            ->assertDontSee($this->products[0]->slug)
            ->assertSee($this->products[1]->slug)
            ->assertDontSee($this->products[2]->slug)
            ->assertDontSee($this->products[3]->slug);
    }

    /** @test */
    public function there_is_new_products_works_like_categories()
    {
        $this->presetStore();
        $created_products_page = factory(Page::class)->create([
            'slug' => 'new_products',
            'page_template' => 'new_products',
            'status' => 1,
            // 'relation' => 1,
            'parent_id' => 1,
        ]);

        $this->products[1]->update(['is_new' => 0]);

        $this->get('/new_products/' . $this->categories[0]->slug)
            ->assertStatus(200)
            ->assertSee($this->categories[1]->slug)
            ->assertSee($this->categories[2]->slug)
            ->assertSee($this->products[0]->slug)
            ->assertDontSee($this->products[1]->slug)
            ->assertSee($this->products[2]->slug)
            ->assertSee($this->products[3]->slug);
    }

    /** @test */
    public function there_is_discounted_products_works_like_categories()
    {
        $this->presetStore();
        $created_products_page = factory(Page::class)->create([
            'slug' => 'discounted_products',
            'page_template' => 'discounted_products',
            'status' => 1,
            // 'relation' => 1,
            'parent_id' => 1,
        ]);

        $this->products[1]->versions()->first()->update(['price' => 100000, 'discount' => 2, 'discount_price' => 98000]);

        $this->get('/discounted_products/' . $this->categories[0]->slug)
            ->assertStatus(200)
            ->assertSee($this->categories[1]->slug)
            ->assertSee($this->categories[2]->slug)
            ->assertDontSee($this->products[0]->slug)
            ->assertSee($this->products[1]->slug)
            ->assertDontSee($this->products[2]->slug)
            ->assertDontSee($this->products[3]->slug);
    }

    /**
     * ---------------
     * filters
     * ---------------
     */

    /** @test */
    public function products_can_be_filtered()
    {
        $this->parameterGenerate();

        $parameter_datas = ParameterData::groupBy('parameter_id')->get();
        $filter_slug = $parameter_datas->pluck('id')->implode('_');
        foreach ($parameter_datas as $data) {
            ProductValue::where([
                'parameter_id' => $data->parameter_id,
                'product_id' => 1,
            ])->update([
                'integer' => $data->id
            ]);
        }

        $this->get('/' . $this->categories[0]->slug . '/' . $filter_slug)
            ->assertStatus(200)
            ->assertSee($this->categories[1]->slug)
            ->assertSee($this->categories[2]->slug)
            ->assertSee($this->products[0]->slug)
            ->assertDontSee($this->products[1]->slug)
            ->assertDontSee($this->products[2]->slug)
            ->assertDontSee($this->products[3]->slug);
    }

    /** @test */
    public function filters_must_be_in_alphabeticall_order()
    {
        $this->withExceptionHandling();
        $this->parameterGenerate();

        $filter_slug = ParameterData::groupBy('parameter_id')->orderByDesc('id')->pluck('id')->implode('_');

        $this->get('/' . $this->categories[0]->slug . '/' . $filter_slug)
            ->assertStatus(404);
    }
}
