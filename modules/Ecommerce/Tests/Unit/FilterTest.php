<?php

namespace Modules\Ecommerce\Tests\Unit;

use Modules\Page\Entities\Page;
use Modules\Core\Tests\CoreTestCase;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Entities\ProductValue;
use Modules\Ecommerce\Entities\ParameterData;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\Eloquent\EloquentCategoryRepository;

class FilterTest extends CoreTestCase
{
    public function presetStore()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1]);
        $created_products_page = factory(Page::class)->create([
            'slug' => 'fantom_storepage_u_cant_see',
            'page_template' => 'products',
            'status' => 1,
            // 'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $categories[0] = factory(Category::class)->states('published')->create([
            'parent_id' => null,
            'slug' => 'category_0',
        ]);
        $categories[1] = factory(Category::class)->states('published')->create([
            'parent_id' => 1,
            'slug' => 'category_1',
        ]);

        $categories[2] = factory(Category::class)->states('published')->create([
            'parent_id' => 1,
            'slug' => 'category_2',
        ]);
        $categories[11] = factory(Category::class)->states('published')->create([
            'parent_id' => $categories[1]->id,
            'slug' => 'category_3',
        ]);
        $products[0] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[0]->id,
            'slug' => 'products_0',
        ]);

        $products[1] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[1]->id,
            'slug' => 'products_1',
        ]);

        $products[2] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[11]->id,
            'slug' => 'products_2',
        ]);

        $products[3] = factory(Product::class)->states('published')->create([
            'category_id' => $categories[2]->id,
            'slug' => 'products_3',
        ]);

        foreach ($products as $product) {
            factory(ProductVersion::class)->create(['product_id' => $product->id]);
        }

        $this->categories = $categories;
        $this->products = $products;
    }

    public function parameterGenerate()
    {
        if (!isset($this->products)) {
            $this->presetStore();
        }

        factory(Product::class, 20)->states('published')->create([
            'category_id' => 4
        ]);

        factory(Parameter::class, 3)->create(['data_type' => 4]);

        $keyed = Parameter::whereIn('data_type', [4, 5])->get();

        foreach ($keyed as $fkey) {
            factory(ParameterData::class, 3)->create(['parameter_id' => $fkey->id]);
        }

        $parameters = Parameter::all();
        $products = Product::all();

        foreach ($parameters as $parameter) {
            if ($parameter->key_type == 'multifkey' || !$parameter->field_type) {
                return;
            }
            $p_min = $parameter->data()->min('id');
            $p_max = $parameter->data()->max('id');
            foreach ($products as $product) {
                ProductValue::create([
                    'parameter_id' => $parameter->id,
                    'product_id' => $product->id,
                    'integer' => rand($p_min, $p_max),
                    'data_type' => 'fkey'
                ]);
            }
        }
    }

    /** @test */
    public function can_get_filter_array()
    {
        $this->parameterGenerate();

        // $filterArray = (new EloquentCategoryRepository(new Category()))->getFilterArray(Category::find(1), Category::descendantsAndSelf(1));
    }
}
