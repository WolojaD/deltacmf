<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ProductVersionTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'parameters' => $this->parameters,
            'discount' => $this->discount,
            'article' => $this->article,
            'status' => $this->status,
            'discount_price' => $this->discount_price,
            'quantity' => $this->quantity,
            'price' => $this->price
        ];

        // foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
        //     $pageData[$locale] = [];

        //     foreach ($this->translatedAttributes as $translatedAttribute) {
        //         $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
        //     }
        // }

        return $data;
    }
}
