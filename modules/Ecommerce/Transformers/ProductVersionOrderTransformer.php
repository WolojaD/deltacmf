<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ProductVersionOrderTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        preg_match('/\d+$/', $request->path(), $id);

        $data = $this->getAttributes();
        $data['parameters'] = isset($data['parameters']) && $data['parameters'] ? $data['parameters'] : $this->parameter;
        $data['title'] = $this->product_title;
        $data['product_id'] = $this->product ? $this->product->id : null;
        $data['storage_quantity'] = $this->product_version->quantity ?? 0;
        $data['quantity'] = $this->quantity;
        $data['discount_price'] = substract_percents($data['price'], $data['discount']);
        $data['discount_sum'] = $data['discount_price'] * $data['quantity'];

        return $data;
    }
}
