<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullOrderTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'comment' => $this->comment,
            'delivery' => $this->delivery,
            'delivery_price' => $this->delivery_price,
            'payment' => $this->payment,
            'is_quick' => $this->is_quick,
            'user_id' => $this->user_id,
            'title' => $this->title,
            'total' => $this->total,
            'situation' => $this->situation,
        ];
    }
}
