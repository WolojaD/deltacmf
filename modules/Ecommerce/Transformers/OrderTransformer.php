<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class OrderTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $items = $this->pivot->reduce(function ($carry, $product) {
            return $carry + $product->quantity;
        }, 0);

        $result = [
            'id' => $this->id,
            'status' => $this->status ?? '',
            'children_count' => $items,
            'payment' => $this->payment,
            'total' => $this->total,
            'situation_title' => $this->situation_title,
        ];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['is_quick'] = $this->is_quick ? 'быстрый заказ' : '';
        $result['created_at'] = $this->created_at->toRfc2822String();

        return $result;
    }
}
