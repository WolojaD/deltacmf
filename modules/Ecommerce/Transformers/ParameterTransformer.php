<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ParameterTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $datas = $this->data ?? collect([]);
        $result = ['id' => $this->id, 'status' => $this->status ?? '', 'children_count' => $datas->count() ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['required'] = $this->required;
        $result['in_filter'] = $this->in_filter;
        $result['data_type'] = [
            'table_title' => $this->field_type,
            'original_title' => $this->field_type
        ];

        return $result;
    }
}
