<?php

namespace Modules\Ecommerce\Transformers;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class SaleTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = [
            'id' => $this->id,
            'status' => $this->status ?? '',
        ];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['date_from'] = $this->date_from ? $this->date_from->toRfc2822String() : '';
        $result['date_to'] = $this->date_to ? $this->date_to->toRfc2822String() : '';
        $result['children_count'] = $this->products->count();

        if (Carbon::now()->gt($this->date_from)) {
            $result['time_left'] = $this->date_to ? $this->date_to->diffForHumans() : '';
        }

        if (!$this->date_to && Carbon::now()->gt($this->date_from)) {
            $result['time_left'] = trans('ecommerce::messages.sale.active');
        } elseif ($this->date_to && Carbon::now()->gt($this->date_to)) {
            $result['time_left'] = trans('ecommerce::messages.sale.ended');
        }

        return $result;
    }
}
