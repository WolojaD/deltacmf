<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class StockTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['store_title'] = $this->store ? $this->store->defaultTranslate()->title : '';
        $result['product_title'] = $this->version ? $this->version->product->defaultTranslate()->title : '';

        return $result;
    }
}
