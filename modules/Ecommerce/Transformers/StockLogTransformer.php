<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class StockLogTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $first_name = $this->user->first_name ?? '';
        $last_name = $this->user->last_name ?? '';

        $result['product_title'] = $this->stock->version->product->title ?? '';
        $result['product_article'] = $this->stock->article ;
        $result['store_title'] = $this->store ? $this->store->defaultTranslate()->title : '';
        $result['user_title'] = $first_name . ' ' . $last_name;
        $result['count_before'] = !is_null($this->count_before) ? $this->count_before : 'товар отсутствовал';
        $result['created_at'] = $this->created_at ? $this->created_at->toRfc2822String() : '';

        return $result;
    }
}
