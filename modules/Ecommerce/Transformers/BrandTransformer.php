<?php

namespace Modules\Ecommerce\Transformers;

use Modules\Seo\Entities\Structure;
use Illuminate\Http\Resources\Json\Resource;

class BrandTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }
        $result['children_count'] = $this->products->count();
        $result['is_top'] = $this->is_top;
        $result['path'] = Structure::where('page_template', 'brands')->select('url')->first()->url . '/' . $this->slug;

        return $result;
    }
}
