<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullSaleTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'slug' => $this->slug,
            'mobile_image' => $this->mobile_image,
            'desktop_image' => $this->desktop_image,
            'image' => $this->image,
            'date_from' => $this->date_from ? $this->date_from->format('Y-m-d H:i:s') : '',
            'date_to' => $this->date_to ? $this->date_to->format('Y-m-d H:i:s') : '',
            'discount' => $this->discount,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
