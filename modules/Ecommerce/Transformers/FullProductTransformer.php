<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullProductTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'slug' => $this->slug,
            'status' => $this->status,
            'article' => $this->article,
            'is_new' => $this->is_new,
            'new_till' => $this->new_till,
            'is_top' => $this->is_top,
            'discount' => $this->discount,
            'category_id' => $this->category_id,
            'brand_id' => $this->brand_id,
            'gallery_id' => $this->gallery_id,
            'canShow' => ($this->status > 0) && ($this->versions()->where('status', '>', 0)->count() > 0)
        ];

        $versions = $this->versions()->with('parameterData')->get();

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;

                foreach ($versions as $id => $version) {
                    // $versions[$id][$locale] = ['title' => $version->translateOrNew($locale)->title];
                }
            }
        }

        foreach ($this->values as $value) {
            $field_name = $value->parameter->field_type;

            if ($value->parameter->is_translatable) {
                foreach (\app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                    $data[$locale][$value->parameter->slug] = $value->translateOrNew($locale)->$field_name;
                }
            } elseif ($value->parameter->key_type === 'multifkey') {
                $data[$value->parameter->slug] = $value->parameter_datas->pluck('id');
            } else {
                $data[$value->parameter->slug] = $value->$field_name;
            }
        }

        $data['versions'] = ProductVersionTransformer::collection($versions)->toArray(request());

        foreach ($versions as $id => $version) {
            $data['versions'][$id]['parameters'] = [];
            foreach ($version->parameterData as $parameter_data) {
                $data['versions'][$id]['parameters'][$parameter_data->parameter->slug] = $parameter_data->id;
            }
        }

        foreach ($data['versions'] as $id => $version) {
            $data['versions'][$id]['status'] = $version['status'] > 0;
        }

        return $data;
    }
}
