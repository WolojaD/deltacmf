<?php

namespace Modules\Ecommerce\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CategoryTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }
        $result['children_count'] = $this->children_count;
        $result['all_products_count'] = $this->all_products_count;
        $result['path'] = $this->path;

        return $result;
    }
}
