<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Parameter extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_parameters';

    public $translatedAttributes = [];

    public $appends = ['type', 'categories_titles'];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/parameter.json';

    public $data_types = [
        1 => [
            'data' => 'string',
            'field' => 'string',
            'form' => 'text',
            'name' => ['en' => 'String', 'ru' => 'Строка'],
        ],
        [
            'data' => 'translatable_string',
            'field' => 'translatable_string',
            'form' => 'text',
            'name' => ['en' => 'Translatable string', 'ru' => 'Переводимая строка'],
        ],
        [
            'data' => 'bool',
            'field' => 'integer',
            'form' => 'checkbox',
            'name' => ['en' => 'Checkbox', 'ru' => 'Галочка'],
        ],
        [
            'data' => 'fkey',
            'field' => 'integer',
            'form' => 'select',
            'name' => ['en' => 'External key', 'ru' => 'Выбор значения'],
        ],
        [
            'data' => 'multifkey',
            'field' => 'pivot',
            'form' => 'multi-select',
            'name' => ['en' => 'External multi-key', 'ru' => 'Множественный выбор значения'],
        ],
        [
            'data' => 'divider',
            'name' => ['en' => 'Divider', 'ru' => 'Разделитель'],
        ],
        [
            'data' => 'html',
            'field' => 'text',
            'form' => 'ckeditor',
            'name' => ['en' => 'HTML', 'ru' => 'HTML'],
        ],
        [
            'data' => 'translatable_html',
            'field' => 'translatable_text',
            'form' => 'ckeditor',
            'name' => ['en' => 'Translatable HTML', 'ru' => 'Переводимый HTML'],
        ],
        [
            'data' => 'text',
            'field' => 'text',
            'form' => 'textarea',
            'name' => ['en' => 'Text', 'ru' => 'Текст'],
        ],
        [
            'data' => 'translatable_text',
            'field' => 'translatable_text',
            'form' => 'textarea',
            'name' => ['en' => 'Translatable Text', 'ru' => 'Переводимый Текст'],
        ],
        [
            'data' => 'number',
            'field' => 'decimal',
            'form' => 'decimal',
            'name' => ['en' => 'Number', 'ru' => 'Число'],
        ],
        [
            'data' => 'image',
            'field' => 'string',
            'form' => 'upload-image',
            'name' => ['en' => 'Image', 'ru' => 'Изображение'],
        ],
        [
            'data' => 'datetime',
            'field' => 'datetime',
            'form' => 'datetime',
            'name' => ['en' => 'Date', 'ru' => 'Дата'],
        ]
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getField($key)
    {
        $id = $this->getFieldId($key);

        return $id ? $this->data_types[$id]['field'] : '';
    }

    public function getFieldId($key)
    {
        foreach ($this->data_types as $id => $type) {
            if ($type['data'] == $key) {
                return $id;
            }
        }

        return false;
    }

    public function infoTypeIds()
    {
        return [4, 5];
    }

    public function randomizeData()
    {
        $faker = \Faker\Factory::create();

        switch ($this->field_type) {
            case 'string':
                return $faker->realText(10);
            case 'text':
                return $faker->realText(100);
            case 'translatable_string':
                return [
                    'uk' => [
                        'translatable_string' => $faker->realText(10)
                    ]
                ];
            case 'translatable_text':
                return [
                    'uk' => [
                        'translatable_text' => $faker->realText(100)
                    ]
                ];
            case 'integer':
            case 'decimal':
                return $this->data_type == 4 ? rand(1, 12) : 1;
            case 'datetime':
                return $faker->datetime;
            default:
                return $faker->realText(10);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function data()
    {
        return $this->hasMany(ParameterData::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'ecommerce_category_parameters', 'parameter_id', 'category_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getTypeAttribute()
    {
        return $this->data_types[$this->data_type]['name'][locale()];
    }

    public function getKeyTypeAttribute()
    {
        return $this->data_types[$this->data_type]['data'] ?? '';
    }

    public function getFieldTypeAttribute()
    {
        return $this->data_types[$this->data_type]['field'] ?? '';
    }

    public function getIsInfoTypeAttribute()
    {
        return in_array($this->data_type, $this->infoTypeIds());
    }

    public function getCategoriesTitlesAttribute()
    {
        return $this->categories->implode('title', ', ');
    }

    public function getIsTranslatableAttribute()
    {
        return strpos($this->key_type, 'translatable_') === 0;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
