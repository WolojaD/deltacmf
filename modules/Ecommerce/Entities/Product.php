<?php

namespace Modules\Ecommerce\Entities;

use Carbon\Carbon;
use Laravel\Scout\Searchable;
use Modules\Page\Entities\Page;
use Modules\Core\Eloquent\Model;
use Modules\Seo\Helpers\Templates;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Gallery\Entities\Gallery;
use Modules\Core\Scopes\DraftableScope;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;
use Modules\Seo\Repositories\Eloquent\EloquentStructureRepository;

class Product extends Model implements SeoSyncInterface
{
    use Translatable,
        SeoSyncTrait,
        Searchable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $groupOrder = 'category_id';

    protected $table = 'ecommerce_products';

    public $translatedAttributes = [];

    protected $appends = ['price'];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/product.json';

    public $page_template = 'product';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return void
     */
    public static function boot()
    {
        static::creating(function ($obj) {
            if (!isset($obj->gallery_id) || !$obj->gallery_id) {
                foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                    $gallery[$key]['title'] = $obj['translations']->filter(function ($translation) use ($key) {
                        return $translation->locale == $key;
                    })->first()->title ?? '';
                }

                $gallery['type'] = 'product';
                $gallery['status'] = 1;
                $gallery['is_temp'] = 0;

                $obj->gallery_id = Gallery::create($gallery)->id;
            }
        });

        static::deleting(function ($obj) {
            $obj->gallery()->delete();
        });

        parent::boot();
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['translations']);

        if ($this->translatedAttributes) {
            foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                foreach ($this->translatedAttributes as $translatedAttribute) {
                    $array[$translatedAttribute . '_' . $locale] = $this->translateOrNew($locale)->$translatedAttribute;
                }
            }
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    public function getSuggestionItems()
    {
        $limit = settings('ecommerce::suggested_count') ? settings('ecommerce::suggested_count') : 12;
        $suggested = $this->suggested()->inRandomOrder()->limit($limit)->get();

        if ($suggested->count()) {
            return $suggested;
        }

        if (settings('ecommerce::suggested_percents_range') && is_numeric(settings('ecommerce::suggested_percents_range'))) {
            $range = (int) settings('ecommerce::suggested_percents_range');
        } else {
            $range = 20;
        }

        return self::whereHas('versions', function ($query) use ($range) {
            $query->where('discount_price', '<=', floor(substract_percents($this->discounted_price, -$range) * 100))
                  ->where('discount_price', '>=', floor(substract_percents($this->discounted_price, $range) * 100));
        })
            ->where('category_id', $this->category_id)
            ->where('id', '!=', $this->id)
            ->inRandomOrder()
            ->limit($limit)
            ->get();
    }

    public function pushToRecent()
    {
        session_unique_push('products.recently_viewed', $this->getKey());

        return $this;
    }

    public function pushToCompare()
    {
        session_unique_push('products.to_compare', $this->getKey());

        return $this;
    }

    public function deleteFromCompare()
    {
        session_unique_substract('products.to_compare', $this->getKey());

        return $this;
    }

    public function parseFilterRow($filter_items, $id = false)
    {
        $filter_list = [
            'min_price' => false,
            'max_price' => false,
        ];

        foreach ($filter_items as $item) {
            if (is_numeric($item)) {
                $filter_list['ids'][] = $item;
            } elseif (strpos($item, 'p') === 0) {
                $price_counter = isset($price_counter) ? $price_counter + 1 : 1;
                $price = (int)str_replace('p', '', $item);

                if ($filter_list['min_price'] === false) {
                    $filter_list['min_price'] = $price;
                    request()->merge(['min_price' => $price]);
                } elseif ($filter_list['max_price'] === false) {
                    $filter_list['max_price'] = $price;
                    request()->merge(['max_price' => $price]);
                } else {
                    abort(404);
                }
            } elseif (strpos($item, 'c') === 0) {
                $filter_list['categories'][] = (int)str_replace('c', '', $item);
            } elseif (strpos($item, 'b') === 0) {
                $filter_list['brands'][] = (int)str_replace('b', '', $item);
            }
        }

        if (isset($price_counter) && $price_counter !== 2) {
            abort(404);
        }

        if ($filter_list['categories'] ?? false) {
            // not used mb
            $filter_list['categories'] = Category::whereIn('id', $filter_list['categories'])->get(['id', 'parent_id'])->map(function ($item) {
                return $item->getAllIds();
            })->flatten();
        }

        if ($filter_list['min_price'] && $filter_list['min_price'] > $filter_list['max_price']) {
            abort(404);
        }

        if ($filter_list['ids'] ?? '') {
            $parameter_datas = ParameterData::whereIn('id', $filter_list['ids'])
            ->when($id, function ($query) use ($id) {
                $query->where('parameter_id', '!=', $id);
            })
            ->get(['parameter_id', 'id']);

            if (!$id && $parameter_datas->count() != count($filter_list['ids'])) {
                abort(404);
            }

            $filter_list['ids'] = $parameter_datas->groupBy('parameter_id');
        }

        return $filter_list;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function values()
    {
        return $this->hasMany(ProductValue::class)->with('translations');
    }

    public function versions()
    {
        return $this->hasMany(ProductVersion::class);
    }

    // public function pivot()
    // {
    //     return $this->hasMany(OrderProduct::class, 'product_id')->with('version');
    // }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function sales()
    {
        return $this->belongsToMany(Sale::class, 'ecommerce_product_sales')->using(ProductSale::class);
    }

    public function suggested()
    {
        return $this->belongsToMany(Product::class, 'ecommerce_product_suggestions', 'product_id', 'suggested_id');
    }

    public function salePivot()
    {
        return $this->hasMany(ProductSale::class)->whereHas('sale', function ($query) {
            $query->active();
        });
    }

    public function structure()
    {
        return $this->hasOne(Structure::class, 'object_id')->where('model', self::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeNewItems($query)
    {
        return $query->where('is_new', 1)->where(
            function ($query) {
                $query->where('created_at', '>', Carbon::parse('-14 days')->format('Y-m-d H:i:s'))
                    ->orWhere('new_till', '>', Carbon::now()->format('Y-m-d H:i:s'));
            }
        );
    }

    public function scopeTopItems($query)
    {
        return $query->where('is_top', 1);
    }

    public function scopeDiscountedItems($query)
    {
        $product_versions = (new ProductVersion)->getTable();
        $products = (new Product)->getTable();

        $query->join($product_versions, $product_versions . '.product_id', '=', $products . '.id')
            ->whereRaw("`{$product_versions}`.`price` > `{$product_versions}`.`discount_price`")
            ->where($product_versions . '.quantity', '>', 0);
    }

    public function scopeNotIsOut($query)
    {
        return $query->where('is_out', 0);
    }

    public function scopeFrontSearch($query, $search)
    {
        if (!$search) {
            return $query;
        }

        return $query->where($this->getTable() . '.id', $search)
            ->orWhereTranslationLike('title', "%{$search}%")
            ->orWhereHas('category', function ($query) use ($search) {
                $query->whereTranslationLike('title', "%{$search}%");
            })
            ->orWhereHas('versions', function ($query) use ($search) {
                $query->where('article', 'like', "%{$search}%");
            })
            ->orWhereHas('brand', function ($query) use ($search) {
                $query->whereTranslationLike('title', "%{$search}%");
            });
    }

    /**
     * Gets current request filter and filter products
     */
    public function scopeFrontFilter($query, $id = false)
    {
        $filter = request()->get('filterParsed', false);

        if (is_array($filter)) {
            return $query->indexesFilter($this->parseFilterRow($filter, $id), $id);
        } elseif ($filter) {
            preg_match('/^([-a-z0-9]*)_([-a-z0-9]*)$/', $filter, $filtered);
        }

        if ($filtered[0] ?? false) {
            return $query->slugifiedFilter($filtered, $id);
        }

        return $query;
    }

    /**
     * subquery scope works with parameter_data
     */
    public function scopeFilterByParameterData($query)
    {
        $query->whereHas('values', function ($query) {
            $query->whereRaw('`integer` = ' . with(new ParameterData)->getTable() . '.id')
            ->join('ecommerce_product_value_parameter_datas', 'product_value_id', '=', with(new ProductValue)->getTable() . '.id')
            ->orWhereRaw('ecommerce_product_value_parameter_datas.parameter_data_id = ' . with(new ParameterData)->getTable() . '.id');
        });
    }

    public function scopeSlugifiedFilter($query, $filtered, $id)
    {
        $query->when($filtered[1] == 'brand', function ($query) use ($filtered, $id) {
            $query->whereHas('brand', function ($query) use ($filtered, $id) {
                $query->when(!$id, function ($query) use ($filtered) {
                    $query->where('slug', $filtered[2]);
                });
            });
        }, function ($query) use ($filtered, $id) {
            $query->whereHas('values', function ($query) use ($filtered, $id) {
                $query->whereHas('parameter', function ($query) use ($filtered, $id) {
                    $query->where('slug', $filtered[1])
                            ->when($id, function ($query) use ($id) {
                                $query->orWhere('id', $id);
                            });
                })
                ->where(function ($query) use ($filtered, $id) {
                    $query->whereIn('integer', function ($query) use ($filtered, $id) {
                        $query->select('id')
                            ->from((new ParameterData)->getTable())
                            ->where('slug', $filtered[2])
                            ->when($id, function ($query) use ($id) {
                                $query->orWhere('parameter_id', $id);
                            });
                    })
                    ->orWhereHas('parameter_datas', function ($query) use ($filtered, $id) {
                        $query->where('slug', $filtered[2])
                        ->when($id, function ($query) use ($id) {
                            $query->orWhere('parameter_id', $id);
                        });
                    });
                });
            });
        });
    }

    public function scopeIndexesFilter($query, $filter_list, $id)
    {
        $query->when($filter_list['ids'] ?? false, function ($query) use ($filter_list) {
            foreach ($filter_list['ids'] as $item) {
                $query->where(function ($query) use ($item) {
                    $query->whereHas('values', function ($query) use ($item) {
                        $query->whereIn('integer', $item->pluck('id'))
                        ->orWhereHas('parameter_datas', function ($query) use ($item) {
                            $query->whereIn('id', $item->pluck('id'));
                        });
                    })
                    ->orWhereHas('versions', function ($query) use ($item) {
                        $query->whereHas('parameterData', function ($query) use ($item) {
                            $query->whereIn('id', $item->pluck('id'));
                        });
                    });
                });
            }
        })
        ->whereHas('versions', function ($query) use ($filter_list) {
            $query->when($filter_list['min_price'] !== false, function ($query) use ($filter_list) {
                $query->whereBetween('discount_price', [$filter_list['min_price'] * 100, $filter_list['max_price'] * 100]);
            });
        })
        ->when($filter_list['brands'] ?? false, function ($query) use ($filter_list, $id) {
            $query->when($id !== 'brand', function ($query) use ($filter_list) {
                $query->whereIn('brand_id', $filter_list['brands']);
            });
        })
        ->when($filter_list['categories'] ?? false, function ($query) use ($filter_list) {
            $query->whereIn('category_id', $filter_list['categories']);
        });
        // $query->toSqlWithBindings();
    }

    public function scopeTemplateFilter($query, $template)
    {
        switch ($template) {
            case 'top_products':
                return $query->topItems();
            case 'new_products':
                return $query->newItems();
            case 'discounted_products':
                return $query->discountedItems();
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getCategoryTitleAttribute()
    {
        if ($this->category_id) {
            return $this->category->title ?? '';
        }

        return '';
    }

    public function getBrandTitleAttribute()
    {
        if ($this->brand_id) {
            return $this->brand->title ?? '';
        }

        return '';
    }

    public function getGalleryTitleAttribute()
    {
        if ($this->gallery_id) {
            return $this->gallery->title ?? '';
        }

        return '';
    }

    public function getPriceAttribute()
    {
        $versions = $this->versions;

        if ($versions->count() > 0) {
            $min = $versions->min('price');
            $max = $versions->max('price');

            return $min != $max ? $min . '-' . $max : $min;
        }

        return 0;
    }

    public function getDiscountedPriceAttribute()
    {
        return $this->versions->min('discount_price');
    }

    public function getMinimumPriceAttribute()
    {
        return $this->versions->min('price');
    }

    public function getPathAttribute()
    {
        return "/{$this->slug}";
    }

    public function getDiscountAttribute()
    {
        return floatval($this->salePivot->last(
            function ($item) {
                return $item->sale_id;
            }
        )->discount ?? ($this->sales()->active()->first()->discount ?? 0));
    }

    public function getCharacteristicsListAttribute()
    {
        return $this->values->mapWithKeys(function ($item) {
            return $item->value;
        });
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */
    public function parentStructure()
    {
        return Structure::where('object_id', $this->category_id)
            ->whereIn('model', [Category::class, Page::class])
            ->first();
    }

    public function createSeoStructure()
    {
        $templates = new Templates();

        $template = $templates->getTemplate('product');

        $structure = Structure::where('object_id', $this->category_id)
            ->withoutGlobalScope(DraftableScope::class)
            ->where('model', Category::class)
            ->first();

        if (!$structure) {
            return false;
        }

        $data = [
            'model' => self::class,
            'object_id' => $this->id,
            'parent_id' => $structure->id ?? 0,
            'slug' => $this->slug,
            'status' => $this->status ?? 1,
            'page_template' => 'product',
            'url' => '/' . $this->slug,
            'depth' => $structure->depth + 1,
            'weight' => $template['weight'],
            'frequency' => $template['frequency']
        ];

        $translations = $this->translations->mapWithKeys(function ($translation) {
            return [$translation->locale => [
                'title' => $translation->title
            ]];
        })->toArray();

        return (new EloquentStructureRepository(new Structure))->create(array_merge($translations, $data));
    }

    public function updateSeoStructure()
    {
        $templates = new Templates();

        $template = $templates->getTemplate('product');

        $structure = Structure::where('object_id', $this->category_id)
            ->withoutGlobalScope(DraftableScope::class)
            ->where('model', Category::class)
            ->first();

        if (!$structure) {
            return false;
        }

        $url = !$structure ? '/' . $this->slug : $structure->url . '/' . $this->slug;

        $data = [
            'parent_id' => $structure->id ?? 0,
            'slug' => $this->slug,
            'status' => $this->status ?? 1,
            'depth' => $structure->depth + 1,
            'page_template' => 'product',
            'url' => '/' . $this->slug,
            'weight' => $template['weight'],
            'frequency' => $template['frequency']
        ];

        $translations = $this->translations->mapWithKeys(function ($translation) {
            return [$translation->locale => [
                'title' => $translation->title
            ]];
        })->toArray();

        $current_structure = Structure::where('object_id', $this->id)
                ->withoutGlobalScope(DraftableScope::class)
                ->where('model', self::class)
                ->first();

        $current_structure->update(array_merge($translations, $data));
    }
}
