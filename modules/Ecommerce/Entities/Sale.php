<?php

namespace Modules\Ecommerce\Entities;

use Carbon\Carbon;
use Modules\Core\Eloquent\Model;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;

class Sale extends Model implements SeoSyncInterface
{
    use Translatable,
        SeoSyncTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_sales';

    public $translatedAttributes = [];

    public $casts = [
        'date_from' => 'datetime',
        'date_to' => 'datetime',
    ];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/sale.json';

    public $page_template = 'sale';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function products()
    {
        return $this->belongsToMany(Product::class, 'ecommerce_product_sales', 'sale_id', 'product_id');
    }

    public function productsPivot()
    {
        return $this->hasMany(ProductSale::class)->whereHas('sale', function ($query) {
            $query->active();
        });
    }

    public function structure()
    {
        return $this->hasOne(Structure::class, 'object_id')->where('page_template', 'sale')->with('translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeActive($query)
    {
        return $query->where('date_from', '<=', Carbon::now()->format('Y-m-d H:i:s'))
        ->where('date_to', '>', Carbon::now()->format('Y-m-d H:i:s'));
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */

    public function parentStructure()
    {
        return Structure::whereNotNull('page_id')
            ->where('page_template', 'sales')
            ->first();
    }
}
