<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\TranslationModel;

class ParameterTranslation extends TranslationModel
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_parameter_translations';

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/parameter.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return void
     */
    public static function boot()
    {
        static::creating(function ($obj) {
            if (!($obj->title ?? false)) {
                return false;
            }
        });

        static::updating(function ($obj) {
            if (!($obj->title ?? false)) {
                return false;
            }
        });

        parent::boot();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
