<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\Model;

// use Modules\Core\Internationalisation\Translatable;

class ProductVersion extends Model
{
    // use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_product_versions';

    protected $touches = ['parameterData', 'orders'];

    public $translatedAttributes = [];

    protected $appends = ['category_title', 'parameters_name'];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/product_version.json';

    protected $casts = ['discount' => 'float'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function parameterData()
    {
        return $this->belongsToMany(ParameterData::class, 'ecommerce_product_version_parameter_data', 'product_version_id', 'parameter_data_id', 'id', 'id', 'parameterData')->withTimestamps();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'ecommerce_order_product_versions')->using(OrderProduct::class);
    }

    public function pivot()
    {
        return $this->hasMany(OrderProduct::class)->with('product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeDiscounted($query)
    {
        return $query->whereHas('product', function ($query) {
            $query->where('status', 1);
        })->whereRaw('price > discount_price')
          ->where('quantity', '>', 0);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getCategoryTitleAttribute()
    {
        return $this->product->category->title ?? '';
    }

    public function getParametersNameAttribute()
    {
        $parameters = $this->parameterData->map(function ($item) {
            return $item->parameter->title . ': ' . $item->title;
        })->implode(', ');

        return $parameters ? 'Артикул: ' . $this->article . ' | ' . $parameters : $this->article;
    }

    public function getPriceAttribute()
    {
        return $this->attributes['price'] / 100;
    }

    public function getDiscountPriceAttribute()
    {
        return substract_percents($this->attributes['price'] / 100, $this->product_discount);
    }

    public function getProductDiscountAttribute()
    {
        return $this->discount ? $this->discount : $this->product->discount;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setPriceAttribute($value)
    {
        return $this->attributes['price'] = round($value * 100, 0);
    }

    public function setDiscountPriceAttribute($value)
    {
        return $this->attributes['discount_price'] = round($value * 100, 0);
    }

    // public function setStatusAttribute($value)
    // {
    //     $this->attributes['status'] = ($value ?? 0) ? 1 : 0;
    // }
}
