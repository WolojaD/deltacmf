<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;

class Brand extends Model implements SeoSyncInterface
{
    use Translatable, SeoSyncTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_brands';

    public $translatedAttributes = [
    ];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/brand.json';

    public $page_template = 'brand';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getFullFilterPath($filters)
    {
        return str_start(str_finish(request()->noFilterPath, '/'), '/') . $this->filterPath($filters);
    }

    public function filterPath($filters)
    {
        $filter = request()->get('filterParsed', false);

        if (is_array($filter)) {
            if (in_array('b' . $this->id, $filter)) {
                array_drop($filter, 'b' . $this->id);

                if (count($filter) == 1) {
                    $filter = array_pop($filter);

                    if (starts_with($filter, 'b')) {
                        $filter = 'brand_' . $filters['brands']->where('id', str_replace('b', '', $filter))->first()->slug;
                    } else {
                        $parameter_data = $filters['parameter_datas']->where('id', str_replace('b', '', $filter))->first();
                        $filter = $parameter_data->parameter_slug . '_' . $parameter_data->slug;
                    }
                }
            } else {
                $filter[] = 'b' . $this->id;
            }
        } elseif ($filter) {
            if ($filters['filterParameter'] ?? false) {
                $filter = [$filters['filterParameter'], 'b' . $this->id];
            } elseif (($filters['filterBrand'] ?? false) && $filters['filterBrand'] == $this->id) {
                return '';
            } elseif (starts_with($filter, 'brand_') && $filters['filterBrand'] ?? false) {
                $filter = $filters['filterBrand'] == $this->slug ? '' : ['b' . $filters['filterBrand'], 'b' . $this->id];
            }
        } else {
            return 'brand_' . $this->slug;
        }

        if (is_array($filter)) {
            sort($filter, SORT_STRING);

            return implode('_', $filter);
        }

        return $filter;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeWithCountProducts($query, $categories, $id = false)
    {
        $query->addSubCount(Product::whereIn('id', function ($query) use ($categories) {
            return $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->whereIn('category_id', $categories);
        })
        ->whereRaw('brand_id = ' . with(new Brand)->getTable() . '.id')
        ->frontFilter($id ? 'brand' : false), 'count');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getPathAttribute()
    {
        return "/brands/{$this->slug}";
    }

    public function getDisabledAttribute()
    {
        return ($this->count ?? 0) > 0 ? '' : 'disabled';
    }

    public function getCheckedAttribute()
    {
        $filter = request()->get('filterParsed', false);

        if (is_array($filter)) {
            return in_array('b' . $this->id, $filter) ? 'checked' : '';
        } elseif (is_string($filter)) {
            return $this->slug == str_replace('brand_', '', $filter) ? 'checked' : '';
        }

        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */
    public function parentStructure()
    {
        return Structure::whereNotNull('page_id')
            ->where('page_template', 'brands')
            ->first();
    }
}
