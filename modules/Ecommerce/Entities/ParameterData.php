<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class ParameterData extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_parameter_datas';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/parameter_data.json';

    public $groupOrder = 'parameter_id';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getFullFilterPath($filters)
    {
        return str_start(str_finish(request()->noFilterPath, '/'), '/') . $this->filterPath($filters);
    }

    public function filterPath($filters)
    {
        $filter = request()->get('filterParsed', false);

        if (is_array($filter)) {
            if (in_array($this->id, $filter)) {
                array_drop($filter, $this->id);

                if (count($filter) == 1) {
                    $filter = array_pop($filter);

                    if (starts_with($filter, 'b')) {
                        $filter = 'brand_' . $filters['brands']->where('id', str_replace('b', '', $filter))->first()->slug;
                    } else {
                        $parameter_data = $filters['parameter_datas']->where('id', $filter)->first();
                        $filter = ($parameter_data->parameter_slug ?? $parameter_data->parameter->slug) . '_' . $parameter_data->slug;
                    }
                }
            } else {
                $filter[] = $this->id;
            }
        } elseif ($filter) {
            if (starts_with($filter, 'brand_') && $filters['filterBrand'] ?? false) {
                $filter = $filters['filterBrand'] == $this->slug ? '' : ['b' . $filters['filterBrand'], $this->id];
            } elseif (($filters['filterParameter'] ?? false) && $filters['filterParameter'] == $this->id) {
                return '';
            } elseif (($filters['filterParameter'] ?? false)) {
                $filter = [$filters['filterParameter'], $this->id];
            }
        } else {
            $parameter_slug = $this->parameter_slug ?? optional($this->parameter)->slug;

            return $parameter_slug . '_' . $this->slug;
        }

        if (is_array($filter)) {
            $filter = array_unique($filter);
            sort($filter, SORT_STRING);

            return implode('_', $filter);
        }

        return $filter;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }

    public function product_value()
    {
        return $this->hasMany(ProductValue::class, 'integer')->where('data_type', 'fkey');
    }

    public function product_versions()
    {
        return $this->belongsToMany(ProductVersion::class, 'ecommerce_product_version_parameter_data', 'parameter_data_id', 'product_version_id', 'id', 'id')->withTimestamps();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeWithProductsByCategoryIds($query, $categories)
    {
        $query->whereIn($this->table . '.id', function ($query) use ($categories) {
            return $query->select('integer')
                ->from(with(new ProductValue)->getTable())
                ->whereIn('product_id', function ($query) use ($categories) {
                    return $query->select('id')
                        ->from(with(new Product)->getTable())
                        ->whereIn('category_id', $categories);
                })
                ->where('data_type', 'fkey');
        })
        ->orWhereHas('product_versions', function ($query) use ($categories) {
            return $query->whereIn(with(new ProductVersion)->getTable() . '.product_id', function ($query) use ($categories) {
                return $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->whereIn('category_id', $categories);
            });
        })
        ->orWhereHas('product_value', function ($query) use ($categories) {
            return $query->whereIn('product_id', function ($query) use ($categories) {
                return $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->whereIn('category_id', $categories);
            });
        });
    }

    public function scopeWithCountProducts($query, $categories, $id = false)
    {
        $query->addSubCount(Product::whereIn('id', function ($query) use ($categories) {
            return $query->select('id')
                    ->from(with(new Product)->getTable())
                    ->whereIn('category_id', $categories);
        })
        ->whereHas('values', function ($query) {
            $query->whereIn('data_type', ['multifkey', 'fkey'])
            ->whereRaw('`integer` = ' . with(new ParameterData)->getTable() . '.id')
            ->orWhereHas('parameter_datas');
        })
        ->orWhereHas('versions', function ($query) {
            $query->whereHas('parameterData', function ($query) {
                $query->whereRaw('id =' . with(new ParameterData)->getTable() . '.id');
            });
        })
        ->frontFilter($id), 'count');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getParameterTitleAttribute()
    {
        return $this->parameter->title;
    }

    public function getCheckedAttribute()
    {
        return request()->get('filterParsed', false) && is_array(request()->filterParsed) ?
            (in_array($this->id, request()->filterParsed) ? 'checked' : '') :
            '';
    }

    public function getDisabledAttribute()
    {
        return ($this->count ?? 0) > 0 ? '' : 'disabled';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
