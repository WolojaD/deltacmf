<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Users\Entities\Sentinel\User;

class Order extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_orders';

    public $translatedAttributes = [
    ];

    public $appends = [
        'total'
    ];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/order.json';

    protected $situations = [
        1 => [
            'name' => 'new',
            'label' => ['en' => 'New', 'ru' => 'Новый'],
        ],
        [
            'name' => 'accepted',
            'label' => ['en' => 'Accepted', 'ru' => 'Принят'],
        ],
        [
            'name' => 'refuse',
            'label' => ['en' => 'Refuse', 'ru' => 'Отказ'],
        ],
        [
            'name' => 'done',
            'label' => ['en' => 'Done', 'ru' => 'Выполнен'],
        ],
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function situations()
    {
        return collect($this->situations)->map(function ($situation) {
            return $situation['label'][locale()];
        });
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'ecommerce_order_product_versions')->using(OrderProduct::class);
    }

    public function pivot()
    {
        return $this->hasMany(OrderProduct::class)->with('product');
    }

    public function product_versions()
    {
        return $this->belongsToMany(ProductVersion::class, 'ecommerce_order_product_versions')->using(OrderProduct::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getUserTitleAttribute()
    {
        return $this->user->title ?? '';
    }

    public function getTotalAttribute()
    {
        return $this->pivot->reduce(function ($carry, $item) {
            return $carry + ($item->discount_price ?? 0) * $item->quantity;
        }, 0) + ($this->delivery_price ?? 0);
    }

    public function getSituationTitleAttribute()
    {
        return $this->situations[$this->situation]['label'][locale()];
    }

    /**
     * Get all headers user can view in this template
     *
     * @param $columns
     *
     * @return mixed
     */
    public function getHeadersOfCollectionFor($template = 'default')
    {
        if ($template == 'default') {
            return [
                'total' => 'Сумма',
                'title' => 'ФИО',
                'email' => 'E-mail',
                'phone' => 'Телефон',
                'address' => 'Адресс',
                'comment' => 'Комментарий',
                'delivery' => 'Доставка',
                'payment' => 'Оплата',
                'situation_title' => 'Статус',
                'is_quick' => 'Быстрый заказ',
                'created_at' => 'Дата',
                'id' => 'ID',
            ];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
