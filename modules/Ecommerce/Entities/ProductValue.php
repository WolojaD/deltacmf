<?php

namespace Modules\Ecommerce\Entities;

use Modules\Core\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Modules\Core\Internationalisation\Translatable;

class ProductValue extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_product_values';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/product_value.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return type
     */
    public static function boot()
    {
        self::deleting(function ($entity) {
            $folders = Image::select('title', 'id')->get();

            foreach ($folders as $folder) {
                $pre_path = 'storage/' . $folder->rootPath . '/' . $folder->title . '/';

                if ($entity->string && File::exists(public_path($pre_path . ltrim($entity->string, '/origin')))) {
                    File::delete(public_path($pre_path . ltrim($entity->string, '/origin')));
                }
            }
        });

        parent::boot();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }

    public function parameter_datas()
    {
        return $this->belongsToMany(ParameterData::class, 'ecommerce_product_value_parameter_datas');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getValueAttribute()
    {
        switch ($this->parameter->field_type ?? false) {
            case false:
                return '';
            case 'pivot':
                return [
                    $this->parameter->slug => [
                        'label' => $this->parameter->title,
                        'value' => $this->parameter_datas()->listsTranslations('title')->get(['title'])->implode('title', ', '),
                    ]
                ];
            default:
                return [
                    $this->parameter->slug => [
                        'label' => $this->parameter->title,
                        'value' => $this->{$this->parameter->field_type},
                    ]
                ];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setStringAttribute($value)
    {
        if ((is_null($value) || !$value) && $this->getOriginal('string')) {
            \File::delete(base_path() . '/storage/app/public/origin/' . $this->getOriginal('string'));

            $this->attributes['string'] = $value ?? '';
        } elseif (starts_with($value, 'data:image')) {
            $this->uploadBase64Image($value, 'string', 'origin');
        } elseif ($value instanceof UploadedFile && $value->isValid()) {
            $this->uploadFile($value, 'string', 'origin');
        } else {
            $this->attributes['string'] = $value ?? '';
        }
    }
}
