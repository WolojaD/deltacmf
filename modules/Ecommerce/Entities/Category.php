<?php

namespace Modules\Ecommerce\Entities;

use Kalnoy\Nestedset\NodeTrait;
use Modules\Core\Eloquent\Model;
use Modules\Core\Eloquent\Builder;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Core\Eloquent\NestedBuilder;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;
use Modules\Seo\Repositories\Eloquent\EloquentStructureRepository;

class Category extends Model implements SeoSyncInterface
{
    use Translatable, SeoSyncTrait, NodeTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ecommerce_categories';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/ecommerce';

    protected $file_path = 'modules/Ecommerce/Config/models/category.json';

    public $page_template = 'products';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getChildrenList($result = [])
    {
        $children = $this->children;

        foreach ($children as $child) {
            if ($child->children) {
                $result = $child->getChildrenList($result);
            }

            $result[] = $child->id;
        }

        return $result;
    }

    public static function boot()
    {
        static::deleting(function ($obj) {
            $obj->children->each(function ($child) {
                $child->delete();
            });
        });

        parent::boot();
    }

    public function getAllProductsCount()
    {
        return Product::join($this->getTable(), function ($query) {
            $query->on($this->getTable() . '.id', '=', with(new Product)->getTable() . '.category_id')
                ->whereBetween('_lft', [$this->_lft, $this->_rgt]);
        })
        ->count();
    }

    public function getAllIds()
    {
        $products[] = $this->id;
        $children = $this->children()->get(['id', 'parent_id']);

        foreach ($this->children as $child) {
            $products = array_merge($child->getAllIds(), $products);
        }

        return $products;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parameters()
    {
        return $this->belongsToMany(Parameter::class, 'ecommerce_category_parameters', 'category_id', 'parameter_id');
    }

    public function first_children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('translations');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function nice_products()
    {
        return $this->hasMany(Product::class)->whereHas('versions');
    }

    public function discounted_products()
    {
        return $this->hasMany(Product::class)->discountedItems();
    }

    public function new_products()
    {
        return $this->hasMany(Product::class)->newItems();
    }

    public function top_products()
    {
        return $this->hasMany(Product::class)->topItems();
    }

    public function structure()
    {
        return $this->hasOne(Structure::class, 'object_id')->where('page_template', 'products')->with('translations');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('children', 'translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * get category from form request (if edit from product, else from ident)
     *
     * @param Builder $query
     * @param boolean $is_edit
     * @return Builder
     */
    public function scopeFromFormRequest($query, $is_edit = false)
    {
        return $query->when(
            $is_edit,
            function ($query) {
                $query->where(
                    'id',
                    function ($query) {
                        $query->from(with(new Product)->getTable())
                            ->where('id', request()->id ?? request()->get('ident', 0))
                            ->select('category_id');
                    }
                );
            },
            function ($query) {
                $query->where('id', request()->get('ident', 0));
            }
        );
    }

    public function scopeWithCountProducts($query, $categories)
    {
        $query->addSubCount(
            Product::whereIn('category_id', function ($query) use ($categories) {
                return $query->select('id')
                    ->from(with(new Category)->getTable() . ' as subc')
                    ->whereRaw('subc._lft between ' . with(new Category)->getTable() . '._lft + 1 and ' . with(new Category)->getTable() . '._rgt');
            })
            ->frontFilter(),
            'count'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getParameterListAttribute()
    {
        $all_params = Parameter::doesntHave('categories')->get();
        $parameters = $this->parameters;
        $parent = $this->parent;

        while (!is_null($parent)) {
            $parameters = $parent->parameters->merge($parameters);
            $parent = $parent->parent;
        }

        return $parameters->merge($all_params)->sortBy('order');
    }

    public function getAllProductsCountAttribute()
    {
        return $this->getAllProductsCount();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */
    public function parentStructure()
    {
        return Structure::where(function ($query) {
            $query->whereNotNull('page_id')
                ->where('page_template', 'products');
        })
        ->orWhere(function ($query) {
            $query->where('object_id', $this->parent_id)
                ->where('page_template', 'products');
        })
        ->orderByDesc('depth')
        ->first();
    }

    public static function seoAddChildrenToStructure($structure)
    {
        if ($structure->page_template != 'products') {
            return;
        }
        $category = self::where('id', $structure->object_id)->first();

        if (!$category) {
            return;
        }

        $structure_repository = (new EloquentStructureRepository(new Structure));

        $children = $category->children;

        foreach ($children as $sub_category) {
            $structure_repository->createWithModel($sub_category);
        }

        $products = $category->products;

        foreach ($products as $product) {
            $structure_repository->createWithModel($product);
        }
    }

    /**
     * Changes builder to custom
     *
     * @param [type] $query
     *
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new NestedBuilder($query);
    }
}
