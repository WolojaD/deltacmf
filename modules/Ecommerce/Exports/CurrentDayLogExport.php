<?php

namespace Modules\Ecommerce\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CurrentDayLogExport implements FromView
{
    protected $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function view(): View
    {
        return view('ecommerce::export.current_day', ['products' => $this->collection['products'], 'storages' => $this->collection['storages']]);
    }
}
