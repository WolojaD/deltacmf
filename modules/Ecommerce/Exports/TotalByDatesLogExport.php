<?php

namespace Modules\Ecommerce\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TotalByDatesLogExport implements FromView
{
    protected $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function view(): View
    {
        return view('ecommerce::export.total_by_dates', ['dates' => $this->collection['dates'], 'products' => $this->collection['products']]);
    }
}
