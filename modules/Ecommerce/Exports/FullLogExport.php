<?php

namespace Modules\Ecommerce\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FullLogExport implements FromView
{
    protected $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function view(): View
    {
        return view('ecommerce::export.full', ['dates' => $this->collection['dates'], 'storages' => $this->collection['storages']]);
    }
}
