<?php

namespace Modules\Ecommerce\Traits;

use Modules\Emailtemplates\Providers\EmailtemplatesTraitsProvider;

trait EmailTemplatesTrait
{
    public function sendOrderDataForUser($order_data)
    {
        $trait_provider = new EmailtemplatesTraitsProvider();

        $data_array = [
            'module' => 'Ecommerce',
            'method' => __FUNCTION__,
            'user_email' => $order_data->email ?? $order_data->user->email,
            'variables' => [
                'user_fullname' => $order_data->title,
                'user_phone' => $order_data->phone,
                'user_address' => $order_data->address,
                'user_delivery' => $order_data->delivery,
                'user_delivery_price' => $order_data->delivery_price,
                'user_comment' => $order_data->comment,
            ],
            'variables_for_control_structures' => [
                'order_product_versions' => $order_data->pivot,
            ],
        ];

        $trait_provider->handle($data_array);
    }

    public function sendOrderDataForAdmin($order_data)
    {
        $trait_provider = new EmailtemplatesTraitsProvider();

        $data_array = [
            'module' => 'Ecommerce',
            'method' => __FUNCTION__,
            'variables' => [
                'user_fullname' => $order_data->title,
                'user_email' => $order_data->email ?? $order_data->user->email,
                'user_phone' => $order_data->phone,
                'user_address' => $order_data->address,
                'user_delivery' => $order_data->delivery,
                'user_delivery_price' => $order_data->delivery_price,
                'user_comment' => $order_data->comment,
            ],
            'variables_for_control_structures' => [
                'order_product_versions' => $order_data->pivot,
            ],
        ];

        $trait_provider->handle($data_array);
    }
}
