<?php

namespace Modules\Ecommerce\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Modules\Core\Providers\ServiceProvider;
use Modules\Core\Events\LoadingBackendTranslations;

class EcommerceServiceProvider extends ServiceProvider
{
    protected $model;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('ecommerce_sidebar', array_dot(trans('ecommerce::sidebar')));
            $event->load('ecommerce_table-fields', array_dot(trans('ecommerce::table-fields')));
            $event->load('ecommerce_permissions', array_dot(trans('ecommerce::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('ecommerce', 'config');
        $this->publishConfig('ecommerce', 'permissions');
        $this->publishConfig('ecommerce', 'settings');

        $this->registerFactories();

        $this->bladeDirectives();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $folder = base_path('modules/Ecommerce/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }

        foreach ($models as $model_name => $model) {
            $model_name = studly_case($model_name);
            $this->model = $model;

            $this->app->bind("Modules\Ecommerce\Repositories\\{$model_name}Repository", function () use ($model_name, $model) {
                $eloquent_repository = "\Modules\Ecommerce\Repositories\Eloquent\Eloquent{$model_name}Repository";
                $entity = "\Modules\Ecommerce\Entities\\{$model_name}";
                $cache_decorator = "\Modules\Ecommerce\Repositories\Cache\Cache{$model_name}Decorator";

                $repository = new $eloquent_repository(new $entity());

                if (!Config::get('app.cache') && !($model->no_cache ?? false)) {
                    return $repository;
                }

                return new $cache_decorator($repository);
            });
        }
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }

    private function bladeDirectives()
    {
        Blade::directive('comparisonLink', function () {
            $ids = session()->get('products.to_compare') ?? [];

            return count($ids) > 1 ? locale_url('comparison/' . implode('-', $ids)) : locale_url('comparison');
        });
    }
}
