<?php

namespace Modules\Ecommerce\Providers;

use Modules\Ecommerce\Events\StockWasUpdated;
use Modules\Ecommerce\Events\Handlers\StockLogger;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        StockWasUpdated::class => [
            StockLogger::class,
        ],
    ];
}
