<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Ecommerce\Repositories\ProductVersionRepository;

class CacheProductVersionDecorator extends CoreCacheDecorator implements ProductVersionRepository
{
    /**
    * @var ProductVersionRepository
    */
    protected $repository;

    public function __construct(ProductVersionRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
