<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Ecommerce\Repositories\ParameterDataRepository;

class CacheParameterDataDecorator extends CoreCacheDecorator implements ParameterDataRepository
{
    /**
    * @var ParameterDataRepository
    */
    protected $repository;

    public function __construct(ParameterDataRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
