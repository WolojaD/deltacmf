<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Ecommerce\Repositories\DeliveryPayRepository;

class CacheDeliveryPayDecorator extends CoreCacheDecorator implements DeliveryPayRepository
{
    /**
    * @var DeliveryPayRepository
    */
    protected $repository;

    public function __construct(DeliveryPayRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
