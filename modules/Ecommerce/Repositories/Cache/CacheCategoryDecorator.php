<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\CategoryRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheCategoryDecorator extends CoreCacheDecorator implements CategoryRepository
{
    /**
    * @var CategoryRepository
    */
    protected $repository;

    public function __construct(CategoryRepository $ecommerce)
    {
        parent::__construct();

        $this->entityName = 'ecommerce_categories';
        $this->repository = $ecommerce;
    }

    public function getProductFields($is_edit = false)
    {
        return $this->repository->getProductFields($is_edit);
    }

    /**
     * {@inheritdoc}
     */
    public function getViewData($structure, $filter, $paginated, $filtered)
    {
        return $this->remember(function () use ($structure, $filter, $paginated, $filtered) {
            return $this->repository->getViewData($structure, $filter, $paginated, $filtered);
        });
    }

    // public function getViewData($structure, $filter, $paginated, $filtered)
    // {
    //     return $this->repository->getViewData($structure, $filter, $paginated, $filtered);
    // }

    /**
     * {@inheritdoc}
     */
    public function getCategoryMenu()
    {
        return $this->remember(function () {
            return $this->repository->getCategoryMenu();
        });
    }

    // public function getCategoryMenu()
    // {
    //     return $this->repository->getCategoryMenu();
    // }
}
