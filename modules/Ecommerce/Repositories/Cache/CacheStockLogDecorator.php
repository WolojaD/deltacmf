<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\StockLogRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheStockLogDecorator extends CoreCacheDecorator implements StockLogRepository
{
    /**
     * @var StockLogRepository
     */
    protected $repository;

    public function __construct(StockLogRepository $ecommerce)
    {
        parent::__construct();

        $this->entityName = 'StockLog';
        $this->repository = $ecommerce;
    }
}
