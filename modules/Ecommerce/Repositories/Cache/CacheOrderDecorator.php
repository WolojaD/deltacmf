<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\OrderRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheOrderDecorator extends CoreCacheDecorator implements OrderRepository
{
    /**
    * @var OrderRepository
    */
    protected $repository;

    public function __construct(OrderRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
