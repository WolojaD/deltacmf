<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheProductDecorator extends CoreCacheDecorator implements ProductRepository
{
    /**
    * @var ProductRepository
    */
    protected $repository;

    public function __construct(ProductRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
