<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Ecommerce\Repositories\ParameterRepository;

class CacheParameterDecorator extends CoreCacheDecorator implements ParameterRepository
{
    /**
    * @var ParameterRepository
    */
    protected $repository;

    public function __construct(ParameterRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
