<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\SaleRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheSaleDecorator extends CoreCacheDecorator implements SaleRepository
{
    /**
    * @var SaleRepository
    */
    protected $repository;

    public function __construct(SaleRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
