<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Ecommerce\Repositories\ProductValueRepository;

class CacheProductValueDecorator extends CoreCacheDecorator implements ProductValueRepository
{
    /**
    * @var ProductValueRepository
    */
    protected $repository;

    public function __construct(ProductValueRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
