<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\StockRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheStockDecorator extends CoreCacheDecorator implements StockRepository
{
    /**
     * @var StockRepository
     */
    protected $repository;

    public function __construct(StockRepository $ecommerce)
    {
        parent::__construct();

        $this->entityName = 'Stock';
        $this->repository = $ecommerce;
    }
}
