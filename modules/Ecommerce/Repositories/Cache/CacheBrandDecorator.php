<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Ecommerce\Repositories\BrandRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheBrandDecorator extends CoreCacheDecorator implements BrandRepository
{
    /**
    * @var BrandRepository
    */
    protected $repository;

    public function __construct(BrandRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
