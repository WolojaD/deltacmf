<?php

namespace Modules\Ecommerce\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Ecommerce\Repositories\DeliveryTypeRepository;

class CacheDeliveryTypeDecorator extends CoreCacheDecorator implements DeliveryTypeRepository
{
    /**
    * @var DeliveryTypeRepository
    */
    protected $repository;

    public function __construct(DeliveryTypeRepository $ecommerce)
    {
        parent::__construct();

        $this->repository = $ecommerce;
    }
}
