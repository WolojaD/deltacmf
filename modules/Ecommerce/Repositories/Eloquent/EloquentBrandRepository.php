<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Repositories\BrandRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentBrandRepository extends EloquentCoreRepository implements BrandRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/brand.json';

    /**
     * Remove products from brand
     *
     * @param array $detach_list
     * @param integer $brand_id
     *
     * @return void
     */
    public function detachProducts($detach_list, $brand_id)
    {
        $detach_list = $this->validForPivotMaking($detach_list);

        if ($detach_list) {
            Product::whereIn('id', $detach_list)->update(['brand_id' => 0]);
        }
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
    * Data generation to show in view blade
    */
    public function getViewData($structure)
    {
        return [
            'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
            'structure' => $structure,
            'brands' => $this->all()
        ];
    }
}
