<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Repositories\ParameterDataRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentParameterDataRepository extends EloquentCoreRepository implements ParameterDataRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/parameterdata.json';

    public function visible()
    {
        if (strpos(request()->path(), 'find-new')) {
            $repository = new EloquentParameterRepository(new Parameter);

            return (bool) $repository->isColored(request()->ident);
        }

        return $this->model->where('id', request()->ident)->whereHas('parameter', function ($query) {
            $query->where('colored', 1);
        })->value('id') ?? false;
    }
}
