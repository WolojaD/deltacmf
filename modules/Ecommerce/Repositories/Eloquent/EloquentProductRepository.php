<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Ecommerce\Entities\Sale;
use Modules\Ecommerce\Entities\Brand;
use Modules\Gallery\Entities\Gallery;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Entities\ProductSale;
use Modules\Ecommerce\Entities\ProductValue;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Ecommerce\Transformers\FrontProductTransformer;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\Gallery\Repositories\Eloquent\EloquentGalleryRepository;

class EloquentProductRepository extends EloquentCoreRepository implements ProductRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/product.json';

    private $parameter;
    private $category;
    private $brand;
    private $sale;

    /**
     * @param \Modules\Ecommerce\Entities\Product $model
     */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->category = new EloquentCategoryRepository(new Category);
        $this->parameter = new EloquentParameterRepository(new Parameter);
        $this->brand = new EloquentBrandRepository(new Brand);
        $this->gallery = new EloquentGalleryRepository(new Gallery);
        $this->sale = new EloquentSaleRepository(new Sale);
    }

    /**
     * Create product
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create($data)
    {
        return $this->storeData($data, function ($result, $phantom_result) {
            $product = $this->model->create($result);

            $this->saveDynamicValues($phantom_result, $product);

            $this->saveVersions($product);

            if ($gallery_id = $this->duplicateExistedGallery($result['gallery_id'])) {
                $this->model->where('id', $product->id)->update(['gallery_id' => $gallery_id]);
            }

            if (ends_with(request()->headers->get('referer'), '/edit') && ($sale = $this->model->where('id', request()->ident)->first()->sales()->first() ?? false)) {
                $sale = $this->sale->find($sale->id);

                ProductSale::whereIn('product_id', [$product->id])->delete();

                $sale->products()->syncWithoutDetaching([$product->id]);

                ProductVersion::whereIn('product_id', [$product->id])->update([
                    'discount_price' => \DB::raw('`price`*' . ((100 - $sale->discount) / 100))
                ]);
            }

            return $product;
        });
    }

    /**
     * Update product
     *
     * @param \Modules\Ecommerce\Entities\Product $product
     * @param array $data
     *
     * @return mixed
     */
    public function update($product, $data)
    {
        return $this->storeData($data, function ($result, $phantom_result) use ($product) {
            $this->saveDynamicValues($phantom_result, $product);

            $product->update($result);

            $this->saveVersions($product);

            return $product;
        });
    }

    public function relationsLists()
    {
        $categories = $this->category->all();
        $brands = $this->brand->all();

        return compact('categories', 'brands');
    }

    /**
     * Store data with transactions
     *
     * @param array $data
     * @param Callable $callback
     *
     * @return mixed
     */
    public function storeData($data, $callback)
    {
        list($result, $phantom_result) = $this->prepareDataToStore($data);

        DB::beginTransaction();

        try {
            $product = $callback($result, $phantom_result);

            DB::commit();

            return $product;
        } catch (\Exception $exception) {
            DB::rollback();

            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    /**
     * @return mixed
     */
    public function brands()
    {
        return $this->brand->model->listsDefaultTranslations('title')->pluck('title', 'id')->sort()->toArray();
    }

    public function categories()
    {
        return $this->category->model->whereIsRoot()->with('children', 'translations')->translated()->get()->flatten()->toArray();
    }

    /**
     * Generate fields components with parameters for create/update page.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFieldsArray()
    {
        return $this->fieldsFromParameters(
            parent::getFieldsArray(),
            $this->category->getProductFields(
                strpos(request()->route()->getName(), 'findEdit') !== false ||
                strpos(request()->route()->getName(), 'update') !== false
            )
        );
    }

    /**
     * @param array $result
     * @param \Illuminate\Database\Eloquent\Collection $fields
     *
     * @return mixed
     */
    private function fieldsFromParameters($result, $fields)
    {
        $key = '{"en":"Parameters","ru":"Параметры"}';
        $front_main_locale = key(get_application_frontend_locales());
        // TODO Fix Langs
        foreach ($fields as $field) {
            if ($field->key_type === 'divider') {
                $key = $field->translations->pluck('title', 'locale');
                $key['ru'] = $key[$front_main_locale];
                $key['en'] = $key[$front_main_locale];
                $key = json_encode($key);

                continue;
            }

            if ($field->to_versions && $field->key_type === 'multifkey') {
                continue;
            }

            $labels = $field->translations->pluck('title', 'locale')->toArray();

            // ru is main language of backend
            $labels['ru'] = $labels[$front_main_locale] ?? '';
            $labels['en'] = $labels[$front_main_locale] ?? '';

            $field_data = [
                'field_type' => $field->data_types[(int)$field->data_type]['form'],
                'parameters' => [
                    'field_label' => $labels,
                    'hint' => ['en' => $field->comment, 'ru' => $field->comment],
                    'model' => $this->model->table_name,
                    'required' => !!$field->required ?? false,
                    'validation' => $field->validation ?? '',
                    'data_type' => $field->data_types[(int) $field->data_type]['data'],
                    'not_exists' => true,
                    'array_values' => $this->getParameterArrayValues($field),
                    'settings' => $field->settings ?? ''
                ],
            ];

            $translatable = strpos($field->data_types[(int)$field->data_type]['data'], 'translatable_') === 0;

            if ($translatable) {
                $result[$key]['translatable'][$field->slug] = $field_data;
            } else {
                $result[$key][$field->slug] = $field_data;
            }
        }

        return $result;
    }

    public function getParameterArrayValues($field)
    {
        if (in_array($field->key_type, ['fkey', 'multifkey'])) {
            return $field->data()->listsDefaultTranslations('title')->pluck('title', 'id')->sort()->toArray();
        }

        return [];
    }

    /**
     * Prepare product relative and internal data
     * @param array $data
     *
     * @return array
     */
    private function prepareDataToStore($data): array
    {
        $fields = $this->getFieldsArray();

        $formatted_fields = [];

        foreach ($fields as $tab => $value) {
            $formatted_fields = array_merge_recursive($value, $formatted_fields);
        }

        $result = [];
        $phantom_result = [];

        $locales = app('laravellocalization')->getSupportedFrontendLocales();

        foreach ($data as $field => $value) {
            $translation_value = $locales[$field] ?? false;

            if (!$translation_value && isset($formatted_fields[$field])) {
                list($phantom_result, $result) = $this->simpleDataFilter($formatted_fields, $field, $value, $phantom_result, $result);
            } else {
                if (!is_array($value) || !($locales[$field] ?? false)) {
                    continue;
                }

                list($formatted_fields, $phantom_result, $result) = $this->translatedDataFilter($value, $formatted_fields, $phantom_result, $field, $result);
            }
        }

        $status = false;

        foreach ($result['versions'] as $version) {
            if ($version['status'] == 1) {
                $status = 1;
            }
        }

        if (!$status) {
            $result['status'] = 0;
        }

        unset($result['versions']);

        return [$result, $phantom_result];
    }

    /**
     * Save product relative data
     * @param array $phantom_result
     * @param \Modules\Ecommerce\Entities\Product $product
     */
    private function saveDynamicValues($phantom_result, $product): void
    {
        foreach ($phantom_result as $fieldOrLocale => $value) {
            $parameter = Parameter::where('slug', $fieldOrLocale)->first();

            if ($parameter == null) {
                continue;
            }

            if ($parameter->key_type == 'multifkey' && is_array($value[$parameter->key_type])) {
                ProductValue::updateOrCreate([
                    'parameter_id' => $parameter->id,
                    'product_id' => $product->id,
                ], [
                    'data_type' => $parameter->key_type
                ])
                ->parameter_datas()
                ->sync($value[$parameter->key_type]);

                continue;
            }

            if (is_array($value[$parameter->key_type])) {
                continue;
            }

            $field_type = $value[$parameter->key_type];

            if ($parameter->key_type == 'datetime') {
                $field_type = Carbon::parse($field_type);
            }

            ProductValue::updateOrCreate([
                'parameter_id' => $parameter->id,
                'product_id' => $product->id,
            ], [
                $parameter->field_type => $field_type,
                'data_type' => $parameter->key_type
            ]);
        }

        $trans_fields = [];
        $locales = app('laravellocalization')->getSupportedFrontendLocales();

        foreach ($locales as $locale => $data) {
            foreach ($phantom_result[$locale] ?? [] as $key => $item) {
                $field = key($item);
                $item = [$this->parameter->getField($field) => current($item)];
                $trans_fields[$key]['parameter'] = Parameter::where('slug', $key)->first();

                $trans_fields[$key][$locale] = $item;
            }
        }

        foreach ($trans_fields as $field => $value) {
            $parameter_id = $value['parameter']->id;
            $value['data_type'] = $value['parameter']->key_type;

            unset($value['parameter']);

            ProductValue::updateOrCreate([
                'parameter_id' => $parameter_id,
                'product_id' => $product->id,
            ], $value);
        }
    }

    private function saveVersions($product)
    {
        if (!request()->filled('versions')) {
            return;
        }

        $discounts_in_versions = count(
            array_filter(
                array_column(request()->versions, 'discount'),
                function ($item) {
                    return (float) $item > 0;
                }
                )
            );
        $discount = 0;

        if ($discounts_in_versions) {
            $product->sales()->detach();
        } else {
            $discount = (float) $product->discount;
        }

        $version_ids = array_column(request()->versions ?? [], 'id');

        $product->versions()
                ->when(
                    count($version_ids),
                    function ($q) use ($version_ids) {
                        $q->whereNotIn('id', $version_ids);
                    }
                )
                ->get()
                ->each(function ($item) {
                    $item->delete();
                });

        if (!count(request()->versions)) {
            return response()->json(['error' => trans('ecommerce.messages.errors.no_versions')], 422);
        }

        foreach (request()->versions as $version) {
            $parameters = $version['parameters'] ?? []; // TODO BUG must be correct index
            unset($version['parameters']);

            if (isset($version['id']) && request()->path() == 'api/backend/ecommerce/product') {
                unset($version['id']);
            }

            $version['discount_price'] = $version['price'] * (100 - ($discount ?: $version['discount'])) / 100;

            $version_item = ProductVersion::where('id', $version['id'] ?? 0)->first();

            if ($version_item) {
                $version_item->update($version);
            } else {
                $version_item = $product->versions()->create($version);
            }

            if (($parameters ?? false) && count($parameters)) {
                foreach ($parameters as $id => $parameter) {
                    if (!($$id ?? false)) {
                        $$id = Parameter::where('slug', $id)->select('id')->first()->id;
                    }

                    $data[(int)$parameter] = [
                        'parameter_id' => $$id,
                        'product_id' => $version_item->product_id
                    ];

                    unset($$id);
                }

                if (!isset($data)) {
                    continue;
                }

                $version_item->parameterData()->sync($data);
                unset($parameters, $data);
            }
        }

        if (($product->versions()->where('status', '>', 0)->count() == 0) && ($product->status == 1)) {
            $product->status = 0;
            // $product->save();
        }
    }

    public function duplicateExistedGallery($gallery_id)
    {
        return $this->gallery->duplicateExisted($gallery_id);
    }

    /**
     * @param array $formatted_fields
     * @param string $field
     * @param mixed $value
     * @param array $phantom_result
     * @param array $result
     *
     * @return array
     */
    private function simpleDataFilter($formatted_fields, $field, $value, $phantom_result, $result): array
    {
        if ($formatted_fields[$field]['parameters']['not_exists'] ?? false) {
            $phantom_result[$field][$formatted_fields[$field]['parameters']['data_type']] = $value;
        } else {
            $result[$field] = $value;
        }

        return [$phantom_result, $result];
    }

    /**
     * @param mixed $value
     * @param array $formatted_fields
     * @param array $phantom_result
     * @param string $field
     * @param array $result
     *
     * @return array
     */
    private function translatedDataFilter($value, $formatted_fields, $phantom_result, $field, $result): array
    {
        foreach ($value as $trans_field => $trans_value) {
            if (isset($formatted_fields['translatable'][$trans_field])) {
                if ($formatted_fields['translatable'][$trans_field]['parameters']['not_exists'] ?? false) {
                    $phantom_result[$field][$trans_field][$formatted_fields['translatable'][$trans_field]['parameters']['data_type']] = $trans_value;
                } else {
                    $result[$field][$trans_field] = $trans_value;
                }
            }
        }

        return [$formatted_fields, $phantom_result, $result];
    }

    public function getProductsListWithoutDiscounts($data, $id = false)
    {
        $products = $this->model->when($data->ident, function ($q) use ($data) {
            return $q->whereDoesntHave('sales', function ($q) use ($data) {
                $q->where('sale_id', $data->ident);
            });
        })
            ->whereDoesntHave('versions', function ($q) use ($data) {
                $q->where('discount', '>', 0);
            })
            ->notIsOut()
            ->when($data->id ?? false, function ($q) use ($data) {
                return $q->where('id', $data->id);
            })
            ->when($id, function ($q) use ($id) {//if we add suggestions, we cant add self
                return $q->where('id', '!=', $id);
            })
            ->when($data->article ?? false, function ($q) use ($data) {
                return $q->whereHas('versions', function ($query) use ($data) {
                    return $query->where('article', $data->article);
                });
            })
            ->when($data->title ?? false, function ($q) use ($data) {
                return $q->whereTranslationLike('title', '%' . $data->title . '%');
            })
            ->when($data->categories ?? false, function ($q) use ($data) {
                $category = Category::find($data->categories);
                $children_list = $category->getChildrenList();
                $children_list[] = $category->id;

                return $q->whereIn('category_id', $children_list);
            })
            ->when($data->brands ?? false, function ($q) use ($data) {
                return $q->where('brand_id', $data->brands);
            })
            ->get()
            ->map(function ($product) {
                return [
                    'title' => $product->title,
                    'id' => $product->id,
                    'category_title' => $product->category_title,
                    'price' => $product->price,
                ];

                return $product->getAttributes();
            });

        return $this->paginator($products, 20);
    }

    /**
    * Data generation to show in view blade
    */
    public function getViewData($structure)
    {
        if ($structure->object_id) {
            $product = $structure->getProduct()->pushToRecent();

            $suggestions = $product->getSuggestionItems();

            $recent = $this->getRecent($product->id);

            return [
                'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
                'structure' => $structure,
                'product' => $product,
                'suggestions' => $suggestions,
                'recent' => $recent,
            ];
        }

        return [];
    }

    public function getRecent($id = false)
    {
        return $this->model->whereIn('id', session()->get('products.recently_viewed') ?? [])
                           ->when($id, function ($query) use ($id) {
                               $query->where('id', '!=', $id);
                           })->get();
    }

    public function frontSearch($request)
    {
        $search = $request->get('search', '');

        if (!$search) {
            return [];
        }

        $query = $this->model->frontSearch($search);

        return $request->ajax() ? FrontProductTransformer::collection($query->limit(8)->get()) : $query->paginate(10);
    }

    /*
    |--------------------------------------------------------------------------
    | Compare functions
    |--------------------------------------------------------------------------
    */

    /**
     * get products and table of characteristics of products parsed from slug
     *
     * @param string $slug
     * @return array
     */
    public function getCompared(string $slug)
    {
        $ids = explode('-', $slug);

        $products_to_compare = $this->model->whereIn('id', $ids)->get();
        $table = [];

        $products_to_compare->each(function ($item) use (&$table) {
            $item->characteristicsList->each(function ($value, $index) use (&$table, $item) {
                $table[$index]['label'] = $value['label'];
                $table[$index]['products'][$item->id] = $value['value'];
            });
        });

        return compact('products_to_compare', 'table');
    }

    public function pushToCompare($request)
    {
        if ($request->filled('id') && is_numeric($request->id)) {
            return $this->model->where('id', (int) $request->id)->first()->pushToCompare();
        }

        return false;
    }

    public function deleteFromCompare($id)
    {
        if (is_numeric($id)) {
            return $this->model->where('id', (int) $id)->first()->deleteFromCompare();
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | Attaching/detaching products to suggested
    |--------------------------------------------------------------------------
    */

    public function getSuggestions($request, $template = 'suggested')
    {
        $product = $this->model->where('id', $request->ident)->first();
        $products = $product->suggested;

        $carry = [
            [
                'name' => $product->defaultTranslate()->title,
                'link' => [
                    'name' => 'api.backend.ecommerce_product.edit',
                    'params' => [
                        'ident' => $request->ident
                    ]
                ]
            ],
            [
                'name' => trans('core::breadcrumbs.product suggestions'),
            ]
        ];

        $breadcrumbs = $product->getBackendBreadcrumbs();
        array_pop($breadcrumbs);
        $breadcrumbs = array_merge($breadcrumbs, $carry);

        return $this->paginator(
            $products,
            $request->get('per_page', 10),
            false,
            $template
            )
        ->changeDataByKey('breadcrumbs', $breadcrumbs);
    }

    /**
     * Add products to suggested
     *
     * @param array $products
     * @param integer $id
     *
     * @return void
     */
    public function attachProducts($products, $id)
    {
        return $this->model->find($id)->suggested()->attach(array_column($products, 'id'));
    }

    /**
     * Remove products from suggested
     *
     * @param array $detach_list
     * @param integer $product_id
     *
     * @return void
     */
    public function detachProducts($detach_list, $product_id)
    {
        if ($detach_list = $this->validForPivotMaking($detach_list)) {
            $this->model->find($product_id)->suggested()->detach($detach_list);
        }
    }
}
