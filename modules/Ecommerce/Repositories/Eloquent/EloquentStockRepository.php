<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Stores\Entities\Store;
use Modules\Ecommerce\Events\StockWasUpdated;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\StockRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\Stores\Repositories\Eloquent\EloquentStoreRepository;

class EloquentStockRepository extends EloquentCoreRepository implements StockRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/stock.json';

    /**
     * @var
     */
    protected $model;
    protected $product_version;
    protected $store;

    /**
     * EloquentStockRepository constructor.
     *
     * @param $model
     */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->product_version = new EloquentProductVersionRepository(new ProductVersion);
        $this->store = new EloquentStoreRepository(new Store());
    }

    /**
     * List of product versions to add to stock in admin panel
     *
     * @param array $data
     *
     * @return \Modules\Core\Helpers\BackendPaginator
     */
    public function getProductVersionsList($data)
    {
        $product_versions = $this->product_version->model->with('product')
            ->when($data->article, function ($q) use ($data) {
                return $q->where('article', 'like', $data->article . '%');
            })
            ->select('ecommerce_product_versions.*')
            ->get()
            ->map(function ($product_version) {
                return [
                    'title' => $product_version->product->title,
                    'id' => $product_version->id,
                    'price' => $product_version->price,
                    'discount' => $product_version->product_discount,
                    'article' => $product_version->article,
                    'product_id' => $product_version->product->id,
                    'quantity' => $product_version->quantity
                ];

                return $product_version->getAttributes();
            });

        return $this->paginator($product_versions, 20);
    }

    /**
     * Change product quantity in stock
     *
     * @param $data
     * @param $id
     *
     * @return mixed
     */
    public function changeQuantity($data, $id)
    {
        $stock = $this->model->findOrFail($id);
        $count_before = $stock->storage_quantity;
        $stock->storage_quantity = $data['value'];
        if ($data['store_id'] != 0) {
            $stock->store_id = $data['store_id'];
        }
        $stock->save();

        $stock->count_before = $count_before;

        event($event = new StockWasUpdated($stock->id, $stock));

        return $stock;
    }

    public function relationsLists()
    {
        $stores = $this->store->all();

        return compact('stores');
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data)
    {
        $stock = $this->model->create($data);

        $stock->count_before = null;
        event($event = new StockWasUpdated($stock->id, $stock));

        return $stock;
    }
}
