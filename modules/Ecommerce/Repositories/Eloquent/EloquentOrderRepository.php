<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Entities\Order;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\OrderProduct;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\OrderRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentOrderRepository extends EloquentCoreRepository implements OrderRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/order.json';

    /**
     * @var \Modules\Ecommerce\Entities\Order
     */
    protected $model;
    protected $product_version;

    /**
     * @param \Modules\Ecommerce\Entities\Order $model
     */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->product_version = new EloquentProductVersionRepository(new ProductVersion);
    }

    /**
     * Relation list of situations for change in order
     *
     * @return array
     */
    public function situations()
    {
        return $this->model->situations();
    }

    /**
     * Subtable productVersions data of order
     *
     * @param integer $id
     *
     * @return \Modules\Core\Helpers\BackendPaginator
     */
    public function productVersions($id)
    {
        $repository = new EloquentProductVersionRepository(new ProductVersion);

        $filter = request()->get('filter', false);

        $order_products = OrderProduct::where('order_id', $id)->when($filter, function ($query) use ($filter) {
            return $query->where(function ($query) use ($filter) {
                return $query->where('product_title', 'like', "%{$filter}%")
                  ->orWhere('article', 'like', "%{$filter}%");
            });
        })
        ->get();

        // for detach we need id
        $order_products->map(function ($product) {
            $product->id = $product->product_version_id;
        });

        return $repository->paginator(
            $order_products,
            request()->get('per_page', 100),
            false,
            'order'
        );
    }

    /**
     * Get all orders user has
     *
     * @param integer $id
     *
     * @return array
     */
    public function getUserOrders($id)
    {
        $orders = $this->model->where('user_id', $id)->with('pivot')->get();

        $user = $orders->first()->user;
        $user->total = $orders->reduce(function ($carry, $order) {
            return $carry + $order->total;
        }, 0);

        $orders = $orders->map(function ($order) {
            $data = $order->getAttributes();
            $data['total'] = $order->total;

            $data['pivot'] = $order->pivot->map(function ($product) {
                $data = $product->getAttributes();
                $data['discount_price'] = $product->discount_price;

                return $data;
            });

            return $data;
        });

        return compact('user', 'orders');
    }

    /**
     * Add product version to order
     *
     * @param mixed $product_versions
     * @param integer $id
     *
     * @return void
     */
    public function attachProducts($product_versions, $id)
    {
        $ids = collect($product_versions)->mapWithKeys(function ($product_version) {
            if (is_array($product_version)) {
                return [
                    $product_version['id'] => [
                        'product_id' => $product_version['product_id'],
                        'product_title' => $product_version['product_title'],
                        'article' => $product_version['article'],
                        'parameter' => $product_version['parameter'] ?? '',
                        'quantity' => 1,
                        'discount' => $product_version['discount'] ?? 0,
                        'price' => $product_version['price']
                    ]
                ];
            } else {
                return [
                    $product_version->id => [
                        'product_id' => $product_version->product->id,
                        'product_title' => $product_version->product->title,
                        'article' => $product_version->article,
                        'parameter' => $product_version->parameters_name ?? ($product_version->title ?? ''),
                        'quantity' => 1,
                        'discount' => $product_version->discount ?? 0,
                        'price' => $product_version->price
                    ]
                ];
            }
        });

        $this->model->find($id)->product_versions()->syncWithoutDetaching($ids);
    }

    /**
     * Remove product version from order (any quantity it has)
     *
     * @param array $detach_list
     * @param integer $sale_id
     *
     * @return void
     */
    public function detachProducts($detach_list, $sale_id)
    {
        $detach_list = $this->validForPivotMaking($detach_list);

        if ($detach_list) {
            $this->model->find($sale_id)->product_versions()->detach($detach_list);
        }
    }

    /**
     * List of product versions to add to order in admin panel
     *
     * @param array $data
     *
     * @return \Modules\Core\Helpers\BackendPaginator
     */
    public function getProductVersionsList($data)
    {
        $product_versions = $this->product_version->model->with('product', 'product.category')
            ->join('ecommerce_products', 'ecommerce_products.id', '=', 'ecommerce_product_versions.product_id')
            ->whereHas('product', function ($q) use ($data) {
                $q->notIsOut()
                  ->when($data->title ?? false, function ($q) use ($data) {
                      $q->whereTranslationLike('title', '%' . $data->title . '%');
                  });
            })
            ->when($data->ident, function ($q) use ($data) {
                return $q->whereDoesntHave('orders', function ($q) use ($data) {
                    $q->where('order_id', $data->ident);
                });
            })
            ->when($data->id ?? false, function ($q) use ($data) {
                return $q->where('ecommerce_products.id', $data->id);
            })
            ->when($data->categories ?? false, function ($q) use ($data) {
                $category = Category::find($data->categories);
                $children_list = $category->getChildrenList();
                $children_list[] = $category->id;

                return $q->whereIn('ecommerce_products.category_id', $children_list);
            })
            ->when($data->brands ?? false, function ($q) use ($data) {
                return $q->where('ecommerce_products.brand_id', $data->brands);
            })
            ->when($data->article, function ($q) use ($data) {
                return $q->where('article', 'like', $data->article . '%');
            })
            ->select('ecommerce_product_versions.*')
            ->get()
            ->map(function ($product_version) {
                return [
                    'title' => $product_version->product->title,
                    'id' => $product_version->product->id,
                    'category_title' => $product_version->category_title,
                    'price' => $product_version->price,
                    'discount' => $product_version->product_discount,
                    'parameter' => $product_version->parameters_name,
                    'article' => $product_version->article,
                    'product_id' => $product_version->product->id,
                    'product_title' => $product_version->product->title,
                ];

                return $product_version->getAttributes();
            });

        return $this->paginator($product_versions, 20);
    }

    /**
     * Change product quantity in order
     *
     * @param integer $item
     * @param integer $id
     *
     * @return void
     */
    public function changeQuantity($data, $id)
    {
        OrderProduct::where('product_version_id', $data['item'])->where('order_id', $id)->update(['quantity' => $data['value']]);

        return OrderProduct::where('product_version_id', $data['item'])->where('order_id', $id)->first();
    }

    /**
     * @param $columns
     *
     * @return mixed
     */
    public function selectListForExportExcel($columns)
    {
        return [
            'title' => 'title',
            'email' => 'email',
            'phone' => 'phone',
            'address' => 'address',
            'comment' => 'comment',
            'delivery' => 'delivery',
            'payment' => 'payment',
            'situation_title' => 'situation',
            'is_quick' => 'is_quick'
        ];
    }

    /**
     * Get all order data with user for send by email
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getMailOrderData($id)
    {
        return Order::where('id', $id)->with('pivot', 'user')->first();
    }
}
