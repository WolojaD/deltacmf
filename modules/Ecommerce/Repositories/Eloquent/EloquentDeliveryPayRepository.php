<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Repositories\DeliveryPayRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentDeliveryPayRepository extends EloquentCoreRepository implements DeliveryPayRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/deliverypay.json';
}
