<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Repositories\DeliveryTypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentDeliveryTypeRepository extends EloquentCoreRepository implements DeliveryTypeRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/deliverytype.json';
}
