<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Repositories\ParameterRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentParameterRepository extends EloquentCoreRepository implements ParameterRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/parameter.json';

    private $category;

    /**
     * @var \Modules\Ecommerce\Entities\Parameter
     */
    protected $model;

    /**
     * @param \Modules\Ecommerce\Entities\Parameter $model
     */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->model = $model;

        $this->category = new EloquentCategoryRepository(new Category);
    }

    public function categories()
    {
        return $this->category->model->whereIsRoot()->with('children', 'translations')->translated()->get()->flatten()->toArray();
    }

    public function types()
    {
        return array_map(function ($item) {
            return $item['name'][locale()];
        }, $this->model->data_types);
    }

    public function getField($key)
    {
        return $this->model->getField($key);
    }

    public function getFieldId($key)
    {
        return $this->model->getFieldId($key);
    }

    public function isTranslatable($key)
    {
        return strpos($this->model->data_types['data'][(int) $key], 'translatable_') === 0;
    }

    public function infoList()
    {
        $request = request();
        $request->merge(['whereIn' => [
            'data_type' => $this->model->infoTypeIds()
        ]]);

        return $this->getSorted($request, 'info');
    }

    /**
     * Create product
     *
     * @param array $data
     * @return mixed
     */
    public function create($data)
    {
        $categories = $data['categories'] ?? [];

        if (isset($data['categories'])) {
            unset($data['categories']);
        }

        $parameter = $this->model->create($data);

        $parameter->categories()->attach($categories);

        return $parameter;
    }

    /**
     * @param \Modules\Ecommerce\Entities\Parameter $model
     * @param array $data
     *
     * @return object
     */
    public function update($model, $data)
    {
        $categories = $data['categories'] ?? [];

        if (isset($data['categories'])) {
            unset($data['categories']);
        }

        $model->update($data);

        $model->categories()->sync($categories);

        return $model;
    }

    public function isColored($id)
    {
        return $this->model->where('id', $id)->value('colored') ?? false;
    }
}
