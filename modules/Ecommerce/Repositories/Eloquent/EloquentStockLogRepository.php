<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Stores\Entities\Store;
use Modules\Ecommerce\Entities\Stock;
use Modules\Ecommerce\Repositories\StockLogRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentStockLogRepository extends EloquentCoreRepository implements StockLogRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/stocklog.json';

    protected $model;
    protected $store;

    /**
     * EloquentStockLogRepository constructor.
     *
     * @param $model
     */
    public function __construct($model)
    {
        parent::__construct($model);
    }

    public function getFullLog()
    {
        $date_from = Carbon::now()->format('Y-m-d');
        $date_to = Carbon::now()->subDays(6)->format('Y-m-d');

        $dates = $this->createRange($date_from, $date_to);

        $log_dates = $this->model
            ->whereRaw('id IN (SELECT max(id) as id FROM ecommerce_stock_logs GROUP BY stock_id, CAST(created_at AS DATE) )')
            ->whereDate('created_at', '>=', $date_to)
            ->get()
            ->groupBy('stock_id')
            ->map(function ($product) {
                return $product->keyBy(function ($item) {
                    return substr($item['created_at'], 0, 10);
                });
            });

        $storages = Store::with('stocks')->get();
        $storages = $storages->map(function ($storage) use ($dates, $log_dates) {
            $data = $storage->getAttributes();
            $data['title'] = $storage->title;
            $products = $storage->stocks
                ->keyBy('article')
                ->map(function ($product) use ($storage, $dates, $log_dates) {
                    $data = $product->getAttributes();
                    $quantity = $product->storage_quantity;

                    foreach ($dates as $date) {
                        $count = isset($log_dates[$product->id]) ? $log_dates[$product->id]->get($date) : null;
                        if (null !== $count) {
                            $quantity = $count->count_after;
                        } else {
                            if ($product->created_at->format('Y-m-d') > $date) {
                                $quantity = ' - ';
                            }
                        }
                        $data['dates'][$date]['count'] = $quantity;
                    }

                    return $data;
                });

            $data['products'] = $products;

            return $data;
        });

        return compact('dates', 'storages');
    }

    public function getTotalByDatesLog()
    {
        $date_from = Carbon::now()->format('Y-m-d');
        $date_to = Carbon::now()->subDays(6)->format('Y-m-d');

        $dates = $this->createRange($date_from, $date_to);

        $log_dates = $this->model
            ->whereRaw('id IN (SELECT max(id) as id FROM ecommerce_stock_logs GROUP BY stock_id, CAST(created_at AS DATE) )')
            ->whereDate('created_at', '>=', $date_to)
            ->get()
            ->groupBy('stock_id')
            ->map(function ($product) {
                return $product->keyBy(function ($item) {
                    return substr($item['created_at'], 0, 10);
                });
            });

        $products = Stock::get()
            ->groupBy('article')
            ->map(function ($prods) use ($dates, $date_to, $log_dates) {
                $products = $prods->map(function ($product) use ($dates, $date_to, $log_dates) {
                    $data = [];

                    $quantity = $product->storage_quantity;

                    foreach ($dates as $date) {
                        $count = isset($log_dates[$product->id]) ? $log_dates[$product->id]->get($date) : null;
                        if (null !== $count) {
                            $quantity = $count->count_after;
                        } else {
                            if ($product->created_at->format('Y-m-d') > $date) {
                                $quantity = 0;
                            }
                        }
                        $data[$date] = $quantity;
                    }

                    return $data;
                });
                $data = [];
                foreach ($dates as $date) {
                    $data[$date] = $products->sum($date);
                }

                return $data;
            });

        return compact('dates', 'products');
    }

    public function getCurrentDayLog()
    {
        $storages = Store::get();
        $products = Stock::get()
            ->groupBy('article')
            ->map(function ($product) {
                return $product->keyBy('store_id');
            });

        return compact('products', 'storages');
    }

    public function createRange($start, $end, $format = 'Y-m-d')
    {
        $start = new \DateTime($start);
        $end = new \DateTime($end);
        $invert = $start > $end;

        $dates = [];
        $dates[] = $start->format($format);
        while ($start != $end) {
            $start->modify(($invert ? '-' : '+') . '1 day');
            $dates[] = $start->format($format);
        }

        return $dates;
    }
}
