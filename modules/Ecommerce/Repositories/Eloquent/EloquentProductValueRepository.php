<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Repositories\ProductValueRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentProductValueRepository extends EloquentCoreRepository implements ProductValueRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/productvalue.json';
}
