<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Entities\Sale;
use Modules\Ecommerce\Entities\Product;
use Modules\Core\Helpers\BackendPaginator;
use Modules\Ecommerce\Entities\ProductSale;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\SaleRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentSaleRepository extends EloquentCoreRepository implements SaleRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/sale.json';

    /**
     * All products sale have
     *
     * @param integer $id
     *
     * @return BackendPaginator
     */
    public function getRelatedProducts($id)
    {
        $repository = new EloquentProductRepository(new Product());
        $sale = $this->model->where('id', $id)->firstOrFail();

        $filter = request()->get('filter', false);

        return $repository->paginator(
            $sale->products()->when($filter, function ($query) use ($filter) {
                $query->whereTranslationLike('title', "%{$filter}%");
            })
            ->get(),
            request()->get('per_page', 100),
            $sale,
            'sale'
        );
    }

    /**
     * Add products to sale
     *
     * @param array $products
     * @param integer $id
     *
     * @return void
     */
    public function attachProducts($products, $id)
    {
        $sale = $this->model->find($id);
        $product_ids = Product::whereIn('id', array_column($products, 'id'))
        ->whereDoesntHave('versions', function ($q) {
            $q->where('discount', '>', 0);
        })
        ->get()
        ->pluck('id')
        ->toArray();

        ProductSale::whereIn('product_id', $product_ids)->delete();

        $sale->products()->syncWithoutDetaching($product_ids);

        ProductVersion::whereIn('product_id', $product_ids)->update([
            'discount_price' => \DB::raw('`price`*' . ((100 - $sale->discount) / 100))
        ]);
    }

    /**
     * Remove products from sale
     *
     * @param array $detach_list
     * @param integer $sale_id
     *
     * @return void
     */
    public function detachProducts($detach_list, $sale_id)
    {
        $detach_list = $this->validForPivotMaking($detach_list);

        if ($detach_list) {
            $this->model->find($sale_id)->products()->detach($detach_list);
        }
    }

    /**
     * Change discount of product
     *
     * @param array $data
     * @param integer $sale_id
     *
     * @return boolean
     */
    public function changeDiscount($data, $sale_id)
    {
        $product = \DB::table('ecommerce_product_sales')->where('sale_id', $sale_id)
            ->where('product_id', $data['product_id']);

        $product->update(['discount' => $data['value']]);
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
    * Data generation to show in view blade
    */
    public function getViewData($structure)
    {
        $result = [
            'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
            'structure' => $structure,
        ];

        if ($structure->page_template == 'sales') {
            $result['sales'] = $this->all();
        } elseif ($structure->page_template == 'sale') {
            $result['sale'] = $this->find($structure->object_id);
        }

        return $result;
    }
}
