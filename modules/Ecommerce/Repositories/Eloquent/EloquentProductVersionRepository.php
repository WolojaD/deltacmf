<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Ecommerce\Repositories\ProductVersionRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentProductVersionRepository extends EloquentCoreRepository implements ProductVersionRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/productversion.json';
}
