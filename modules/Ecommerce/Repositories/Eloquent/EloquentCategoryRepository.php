<?php

namespace Modules\Ecommerce\Repositories\Eloquent;

use Modules\Seo\Entities\Structure;
use Modules\Ecommerce\Entities\Brand;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Entities\Parameter;
use Modules\Ecommerce\Entities\ParameterData;
use Modules\Ecommerce\Entities\ProductVersion;
use Modules\Ecommerce\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\Seo\Repositories\Eloquent\EloquentStructureRepository;

class EloquentCategoryRepository extends EloquentCoreRepository implements CategoryRepository
{
    public $model_config = 'modules/Ecommerce/Config/models/category.json';

    public function getProductFields($is_edit = false)
    {
        return $this->model->fromFormRequest($is_edit)->first()->parameter_list;
    }

    /**
    * Data generation to show in view blade
    */
    public function getViewData($structure, $paginated, $sub_path)
    {
        if ($structure->object_id) {
            $category = $this->model->where('id', $structure->object_id)->with('first_children')->firstOrFail();
            $categories_id = $this->model->descendantsAndSelf($category->id)->pluck('id');

            $products = Product::whereHas('versions')
                ->frontFilter()
                ->whereIn('category_id', $categories_id)
                ->with('translations')
                ->paginate((int) settings('ecommerce::products_per_page') ?: 10);

            return [
                'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
                'structure' => $structure,
                'category' => $category,
                'sub_categories' => $category->first_children,
                'products' => $products,
                'filters' => $this->getFilterArray($category->id, $categories_id)
            ];
        }

        list($filter_structure) = (new EloquentStructureRepository(new Structure))->findByPath($sub_path);
        list($parent, $filter_breadcrumbs) = $this->getFrontBreadcrumbs($filter_structure);
        array_shift($filter_breadcrumbs);

        $breadcrumbs = array_merge($breadcrumbs, $filter_breadcrumbs);

        if (isset($filter_structure->object_id)) {
            $category = $this->model->where('id', $filter_structure->object_id)->with('first_children')->firstOrFail();
            $products = Product::whereIn('category_id', $this->model->descendantsAndSelf($category->id)->pluck('id'))
                ->with('translations')
                ->templateFilter($structure->page_template)
                ->paginate((int) settings('ecommerce::products_per_page') ?: 10);

            return [
                'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
                'structure' => $structure,
                'filter_structure' => $filter_structure,
                'category' => $category,
                'sub_categories' => $category->first_children,
                'products' => $products,
            ];
        }

        $categories = $this->model
            ->whereIsRoot()
            ->with('first_children')
            ->get();

        $products = Product::whereHas('versions')
                ->with('translations')
                ->whereIn('category_id', Category::pluck('id'))
                ->get();

        return [
            'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
            'structure' => $structure,
            'filter_structure' => $filter_structure,
            'sub_categories' => $categories,
            'products' => $products,
        ];
    }

    public function getFilterArray($category_id, $categories_id)
    {
        $result['categories'] = Category::listsTranslations('title')
            ->select('title', 'slug', with(new Category)->getTable() . '.id')
            ->addSubSelect(
                'url',
                Structure::select('url')
                ->whereRaw('object_id = ' . with(new Category)->getTable() . '.id and page_template = "products"')
            )
            ->withCountProducts($categories_id)
            ->where('parent_id', $category_id)
            ->get();

        $result['brands'] = Brand::whereHas('products', function ($query) use ($categories_id) {
            $query->whereIn('category_id', $categories_id);
        })
        ->withCountProducts($categories_id, true)
        ->get();

        $result['prices'] = ProductVersion::selectRaw('max(`discount_price`) / 100 as max, min(`discount_price`) / 100 as min')->first()->toArray();

        if (request()->get('min_price', false)) {
            $result['filtered_prices']['min_price'] = request()->min_price;
            $result['filtered_prices']['max_price'] = request()->max_price;
        } else {
            $result['filtered_prices'] = ProductVersion::selectRaw('max(`discount_price`) / 100 as max, min(`discount_price`) / 100 as min')
            ->whereHas('product', function ($query) {
                $query->frontFilter();
            })->first()->toArray();
        }

        $result['parameter_datas'] = ParameterData::listsTranslations('title')
            ->select(
                'parameter_id',
                'title',
                with(new ParameterData)->getTable() . '.slug',
                with(new ParameterData)->getTable() . '.id',
                with(new Parameter)->getTable() . '.slug as parameter_slug'
            ) // select needed for subqueries
            ->withProductsByCategoryIds($categories_id)
            ->withCountProducts($categories_id)
            ->join(with(new Parameter)->getTable(), with(new ParameterData)->getTable() . '.parameter_id', '=', with(new Parameter)->getTable() . '.id')
            // ->toSqlWithBindings()
            ->get();

        if (is_string(request()->get('filterParsed', ''))) {
            $exploded_filter = explode('_', request()->get('filterParsed', ''));
            $result['explodedFilter'] = $exploded_filter;

            if (($exploded_filter[0] ?? false) && $exploded_filter[0] == 'brand') {
                $result['filterBrand'] = $result['brands']->where('slug', $exploded_filter[1])->first()->id;
            } elseif (($exploded_filter[0] ?? false)) {
                $result['filterParameter'] = $result['parameter_datas']->where('slug', $exploded_filter[1])->first()->id ?? '';
            }
        }

        $result['parameters'] = $result['parameter_datas']->groupBy('parameter_id');

        if (is_array(request()->get('filterParsed', false))) {
            $ids = request()->filterParsed;
        } elseif ($result['filterParameter'] ?? false) {
            $ids = [$result['filterParameter']];
            request()->filterParsed = $ids;
        }

        if (isset($ids)) {
            $parameters = $result['parameter_datas']->whereIn('id', $ids)->pluck('parameter_id')->unique()->toArray();
            foreach ($parameters as $id) {
                ParameterData::whereNotIn('id', $ids)
                ->where('parameter_id', $id)
                ->select('id')
                ->withCountProducts($categories_id, $id)
                ->get()
                ->each(function ($item) use ($result) {
                    if ($data = $result['parameter_datas']->where('id', $item->id)->first()) {
                        $data->count = $item->count ?? 0;
                    }
                });
            }
        }

        return $result;
    }

    /**
     * @return array|mixed
     */
    public function getCategoryMenu()
    {
        $data = $this->model
            ->whereHas('structure')
            ->withCount('products')
            ->listsTranslations('title')
            ->with('structure')
            ->orderBy('order')
            ->get();

        $data = $this->buildTree($data, $data->min('parent_id'));

        return $data;
    }

    public function buildTree(&$data, $parent = 0, $level = 0)
    {
        $tree = collect();
        $level++;

        foreach ($data as $id => $node) {
            if ($node->parent_id == $parent) {
                unset($data[$id]);
                $node->level = $level;
                $node->childs = $this->buildTree($data, $node->id, $level);
                $node->product_count_in_category = $node->products_count;
                $count = $node->childs->sum('total_product_count');
                $node->total_product_count = $node->product_count_in_category + $count;
                $tree->push($node);
            }
        }

        return $tree;
    }
}
