<?php

namespace Modules\Ecommerce\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface CategoryRepository extends CoreInterface
{
    /**
     * @return mixed
     */
    public function getProductFields($is_edit = false);

    // public function getViewData($structure, $paginated, $sub_path);

    public function getCategoryMenu();
}
