<?php

namespace Modules\Ecommerce\Imports;

use Modules\Ecommerce\Entities\Stock;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class StockImport implements ToModel, SkipsOnFailure, WithHeadingRow, WithChunkReading
{
    public function model(array $row)
    {
        if ((int)$row['count'] == 0) {
            return;
        }

        $article = explode(',', $row['name']);

        return new Stock([
           'article' => trim($article[0]),
           'store_id' => trim($row['storage']),
           'storage_quantity' => $row['count']
        ]);
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    }

    public function chunkSize(): int
    {
        return 200;
    }

    public function headingRow(): int
    {
        return 1;
    }
}
