<?php

namespace Modules\Ecommerce\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateStockLogRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Ecommerce/Config/models/stock_log.json');
    }

    public function authorize()
    {
        return true;
    }
}
