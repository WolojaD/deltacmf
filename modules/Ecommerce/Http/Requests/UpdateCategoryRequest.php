<?php

namespace Modules\Ecommerce\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateCategoryRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        parent::__construct();

        $this->file = base_path('modules/Ecommerce/Config/models/category.json');
    }

    public function authorize()
    {
        return true;
    }
}
