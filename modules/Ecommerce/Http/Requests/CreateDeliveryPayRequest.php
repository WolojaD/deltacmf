<?php

namespace Modules\Ecommerce\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateDeliveryPayRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Ecommerce/Config/models/delivery_pay.json');
    }

    public function authorize()
    {
        return true;
    }
}
