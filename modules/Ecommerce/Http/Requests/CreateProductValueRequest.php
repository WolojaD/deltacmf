<?php

namespace Modules\Ecommerce\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateProductValueRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        parent::__construct();

        $this->file = base_path('modules/Ecommerce/Config/models/productvalue.json');
    }

    public function authorize()
    {
        return true;
    }
}
