<?php

namespace Modules\Ecommerce\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateDeliveryTypeRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        parent::__construct();

        $this->file = base_path('modules/Ecommerce/Config/models/delivery_type.json');
    }

    public function authorize()
    {
        return true;
    }
}
