<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Core\Http\Controllers\CorePublicController;

class ComparisonController extends CorePublicController
{
    public $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        return view('ecommerce::comparison', $this->product->getCompared($slug));
    }

    public function store(Request $request)
    {
        if ($this->product->pushToCompare($request)) {
            return response()->json([
                'message' => trans('frontend::ecommerce.messages.added_to_compare')
            ], 201);
        }

        return response()->json([
            'message' => trans('frontend::ecommerce.errors.not_added_to_compare')
        ], 501);
    }

    public function destroy($id)
    {
        if ($this->product->deleteFromCompare($id)) {
            return response()->json([
                'message' => trans('frontend::ecommerce.messages.deleted_from_compare')
            ], 201);
        }

        return response()->json([
            'message' => trans('frontend::ecommerce.errors.not_deleted_from_compare')
        ], 501);
    }
}
