<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Repositories\ParameterRepository;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Transformers\ParameterTransformer;
use Modules\Ecommerce\Http\Requests\CreateParameterRequest;
use Modules\Ecommerce\Http\Requests\UpdateParameterRequest;
use Modules\Ecommerce\Repositories\ParameterDataRepository;
use Modules\Ecommerce\Transformers\FullParameterTransformer;
use Modules\Ecommerce\Transformers\ParameterDataTransformer;

class ParameterController extends CoreApiController
{
    /**
     * @var ParameterRepository
     */
    protected $repository;

    /**
     * @var ParameterDataRepository
     */
    protected $parameter_data_repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(ParameterRepository $parameter, ParameterDataRepository $parameter_data)
    {
        $this->repository = $parameter;
        $this->parameter_data_repository = $parameter_data;
        $this->modelInfo = json_decode(file_get_contents(base_path('modules/Ecommerce/Config/models/parameter.json')));
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['parameter_id' => $id]]);

            return ParameterDataTransformer::collection($this->parameter_data_repository->serverPaginationFilteringFor($request));
        }

        return ParameterTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $parameter = $this->repository->find($id);

        return new FullParameterTransformer($parameter);
    }

    public function update($id, UpdateParameterRequest $request)
    {
        $parameter = $this->repository->find($id);

        $this->repository->update($parameter, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.parameter')]),
            'id' => $parameter->id,
        ]);
    }

    public function store(CreateParameterRequest $request)
    {
        $parameter = $this->repository->create($request->except('ident'));

        $url = $parameter->is_info_type ? ['name' => 'api.backend.ecommerce_info.show', 'params' => ['ident' => $parameter->id]] : '';

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.parameter')]),
            'id' => $parameter->id,
            'goto' => $url
        ]);
    }

    public function destroy($id)
    {
        $parameter = $this->repository->find($id);

        $this->repository->destroy($parameter);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.parameter')]),
        ]);
    }
}
