<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Ecommerce\Repositories\CategoryRepository;
use Modules\Ecommerce\Transformers\ProductTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Http\Requests\CreateProductRequest;
use Modules\Ecommerce\Http\Requests\UpdateProductRequest;
use Modules\Ecommerce\Transformers\FullProductTransformer;

class ProductController extends CoreApiController
{
    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $gallery;

    protected $category;

    public function __construct(ProductRepository $product, GalleryRepository $gallery, CategoryRepository $category)
    {
        $this->repository = $product;
        $this->modelInfo = json_decode(file_get_contents(base_path('modules/Ecommerce/Config/models/product.json')));
        $this->gallery = $gallery;
        $this->category = $category;
    }

    public function index(Request $request, $id = false)
    {
        return ProductTransformer::collection($this->repository->serverPaginationFilteringFor($request, 'product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function findNew(Request $request)
    {
        $gallery = $this->galleryForProduct();
        $carry = [
            [
                'name' => trans('core::breadcrumbs.product create')
            ]
        ];

        return [
            'fields' => $this->repository->getFieldsArray(),
            'fields_data' => [
                'gallery_id' => $gallery->id
            ],
            // 'buttons' => [
            //     'duplicate', 'show', 'delete', 'create'
            // ],
            'breadcrumbs' => $this->category->find($request->ident)->getBackendBreadcrumbs($carry)
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function findEdit(Request $request)
    {
        $carry = [
            [
                'name' => trans('core::breadcrumbs.product edit')
            ]
        ];
        $product = $this->repository->find($request->ident);

        return [
            'fields' => $this->repository->getFieldsArray(),
            'buttons' => [
                'duplicate', 'show', 'delete', 'create', 'suggestions'
            ],
            'breadcrumbs' => $product ? $product->category->getBackendBreadcrumbs($carry) : []
        ];
    }

    public function find($id)
    {
        $product = $this->repository->find($id);

        return new FullProductTransformer($product);
    }

    public function update($id, UpdateProductRequest $request)
    {
        $request->validate([
            'versions' => 'required|min:1',
            'versions.*.price' => 'numeric|min:0',
            'versions.*.discount' => 'numeric|max:100',
            'versions.*.discount_price' => 'numeric|min:0',
            'versions.*.quantity' => 'numeric|min:0',
            'versions.*.article' => 'required|max:100'
        ]);

        $product = $this->repository->find($id);

        $update = $this->repository->update($product, $request->except('ident'));

        if ($update instanceof \Illuminate\Http\JsonResponse) {
            return $update;
        }

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.product')]),
            'id' => $product->id,
            'goto' => ['name' => 'api.backend.ecommerce_category.show', 'params' => ['ident' => $product->category_id]]
        ]);
    }

    public function store(CreateProductRequest $request)
    {
        $request->validate([
            'versions.*.price' => 'numeric|min:0',
            'versions.*.discount' => 'numeric|max:100',
            'versions.*.discount_price' => 'numeric|min:0',
            'versions.*.quantity' => 'numeric|min:0',
            'versions.*.article' => 'required|max:100'
        ]);

        $product = $this->repository->create($request->except('ident'));

        if ($product instanceof \Illuminate\Http\JsonResponse) {
            return $product;
        }

        $gallery = $this->galleryForProduct($product);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.product')]),
            'id' => $product->id,
            'goto' => ['name' => 'api.backend.ecommerce_category.show', 'params' => ['ident' => $product->category_id]]
        ]);
    }

    public function destroy($id)
    {
        $product = $this->repository->find($id);

        $this->repository->destroy($product);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.product')]),
        ]);
    }

    public function versionsFields(Request $request)
    {
        if ($request->type == 'create') {
            $category = Category::where('id', $request->ident)->with('parameters', 'parameters', 'parameters.data')->first();
        } else {
            $category = Product::where('id', $request->ident)
                ->with('category', 'category.parameters', 'category.parameters', 'category.parameters.data')
                ->first()
                ->category;
        }

        return $category->parameter_list->filter(function ($parameter) {
            return $parameter->key_type == 'multifkey' && $parameter->to_versions;
        })->mapWithKeys(function ($parameter) {
            return [
                $parameter->slug => [
                    'id' => $parameter->id,
                    'title' => $parameter->title,
                    'data' => $parameter->data
                ]
            ];
        });
    }

    public function relationsLists(Request $requst)
    {
        return $this->repository->relationsLists();
    }

    private function galleryForProduct($product = false)
    {
        $gallery = [];

        $model_gallery = $this->gallery->tempUserGallery();

        if (!$product && $model_gallery) {
            return $model_gallery;
        }

        foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
            $gallery[$key]['title'] = $product ?
                $product['translations']->filter(function ($translation) use ($key) {
                    return $translation->locale == $key;
                })->first()->title ?? ''
            : 'temp gallery for product';
        }

        $gallery['type'] = 'product';
        $gallery['status'] = 1;
        $gallery['is_temp'] = 1;

        if ($product && $product->gallery_id) {
            $gallery['is_temp'] = 0;
            $model_gallery = $model_gallery ? $model_gallery : $product->gallery;

            return $model_gallery->update($gallery);
        }

        return $this->gallery->create($gallery);
    }

    /**
     * suggestions for product table
     *
     * @param Request $request
     * @param boolean $id
     * @return array
     */
    public function suggestions(Request $request, $id = false)
    {
        return ProductTransformer::collection($this->repository->getSuggestions($request, 'suggested'));
    }

    /*
    |--------------------------------------------------------------------------
    | Attaching/detaching products to suggestions
    |--------------------------------------------------------------------------
    */

    /**
     * get products for attaching to suggestions (mb not only)
     *
     * @param Request $request
     * @return void
     */
    public function filterProductList(Request $request)
    {
        $data = json_decode($request->form);

        return $this->repository->getProductsListWithoutDiscounts($data, $data->ident);
    }

    public function addProducts(Request $request, $id)
    {
        $translation = count($request->products) > 1 ? 'multi_attach' : 'attach';
        $this->repository->attachProducts($request->products, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.' . $translation, ['field' => trans('ecommerce::models.of.product')]),
        ]);
    }

    public function detachProduct(Request $request, $id)
    {
        $this->repository->detachProducts($request->item_id, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.detached', ['field' => trans('ecommerce::models.of.product')]),
        ]);
    }

    public function detachProducts(Request $request, $id)
    {
        $this->repository->detachProducts(json_decode($request->checked), $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.multi_detached', ['field' => trans('ecommerce::models.of.product')]),
        ]);
    }
}
