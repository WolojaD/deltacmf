<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Repositories\BrandRepository;
use Modules\Ecommerce\Transformers\BrandTransformer;
use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Ecommerce\Transformers\ProductTransformer;
use Modules\Ecommerce\Http\Requests\CreateBrandRequest;
use Modules\Ecommerce\Http\Requests\UpdateBrandRequest;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Transformers\FullBrandTransformer;

class BrandController extends CoreApiController
{
    /**
     * @var BrandRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    /**
     * @var ProductRepository
     */
    protected $product;

    public function __construct(BrandRepository $brand, ProductRepository $product)
    {
        $this->repository = $brand;
        $this->modelInfo = json_decode(file_get_contents(base_path('modules/Ecommerce/Config/models/brand.json')));
        $this->product = $product;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['brand_id' => $id]]);

            return ProductTransformer::collection($this->product->serverPaginationFilteringFor($request, 'brand'));
        }

        return BrandTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $brand = $this->repository->find($id);

        return new FullBrandTransformer($brand);
    }

    public function update($id, UpdateBrandRequest $request)
    {
        $brand = $this->repository->find($id);

        $this->repository->update($brand, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.brand')]),
            'id' => $brand->id
        ]);
    }

    public function store(CreateBrandRequest $request)
    {
        $brand = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.brand')]),
            'id' => $brand->id
        ]);
    }

    public function destroy($id)
    {
        $brand = $this->repository->find($id);

        $this->repository->destroy($brand);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.brand')]),
        ]);
    }

    public function detachProduct(Request $request, $id)
    {
        $this->repository->detachProducts($request->item_id, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.detached', ['field' => trans('ecommerce::models.of.brand')]),
        ]);
    }

    public function detachProducts(Request $request, $id)
    {
        $this->repository->detachProducts(json_decode($request->checked), $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.multi_detached', ['field' => trans('ecommerce::models.of.brand')]),
        ]);
    }
}
