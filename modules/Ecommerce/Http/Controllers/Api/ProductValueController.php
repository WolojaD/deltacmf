<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Entities\ProductValue;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Repositories\ProductValueRepository;
use Modules\Ecommerce\Transformers\ProductValueTransformer;
use Modules\Ecommerce\Http\Requests\CreateProductValueRequest;
use Modules\Ecommerce\Http\Requests\UpdateProductValueRequest;
use Modules\Ecommerce\Transformers\FullProductValueTransformer;

class ProductValueController extends CoreApiController
{
    /**
     * @var ProductValueRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(ProductValueRepository $productvalue)
    {
        $this->repository = $productvalue;
        $this->modelInfo = json_decode(file_get_contents(base_path('modules/Ecommerce/Config/models/product_value.json')));
    }

    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return ProductValueTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return ProductValueTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return ProductValueTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find(ProductValue $productvalue)
    {
        return new FullProductValueTransformer($productvalue);
    }

    public function update($id, UpdateProductValueRequest $request)
    {
        $productvalue = $this->repository->find($id);

        $this->repository->update($productvalue, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.product_value')]),
            'id' => $productvalue->id
        ]);
    }

    public function store(CreateProductValueRequest $request)
    {
        $productvalue = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.product_value')]),
            'id' => $productvalue->id
        ]);
    }

    public function destroy($id)
    {
        $productvalue = $this->repository->find($id);

        $this->repository->destroy($productvalue);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.product_value')]),
        ]);
    }
}
