<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Repositories\ProductVersionRepository;
use Modules\Ecommerce\Transformers\ProductVersionTransformer;
use Modules\Ecommerce\Http\Requests\CreateProductVersionRequest;
use Modules\Ecommerce\Http\Requests\UpdateProductVersionRequest;
use Modules\Ecommerce\Transformers\FullProductVersionTransformer;

class ProductVersionController extends CoreApiController
{
    /**
     * @var ProductVersionRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(ProductVersionRepository $productversion)
    {
        $this->repository = $productversion;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return ProductVersionTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return ProductVersionTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return ProductVersionTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $productversion = $this->repository->find($id);

        return new FullProductVersionTransformer($productversion);
    }

    public function update($id, UpdateProductVersionRequest $request)
    {
        $productversion = $this->repository->find($id);

        $this->repository->update($productversion, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.product_version')]),
            'id' => $productversion->id
        ]);
    }

    public function store(CreateProductVersionRequest $request)
    {
        $productversion = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.product_version')]),
            'id' => $productversion->id
        ]);
    }

    public function destroy($id)
    {
        $productversion = $this->repository->find($id);

        $this->repository->destroy($productversion);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.product_version')]),
        ]);
    }
}
