<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Ecommerce\Imports\StockImport;
use Modules\Users\Repositories\UserInterface;
use Modules\Ecommerce\Repositories\StockRepository;
use Modules\Ecommerce\Transformers\StockTransformer;
use Modules\Ecommerce\Http\Requests\CreateStockRequest;
use Modules\Ecommerce\Http\Requests\UpdateStockRequest;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Transformers\FullStockTransformer;

class StockController extends CoreApiController
{
    /**
     * @var StockRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;
    protected $user;

    public function __construct(StockRepository $stock, UserInterface $user)
    {
        $this->repository = $stock;
        $this->modelInfo = $this->repository->getJsonList();
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return StockTransformer
     */
    public function index(Request $request, $id = false)
    {
        $store_id = $this->getUserStoreId();
        if ($store_id != 0) {
            $request->merge(['where' => ['store_id' => $store_id]]);
        }

        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return StockTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        return StockTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullStockTransformer
     */
    public function find($id)
    {
        $stock = $this->repository->find($id);

        return new FullStockTransformer($stock);
    }

    /**
     * @param int $id
     * @param UpdateStockRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateStockRequest $request)
    {
        $stock = $this->repository->find($id);

        $this->repository->update($stock, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.stock')]),
            'id' => $stock->id
        ]);
    }

    /**
     * @param CreateStockRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStockRequest $request)
    {
        $stock = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.stock')]),
            'id' => $stock->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = $this->repository->find($id);

        $this->repository->destroy($stock);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.stock')]),
        ]);
    }

    public function relationsLists(Request $requst)
    {
        return $this->repository->relationsLists();
    }

    public function changeQuantity(Request $request, $id)
    {
        $data = $request->validate([
            'item' => 'required|numeric|min:1',
            'value' => 'required|numeric|min:0'
        ]);

        $data['store_id'] = $this->getUserStoreId();

        $item = $this->repository->changeQuantity($data, $id);

        return response()->json([
            'errors' => false,
            'message' => $item->article . ' - ' . $item->storage_quantity,
        ]);
    }

    public function filterProductList(Request $request)
    {
        $data = json_decode($request->form);

        return $this->repository->getProductVersionsList($data);
    }

    public function addProduct(Request $request, $id)
    {
        $data = $request->except(['title', 'id', 'price', 'discount', 'product_id', 'quantity']);

        $data['store_id'] = $this->getUserStoreId();

        $product = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.product')]),
            'id' => $product->id
        ]);
    }

    public function getUserStoreId()
    {
        $user_id = \Sentinel::getUser()->id ?? 0;
        $store_id = \DB::table('store_users')->where('user_id', $user_id)->first();

        return $store_id->store_id ?? 0;
    }

    public function importExcel(Request $request)
    {
        if ($request->hasFile('import_excel') && $request->file('import_excel')->isValid()) {
            Excel::import(new StockImport, $request->file('import_excel'));

            return response()->json([
                'errors' => false,
                'message' => trans('seo::messages.api.import excel success'),
            ]);
        }

        return response()->json([
            'errors' => true,
            'message' => trans('seo::messages.api.import excel error'),
        ]);
    }
}
