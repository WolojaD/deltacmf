<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Traits\EmailTemplatesTrait;
use Modules\Ecommerce\Repositories\OrderRepository;
use Modules\Ecommerce\Transformers\OrderTransformer;
use Modules\Ecommerce\Http\Requests\CreateOrderRequest;
use Modules\Ecommerce\Http\Requests\UpdateOrderRequest;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Transformers\FullOrderTransformer;
use Modules\Ecommerce\Transformers\ProductVersionOrderTransformer;

class OrderController extends CoreApiController
{
    use EmailTemplatesTrait;

    /**
     * @var OrderRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(OrderRepository $order)
    {
        $this->repository = $order;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        return OrderTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $order = $this->repository->find($id);

        $breadcrumbs = [
            [
                'name' => trans('core::breadcrumbs.order'),
                'link' => [
                    'name' => 'api.backend.ecommerce_order'
                ]
            ],
            [
                'name' => $order->title
            ]
        ];

        return [
            'data' => new FullOrderTransformer($order),
            'bradcrumbs' => $breadcrumbs,
            'title' => trans('ecommerce::messages.order.content') . ' №' . $order->id
        ];
    }

    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->repository->find($id);

        $data = $request->except('ident', 'total');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $this->repository->update($order, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.order')]),
            'id' => $order->id
        ]);
    }

    public function store(CreateOrderRequest $request)
    {
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $order = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.order')]),
            'id' => $order->id
        ]);
    }

    public function destroy($id)
    {
        $order = $this->repository->find($id);

        $this->repository->destroy($order);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.order')]),
        ]);
    }

    public function findTable(Request $request, $id)
    {
        if (strpos($request->route()->getName(), 'ecommerce_product_versions')) {
            return ProductVersionOrderTransformer::collection($this->repository->productVersions($id));
        }

        return parent::findTable($request, $id);
    }

    public function userOrders($id)
    {
        return $this->repository->getUserOrders($id);
    }

    public function filterProductList(Request $request)
    {
        $data = json_decode($request->form);

        return $this->repository->getProductVersionsList($data);
    }

    public function addProducts(Request $request, $id)
    {
        $translation = count($request->products) > 1 ? 'multi_attach' : 'attach';
        $this->repository->attachProducts($request->products, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.' . $translation, ['field' => trans('ecommerce::models.of.order')]),
        ]);
    }

    public function detachProduct(Request $request, $id)
    {
        $this->repository->detachProducts($request->item_id, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.detached', ['field' => trans('ecommerce::models.of.order')]),
        ]);
    }

    public function detachProducts(Request $request, $id)
    {
        $this->repository->detachProducts(json_decode($request->checked), $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.multi_detached', ['field' => trans('ecommerce::models.of.order')]),
        ]);
    }

    public function changeQuantity(Request $request, $id)
    {
        $data = $request->validate([
            'item' => 'required|numeric|min:1',
            'value' => 'required|numeric|min:1'
        ]);

        $product = $this->repository->changeQuantity($data, $id);

        return response()->json([
            'errors' => false,
            'message' => $product->product_title . ' - ' . $product->quantity . 'x' . $product->discount_price,
        ]);
    }

    /**
     * @param int $ident
     *
     * @return [type]
     */
    public function sendMailOrderData($ident)
    {
        $order_data = $this->repository->getMailOrderData($ident);

        $this->sendOrderDataForUser($order_data);
        $this->sendOrderDataForAdmin($order_data);

        return response()->json([
            'errors' => false,
            'message' => 'Test sendMailOrderData send Successful',
        ]);
    }
}
