<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Repositories\ParameterRepository;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Repositories\ParameterDataRepository;
use Modules\Ecommerce\Transformers\ParameterDataTransformer;
use Modules\Ecommerce\Http\Requests\CreateParameterDataRequest;
use Modules\Ecommerce\Http\Requests\UpdateParameterDataRequest;
use Modules\Ecommerce\Transformers\FullParameterDataTransformer;

class ParameterDataController extends CoreApiController
{
    /**
     * @var ParameterDataRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $parameter;

    public function __construct(ParameterDataRepository $parameterdata, ParameterRepository $parameter)
    {
        $this->repository = $parameterdata;
        $this->modelInfo = $this->repository->getJsonList();
        $this->parameter = $parameter;
    }

    public function index(Request $request, $id = false)
    {
        return ParameterDataTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $parameterdata = $this->repository->find($id);

        return new FullParameterDataTransformer($parameterdata);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return
    */
    public function findNew(Request $request)
    {
        $parameter = $this->parameter->find($request->ident);
        $carry = [
            ['name' => trans('core::breadcrumbs.info'), 'link' => 'api.backend.ecommerce_info'],
            [
                'name' => $parameter->defaultTranslate()->title,
                'link' => [
                    'name' => 'api.backend.ecommerce_info.show',
                    'params' => [
                        'ident' => $parameter->id
                    ]
                ],
            ],
            [
                'name' => trans('core::breadcrumbs.parameter data create')
            ]
        ];

        return [
            'fields' => $this->repository->getFieldsArray(),
            'breadcrumbs' => $carry
        ];
    }

    /**
     * Show the form for editin a resource.
     *
     * @return
     */
    public function findEdit(Request $request)
    {
        $parameter = $this->repository->find($request->ident)->parameter;
        $carry = [
            ['name' => trans('core::breadcrumbs.info'), 'link' => 'api.backend.ecommerce_info'],
            [
                'name' => $parameter->defaultTranslate()->title,
                'link' => [
                    'name' => 'api.backend.ecommerce_info.show',
                    'params' => [
                        'ident' => $parameter->id
                    ]
                ],
            ],
            [
                'name' => trans('core::breadcrumbs.parameter data edit')
            ]
        ];

        return [
            'fields' => $this->repository->getFieldsArray(),
            'breadcrumbs' => $carry
        ];
    }

    public function update($id, UpdateParameterDataRequest $request)
    {
        $parameterdata = $this->repository->find($id);

        $this->repository->update($parameterdata, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.parameter_data')]),
            'goto' => ['name' => 'api.backend.ecommerce_info.show', 'params' => ['ident' => $parameterdata->parameter_id]],
            'id' => $parameterdata->id
        ]);
    }

    public function store(CreateParameterDataRequest $request)
    {
        $parameterdata = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.parameter_data')]),
            'goto' => ['name' => 'api.backend.ecommerce_info.show', 'params' => ['ident' => $parameterdata->parameter_id]],
            'id' => $parameterdata->id
        ]);
    }

    public function destroy($id)
    {
        $parameterdata = $this->repository->find($id);

        $this->repository->destroy($parameterdata);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.parameter_data')]),
        ]);
    }
}
