<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Repositories\DeliveryTypeRepository;
use Modules\Ecommerce\Transformers\DeliveryTypeTransformer;
use Modules\Ecommerce\Http\Requests\CreateDeliveryTypeRequest;
use Modules\Ecommerce\Http\Requests\UpdateDeliveryTypeRequest;
use Modules\Ecommerce\Transformers\FullDeliveryTypeTransformer;

class DeliveryTypeController extends CoreApiController
{
    /**
     * @var DeliveryTypeRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(DeliveryTypeRepository $deliverytype)
    {
        $this->repository = $deliverytype;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        return DeliveryTypeTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $deliverytype = $this->repository->find($id);

        return new FullDeliveryTypeTransformer($deliverytype);
    }

    public function update($id, UpdateDeliveryTypeRequest $request)
    {
        $deliverytype = $this->repository->find($id);

        $this->repository->update($deliverytype, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.delivery_type')]),
            'id' => $deliverytype->id
        ]);
    }

    public function store(CreateDeliveryTypeRequest $request)
    {
        $deliverytype = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.delivery_type')]),
            'id' => $deliverytype->id
        ]);
    }

    public function destroy($id)
    {
        $deliverytype = $this->repository->find($id);

        $this->repository->destroy($deliverytype);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.delivery_type')]),
        ]);
    }
}
