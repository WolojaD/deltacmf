<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Repositories\DeliveryPayRepository;
use Modules\Ecommerce\Transformers\DeliveryPayTransformer;
use Modules\Ecommerce\Http\Requests\CreateDeliveryPayRequest;
use Modules\Ecommerce\Http\Requests\UpdateDeliveryPayRequest;
use Modules\Ecommerce\Transformers\FullDeliveryPayTransformer;

class DeliveryPayController extends CoreApiController
{
    /**
     * @var DeliveryPayRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(DeliveryPayRepository $deliverypay)
    {
        $this->repository = $deliverypay;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        return DeliveryPayTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $deliverypay = $this->repository->find($id);

        return new FullDeliveryPayTransformer($deliverypay);
    }

    public function update($id, UpdateDeliveryPayRequest $request)
    {
        $deliverypay = $this->repository->find($id);

        $this->repository->update($deliverypay, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.delivery_pay')]),
            'id' => $deliverypay->id
        ]);
    }

    public function store(CreateDeliveryPayRequest $request)
    {
        $deliverypay = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.delivery_pay')]),
            'id' => $deliverypay->id
        ]);
    }

    public function destroy($id)
    {
        $deliverypay = $this->repository->find($id);

        $this->repository->destroy($deliverypay);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.delivery_pay')]),
        ]);
    }
}
