<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Repositories\SaleRepository;
use Modules\Ecommerce\Transformers\SaleTransformer;
use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Ecommerce\Http\Requests\CreateSaleRequest;
use Modules\Ecommerce\Http\Requests\UpdateSaleRequest;
use Modules\Ecommerce\Transformers\ProductTransformer;
use Modules\Ecommerce\Transformers\FullSaleTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class SaleController extends CoreApiController
{
    /**
     * @var SaleRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $product;

    public function __construct(SaleRepository $sale, ProductRepository $product)
    {
        $this->repository = $sale;
        $this->modelInfo = $this->repository->getJsonList();
        $this->product = $product;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            return ProductTransformer::collection($this->repository->getRelatedProducts($id));
        }

        return SaleTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $sale = $this->repository->find($id);

        return new FullSaleTransformer($sale);
    }

    public function update($id, UpdateSaleRequest $request)
    {
        $sale = $this->repository->find($id);

        $this->repository->update($sale, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.sale')]),
            'id' => $sale->id
        ]);
    }

    public function store(CreateSaleRequest $request)
    {
        $sale = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.sale')]),
            'id' => $sale->id
        ]);
    }

    public function destroy($id)
    {
        $sale = $this->repository->find($id);

        $this->repository->destroy($sale);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.sale')]),
        ]);
    }

    public function findTable(Request $request, $id)
    {
        if (strpos($request->route()->getName(), 'ecommerce_products')) {
            return ProductTransformer::collection($this->repository->getRelatedProducts($id));
        }

        return parent::findTable($request, $id);
    }

    /*
    |--------------------------------------------------------------------------
    | Attaching/detaching products to sale
    |--------------------------------------------------------------------------
    */

    /**
     * get list for attaching products to sale
     *
     * @param Request $request
     * @return void
     */
    public function filterProductList(Request $request)
    {
        $data = json_decode($request->form);

        return $this->product->getProductsListWithoutDiscounts($data);
    }

    public function addProducts(Request $request, $id)
    {
        $translation = count($request->products) > 1 ? 'multi_attach' : 'attach';
        $this->repository->attachProducts($request->products, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.' . $translation, ['field' => trans('ecommerce::models.of.sale')]),
        ]);
    }

    public function detachProduct(Request $request, $id)
    {
        $this->repository->detachProducts($request->item_id, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.detached', ['field' => trans('ecommerce::models.of.sale')]),
        ]);
    }

    public function detachProducts(Request $request, $id)
    {
        $this->repository->detachProducts(json_decode($request->checked), $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.multi_detached', ['field' => trans('ecommerce::models.of.sale')]),
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Other
    |--------------------------------------------------------------------------
    */

    /**
     * change personal product discount
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function changeDiscount(Request $request, $id)
    {
        $this->repository->changeDiscount($request->all(), $id);

        return response()->json([
            'errors' => false,
            'message' => trans('ecommerce::messages.discount_changed'),
        ]);
    }
}
