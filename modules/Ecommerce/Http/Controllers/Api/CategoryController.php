<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Ecommerce\Entities\Product;
use Modules\Ecommerce\Entities\Category;
use Modules\Ecommerce\Repositories\CategoryRepository;
use Modules\Ecommerce\Transformers\CategoryTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Http\Requests\CreateCategoryRequest;
use Modules\Ecommerce\Http\Requests\UpdateCategoryRequest;
use Modules\Ecommerce\Transformers\FullCategoryTransformer;

class CategoryController extends CoreApiController
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(CategoryRepository $category)
    {
        $this->repository = $category;
        $this->modelInfo = json_decode(file_get_contents(base_path('modules/Ecommerce/Config/models/category.json')));
    }

    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);
            // $request->merge(['addSubCount' => [
            //     'query' => Product::join(with(new Category)->getTable() . 'as subc', function ($query) {
            //         $query->on('subc.id', '=', with(new Product)->getTable() . '.category_id')
            //         ->whereRaw('subc._lft between ' . with(new Category)->getTable() . '._lft + 1 and ' . with(new Category)->getTable() . '._rgt');
            //     }),
            //     'count_name' => 'all_products_count'
            // ]]);
        }

        return CategoryTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $category = $this->repository->find($id);

        return new FullCategoryTransformer($category);
    }

    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->repository->find($id);

        $this->repository->update($category, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.category')]),
            'id' => $category->id
        ]);
    }

    public function store(CreateCategoryRequest $request)
    {
        $category = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.category')]),
            'id' => $category->id
        ]);
    }

    public function destroy($id)
    {
        $category = $this->repository->find($id);

        $this->repository->destroy($category);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.category')]),
        ]);
    }
}
