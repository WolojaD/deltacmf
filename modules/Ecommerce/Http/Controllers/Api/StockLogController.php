<?php

namespace Modules\Ecommerce\Http\Controllers\Api;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Ecommerce\Exports\FullLogExport;
use Modules\Ecommerce\Exports\CurrentDayLogExport;
use Modules\Ecommerce\Exports\TotalByDatesLogExport;
use Modules\Ecommerce\Repositories\StockLogRepository;
use Modules\Ecommerce\Transformers\StockLogTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Ecommerce\Http\Requests\CreateStockLogRequest;
use Modules\Ecommerce\Http\Requests\UpdateStockLogRequest;
use Modules\Ecommerce\Transformers\FullStockLogTransformer;

class StockLogController extends CoreApiController
{
    /**
     * @var StockLogRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(StockLogRepository $stocklog)
    {
        $this->repository = $stocklog;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return StockLogTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: 0]);

            return StockLogTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return StockLogTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return StockLogTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullStockLogTransformer
     */
    public function find($id)
    {
        $stocklog = $this->repository->find($id);

        return new FullStockLogTransformer($stocklog);
    }

    /**
     * @param int $id
     * @param UpdateStockLogRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateStockLogRequest $request)
    {
        $stocklog = $this->repository->find($id);

        $this->repository->update($stocklog, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('ecommerce::models.of.stocklog')]),
            'id' => $stocklog->id
        ]);
    }

    /**
     * @param CreateStockLogRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStockLogRequest $request)
    {
        $stocklog = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('ecommerce::models.of.stocklog')]),
            'id' => $stocklog->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stocklog = $this->repository->find($id);

        $this->repository->destroy($stocklog);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('ecommerce::models.of.stocklog')]),
        ]);
    }

    public function fullLog()
    {
        return $this->repository->getFullLog();
    }

    public function currentDayLog()
    {
        return $this->repository->getCurrentDayLog();
    }

    public function totalByDatesLog()
    {
        return $this->repository->getTotalByDatesLog();
    }

    public function fullExportExcel(Request $request)
    {
        $file_name = 'full_log ' . \Carbon\Carbon::now()->format('d-m-Y H:i:s') . '.xlsx';

        $collection = $this->repository->getFullLog();

        return Excel::download(new FullLogExport($collection), $file_name);
    }

    public function currentDayExportExcel(Request $request)
    {
        $file_name = 'current_day ' . \Carbon\Carbon::now()->format('d-m-Y H:i:s') . '.xlsx';

        $collection = $this->repository->getCurrentDayLog();

        return Excel::download(new CurrentDayLogExport($collection), $file_name);
    }

    public function totalByDatesExportExcel(Request $request)
    {
        $file_name = 'total ' . \Carbon\Carbon::now()->format('d-m-Y H:i:s') . '.xlsx';

        $collection = $this->repository->getTotalByDatesLog();

        return Excel::download(new TotalByDatesLogExport($collection), $file_name);
    }
}
