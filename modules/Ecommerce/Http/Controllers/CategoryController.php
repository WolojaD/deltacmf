<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Ecommerce\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $tail = explode('/', $slug);
        $tail = array_pop($tail);
        $category = $this->category->findBySlug($tail);

        if ($category && $category->path == '/' . $slug) {
            return view('ecommerce::category.show', compact('category'));
        }

        return app('\Modules\Page\Http\Controllers\PublicController')->slug($slug);
    }
}
