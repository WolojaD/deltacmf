<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Ecommerce\Repositories\ProductRepository;
use Modules\Core\Http\Controllers\CorePublicController;

class SearchController extends CorePublicController
{
    public $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $response = [
            'products' => $this->product->frontSearch($request)
        ];

        return $request->ajax() ? response()->json($response) : view('ecommerce::search', $response);
    }
}
