<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Ecommerce\Repositories\SaleRepository;

class SaleController extends Controller
{
    public $sale;

    public function __construct(SaleRepository $sale)
    {
        $this->sale = $sale;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($page, $slug)
    {
        // TODO personal find by slug
        $sale = $this->sale->findBySlug($slug);

        if ($sale) {
            return view('ecommerce::sale.show', compact('sale'));
        }

        abort(404);
    }
}
