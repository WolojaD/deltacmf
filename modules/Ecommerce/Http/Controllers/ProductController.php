<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Ecommerce\Repositories\ProductRepository;

class ProductController extends Controller
{
    public $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        // TODO personal find by slug
        $product = $this->product->findBySlug($slug);

        if ($product) {
            return view('ecommerce::product.show', compact('product'));
        }

        return app(CategoryController::class)->show($slug);
    }
}
