<?php

namespace Modules\Ecommerce\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Ecommerce\Repositories\BrandRepository;

class BrandController extends Controller
{
    public $brand;

    public function __construct(BrandRepository $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($page, $slug)
    {
        // TODO personal find by slug
        $brand = $this->brand->findBySlug($slug);

        if ($brand) {
            return view('ecommerce::brand.show', compact('brand'));
        }

        abort(404);
    }
}
