<?php

namespace Modules\Ecommerce\Events\Handlers;

use Modules\Ecommerce\Entities\StockLog;
use Modules\Ecommerce\Events\StockWasUpdated;

class StockLogger
{
    /**
     * Handle the event.
     *
     * @param  $event
     */
    public function handle(StockWasUpdated $event)
    {
        $user_id = auth()->user()->id;

        StockLog::create([
            'user_id' => $user_id,
            'stock_id' => $event->ident,
            'store_id' => $event->data->store_id,
            'count_before' => $event->data->count_before,
            'count_after' => $event->data->storage_quantity
        ]);
    }
}
