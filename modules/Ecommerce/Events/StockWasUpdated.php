<?php

namespace Modules\Ecommerce\Events;

class StockWasUpdated
{
    /**
     * @var
     */
    public $ident;
    public $data;

    public function __construct($ident, $data)
    {
        $this->ident = $ident;
        $this->data = $data;
    }
}
