<?php

namespace Modules\Ecommerce\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Cache;
use Modules\Ecommerce\Repositories\CategoryRepository;

class NavigationViewComposer
{
    /**
     * @var CategoryRepository
     */
    protected $category;

    /**
     * NavigationViewComposer constructor.
     *
     * @param CategoryRepository $category
     */
    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    public function compose(View $view)
    {
        $view->with('catalog_menu', $this->getNavigation());
    }

    private function getNavigation()
    {
        if (app()->isLocal()) {
            return $this->category->getCategoryMenu();
        } else {
            if (!Cache::has('category_menu')) {
                return $category_menu = Cache::rememberForever('category_menu', function () {
                    return $this->category->getCategoryMenu();
                });
            } else {
                return Cache::get('category_menu');
            }
        }
    }
}
