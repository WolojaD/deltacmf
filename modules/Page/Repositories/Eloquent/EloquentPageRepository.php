<?php

namespace Modules\Page\Repositories\Eloquent;

use Modules\Page\Entities\Page;
use Modules\Seo\Helpers\Templates;
use Modules\Seo\Entities\Structure;
use Modules\Page\Entities\MenuPivot;
use Modules\Page\Entities\MenuPosition;
use Modules\Page\Events\PageIsCreating;
use Modules\Page\Events\PageWasCreated;
use Modules\Page\Events\PageWasUpdated;
use Illuminate\Database\Eloquent\Builder;
use Modules\Page\Repositories\PageRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\Seo\Repositories\Eloquent\EloquentStructureRepository;

class EloquentPageRepository extends EloquentCoreRepository implements PageRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model
     */
    protected $model;

    public $model_config = 'modules/Page/Config/models/page.json';

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->structure = new EloquentStructureRepository(new Structure);
    }

    /**
     * Put here and get all data for homepage
     *
     * @return type
     */
    public function getHomepage()
    {
        $data['page'] = $this->getHomepageData();

        return $data;
    }

    /**
     * Get homepage data
     *
     * @return object
     */
    protected function getHomepageData()
    {
        return $this->model->whereIsRoot()
            ->whereNull('slug')
            ->orWhere('slug', '')
            ->orWhere('slug', env('APP_URL'))
            // ->withTranslation()
            ->firstOrFail();
    }

    /**
     * Find the page set as homepage.
     *
     * @return object
     */
    public function findHomepage()
    {
        return $this->model->where('is_home', 1)->first();
    }

    /**
     * @param string $path
     *
     * @return \Modules\Page\Entities\Page
     */
    public function findByPath($path)
    {
        return $this->model->findByPath($path)->first();
    }

    /**
     * @param string $slug
     * @param string $locale
     *
     * @return object
     */
    public function findBySlugInLocale($slug, $locale)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                $q->where('slug', $slug);
                $q->where('locale', $locale);
            })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }

    /**
     * @param string $slug
     *
     * @return type
     */
    public function findBySlug($slug)
    {
        return $this->model
            ->where('slug', $slug)
            ->with('translations')
            ->firstOrFail();
    }

    /**
     * @param string $slugs
     *
     * @return type
     */
    public function findBySlugs($slugs)
    {
        return $this->model->withoutGlobalScope(OrderScope::class)
            ->whereIn('slug', $slugs)
            ->orderBy('depth', 'desc')
            ->first();
    }

    /**
     * Count all records.
     *
     * @return int
     */
    public function countAll()
    {
        return $this->model->count();
    }

    /**
     * @param mixed $data
     *
     * @return object
     */
    public function create($data)
    {
        if (!app()->runningInConsole() && $data['parent_id'] == 0) {
            return false;
        }

        event($event = new PageIsCreating($data));

        $attributes = $event->getAttributes();

        $menuPositionList = $attributes['menuPositions'] ?? [];

        if (isset($attributes['menuPositions'])) {
            unset($attributes['menuPositions']);
        }

        $page_template = $attributes['page_template'];

        if (is_array($page_template)) {
            $attributes['page_template'] = $page_template[0];
            $attributes['relation'] = $page_template[1] ?? null;
        } else {
            $attributes['page_template'] = $page_template;
        }

        $page = $this->model->create($attributes);

        $page->menuPositions()->delete();

        foreach ($menuPositionList as $menuPosition) {
            $id = array_search($menuPosition, (new MenuPosition())->lists());

            if ($id) {
                MenuPivot::create([
                    'menu_id' => $id,
                    'page_id' => $page->id,
                ]);
            }
        }

        event(new PageWasCreated($page, $data));

        return $page;
    }

    /**
     * @param $model
     * @param array $data
     *
     * @return object
     */
    public function update($model, $data)
    {
        if (!app()->runningInConsole() && $model->getOriginal('parent_id') == 0 && $data['parent_id'] == 0) {
            return false;
        }

        $menuPositionList = $data['menuPositions'] ?? [];

        if (isset($data['menuPositions'])) {
            unset($data['menuPositions']);
        }

        if (in_array((int) ($data['id'] ?? $model->id), [1])) {
            $data['slug'] = '';
        }

        $page_template = $data['page_template'] ?? $model->page_template;

        if (is_array($page_template)) {
            $data['page_template'] = $page_template[0];
            $data['relation'] = $page_template[1] ?? null;
        } else {
            $data['page_template'] = $page_template;
        }

        parent::update($model, $data);

        $model->menuPositions()->delete();

        foreach ($menuPositionList as $menuPosition) {
            $id = array_search($menuPosition, (new MenuPosition())->lists());

            if ($id) {
                MenuPivot::create([
                    'menu_id' => $id,
                    'page_id' => $model->id,
                ]);
            }
        }

        event(new PageWasUpdated($model, $data));

        return $model;
    }

    /**
     * Data generation to show in view blade
     *
     * @param [type] $structure
     *
     * @return [type]
     */
    public function getViewData($structure)
    {
        if ($structure->page_id) {
            return [
                'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
                'structure' => $structure,
                // 'header_menu' => $this->getPagesWithMenuPositions(MenuPosition::HEADER, $structure->page_id)
            ];
        }

        return [];
    }

    /**
     * @param int $menu_id
     * @param int|null $parent_id
     *
     * @return array|\Modules\Page\Repositories\type
     */
    public function getPagesWithMenuPositions($menu_id, $parent_id = null)
    {
        $structure_table = with(new Structure)->getTable();

        $data = $this->model
            ->whereHas('menuPositions', function ($query) use ($menu_id) {
                $query->where('menu_id', $menu_id);
            })
            ->join($structure_table, $this->model->getTable() . '.id', '=', $structure_table . '.page_id')
            ->with('translations')
            ->noStore()
            ->orderBy('order')
            ->get();

        $menu_settings = $this->getMenuSettings();

        $data = $this->buildTree($data, $parent_id ?? $data->min('parent_id'), 0, $menu_settings['max_level']);

        return $data;
    }

    /**
     * @param $data
     * @param int $parent
     * @param int $level
     * @param null $max_level
     *
     * @return array
     */
    public function buildTree(&$data, $parent = 0, $level = 0, $max_level = null)
    {
        $tree = [];
        $level++;

        foreach ($data as $id => $node) {
            if ($node->parent_id == $parent) {
                unset($data[$id]);
                $node->level = $level;
                $node->childs = null != $max_level && ($level >= $max_level) ? [] : $this->buildTree($data, $node->id, $level, $max_level);
                $tree[] = $node;
            }
        }

        return $tree;
    }

    /**
     * @return array
     */
    public function getMenuSettings()
    {
        return [
            'max_level' => settings('page::page_menu_max_level', null, '') != '' ? (int) settings('page::page_menu_max_level') : null
        ];
    }

    public function templates()
    {
        return (new Templates())->getPageList();
    }

    public function showPageTemplate()
    {
        $exploded = array_reverse(explode('/', trim(request()->headers->get('referer'), '/')));

        return $this->model->where('parent_id', $exploded[1])->where('page_template', '!=', 'default')->count() === 0;
    }
}
