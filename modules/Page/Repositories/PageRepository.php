<?php

namespace Modules\Page\Repositories;

use Modules\Page\Entities\Page;
use Modules\Core\Repositories\CoreInterface;

interface PageRepository extends CoreInterface
{
    /**
     * Find the page set as homepage.
     *
     * @return object
     */
    public function findHomepage();

    /**
     * @param string $path
     *
     * @return \Modules\Page\Entities\Page
     */
    public function findByPath($path);

    /**
     * @param $slug
     * @param $locale
     *
     * @return object
     */
    public function findBySlugInLocale($slug, $locale);

    /**
     * @param string $slug
     *
     * @return type
     */
    public function findBySlug($slug);

    /**
     * @param string $slugs
     *
     * @return type
     */
    public function findBySlugs($slugs);

    /**
     * Count all records.
     *
     * @return int
     */
    public function countAll();

    /**
     * Data generation to show in view blade
     *
     * @param [type] $structure
     *
     * @return [type]
     */
    public function getViewData($structure);

    /**
     * @param int $menu_id
     * @param int|null $parent_id
     *
     * @return array|\Modules\Page\Repositories\type
     */
    public function getPagesWithMenuPositions($menu_id, $parent_id = null);

    /**
     * @param $data
     * @param int $parent
     * @param int $level
     * @param null $max_level
     *
     * @return array
     */
    public function buildTree(&$data, $parent = 0, $level = 0, $max_level = null);
}
