<?php

namespace Modules\Page\Repositories\Cache;

use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CachePageDecorator extends CoreCacheDecorator implements PageRepository
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(PageRepository $page)
    {
        parent::__construct();

        $this->entityName = 'pages';
        $this->repository = $page;
    }

    /**
     * Find the page set as homepage.
     *
     * @return object
     */
    public function findHomepage()
    {
        return $this->remember(function () {
            return $this->repository->findHomepage();
        });
    }

    /**
     * @param string $path
     *
     * @return \Modules\Page\Entities\Page
     */
    public function findByPath($path)
    {
        return $this->repository->findByPath($path);
    }

    /**
     * @param $slug
     * @param $locale
     *
     * @return object
     */
    public function findBySlugInLocale($slug, $locale)
    {
        return $this->repository->findBySlugInLocale($slug, $locale);
    }

    /**
     * @param string $slug
     *
     * @return type
     */
    public function findBySlug($slug)
    {
        return $this->repository->findBySlug($slug);
    }

    /**
     * @param string $slugs
     *
     * @return type
     */
    public function findBySlugs($slugs)
    {
        return $this->repository->findBySlugs($slugs);
    }

    /**
     * Count all records.
     *
     * @return int
     */
    public function countAll()
    {
        return $this->remember(function () {
            return $this->repository->countAll();
        });
    }

    /**
     * Data generation to show in view blade
     *
     * @param [type] $structure
     *
     * @return [type]
     */
    public function getViewData($structure)
    {
        return $this->remember(function () use ($structure) {
            return $this->repository->getViewData($structure);
        });
    }

    /**
     * @param int $menu_id
     * @param int|null $parent_id
     *
     * @return array|\Modules\Page\Repositories\type
     */
    public function getPagesWithMenuPositions($menu_id, $parent_id = null)
    {
        // cache()->tags($this->entityName)->flush();

        return $this->remember(function () use ($menu_id, $parent_id) {
            return $this->repository->getPagesWithMenuPositions($menu_id, $parent_id);
        });
    }

    /**
     * @param $data
     * @param int $parent
     * @param int $level
     * @param null $max_level
     *
     * @return array
     */
    public function buildTree(&$data, $parent = 0, $level = 0, $max_level = null)
    {
        return $this->remember(function () use ($data, $parent, $level, $max_level) {
            return $this->repository->buildTree($data, $parent, $level, $max_level);
        });
    }
}
