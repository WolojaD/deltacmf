<?php

namespace Modules\Page\Traits;

use Modules\Page\Entities\Page;

trait Attached
{
    /**
     * Get all of the pages for the block.
     */
    public function pages()
    {
        return $this->morphToMany(Page::class, 'attached');
    }

    /**
     * Attach to page.
     *
     * @return Model
     */
    public function attachToPage($page_id)
    {
        $attributes = ['page_id' => $page_id];

        if (!$this->pages()->where($attributes)->exists()) {
            return $this->pages()->create($attributes);
        }
    }

    /**
     * Attach to pages.
     *
     * @return \Illuminate\Support\Collection
     */
    public function attachToPages($page_ids)
    {
        foreach ($page_ids as $page_id) {
            $result[] = $this->attachToPage($page_id);
        }

        return collect($result ?? []);
    }

    /**
     * get all items attached to page.
     *
     * @return Model
     */
    public function attachedToPage($page_id)
    {
        return $this->pages()->where(['page_id' => $page_id])->get();
    }

    /**
     * Get the number of attached items for the item.
     *
     * @return int
     */
    public function getAttachedCountAttribute()
    {
        return $this->attached->count();
    }
}
