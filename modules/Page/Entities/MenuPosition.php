<?php

namespace Modules\Page\Entities;

class MenuPosition
{
    const MAIN = 1;
    const HEADER = 2;
    const FOOTER = 3;

    /**
     * @var array
     */
    private $positions = [];

    public function __construct()
    {
        $this->positions = [
            self::MAIN => trans('page::menuPosition.main'),
            self::HEADER => trans('page::menuPosition.header'),
            self::FOOTER => trans('page::menuPosition.footer'),
        ];
    }

    /**
     * Get the available positions.
     *
     * @return array
     */
    public function lists()
    {
        return $this->positions;
    }

    /**
     * Get the post status.
     *
     * @param int $positionId
     *
     * @return string
     */
    public function get($positionId)
    {
        if (isset($this->positions[$positionId])) {
            return $this->positions[$positionId];
        }

        return $this->positions[self::DRAFT];
    }

    /**
     * Get the post status.
     *
     * @param int $positionId
     *
     * @return string
     */
    public function getList(array $positionIds)
    {
        foreach ($positionIds as $positionId) {
            if (isset($this->positions[$positionId])) {
                $result[] = $this->positions[$positionId];
            }
        }

        return $result ?? [];
    }
}
