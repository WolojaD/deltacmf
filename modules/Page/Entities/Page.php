<?php

namespace Modules\Page\Entities;

use Laravel\Scout\Searchable;
use Kalnoy\Nestedset\NodeTrait;
use Modules\Core\Eloquent\Model;
use Modules\Core\Eloquent\Builder;
use Modules\Seo\Helpers\Templates;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Seo\Helpers\StructureSync;
use Modules\Core\Eloquent\NestedBuilder;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;

class Page extends Model implements SeoSyncInterface
{
    use Translatable,
        SeoSyncTrait,
        Searchable,
        NodeTrait {
            NodeTrait::usesSoftDelete insteadof Searchable;
        }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $asYouType = true;

    protected $table = 'pages';
    protected $translations_module = 'page';
    public $translatedAttributes = [
        'page_id',
        'title',
        'menu_title',
        'body',
    ];
    protected $fillable = [
        'page_template',
        'page_id',
        'title',
        'menu_title',
        'slug',
        'relation',
        'status',
        'body',
        'parent_id',
        'depth',
        'order',
        'mobile_image',
        'desktop_image',
        'menuPositions',
    ];
    protected $searchable = [
        'title',
        'slug',
    ];
    protected $casts = [
        'status' => 'integer',
    ];
    protected static $entityNamespace = 'application/page';

    protected $file_path = 'modules/Page/Config/models/page.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($obj) {
            if ($obj->parent_id == 0) {
                return false;
            }

            $obj->children->each(function ($child) {
                $child->delete();
            });
        });
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['translations']);

        if ($this->translatedAttributes) {
            foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                foreach ($this->translatedAttributes as $translatedAttribute) {
                    $array[$translatedAttribute . '_' . $locale] = $this->translateOrNew($locale)->$translatedAttribute;
                }
            }
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    public function getEditUrl($locale): string
    {
        if ($locale == 'ru') {
            return url('/') . '/' . config('application.core.core.admin-prefix', 'backend') . '/pages/' . $this->id . '/edit/';
        }

        return url('/') . '/' . $locale . '/' . config('application.core.core.admin-prefix', 'backend') . '/pages/' . $this->id . '/edit/';
    }

    /**
    * Method allows to show page template column in form
    *
    * @return void
    */
    public function showPageTemplate()
    {
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function menuPositions()
    {
        return $this->hasMany(MenuPivot::class, 'page_id', 'id');
    }

    public function structure()
    {
        return $this->hasOne(Structure::class, 'page_id')->with('translations');
    }

    public function children()
    {
        return $this->hasMany(Page::class, 'parent_id')->with('children', 'translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeNoStore($q)
    {
        return $q->where($this->getTable() . '.slug', '!=', 'fantom_storepage_u_cant_see');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getMenusAttribute()
    {
        $list = (new MenuPosition())->lists();

        return $this->menuPositions->reduce(function ($carry, $position) use ($list) {
            return $carry ? $carry . ', ' . $list[$position->menu_id] : $list[$position->menu_id];
        });
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */
    public function parentStructure()
    {
        return Structure::where('page_id', $this->parent_id ?? null)
            ->where('model', self::class)
            ->first();
    }

    public static function seoAddChildrenToStructure($structure)
    {
        $model = (new Templates($structure->page_template))->getModel();
        $model::when((new $model)->isNested(), function ($q) use ($structure) {
            if ($structure->page_template == 'products') {
                $q->where('parent_id', $structure->object_id ?? null);
            } else {
                $q->where('parent_id', $structure->object_id ?? -1);
            }
        })
        ->get()
        ->each(function ($item) {
            (new StructureSync($item))->create();
        });
    }

    /**
     * Changes builder to custom
     *
     * @param [type] $query
     *
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new NestedBuilder($query);
    }
}
