<?php

namespace Modules\Page\Events;

use Modules\Page\Entities\Page;
use Modules\Core\Events\AbstractEntityHook;
use Modules\Core\Contracts\EntityIsChanging;

class PageIsUpdating extends AbstractEntityHook implements EntityIsChanging
{
    /**
     * @var Page
     */
    private $page;

    public function __construct(Page $page, array $attributes)
    {
        $this->page = $page;

        parent::__construct($attributes);
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }
}
