<?php

namespace Modules\Page\Composers;

use Illuminate\View\View;
use Modules\Page\Entities\MenuPosition;
use Modules\Page\Repositories\PageRepository;

class NavigationViewComposer
{
    /**
     * The page repository implementation.
     *
     * @var PageRepository
     */
    protected $page;

    /**
     * Create a new profile composer.
     *
     * @param  PageRepository  $page
     * @return void
     */
    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function compose(View $view)
    {
        $view->with('navigation', $this->getNavigation());
    }

    private function getNavigation()
    {
        return $analytic_scripts = [
            'main_menu' => $this->getMainMenu(),
            'footer_menu' => $this->getFooterMenu(),
            'header_menu' => $this->getHeaderMenu(),
        ];
    }

    private function getMainMenu()
    {
        return $this->page->getPagesWithMenuPositions(MenuPosition::MAIN);
    }

    private function getHeaderMenu()
    {
        return $this->page->getPagesWithMenuPositions(MenuPosition::HEADER);
    }

    private function getFooterMenu()
    {
        return $this->page->getPagesWithMenuPositions(MenuPosition::FOOTER);
    }
}
