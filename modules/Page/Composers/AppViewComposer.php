<?php

namespace Modules\Page\Composers;

use Illuminate\View\View;

class AppViewComposer
{
    /**
     * @param View $view
     *
     * @return type
     */
    public function compose(View $view)
    {
        $view->with('analytic_scripts', $this->getAnalyticScripts());
    }

    /**
     * Read template name defined in comments.
     *
     * @param $template
     *
     * @return string
     */
    private function getAnalyticScripts()
    {
        return $analytic_scripts = [
            'analytic_script_head' => cache()->tags(['application.settings', 'global'])->get('analytic_script_head'),
            'analytic_script_body_begin' => cache()->tags(['application.settings', 'global'])->get('analytic_script_body_begin'),
            'analytic_script_body_end' => cache()->tags(['application.settings', 'global'])->get('analytic_script_body_end'),
        ];
    }
}
