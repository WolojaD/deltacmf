<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Page\Entities\Page;
use Illuminate\Support\Facades\DB;
use Modules\Articles\Entities\Category;
use Modules\Page\Repositories\PageRepository;

class PageDatabaseSeeder extends Seeder
{
    /**
     * @var PageRepository
     */
    private $page;

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function run()
    {
        $data = [];

        foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
            $data[$key] = [
                'title' => 'Главная',
                'menu_title' => 'Главная',
                'body' => '',
            ];
        }

        factory(Page::class)->states('is_home')->create($data);

        unset($data);

        if (!app()->environment('production')) {
            $category = Category::whereIsRoot()->first();
            $categories = Category::where('parent_id', $category->id)->select('id')->limit(2)->get()->pluck('id')->toArray();

            $this->generatePages();

            $data = ['slug' => 'articles', 'page_template' => 'articles', 'relation' => $categories[0] ?? 1, 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Статьи',
                    'menu_title' => 'Статьи',
                    'body' => '',
                ];
            }
            factory(Page::class)->create($data);
            sleep(6);

            $data = ['slug' => 'news', 'page_template' => 'news', 'relation' => $categories[1] ?? 1, 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Новости',
                    'menu_title' => 'Новости',
                    'body' => '',
                ];
            }

            factory(Page::class)->create($data);

            $data = ['slug' => 'brands', 'page_template' => 'brands', 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Бренды',
                    'menu_title' => 'Бренды',
                    'body' => '',
                ];
            }

            factory(Page::class)->create($data);

            $data = ['slug' => 'sales', 'page_template' => 'sales', 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Распродажи',
                    'menu_title' => 'Распродажи',
                    'body' => '',
                ];
            }

            factory(Page::class)->create($data);

            $data = ['slug' => 'top_products', 'page_template' => 'top_products', 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Топ продаж',
                    'menu_title' => 'Топ продаж',
                    'body' => '',
                ];
            }

            factory(Page::class)->create($data);

            $data = ['slug' => 'new_products', 'page_template' => 'new_products', 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Новинки',
                    'menu_title' => 'Новинки',
                    'body' => '',
                ];
            }
            factory(Page::class)->create($data);

            $data = ['slug' => 'discounted_products', 'page_template' => 'discounted_products', 'parent_id' => 2];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Все товары со скидкой',
                    'menu_title' => 'Все товары со скидкой',
                    'body' => '',
                ];
            }

            factory(Page::class)->create($data);

            factory(Page::class)->create([
                'slug' => 'fantom_storepage_u_cant_see',
                'status' => 1,
                'page_template' => 'products',
                'parent_id' => 1]);

            $pages = Page::whereIsLeaf()->noStore()->get();

            foreach ($pages as $page) {
                DB::table('page_menu_pivots')->insert(
                    [
                        'page_id' => $page->id,
                        'menu_id' => 3,
                    ]
                );
                DB::table('page_menu_pivots')->insert(
                    [
                        'page_id' => $page->id,
                        'menu_id' => 2,
                    ]
                );
                DB::table('page_menu_pivots')->insert(
                    [
                        'page_id' => $page->id,
                        'menu_id' => 1,
                    ]
                );
            }
        }
    }

    public function generatePages()
    {
        for ($i = 1; $i < 7; $i++) {
            $data = [
                'page_template' => 'default',
                'parent_id' => 1,
                'status' => 1,
                'slug' => 'stranica-' . $i,
            ];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'Страница ' . $i,
                    'menu_title' => 'Страница ' . $i,
                    'body' => '',
                ];
            }

            $page = Page::create($data);
            unset($data);

            $j_max = rand(3, 6);

            for ($j = 1; $j < $j_max; $j++) {
                $data = [
                    'page_template' => 'default',
                    'parent_id' => $page->id,
                    'status' => 1,
                    'slug' => 'podstranica-' . $j . '-stranicy-' . $i,
                ];

                foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                    $data[$key] = [
                        'title' => 'Подстраница ' . $j . ' страницы' . $i,
                        'menu_title' => 'Подстраница ' . $j . ' страницы' . $i,
                        'body' => '',
                    ];
                }

                $sub_page = Page::create($data);
                unset($data);

                if (rand(0, 1)) {
                    continue;
                }

                $k_max = rand(2, 4);

                for ($k = 1; $k < $k_max; $k++) {
                    $data = [
                        'page_template' => 'default',
                        'parent_id' => $sub_page->id,
                        'status' => 1,
                        'slug' => 'podpodstranica-' . $k . '-podstranicy-' . $j . '-stranicy-' . $i,
                    ];

                    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                        $data[$key] = [
                            'title' => 'Подподстраница ' . $k . ' подстраницы ' . $j . ' страницы' . $i,
                            'menu_title' => 'Подподстраница ' . $k . ' подстраницы ' . $j . ' страницы' . $i,
                            'body' => '',
                        ];
                    }

                    Page::create($data);
                    unset($data);
                }
            }
        }
    }
}
