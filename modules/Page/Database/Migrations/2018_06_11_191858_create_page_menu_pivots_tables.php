<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageMenuPivotsTables extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('page_menu_pivots', function (Blueprint $table) {
            $table->unsignedInteger('page_id');
            $table->unsignedInteger('menu_id');
            $table->nullableTimestamps();
            $table->primary(['page_id', 'menu_id']);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('page_menu_pivots');
    }
}
