<?php

use Faker\Generator as Faker;
use Modules\Page\Entities\Page;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Page::class, function (Faker $faker) {
    $data = [
        'page_template' => 'default',
        'parent_id' => 1,
        'status' => $faker->numberBetween(0, 1),
        'slug' => $faker->slug,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->word,
            'menu_title' => $faker->word,
            'body' => $faker->randomHtml(2, 3),
        ];
    }

    return $data;
});

$factory->state(Page::class, 'is_home', function (Faker $faker) {
    $data = [
        'slug' => '',
        'depth' => 1,
        'status' => 1,
        'parent_id' => null
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->word,
            'menu_title' => $faker->word,
        ];
    }

    return $data;
});

$factory->state(Page::class, 'online', [
    'status' => 1,
]);

$factory->state(Page::class, 'offline', [
    'status' => 0,
]);

$factory->state(Page::class, 'testing', [
    'status' => 2,
]);
