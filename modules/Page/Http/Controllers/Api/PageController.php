<?php

namespace Modules\Page\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Page\Entities\MenuPosition;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Transformers\PageTransformer;
use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Transformers\FullPageTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class PageController extends CoreApiController
{
    /**
     * @var PageRepository
     */
    protected $repository;

    /**
     * @var MenuPosition
     */
    protected $menuPosition;

    public function __construct(PageRepository $page, MenuPosition $menuPosition)
    {
        $this->repository = $page;
        $this->menuPosition = $menuPosition;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = 0)
    {
        $request->merge(['pageId' => $id ?: null]);
        $request->merge(['with' => 'menuPositions']);
        $request->merge(['where' => [['slug', '!=', 'fantom_storepage_u_cant_see']]]);

        return PageTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $page = $this->repository->find($id);

        return new FullPageTransformer($page);
    }

    public function update($id, UpdatePageRequest $request)
    {
        $page = $this->repository->find($id);

        if ($this->repository->update($page, $request->all())) {
            return response()->json([
                'errors' => false,
                'message' => trans('page::messages.api.page updated'),
                'id' => $page->id,
            ]);
        }

        return response()->json([
            'errors' => true,
            'message' => trans('page::messages.api.page disable'),
        ]);
    }

    public function store(CreatePageRequest $request)
    {
        $page = $this->repository->create($request->except(0));

        return response()->json([
            'errors' => false,
            'message' => trans('page::messages.api.page created'),
            'id' => $page->id,
        ]);
    }

    public function destroy($id)
    {
        if (in_array((int) $id, [1])) {
            return response()->json([
                'errors' => true,
                'message' => trans('page::messages.api.page disable'),
            ]);
        }

        $page = $this->repository->find($id);

        if ($this->repository->destroy($page)) {
            return response()->json([
                'errors' => false,
                'message' => trans('page::messages.api.page deleted'),
                'id' => $page->id,
            ]);
        }

        return response()->json([
            'errors' => true,
            'message' => trans('page::messages.api.page disable'),
        ]);
    }
}
