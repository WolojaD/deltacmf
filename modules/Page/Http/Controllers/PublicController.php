<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Page\Entities\Page;
use Modules\Seo\Helpers\Templates;
use Webwizo\Shortcodes\Facades\Shortcode;
use Modules\Core\Helpers\FrontendPaginator;
use Modules\Page\Repositories\PageRepository;
use Illuminate\Contracts\Foundation\Application;
use Modules\Seo\Repositories\StructureInterface;
use Modules\Core\Http\Controllers\CorePublicController;

class PublicController extends CorePublicController
{
    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var Application
     */
    private $app;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var StructureInterface
     */
    protected $structure;

    public function __construct(PageRepository $page, Application $app, Request $request, StructureInterface $structure)
    {
        parent::__construct();

        $this->page = $page;
        $this->app = $app;
        $this->request = $request;
        $this->structure = $structure;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage()
    {
        Shortcode::enable();

        list($structure, $filterable) = $this->structure->findByPath('');

        if (!$structure) {
            abort(404);
        }

        $template = (new Templates())->getTemplate($structure->page_template);

        $repository = new $template['repository'](new $template['model']);
        $result = $repository->getViewData($structure);

        $result['paginated'] = false;

        return view('page::index', $result)->withShortcodes();
    }

    /**
     * gets slug and pass it to needed places
     * @param $slug
     *
     * @return mixed
     */
    public function show($slug = '')
    {
        preg_match('/page-(\d+)$/', $slug, $pagination);
        $paginated = false;

        if (isset($pagination[1]) && $pagination[1] > 1) {
            request()->merge(['page' => $pagination[1]]);
            $slug = str_replace('/' . $pagination[0], '', $slug);
            $paginated = true;
        } elseif (isset($pagination[1])) {
            abort(404);
        }

        Shortcode::enable();

        list($structure, $sub_path) = $this->structure->findByPath($slug);

        if (!$structure) {
            abort(404);
        }

        if ($paginated) {
            $structure->paginationPage = $pagination[1];
        }

        $template = (new Templates())->getTemplate($structure->page_template);

        $repository = new $template['repository'](new $template['model']);
        $result = $repository->getViewData($structure, $paginated, $sub_path);
        $pageable = false;

        foreach ($result as $item) {
            if ($item instanceof FrontendPaginator) {
                $pageable = true;
            }
        }

        $result['paginated'] = $paginated;

        if (!$pageable && $paginated) {
            abort(404);
        }

        return view('page::' . $this->getViewTemplate($structure->page_template), $result)->withShortcodes();
    }

    /**
     * Return the template for the given page
     * or the default template if none found.
     *
     * @return string
     */
    public function getViewTemplate($page_template)
    {
        return view()->exists('page::' . $page_template) ? $page_template : 'default';
    }

    /**
     * Return the method for the given page
     * or the default method if none found.
     *
     * @param $page
     *
     * @return string
     */
    private function getMethodForPage($page)
    {
        return method_exists($this, $page->page_template) ? $page->page_template : 'default';
    }
}
