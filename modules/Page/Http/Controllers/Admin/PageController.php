<?php

namespace Modules\Page\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class PageController extends CoreAdminController
{
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('core::router_view');
    }
}
