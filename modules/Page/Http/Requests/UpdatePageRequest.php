<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePageRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $required = 0 == $this->parent_id ? '' : 'required|';

        return [
            'page_template' => 'required',
            'slug' => $required . 'max:191|unique:pages,slug,' . $this->id,
        ];
    }

    public function translationRules()
    {
        return [
            'title' => 'required|max:255',
            'og_title' => 'max:255',
            'meta_title' => 'max:255',
            'og_description' => 'max:500',
            'meta_description' => 'max:500',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
