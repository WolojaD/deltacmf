<?php

namespace Modules\Page\Providers;

use Modules\Page\Entities\Page;
use Illuminate\Support\Facades\Config;
use Modules\Page\Services\FinderService;
use Illuminate\Database\Eloquent\Factory;
use Modules\Core\Providers\ServiceProvider;
use Modules\Page\Repositories\PageRepository;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Page\Repositories\Cache\CachePageDecorator;
use Modules\Page\Repositories\Eloquent\EloquentPageRepository;

class PageServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('page_sidebar', array_dot(trans('page::sidebar')));
            $event->load('page_table-fields', array_dot(trans('page::table-fields')));
            $event->load('page_permissions', array_dot(trans('page::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('page', 'config');
        $this->publishConfig('page', 'permissions');
        $this->publishConfig('page', 'template_to_controller');
        $this->publishConfig('page', 'settings');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->bind(FinderService::class, function () {
            return new FinderService();
        });

        $this->app->bind(PageRepository::class, function () {
            $repository = new EloquentPageRepository(new Page());

            if (!Config::get('app.cache')) {
                return $repository;
            }

            return new CachePageDecorator($repository);
        });
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        app(Factory::class)->load(__DIR__ . '/../Database/factories/');
    }
}
