<?php

namespace Modules\Page\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class PageTransformer extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'is_home' => $this->is_home,
            'page_template' => $this->page_template,
            'slug' => $this->slug ?: url('/'),
            'status' => $this->status,
            'menus' => $this->menus,
            'path' => $this->path,
            'children_count' => $this->children()->noStore()->count() ?? null,
            'parent_id' => $this->parent_id ?? null,
            'order' => $this->order ?? 0,
            'depth' => $this->depth ?? 0,
            'created_at' => $this->created_at->toRfc2822String(),
            'title' => $this->defaultTranslate()->title,
        ];
    }
}
