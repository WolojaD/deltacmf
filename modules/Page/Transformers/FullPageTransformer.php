<?php

namespace Modules\Page\Transformers;

use Modules\Page\Entities\MenuPosition;
use Illuminate\Http\Resources\Json\Resource;

class FullPageTransformer extends Resource
{
    public function toArray($request)
    {
        $pageData = [
            'id' => $this->id,
            'page_template' => [$this->page_template, $this->relation],
            'is_home' => $this->is_home,
            'slug' => $this->slug,
            'path' => $this->path,
            'status' => $this->status,
            'parent_id' => $this->parent_id,
            'depth' => $this->depth,
            'order' => $this->order,
            'mobile_image' => $this->mobile_image,
            'desktop_image' => $this->desktop_image,
            'status' => $this->status,
            'menuPositions' => (new MenuPosition())->getList($this->menuPositions()->pluck('menu_id')->toArray()),
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $pageData[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $pageData[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $pageData;
    }
}
