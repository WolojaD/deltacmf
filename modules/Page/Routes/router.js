import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/pages',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.page_page',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Pages',
                    breadcrumb: [
                        { name: 'Pages' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.page_page.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Pages',
                    breadcrumb: [
                        { name: 'Pages' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.page_page.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Page Create',
                    breadcrumb: [
                        { name: 'Pages', link: 'api.backend.page_page' },
                        { name: 'Page Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.page_page.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Page Edit',
                    breadcrumb: [
                        { name: 'Pages', link: 'api.backend.page_page' },
                        { name: 'Page Edit' }
                    ]
                }
            }
        ]
    }
]
