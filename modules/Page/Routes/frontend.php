<?php

/*
|--------------------------------------------------------------------------
| FrontEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register FrontEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "frontend" middleware group. Now create something great!
|
*/

Route::get('/', ['middleware' => config('application.page.config.middleware'), 'as' => 'homepage', 'uses' => 'PublicController@homepage']);

Route::get('{slug?}', ['middleware' => config('application.page.config.middleware'), 'as' => 'page', 'uses' => 'PublicController@show'])->where(['slug' => '.*']);
