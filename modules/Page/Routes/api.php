<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Page');

Route::group(['prefix' => '/pages', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::post('text-editor-upload', ['as' => 'api.backend.page_page.textEditorUpload', 'uses' => 'PageController@textEditorUpload', 'middleware' => 'token-can:page.page.edit']);
});
