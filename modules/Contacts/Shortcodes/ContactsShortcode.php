<?php

namespace Modules\Contacts\Shortcodes;

use Illuminate\Support\Facades\View;
use Modules\Contacts\Entities\Group;

class ContactsShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        $contacts_group = Group::where('short_code', $shortcode->code)
            ->with('contacts', 'translations')->first();

        if (null === $contacts_group) {
            return '';
        }

        if ($contacts_group) {
            return $contacts_group->contacts->count() ? view('contacts::' . $contacts_group->contacts_template, compact('contacts_group')) : '';
        } else {
            return '';
        }
    }
}
