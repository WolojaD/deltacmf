<?php

namespace Modules\Contacts\Repositories\Cache;

use Modules\Contacts\Repositories\GroupInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheGroupDecorator extends CoreCacheDecorator implements GroupInterface
{
    /**
     * @var GroupInterface
     */
    protected $repository;

    public function __construct(GroupInterface $contacts)
    {
        parent::__construct();

        $this->entityName = 'contacts_groups';
        $this->repository = $contacts;
    }
}
