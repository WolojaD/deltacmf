<?php

namespace Modules\Contacts\Repositories\Cache;

use Modules\Contacts\Repositories\ContactInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheContactDecorator extends CoreCacheDecorator implements ContactInterface
{
    /**
     * @var ContactInterface
     */
    protected $repository;

    public function __construct(ContactInterface $contacts)
    {
        parent::__construct();

        $this->entityName = 'contacts';
        $this->repository = $contacts;
    }
}
