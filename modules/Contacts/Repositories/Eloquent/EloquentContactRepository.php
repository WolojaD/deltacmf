<?php

namespace Modules\Contacts\Repositories\Eloquent;

use Modules\Contacts\Entities\Group;
use Modules\Contacts\Repositories\ContactInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentContactRepository extends EloquentCoreRepository implements ContactInterface
{
    public $model_config = 'modules/Contacts/Config/models/contact.json';

    protected $groups;

    public function __construct($model)
    {
        parent::__construct($model);

        $this->groups = new EloquentGroupRepository(new Group);
    }

    public function groups()
    {
        return $this->groups->all()->pluck('title', 'id')->toArray();
    }

    protected function getFieldTypeValues()
    {
        return config('application.contacts.fields-type.field_type');
    }
}
