<?php

namespace Modules\Contacts\Repositories\Eloquent;

use Modules\Contacts\Repositories\GroupInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentGroupRepository extends EloquentCoreRepository implements GroupInterface
{
    public $model_config = 'modules/Contacts/Config/models/group.json';
}
