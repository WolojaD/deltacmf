<?php

namespace Modules\Contacts\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Group extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contacts_groups';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/contacts';

    protected $file_path = 'modules/Contacts/Config/models/group.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        static::deleting(function ($obj) {
            $obj->contacts->each(function ($contact) {
                $contact->delete();
            });
        });

        parent::boot();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contacts()
    {
        return $this->hasMany(Contact::class)->with('translations')->orderBy('order');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
