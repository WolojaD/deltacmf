<?php

use Faker\Generator as Faker;
use Modules\Contacts\Entities\Group;
use Modules\Contacts\Entities\Contact;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Group::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'show_title' => $faker->numberBetween(0, 1),
        'comment' => $faker->numberBetween(0, 1) == 1 ? $faker->sentence : null,
        'short_code' => $faker->lexify('?????????'),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
        ];
    }

    return $data;
});

$factory->define(Contact::class, function (Faker $faker) {
    static $number = 1;

    $data = [
        'status' => $faker->numberBetween(0, 1),
        'class' => $faker->numberBetween(0, 1) == 1 ? $faker->word : null,
        'group_id' => 1,
        'order' => $number++,
    ];

    return $data;
});

$factory->state(Contact::class, 'link', function (Faker $faker) {
    $data = [
        'field_type' => 'link',
        'url' => $faker->url,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'body' => $faker->name,
        ];
    }

    return $data;
});

$factory->state(Contact::class, 'other', function (Faker $faker) {
    $data = [
        'field_type' => 'other',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'body' => $faker->name,
        ];
    }

    return $data;
});

$factory->state(Contact::class, 'phone', function (Faker $faker) {
    $data = [
        'field_type' => 'phone',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'body' => $faker->numerify('############'),
        ];
    }

    return $data;
});

$factory->state(Contact::class, 'email', function (Faker $faker) {
    $data = [
        'field_type' => 'email',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'body' => $faker->email,
        ];
    }

    return $data;
});
