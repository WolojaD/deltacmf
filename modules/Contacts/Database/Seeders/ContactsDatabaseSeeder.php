<?php

namespace Modules\Contacts\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Contacts\Entities\Group;
use Modules\Contacts\Entities\Contact;

class ContactsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Group::class, 1)->create();
            factory(Contact::class, random_int(1, 5))->states('phone')->create();
            factory(Contact::class, random_int(1, 5))->states('link')->create();
            factory(Contact::class, random_int(1, 5))->states('email')->create();
            factory(Contact::class, random_int(1, 5))->states('other')->create();
        }
    }
}
