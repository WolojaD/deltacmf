<?php

namespace Modules\Contacts\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Webwizo\Shortcodes\Facades\Shortcode;
use Modules\Core\Providers\ServiceProvider;
use Modules\Contacts\Shortcodes\ContactsShortcode;
use Modules\Core\Events\LoadingBackendTranslations;

class ContactsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig('contacts', 'config');
        $this->publishConfig('contacts', 'permissions');
        $this->publishConfig('contacts', 'fields-type');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();

        Shortcode::register('contacts', ContactsShortcode::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('contacts_sidebar', array_dot(trans('contacts::sidebar')));
            $event->load('contacts_table-fields', array_dot(trans('contacts::table-fields')));
            $event->load('contacts_permissions', array_dot(trans('contacts::permissions')));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $folder = base_path('modules/Contacts/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }

        foreach ($models as $model_name => $model) {
            $model_name = studly_case($model_name);

            $this->app->bind("Modules\Contacts\Repositories\\{$model_name}Interface", function () use ($model_name, $model) {
                $eloquent_repository = "\Modules\Contacts\Repositories\Eloquent\Eloquent{$model_name}Repository";
                $entity = "\Modules\Contacts\Entities\\{$model_name}";
                $cache_decorator = "\Modules\Contacts\Repositories\Cache\Cache{$model_name}Decorator";

                $repository = new $eloquent_repository(new $entity());

                if (!Config::get('app.cache') && !($model->no_cache ?? false)) {
                    return $repository;
                }

                return new $cache_decorator($repository);
            });
        }
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
