<?php

namespace Modules\Contacts\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Contacts\Repositories\GroupInterface;
use Modules\Contacts\Repositories\ContactInterface;
use Modules\Contacts\Transformers\ContactTransformer;
use Modules\Contacts\Http\Requests\CreateContactRequest;
use Modules\Contacts\Http\Requests\UpdateContactRequest;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Contacts\Transformers\FullContactTransformer;

class ContactController extends CoreApiController
{
    /**
     * @var ContactInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    // protected $group;

    public function __construct(ContactInterface $contact, GroupInterface $group)
    {
        $this->repository = $contact;
        $this->modelInfo = $this->repository->getJsonList();
        $this->group = $group;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return ContactTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return ContactTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return ContactTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return ContactTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullContactTransformer
     */
    public function find($id)
    {
        $contact = $this->repository->find($id);

        return new FullContactTransformer($contact);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function findNew(Request $request)
    {
        $carry = [
            [
                'name' => trans('core::breadcrumbs.contacts create')
            ]
        ];

        return [
            'fields' => $this->repository->getFieldsArray(),
            'breadcrumbs' => $this->group->find($request->ident)->getBackendBreadcrumbs($carry)
        ];
    }

    /**
     * @param int $id
     * @param UpdateContactRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateContactRequest $request)
    {
        $contact = $this->repository->find($id);

        $this->repository->update($contact, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('contacts::models.of.contact')]),
            // 'goto' => ['name' => 'api.backend.contactsgrp_contactsgrp.show', 'params' => ['ident' => $contact->contactsgrp_id]],
            'id' => $contact->id
        ]);
    }

    /**
     * @param CreateContactRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateContactRequest $request)
    {
        $contact = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('contacts::models.of.contact')]),
            // 'goto' => ['name' => 'api.backend.contactsgrp_contactsgrp.show', 'params' => ['ident' => $contact->contactsgrp_id]],
            'id' => $contact->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = $this->repository->find($id);

        $this->repository->destroy($contact);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('contacts::models.of.contact')]),
        ]);
    }
}
