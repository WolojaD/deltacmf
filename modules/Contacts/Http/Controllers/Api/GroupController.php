<?php

namespace Modules\Contacts\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Contacts\Repositories\GroupInterface;
use Modules\Contacts\Repositories\ContactInterface;
use Modules\Contacts\Transformers\GroupTransformer;
use Modules\Contacts\Transformers\ContactTransformer;
use Modules\Contacts\Http\Requests\CreateGroupRequest;
use Modules\Contacts\Http\Requests\UpdateGroupRequest;
use Modules\Contacts\Transformers\FullGroupTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class GroupController extends CoreApiController
{
    /**
     * @var GroupInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(ContactInterface $contacts, GroupInterface $group)
    {
        $this->repository = $group;
        $this->modelInfo = $this->repository->getJsonList();
        $this->contacts = $contacts;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return GroupTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['group_id' => $id]]);

            return ContactTransformer::collection($this->contacts->serverPaginationFilteringFor($request));
        }

        return GroupTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullGroupTransformer
     */
    public function find($id)
    {
        $group = $this->repository->find($id);

        return new FullGroupTransformer($group);
    }

    /**
     * @param int $id
     * @param UpdateGroupRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateGroupRequest $request)
    {
        $group = $this->repository->find($id);

        $this->repository->update($group, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('contacts::models.of.group')]),
            'id' => $group->id
        ]);
    }

    /**
     * @param CreateGroupRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroupRequest $request)
    {
        $group = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('contacts::models.of.group')]),
            'id' => $group->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = $this->repository->find($id);

        $this->repository->destroy($group);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('contacts::models.of.group')]),
        ]);
    }
}
