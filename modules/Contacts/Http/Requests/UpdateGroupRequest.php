<?php

namespace Modules\Contacts\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateGroupRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Contacts/Config/models/group.json');
    }

    public function authorize()
    {
        return true;
    }
}
