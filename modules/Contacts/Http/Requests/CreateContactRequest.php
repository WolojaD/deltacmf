<?php

namespace Modules\Contacts\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateContactRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Contacts/Config/models/contact.json');
    }

    public function authorize()
    {
        return true;
    }

    public function translationRules()
    {
        $request = $this;

        switch ($request->field_type) {
            case 'email':
                return [
                    'body' => 'required|regex:/^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/u'
                ];
                break;
            case 'phone':
                return [
                    'body' => 'required|regex:/^(?=.*[0-9])[- +()0-9,]+$/'
                ];
                break;
            case 'link':
            case 'other':
                return [
                    'body' => 'required'
                ];
                break;
        }
    }
}
