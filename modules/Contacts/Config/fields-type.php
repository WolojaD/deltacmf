<?php

return [
    'field_type' => [
        'email' => trans('contacts::field-types.email'),
        'phone' => trans('contacts::field-types.phone'),
        'link' => trans('contacts::field-types.link'),
        'other' => trans('contacts::field-types.other'),
    ],
];
