import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/contacts/group',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.contacts_group',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Contacts',
                    breadcrumb: [
                        { name: 'Contacts' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.contacts_group.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Contacts Create',
                    breadcrumb: [
                        { name: 'Contacts', link: 'api.backend.contacts_group' },
                        { name: 'Contacts Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.contacts_group.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Contacts Edit',
                    breadcrumb: [
                        { name: 'Contacts', link: 'api.backend.contacts_group' },
                        { name: 'Contacts Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.contacts_group.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Contacts',
                    breadcrumb: [
                        { name: 'Contacts', link: 'api.backend.contacts_group' },
                        { name: 'Contact' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/contacts/contact',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.contacts_contact.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Contact Create',
                    breadcrumb: [
                        { name: 'Contacts', link: 'api.backend.contacts_group' },
                        { name: 'Contact Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.contacts_contact.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Contact Edit',
                    breadcrumb: [
                        { name: 'Contacts', link: 'api.backend.contacts_group' },
                        { name: 'Contact Edit' }
                    ]
                }
            }
        ]
    }
]