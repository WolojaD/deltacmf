<?php

namespace Modules\Gallery\Repositories\Eloquent;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentGalleryRepository extends EloquentCoreRepository implements GalleryRepository
{
    public function tempUserGallery()
    {
        return $this->model->where('user_id', Sentinel::getUser()->id)->where('is_temp', 1)->first();
    }

    public function duplicateExisted($gallery_id)
    {
        return $this->model->where('id', $gallery_id)->first()->duplicateExisted();
    }
}
