<?php

namespace Modules\Gallery\Repositories\Eloquent;

use Modules\Gallery\Repositories\ImageRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentImageRepository extends EloquentCoreRepository implements ImageRepository
{
    /**
     * Setting status for selected ids.
     *
     * @param array $selected_ids
     * @param int $value
     * @param string $field
     *
     * @return \Illuminate\Support\Collection
     */
    public function markSelectedAs(array $selected_ids, $value, $field)
    {
        if (!count($selected_ids)) {
            return collect([]);
        }

        $entities = $this->model->whereIn('id', $selected_ids);
        $entities->update([$field => $value ?? 0]);

        return $entities->pluck($field, 'id');
    }
}
