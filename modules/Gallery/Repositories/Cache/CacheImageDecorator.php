<?php

namespace Modules\Gallery\Repositories\Cache;

use Modules\Gallery\Repositories\ImageRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheImageDecorator extends CoreCacheDecorator implements ImageRepository
{
    /**
     * @var ImageRepository
     */
    protected $repository;

    public function __construct(ImageRepository $image)
    {
        parent::__construct();

        $this->entityName = 'gallery_images';
        $this->repository = $image;
    }
}
