<?php

namespace Modules\Gallery\Repositories\Cache;

use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheGalleryDecorator extends CoreCacheDecorator implements GalleryRepository
{
    /**
     * @var GalleryRepository
     */
    protected $repository;

    public function __construct(GalleryRepository $gallery)
    {
        parent::__construct();

        $this->entityName = 'galleries';
        $this->repository = $gallery;
    }
}
