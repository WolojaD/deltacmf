<?php

namespace Modules\Gallery\Shortcodes;

use Modules\Gallery\Entities\Gallery;

class GalleryShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        $gallery = Gallery::when(!is_numeric($shortcode->code), function ($q) use ($shortcode) {
            $q->where('short_code', $shortcode->code);
        }, function ($q) use ($shortcode) {
            $q->where('id', $shortcode->code);
        })->with('images')->first();

        if (null === $gallery) {
            return '';
        }

        if ($gallery) {
            return $gallery->images && $gallery->images->count() ? view('gallery::' . $gallery->template, compact('gallery')) : '';
        } else {
            return '';
        }
    }
}
