<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'gallery', 'middleware' => ['api.token', 'auth.admin']], function () {
    /*
    |--------------------------------------------------------------------------
    | Gallery Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => '/'], function () {
        Route::get('/', ['as' => 'api.backend.gallery.index', 'uses' => 'GalleryController@index', 'middleware' => 'token-can:gallery.index']);

        Route::get('{gallery}', ['as' => 'api.backend.gallery.show', 'uses' => 'GalleryController@show', 'middleware' => 'token-can:gallery.index']);

        Route::put('toggle-field', ['as' => 'api.backend.gallery.status', 'uses' => 'GalleryController@toggleField', 'middleware' => 'token-can:gallery.edit']);

        Route::put('{gallery}', ['as' => 'api.backend.gallery.update', 'uses' => 'GalleryController@update', 'middleware' => 'token-can:gallery.edit']);

        Route::post('/', ['as' => 'api.backend.gallery.store', 'uses' => 'GalleryController@store', 'middleware' => 'token-can:gallery.create']);

        Route::delete('{gallery}', ['as' => 'api.backend.gallery.destroy', 'uses' => 'GalleryController@destroy', 'middleware' => 'token-can:gallery.destroy']);

        /*
        |--------------------------------------------------------------------------
        | Image Routes
        |--------------------------------------------------------------------------
        */

        Route::group(['prefix' => 'image'], function () {
            Route::post('upload', ['as' => 'api.backend.gallery.photo.upload', 'uses' => 'ImageController@store', 'middleware' => 'token-can:gallery.edit']);

            Route::put('order', ['as' => 'api.backend.gallery.photo.order', 'uses' => 'ImageController@changeOrder', 'middleware' => 'token-can:gallery.edit']);

            Route::put('toggle-field', ['as' => 'api.backend.photo.status', 'uses' => 'ImageController@toggleField', 'middleware' => 'token-can:gallery.edit']);

            Route::put('{image}', ['as' => 'api.backend.gallery.photo.update', 'uses' => 'ImageController@update', 'middleware' => 'token-can:gallery.edit']);

            Route::delete('{image}', ['as' => 'api.backend.gallery.photo.destroy', 'uses' => 'ImageController@destroy', 'middleware' => 'token-can:gallery.destroy']);
        });
    });
});
