<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/gallery'], function () {
    Route::get('/', ['as' => 'backend.gallery.index', 'uses' => 'GalleryController@index', 'middleware' => 'can:gallery.index']);

    Route::get('{id}/show', ['as' => 'backend.gallery.show', 'uses' => 'GalleryController@index', 'middleware' => 'can:gallery.index']);
});
