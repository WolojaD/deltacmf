<?php

namespace Modules\Gallery\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Gallery\Entities\Gallery;
use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Gallery\Transformers\GalleryTransformer;
use Modules\Gallery\Http\Requests\CreateGalleryRequest;
use Modules\Gallery\Http\Requests\UpdateGalleryRequest;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Gallery\Transformers\FullGalleryTransformer;

class GalleryController extends CoreApiController
{
    /**
     * @var GalleryRepository
     */
    protected $repository;

    /**
     * @var Status
     */
    protected $status;

    public function __construct(GalleryRepository $gallery)
    {
        $this->repository = $gallery;
    }

    public function index(Request $request)
    {
        $request->merge([
            'sort' => 'created_at|desc',
            'per_page' => 500,
            'with' => 'images',
            'where' => ['type' => 'gallery']
        ]);

        return GalleryTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show($id)
    {
        return new FullGalleryTransformer(Gallery::with('images')->find($id));
    }

    public function update(Gallery $gallery, UpdateGalleryRequest $request)
    {
        $this->repository->update($gallery, $request->gallery);

        return response()->json([
            'errors' => false,
            'message' => trans('gallery::messages.api.gallery updated'),
            'gallery' => new FullGalleryTransformer($gallery),
        ]);
    }

    public function store(CreateGalleryRequest $request)
    {
        $gallery = $request->gallery;
        $gallery['status'] = true;

        $gallery = $this->repository->create($gallery);

        return response()->json([
            'errors' => false,
            'message' => trans('gallery::messages.api.gallery created'),
            'gallery' => new FullGalleryTransformer($gallery),
        ]);
    }

    public function destroy(Gallery $gallery)
    {
        $this->repository->destroy($gallery);

        return response()->json([
            'errors' => false,
            'message' => trans('gallery::messages.api.gallery deleted'),
        ]);
    }
}
