<?php

namespace Modules\Gallery\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Gallery\Entities\Image;
use Modules\Gallery\Repositories\ImageRepository;
use Modules\Gallery\Transformers\ImageTransformer;
use Modules\Gallery\Http\Requests\CreateImageRequest;
use Modules\Gallery\Transformers\FullImageTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class ImageController extends CoreApiController
{
    /**
     * @var ImageRepository
     */
    protected $repository;

    public function __construct(ImageRepository $image)
    {
        $this->repository = $image;
    }

    /**
     * @param Request $request
     *
     * @return type
     */
    public function toggleField(Request $request)
    {
        return $this->repository->markSelectedAs($request->get('items', []), $request->position, $request->field);
    }

    public function update(Image $image, Request $request)
    {
        $this->repository->update($image, $request->photo);

        return response()->json([
            'errors' => false,
            'message' => trans('gallery::messages.api.image updated'),
            'image' => new FullImageTransformer($image),
        ]);
    }

    public function store(CreateImageRequest $request)
    {
        foreach ($request->file as $file) {
            if (!$file->isValid()) {
                continue;
            }

            $images[] = $this->repository->create([
                'path' => $file,
                'status' => true,
                'gallery_id' => $request->gallery_id,
            ]);
        }

        return ImageTransformer::collection(collect($images ?? []));
    }

    public function destroy(Image $image)
    {
        $this->repository->destroy($image);

        return response()->json([
            'errors' => false,
            'message' => trans('gallery::messages.api.image deleted'),
        ]);
    }
}
