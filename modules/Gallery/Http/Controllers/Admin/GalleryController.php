<?php

namespace Modules\Gallery\Http\Controllers\Admin;

use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class GalleryController extends CoreAdminController
{
    /**
     * @var GalleryRepository
     */
    private $gallery;

    public function __construct(GalleryRepository $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->gallery->delete($id);

        return redirect()->route('backend.gallery.index')
            ->withSuccess(trans('Gallery deleted'));
    }
}
