<?php

namespace Modules\Gallery\Providers;

use Modules\Gallery\Entities\Image;
use Modules\Gallery\Entities\Gallery;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Webwizo\Shortcodes\Facades\Shortcode;
use Modules\Core\Providers\ServiceProvider;
use Modules\Gallery\Shortcodes\GalleryShortcode;
use Modules\Gallery\Repositories\ImageRepository;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Gallery\Repositories\Cache\CacheImageDecorator;
use Modules\Gallery\Repositories\Cache\CacheGalleryDecorator;
use Modules\Gallery\Repositories\Eloquent\EloquentImageRepository;
use Modules\Gallery\Repositories\Eloquent\EloquentGalleryRepository;

class GalleryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        Shortcode::register('gallery', GalleryShortcode::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('gallery_sidebar', array_dot(trans('gallery::sidebar')));
            $event->load('gallery_table-fields', array_dot(trans('gallery::table-fields')));
            $event->load('gallery_permissions', array_dot(trans('gallery::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('gallery', 'config');
        $this->publishConfig('gallery', 'permissions');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->bind(GalleryRepository::class, function () {
            $repository = new EloquentGalleryRepository(new Gallery());

            if (!Config::get('app.cache')) {
                return $repository;
            }

            return new CacheGalleryDecorator($repository);
        });

        $this->app->bind(ImageRepository::class, function () {
            $repository = new EloquentImageRepository(new Image());

            if (!Config::get('app.cache')) {
                return $repository;
            }

            return new CacheImageDecorator($repository);
        });
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
