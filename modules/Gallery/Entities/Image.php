<?php

namespace Modules\Gallery\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\DraftableTrait;
use Modules\Core\Traits\FileUploaderTrait;
use Modules\Core\Traits\ImageUploaderTrait;
use Modules\Core\Traits\NamespacedEntityTrait;
use Modules\Core\Traits\SearchableSortableTrait;
use Modules\Core\Internationalisation\Translatable;
use Modules\Settings\Entities\Image as FoldersImage;

class Image extends Model
{
    use Translatable,
        NamespacedEntityTrait,
        SearchableSortableTrait,
        FileUploaderTrait,
        ImageUploaderTrait,
        DraftableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'gallery_images';
    protected $translations_module = 'gallery';
    public $path_size = ['width' => 200];
    public $translatedAttributes = [
        'title',
        'description',
    ];
    protected $fillable = [
        'gallery_id',
        'order',
        'title',
        'path',
        'status',
        'description',
    ];
    protected static $entityNamespace = 'application/gallery';
    protected $casts = [
        'status' => 'boolean',
    ];
    protected $appends = ['width', 'height', 'size', 'url', 'file_name'];
    protected $image_properties = [
        'path' => [
            'resolutions' => 'auto',
        ],
    ];
    public $groupOrder = 'gallery_id';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return type
     */
    public static function boot()
    {
        static::deleting(function ($entity) {
            $folders = FoldersImage::select('title', 'id')->get();

            foreach ($folders as $folder) {
                $pre_path = 'storage/' . $folder->rootPath . '/' . $folder->title . '/';

                if ($entity->path) {
                    \File::delete(public_path($pre_path . $entity->path));
                }
            }

            if ($entity->path) {
                \File::delete(public_path('storage/origin' . $entity->path));
            }
        });

        parent::boot();
    }

    public function duplicateTo($gallery_id)
    {
        $old_path = $this->path;
        $path = preg_replace('/gallery_images\/(\d+)\//', 'gallery_images/' . $gallery_id . '/', $this->path);

        $dirname = pathinfo(public_path('storage/origin' . $path), PATHINFO_DIRNAME);

        if (!file_exists($dirname)) {
            mkdir($dirname, 0777, true);
        }

        copy(public_path('storage/origin' . $old_path), public_path('storage/origin' . $path));

        $data = [
            'gallery_id' => $gallery_id,
            'status' => $this->status,
            'order' => $this->order,
            'path' => $path,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $data[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        self::create($data);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getWidthAttribute()
    {
        $path = public_path('storage/origin' . $this->path);

        return is_file($path) ? getimagesize($path)[0] : 0;
    }

    public function getHeightAttribute()
    {
        $path = public_path('storage/origin' . $this->path);

        return is_file($path) ? getimagesize($path)[1] : 0;
    }

    public function getSizeAttribute()
    {
        $path = public_path('storage/origin' . $this->path);

        return is_file($path) ? filesize($path) : 0;
    }

    public function getFileNameAttribute()
    {
        return pathinfo($this->path)['basename'];
    }

    public function getUrlAttribute()
    {
        return $this->path;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setPathAttribute($value)
    {
        if (starts_with($value, 'data:image')) {
            $this->uploadImageToDisk($value, 'path');
        } else {
            $this->uploadFile($value, 'path', 'origin');
        }
    }
}
