<?php

namespace Modules\Gallery\Entities;

use Modules\Core\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\DraftableTrait;
use Modules\Core\Traits\NamespacedEntityTrait;
use Modules\Core\Traits\SearchableSortableTrait;
use Modules\Core\Internationalisation\Translatable;

class Gallery extends Model
{
    use Translatable,
        NamespacedEntityTrait,
        SearchableSortableTrait,
        DraftableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'galleries';
    protected $translations_module = 'gallery';
    public $translatedAttributes = [
        'title',
        'description',
    ];
    protected $fillable = [
        'title',
        'description',
        'status',
        'show_title',
        'template',
        'is_temp',
        'user_id',
        'type',
    ];
    protected static $entityNamespace = 'application/gallery';
    protected $casts = [
        'status' => 'boolean',
        'show_title' => 'boolean',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Changes builder to custom
     *
     * @param [type] $query
     *
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    public function getConfiguratorPath()
    {
        return '';
    }

    /**
     * @return type
     */
    public static function boot()
    {
        static::creating(function ($obj) {
            $obj->user_id = \Sentinel::getUser()->id ?? 0;
        });

        static::deleting(function ($obj) {
            $obj->images->each(function ($image) {
                $image->delete();
            });
        });

        parent::boot();
    }

    public function duplicateExisted()
    {
        $data = [
            'status' => $this->status,
            'show_title' => $this->show_title,
            'template' => $this->template,
            'is_temp' => $this->is_temp,
            'user_id' => auth()->user()->id ?? 0,
            'type' => $this->type,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $data[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        $new_gallery = self::create($data);

        $this->images->each(function ($image) use ($new_gallery) {
            return $image->duplicateTo($new_gallery->id);
        });

        return $new_gallery->id;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function images()
    {
        return $this->hasMany(Image::class)->orderBy('order', 'asc')->with('translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
