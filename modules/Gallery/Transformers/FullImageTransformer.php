<?php

namespace Modules\Gallery\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullImageTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $array[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $array[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $array;
    }
}
