<?php

namespace Modules\Gallery\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullGalleryTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);

        $array['images'] = ImageTransformer::collection($this->images);

        return $array;
    }
}
