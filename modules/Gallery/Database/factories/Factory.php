<?php

use Faker\Generator as Faker;
use Modules\Gallery\Entities\Gallery;

$factory->define(Gallery::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'show_title' => $faker->numberBetween(0, 1),
        'template' => $faker->randomElement(['slider', 'block']),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'description' => $faker->sentence,
        ];
    }

    return $data;
});
