<?php

namespace Modules\Gallery\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Gallery\Entities\Gallery;

class GalleryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Gallery::class, 15)->create();
        }
    }
}
