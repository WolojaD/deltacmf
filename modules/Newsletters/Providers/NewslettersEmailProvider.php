<?php

namespace Modules\Newsletters\Providers;

use Illuminate\Support\Facades\Mail;
use Modules\Emailtemplates\Mail\EmailAdminAutoMailable;
use Modules\Emailtemplates\Providers\EmailtemplatesTraitsProvider;

class NewslettersEmailProvider extends EmailtemplatesTraitsProvider
{
    public function handle($data_array = [])
    {
        $email_template = $this->repository->getTemplate($data_array['module'], $data_array['method']);

        $emails = null !== $email_template->emails ? $this->trimExplode($email_template->emails) : $this->getEmailsFromSettings();

        $send_to_admin = settings('newsletters::send_to_administrator', null, 0);

        if ($send_to_admin && null !== settings('emailtemplates::administrator_emails', null, null)) {
            $emails = array_merge($emails, $this->getAdminEmailsFromSettings());
        }

        if (count(array_unique(array_filter($emails), SORT_STRING)) > 0) {
            foreach ($emails as $email) {
                Mail::to(clear_email($email))->send(new EmailAdminAutoMailable($data_array, $email_template));
            }
        }
    }

    /**
     * @param $str
     *
     * @return array
     */
    public function trimExplode($str)
    {
        return array_map('trim', explode(',', $str));
    }

    /**
     * @return array
     */
    protected function getEmailsFromSettings()
    {
        return $this->trimExplode(settings('newsletters::email_list', null, ''));
    }

    /**
     * @return array
     */
    protected function getAdminEmailsFromSettings()
    {
        return $this->trimExplode(settings('emailtemplates::administrator_emails', null, ''));
    }
}
