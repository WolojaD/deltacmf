<?php

namespace Modules\Newsletters\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FollowerTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['is_blocked'] = $this->is_blocked ? 'Да' : '';
        $result['created_at'] = $this->created_at->toRfc2822String();

        return $result;
    }
}
