<?php

namespace Modules\Newsletters\Traits;

use Modules\Newsletters\Providers\NewslettersEmailProvider;

trait EmailTemplatesTrait
{
    public $provider;

    public function sendSubscribeDataForAdmin($data)
    {
        $trait_provider = new NewslettersEmailProvider();
        $data_array = [
            'module' => 'Newsletters',
            'method' => __FUNCTION__,
            'variables' => [
                'email' => $data['email']
            ]
        ];

        $trait_provider->handle($data_array);
    }
}
