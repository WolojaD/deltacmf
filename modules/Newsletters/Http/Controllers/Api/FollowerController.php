<?php

namespace Modules\Newsletters\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Newsletters\Traits\EmailTemplatesTrait;
use Modules\Newsletters\Repositories\FollowerInterface;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Newsletters\Transformers\FollowerTransformer;
use Modules\Newsletters\Http\Requests\CreateFollowerRequest;
use Modules\Newsletters\Http\Requests\UpdateFollowerRequest;
use Modules\Newsletters\Transformers\FullFollowerTransformer;

class FollowerController extends CoreApiController
{
    use EmailTemplatesTrait;

    /**
     * @var FollowerInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(FollowerInterface $follower)
    {
        $this->repository = $follower;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return FollowerTransformer
     */
    public function index(Request $request, $id = false)
    {
        return FollowerTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullFollowerTransformer
     */
    public function find($id)
    {
        $follower = $this->repository->find($id);

        return new FullFollowerTransformer($follower);
    }

    /**
     * @param int $id
     * @param UpdateFollowerRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateFollowerRequest $request)
    {
        $follower = $this->repository->find($id);

        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $data['user'] = null != $request->input('user', null) ? $data['user'] : explode('@', $data['email'])[0];

        $this->repository->update($follower, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('newsletters::models.of.follower')]),
            'id' => $follower->id
        ]);
    }

    /**
     * @param CreateFollowerRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFollowerRequest $request)
    {
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }
        $data['user'] = null != $request->input('user', null) ? $data['user'] : explode('@', $data['email'])[0];

        $follower = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('newsletters::models.of.follower')]),
            'id' => $follower->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $follower = $this->repository->find($id);

        $this->repository->destroy($follower);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('newsletters::models.of.follower')]),
        ]);
    }

    /**
     * @param $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMailData($data)
    {
        $this->sendSubscribeDataForAdmin($data);

        return response()->json([
            'errors' => false,
            'message' => 'Test sendMailData send Successful',
        ]);
    }
}
