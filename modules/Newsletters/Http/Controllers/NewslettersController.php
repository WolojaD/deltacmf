<?php

namespace Modules\Newsletters\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Newsletters\Traits\EmailTemplatesTrait;
use Modules\Core\Http\Controllers\CorePublicController;
use Modules\Newsletters\Repositories\FollowerInterface;

class NewslettersController extends CorePublicController
{
    use EmailTemplatesTrait;

    /**
     * @var FollowerInterface
     */
    protected $follower;

    public function __construct(FollowerInterface $follower)
    {
        $this->follower = $follower;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        dd('Module newsletters successfully created!\\n Middleware is missed for this module! Config folder should be created with configuration config\application\module_name');

        return view('newsletters::index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('newsletters::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (null !== $request->all()) {
            $form_request = $request->except(['_method', '_token']);

            $rules = [
                'email' => 'required|email|unique:newsletters_followers'
            ];

            $messsages = [
                'email.unique' => trans('frontend::newsletters.form rules.errors.email unique'),
                'email.email' => trans('frontend::newsletters.form rules.errors.email email'),
            ];

            $validator = \Validator::make($form_request, $rules, $messsages);

            if ($validator->fails()) {
                return response()->json(['error' => true, 'msg' => $validator->messages()], 422);
            }

            $form_request['user'] = null != $request->input('user', null) ? $form_request['user'] : explode('@', $form_request['email'])[0];

            $response = $this->follower->create($form_request);

            if ($response) {
                $this->sendSubscribeDataForAdmin($form_request);

                return response()->json([
                    'errors' => false,
                    'message' => 'Follower send Successful',
                ]);
            } else {
                return response()->json(['error' => true, 'msg' => \Exception::getMessage()], 422);
            }
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('newsletters::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('newsletters::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
    }
}
