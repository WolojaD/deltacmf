<?php

namespace Modules\Newsletters\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateFollowerRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Newsletters/Config/models/follower.json');
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
            'comment' => 'required_if:is_blocked,1',
        ];
    }
}
