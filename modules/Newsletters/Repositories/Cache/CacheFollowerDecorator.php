<?php

namespace Modules\Newsletters\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Newsletters\Repositories\FollowerInterface;

class CacheFollowerDecorator extends CoreCacheDecorator implements FollowerInterface
{
    /**
     * @var FollowerInterface
     */
    protected $repository;

    public function __construct(FollowerInterface $newsletters)
    {
        parent::__construct();

        $this->entityName = 'newsletters_followers';
        $this->repository = $newsletters;
    }
}
