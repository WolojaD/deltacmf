<?php

namespace Modules\Newsletters\Repositories\Eloquent;

use Modules\Newsletters\Repositories\FollowerInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentFollowerRepository extends EloquentCoreRepository implements FollowerInterface
{
    public $model_config = 'modules/Newsletters/Config/models/follower.json';
}
