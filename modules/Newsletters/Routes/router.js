import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/newsletters',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        redirect: { name: 'api.backend.newsletters_follower' }
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/newsletters/follower',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.newsletters_follower',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Follower',
                    breadcrumb: [
                        { name: 'Follower' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.newsletters_follower.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Follower Create',
                    breadcrumb: [
                        { name: 'Follower', link: 'api.backend.newsletters_follower' },
                        { name: 'Follower Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.newsletters_follower.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Follower Edit',
                    breadcrumb: [
                        { name: 'Follower', link: 'api.backend.newsletters_follower' },
                        { name: 'Follower Edit' }
                    ]
                }
            }
        ]
    },
]
