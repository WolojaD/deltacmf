<?php

namespace Modules\Users\Mail;

use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Users\Entities\UserEntitiInterface;

// class ResetPasswordEmail extends Mailable implements ShouldQueue
class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var UserEntitiInterface
     */
    public $user;

    /**
     * @var
     */
    public $code;

    public function __construct(UserEntitiInterface $user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS') ?? strtolower(config('app.name') . '@mail.com'), config('app.name'))->view('users::emails.reminder')->subject('Reset password email');
    }
}
