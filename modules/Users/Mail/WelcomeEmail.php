<?php

namespace Modules\Users\Mail;

use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Users\Entities\UserEntitiInterface;

// class WelcomeEmail extends Mailable implements ShouldQueue
class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var UserEntitiInterface
     */
    public $user;

    /**
     * @var
     */
    public $activationCode;

    public function __construct(UserEntitiInterface $user, $activationCode)
    {
        $this->user = $user;
        $this->activationCode = $activationCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS') ?? strtolower(config('app.name') . '@mail.com'), config('app.name'))->view('users::emails.welcome')->subject('Welcome email');
    }
}
