<?php

namespace Modules\Users\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Users\Contracts\Authentication;
use Modules\Users\Entities\UserEntitiInterface;
use Modules\Users\Repositories\UserTokenInterface;

class TokenCan
{
    /**
     * @var UserTokenInterface
     */
    private $userToken;

    /**
     * @var Authentication
     */
    private $auth;

    public function __construct(UserTokenInterface $userToken, Authentication $auth)
    {
        $this->userToken = $userToken;
        $this->auth = $auth;
    }

    /**
     * @param Request  $request
     * @param \Closure $next
     * @param string   $permission
     *
     * @return Response
     */
    public function handle(Request $request, \Closure $next, $permission)
    {
        if (null === $request->header('Authorization')) {
            return new Response(base64_encode('Forbidden! TokenCan'), Response::HTTP_FORBIDDEN);
        }

        $user = $this->getUserFromToken($request->header('Authorization'));

        if (false === $user->hasAccess($permission)) {
            return response(base64_encode('Unauthorized! TokenCan'), Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }

    /**
     * @param string $token
     *
     * @return UserEntitiInterface
     */
    private function getUserFromToken($token)
    {
        $token = $this->userToken->findByAttributes(['access_token' => $this->parseToken($token)]);

        return $token->user;
    }

    /**
     * @param string $token
     *
     * @return string
     */
    private function parseToken($token)
    {
        return str_replace('Bearer ', '', $token);
    }
}
