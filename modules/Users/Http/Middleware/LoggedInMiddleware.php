<?php

namespace Modules\Users\Http\Middleware;

use Modules\Users\Contracts\Authentication;

class LoggedInMiddleware
{
    /**
     * @var Authentication
     */
    private $auth;

    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (!$this->auth->check()) {
            return redirect()->guest(config('application.users.config.redirect_route_not_logged_in', 'auth/login'));
        }

        return $next($request);
    }
}
