<?php

namespace Modules\Users\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Users\Repositories\UserTokenInterface;

class AuthorisedApiTokenDeveloper
{
    /**
     * @var UserTokenInterface
     */
    private $userToken;

    public function __construct(UserTokenInterface $userToken)
    {
        $this->userToken = $userToken;
    }

    public function handle(Request $request, \Closure $next)
    {
        if (null === $request->header('Authorization')) {
            return new Response(base64_encode('Forbidden! AuthorisedApiTokenAdmin'), 404);
        }

        if (false === $this->isValidToken($request->header('Authorization'))) {
            return new Response(base64_encode('Forbidden! AuthorisedApiTokenAdmin'), 404);
        }

        return $next($request);
    }

    private function isValidToken($token)
    {
        $found = $this->userToken->findByAttributes(['access_token' => $this->parseToken($token)]);

        if (null === $found) {
            return false;
        }

        if (false === $found->user->hasRoleSlug('developer')) {
            return false;
        }

        return true;
    }

    private function parseToken($token)
    {
        return str_replace('Bearer ', '', $token);
    }
}
