<?php

namespace Modules\Users\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Users\Contracts\Authentication;
use Modules\Users\Repositories\UserTokenInterface;

class AuthorisedApiToken
{
    /**
     * @var UserTokenInterface
     */
    private $userToken;

    /**
     * @var Authentication
     */
    private $auth;

    public function __construct(UserTokenInterface $userToken, Authentication $auth)
    {
        $this->userToken = $userToken;
        $this->auth = $auth;
    }

    public function handle(Request $request, \Closure $next)
    {
        if (null === $request->header('Authorization')) {
            return new Response(base64_encode('Forbidden! AuthorisedApiToken'), 403);
        }

        if (false === $this->isValidToken($request->header('Authorization'))) {
            return new Response(base64_encode('Forbidden! AuthorisedApiToken'), 403);
        }

        return $next($request);
    }

    private function isValidToken($token)
    {
        $found = $this->userToken->findByAttributes(['access_token' => $this->parseToken($token)]);

        if (null === $found) {
            return false;
        }

        $this->auth->logUserIn($found->user);

        return true;
    }

    private function parseToken($token)
    {
        return str_replace('Bearer ', '', $token);
    }
}
