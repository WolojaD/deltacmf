<?php

namespace Modules\Users\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateRoleRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $role = $this->route('role');

        return [
            'name' => 'required',
            'slug' => 'required|regex:/^[-a-z0-9]*$/|unique:roles,slug,' . $role->id . ',id',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('users::messages.form roles.errors.name required'),
            'slug.required' => trans('users::messages.form roles.errors.slug required'),
            'slug.unique' => trans('users::messages.form roles.errors.slug unique'),
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
