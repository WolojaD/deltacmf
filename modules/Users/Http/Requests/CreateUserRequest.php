<?php

namespace Modules\Users\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateUserRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:3|confirmed',
            'roles' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => trans('users::messages.form users.errors.first_name required'),
            'last_name.required' => trans('users::messages.form users.errors.last_name required'),
            'email.required' => trans('users::messages.form users.errors.email required'),
            'email.unique' => trans('users::messages.form users.errors.email unique'),
            'password.required' => trans('users::messages.form users.errors.password required'),
            'roles.required' => trans('users::messages.form users.errors.roles required'),
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
