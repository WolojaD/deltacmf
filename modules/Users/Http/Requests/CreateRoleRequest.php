<?php

namespace Modules\Users\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateRoleRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name',
            'is_admin' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('users::messages.form roles.errors.name required'),
            'name.unique' => trans('users::messages.form roles.errors.name unique'),
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
