<?php

namespace Modules\Users\Http\Controllers;

use Modules\Users\Services\UserResetter;
use Modules\Users\Services\UserRegistration;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Modules\Users\Http\Requests\LoginRequest;
use Modules\Users\Http\Requests\ResetRequest;
use Modules\Users\Http\Requests\RegisterRequest;
use Modules\Users\Exceptions\UserNotFoundException;
use Modules\Users\Http\Requests\ResetCompleteRequest;
use Modules\Core\Http\Controllers\CorePublicController;
use Modules\Users\Exceptions\InvalidOrExpiredResetCode;

class AuthController extends CorePublicController
{
    use DispatchesJobs;

    public function __construct()
    {
        parent::__construct();

        if (locale() !== 'en') {
            app()->setLocale('ru');
        }
    }

    public function getLogin()
    {
        return view('users::auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $remember = (bool) $request->get('remember_me', false);
        $error = $this->auth->login($credentials, $remember);

        if ($error) {
            return redirect()->back()->withInput()->withError($error);
        }

        return redirect()->route('backend.dashboard.redirectIntended');
    }

    public function getRegister()
    {
        if (config('application.users.config.allow_user_registration', false)) {
            return view('users::auth.register');
        }
    }

    public function postRegister(RegisterRequest $request)
    {
        if (config('application.users.config.allow_user_registration', false)) {
            app(UserRegistration::class)->register($request->all());

            if (config('application.users.config.allow_send_user_registration_email', false)) {
                return redirect()->route('register')->withSuccess(trans('users::auth.account created check email'));
            }

            return redirect()->route('register')->withSuccess(trans('users::auth.account created'));
        }
    }

    public function getActivate($userId, $code)
    {
        if ($this->auth->activate($userId, $code)) {
            app(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($userId);

            return redirect()->route('login')
                ->withSuccess('Account activated! You can now login');
        }

        return redirect()->route('register')
            ->withError('There was an error with the activation!');
    }

    public function getReset()
    {
        if (config('application.users.config.allow_user_reset', false)) {
            return view('users::auth.reset.begin');
        }
    }

    public function postReset(ResetRequest $request)
    {
        if (config('application.users.config.allow_user_reset', false)) {
            try {
                app(UserResetter::class)->startReset($request->all());
            } catch (UserNotFoundException $e) {
                return redirect()->back()->withInput()
                    ->withError('No user found');
            }

            return redirect()->route('reset')
                ->withSuccess('Check email to reset password');
        }
    }

    public function getResetComplete()
    {
        if (config('application.users.config.allow_user_reset', false)) {
            return view('users::auth.reset.complete');
        }
    }

    public function postResetComplete($userId, $code, ResetCompleteRequest $request)
    {
        if (config('application.users.config.allow_user_reset', false)) {
            try {
                app(UserResetter::class)->finishReset(
                    array_merge($request->all(), ['userId' => $userId, 'code' => $code])
                );
            } catch (UserNotFoundException $e) {
                return redirect()->back()->withInput()
                    ->withError(trans('User no longer exists'));
            } catch (InvalidOrExpiredResetCode $e) {
                return redirect()->back()->withInput()
                    ->withError(trans('Invalid reset code'));
            }

            return redirect()->route('login')
                ->withSuccess(trans('Password reset'));
        }
    }

    public function getLogout()
    {
        $this->auth->logout();

        session()->forget('url.intented');

        return redirect()->route('login');
    }
}
