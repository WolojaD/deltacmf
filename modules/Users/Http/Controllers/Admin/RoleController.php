<?php

namespace Modules\Users\Http\Controllers\Admin;

use Modules\Users\Repositories\RoleInterface;
use Modules\Users\Permissions\PermissionManager;

class RoleController extends BaseUserModuleController
{
    /**
     * @var RoleInterface
     */
    private $role;

    public function __construct(PermissionManager $permissions, RoleInterface $role)
    {
        $this->permissions = $permissions;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->role->delete($id);

        return redirect()->route('backend.role.index')
            ->withSuccess(trans('users::messages.api.role deleted'));
    }
}
