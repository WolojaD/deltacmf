<?php

namespace Modules\Users\Http\Controllers\Admin;

use Modules\Users\Repositories\UserInterface;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Modules\Users\Services\SentinelImpersonateManager;
use Modules\Core\Http\Controllers\CorePublicController;

class ImpersonateController extends CorePublicController
{
    /**
     * @var UserInterface
     */
    private $user;
    private $manager;

    public function __construct(UserInterface $user, SentinelImpersonateManager $manager)
    {
        parent::__construct();

        $this->user = $user;
        $this->manager = $manager;
    }

    public function impersonateTake(int $id)
    {
        $user = $this->user->find($id);

        if (!$user || null === $user->roles->first()) {
            return redirect()->route('backend.users.index', ['type' => 'admin'])
                ->withError('Something go wrong with user or users role!');
        } elseif (!Activation::completed($user)) {
            return redirect()->route('backend.users.index', ['type' => 'admin'])
                ->withError('Something go wrong with user or users role!');
        }

        if ($this->manager->take($this->manager->findUserById($this->auth->id()), $user)) {
            return redirect()->route(config('laravel-impersonate.take_redirect_to'));
        }

        return redirect()->route('backend.users.index', ['type' => 'admin'])
            ->withError('Something go wrong!');
    }

    public function impersonateLeave()
    {
        if ($this->manager->leave()) {
            return redirect()->route(config('laravel-impersonate.leave_redirect_to'), ['type' => 'admin']);
        }

        return response()->json([
            'errors' => true,
            'message' => 'Something go wrong!',
        ]);
    }
}
