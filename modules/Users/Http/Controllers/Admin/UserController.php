<?php

namespace Modules\Users\Http\Controllers\Admin;

use Modules\Users\Contracts\Authentication;
use Modules\Users\Repositories\RoleInterface;
use Modules\Users\Repositories\UserInterface;
use Modules\Users\Permissions\PermissionManager;

class UserController extends BaseUserModuleController
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var RoleInterface
     */
    private $role;

    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @param PermissionManager $permissions
     * @param UserInterface    $user
     * @param RoleInterface    $role
     * @param Authentication    $auth
     */
    public function __construct(PermissionManager $permissions, UserInterface $user, RoleInterface $role, Authentication $auth)
    {
        $this->permissions = $permissions;
        $this->user = $user;
        $this->role = $role;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function destroy($id)
    {
        return $this->user->delete($id);
    }
}
