<?php

namespace Modules\Users\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Users\Permissions\PermissionManager;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

abstract class BaseUserModuleController extends CoreAdminController
{
    /**
     * @var PermissionManager
     */
    protected $permissions;

    /**
     * @param Request $request
     *
     * @return array
     */
    protected function mergeRequestWithPermissions(Request $request)
    {
        $permissions = $this->permissions->clean($request->permissions);

        return array_merge($request->all(), ['permissions' => $permissions]);
    }
}
