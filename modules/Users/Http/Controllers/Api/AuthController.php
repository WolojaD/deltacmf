<?php

namespace Modules\Users\Http\Controllers\Api;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class AuthController
{
    /**
     * Check users auth key.
     *
     * @return string
     */
    public function check()
    {
        if (Sentinel::check()) {
            return 1;
        }

        Sentinel::logout();

        return redirect()->route('login');
    }
}
