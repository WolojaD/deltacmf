<?php

namespace Modules\Users\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Users\Permissions\PermissionManager;
use Modules\Users\Traits\ValidateUserByRoleAccess;

class PermissionsController extends Controller
{
    use ValidateUserByRoleAccess;

    /**
     * @var PermissionManager
     */
    private $permissionManager;

    public function __construct(PermissionManager $permissionManager)
    {
        $this->permissionManager = $permissionManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return type
     */
    public function index()
    {
        return response()->json([
            'permissions' => validate_permissions_by_user_role($this->permissionManager->all()),
        ]);
    }

    public function hasPermission($user_id)
    {
        if ($this->userDeveloper()) {
            return response()->json([
                'hasPermission' => $this->userDeveloper(),
            ]);
        } elseif (intval($user_id) == auth()->user()->id) {
            return response()->json([
                'hasPermission' => true,
            ]);
        } else {
            return response()->json([
                'hasPermission' => config('application.config.allow_user_reset'),
            ]);
        }
    }
}
