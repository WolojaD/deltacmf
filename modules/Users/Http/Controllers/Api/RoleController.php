<?php

namespace Modules\Users\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Modules\Users\Repositories\RoleInterface;
use Modules\Users\Transformers\RoleTransformer;
use Modules\Users\Permissions\PermissionManager;
use Modules\Users\Http\Requests\CreateRoleRequest;
use Modules\Users\Http\Requests\UpdateRoleRequest;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Modules\Users\Transformers\FullRoleTransformer;

class RoleController extends Controller
{
    use ValidateUserByRoleAccess;

    /**
     * @var RoleInterface
     */
    private $role;

    /**
     * @var PermissionManager
     */
    private $permissions;

    public function __construct(RoleInterface $role, PermissionManager $permissions)
    {
        $this->role = $role;
        $this->permissions = $permissions;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return RoleTransformer::collection($this->role->serverPaginationFilteringFor($request));
    }

    /**
     * Show the form for updateing a resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        $role = $this->role->find($id);

        if (!$role) {
            abort(404);
        }

        return new FullRoleTransformer($role->load('users'));
    }

    public function findNew(EloquentRole $role)
    {
        return new FullRoleTransformer($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EloquentRole      $role
     * @param UpdateRoleRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(EloquentRole $role, UpdateRoleRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request);

        $this->role->update($role->id, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('users::messages.api.role updated'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoleRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request);

        $this->role->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('users::messages.api.role created'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $role = $this->role->find($id);

        if (!$role) {
            abort(404);
        }

        if ($role->slug == 'developer') {
            return response()->json([
                'errors' => true,
                'message' => trans('users::messages.api.role disable'),
            ]);
        }

        if (count($role->users)) {
            return response()->json([
                'errors' => true,
                'message' => trans('users::messages.api.role has users'),
            ]);
        }

        $this->role->delete($role->id);

        return response()->json([
            'errors' => false,
            'message' => trans('users::messages.api.role deleted'),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function mergeRequestWithPermissions(Request $request)
    {
        $permissions = $this->permissions->clean($request->get('permissions'), $request->get('is_admin'));

        return array_merge($request->all(), ['permissions' => $permissions]);
    }

    /**
     * Get all roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function allRoles()
    {
        if ($this->userDeveloper()) {
            return RoleTransformer::collection($this->role->all());
        }

        return RoleTransformer::collection($this->role->all()->reject(function ($role) {
            return 'developer' == $role->slug;
        }));
    }
}
