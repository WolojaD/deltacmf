<?php

namespace Modules\Users\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Users\Entities\UserToken;
use Modules\Users\Contracts\Authentication;
use Modules\Users\Repositories\UserTokenInterface;
use Modules\Users\Transformers\ApiKeysTransformer;

class ApiKeysController extends Controller
{
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @var UserTokenInterface
     */
    private $userToken;

    public function __construct(Authentication $auth, UserTokenInterface $userToken)
    {
        $this->auth = $auth;
        $this->userToken = $userToken;
    }

    public function index()
    {
        $tokens = $this->userToken->allForUser($this->auth->id());

        return ApiKeysTransformer::collection($tokens);
    }

    public function create()
    {
        $userId = $this->auth->id();
        $this->userToken->generateFor($userId);
        $tokens = $this->userToken->allForUser($userId);

        return response()->json([
            'errors' => false,
            'message' => trans('users::users.token generated'),
            'data' => ApiKeysTransformer::collection($tokens),
        ]);
    }

    public function destroy(UserToken $userToken)
    {
        $this->userToken->destroy($userToken);
        $tokens = $this->userToken->allForUser($this->auth->id());

        return response()->json([
            'errors' => false,
            'message' => trans('users::users.token deleted'),
            'data' => ApiKeysTransformer::collection($tokens),
        ]);
    }
}
