<?php

namespace Modules\Users\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Users\Entities\Sentinel\User;
use Modules\Users\Contracts\Authentication;
use Modules\Users\Repositories\UserInterface;
use Modules\Users\Transformers\UserTransformer;
use Modules\Users\Permissions\PermissionManager;
use Modules\Users\Events\UserHasBegunResetProcess;
use Modules\Users\Http\Requests\CreateUserRequest;
use Modules\Users\Http\Requests\UpdateUserRequest;
use Modules\Users\Transformers\FullUserTransformer;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var PermissionManager
     */
    private $permissions;

    public function __construct(UserInterface $user, PermissionManager $permissions)
    {
        $this->user = $user;
        $this->permissions = $permissions;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type = null)
    {
        $request->merge(['type' => $type]);

        return UserTransformer::collection($this->user->serverPaginationFilteringFor($request));
    }

    /**
     * Show the form for updateing a resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        $user = $this->user->find($id);

        if (!$user) {
            abort(404);
        }

        return new FullUserTransformer($user->load('roles'));
    }

    /**
     * Change user activation status.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleField(Request $request)
    {
        if (in_array((int) $request->get('items')[0], config('application.users.config.core_developers_id'))) {
            return response()->json([
                'errors' => true,
                'message' => trans('users::messages.api.user disabled'),
            ]);
        }

        if ('status' == $request->get('field')) {
            $data['activated'] = (bool) $request->get('position');
            $user = $this->user->find((int) $request->get('items')[0]);

            return (int) $this->user->getCheckForManualActivationFunction($user, $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User              $user_id
     * @param UpdateUserRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(User $user_id, UpdateUserRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request, $user_id->id);

        $this->user->updateAndSyncRoles($user_id->id, $data, $request->get('roles'));

        return response()->json([
            'errors' => false,
            'message' => trans('users::messages.api.user updated'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request);

        $this->user->createWithRoles($data, $request->get('roles'), $request->get('activated'));

        return response()->json([
            'errors' => false,
            'message' => trans('users::messages.api.user created'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        if (null !== $id && in_array($id, config('application.users.config.core_developers_id'))) {
            return response()->json([
                'errors' => true,
                'message' => trans('users::messages.api.user disabled'),
            ]);
        }

        $user = $this->user->find($id);

        if (!$user) {
            abort(404);
        }

        $this->user->delete($user->id);

        return response()->json([
            'errors' => false,
            'message' => trans('users::messages.api.user deleted'),
        ]);
    }

    /**
     * Send email with link for reset password.
     *
     * @param int $user_id
     * @param Authentication $auth
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResetPassword($user_id, Authentication $auth)
    {
        if (config('application.users.config.allow_user_reset', false)) {
            $user = $this->user->find($user_id);

            $code = $auth->createReminderCode($user);

            event(new UserHasBegunResetProcess($user, $code));

            return response()->json([
                'errors' => false,
                'message' => trans('users::messages.api.reset password email was sent'),
            ]);
        }

        return response()->json([
            'errors' => true,
            'message' => trans('users::messages.api.reset password email disabled'),
        ]);
    }

    /**
     * @param Request $request
     * @param null|int $user_id
     *
     * @return array
     */
    private function mergeRequestWithPermissions(Request $request, $user_id = null)
    {
        $permissions = $this->permissions->cleanForUsers($request->get('permissions'));

        if (null !== $user_id && in_array($user_id, config('application.users.config.core_developers_id'))) {
            $request['activated'] = true;
        }

        return array_merge($request->all(), ['permissions' => $permissions]);
    }
}
