<?php

/*
|--------------------------------------------------------------------------
| FrontEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register FrontEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "frontend" middleware group.
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::get('login', ['middleware' => 'auth.guest', 'as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'AuthController@postLogin']);

    Route::get('/impersonate/leave', ['as' => 'impersonate.leave', 'uses' => 'Admin\ImpersonateController@impersonateLeave']);

    if (config('application.users.config.allow_user_registration', false)) {
        Route::get('register', ['middleware' => 'auth.guest', 'as' => 'register', 'uses' => 'AuthController@getRegister']);
        Route::post('register', ['as' => 'register.post', 'uses' => 'AuthController@postRegister']);

        Route::get('activate/{userId}/{activationCode}', 'AuthController@getActivate');
    }

    if (config('application.users.config.allow_user_reset', false)) {
        Route::get('reset', ['as' => 'reset', 'uses' => 'AuthController@getReset']);
        Route::post('reset', ['as' => 'reset.post', 'uses' => 'AuthController@postReset']);
        Route::get('reset/{id}/{code}', ['as' => 'reset.complete', 'uses' => 'AuthController@getResetComplete']);
        Route::post('reset/{id}/{code}', ['as' => 'reset.complete.post', 'uses' => 'AuthController@postResetComplete']);
    }

    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
