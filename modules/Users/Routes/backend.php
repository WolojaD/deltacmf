<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'users'], function () {
    /*
    |--------------------------------------------------------------------------
    | User Routes
    |--------------------------------------------------------------------------
    */

    Route::get('{type}', ['as' => 'backend.users.index', 'uses' => 'UserController@index', 'middleware' => 'can:users.index'])->where('type', '(users|admin)');

    Route::get('create', ['as' => 'backend.users.create', 'uses' => 'UserController@create', 'middleware' => 'can:users.index']);

    Route::get('{user}/edit', ['as' => 'backend.users.edit', 'uses' => 'UserController@edit', 'middleware' => 'can:users.index'])->where('user', '[0-9]+');

    Route::get('/impersonate/take/{id}', ['as' => 'backend.users.impersonate', 'uses' => 'ImpersonateController@impersonateTake', 'middleware' => 'can:users.impersonate'])->where('id', '[0-9]+');

    /*
    |--------------------------------------------------------------------------
    | Role Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', ['as' => 'backend.roles.index', 'uses' => 'RoleController@index', 'middleware' => 'can:roles.index']);

        Route::get('create', ['as' => 'backend.roles.create', 'uses' => 'RoleController@create', 'middleware' => 'can:roles.create']);

        Route::get('{role}/edit', ['as' => 'backend.roles.edit', 'uses' => 'RoleController@edit', 'middleware' => 'can:roles.edit'])->where('role', '[0-9]+');
    });
});
