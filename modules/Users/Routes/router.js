import helpers from '@services/helpers'

const allFrontendLocales = helpers.appStorage().allFrontendLocales

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/users',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':type(users|admin)',
                name: 'api.backend.users',
                component: require('@/views/users/index').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Users',
                    breadcrumb: [
                        { name: 'Users' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.users.create',
                component: require('@/views/users/crud').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'User Create',
                    breadcrumb: [
                        { name: 'Users', link: 'api.backend.users' },
                        { name: 'User Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.users.edit',
                component: require('@/views/users/crud').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'User Edit',
                    breadcrumb: [
                        { name: 'Users', link: 'api.backend.users' },
                        { name: 'User Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/users/roles',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.roles',
                component: require('@/views/users/roles/index').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Roles',
                    breadcrumb: [
                        { name: 'Roles' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.roles.create',
                component: require('@/views/users/roles/crud').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Role Create',
                    breadcrumb: [
                        { name: 'Roles', link: 'api.backend.roles' },
                        { name: 'Role Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.roles.edit',
                component: require('@/views/users/roles/crud').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Role Edit',
                    breadcrumb: [
                        { name: 'Roles', link: 'api.backend.roles' },
                        { name: 'Role Edit' }
                    ]
                }
            }
        ]
    }
]
