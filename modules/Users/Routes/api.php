<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users', 'middleware' => ['api.token', 'auth.admin']], function () {
    /*
    |--------------------------------------------------------------------------
    | User Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => '/'], function () {
        Route::bind('user', function ($id) {
            return app(\Modules\Users\Repositories\UserInterface::class)->find($id);
        });

        Route::get('{type}', ['as' => 'api.backend.users.index', 'uses' => 'UserController@index', 'middleware' => 'token-can:users.index'])->where('type', '(users|admin)');

        Route::put('toggle-field', ['as' => 'api.backend.users.toggleField', 'uses' => 'UserController@toggleField', 'middleware' => 'token-can:users.edit']);

        Route::get('find/{id}', ['as' => 'api.backend.users.find', 'uses' => 'UserController@find', 'middleware' => 'token-can:users.index'])->where('id', '[0-9]+');

        Route::get('has-permission/{user_id}', ['as' => 'api.backend.permissions.hasPermission', 'uses' => 'PermissionsController@hasPermission', 'middleware' => 'token-can:users.index']);

        Route::post('{user_id}/edit', ['as' => 'api.backend.users.update', 'uses' => 'UserController@update', 'middleware' => 'token-can:users.edit'])->where('user_id', '[0-9]+');

        Route::post('/', ['as' => 'api.backend.users.store', 'uses' => 'UserController@store', 'middleware' => 'token-can:users.create']);

        Route::get('{user_id}/send-reset-password', ['as' => 'api.backend.users.sendResetPassword', 'uses' => 'UserController@sendResetPassword', 'middleware' => 'token-can:users.edit'])->where('user_id', '[0-9]+');

        Route::delete('{id}', ['as' => 'api.backend.users.destroy', 'uses' => 'UserController@destroy', 'middleware' => 'token-can:users.destroy'])->where('id', '[0-9]+');
    });

    /*
    |--------------------------------------------------------------------------
    | Role Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'roles'], function () {
        Route::bind('role', function ($id) {
            return app(\Modules\Users\Repositories\RoleInterface::class)->find($id);
        });

        Route::get('/', ['as' => 'api.backend.roles.index', 'uses' => 'RoleController@index', 'middleware' => 'token-can:roles.index']);

        Route::get('all-roles', ['as' => 'api.backend.roles.all-roles', 'uses' => 'RoleController@allRoles', 'middleware' => 'token-can:users.index']);

        Route::get('find/{id}', ['as' => 'api.backend.roles.find', 'uses' => 'RoleController@find', 'middleware' => 'token-can:roles.index'])->where('id', '[0-9]+');

        Route::get('find-new', ['as' => 'api.backend.roles.find-new', 'uses' => 'RoleController@findNew', 'middleware' => 'token-can:roles.create']);

        Route::post('{role}/edit', ['as' => 'api.backend.roles.update', 'uses' => 'RoleController@update', 'middleware' => 'token-can:roles.edit'])->where('role', '[0-9]+');

        Route::post('/', ['as' => 'api.backend.roles.store', 'uses' => 'RoleController@store', 'middleware' => 'token-can:roles.create']);

        Route::delete('{id}', ['as' => 'api.backend.roles.destroy', 'uses' => 'RoleController@destroy', 'middleware' => 'token-can:roles.destroy'])->where('id', '[0-9]+');
    });

    /*
    |--------------------------------------------------------------------------
    | Permission Routes
    |--------------------------------------------------------------------------
    */

    Route::get('permissions', ['as' => 'api.backend.permissions.index', 'uses' => 'PermissionsController@index', 'middleware' => 'token-can:roles.index']);

    Route::get('check', ['as' => 'api.backend.check', 'uses' => 'AuthController@check']);
});
