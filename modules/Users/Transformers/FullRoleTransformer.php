<?php

namespace Modules\Users\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Users\Permissions\PermissionManager;

class FullRoleTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $permissionsManager = app(PermissionManager::class);
        $permissions = $this->buildPermissionList($permissionsManager->all());

        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_admin' => $this->is_admin,
            'permissions' => $permissions,
            'role_permissions' => null,
            'users' => UserTransformer::collection($this->whenLoaded('users')),
        ];

        return $data;
    }

    private function buildPermissionList(array $permissionsConfig): array
    {
        $list = [];

        if (null === $permissionsConfig) {
            return $list;
        }

        foreach ($permissionsConfig as $mainKey => $subPermissions) {
            foreach ($subPermissions as $key => $permissionGroup) {
                foreach ($permissionGroup as $lastKey => $description) {
                    $list[strtolower($key) . '.' . $lastKey] = current_permission_value_for_role($this, $key, $lastKey);
                }
            }
        }

        return $list;
    }
}
