<?php

namespace Modules\Users\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ApiKeysTransformer extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'access_token' => $this->access_token,
            'created_at' => $this->created_at,
        ];
    }
}
