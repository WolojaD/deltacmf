<?php

namespace Modules\Users\Transformers;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class UserTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fullname' => $this->present()->fullname ?? '',
            'email' => $this->email,
            'impersonate' => $this->id,
            'role' => $this->roles->first()['name'],
            'status' => $this->isActivated(),
            'permissions' => $this->permissions ? $this->permissions : null,
            'last_login' => $this->last_login,
            'online_status' => ($this->last_login > Carbon::now()->subMinute(5)) ? true : false,
            'created_at' => $this->created_at->toRfc2822String() ?? Carbon::now(),
        ];
    }
}
