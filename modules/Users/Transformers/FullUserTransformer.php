<?php

namespace Modules\Users\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Users\Permissions\PermissionManager;

class FullUserTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $permissionsManager = app(PermissionManager::class);

        if (isset($this->permissions) && !empty($this->permissions)) {
            $permissions = $this->buildPermissionList($this->permissions, $permissionsManager->all());
        } else {
            $permissions = null;
        }

        $data = [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'activated' => $this->isActivated(),
            'last_login' => $this->last_login,
            'created_at' => $this->created_at,
            'permissions' => $permissions,
            'role_permissions' => $this->roles->first() ? $this->buildPermissionList($this->roles->first()->permissions, $permissionsManager->all()) : null,
            'roles' => $this->roles->pluck('id')[0] ?? '',
        ];

        return $data;
    }

    private function buildPermissionList(array $permissions, array $permissionsManager): array
    {
        foreach ($permissionsManager as $mainKey => $subPermissions) {
            foreach ($subPermissions as $key => $permissionGroup) {
                foreach ($permissionGroup as $lastKey => $description) {
                    $list[strtolower($key) . '.' . $lastKey] = current_permission_value_for_user($permissions, $key, $lastKey);
                }
            }
        }

        return $list;
    }
}
