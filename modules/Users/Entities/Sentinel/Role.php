<?php

namespace Modules\Users\Entities\Sentinel;

use Cartalyst\Sentinel\Roles\EloquentRole;

class Role extends EloquentRole
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $fillable = [
        'name',
        'slug',
        'is_admin',
        'permissions',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /**
     * Set mutator for the "permissions" attribute.
     *
     * @param mixed $permissions
     *
     * @return void
     */
    public function setPermissionsAttribute($permissions)
    {
        $this->attributes['permissions'] = $permissions ? json_encode($permissions) : null;
    }
}
