<?php

namespace Modules\Users\Entities\Sentinel;

use Illuminate\Auth\Authenticatable;
use Modules\Users\Entities\UserToken;
use Laracasts\Presenter\PresentableTrait;
use Cartalyst\Sentinel\Users\EloquentUser;
use Lab404\Impersonate\Models\Impersonate;
use Modules\Users\Presenters\UserPresenter;
use Modules\Users\Entities\UserEntitiInterface;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends EloquentUser implements UserEntitiInterface, AuthenticatableContract
{
    use PresentableTrait, Authenticatable, Impersonate;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $fillable = [
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $loginNames = ['email'];
    protected $presenter = UserPresenter::class;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function __construct(array $attributes = [])
    {
        $this->loginNames = config('application.users.config.login_columns');
        $this->fillable = config('application.users.config.fillable');

        if (config()->has('application.users.config.presenter')) {
            $this->presenter = config('application.users.config.presenter', UserPresenter::class);
        }

        if (config()->has('application.users.config.dates')) {
            $this->dates = config('application.users.config.dates', []);
        }

        if (config()->has('application.users.config.casts')) {
            $this->casts = config('application.users.config.casts', []);
        }

        parent::__construct($attributes);
    }

    public static function boot()
    {
        static::deleting(function ($model) {
            if (in_array($model->id, config('application.users.config.core_developers_id'))) {
                return false;
            }
        });

        parent::boot();
    }

    /**
     * Check user permissions from role or itself.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasAccess(string $permission)
    {
        $permissions = $this->getPermissionsInstance();

        return $permissions->hasAccess($permission);
    }

    public function isActivated()
    {
        if (Activation::completed($this)) {
            return true;
        }

        return false;
    }

    public function getFirstApiKey()
    {
        $userToken = $this->apiKeys->first();

        if (null === $userToken) {
            return '1X2x3X4x5X6x7X8x9X0x';
        }

        return $userToken->access_token;
    }

    /**
     * Get current user data for user-view-modal in navigation menu.
     *
     * @return string
     */
    public function getCurrentUser()
    {
        $role = $this->getRoles()->first();
        $permissions = $this->permissions ? $this->permissions : $role['permissions'];

        return json_encode([
            'id' => $this->id ?? '',
            'first_name' => $this->first_name ?? '',
            'last_name' => $this->last_name ?? '',
            'email' => $this->email,
            'role' => $role['name'] ?? '',
            'role_slug' => $role['slug'] ?? '',
            'permissions' => $permissions ?? '',
            'status' => $this->isActivated() ? 'Activated' : '',
            'last_login' => $this->last_login ?? '',
            'created_at' => $this->created_at->toRfc2822String() ?? '',
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function __call($method, $parameters)
    {
        $config = implode('.', ['application.users.config.relations', $method]);

        if (config()->has($config)) {
            $function = config()->get($config);
            $bound = $function->bindTo($this);

            return $bound();
        }

        return parent::__call($method, $parameters);
    }

    public function apiKeys()
    {
        return $this->hasMany(UserToken::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function hasRoleId($roleId)
    {
        return $this->roles()->whereId($roleId)->count() >= 1;
    }

    public function hasRoleSlug($slug)
    {
        return $this->roles()->whereSlug($slug)->count() >= 1;
    }

    public function hasRoleName($name)
    {
        return $this->roles()->whereName($name)->count() >= 1;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getFullnameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getHeadersOfCollectionFor($template = 'default')
    {
        if ($template == 'default') {
            return [
                'email' => 'E-mail',
                'first_name' => 'Имя',
                'last_name' => 'Фамилия',
                'created_at' => 'Создано',
            ];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
