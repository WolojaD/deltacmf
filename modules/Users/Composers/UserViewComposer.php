<?php

namespace Modules\Users\Composers;

use Illuminate\Contracts\View\View;
use Modules\Users\Contracts\Authentication;

class UserViewComposer
{
    /**
     * @var Authentication
     */
    private $auth;

    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function compose(View $view)
    {
        $view->with('user', $this->auth->user());
    }
}
