<?php

namespace Modules\Users\Providers;

use Illuminate\View\Compilers\BladeCompiler;
use Lab404\Impersonate\ImpersonateServiceProvider;
use Lab404\Impersonate\Services\ImpersonateManager;
use Modules\Users\Services\SentinelImpersonateManager;

class SentinelImpersonateServiceProvider extends ImpersonateServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $configPath = base_path('config/' . $this->configName . '.php');

        $this->mergeConfigFrom($configPath, $this->configName);

        $this->app->bind(ImpersonateManager::class, SentinelImpersonateManager::class);

        $this->app->singleton(ImpersonateManager::class, function ($app) {
            return new SentinelImpersonateManager($app);
        });

        $this->app->alias(SentinelImpersonateManager::class, 'impersonate');

        $router = $this->app['router'];
        $router->macro('impersonate-origin', function () use ($router) {
            $router->get('/impersonate-origin/take/{id}', '\Lab404\Impersonate\Controllers\ImpersonateController@take')->name('impersonate');
            $router->get('/impersonate-origin/leave', '\Lab404\Impersonate\Controllers\ImpersonateController@leave')->name('impersonate.leave');
        });

        $this->registerBladeDirectives();
    }

    /**
     * Register plugin blade directives.
     *
     * @param   void
     * @return  void
     */
    protected function registerBladeDirectives()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $bladeCompiler->directive('impersonating', function () {
                return '<?php if (app()["auth"]->check() && app()["auth"]->user()->isImpersonated()): ?>';
            });
            $bladeCompiler->directive('endImpersonating', function () {
                return '<?php endif; ?>';
            });

            $bladeCompiler->directive('canImpersonate', function () {
                return '<?php if (app()["auth"]->check() && app()["auth"]->user()->canImpersonate()): ?>';
            });
            $bladeCompiler->directive('endCanImpersonate', function () {
                return '<?php endif; ?>';
            });

            $bladeCompiler->directive('canBeImpersonated', function ($expression) {
                $user = trim($expression);

                return "<?php if (app()['auth']->check() && app()['auth']->user()->id != {$user}->id && {$user}->canBeImpersonated()): ?>";
            });
            $bladeCompiler->directive('endCanBeImpersonated', function () {
                return '<?php endif; ?>';
            });
        });
    }
}
