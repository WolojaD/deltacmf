<?php

namespace Modules\Users\Providers;

use Modules\Users\Events\UserHasRegistered;
use Modules\Users\Events\UserHasBegunResetProcess;
use Modules\Users\Events\Handlers\SendResetCodeEmail;
use Modules\Users\Events\Handlers\SendRegistrationConfirmationEmail;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UserHasBegunResetProcess::class => [
            SendResetCodeEmail::class,
        ],
    ];

    public function boot()
    {
        parent::boot();

        if (config('application.users.config.allow_send_user_registration_email', false)) {
            app('events')->listen(UserHasRegistered::class, SendRegistrationConfirmationEmail::class);
        }
    }
}
