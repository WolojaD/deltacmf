<?php

namespace Modules\Users\Providers;

use Modules\Users\Guards\Sentinel;
use Illuminate\Support\Facades\Auth;
use Modules\Users\Entities\UserToken;
use Modules\Users\Contracts\Authentication;
use Modules\Users\Http\Middleware\TokenCan;
use Modules\Users\Repositories\RoleInterface;
use Modules\Users\Repositories\UserInterface;
use Modules\Users\Http\Middleware\GuestMiddleware;
use Modules\Users\Repositories\UserTokenInterface;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Core\Traits\CanPublishConfigurationTrait;
use Modules\Users\Http\Middleware\AuthorisedApiToken;
use Modules\Users\Http\Middleware\LoggedInMiddleware;
use Cartalyst\Sentinel\Laravel\SentinelServiceProvider;
use Modules\Users\Http\Middleware\AuthorisedApiTokenAdmin;
use Modules\Users\Repositories\Cache\CacheUserTokenDecorator;
use Modules\Users\Http\Middleware\AuthorisedApiTokenDeveloper;
use Modules\Users\Repositories\Sentinel\IlluminateUserRepository;
use Modules\Users\Repositories\Eloquent\EloquentUserTokenRepository;

class UsersServiceProvider extends SentinelServiceProvider
{
    use CanPublishConfigurationTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * @var array
     */
    protected $providers = [
        'Sentinel' => SentinelServiceProvider::class,
    ];

    /**
     * @var array
     */
    protected $middleware = [
        'auth.guest' => GuestMiddleware::class,
        'logged.in' => LoggedInMiddleware::class,
        'api.token' => AuthorisedApiToken::class,
        'api.token.admin' => AuthorisedApiTokenAdmin::class,
        'api.token.developer' => AuthorisedApiTokenDeveloper::class,
        'token-can' => TokenCan::class,
    ];

    /**
     * Boot the application events.
     */
    public function boot()
    {
        parent::boot();

        $this->registerMiddleware();

        $this->publishConfig('users', 'config');
        $this->publishConfig('users', 'settings');
        $this->publishConfig('users', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        Auth::extend('sentinel-guard', function () {
            return new Sentinel();
        });
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        parent::register();

        $this->app->register($this->getUserPackageServiceProvider());

        $this->registerBindings();

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('users_sidebar', array_dot(trans('users::sidebar')));
            $event->load('users_crud-fields', array_dot(trans('users::crud-fields')));
            $event->load('users_table-fields', array_dot(trans('users::table-fields')));
            $event->load('users_permissions', array_dot(trans('users::permissions')));
            $event->load('users_modals', array_dot(trans('users::modals')));
            $event->load('roles_permissions', array_dot(trans('users::permissions')));
        });
    }

    /**
     * Registers the users.
     *
     * @return void
     */
    protected function registerUsers()
    {
        parent::registerUsers();

        $this->app->singleton('sentinel.users', function ($app) {
            $config = $app['config']->get('cartalyst.sentinel.users');

            return new IlluminateUserRepository(
                $app['sentinel.hasher'],
                $app['events'],
                $config['model']
            );
        });
    }

    private function registerBindings()
    {
        $driver = config('application.users.config.driver', 'Sentinel');

        $this->app->bind(
            UserInterface::class,
            "Modules\\Users\\Repositories\\{$driver}\\{$driver}UserRepository"
        );

        $this->app->bind(
            RoleInterface::class,
            "Modules\\Users\\Repositories\\{$driver}\\{$driver}RoleRepository"
        );

        $this->app->bind(
            Authentication::class,
            "Modules\\Users\\Repositories\\{$driver}\\{$driver}Authentication"
        );

        $this->app->bind(UserTokenInterface::class, function () {
            $repository = new EloquentUserTokenRepository(new UserToken());

            if (!config('app.cache')) {
                return $repository;
            }

            return new CacheUserTokenDecorator($repository);
        });
    }

    private function registerMiddleware()
    {
        foreach ($this->middleware as $name => $class) {
            $this->app['router']->aliasMiddleware($name, $class);
        }
    }

    private function getUserPackageServiceProvider()
    {
        $driver = config('application.users.config.driver', 'Sentinel');

        if (!isset($this->providers[$driver])) {
            throw new \Exception("Driver [{$driver}] does not exist");
        }

        return $this->providers[$driver];
    }
}
