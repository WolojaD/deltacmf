<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

class SentinelUserSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        // Create a developer_kirill user
        $developer_kirill = Sentinel::create(
            [
                'email' => 'gudvon@gmail.com',
                'password' => base64_decode('Z3Vkdm9uQGdtYWlsLmNvbQ=='),
                'first_name' => 'Kirill',
                'last_name' => 'Developer',
            ]
        );
        // Activate the developer_kirill directly
        $activation = Activation::create($developer_kirill);
        Activation::complete($developer_kirill, $activation->code);
        \App::make(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($developer_kirill->id);
        // Find the group using the group id by slug
        $developerGroup = Sentinel::findRoleBySlug('developer');
        // Assign the group to the developer_kirill
        $developerGroup->users()->attach($developer_kirill);

        // Create a developer_aleksey user
        $developer_aleksey = Sentinel::create(
            [
                'email' => 'adkashyn@gmail.com',
                'password' => base64_decode('WDRTdHc5SUd2cTUj'),
                'first_name' => 'Aleksey',
                'last_name' => 'Developer',
            ]
        );
        // Activate the developer_aleksey directly
        $activation = Activation::create($developer_aleksey);
        Activation::complete($developer_aleksey, $activation->code);
        \App::make(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($developer_aleksey->id);
        // Find the group using the group id by slug
        $developerGroup = Sentinel::findRoleBySlug('developer');
        // Assign the group to the developer_aleksey
        $developerGroup->users()->attach($developer_aleksey);

        // Create a developer_ann user
        $developer_ann = Sentinel::create(
            [
                'email' => 'ann@c-format.ua',
                'password' => 'ann@c-format.ua',
                'first_name' => 'Ann',
                'last_name' => 'Developer',
            ]
        );
        // Activate the developer_ann directly
        $activation = Activation::create($developer_ann);
        Activation::complete($developer_ann, $activation->code);
        \App::make(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($developer_ann->id);
        // Find the group using the group id by slug
        $developerGroup = Sentinel::findRoleBySlug('developer');
        // Assign the group to the developer_ann
        $developerGroup->users()->attach($developer_ann);

        // Create a developer user
        $developer = Sentinel::create(
            [
                'email' => 'cfdigital@c-format.ua',
                'password' => base64_decode('Y2ZkaWdpdGFsQGMtZm9ybWF0LnVh'),
                'first_name' => 'CF.Digital',
                'last_name' => 'CF.Digital',
            ]
        );
        // Activate the developer directly
        $activation = Activation::create($developer);
        Activation::complete($developer, $activation->code);
        \App::make(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($developer->id);
        // Find the group using the group id by slug
        $developerGroup = Sentinel::findRoleBySlug('developer');
        // Assign the group to the developer
        $developerGroup->users()->attach($developer);

        if (!app()->environment('production')) {
            // Create 5 Admin users
            for ($i = 1; $i < 6; ++$i) {
                $admin = Sentinel::create(
                    [
                        'first_name' => 'Admin-' . $i . ' ' . str_random(7),
                        'last_name' => str_random(11),
                        'email' => 'admin' . $i . '@mail.com',
                        'password' => 'admin' . $i . '@mail.com',
                    ]
                );
                // Activate the user directly
                $activation = Activation::create($admin);
                Activation::complete($admin, $activation->code);
                \App::make(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($admin->id);
                // Find the group using the group id by slug
                $adminGroup = Sentinel::findRoleBySlug('admin');
                // Assign the group to the developer
                $adminGroup->users()->attach($admin);
            }

            // Create 5 User users
            for ($i = 1; $i < 6; ++$i) {
                $user = Sentinel::create(
                    [
                        'first_name' => 'User-' . $i . ' ' . str_random(7),
                        'last_name' => str_random(11),
                        'email' => 'user' . $i . '@mail.com',
                        'password' => 'user' . $i . '@mail.com',
                    ]
                );
                // Activate the user directly
                $activation = Activation::create($user);
                Activation::complete($user, $activation->code);
                \App::make(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($user->id);
                // Find the group using the group id by slug
                $userGroup = Sentinel::findRoleBySlug('user');
                // Assign the group to the developer
                $userGroup->users()->attach($user);
            }
        }
    }
}
