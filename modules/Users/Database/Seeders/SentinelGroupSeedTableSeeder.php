<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class SentinelGroupSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        $groups = Sentinel::getRoleRepository();

        // Create an Developer group
        $groups->createModel()->create(
            [
                'name' => 'Developer',
                'slug' => 'developer',
                'is_admin' => 1,
            ]
        );

        // Create an Admin group
        $groups->createModel()->create(
            [
                'name' => 'Admin',
                'slug' => 'admin',
                'is_admin' => 1,
            ]
        );

        // Create an Seller group
        $groups->createModel()->create(
            [
                'name' => 'Seller',
                'slug' => 'seller',
                'is_admin' => 1,
            ]
        );

        // Create an User group
        $groups->createModel()->create(
            [
                'name' => 'User',
                'slug' => 'user',
                'is_admin' => 0,
            ]
        );

        // Save the permissions for Developer group
        $group = Sentinel::findRoleBySlug('developer');
        $group->permissions = [
            /* Dashboard */
            'dashboard.index' => true,

            /* Users */
            'users.index' => true,
            'users.create' => true,
            'users.edit' => true,
            'users.destroy' => true,
            'users.impersonate' => true,

            /* Roles */
            'roles.index' => true,
            'roles.create' => true,
            'roles.edit' => true,
            'roles.destroy' => true,

            /* Translation */
            'translation.frontend.index' => true,
            'translation.frontend.edit' => true,
            'translation.backend.index' => true,
            'translation.backend.edit' => true,

            /* Settings */
            'settings.index' => true,
            'settings.edit' => true,
            /* Settings developer */
            'settings.developer.index' => true,
            /* Settings image */
            'settings.image.index' => true,
            'settings.image.create' => true,
            'settings.image.edit' => true,
            'settings.image.destroy' => true,

            /* Forms */
            'forms.form.index' => true,
            'forms.form.create' => true,
            'forms.form.show' => true,
            'forms.form.edit' => true,
            'forms.form.destroy' => true,
            /* Forms fields */
            'forms.field.index' => true,
            'forms.field.create' => true,
            'forms.field.show' => true,
            'forms.field.edit' => true,
            'forms.field.destroy' => true,
            /* Forms field data */
            'forms.field.data.index' => true,
            'forms.field.data.create' => true,
            'forms.field.data.show' => true,
            'forms.field.data.edit' => true,
            'forms.field.data.destroy' => true,
            /* Forms regex */
            'forms.regex.index' => true,
            'forms.regex.create' => true,
            'forms.regex.show' => true,
            'forms.regex.edit' => true,
            'forms.regex.destroy' => true,
            /* Forms response */
            'forms.response.index' => true,
            'forms.response.create' => true,
            'forms.response.show' => true,
            'forms.response.edit' => true,
            'forms.response.destroy' => true,

            /* Banners */
            'banners.group.index' => true,
            'banners.group.create' => true,
            'banners.group.show' => true,
            'banners.group.edit' => true,
            'banners.group.destroy' => true,
            /* Banners banner */
            'banners.banner.index' => true,
            'banners.banner.create' => true,
            'banners.banner.show' => true,
            'banners.banner.edit' => true,
            'banners.banner.destroy' => true,
            /* Banners banner statistic */
            'banners.banner.statistic.index' => true,
            'banners.banner.statistic.create' => true,
            'banners.banner.statistic.edit' => true,
            'banners.banner.statistic.destroy' => true,

            /* Contacts */
            'contacts.group.index' => true,
            'contacts.group.create' => true,
            'contacts.group.show' => true,
            'contacts.group.edit' => true,
            'contacts.group.destroy' => true,
            /* Contacts contact */
            'contacts.contact.index' => true,
            'contacts.contact.create' => true,
            'contacts.contact.show' => true,
            'contacts.contact.edit' => true,
            'contacts.contact.destroy' => true,

            /* Page */
            'page.page.index' => true,
            'page.page.create' => true,
            'page.page.show' => true,
            'page.page.edit' => true,
            'page.page.destroy' => true,

            /* Articles */
            'articles.post.index' => true,
            'articles.post.create' => true,
            'articles.post.show' => true,
            'articles.post.edit' => true,
            'articles.post.destroy' => true,
            /* Articles categories */
            'articles.category.index' => true,
            'articles.category.create' => true,
            'articles.category.show' => true,
            'articles.category.edit' => true,
            'articles.category.destroy' => true,

            /* Stores */
            'stores.store.index' => true,
            'stores.store.create' => true,
            'stores.store.show' => true,
            'stores.store.edit' => true,
            'stores.store.destroy' => true,
            /* Stores cities */
            'stores.city.index' => true,
            'stores.city.create' => true,
            'stores.city.show' => true,
            'stores.city.edit' => true,
            'stores.city.destroy' => true,

            /* Infoblocks template*/
            'infoblocks.template.index' => true,
            'infoblocks.template.create' => true,
            'infoblocks.template.show' => true,
            'infoblocks.template.edit' => true,
            'infoblocks.template.destroy' => true,
            /* Infoblocks group */
            'infoblocks.group.index' => true,
            'infoblocks.group.create' => true,
            'infoblocks.group.show' => true,
            'infoblocks.group.edit' => true,
            'infoblocks.group.destroy' => true,
            /* Infoblocks infoblock */
            'infoblocks.infoblock.index' => true,
            'infoblocks.infoblock.create' => true,
            'infoblocks.infoblock.show' => true,
            'infoblocks.infoblock.edit' => true,
            'infoblocks.infoblock.destroy' => true,

            /* Text blocks */
            'textblocks.text.block.index' => true,
            'textblocks.text.block.create' => true,
            'textblocks.text.block.show' => true,
            'textblocks.text.block.edit' => true,
            'textblocks.text.block.destroy' => true,

            /* Email templates */
            'emailtemplates.email.template.index' => true,
            'emailtemplates.email.template.create' => true,
            'emailtemplates.email.template.show' => true,
            'emailtemplates.email.template.edit' => true,
            'emailtemplates.email.template.destroy' => true,

            /* Social networks */
            'socialnetworks.social.network.index' => true,
            'socialnetworks.social.network.create' => true,
            'socialnetworks.social.network.show' => true,
            'socialnetworks.social.network.edit' => true,
            'socialnetworks.social.network.destroy' => true,

            /* Fields */
            'fieldgenerator.index' => true,
            'fieldgenerator.create' => true,
            'fieldgenerator.show' => true,
            'fieldgenerator.edit' => true,
            'fieldgenerator.destroy' => true,

            /* Slider */
            'slider.slider.index' => true,
            'slider.slider.create' => true,
            'slider.slider.show' => true,
            'slider.slider.edit' => true,
            'slider.slider.destroy' => true,
            /* Slide in slider */
            'slider.slide.index' => true,
            'slider.slide.create' => true,
            'slider.slide.show' => true,
            'slider.slide.edit' => true,
            'slider.slide.destroy' => true,

            /* Gallery */
            'gallery.index' => true,
            'gallery.create' => true,
            'gallery.show' => true,
            'gallery.edit' => true,
            'gallery.destroy' => true,

            /* Ecommerce category */
            'ecommerce.category.index' => true,
            'ecommerce.category.create' => true,
            'ecommerce.category.show' => true,
            'ecommerce.category.edit' => true,
            'ecommerce.category.destroy' => true,
            /* Ecommerce product */
            'ecommerce.product.index' => true,
            'ecommerce.product.create' => true,
            'ecommerce.product.show' => true,
            'ecommerce.product.edit' => true,
            'ecommerce.product.destroy' => true,
            /* Ecommerce brand */
            'ecommerce.brand.index' => true,
            'ecommerce.brand.create' => true,
            'ecommerce.brand.show' => true,
            'ecommerce.brand.edit' => true,
            'ecommerce.brand.destroy' => true,
            /* Ecommerce product */
            'ecommerce.product.index' => true,
            'ecommerce.product.create' => true,
            'ecommerce.product.show' => true,
            'ecommerce.product.edit' => true,
            'ecommerce.product.destroy' => true,
            /* Ecommerce product value */
            'ecommerce.product.value.index' => true,
            'ecommerce.product.value.create' => true,
            'ecommerce.product.value.show' => true,
            'ecommerce.product.value.edit' => true,
            'ecommerce.product.value.destroy' => true,
            /* Ecommerce product version */
            'ecommerce.product.version.index' => true,
            'ecommerce.product.version.create' => true,
            'ecommerce.product.version.show' => true,
            'ecommerce.product.version.edit' => true,
            'ecommerce.product.version.destroy' => true,
            /* Ecommerce parameter */
            'ecommerce.parameter.index' => true,
            'ecommerce.parameter.create' => true,
            'ecommerce.parameter.show' => true,
            'ecommerce.parameter.edit' => true,
            'ecommerce.parameter.destroy' => true,
            /* Ecommerce info data */
            'ecommerce.info.data.index' => true,
            'ecommerce.info.data.create' => true,
            'ecommerce.info.data.show' => true,
            'ecommerce.info.data.edit' => true,
            'ecommerce.info.data.destroy' => true,
            /* Ecommerce parameter data */
            'ecommerce.parameter.data.index' => true,
            'ecommerce.parameter.data.create' => true,
            'ecommerce.parameter.data.show' => true,
            'ecommerce.parameter.data.edit' => true,
            'ecommerce.parameter.data.destroy' => true,
            /* Ecommerce delivery pay */
            'ecommerce.delivery.pay.index' => true,
            'ecommerce.delivery.pay.create' => true,
            'ecommerce.delivery.pay.show' => true,
            'ecommerce.delivery.pay.edit' => true,
            'ecommerce.delivery.pay.destroy' => true,
            /* Ecommerce delivery type */
            'ecommerce.delivery.type.index' => true,
            'ecommerce.delivery.type.create' => true,
            'ecommerce.delivery.type.show' => true,
            'ecommerce.delivery.type.edit' => true,
            'ecommerce.delivery.type.destroy' => true,
            /* Ecommerce order */
            'ecommerce.order.index' => true,
            'ecommerce.order.create' => true,
            'ecommerce.order.show' => true,
            'ecommerce.order.edit' => true,
            'ecommerce.order.destroy' => true,
            /* Ecommerce sale */
            'ecommerce.sale.index' => true,
            'ecommerce.sale.create' => true,
            'ecommerce.sale.show' => true,
            'ecommerce.sale.edit' => true,
            'ecommerce.sale.destroy' => true,
            /* Ecomerce stock */
            'ecommerce.stock.index' => true,
            'ecommerce.stock.create' => true,
            'ecommerce.stock.show' => true,
            'ecommerce.stock.edit' => true,
            'ecommerce.stock.destroy' => true,
            /* Ecomerce stock log */
            'ecommerce.stock.log.index' => true,
            'ecommerce.stock.log.create' => true,
            'ecommerce.stock.log.show' => true,
            'ecommerce.stock.log.edit' => true,
            'ecommerce.stock.log.destroy' => true,

            /* SEO structure */
            'seo.structure.index' => true,
            'seo.structure.create' => true,
            'seo.structure.show' => true,
            'seo.structure.edit' => true,
            'seo.structure.destroy' => true,
            /* SEO metatag */
            'seo.metatag.index' => true,
            'seo.metatag.create' => true,
            'seo.metatag.show' => true,
            'seo.metatag.edit' => true,
            'seo.metatag.destroy' => true,
            /* SEO redirect */
            'seo.redirect.index' => true,
            'seo.redirect.create' => true,
            'seo.redirect.show' => true,
            'seo.redirect.edit' => true,
            'seo.redirect.destroy' => true,
            /* SEO robots */
            'seo.robots.index' => true,
            'seo.robots.edit' => true,

            /* Newsletters followers */
            'newsletters.follower.index' => true,
            'newsletters.follower.create' => true,
            'newsletters.follower.show' => true,
            'newsletters.follower.edit' => true,
            'newsletters.follower.destroy' => true,

            /* Logs */
            'logs.folder.index' => true,
            'logs.mail.index' => true,
            'logs.mail.destroy' => true,
            'logs.job.index' => true,
            'logs.job.edit' => true,
            'logs.job.fail.index' => true,
        ];
        $group->save();

        if (!app()->environment('production')) {
            // Save the permissions for Admin group
            $group = Sentinel::findRoleBySlug('admin');
            $group->permissions = [
                /* Dashboard */
                'dashboard.index' => true,

                /* Users */
                'users.index' => true,
                'users.create' => true,
                'users.edit' => true,
                'users.destroy' => true,
                // 'users.impersonate' => true,

                /* Roles */
                'roles.index' => true,
                'roles.create' => true,
                'roles.edit' => true,
                'roles.destroy' => true,

                /* Translation */
                'translation.frontend.index' => true,
                'translation.frontend.edit' => true,
                // 'translation.backend.index' => true,
                // 'translation.backend.edit' => true,

                /* Settings */
                'settings.index' => true,
                'settings.edit' => true,
                /* Settings developer */
                // 'settings.developer.index' => true,
                /* Settings image */
                // 'settings.image.index' => true,
                // 'settings.image.create' => true,
                'settings.image.edit' => true,
                // 'settings.image.destroy' => true,

                /* Forms */
                'forms.form.index' => true,
                // 'forms.form.create' => true,
                'forms.form.show' => true,
                'forms.form.edit' => true,
                // 'forms.form.destroy' => true,
                /* Forms fields */
                'forms.field.index' => true,
                'forms.field.create' => true,
                'forms.field.show' => true,
                'forms.field.edit' => true,
                'forms.field.destroy' => true,
                /* Forms field data */
                'forms.field.data.index' => true,
                'forms.field.data.create' => true,
                'forms.field.data.show' => true,
                'forms.field.data.edit' => true,
                'forms.field.data.destroy' => true,
                /* Forms regex */
                // 'forms.regex.index' => true,
                // 'forms.regex.create' => true,
                // 'forms.regex.show' => true,
                // 'forms.regex.edit' => true,
                // 'forms.regex.destroy' => true,
                /* Forms response */
                'forms.response.index' => true,
                // 'forms.response.create' => true,
                'forms.response.show' => true,
                // 'forms.response.edit' => true,
                'forms.response.destroy' => true,

                /* Banners */
                'banners.group.index' => true,
                // 'banners.group.create' => true,
                'banners.group.show' => true,
                'banners.group.edit' => true,
                // 'banners.group.destroy' => true,
                /* Banners banner */
                'banners.banner.index' => true,
                'banners.banner.create' => true,
                'banners.banner.show' => true,
                'banners.banner.edit' => true,
                'banners.banner.destroy' => true,
                /* Banners banner statistic */
                'banners.banner.statistic.index' => true,
                // 'banners.banner.statistic.create' => true,
                // 'banners.banner.statistic.edit' => true,
                // 'banners.banner.statistic.destroy' => true,

                /* Contacts */
                'contacts.group.index' => true,
                // 'contacts.group.create' => true,
                'contacts.group.show' => true,
                'contacts.group.edit' => true,
                // 'contacts.group.destroy' => true,
                /* Contacts contact */
                'contacts.contact.index' => true,
                'contacts.contact.create' => true,
                'contacts.contact.show' => true,
                'contacts.contact.edit' => true,
                'contacts.contact.destroy' => true,

                /* Page */
                'page.page.index' => true,
                'page.page.create' => true,
                'page.page.show' => true,
                'page.page.edit' => true,
                'page.page.destroy' => true,

                /* Articles */
                'articles.post.index' => true,
                'articles.post.create' => true,
                'articles.post.show' => true,
                'articles.post.edit' => true,
                'articles.post.destroy' => true,
                /* Articles categories */
                'articles.category.index' => true,
                'articles.category.create' => true,
                'articles.category.show' => true,
                'articles.category.edit' => true,
                'articles.category.destroy' => true,

                /* Stores */
                'stores.store.index' => true,
                'stores.store.create' => true,
                'stores.store.show' => true,
                'stores.store.edit' => true,
                'stores.store.destroy' => true,
                /* Stores cities */
                'stores.city.index' => true,
                'stores.city.create' => true,
                'stores.city.show' => true,
                'stores.city.edit' => true,
                'stores.city.destroy' => true,

                /* Infoblocks */
                'infoblocks.group.index' => true,
                'infoblocks.group.create' => true,
                'infoblocks.group.edit' => true,
                'infoblocks.group.destroy' => true,
                /* Infoblocks infoblock */
                'infoblocks.infoblock.index' => true,
                'infoblocks.infoblock.create' => true,
                'infoblocks.infoblock.edit' => true,
                'infoblocks.infoblock.destroy' => true,

                /* Text blocks */
                'textblocks.text.block.index' => true,
                'textblocks.text.block.create' => true,
                'textblocks.text.block.show' => true,
                'textblocks.text.block.edit' => true,
                'textblocks.text.block.destroy' => true,

                /* Email templates */
                'emailtemplates.email.template.index' => true,
                // 'emailtemplates.email.template.create' => true,
                'emailtemplates.email.template.show' => true,
                'emailtemplates.email.template.edit' => true,
                // 'emailtemplates.email.template.destroy' => true,

                /* Social networks */
                'socialnetworks.social.network.index' => true,
                'socialnetworks.social.network.create' => true,
                'socialnetworks.social.network.show' => true,
                'socialnetworks.social.network.edit' => true,
                'socialnetworks.social.network.destroy' => true,

                /* Fields */
                // 'fieldgenerator.index' => true,
                // 'fieldgenerator.create' => true,
                // 'fieldgenerator.show' => true,
                // 'fieldgenerator.edit' => true,
                // 'fieldgenerator.destroy' => true,

                /* Slider */
                'slider.slider.index' => true,
                'slider.slider.create' => true,
                'slider.slider.show' => true,
                'slider.slider.edit' => true,
                'slider.slider.destroy' => true,
                /* Slide in slider */
                'slider.slide.index' => true,
                'slider.slide.create' => true,
                'slider.slide.show' => true,
                'slider.slide.edit' => true,
                'slider.slide.destroy' => true,

                /* Gallery */
                'gallery.index' => true,
                'gallery.create' => true,
                'gallery.show' => true,
                'gallery.edit' => true,
                'gallery.destroy' => true,

                /* Ecommerce category */
                'ecommerce.category.index' => true,
                'ecommerce.category.create' => true,
                'ecommerce.category.show' => true,
                'ecommerce.category.edit' => true,
                'ecommerce.category.destroy' => true,
                /* Ecommerce product */
                'ecommerce.product.index' => true,
                'ecommerce.product.create' => true,
                'ecommerce.product.show' => true,
                'ecommerce.product.edit' => true,
                'ecommerce.product.destroy' => true,
                /* Ecommerce brand */
                'ecommerce.brand.index' => true,
                'ecommerce.brand.create' => true,
                'ecommerce.brand.show' => true,
                'ecommerce.brand.edit' => true,
                'ecommerce.brand.destroy' => true,
                /* Ecommerce product value */
                'ecommerce.product.value.index' => true,
                'ecommerce.product.value.create' => true,
                'ecommerce.product.value.show' => true,
                'ecommerce.product.value.edit' => true,
                'ecommerce.product.value.destroy' => true,
                /* Ecommerce product version */
                'ecommerce.product.version.index' => true,
                'ecommerce.product.version.create' => true,
                'ecommerce.product.version.show' => true,
                'ecommerce.product.version.edit' => true,
                'ecommerce.product.version.destroy' => true,
                /* Ecommerce parameter */
                'ecommerce.parameter.index' => true,
                'ecommerce.parameter.create' => true,
                'ecommerce.parameter.show' => true,
                'ecommerce.parameter.edit' => true,
                'ecommerce.parameter.destroy' => true,
                /* Ecommerce info data */
                'ecommerce.info.data.index' => true,
                'ecommerce.info.data.create' => true,
                'ecommerce.info.data.show' => true,
                'ecommerce.info.data.edit' => true,
                'ecommerce.info.data.destroy' => true,
                /* Ecommerce parameter data */
                'ecommerce.parameter.data.index' => true,
                'ecommerce.parameter.data.create' => true,
                'ecommerce.parameter.data.show' => true,
                'ecommerce.parameter.data.edit' => true,
                'ecommerce.parameter.data.destroy' => true,
                /* Ecommerce delivery pay */
                'ecommerce.delivery.pay.index' => true,
                'ecommerce.delivery.pay.create' => true,
                'ecommerce.delivery.pay.show' => true,
                'ecommerce.delivery.pay.edit' => true,
                'ecommerce.delivery.pay.destroy' => true,
                /* Ecommerce delivery type */
                'ecommerce.delivery.type.index' => true,
                'ecommerce.delivery.type.create' => true,
                'ecommerce.delivery.type.show' => true,
                'ecommerce.delivery.type.edit' => true,
                'ecommerce.delivery.type.destroy' => true,
                /* Ecommerce order */
                'ecommerce.order.index' => true,
                'ecommerce.order.create' => true,
                'ecommerce.order.show' => true,
                'ecommerce.order.edit' => true,
                'ecommerce.order.destroy' => true,
                /* Ecommerce sale */
                'ecommerce.sale.index' => true,
                'ecommerce.sale.create' => true,
                'ecommerce.sale.show' => true,
                'ecommerce.sale.edit' => true,
                'ecommerce.sale.destroy' => true,
                /* Ecomerce stock */
                'ecommerce.stock.index' => true,
                'ecommerce.stock.create' => true,
                'ecommerce.stock.show' => true,
                'ecommerce.stock.edit' => true,
                'ecommerce.stock.destroy' => true,
                /* Ecomerce stock log */
                'ecommerce.stock.log.index' => true,
                'ecommerce.stock.log.create' => true,
                'ecommerce.stock.log.show' => true,
                'ecommerce.stock.log.edit' => true,
                'ecommerce.stock.log.destroy' => true,

                /* SEO structure */
                'seo.structure.index' => true,
                'seo.structure.create' => true,
                'seo.structure.show' => true,
                'seo.structure.edit' => true,
                'seo.structure.destroy' => true,
                /* SEO metatag */
                'seo.metatag.index' => true,
                'seo.metatag.create' => true,
                'seo.metatag.show' => true,
                'seo.metatag.edit' => true,
                'seo.metatag.destroy' => true,
                /* SEO redirect */
                'seo.redirect.index' => true,
                'seo.redirect.create' => true,
                'seo.redirect.show' => true,
                'seo.redirect.edit' => true,
                'seo.redirect.destroy' => true,
                /* SEO robots */
                'seo.robots.index' => true,
                // 'seo.robots.edit' => true,

                /* Newsletters followers */
                'newsletters.follower.index' => true,
                'newsletters.follower.create' => true,
                'newsletters.follower.show' => true,
                'newsletters.follower.edit' => true,
                'newsletters.follower.destroy' => true,

                /* Logs */
                // 'logs.folder.index' => true,
                // 'logs.mail.index' => true,
                // 'logs.mail.destroy' => true,
                // 'logs.job.index' => true,
                // 'logs.job.edit' => true,
                // 'logs.job.fail.index' => true,
            ];
            $group->save();

            // Save the permissions for Seller group
            $group = Sentinel::findRoleBySlug('seller');
            $group->permissions = [
                /* Dashboard */
                'dashboard.index' => true,

                /* Ecommerce category */
                'ecommerce.category.index' => true,
                'ecommerce.category.create' => true,
                'ecommerce.category.show' => true,
                'ecommerce.category.edit' => true,
                'ecommerce.category.destroy' => true,
                /* Ecommerce product */
                'ecommerce.product.index' => true,
                'ecommerce.product.create' => true,
                'ecommerce.product.show' => true,
                'ecommerce.product.edit' => true,
                'ecommerce.product.destroy' => true,
                /* Ecommerce brand */
                'ecommerce.brand.index' => true,
                'ecommerce.brand.create' => true,
                'ecommerce.brand.show' => true,
                'ecommerce.brand.edit' => true,
                'ecommerce.brand.destroy' => true,
                /* Ecommerce product value */
                'ecommerce.product.value.index' => true,
                'ecommerce.product.value.create' => true,
                'ecommerce.product.value.show' => true,
                'ecommerce.product.value.edit' => true,
                'ecommerce.product.value.destroy' => true,
                /* Ecommerce product version */
                'ecommerce.product.version.index' => true,
                'ecommerce.product.version.create' => true,
                'ecommerce.product.version.show' => true,
                'ecommerce.product.version.edit' => true,
                'ecommerce.product.version.destroy' => true,
                /* Ecommerce parameter */
                'ecommerce.parameter.index' => true,
                'ecommerce.parameter.create' => true,
                'ecommerce.parameter.show' => true,
                'ecommerce.parameter.edit' => true,
                'ecommerce.parameter.destroy' => true,
                /* Ecommerce info data */
                'ecommerce.info.data.index' => true,
                'ecommerce.info.data.create' => true,
                'ecommerce.info.data.show' => true,
                'ecommerce.info.data.edit' => true,
                'ecommerce.info.data.destroy' => true,
                /* Ecommerce parameter data */
                'ecommerce.parameter.data.index' => true,
                'ecommerce.parameter.data.create' => true,
                'ecommerce.parameter.data.show' => true,
                'ecommerce.parameter.data.edit' => true,
                'ecommerce.parameter.data.destroy' => true,
                /* Ecommerce delivery pay */
                'ecommerce.delivery.pay.index' => true,
                'ecommerce.delivery.pay.create' => true,
                'ecommerce.delivery.pay.show' => true,
                'ecommerce.delivery.pay.edit' => true,
                'ecommerce.delivery.pay.destroy' => true,
                /* Ecommerce delivery type */
                'ecommerce.delivery.type.index' => true,
                'ecommerce.delivery.type.create' => true,
                'ecommerce.delivery.type.show' => true,
                'ecommerce.delivery.type.edit' => true,
                'ecommerce.delivery.type.destroy' => true,
                /* Ecommerce order */
                'ecommerce.order.index' => true,
                'ecommerce.order.create' => true,
                'ecommerce.order.show' => true,
                'ecommerce.order.edit' => true,
                'ecommerce.order.destroy' => true,
                /* Ecommerce sale */
                'ecommerce.sale.index' => true,
                'ecommerce.sale.create' => true,
                'ecommerce.sale.show' => true,
                'ecommerce.sale.edit' => true,
                'ecommerce.sale.destroy' => true,
                /* Ecomerce stock */
                'ecommerce.stock.index' => true,
                'ecommerce.stock.create' => true,
                'ecommerce.stock.show' => true,
                'ecommerce.stock.edit' => true,
                'ecommerce.stock.destroy' => true,
            ];
            $group->save();

            // Save the permissions for User group
            $group = Sentinel::findRoleBySlug('user');
            // $group->permissions = [
            //     /* Dashboard */
            //     'dashboard.index' => true,
            // ];
            $group->save();
        }
    }
}
