<?php

namespace Modules\Users\Repositories\Sentinel;

use Carbon\Carbon;
use Cartalyst\Sentinel\Users\IlluminateUserRepository as SentinelIlluminateUserRepository;

class IlluminateUserRepository extends SentinelIlluminateUserRepository
{
    public function recordLogin($user)
    {
        $path = request()->path();

        if (stristr($path, 'backend') !== false) {
            if (!str_contains($path, 'backend/users') or !str_contains($path, 'backend/telescope')) {
                $user->last_url = request()->headers->get('referer');
            }
        }

        $user->last_ip = (request()->ip() == '127.0.0.1') ? 'localhost' : request()->ip();
        $user->last_login = Carbon::now();

        return $user->save() ? $user : false;
    }
}
