<?php

namespace Modules\Users\Repositories\Sentinel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Users\Events\UserIsCreating;
use Modules\Users\Events\UserIsUpdating;
use Modules\Users\Events\UserWasCreated;
use Modules\Users\Events\UserWasUpdated;
use Illuminate\Database\Eloquent\Builder;
use Modules\Users\Entities\Sentinel\User;
use Modules\Users\Events\UserHasRegistered;
use Modules\Users\Repositories\UserInterface;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Modules\Users\Exceptions\UserNotFoundException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class SentinelUserRepository implements UserInterface
{
    use ValidateUserByRoleAccess;

    /**
     * @var \Modules\User\Entities\Sentinel\User
     */
    protected $user;

    /**
     * @var \Cartalyst\Sentinel\Roles\EloquentRole
     */
    protected $role;

    public function __construct()
    {
        $this->user = Sentinel::getUserRepository()->createModel();
        $this->role = Sentinel::getRoleRepository()->createModel();
    }

    /**
     * Returns all the users.
     *
     * @return object
     */
    public function all()
    {
        return $this->user->all();
    }

    /**
     * Create a user resource.
     *
     * @param array $data
     * @param bool  $activated
     *
     * @return mixed
     */
    public function create(array $data, $activated = false)
    {
        $this->hashPassword($data);

        event($event = new UserIsCreating($data));

        $user = $this->user->create($event->getAttributes());

        if ($activated) {
            $this->activateUser($user);

            event(new UserWasCreated($user));
        } else {
            event(new UserHasRegistered($user));
        }

        app(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($user->id);

        return $user;
    }

    /**
     * Create a user and assign roles to it.
     *
     * @param array $data
     * @param array $roles
     * @param bool  $activated
     *
     * @return User
     */
    public function createWithRoles($data, $roles, $activated = false)
    {
        $user = $this->create((array) $data, $activated);

        if (!empty($roles)) {
            $user->roles()->attach($roles);
        }

        return $user;
    }

    /**
     * Create a user and assign roles to it
     * But don't fire the user created event.
     *
     * @param array $data
     * @param array $roles
     * @param bool  $activated
     *
     * @return User
     */
    public function createWithRolesFromCli($data, $roles, $activated = false)
    {
        $this->hashPassword($data);
        $user = $this->user->create((array) $data);

        if (!empty($roles)) {
            $user->roles()->attach($roles);
        }

        if ($activated) {
            $this->activateUser($user);
        }

        return $user;
    }

    /**
     * Find a user by its ID.
     *
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        $data = $this->allWithBuilder()->find($id);

        return $data;
    }

    /**
     * Update a user.
     *
     * @param $user
     * @param $data
     *
     * @return mixed
     */
    public function update($user, $data)
    {
        $this->checkForNewPassword($data);

        event($event = new UserIsUpdating($user, $data));

        $user->fill($event->getAttributes());
        $user->save();

        event(new UserWasUpdated($user));

        return $user;
    }

    /**
     * @param $userId
     * @param $data
     * @param $roles
     *
     * @internal param $user
     *
     * @return mixed
     */
    public function updateAndSyncRoles($userId, $data, $roles)
    {
        $user = $this->user->find($userId);

        $this->checkForNewPassword($data);
        $this->checkForManualActivation($user, $data);

        event($event = new UserIsUpdating($user, $data));

        $attributes = $event->getAttributes();

        if ($user->roles->first()) {
            if ($user->roles->first()->id !== $data['roles']) {
                $attributes['permissions'] = [];
            }
        }

        $user->fill($attributes);
        $user->save();

        event(new UserWasUpdated($user));

        if (!empty($roles)) {
            $user->roles()->sync($roles);
        }
    }

    /**
     * Deletes a user.
     *
     * @param $id
     *
     * @throws UserNotFoundException
     *
     * @return mixed
     */
    public function delete($id)
    {
        if ($user = $this->user->find($id)) {
            return $user->delete();
        }

        throw new UserNotFoundException();
    }

    /**
     * Find a user by its credentials.
     *
     * @param array $credentials
     *
     * @return mixed
     */
    public function findByCredentials(array $credentials)
    {
        return Sentinel::findByCredentials($credentials);
    }

    /**
     * Paginating, ordering and searching through pages for server side index table.
     *
     * @param Request $request
     *
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $data = $this->allWithBuilder();

        $data->whereNotIn('id', config('application.users.config.hidden_core_developers_id'));

        if ($request->exists('type') && null !== $request->get('type')) {
            if ($request->get('type') == 'admin') {
                $data->whereHas('roles', function ($q) {
                    $q->where('is_admin', 1);
                });
            } else {
                $data->doesntHave('roles')->orWhereHas('roles', function ($q) {
                    $q->where('is_admin', 0);
                });
            }
        }

        if ($request->exists('search') && null !== $request->get('search')) {
            $data->where(function ($query) use ($request) {
                $value = "%{$request->search}%";
                $query->where('first_name', 'like', $value)
                    ->orWhere('last_name', 'like', $value)
                    ->orWhere('email', 'like', $value)
                    ->orWhere('id', $value);
            });
        }

        if ($request->exists('filter') && null !== $request->get('filter')) {
            $data->where(function ($query) use ($request) {
                $value = "%{$request->filter}%";
                $query->where('first_name', 'like', $value)
                    ->orWhere('last_name', 'like', $value)
                    ->orWhere('email', 'like', $value)
                    ->orWhere('id', $value);
            });
        }

        // if ($request->exists('sort') && null !== $request->get('sort') && 'null' !== $request->get('sort')) {
        //     list($column, $direction) = explode('|', $request->get('sort'));

        //     if ($column == 'admin') {
        //         $data->whereHas('roles', function ($q) {
        //             $q->where('is_admin', 1);
        //         });

        //         $data->orderBy('id', $direction);
        //     } elseif ($column == 'user') {
        //         $data->whereHas('roles', function ($q) {
        //             $q->where('is_admin', 0);
        //         });

        //         $data->orderBy('id', $direction);
        //     } else {
        //         $data->orderBy($column, $direction);
        //     }
        // } else {
        //     $data->orderBy('created_at', 'desc');
        // }

        $data->orderBy('created_at', 'desc');

        return $data->paginate($request->get('per_page', 10));
    }

    public function allWithBuilder()
    {
        return $this->getByRole($this->userDeveloper());
    }

    /**
     * Validate by role data from database.
     *
     * @param type $data
     *
     * @return type
     */
    public function getByRole($permission): Builder
    {
        $data = $this->user->newQuery();

        if ($permission) {
            return $data;
        }

        return $data->whereNotIn('id', config('application.users.config.core_developers_id'))->whereDoesntHave('roles', function (Builder $query) {
            $query->where('slug', '=', 'developer');
        });
    }

    /**
     * Hash the password key.
     *
     * @param array $data
     */
    private function hashPassword(array &$data)
    {
        $data['password'] = Hash::make($data['password']);
    }

    /**
     * Check if there is a new password given
     * If not, unset the password field.
     *
     * @param array $data
     */
    private function checkForNewPassword(array &$data)
    {
        if (false === array_key_exists('password', $data)) {
            return;
        }

        if ('' === $data['password'] || null === $data['password']) {
            unset($data['password']);

            return;
        }

        $data['password'] = Hash::make($data['password']);
    }

    public function getCheckForManualActivationFunction($user, $data)
    {
        return $this->checkForManualActivation($user, $data);
    }

    /**
     * Check and manually activate or remove activation for the user.
     *
     * @param $user
     * @param array $data
     */
    private function checkForManualActivation($user, array &$data)
    {
        if (Activation::completed($user) && !$data['activated']) {
            app(\Modules\Users\Repositories\UserTokenInterface::class)->destroy($user->id);

            return Activation::remove($user);
        }

        if (!Activation::completed($user) && $data['activated']) {
            $activation = Activation::create($user);

            app(\Modules\Users\Repositories\UserTokenInterface::class)->generateFor($user->id);

            return Activation::complete($user, $activation->code);
        }
    }

    /**
     * Activate a user automatically.
     *
     * @param $user
     */
    private function activateUser($user)
    {
        $activation = Activation::create($user);

        Activation::complete($user, $activation->code);
    }
}
