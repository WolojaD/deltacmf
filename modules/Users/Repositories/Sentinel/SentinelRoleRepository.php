<?php

namespace Modules\Users\Repositories\Sentinel;

use Illuminate\Http\Request;
use Modules\Users\Events\RoleIsCreating;
use Modules\Users\Events\RoleIsUpdating;
use Modules\Users\Events\RoleWasCreated;
use Modules\Users\Events\RoleWasUpdated;
use Illuminate\Database\Eloquent\Builder;
use Modules\Users\Repositories\RoleInterface;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class SentinelRoleRepository implements RoleInterface
{
    use ValidateUserByRoleAccess;

    /**
     * @var \Cartalyst\Sentinel\Roles\EloquentRole
     */
    protected $role;

    public function __construct()
    {
        $this->role = Sentinel::getRoleRepository()->createModel();
    }

    /**
     * Return all the roles.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->role->all();
    }

    /**
     * Find a role by its id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        $data = $this->allWithBuilder()->find($id);

        return $data;
    }

    /**
     * Paginating, ordering and searching through pages for server side index table.
     *
     * @param Request $request
     *
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $roles = $this->allWithBuilder();

        if (null !== $request->get('search')) {
            $term = $request->get('search');
            $roles->where('name', 'LIKE', "%{$term}%")
                ->orWhere('slug', 'LIKE', "%{$term}%")
                ->orWhere('id', $term);
        }

        if (null !== $request->get('order_by') && 'null' !== $request->get('order')) {
            $order = 'ascending' === $request->get('order') ? 'asc' : 'desc';

            $roles->orderBy($request->get('order_by'), $order);
        } else {
            $roles->orderBy('created_at', 'desc');
        }

        return $roles->paginate($request->get('per_page', 10));
    }

    /**
     * @return Builder
     */
    public function allWithBuilder()
    {
        return $this->getByRole($this->userDeveloper());
    }

    /**
     * Validate by role data from database.
     *
     * @param type $data
     *
     * @return Builder
     */
    public function getByRole($permission): Builder
    {
        $data = $this->role->newQuery();

        if ($permission) {
            return $data;
        }

        return $data->where('slug', '!=', 'developer')->orWhere('id', '!=', 1);
    }

    /**
     * Create a role resource.
     *
     * @return mixed
     */
    public function create($data)
    {
        $data['slug'] = str_slug($data['name']);

        event($event = new RoleIsCreating($data));

        $role = $this->role->create($event->getAttributes());

        event(new RoleWasCreated($role));

        return $role;
    }

    /**
     * Update a role.
     *
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function update($id, $data)
    {
        $role = $this->role->find($id);

        event($event = new RoleIsUpdating($role, $data));

        $role->fill($event->getAttributes());
        $role->save();

        event(new RoleWasUpdated($role));

        return $role;
    }

    /**
     * Delete a role.
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        $role = $this->role->find($id);

        return $role->delete();
    }

    /**
     * Find a role by its name.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function findByName($name)
    {
        return Sentinel::findRoleByName($name);
    }
}
