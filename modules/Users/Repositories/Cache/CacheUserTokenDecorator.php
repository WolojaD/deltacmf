<?php

namespace Modules\Users\Repositories\Cache;

use Modules\Users\Repositories\UserTokenInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheUserTokenDecorator extends CoreCacheDecorator implements UserTokenInterface
{
    /**
     * @var UserTokenInterface
     */
    protected $repository;

    public function __construct(UserTokenInterface $repository)
    {
        parent::__construct();
        $this->entityName = 'user_tokens';
        $this->repository = $repository;
    }

    /**
     * Get all tokens for the given user
     *
     * @param int $userId
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allForUser($userId)
    {
        $this->remember(function () use ($userId) {
            return $this->repository->allForUser($userId);
        });
    }

    /**
     * @param int $userId
     *
     * @return \Modules\User\Entities\UserToken
     */
    public function generateFor($userId)
    {
        $this->clearCache();

        return $this->repository->generateFor($userId);
    }
}
