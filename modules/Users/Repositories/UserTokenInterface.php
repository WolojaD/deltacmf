<?php

namespace Modules\Users\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface UserTokenInterface extends CoreInterface
{
    /**
     * Get all tokens for the given user.
     *
     * @param int $userId
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allForUser($userId);

    /**
     * @param int $userId
     *
     * @return \Modules\Users\Entities\UserToken
     */
    public function generateFor($userId);

    /**
     * @param int $userId
     *
     * @return \Modules\Users\Entities\UserToken
     */
    public function destroy($userId);
}
