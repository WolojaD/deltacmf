<?php

namespace Modules\Users\Repositories\Eloquent;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\QueryException;
use Modules\Users\Repositories\UserTokenInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentUserTokenRepository extends EloquentCoreRepository implements UserTokenInterface
{
    /**
     * Get all tokens for the given user.
     *
     * @param int $userId
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allForUser($userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }

    /**
     * @param int $userId
     *
     * @return \Modules\Users\Entities\UserToken
     */
    public function generateFor($userId)
    {
        try {
            $uuid4 = Uuid::generate(4);
            $userToken = $this->model->create(['user_id' => $userId, 'access_token' => $uuid4]);
        } catch (QueryException $error) {
            return base64_encode('QueryException! generateFor' . $error);
        }

        return $userToken;
    }

    /**
     * @param int $userId
     *
     * @return \Modules\Users\Entities\UserToken
     */
    public function destroy($userId)
    {
        return $this->model->where('user_id', $userId)->delete();
    }
}
