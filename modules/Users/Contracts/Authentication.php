<?php

namespace Modules\Users\Contracts;

use Cartalyst\Sentinel\Users\UserInterface;

interface Authentication
{
    /**
     * Authenticate a user.
     *
     * @param array $credentials
     * @param bool  $remember
     *
     * @return mixed
     */
    public function login(array $credentials, $remember = false);

    /**
     * Register a new user.
     *
     * @param array $user
     *
     * @return bool
     */
    public function register(array $user);

    /**
     * Activate the given used id.
     *
     * @param int    $userId
     * @param string $code
     *
     * @return mixed
     */
    public function activate($userId, $code);

    /**
     * Assign a role to the given user.
     *
     * @param \Modules\Users\Repositories\UserInterface $user
     * @param \Modules\Users\Repositories\RoleInterface $role
     *
     * @return mixed
     */
    public function assignRole($user, $role);

    /**
     * Log the user out of the application.
     *
     * @return mixed
     */
    public function logout();

    /**
     * Create an activation code for the given user.
     *
     * @param \Modules\Users\Repositories\UserInterface $user
     *
     * @return mixed
     */
    public function createActivation($user);

    /**
     * Create a reminders code for the given user.
     *
     * @param \Modules\Users\Repositories\UserInterface $user
     *
     * @return mixed
     */
    public function createReminderCode($user);

    /**
     * Completes the reset password process.
     *
     * @param \Modules\Users\Repositories\UserInterface $user
     * @param string $code
     * @param string $password
     *
     * @return bool
     */
    public function completeResetPassword($user, $code, $password);

    /**
     * Determines if the current user has access to given permission.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function hasAccess(string $permission);

    /**
     * Check if the user is logged in.
     *
     * @return bool
     */
    public function check();

    /**
     * Get the currently logged in user.
     *
     * @return \Modules\Users\Entities\UserEntitiInterface
     */
    public function user();

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int
     */
    public function id();

    /**
     * Log a user manually in.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    public function logUserIn(UserInterface $user): UserInterface;
}
