<?php

namespace Modules\Users\Services;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Lab404\Impersonate\Events\TakeImpersonation;
use Lab404\Impersonate\Events\LeaveImpersonation;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Lab404\Impersonate\Services\ImpersonateManager;

class SentinelImpersonateManager extends ImpersonateManager
{
    use ValidateUserByRoleAccess;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param   int $id
     *
     * @return  Model
     */
    public function findUserById($id)
    {
        $user = Sentinel::getUserRepository()->findById($id);

        return $user;
    }

    /**
     * @param Model $from
     * @param Model $to
     *
     * @return bool
     */
    public function take($from, $to)
    {
        if ($this->userAdmin()) {
            try {
                $impersonator = Sentinel::getUser();
                $impersonated = $to;

                session()->put(config('laravel-impersonate.session_key'), $from->getKey());

                Cookie::queue('impersonator_email', $impersonator->email, 120);

                Sentinel::logout();
                Sentinel::login($to);

                $this->app['events']->fire(new TakeImpersonation($impersonator, $impersonated));

                return true;
            } catch (\Exception $e) {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    /**
     * @return  bool
     */
    public function leave()
    {
        try {
            $impersonated = Sentinel::getUser();
            $impersonator = $this->findUserById($this->getImpersonatorId());

            // TODO doesnt forget
            Cookie::queue(Cookie::forget('impersonator_email'));

            Sentinel::logout();

            if (null !== $impersonator) {
                Sentinel::login($impersonator);

                if ($this->userAdmin()) {
                    $impersonator = Sentinel::getUser();

                    $this->clear();

                    $this->app['events']->fire(new LeaveImpersonation($impersonator, $impersonated));

                    return true;
                }
            }

            abort(404);
        } catch (\Exception $e) {
            abort(404);
        }
    }
}
