<?php

namespace Modules\Users\Services;

use Modules\Users\Contracts\Authentication;
use Modules\Users\Events\UserHasRegistered;
use Modules\Users\Repositories\RoleInterface;

class UserRegistration
{
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @var RoleInterface
     */
    private $role;

    /**
     * @var array
     */
    private $input;

    public function __construct(Authentication $auth, RoleInterface $role)
    {
        $this->auth = $auth;
        $this->role = $role;
    }

    /**
     * @param array $input
     *
     * @return mixed
     */
    public function register(array $input)
    {
        $this->input = $input;

        $user = $this->createUser();

        $this->assignUserToUsersGroup($user);

        event(new UserHasRegistered($user));

        return $user;
    }

    private function createUser()
    {
        return $this->auth->register($this->input);
    }

    private function assignUserToUsersGroup($user)
    {
        $role = $this->role->findByName(config('application.users.config.default_role', 'User'));

        $this->auth->assignRole($user, $role);
    }

    /**
     * Check if the request input has a profile key.
     *
     * @return bool
     */
    private function hasProfileData()
    {
        return isset($this->input['profile']);
    }
}
