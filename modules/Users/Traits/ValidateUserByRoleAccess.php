<?php

namespace Modules\Users\Traits;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

trait ValidateUserByRoleAccess
{
    public function userDeveloper()
    {
        return $this->checkUserIsDeveloper();
    }

    public function userAdmin()
    {
        return $this->checkUserIsAdmin();
    }

    public function userUser()
    {
        return $this->checkUserIsUser();
    }

    private function checkUserIsDeveloper()
    {
        return Sentinel::check() && Sentinel::getUser()->inRole('developer');
    }

    private function checkUserIsAdmin()
    {
        return Sentinel::check() && (boolval(Sentinel::getUser()->roles->first()->is_admin ?? false) === boolval(1));
    }

    private function checkUserIsUser()
    {
        return Sentinel::check() && (boolval(Sentinel::getUser()->roles->first()->is_admin ?? false) === boolval(0));
    }
}
