<?php

namespace Modules\Users\Traits;

use Modules\Emailtemplates\Providers\EmailtemplatesTraitsProvider;

trait EmailTemplatesTrait
{
    /**
     * @param string $email
     * @param int $user_id
     * @param string $code
     *
     * @return type
     */
    public function sendResetCodeEmail($email, $user_id, $code)
    {
        $trait_provider = new EmailtemplatesTraitsProvider();

        $data_array = [
            'module' => 'Users',
            'method' => __FUNCTION__,
            'user_email' => $email,
            'variables' => [
                'user_id' => $user_id,
                'code' => $code,
            ],
        ];

        $trait_provider->handle($data_array);
    }
}
