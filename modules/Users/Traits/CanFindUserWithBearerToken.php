<?php

namespace Modules\Users\Traits;

use Modules\Users\Entities\UserEntitiInterface;
use Modules\Users\Repositories\UserTokenInterface;

trait CanFindUserWithBearerToken
{
    /**
     * @param string $token
     *
     * @return UserEntitiInterface|null
     */
    public function findUserWithBearerToken($token)
    {
        $token = app(UserTokenInterface::class)->findByAttributes(['access_token' => $this->parseToken($token)]);

        if (null === $token) {
            return null;
        }

        return $token->user;
    }

    private function parseToken($token)
    {
        return str_replace('Bearer ', '', $token);
    }
}
