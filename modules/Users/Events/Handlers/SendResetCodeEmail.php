<?php

namespace Modules\Users\Events\Handlers;

use Illuminate\Contracts\Mail\Mailer;
use Modules\Users\Mail\ResetPasswordEmail;
use Modules\Users\Traits\EmailTemplatesTrait;
use Modules\Users\Events\UserHasBegunResetProcess;

class SendResetCodeEmail
{
    use EmailTemplatesTrait;

    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(UserHasBegunResetProcess $event)
    {
        $this->sendResetCodeEmail($event->user->email, $event->user->id, $event->code);
        // $this->mailer->to($this->clear_email($event->user->email))->send(new ResetPasswordEmail($event->user, $event->code));
    }
}
