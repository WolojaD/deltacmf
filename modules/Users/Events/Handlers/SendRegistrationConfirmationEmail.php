<?php

namespace Modules\Users\Events\Handlers;

use Modules\Users\Mail\WelcomeEmail;
use Illuminate\Contracts\Mail\Mailer;
use Modules\Users\Contracts\Authentication;
use Modules\Users\Events\UserHasRegistered;

class SendRegistrationConfirmationEmail
{
    /**
     * @var AuthenticationRepository
     */
    private $auth;
    /**
     * @var Mailer
     */
    private $mail;

    public function __construct(Authentication $auth, Mailer $mail)
    {
        $this->auth = $auth;
        $this->mail = $mail;
    }

    public function handle(UserHasRegistered $event)
    {
        $user = $event->user;

        $activationCode = $this->auth->createActivation($user);

        $this->mail->to(clear_email($user->email))->send(new WelcomeEmail($user, $activationCode));
    }
}
