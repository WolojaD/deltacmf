<?php

namespace Modules\Users\Events;

use Modules\Core\Events\AbstractEntityHook;
use Modules\Core\Contracts\EntityIsChanging;
use Modules\Users\Entities\UserEntitiInterface;

final class UserIsUpdating extends AbstractEntityHook implements EntityIsChanging
{
    /**
     * @var UserEntitiInterface
     */
    private $user;

    public function __construct(UserEntitiInterface $user, array $data)
    {
        $this->user = $user;

        parent::__construct($data);
    }

    /**
     * @return UserEntitiInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}
