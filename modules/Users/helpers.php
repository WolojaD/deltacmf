<?php

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

if (false === function_exists('current_permission_value')) {
    function current_permission_value($model, $permissionTitle, $permissionAction)
    {
        $value = array_get($model->permissions, "$permissionTitle.$permissionAction");

        if (true === $value) {
            return 1;
        }

        return 0;
    }
}

if (false === function_exists('current_permission_value_for_role')) {
    function current_permission_value_for_role($model, $permissionTitle, $permissionAction)
    {
        $value = array_get($model->permissions, "$permissionTitle.$permissionAction");

        if (true === $value) {
            return 1;
        }

        return 0;
    }
}

if (false === function_exists('current_permission_value_for_user')) {
    function current_permission_value_for_user($permissions, $permissionTitle, $permissionAction)
    {
        $value = array_get($permissions, "$permissionTitle.$permissionAction");

        if (true === $value) {
            return 1;
        }

        return 0;
    }
}

if (false === function_exists('validate_permissions_by_user_role')) {
    function validate_permissions_by_user_role($permissions)
    {
        $user_permissions = Sentinel::getUser()->roles->first()->permissions;

        if (Sentinel::check()) {
            foreach ($permissions as $moduleName => $modulePermissions) {
                foreach ($modulePermissions as $permissionName => $permissionParameters) {
                    foreach ($permissionParameters as $key => $value) {
                        if (!isset($user_permissions[$permissionName . '.' . $key])) {
                            unset($permissions[$moduleName][$permissionName][$key]);

                            if (!count($permissions[$moduleName][$permissionName])) {
                                unset($permissions[$moduleName][$permissionName]);
                            }

                            if (!count($permissions[$moduleName])) {
                                unset($permissions[$moduleName]);
                            }
                        }
                    }
                }
            }

            return $permissions;
        }

        return abort(404);
    }
}
