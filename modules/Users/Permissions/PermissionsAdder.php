<?php

namespace Modules\Users\Permissions;

use Modules\Users\Repositories\RoleInterface;

final class PermissionsAdder
{
    /**
     * @var string
     */
    private $moduleName;

    /**
     * @var RoleInterface
     */
    private $role;

    public function __construct($moduleName)
    {
        $this->moduleName = $moduleName;
        $this->role = app(RoleInterface::class);
    }

    public function addAll()
    {
        $permissions = $this->buildPermissionList();
        $role = $this->role->find(1);

        foreach ($permissions as $permission) {
            $role->addPermission($permission);
            $role->save();
        }
    }

    /**
     * @return array
     */
    private function buildPermissionList(): array
    {
        $permissionsConfig = config('application.' . strtolower($this->moduleName) . '.permissions');
        $list = [];

        if (null === $permissionsConfig) {
            return $list;
        }

        foreach ($permissionsConfig as $mainKey => $subPermissions) {
            foreach ($subPermissions as $key => $description) {
                $list[] = $mainKey . '.' . $key;
            }
        }

        return $list;
    }
}
