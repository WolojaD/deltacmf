<?php

namespace Modules\Users\Permissions;

use Nwidart\Modules\Module;
use Modules\Users\Traits\ValidateUserByRoleAccess;

class PermissionManager
{
    use ValidateUserByRoleAccess;

    /**
     * @var Module
     */
    private $module;

    public function __construct()
    {
        $this->module = app('modules');
    }

    /**
     * Get the permissions from all the enabled modules.
     *
     * @return array
     */
    public function all()
    {
        $permissions = [];

        foreach ($this->getByRole() as $enabledModule) {
            $configuration = config(strtolower('application.' . $enabledModule->getName()) . '.permissions');

            if ($configuration) {
                $permissions[$enabledModule->getName()] = $configuration;
            }
        }

        return $permissions;
    }

    /**
     * Get modules permissions by user role.
     *
     * @return type
     */
    private function getByRole()
    {
        $allEnabledModules = $this->module->allEnabled();

        foreach (config('application.users.config.developer_modules') as $module) {
            unset($allEnabledModules[$module]);
        }

        return $allEnabledModules;
    }

    /**
     * Return a correctly type casted permissions array.
     *
     * @param $permissions
     * @param int $is_admin
     *
     * @return array
     */
    public function clean($permissions, int $is_admin)
    {
        if (!$permissions) {
            return [];
        }

        if ($is_admin == 1) {
            $cleanedPermissions = ['dashboard.index' => true];

            foreach ($permissions as $permissionName => $checkedPermission) {
                if (null !== $this->getState($checkedPermission)) {
                    $cleanedPermissions[$permissionName] = $this->getState($checkedPermission);
                }
            }

            return $cleanedPermissions;
        }

        return null;
    }

    /**
     * Return a correctly type casted permissions array.
     *
     * @param $permissions
     *
     * @return array
     */
    public function cleanForUsers($permissions)
    {
        if (!$permissions) {
            return [];
        }

        $cleanedPermissions = ['dashboard.index' => true];

        foreach ($permissions as $permissionName => $checkedPermission) {
            if (null !== $this->getState($checkedPermission)) {
                $cleanedPermissions[$permissionName] = $this->getState($checkedPermission);
            }
        }

        return $cleanedPermissions;
    }

    /**
     * @param $checkedPermission
     *
     * @return bool
     */
    protected function getState($checkedPermission)
    {
        if ('1' === $checkedPermission || 1 === $checkedPermission) {
            return true;
        }

        if ('-1' === $checkedPermission || -1 === $checkedPermission) {
            return false;
        }

        return null;
    }

    /**
     * Are all of the permissions passed of false value?
     *
     * @param array $permissions Permissions array
     *
     * @return bool
     */
    public function permissionsAreAllFalse(array $permissions)
    {
        $uniquePermissions = array_unique($permissions);

        if (count($uniquePermissions) > 1) {
            return false;
        }

        $uniquePermission = reset($uniquePermissions);
        $cleanedPermission = $this->getState($uniquePermission);

        return false === $cleanedPermission;
    }
}
