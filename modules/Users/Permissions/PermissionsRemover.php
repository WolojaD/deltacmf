<?php

namespace Modules\Users\Permissions;

use Modules\Users\Repositories\RoleInterface;
use Modules\Users\Repositories\UserInterface;

class PermissionsRemover
{
    /**
     * @var string
     */
    private $moduleName;

    /**
     * @var RoleInterface
     */
    private $role;

    /**
     * @var UserInterface
     */
    private $user;

    public function __construct($moduleName)
    {
        $this->moduleName = $moduleName;
        $this->role = app(RoleInterface::class);
        $this->user = app(UserInterface::class);
    }

    public function removeAll()
    {
        $permissions = $this->buildPermissionList();

        if (0 === count($permissions)) {
            return;
        }

        foreach ($this->role->all() as $role) {
            foreach ($permissions as $permission) {
                $role->removePermission($permission);
                $role->save();
            }
        }

        foreach ($this->user->all() as $user) {
            foreach ($permissions as $permission) {
                $user->removePermission($permission);
                $user->save();
            }
        }
    }

    /**
     * @return array
     */
    private function buildPermissionList()
    {
        $permissionsConfig = config('application.' . strtolower($this->moduleName) . '.permissions');
        $list = [];

        if (null === $permissionsConfig) {
            return $list;
        }

        foreach ($permissionsConfig as $mainKey => $subPermissions) {
            foreach ($subPermissions as $key => $description) {
                $list[] = $mainKey . '.' . $key;
            }
        }

        return $list;
    }
}
