<?php

namespace Modules\Settings\Entities;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntityTrait;
use Modules\Core\Traits\SearchableSortableTrait;
use Intervention\Image\Facades\Image as InterventionImage;

class Image extends Model
{
    use NamespacedEntityTrait, SearchableSortableTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $rootPath = 'i';
    protected $table = 'settings_images';
    protected $fillable = [
        'title',
        'width',
        'height',
        'quality',
        'is_crop',
    ];
    protected $casts = [
        'title' => 'string',
        'width' => 'iteger',
        'height' => 'iteger',
        'quality' => 'iteger',
        'is_crop' => 'boolean',
    ];
    protected $supportedFileExtensions = ['jpg', 'png', 'jpeg'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return type
     */
    public static function boot()
    {
        static::updating(function ($img) {
            File::deleteDirectory(public_path('storage/' . $img->rootPath . '/' . $img->original['title']));
        });

        static::deleted(function ($img) {
            File::deleteDirectory(public_path('storage/' . $img->rootPath . '/' . $img->original['title']));
        });

        parent::boot();
    }

    public function getKeyName()
    {
        return 'title';
    }

    public function getMinified($path = '')
    {
        $origin_path = public_path('storage' . DIRECTORY_SEPARATOR . 'origin' . DIRECTORY_SEPARATOR . $path);
        $minified_path = public_path('storage' . DIRECTORY_SEPARATOR . $this->rootPath . DIRECTORY_SEPARATOR . $this->title . DIRECTORY_SEPARATOR . $path);

        if (!$this->isImagePath($origin_path)) {
            abort(404);
        }
        $this->makeFolder($minified_path);

        if (!($path && is_file($origin_path))) {
            abort(404);
        }

        if (is_file($minified_path)) {
            return $minified_path;
        }

        return $this->createImage($origin_path)->encode('jpeg')->interlace()->save($minified_path, $this->quality);
    }

    /**
     * Make image file from given data.
     *
     * @return type
     */
    protected function createImage($path)
    {
        $image = InterventionImage::make($path);

        if ($this->is_crop) {
            $image_landscapier = ($image->width() / $image->height()) >= ($this->width / $this->height);

            $image = $image_landscapier ? $this->resizeByHeight($image) : $this->resizeByWidth($image);

            return $image->crop($this->width, $this->height);
        }

        return $image->width() <= $image->height() ? $this->resizeByHeight($image) : $this->resizeByWidth($image);
    }

    protected function resizeByWidth($image)
    {
        return $image->resize($this->width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }

    protected function resizeByHeight($image)
    {
        return $image->resize(null, $this->height, function ($constraint) {
            $constraint->aspectRatio();
        });
    }

    protected function makeFolder($path)
    {
        $dirname = pathinfo($path, PATHINFO_DIRNAME);

        if (!file_exists($dirname)) {
            mkdir($dirname, 0777, true);
        }
    }

    protected function isImagePath($path)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        return in_array(strtolower($extension), $this->supportedFileExtensions);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
