<?php

namespace Modules\Settings\Tests\Unit;

use Modules\Core\Tests\CoreTestCase;
use Modules\Settings\Repositories\SettingsRepository;

class SettingsTest extends CoreTestCase
{
    /**
     * @var \Modules\Setting\Support\SettingsSupport
     */
    protected $settings;

    public function setUp()
    {
        parent::setUp();

        $this->settings_repository = app(SettingsRepository::class);
        $this->settings = app('Modules\Settings\Support\SettingsSupport');
    }

    public function testBasicTest()
    {
        // Prepare
        $data = [
            'core::test-name' => 'DeltaCMS',
            'core::template' => 'delta',
        ];

        // Run
        $this->settings_repository->createOrUpdate($data);

        // Assert
        $settings = $this->settings->get('core::test-name');
        $this->assertEquals('DeltaCMS', $settings);
    }
}
