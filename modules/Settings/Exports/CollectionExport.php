<?php

namespace Modules\Settings\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CollectionExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $collection;

    protected $headings;

    public function __construct($collection, $headings = [])
    {
        $this->collection = $collection;
        $this->headings = $headings;
    }

    public function collection()
    {
        return $this->collection;
    }

    public function setCollection($collection)
    {
        return $this->collection = $collection;
    }

    public function headings(): array
    {
        return $this->headings;
    }
}
