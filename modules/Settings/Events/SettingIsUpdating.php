<?php

namespace Modules\Settings\Events;

use Modules\Settings\Entities\Settings;

class SettingIsUpdating
{
    private $settingName;
    private $settingValues;
    private $original;

    /**
     * @var Settings
     */
    private $setting;

    public function __construct(Settings $setting, $settingName, $settingValues)
    {
        $this->settingName = $settingName;
        $this->settingValues = $settingValues;
        $this->original = $settingValues;
        $this->setting = $setting;
    }

    /**
     * @return mixed
     */
    public function getSettingName()
    {
        return $this->settingName;
    }

    /**
     * @return mixed
     */
    public function getSettingValues()
    {
        return $this->settingValues;
    }

    /**
     * @param mixed $settingValues
     */
    public function setSettingValues($settingValues)
    {
        $this->settingValues = $settingValues;
    }

    /**
     * @return mixed
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * @return Settings
     */
    public function getSetting()
    {
        return $this->setting;
    }
}
