<?php

namespace Modules\Settings\Events;

use Modules\Settings\Entities\Settings;

class SettingWasCreated
{
    /**
     * @var Settings
     */
    public $setting;

    public function __construct(Settings $setting)
    {
        $this->setting = $setting;
    }
}
