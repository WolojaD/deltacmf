<?php

namespace Modules\Settings\Events;

use Modules\Settings\Entities\Settings;

class SettingWasUpdated
{
    /**
     * @var Settings
     */
    public $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }
}
