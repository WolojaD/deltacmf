<?php

namespace Modules\Settings\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ImageTransformer extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'height' => (string) $this->height,
            'width' => (string) $this->width,
            'is_crop' => $this->is_crop,
            'quality' => (string) $this->quality,
        ];
    }
}
