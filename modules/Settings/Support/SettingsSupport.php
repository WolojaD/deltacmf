<?php

namespace Modules\Settings\Support;

use Modules\Settings\Contracts\SettingsContract;
use Modules\Settings\Repositories\SettingsRepository;

class SettingsSupport implements SettingsContract
{
    /**
     * @var SettingRepository
     */
    private $setting;

    /**
     * @param SettingRepository $setting
     */
    public function __construct(SettingsRepository $setting)
    {
        $this->setting = $setting;
    }

    /**
     * Getting the setting.
     *
     * @param string $name
     * @param string $locale
     * @param string $default
     *
     * @return mixed
     */
    public function get($name, $locale = null, $default = null)
    {
        $defaultFromConfig = $this->getDefaultFromConfigFor($name);

        $setting = $this->setting->findByName($name);

        if (null === $setting) {
            return is_null($default) ? $defaultFromConfig : $default;
        }

        if ($setting) {
            return '' === trim($setting->plainValue) ? $defaultFromConfig : $setting->plainValue;
        }

        return $defaultFromConfig;
    }

    /**
     * Determine if the given configuration value exists.
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name)
    {
        $default = microtime(true);

        return $this->get($name, null, $default) !== $default;
    }

    /**
     * Set a given configuration value.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return \Modules\Setting\Entities\Setting
     */
    public function set($key, $value)
    {
        return $this->setting->create([
            'name' => $key,
            'plainValue' => $value,
        ]);
    }

    /**
     * Get the default value from the settings configuration file,
     * for the given setting name.
     *
     * @param string $name
     *
     * @return string
     */
    private function getDefaultFromConfigFor($name)
    {
        list($module, $settingName) = explode('::', $name);

        return config("application.$module.settings.$settingName.default", '');
    }
}
