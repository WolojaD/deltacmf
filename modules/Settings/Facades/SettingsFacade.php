<?php

namespace Modules\Settings\Facades;

use Illuminate\Support\Facades\Facade;

class SettingsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'application.settings';
    }
}
