<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'settings'], function () {
    Route::get('{vue?}', ['uses' => 'SettingsController@index']);
});

/*
|--------------------------------------------------------------------------
| Additional Settings Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'additional-settings'], function () {
    /*
    |--------------------------------------------------------------------------
    | Image Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'image'], function () {
        Route::get('/', ['as' => 'backend.settings_image', 'uses' => 'ImageController@index', 'middleware' => 'can:settings.image.index']);

        Route::get('{image}', ['as' => 'backend.settings_image.show', 'uses' => 'ImageController@index', 'middleware' => 'can:settings.image.index']);

        Route::get('{image}/create', ['as' => 'backend.settings_image.create', 'uses' => 'ImageController@create', 'middleware' => 'can:settings.image.index']);

        Route::get('{image}/edit', ['as' => 'backend.settings_image.edit', 'uses' => 'ImageController@edit', 'middleware' => 'can:settings.image.index']);
    });
});

//TODO STUPID PATH DELETE IT LATER (UPLOAD FILES)

Route::post('/file/Uploader/Path', ['as' => 'backend.settings_image', 'uses' => 'ImageController@fileUpload', 'middleware' => 'can:settings.image.index']);
