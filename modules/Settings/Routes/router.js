import helpers from '@services/helpers'

const allFrontendLocales = helpers.appStorage().allFrontendLocales

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/settings',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':module',
                name: 'api.backend.settings.module',
                component: require('@/views/settings/index').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    breadcrumb: [
                        { name: 'Core' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/additional-settings/image',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.settings_image',
                component: require('@/views/settings/image/index').default,
                meta: {
                    pageTitle: 'Image sizes',
                    breadcrumb: [
                        { name: 'Image sizes' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.settings_image.create',
                component: require('@/views/crud/origin/create').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Image size Create',
                    breadcrumb: [
                        { name: 'Image sizes', link: 'api.backend.settings_image' },
                        { name: 'Image size Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.settings_image.edit',
                component: require('@/views/crud/origin/edit').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Image size Edit',
                    breadcrumb: [
                        { name: 'Image sizes', link: 'api.backend.settings_image' },
                        { name: 'Image size Edit' }
                    ]
                }
            }
        ]
    }
]
