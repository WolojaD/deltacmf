<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'settings', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('get-modules-list', ['as' => 'api.backend.settings.getModulesList', 'uses' => 'SettingsController@getModulesList', 'middleware' => 'token-can:settings.index']);

    Route::get('{module}', ['as' => 'api.backend.settings.module', 'uses' => 'SettingsController@getModuleSettings', 'middleware' => 'token-can:settings.index']);

    Route::post('/', ['as' => 'api.backend.settings.store', 'uses' => 'SettingsController@store', 'middleware' => 'token-can:settings.edit']);
});

/*
|--------------------------------------------------------------------------
| Additional Settings Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'additional-settings', 'middleware' => ['api.token', 'auth.admin']], function () {
    /*
    |--------------------------------------------------------------------------
    | Core Buttons Routes
    |--------------------------------------------------------------------------
    */

    Route::get('cache-clear', ['as' => 'api.backend.settings.cacheClear', 'uses' => 'SettingsController@cacheClear', 'middleware' => 'token-can:settings.index']);

    Route::get('storage-link', ['as' => 'api.backend.settings.storageLink', 'uses' => 'SettingsController@storageLink', 'middleware' => 'token-can:settings.edit']);

    Route::get('cache-config', ['as' => 'api.backend.settings.cacheConfig', 'uses' => 'SettingsController@cacheConfig', 'middleware' => 'token-can:settings.edit']);

    /*
    |--------------------------------------------------------------------------
    | Image Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'image'], function () {
        Route::bind('bind_image', function ($id) {
            return app(\Modules\Settings\Repositories\ImageRepository::class)->findById($id);
        });

        Route::get('/', ['as' => 'api.backend.settings_image', 'uses' => 'ImageController@index', 'middleware' => 'token-can:settings.image.index']);

        Route::get('find/{bind_image}', ['as' => 'api.backend.settings_image.find', 'uses' => 'ImageController@find', 'middleware' => 'token-can:settings.image.index']);

        Route::get('find-new', ['as' => 'api.backend.settings_image.findNew', 'uses' => 'ImageController@findNew', 'middleware' => 'token-can:settings.image.index']);

        Route::post('/', ['as' => 'api.backend.settings_image.store', 'uses' => 'ImageController@store', 'middleware' => 'token-can:settings.image.create']);

        Route::post('{bind_image}', ['as' => 'api.backend.settings_image.update', 'uses' => 'ImageController@update', 'middleware' => 'token-can:settings.image.edit']);

        Route::put('toggle-field', ['as' => 'api.backend.settings_image.toggleField', 'uses' => 'ImageController@toggleField', 'middleware' => 'token-can:settings.image.edit']);

        Route::delete('{bind_image}', ['as' => 'api.backend.settings_image.destroy', 'uses' => 'ImageController@destroy', 'middleware' => 'token-can:settings.image.destroy']);
    });
});
