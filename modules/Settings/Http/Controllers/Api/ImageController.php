<?php

namespace Modules\Settings\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Settings\Entities\Image;
use Modules\Settings\Http\Requests\ImageRequest;
use Modules\Settings\Repositories\ImageRepository;
use Modules\Settings\Transformers\ImageTransformer;
use Modules\Settings\Transformers\FullImageTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class ImageController extends CoreApiController
{
    /**
     * @var ImageRepository
     */
    protected $repository;

    public function __construct(ImageRepository $image)
    {
        $this->repository = $image;
    }

    public function index(Request $request)
    {
        return ImageTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find(Image $image)
    {
        return new FullImageTransformer($image);
    }

    public function update(Image $image, ImageRequest $request)
    {
        $this->repository->update($image, $request->request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('Image updated'),
            'id' => $image->id,
        ]);
    }

    public function store(ImageRequest $request)
    {
        $image = $this->repository->create($request->except(0));

        return response()->json([
            'errors' => false,
            'message' => trans('Image created'),
            'id' => $image->id,
        ]);
    }

    public function destroy(Image $image)
    {
        $this->repository->destroy($image);

        return response()->json([
            'errors' => false,
            'message' => trans('Image deleted'),
        ]);
    }
}
