<?php

namespace Modules\Settings\Http\Controllers\Api;

use Nwidart\Modules\Module;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Artisan;
use Modules\Core\Foundation\Theme\ThemeManager;
use Modules\Settings\Http\Requests\SettingsRequest;
use Modules\Settings\Repositories\SettingsRepository;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class SettingsController extends CoreApiController
{
    /**
     * @var SettingsRepository
     */
    private $settings;

    /**
     * @var Module
     */
    private $module;

    /**
     * @var Store
     */
    private $session;

    /**
     * @var ThemeManager
     */
    private $themeManager;

    public function __construct(SettingsRepository $settings, Store $session, ThemeManager $themeManager)
    {
        $this->settings = $settings;
        $this->module = app('modules');
        $this->session = $session;
        $this->themeManager = $themeManager;
    }

    /**
     * Get all enabled modules.
     *
     * @return type
     */
    public function getModulesList()
    {
        return $this->settings->moduleSettings($this->module->allEnabled());
    }

    /**
     * Get current module settings and parameters.
     *
     * @param string $currentModule
     *
     * @return array
     */
    public function getModuleSettings(string $moduleName)
    {
        $module = \Module::find(base64_decode($moduleName));

        $data = [
            'plainModuleSettings' => $this->settings->plainModuleSettings($module->getLowerName()),
            'savedModuleSettings' => $this->settings->savedModuleSettings($module->getLowerName()),
            'themes' => $this->themeManager->allPublicThemes(),
            'locales' => config('application.core.available-locales'),
            'mail_driver' => ['1', '2'],
            'currentModuleName' => $module->getLowerName(),
        ];

        return $data;
    }

    /**
     * Save module data.
     *
     * @param SettingsRequest $request
     *
     * @return type
     */
    public function store(SettingsRequest $request)
    {
        $this->settings->createOrUpdate($request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('settings::messages.settings updated'),
        ]);
    }

    /**
     * Link storage.
     */
    public function storageLink()
    {
        $folder = base_path() . '/public/storage';

        try {
            if (file_exists($folder)) {
                if (is_link($folder)) {
                    unlink($folder);
                } else {
                    return response()->json([
                        'errors' => true,
                        'message' => trans('settings::messages.storage link.unlink error'),
                    ]);
                }
            }
        } catch (\Exception $error) {
            return response()->json([
                'errors' => true,
                'message' => trans('settings::messages.storage link.try error'),
            ]);
        }

        shell_exec('/usr/local/php72/bin/php ' . base_path() . '/artisan storage:link');
        shell_exec('php artisan storage:link');
        Artisan::call('storage:link');

        return response()->json([
            'errors' => false,
            'message' => trans('settings::messages.storage link.storage linked'),
        ]);
    }

    /**
     * Clear cache of all project.
     */
    public function cacheClear()
    {
        shell_exec('/usr/local/php72/bin/php ' . base_path() . '/artisan application:cache-clear');
        shell_exec('php artisan application:cache-clear');
        Artisan::call('application:cache-clear');

        cache()->tags(['settings', 'global'])->flush();

        return response()->json([
            'errors' => false,
            'message' => trans('settings::messages.сache cleared'),
        ]);
    }

    /**
     * Cache config for production mode.
     */
    public function cacheConfig()
    {
        shell_exec('/usr/local/php72/bin/php ' . base_path() . '/artisan config:clear');
        shell_exec('php artisan config:clear');
        Artisan::call('config:clear');

        shell_exec('/usr/local/php72/bin/php ' . base_path() . '/artisan config:cache');
        shell_exec('php artisan config:cache');
        Artisan::call('config:cache');

        return response()->json([
            'errors' => false,
            'message' => trans('settings::messages.сonfig cached'),
        ]);
    }
}
