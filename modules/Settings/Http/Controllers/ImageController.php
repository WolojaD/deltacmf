<?php

namespace Modules\Settings\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Modules\Settings\Repositories\ImageRepository;

class ImageController
{
    /**
     * @var ImageRepository
     */
    private $image;

    public function __construct(ImageRepository $image)
    {
        $this->image = $image;
    }

    public function show(Request $request)
    {
        return Image::make($this->image->show($request))->response();
    }
}
