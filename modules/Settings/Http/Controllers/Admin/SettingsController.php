<?php

namespace Modules\Settings\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class SettingsController extends CoreAdminController
{
    public function index()
    {
        return view('core::router_view');
    }
}
