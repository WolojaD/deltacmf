<?php

namespace Modules\Settings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Settings\Repositories\ImageRepository;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class ImageController extends CoreAdminController
{
    /**
     * @var ImageRepository
     */
    private $page;

    public function __construct(ImageRepository $page)
    {
        $this->page = $page;
    }

    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->image->delete($id);

        return redirect()->route('backend.image.index')
            ->withSuccess(trans('Image deleted'));
    }

    public function fileUpload(Request $request)
    {
        dd($request->all());
    }
}
