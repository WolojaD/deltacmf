<?php

namespace Modules\Settings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:191|unique:pages,slug|regex:/^[-a-z0-9]*$/',
            'width' => 'required|integer|min:1|max:25000',
            'height' => 'required|integer|min:1|max:25000',
            'quality' => 'required|integer|min:1|max:100',
        ];
    }

    public function messages()
    {
        return [];
    }
}
