<?php

namespace Modules\Settings\Blade\Facades;

use Illuminate\Support\Facades\Facade;

final class SettingsDirective extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'settings.directive';
    }
}
