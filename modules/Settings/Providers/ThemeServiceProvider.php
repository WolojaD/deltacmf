<?php

namespace Modules\Settings\Providers;

use Modules\Core\Providers\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerAllThemes();
        $this->setActiveTheme();
    }

    /**
     * Set the active theme based on the settings.
     */
    private function setActiveTheme()
    {
        if (!\Schema::hasTable('settings')) {
            return;
        }

        if ($this->inAdministration()) {
            $themeName = $this->app['config']->get('application.core.core.backend-theme');

            return $this->app['stylist']->activate($themeName, true);
        }

        $themeName = app('application.settings')->get('core::frontend_theme', null, 'simplest');

        return $this->app['stylist']->activate($themeName, true);
    }

    /**
     * Check if we are in the administration.
     *
     * @return bool
     */
    private function inAdministration()
    {
        $segment = config('laravellocalization.hideDefaultLocaleInURL', false) ? 1 : 2;

        return $this->app['request']->segment($segment) === $this->app['config']->get('application.core.core.admin-prefix') ||
            $this->app['request']->segment($segment + 1) === $this->app['config']->get('application.core.core.admin-prefix');
    }

    /**
     * Register all themes with activating them.
     */
    private function registerAllThemes()
    {
        $directories = $this->app['files']->directories(config('stylist.themes.paths', [base_path('/themes')])[0]);

        foreach ($directories as $directory) {
            $this->app['stylist']->registerPath($directory);
        }
    }
}
