<?php

namespace Modules\Settings\Providers;

use Modules\Settings\Entities\Image;
use Illuminate\Foundation\AliasLoader;
use Modules\Settings\Entities\Settings;
use Modules\Core\Providers\ServiceProvider;
use Modules\Settings\Facades\SettingsFacade;
use Modules\Settings\Blade\SettingsDirective;
use Modules\Settings\Support\SettingsSupport;
use Modules\Settings\Repositories\ImageRepository;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Settings\Repositories\SettingsRepository;
use Modules\Settings\Repositories\Cache\CacheSettingsDecorator;
use Modules\Settings\Repositories\Eloquent\EloquentImageRepository;
use Modules\Settings\Repositories\Eloquent\EloquentSettingsRepository;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        $this->app->singleton('application.settings', function ($app) {
            return new SettingsSupport($app[SettingsRepository::class]);
        });

        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('SettingsSupport', SettingsFacade::class);
        });

        $this->app->bind('settings.directive', function () {
            return new SettingsDirective();
        });

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('settings_sidebar', array_dot(trans('settings::sidebar')));
            $event->load('settings_table-fields', array_dot(trans('settings::table-fields')));
            $event->load('settings_permissions', array_dot(trans('settings::permissions')));
            $event->load('settings_settings', array_dot(trans('settings::settings')));
        });
    }

    public function boot()
    {
        $this->publishConfig('settings', 'permissions');
        $this->publishConfig('settings', 'config');

        $this->registerBladeTags();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->bind(SettingsRepository::class, function () {
            $repository = new EloquentSettingsRepository(new Settings());

            if (!config('app.cache')) {
                return $repository;
            }

            return new CacheSettingsDecorator($repository);
        });

        $this->app->bind(\Modules\Settings\Contracts\SettingsContract::class, SettingsSupport::class);

        $this->app->bind(ImageRepository::class, function () {
            $repository = new EloquentImageRepository(new Image());

            return $repository;
        });

        if (!\Schema::hasTable('settings')) {
            return;
        }

        $this->app['cache']
            ->tags(['settings', 'global'])
            ->rememberForever('application.administrator_emails', function () {
                return \Modules\Settings\Entities\Settings::where('name', 'emailtemplates::administrator_emails')->get();
            });
    }

    private function registerBladeTags()
    {
        $this->app['blade.compiler']->directive('settings', function ($value) {
            return "<?php echo SettingsDirective::show([$value]); ?>";
        });
    }
}
