<?php

namespace Modules\Settings\Providers;

use Modules\Settings\Events\SettingWasCreated;
use Modules\Settings\Events\SettingWasUpdated;
use Modules\Settings\Events\Handlers\ClearSettingsCache;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        SettingWasCreated::class => [
            ClearSettingsCache::class,
        ],
        SettingWasUpdated::class => [
            ClearSettingsCache::class,
        ],
    ];
}
