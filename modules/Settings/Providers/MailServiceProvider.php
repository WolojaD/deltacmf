<?php

namespace Modules\Settings\Providers;

use Modules\Core\Providers\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->setMailConfigurations();
    }

    private function setMailConfigurations()
    {
        if (!\Schema::hasTable('settings')) {
            return;
        }

        $mail_config = $this->app['cache']
            ->tags(['settings', 'global'])
            ->rememberForever('application.mail', function () {
                return \Modules\Settings\Entities\Settings::where('name', 'like', '%emailtemplates::mail_%')->get();
            });

        if ($mail_config) {
            $mail_config_array = [];

            foreach ($mail_config as $key => $value) {
                if ($value->name == 'emailtemplates::mail_from_address') {
                    $mail_config_array['from'] = [
                        'address' => $value->plainValue,
                    ];

                    continue;
                }

                if ($value->name == 'emailtemplates::mail_from_name') {
                    $mail_config_array['from'] += [
                        'name' => $value->plainValue,
                    ];

                    continue;
                }

                $mail_config_array[@end((explode('emailtemplates::mail_', $value->name, 2)))] = $value->plainValue;
            }

            $this->app['config']['mail'] = $mail_config_array;
        }
    }
}
