<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Settings\Repositories\ImageRepository;
use Modules\Settings\Repositories\SettingsRepository;

class SettingsDatabaseSeeder extends Seeder
{
    /**
     * @var SettingsRepository
     */
    private $settings;

    /**
     * @var ImageRepository
     */
    private $image;

    public function __construct(SettingsRepository $settings, ImageRepository $image)
    {
        $this->settings = $settings;
        $this->image = $image;
    }

    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        /**
         * Standard settings
         */
        $data = [
            // Dashboard
            'dashboard::site_name' => 'Delta',
            'dashboard::site_description' => 'Delta description',
            'dashboard::analytic_script_head' => null,
            'dashboard::analytic_script_body_begin' => null,
            'dashboard::analytic_script_body_end' => null,

            // Core
            'core::frontend_theme' => 'simplest',
            'core::backend_theme' => 'artisan',
            'core::locales' => array_keys(config('laravellocalization.defaultApplicationLocales')),

            // Articles
            'articles::posts_per_page' => 10,
            'articles::latest_posts_amount' => 5,

            // Users
            'users::allow_user_reset' => 0,
            'users::allow_user_registration' => 0,
            'users::allow_send_user_registration_email' => 0,

            // Emailtemplates
            'emailtemplates::mail_driver' => env('MAIL_DRIVER', 'smtp'),
            'emailtemplates::mail_host' => env('MAIL_HOST', 'smtp.mailgun.org'),
            'emailtemplates::mail_port' => env('MAIL_PORT', 587),
            'emailtemplates::mail_username' => env('MAIL_USERNAME'),
            'emailtemplates::mail_password' => env('MAIL_PASSWORD'),
            'emailtemplates::mail_from_address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),
            'emailtemplates::mail_from_name' => env('MAIL_FROM_NAME', 'Example'),
            'emailtemplates::mail_encryption' => env('MAIL_ENCRYPTION', 'tls'),
            'emailtemplates::administrator_emails' => null,

            // Page
            'page::page_menu_max_level' => null,

            //Newsletters
            'newsletters::send_to_administrator' => 0,
            'newsletters::email_list' => null,
        ];
        $this->settings->createOrUpdate($data);

        /**
         * Backend Image settings
         */
        $image = [
            'title' => 'backend-gallery',
            'width' => 300,
            'height' => 300,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'backend-upload-image',
            'width' => 300,
            'height' => 300,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'backend-table-image',
            'width' => 300,
            'height' => 300,
            'quality' => 90,
        ];
        $this->image->create($image);

        /**
         * Frontend Image settings
         */
        $image = [
            'title' => 'banners-image',
            'width' => 300,
            'height' => 300,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'articles-image',
            'width' => 480,
            'height' => 350,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'news-image',
            'width' => 480,
            'height' => 350,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'slider-image',
            'width' => 480,
            'height' => 350,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'gallery-image',
            'width' => 480,
            'height' => 350,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'infoblock-group-image',
            'width' => 480,
            'height' => 350,
            'quality' => 90,
        ];
        $this->image->create($image);
        $image = [
            'title' => 'infoblock-image',
            'width' => 480,
            'height' => 350,
            'quality' => 90,
        ];
        $this->image->create($image);

        foreach ($this->fields() as $field) {
            \Modules\FieldGenerator\Entities\Field::create($field);
        }
    }

    public function fields()
    {
        return [
            [
                'table_name' => 'settings_images',
                'table_field' => 'title',
                'required' => true,
                'type' => 'text',
                'label' => ['en' => 'Name', 'ru' => 'Название'],
            ],
            [
                'table_name' => 'settings_images',
                'table_field' => 'width',
                'required' => true,
                'type' => 'number',
                'settings' => ['min' => 1],
                'label' => ['en' => 'Width', 'ru' => 'Ширина'],
            ],
            [
                'table_name' => 'settings_images',
                'table_field' => 'height',
                'required' => true,
                'type' => 'number',
                'settings' => ['min' => 1],
                'label' => ['en' => 'Height', 'ru' => 'Высота'],
            ],
            [
                'table_name' => 'settings_images',
                'table_field' => 'is_crop',
                'type' => 'checkbox',
                'label' => ['en' => 'Crop', 'ru' => 'Образеть'],
            ],
            [
                'table_name' => 'settings_images',
                'table_field' => 'quality',
                'required' => true,
                'type' => 'number',
                'settings' => ['min' => 1, 'max' => 100],
                'label' => ['en' => 'Quality', 'ru' => 'Качество'],
                'hint' => ['en' => '1-100', 'ru' => '1-100'],
            ],
        ];
    }
}
