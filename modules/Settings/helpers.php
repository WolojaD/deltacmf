<?php

if (!function_exists('settings')) {
    function settings($name, $locale = null, $default = null)
    {
        return app('application.settings')->get($name, $locale, $default);
    }
}
