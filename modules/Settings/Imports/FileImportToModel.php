<?php

namespace Modules\Settings\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class FileImportToModel implements ToModel, WithValidation, SkipsOnFailure, WithHeadingRow, WithChunkReading
{
    use Importable;

    protected $rows;
    protected $model;

    public function __construct(array $rows, $model)
    {
        $this->rows = $rows;
        $this->model = $model;
    }

    public function model(array $row)
    {
        foreach ($this->rows as $key => $value) {
            if (!isset($row[str_slug($value['title'], '_')])) {
                return null;
            }

            $row_array[$key] = $row[str_slug($value['title'], '_')];
        }

        $model = new $this->model();

        if (method_exists($model, 'customUpdateOrCreate')) {
            return $model->customUpdateOrCreate($row_array, 'file');
        }

        return $model->updateOrCreate($row_array);
    }

    public function rules(): array
    {
        foreach ($this->rows as $key => $value) {
            $validation_array[str_slug($value['title'], '_')] = $value['validation'] ?? 'nullable';
        }

        return $validation_array;
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    }

    public function chunkSize(): int
    {
        return 200;
    }
}
