<?php

namespace Modules\Settings\Repositories\Cache;

use Modules\Settings\Repositories\ImageRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheImageDecorator extends CoreCacheDecorator implements ImageRepository
{
}
