<?php

namespace Modules\Settings\Repositories\Eloquent;

use Symfony\Component\Yaml\Yaml;
use Illuminate\Support\Facades\Config;
use Modules\Settings\Entities\Settings;
use Modules\Settings\Events\SettingIsCreating;
use Modules\Settings\Events\SettingIsUpdating;
use Modules\Settings\Events\SettingWasCreated;
use Modules\Settings\Events\SettingWasUpdated;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Modules\Settings\Repositories\SettingsRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentSettingsRepository extends EloquentCoreRepository implements SettingsRepository
{
    use ValidateUserByRoleAccess;

    /**
     * Return all settings, with the setting name as key.
     *
     * @return array
     */
    public function all()
    {
        $rawSettings = parent::all();
        $settings = [];

        foreach ($rawSettings as $setting) {
            $settings[$setting->name] = $setting;
        }

        return $settings;
    }

    /**
     * Create or update the settings.
     *
     * @param array $settings
     *
     * @return mixed|void
     */
    public function createOrUpdate($settings)
    {
        $this->removeTokenKey($settings);

        foreach ($settings as $settingName => $settingValues) {
            if ($this->isBase64Image($settingValues)) {
                $settingValues['plainValue'] = $this->uploadBase64Image($settingValues['plainValue'], $settingName);
            }

            if ($setting = $this->findByName($settingName)) {
                $this->updateSetting($setting, $settingValues['plainValue']);

                continue;
            }

            if (is_array($settingValues) && in_array('plainValue', $settingValues)) {
                $this->createForName($settingName, $settingValues['plainValue']);
            } else {
                $this->createForName($settingName, $settingValues);
            }
        }
    }

    /**
     * Remove the token from the input array.
     *
     * @param $settings
     */
    private function removeTokenKey(&$settings)
    {
        unset($settings['_token']);
    }

    /**
     * Find a setting by its name.
     *
     * @param $settingName
     *
     * @return mixed
     */
    public function findByName($settingName)
    {
        // use Modules\Settings\Entities\Settings;
        // dd(\Modules\Settings\Entities\Settings::search('core')->get());
        try {
            return $this->model->where('name', $settingName)->first();
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Create a setting with the given name.
     *
     * @param string $settingName
     * @param $settingValues
     *
     * @return Setting
     */
    private function createForName($settingName, $settingValues)
    {
        event($event = new SettingIsCreating($settingName, $settingValues));

        $setting = new $this->model();

        $setting->name = $settingName;
        $setting->plainValue = $this->getSettingPlainValue($event->getSettingValues());
        $setting->save();

        event(new SettingWasCreated($setting));

        return $setting;
    }

    /**
     * Update the given setting.
     *
     * @param object setting
     * @param $settingValues
     */
    private function updateSetting($setting, $settingValues)
    {
        $name = $setting->name;

        event($event = new SettingIsUpdating($setting, $name, $settingValues));

        if ($this->isStaticSetting($name)) {
            $this->setStaticSettingAttributes($event->getSettingValues(), $name);
        }

        $setting->plainValue = $this->getSettingPlainValue($event->getSettingValues());
        $setting->save();

        event(new SettingWasUpdated($setting));

        return $setting;
    }

    /**
     * Check if the given setting name is static (write in config file).
     *
     * @param string $settingName
     *
     * @return bool
     */
    private function isStaticSetting($settingName)
    {
        $configSettingName = $this->getConfigSettingName($settingName);

        $setting = config("$configSettingName");

        return isset($setting['static']) && true === $setting['static'];
    }

    /**
     * @param $settingValues
     * @param $settingName
     */
    private function setStaticSettingAttributes($settingValue, $settingName)
    {
        list($module, $setting) = explode('::', $settingName);

        $moduleFolders = preg_grep('/^([^.])/', scandir(base_path('modules')));
        $moduleFolder = array_search($module, array_map('strtolower', $moduleFolders));
        $modulePath = base_path('modules' . DIRECTORY_SEPARATOR . $moduleFolders[$moduleFolder]) . '/Config/config.yml';

        $yamlParsedFile = Yaml::parseFile($modulePath);

        if (is_int($settingValue) && $settingValue < 2) {
            $yamlParsedFile[$setting] = boolval($settingValue);
            $yaml = Yaml::dump($yamlParsedFile);

            file_put_contents($modulePath, $yaml);

            return true;
        } elseif (is_int($settingValue) || intval($settingValue)) {
            $yamlParsedFile[$setting] = (int) $settingValue;
            $yaml = Yaml::dump($yamlParsedFile);

            file_put_contents($modulePath, $yaml);

            return true;
        }

        $yamlParsedFile[$setting] = $settingValue;
        $yaml = Yaml::dump($yamlParsedFile);

        file_put_contents($modulePath, $yaml);

        return true;
    }

    /**
     * Return the setting value(s). If values are ann array, json_encode them.
     *
     * @param string|array $settingValues
     *
     * @return string
     */
    private function getSettingPlainValue($settingValues)
    {
        if (is_array($settingValues)) {
            return json_encode($settingValues);
        }

        return $settingValues;
    }

    /**
     * Return a setting name using dot notation: application.{module}.settings.{settingName}.
     *
     * @param string $settingName
     *
     * @return string
     */
    private function getConfigSettingName($settingName)
    {
        list($module, $setting) = explode('::', $settingName);

        return "application.{$module}.settings.{$setting}";
    }

    /**
     * Return all modules that have settings
     * with its settings.
     *
     * @param array|string $allEnabledModules
     *
     * @return array
     */
    public function moduleSettings($allEnabledModules)
    {
        if (is_string($allEnabledModules)) {
            $moduleWithSettings = config('application.' . strtolower($allEnabledModules) . '.settings');

            return $this->validateModuleSettingsByPermission($moduleWithSettings, strtolower($allEnabledModules));
        }

        $modulesWithSettings = [];

        foreach ($allEnabledModules as $module) {
            $moduleName = strtolower($module->getName());
            $moduleSettings = config('application.' . $moduleName . '.settings');

            if ($moduleSettings) {
                $modulesWithSettings[$module->getName()] = $moduleSettings;
            }
        }

        $modulesWithSettings = $this->validateModulesWithSettingsByPermission($modulesWithSettings);

        return array_filter($modulesWithSettings);
    }

    /**
     * Return the saved module settings.
     *
     * @param $module
     *
     * @return mixed
     */
    public function savedModuleSettings($module)
    {
        $moduleSettings = [];

        foreach ($this->findByModule($module) as $setting) {
            $moduleSettings[$setting->name] = $setting;
        }

        foreach ($this->moduleSettings($module) as $field => $parameters) {
            if (!array_key_exists($module . '::' . $field, $moduleSettings)) {
                $moduleSettings[$module . '::' . $field] = [
                    'plainValue' => '',
                ];
            }
        }

        $this->validateModuleSettingsByPermission($moduleSettings);

        return $moduleSettings;
    }

    /**
     * Validate module settings fields by 'enable_for_role' parameter in each field and compare with current user role slug
     *
     * @param array $moduleSettings
     * @param string|null $module
     *
     * @return array
     */
    private function validateModuleSettingsByPermission($moduleSettings, $module = null)
    {
        foreach ($moduleSettings as $field => $parameters) {
            if (null !== $module) {
                $enable_for_role = config($this->getConfigSettingName($module . '::' . $field) . '.' . 'enable_for_role') ?? null;
            } else {
                $enable_for_role = config($this->getConfigSettingName($field) . '.' . 'enable_for_role') ?? null;
            }

            if (null !== $enable_for_role && $enable_for_role != Sentinel::getUser()->roles->first()->slug) {
                unset($moduleSettings[$field]);
            }
        }

        return $moduleSettings;
    }

    /**
     * Validate all given modules->settings->fields by 'enable_for_role' parameter in each field and compare with current user role slug
     *
     * @param array $modulesWithSettings
     */
    private function validateModulesWithSettingsByPermission($modulesWithSettings)
    {
        foreach ($modulesWithSettings as $moduleName => $moduleFields) {
            foreach ($moduleFields as $field => $parameters) {
                if (array_key_exists('enable_for_role', $parameters) && $parameters['enable_for_role'] != Sentinel::getUser()->roles->first()->slug) {
                    unset($modulesWithSettings[$moduleName][$field]);
                }
            }
        }

        return $modulesWithSettings;
    }

    /**
     * Find settings by module name.
     *
     * @param string $module Module name
     *
     * @return mixed
     */
    public function findByModule($module)
    {
        return $this->model->where('name', 'LIKE', $module . '::%')->get();
    }

    /**
     * Find the given setting name for the given module.
     *
     * @param string $settingName
     *
     * @return mixed
     */
    public function get($settingName)
    {
        return $this->model->where('name', 'LIKE', "{$settingName}")->first();
    }

    /**
     * Return the non translatable module settings.
     *
     * @param string $module
     *
     * @return array
     */
    public function plainModuleSettings($module)
    {
        if ($module) {
            return $this->moduleSettings($module);

            // return array_filter($this->moduleSettings($module), function ($setting) {
            //     return !isset($setting['translatable']) || false === $setting['translatable'];
            // });
        }
    }

    private function isBase64Image($value)
    {
        if (is_array($value) && in_array('plainValue', $value) && starts_with($value['plainValue'], 'data:image')) {
            return true;
        }

        return false;
    }

    protected function uploadBase64Image($value, $attribute_name)
    {
        list($module, $setting_name) = explode('::', $attribute_name);

        if (!is_null($value)) {
            $file = new \Illuminate\Filesystem\Filesystem;
            $file->cleanDirectory('storage/origin/settings/' . strtolower($setting_name));
        }

        $image = \Intervention\Image\ImageManagerStatic::make($value)->interlace();

        $filename = md5($value . time());

        switch ($image->mime()) {
            case 'image/x-icon':
            case 'image/png':
                $extension = 'png';
                break;
            default:
                $extension = 'jpg';
                break;
        }

        $new_file_name = $filename . '.' . $extension;

        $path = 'origin/settings/' . strtolower($setting_name);

        \Storage::disk('public')->put($path . '/' . $new_file_name, $image->stream($extension, 90));

        return '/settings/' . strtolower($setting_name) . '/' . $new_file_name;
    }
}
