<?php

namespace Modules\Settings\Repositories\Eloquent;

use Illuminate\Http\Request;
use Modules\Settings\Repositories\ImageRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentImageRepository extends EloquentCoreRepository implements ImageRepository
{
    public function show(Request $request)
    {
        $segments = explode('/', $request->path);
        $type = array_shift($segments);
        $path = implode('/', $segments);

        $can_be_dynamic = env('APP_DEBUG') && preg_match("/^[\d*][x\d*c]*$/", $type);

        return $can_be_dynamic ? $this->generateSizeFromPath($type, $path) : $this->generateSizeFromBase($type, $path);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function findById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function generateSizeFromPath($type, $path)
    {
        preg_match("/(\d*)x(\d*)(c)?/", $type, $sizes);

        $image = new $this->model;
        $image->title = $type;
        $image->width = $sizes[1];
        $image->height = $sizes[2];
        $image->is_crop = isset($sizes[3]);
        $image->quality = 100;

        return $image->getMinified($path);
    }

    public function generateSizeFromBase($type, $path)
    {
        return $this->model->findOrFail($type)->getMinified($path);
    }
}
