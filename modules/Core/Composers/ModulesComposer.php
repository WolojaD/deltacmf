<?php

namespace Modules\Core\Composers;

use Illuminate\Contracts\View\View;
use Nwidart\Modules\Facades\Module;

class ModulesComposer
{
    public function compose(View $view)
    {
        $view->with('activeModulesPaths', Module::activePaths());
    }
}
