<?php

namespace Modules\Core\Composers;

use Illuminate\Contracts\View\View;
use Modules\Core\Events\LoadingBackendTranslations;

class TranslationsComposer
{
    public function compose(View $view)
    {
        event($staticTranslations = new LoadingBackendTranslations());

        $view->with('backendTranslations', json_encode($staticTranslations->getTranslations()));
    }
}
