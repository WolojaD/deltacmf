<?php

namespace Modules\Core\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Modules\Settings\Contracts\SettingsContract;

class SiteNameComposer
{
    /**
     * @var SettingsContract
     */
    private $settings;

    public function __construct(SettingsContract $settings)
    {
        $this->settings = $settings;
    }

    public function compose(View $view)
    {
        $view->with('sitename', $this->settings->get('dashboard::site_name', App::getLocale()));
    }
}
