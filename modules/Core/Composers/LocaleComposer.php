<?php

namespace Modules\Core\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;

class LocaleComposer
{
    public function compose(View $view)
    {
        // Current backend locale
        $view->with('currentBackendLocale', App::getLocale());

        // Current default frontend locale
        if (null !== cache()->tags(['settings', 'global'])->get('application.locales')) {
            $view->with('currentFrontendLocale', json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue)[0]);
        } else {
            $view->with('currentFrontendLocale', key(config('laravellocalization.defaultApplicationLocales')));
        }

        // Current all frontend locales
        $availableFrontendLocales = [];

        if (null !== cache()->tags(['settings', 'global'])->get('application.locales')) {
            foreach (json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue) as $locale) {
                $availableFrontendLocales = array_merge($availableFrontendLocales, [$locale => config("application.core.available-locales.$locale")]);
            }
        } else {
            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $locale) {
                $availableFrontendLocales = array_merge($availableFrontendLocales, [$key => config("application.core.available-locales.$key")]);
            }
        }

        $view->with('allFrontendLocales', $availableFrontendLocales);

        // Current all backend locales
        $view->with('allBackendLocales', config('application.core.available-locales-backend'));
    }
}
