<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Config\Repository;
use MikeFrancis\LaravelSecureHeaders\Adapter;
use MikeFrancis\LaravelSecureHeaders\ApplySecureHeaders;
use Aidantwoods\SecureHeaders\SecureHeaders as AidantwoodsSecureHeaders;

class SecureHeaders extends ApplySecureHeaders
{
    /**
     * Instance of Config Repository.
     *
     * @var Repository
     */
    private $config;

    /**
     * Instance of AidantwoodsSecureHeaders Utility.
     *
     * @var AidantwoodsSecureHeaders
     */
    private $headers;

    /**
     * ApplySecureHeaders constructor.
     *
     * @param Repository $config
     * @param AidantwoodsSecureHeaders $headers
     */
    public function __construct(Repository $config, AidantwoodsSecureHeaders $headers)
    {
        $this->config = $config;
        $this->headers = $headers;
    }

    /**
     * Applies AidantwoodsSecureHeaders to the request response.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $this->headers->errorReporting($this->config->get('secure-headers.errorReporting', true));

        $this->setHsts();
        $this->setCsp();
        $this->setMode();
        $this->setExpectCT();

        $adapter = new Adapter($response);

        $this->headers->apply($adapter);

        return $response;
    }

    /**
     * Set any Content Security Policy headers.
     *
     * @return void
     */
    private function setCsp()
    {
        $csp = $this->config->get('secure-headers.csp', []);
        $this->headers->csp($csp);

        $cspro = $this->config->get('secure-headers.cspro', []);
        $this->headers->cspro($cspro);
    }

    /**
     * Set any Strict-Transport-Policy headers.
     *
     * @return void
     */
    private function setHsts()
    {
        if ($hsts = $this->config->get('secure-headers.hsts', false)) {
            if (isset($hsts['maxAge'])) {
                $this->headers->hsts($hsts['maxAge']);
            } elseif (isset($hsts['enabled'])) {
                $this->headers->hsts();
            } else {
                return;
            }

            if (isset($hsts['includeSubDomains'])) {
                $this->headers->hstsSubdomains($hsts['includeSubDomains']);
            }

            if (isset($hsts['preload'])) {
                $this->headers->hstsPreload($hsts['preload']);
            }
        }
    }

    /**
     * Set safe or (inclusive) strict mode, if it is required.
     *
     * @return void
     */
    private function setMode()
    {
        if ($this->config->get('secure-headers.safeMode', false)) {
            $this->headers->safeMode();
        }

        if ($this->config->get('secure-headers.strictMode', false)) {
            $this->headers->strictMode();
        }
    }

    /**
     * Set any Expect-CT headers.
     *
     * @return void
     */
    private function setExpectCT()
    {
        if ($expectCT = $this->config->get('secure-headers.expectCT', false)) {
            $this->headers->expectCT(
                array_get($expectCT, 'maxAge'),
                array_get($expectCT, 'enforce'),
                array_get($expectCT, 'reportUri')
            );
        }
    }
}
