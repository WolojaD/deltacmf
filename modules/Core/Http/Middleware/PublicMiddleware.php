<?php

namespace Modules\Core\Http\Middleware;

use Illuminate\Http\Request;
use Modules\Seo\Entities\Redirect;
use Modules\Seo\Repositories\Eloquent\EloquentRedirectRepository;

class PublicMiddleware
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var EloquentRedirectRepository
     */
    private $redirect_repository;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->redirect_repository = new EloquentRedirectRepository(new Redirect);
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $request_path = $request->getPathInfo();

        if ($request_path == '/fantom_storepage_u_cant_see') {
            return redirect('/', 301);
        }

        $tmp_path = $request_path;

        if (strpos($tmp_path, '//') !== false) {
            $tmp_path = preg_replace('/(\/{2,})/', '/', $tmp_path);
        }

        if (strpos($tmp_path, '.html') !== false) {
            $tmp_path = preg_replace('/(.html)$/', '', $tmp_path);
        }

        if ($tmp_path !== $request_path) {
            return redirect($tmp_path, 301);
        }

        $params = explode('/', $request_path);

        array_shift($params);

        if (count($params) > 0) {
            $redirect = $this->redirect_repository->findByUrlForPublicMiddleware(app('laravellocalization')->getNonLocalizedURL());

            if ($redirect && $redirect !== null) {
                return redirect($redirect->new_url, 301);
            }
        }

        return $next($request);
    }
}
