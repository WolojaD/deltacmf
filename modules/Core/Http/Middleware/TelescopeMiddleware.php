<?php

namespace Modules\Core\Http\Middleware;

use Modules\Users\Traits\ValidateUserByRoleAccess;

class TelescopeMiddleware
{
    use ValidateUserByRoleAccess;

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next)
    {
        return $this->userDeveloper() ? $next($request) : abort(404);
    }
}
