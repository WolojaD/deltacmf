<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Modules\Users\Contracts\Authentication;

abstract class CorePublicController extends Controller
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @var Locale
     */
    public $locale;

    public function __construct()
    {
        $this->locale = App::getLocale();
        $this->auth = app(Authentication::class);
    }
}
