<?php

namespace Modules\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Logs\Loggers\ActionsLogger;

class CoreApiController extends Controller
{
    /**
     * @param Request $request
     *
     * @return type
     */
    public function changeStatus(Request $request)
    {
        return $this->repository->markSelectedAsStatus($request->get('items', []), $request->position);
    }

    /**
     * @param Request $request
     *
     * @return type
     */
    public function toggleField(Request $request)
    {
        return $this->repository->markSelectedAs($request->get('items', []), $request->position, $request->field);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function findNew(Request $request)
    {
        return [
            'fields' => $this->repository->getFieldsArray(),
            'breadcrumbs' => $this->repository->getFormBreadcrumbs($request->get('ident', 0), 'create'),
        ];
    }

    /**
     * Show the form for editin a resource.
     *
     * @return
     */
    public function findEdit(Request $request)
    {
        return [
            'fields' => $this->repository->getFieldsArray(),
            'breadcrumbs' => $this->repository->getFormBreadcrumbs($request->get('ident', 0), 'edit'),
        ];
    }

    /**
     * @param Request $request
     *
     * @return type
     */
    public function changeOrder(Request $request)
    {
        return $this->repository->changeOrder($request);
    }

    /**
     * @param Request $request
     *
     * @return type
     */
    public function textEditorUpload(Request $request)
    {
        if ($request->ajax()) {
            if ($request->hasFile('file') && $request->file('file')->isValid()) {
                return $this->repository->textEditorFileUpload($request->file('file'));
            } elseif ($request->hasFile('image') && $request->file('image')->isValid()) {
                return $this->repository->textEditorFileUpload($request->file('image'));
            } else {
                return response()->json([
                    'errors' => true,
                    'message' => 'Error! No \'file\' or \'image\' type',
                ]);
            }
        } else {
            return response()->json([
                'errors' => true,
                'message' => 'Error! No AJAX request',
            ]);
        }
    }

    /**
     * @param  Request $request
     * @param  int $id
     *
     * @return [type]
     */
    public function findTable(Request $request, $id)
    {
        list($main_module, $main_model, $transformer_class, $repository) = $this->getModuleAndModelFromPathName($request);

        if (strpos($request->route()->uri, '/inner-' . $main_module) || strpos($request->route()->uri, '/show-inner-' . $main_module)) {
            $request->merge(['where' => [$main_model . '_id' => $id]]);
        }

        return $transformer_class::collection($repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param  Request $request
     *
     * @return Excel
     */
    public function exportExcel(Request $request)
    {
        preg_match('/api.backend.([a-z]+)_([a-z]+).exportExcel$/', $request->route()->getName(), $model_match);
        $main_model = $model_match[2];
        $file_name = str_plural($main_model) . ' ' . \Carbon\Carbon::now()->format('d-m-Y H:i:s') . '.xlsx';

        $collection = $this->repository->collectionForExport($request);

        (new ActionsLogger($main_model, 'excel exported'))->write();

        return Excel::download($collection, $file_name);
    }

    /**
     * @param  Request $request
     *
     * @return Response
     */
    public function importExcel(Request $request)
    {
        if ($request->hasFile('import_excel') && $request->file('import_excel')->isValid()) {
            preg_match('/api.backend.([a-z]+)_([a-z]+).importExcel$/', $request->route()->getName(), $model_match);
            $main_model = $model_match[2];

            Excel::import($this->repository->collectionForImport($request), $request->file('import_excel'));

            (new ActionsLogger($main_model, 'excel imported'))->write();

            return response()->json([
                'errors' => false,
                'message' => trans('seo::messages.api.import excel success'),
            ]);
        }

        return response()->json([
            'errors' => true,
            'message' => trans('seo::messages.api.import excel error'),
        ]);
    }

    /**
     * @param  Request $request
     * @param  boolean $pattern
     * @param  boolean $pathName
     *
     * @return array
     */
    protected function getModuleAndModelFromPathName(Request $request, $pattern = false, $pathName = false): array
    {
        preg_match('/api.backend.([a-z]+)_([a-z]+).+find-([a-z]+)_([a-z_]+)$/', $request->route()->getName(), $model_match);

        $main_module = strtolower(str_singular($model_match[1]));
        $main_model = strtolower(str_singular($model_match[2]));

        $sub_module = ucfirst($model_match[3]);
        $sub_model = ucfirst(str_singular(studly_case($model_match[4])));

        $repo_class = "\\Modules\\{$sub_module}\\Repositories\\Eloquent\\Eloquent{$sub_model}Repository";
        $transformer_class = "\\Modules\\{$sub_module}\\Transformers\\{$sub_model}Transformer";
        $model_class = "\\Modules\\{$sub_module}\\Entities\\{$sub_model}";

        $repository = new $repo_class(new $model_class());

        return [$main_module, $main_model, $transformer_class, $repository];
    }

    public function destroyMultiple()
    {
        foreach (request()->all() as $id) {
            $post = $this->repository->find((int) $id);

            $this->repository->destroy($post);
        }

        return response()->json([
            'errors' => false,
            'message' => 'success',
        ]);
    }
}
