<?php

namespace Modules\Core\Foundation;

use Illuminate\Foundation\Application;

class CoreApplication extends Application
{
    /**
     * Get the path to the language files.
     *
     * @return string
     */
    public function langPath()
    {
        return $this->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'Translation' . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'core';
    }
}
