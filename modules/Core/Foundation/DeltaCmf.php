<?php

namespace Modules\Core\Foundation;

class DeltaCmf
{
    /**
     * The application version.
     *
     * @var string
     */
    const VERSION = '3.0.0';

    /**
     * The application start development.
     *
     * @var string
     */
    const START = '19.03.2018 21:44';
}
