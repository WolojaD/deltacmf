<?php

namespace Modules\Core\Repositories;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

interface CoreInterface
{
    /**
     * Return a collection of all elements of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * {@inheritdoc}
     */
    public function allWith($relations);

    /**
     * @param int $id
     *
     * @return $model
     */
    public function find($id);

    /**
     * Find a resource by the given slug.
     *
     * @param string $slug
     *
     * @return $model
     */
    public function findBySlug($slug);

    /**
     * Find a resource by an array of attributes.
     *
     * @param array $attributes
     *
     * @return $model
     */
    public function findByAttributes(array $attributes);

    /**
     * Create a resource.
     *
     * @param  $data
     *
     * @return $model
     */
    public function create($data);

    /**
     * Update a resource.
     *
     * @param  $model
     * @param array $data
     *
     * @return $model
     */
    public function update($model, $data);

    /**
     * Destroy a resource.
     *
     * @param  $model
     *
     * @return bool
     */
    public function destroy($model);

    /**
     * @return Builder
     */
    public function allWithBuilder(): Builder;

    /**
     * Paginate the model to $perPage items per page.
     *
     * @param int $perPage
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 10);

    /**
     * Return resources translated in the given language.
     *
     * @param string $lang
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allTranslatedIn($lang);

    /**
     * Get resources by an array of attributes.
     *
     * @param array       $attributes
     * @param null|string $orderBy
     * @param string      $sortOrder
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByAttributes(array $attributes, $orderBy = null, $sortOrder = 'asc');

    /**
     * Return a collection of elements who's ids match.
     *
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByMany(array $ids);

    /**
     * Clear the cache for this Repositories' Entity.
     *
     * @return bool
     */
    public function clearCache();

    /**
     * {@inheritdoc}
     */
    public function getJsonList();

    /**
     * {@inheritdoc}
     */
    public function getFieldsArray();

    /**
     * Get collection of model.
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function serverPaginationFilteringFor(Request $request, $template = 'default');

    /**
     * Generate initial query.
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function generateQuery($request, $template = 'default');

    /**
     * Generating array of field and order from sort string.
     *
     * @param string $sort
     * @param string $template
     *
     * @return array
     */
    public function parseSortElements($sort, $template = 'default');

    /**
    * Paginate a laravel colletion or array of items.
    *
    * @param array|Illuminate\Support\Collection $items array to paginate
    * @param int $perPage number of pages
    * @param null $current_entity
    * @param string $template
    *
    * @return BackendPaginator new LengthAwarePaginator instance
    */
    public function paginator($items, $perPage, $current_entity = null, $template = 'default');

    /**
     * @param array $selected_ids
     * @param int $value
     * @param string $field
     *
     * @return \Illuminate\Support\Collection
     */
    public function markSelectedAs(array $selected_ids, $value, $field);

    /**
     * @param int $id
     * @param string $type
     *
     * @return [type]
     */
    public function getFormBreadcrumbs($id, $type);
}
