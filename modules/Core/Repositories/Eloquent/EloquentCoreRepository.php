<?php

namespace Modules\Core\Repositories\Eloquent;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\DraggableTrait;
use Modules\Core\Traits\StatusableTrait;
use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Helpers\BackendPaginator;
use Modules\FieldGenerator\Entities\Field;
use Modules\Core\Repositories\CoreInterface;
use Modules\Settings\Exports\CollectionExport;
use Modules\Settings\Imports\FileImportToModel;
use Modules\Buldozer\Managers\ModelConfigurationManagerInterface;

abstract class EloquentCoreRepository implements CoreInterface
{
    use StatusableTrait, DraggableTrait;

    /**
     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->orderBy('created_at', 'DESC')->get();
        }

        return $this->model->orderBy('created_at', 'DESC')->get();
    }

    /**
     * {@inheritdoc}
     */
    public function allWith($relations)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations', is_string($relations) ? func_get_args() : $relations)->orderBy('created_at', 'DESC')->get();
        }

        return $this->model->with(is_string($relations) ? func_get_args() : $relations)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->find($id);
        }

        return $this->model->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findBySlug($slug)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $query) use ($slug) {
                $query->where('slug', $slug);
            })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findByAttributes(array $attributes)
    {
        $query = $this->buildQueryByAttributes($attributes);

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function create($data)
    {
        if (!method_exists($this->model, 'getJsonList') || !request()->get('ident', false)) {
            return $this->model->create($data);
        }

        $config = $this->model->getJsonList();

        foreach ($config->templates->default ?? [] as $field_name => $field) {
            if (!request()->filled($field_name) && ($field->from_ident ?? false)) {
                $data[$field_name] = (int) request()->ident;
            }
        }

        return $this->model->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update($model, $data)
    {
        $model->update($data);

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($model)
    {
        return $model->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function allWithBuilder(): Builder
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations');
        }

        return $this->model->query();
    }

    /**
     * {@inheritdoc}
     */
    public function paginate($perPage = 10)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->paginate($perPage);
        }

        return $this->model->paginate($perPage);
    }

    /**
     * {@inheritdoc}
     */
    public function allTranslatedIn($lang)
    {
        return $this->model->whereHas('translations', function (Builder $query) use ($lang) {
            $query->where('locale', "$lang");
        })->with('translations')->orderBy('created_at', 'DESC')->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getByAttributes(array $attributes, $orderBy = null, $sortOrder = 'asc')
    {
        $query = $this->buildQueryByAttributes($attributes, $orderBy, $sortOrder);

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function findByMany(array $ids)
    {
        $query = $this->model->query();

        if (method_exists($this->model, 'translations')) {
            $query = $query->with('translations');
        }

        return $query->whereIn('id', $ids)->get();
    }

    /**
     * {@inheritdoc}
     */
    public function clearCache()
    {
        return true;
    }

    /**
     * Build Query to catch resources by an array of attributes and params.
     *
     * @param array       $attributes
     * @param null|string $orderBy
     * @param string      $sortOrder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function buildQueryByAttributes(array $attributes, $orderBy = null, $sortOrder = 'asc')
    {
        $query = $this->model->query();

        if (method_exists($this->model, 'translations')) {
            $query = $query->with('translations');
        }

        foreach ($attributes as $field => $value) {
            $query = $query->where($field, $value);
        }

        if (null !== $orderBy) {
            $query->orderBy($orderBy, $sortOrder);
        }

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function getJsonList()
    {
        if (!method_exists($this->model, 'getJsonList')) {
            return false;
        }

        return $this->model->getJsonList();
    }

    /**
     * Generate fields components with parameters for create/update page.
     *
     * @return type
     */
    public function getFieldsArray()
    {
        return app()->make(ModelConfigurationManagerInterface::class, [$this->model_config ?? ''])->getFields($this);
    }

    public function getTable()
    {
        $this->model->getTable();
    }

    /**
     * Validate and get needed data for array_values parameter of field.
     *
     * @param type $field
     *
     * @return type
     */
    public function getArrayValues($field)
    {
        if (is_string($field)) {
            if ($field == 'status') {
                return array_values(Lang::get('core::fields.status'));
            }

            $config = config('application.fieldgenerator.fields-config.' . $field);

            if ($config) {
                return $config;
            } elseif ('menuPositions' == $field) {
                return with(new \Modules\Page\Entities\MenuPosition())->lists();
            }

            return $field ? ($this->{$field}() ?? '') : '';
        }

        if ($field->table_field == 'status') {
            return array_values(Lang::get('core::fields.status'));
        }

        $config = config('application.fieldgenerator.fields-config.' . $field->table_field);

        if ($config) {
            return $config;
        } elseif ('menuPositions' == $field->table_field) {
            return with(new \Modules\Page\Entities\MenuPosition())->lists();
        }

        return ($field->relation ?? false) ? ($this->{$field->relation}() ?? '') : '';
    }

    /**
     * Get collection of model.
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function serverPaginationFilteringFor(Request $request, $template = 'default')
    {
        $parent_id = $request->pageId ?? $request->ident;

        return $this->generateQuery($request, $template)->backendPaginate($request->get('per_page', 10), $template, $parent_id);
    }

    /**
     * Generate initial query.
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function generateQuery($request, $template = 'default')
    {
        $sort = $this->parseSortElements($request->get('sort', ''), $template);

        return $this->allWithBuilder()
            ->when($request->get('with', false), function ($q) use ($request) {
                return $q->with($request->with);
            })
            ->when($request->get('filter', false), function ($q) use ($request) {
                return $q->search($request->filter);
            })
            ->when($request->get('scopes', false), function ($q) use ($request) {
                foreach ($request->scopes as $scope) {
                    $q = $q->$scope();
                }

                return $q;
            })
            ->when($request->get('where', false), function ($q) use ($request) {
                return $q->where($request->where);
            })
            ->when($request->get('whereIn', false), function ($q) use ($request) {
                foreach ($request->whereIn as $key => $item) {
                    $q = $q->whereIn($key, $item);
                }

                return $q;
            })
            ->when($request->get('whereHas', false), function ($q) use ($request) {
                return $q->whereHas($request->whereHas['relation'], $request->whereHas['function']);
            })
            ->when($request->get('take', false), function ($q) use ($request) {
                return $q->take($request->take);
            })
            ->when($request->get('skip', false), function ($q) use ($request) {
                return $q->skip($request->skip);
            })
            ->when($request->get('withCount', false), function ($q) use ($request) {
                return $q->withCount($request->withCount);
            })
            ->when(isset($sort['table']) && 'translation' != $sort['table'], function ($q) use ($sort) {
                return $q->with($sort['table']);
            })
            ->when($request->get('select', false), function ($q) use ($request) {
                return $q->select($request->select);
            })
            ->when($request->get('withoutGlobalScope', false), function ($q) use ($request) {
                return $q->withoutGlobalScope($request->withoutGlobalScope);
            })
            ->when($request->has('pageId'), function ($query) use ($request) {
                $query->withCount('children')
                    ->where('parent_id', $request->pageId);
            })
            ->when($request->get('addSubCount', false), function ($q) use ($request) {
                return $q->addSubCount($request->addSubCount['query'], $request->addSubCount['count_name']);
            })
            ->sort($sort);
    }

    /**
     * Generating array of field and order from sort string.
     *
     * @param string $sort
     * @param string $template
     *
     * @return array
     */
    public function parseSortElements($sort, $template = 'default')
    {
        if ($sort) {
            $sort = explode('|', $sort);
        }

        if ($this->model instanceof \Modules\Core\Eloquent\Model) {
            $modelInfo = $this->model->getJsonList();
        }

        $result['field'] = $sort[0] ?? ($modelInfo->templates->$template->standard->sortOrder->field ?? 'created_at');

        if (strpos($result['field'], '.')) {
            $arr = explode('.', $result['field']);
            $result['table'] = $arr[0];
        }

        $result['order'] = $sort[1] ?? ($modelInfo->templates->$template->standard->sortOrder->direction ?? 'asc');

        $this->sort = [
            'field' => $result['field'],
            'sortField' => $result['field'],
            'direction' => $result['order']
        ];

        return $result;
    }

    /**
     * Paginate a laravel colletion or array of items.
     *
     * @param array|Illuminate\Support\Collection $items array to paginate
     * @param int $perPage number of pages
     * @param null $current_entity
     * @param string $template
     *
     * @return BackendPaginator new LengthAwarePaginator instance
     */
    public function paginator($items, $perPage, $current_entity = null, $template = 'default')
    {
        if (is_array($items)) {
            $items = collect($items);
        }

        $options['path'] = Paginator::resolveCurrentPath();

        $options = $this->paginateOptionsGenerate($current_entity, $options, $template);

        return new BackendPaginator(
            $items->forPage(Paginator::resolveCurrentPage(), $perPage),
            $items->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            $options
        );
    }

    /**
     * Setting status for all selected ids.
     *
     * @param array $selected_ids
     * @param int $value
     * @param string $field
     *
     * @return \Illuminate\Support\Collection
     */
    public function markSelectedAs(array $selected_ids, $value, $field)
    {
        if (!count($selected_ids)) {
            return collect([]);
        }

        $entities = $this->model->whereIn('id', $selected_ids)->get();
        $entities->each(function ($item) use ($field, $value) {
            $item->update([$field => $value ?? 1]);
        });

        return $entities->pluck($field, 'id');
    }

    /**
     * Flatten array by relation.
     *
     * @return array
     */
    protected function tableForFlattenByRelation()
    {
        return $this->model->whereIsRoot()->with('children', 'translations')->translated()->get()->flatten()->toArray();
    }

    /**
     * @param $parent
     * @param $options
     * @param string $template
     *
     * @return array
     */
    protected function paginateOptionsGenerate($parent, $options, $template = 'default')
    {
        $json = $this->getJsonList();
        $templateModel = $json->templates->$template ?? false;

        if ($templateModel) {
            $options['fields'] = $this->model->getColumns($template);
            $options['sortOrder'] = ['current' => $this->sort ?? false, 'default' => ($templateModel->standard->sortOrder ?? false)];
            $options['backend_path_name'] = $json->backend_path_name ?? false;
            $options['has_tables'] = $templateModel->standard->has_tables ?? [];
            $options['nested'] = $templateModel->standard->nested ?? false;
            $options['buttons'] = $templateModel->standard->buttons ?? false;
            $options['no_create'] = $templateModel->standard->no_create ?? false;
            $title_field = $templateModel->standard->nested ?? false;
        }

        if ($title_field ?? false) {
            $title_field = $templateModel->standard->title_field ?? false;
        }

        if ((request()->ident ?? false) && is_numeric(request()->ident) && request()->filled('where')) {
            $model = str_replace('_id', '', key(request()->where));

            if (method_exists($this->model, $model)) {
                $model = $this->model->$model()->getRelated()->find(request()->ident);

                if (method_exists($model, 'getBackendBreadcrumbs')) {
                    $options['breadcrumbs'] = $model->getBackendBreadcrumbs();
                }
            } else {
                $options['breadcrumbs'] = $this->model->getBackendBreadcrumbs();
            }
        } elseif (method_exists($this->model, 'getBackendBreadcrumbs')) {
            $options['breadcrumbs'] = $this->model->getBackendBreadcrumbs();
        }

        if ($parent->id ?? false) {
            $options['title'] = isset($title_field) && $title_field ? $parent->{$title_field} : ($parent->title ?? '');
            $options['grand_parent_id'] = $parent->parent_id ?? 0;
            $options['breadcrumbs'] = $parent->getBackendBreadcrumbs();
        }

        if (request()->filled('grand_parent_id')) {
            $options['grand_parent_id'] = request()->grand_parent_id;
        }

        if (request()->filled('back_path_name')) {
            $options['back_path_name'] = request()->back_path_name;
        }

        return $options;
    }

    /**
     * @param array $variable
     *
     * @return [type]
     */
    protected function validForPivotMaking($variable = [])
    {
        if (!is_array($variable) && is_numeric((int) $variable) && (int) $variable) {
            return [(int) $variable];
        } elseif (!is_array($variable)) {
            return false;
        }

        $variable = array_values($variable);

        foreach ($variable as $item) {
            if (!is_int($item)) {
                return false;
            }
        }

        return $variable;
    }

    /**
     * @param Request $request
     * @param string $template
     *
     * @return [type]
     */
    public function collectionForExport($request, $template = 'default')
    {
        $headers = $this->model->getHeadersOfCollectionFor($template);

        $collection = $this->getCollectionForExport($request, $template)->map(function ($item) use ($headers) {
            foreach ($headers as $field => $header) {
                $result[$field] = $item->$field;
            }

            return $result ?? [];
        });

        return new CollectionExport($collection, $headers);
    }

    /**
     * Generate table data collection (used now only for export to excel)
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCollectionForExport($request, $template = 'default')
    {
        $take = $request->get('per_page', 10);
        $skip = $take * ($request->get('page', 1) - 1);

        $columns = $this->model
            ->getColumns($template)
            ->filter(
                function ($item) {
                    return $item['column_type'];
                }
            );

        // $select = $this->selectListForExportExcel($columns);

        $with = $this->withListForExportExcel($columns);

        $request->merge(
            [
                'with' => $with,
                // 'select' => $select,
                'skip' => $skip,
                'take' => $take,
            ]
        );

        return $this->generateQuery($request, $template)->get();
    }

    /**
     * @param $columns
     *
     * @return array
     */
    public function selectListForExportExcel($columns)
    {
        return $columns->map(
            function ($column) {
                return $column['relationMethod'] ? '' : $column['originName'];
            }
            )
            ->filter()
            ->toArray();
    }

    /**
     * @param $columns
     *
     * @return array
     */
    public function withListForExportExcel($columns)
    {
        return $columns->mapWithKeys(
            function ($item) {
                return [$item['relationMethod']];
            }
            )
            ->filter()
            ->toArray();
    }

    /**
    * @param Request $request
    * @param string $template
    *
    * @return [type]
    */
    public function collectionForImport($request, $template = 'default')
    {
        $headers = $this->model->getHeadersOfCollectionFor('import_excel');

        return new FileImportToModel($headers, $this->model);
    }

    /**
     * @param int $id
     * @param string $type
     *
     * @return [type]
     */
    public function getFormBreadcrumbs($id, $type)
    {
        if (!method_exists($this->model, 'getBackendBreadcrumbs')) {
            return false;
        }

        $model_name = strtolower(substr(strrchr(get_class($this->model), '\\'), 1));
        $final_name = trans('core::breadcrumbs.' . $model_name . ' ' . $type);

        if ($type == 'edit' && $id > 0) {
            $breadcrumbs = $this->model->find($id)->getBackendBreadcrumbs();

            $breadcrumbs[count($breadcrumbs) - 1]['name'] = $final_name;

            return $breadcrumbs;
        }

        if ($type == 'create') {
            $carry = [
                [
                    'name' => $final_name
                ]
            ];

            $relation = str_replace('_id', '', $this->model->groupOrder);

            if ($relation) {
                $parent = $this->model->$relation()->where('id', $id)->first();

                return $parent ? $parent->getBackendBreadcrumbs($carry) : $this->model->getBackendBreadcrumbs($carry);
            } else {
                return $this->model->getBackendBreadcrumbs($carry);
            }
        }
    }

    /**
     * @param $structure
     *
     * @return array
     */
    public function getFrontBreadcrumbs($structure): array
    {
        return $structure->ancestors()
            ->orderBy('_rgt')
            ->listsTranslations('title')
            ->select('url', 'title')
            ->get()
            ->map(function ($item) {
                return [
                    'title' => $item->title,
                    'url' => $item->url
                ];
            })
            ->toArray();
    }

    /**
     * @param type $value
     *
     * @return type
     */
    public function textEditorFileUpload($value)
    {
        return $this->model->uploadTextEditorFile($value);
    }

    public function userPermission($type)
    {
        if (method_exists($this->model, 'getJsonList')) {
            $config = $this->model->getJsonList();

            return auth()->user()->hasAccess(strtolower(str_replace('_', '.', $config->backend_path_name)) . '.' . $type);
        }

        return false;
    }
}
