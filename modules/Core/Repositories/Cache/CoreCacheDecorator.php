<?php

namespace Modules\Core\Repositories\Cache;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Cache\Repository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\CoreInterface;
use Modules\Core\Traits\Cache\CacheDraggableTrait;
use Modules\Core\Traits\Cache\CacheStatusableTrait;
use Illuminate\Config\Repository as ConfigRepository;

abstract class CoreCacheDecorator implements CoreInterface
{
    use CacheStatusableTrait, CacheDraggableTrait;

    /**
     * @var \Modules\Core\Repositories\CoreInterface
     */
    protected $repository;

    /**
     * @var Repository
     */
    protected $cache;

    /**
     * @var string The entity name
     */
    protected $entityName;

    /**
     * @var string The application locale
     */
    protected $locale;

    /**
     * @var int caching time
     */
    protected $cacheTime;

    public function __construct()
    {
        $this->cache = app(Repository::class);
        $this->cacheTime = app(ConfigRepository::class)->get('cache.time', 300);
        $this->locale = app()->getLocale();
    }

    public function __call($method, $parameters)
    {
        return $this->repository->$method(...$parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $this->remember(function () {
            return $this->repository->all();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function allWith($relations)
    {
        return $this->remember(function () use ($relations) {
            return $this->repository->allWith($relations);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->remember(function () use ($id) {
            return $this->repository->find($id);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findBySlug($slug)
    {
        return $this->remember(function () use ($slug) {
            return $this->repository->findBySlug($slug);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findByAttributes(array $attributes)
    {
        return $this->remember(function () use ($attributes) {
            return $this->repository->findByAttributes($attributes);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function create($data)
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function createOrUpdate($data)
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->createOrUpdate($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update($model, $data)
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->update($model, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($model)
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->destroy($model);
    }

    /**
     * {@inheritdoc}
     */
    public function allWithBuilder(): Builder
    {
        return $this->remember(function () {
            return $this->repository->allWithBuilder();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function paginate($perPage = 10)
    {
        return $this->remember(function () use ($perPage) {
            return $this->repository->paginate($perPage);
        });

        // return $this->repository->paginate($perPage);
    }

    /**
     * {@inheritdoc}
     */
    public function allTranslatedIn($lang)
    {
        return $this->remember(function () use ($lang) {
            return $this->repository->allTranslatedIn($lang);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getByAttributes(array $attributes, $orderBy = null, $sortOrder = 'asc')
    {
        return $this->remember(function () use ($attributes, $orderBy, $sortOrder) {
            return $this->repository->getByAttributes($attributes, $orderBy, $sortOrder);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findByMany(array $ids)
    {
        return $this->remember(function () use ($ids) {
            return $this->repository->findByMany($ids);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function clearCache()
    {
        $store = $this->cache;

        if (method_exists($this->cache->getStore(), 'tags')) {
            $store = $store->tags($this->entityName);
        }

        return $store->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getJsonList()
    {
        return $this->remember(function () {
            return $this->repository->getJsonList();
        });
    }

    /**
     * Generate fields components with parameters for create/update page.
     *
     * @return type
     */
    public function getFieldsArray()
    {
        return $this->remember(function () {
            return $this->repository->getFieldsArray();
        });
    }

    /**
     * Get collection of model.
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function serverPaginationFilteringFor(Request $request, $template = 'default')
    {
        // TODO Need to fix for cache
        // $page = $request->get('page');
        // $perPage = $request->get('per_page');
        // $sort = $request->get('sort');

        // $key = $this->getBaseKey() . " serverPaginationFilteringFor.{$page}-{$perPage}-{$sort}";

        // return $this->remember(function () use ($request, $template) {
        //     return $this->repository->serverPaginationFilteringFor($request, $template);
        // }, $key);

        return $this->repository->serverPaginationFilteringFor($request, $template);
    }

    /**
     * Generate initial query.
     *
     * @param Request $request
     * @param string $template
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function generateQuery($request, $template = 'default')
    {
        return $this->remember(function () use ($request, $template) {
            return $this->repository->generateQuery($request, $template);
        });
    }

    /**
     * Generating array of field and order from sort string.
     *
     * @param string $sort
     * @param string $template
     *
     * @return array
     */
    public function parseSortElements($sort, $template = 'default')
    {
        return $this->remember(function () use ($sort, $template) {
            return $this->repository->parseSortElements($sort, $template);
        });
    }

    /**
     * Paginate a laravel colletion or array of items.
     *
     * @param array|Illuminate\Support\Collection $items array to paginate
     * @param int $perPage number of pages
     * @param null $current_entity
     * @param string $template
     *
     * @return BackendPaginator new LengthAwarePaginator instance
     */
    public function paginator($items, $perPage, $current_entity = null, $template = 'default')
    {
        return $this->remember(function () use ($items, $perPage, $current_entity, $template) {
            return $this->repository->paginator($items, $perPage, $current_entity, $template);
        });
    }

    /**
     * @param array $selected_ids
     * @param int $value
     * @param string $field
     *
     * @return \Illuminate\Support\Collection
     */
    public function markSelectedAs(array $selected_ids, $value, $field)
    {
        return $this->repository->markSelectedAs($selected_ids, $value, $field);
    }

    /**
     * @param int $id
     * @param string $type
     *
     * @return [type]
     */
    public function getFormBreadcrumbs($id, $type)
    {
        return $this->remember(function () use ($id, $type) {
            return $this->repository->getFormBreadcrumbs($id, $type);
        });
    }

    /**
     * @param \Closure $callback
     * @param null|string $key
     *
     * @return mixed
     */
    protected function remember(\Closure $callback, $key = null)
    {
        $cacheKey = $this->makeCacheKey($key);

        $store = $this->cache;

        if (method_exists($this->cache->getStore(), 'tags')) {
            $store = $store->tags([$this->entityName, 'global']);
        }

        return $store->remember($cacheKey, $this->cacheTime, $callback);
    }

    /**
     * @param \Closure $callback
     * @param null|string $key
     *
     * @return mixed
     */
    protected function rememberForever(\Closure $callback, $key = null)
    {
        $cacheKey = $this->makeCacheKey($key);

        $store = $this->cache;

        if (method_exists($this->cache->getStore(), 'tags')) {
            $store = $store->tags([$this->entityName, 'global']);
        }

        return $store->rememberForever($cacheKey, $callback);
    }

    /**
     * Generate a cache key with the called method name and its arguments
     * If a key is provided, use that instead.
     *
     * @param null|string $key
     *
     * @return string
     */
    private function makeCacheKey($key = null): string
    {
        if (null !== $key) {
            return $key;
        }

        $cacheKey = $this->getBaseKey();

        $backtrace = debug_backtrace()[2];

        return sprintf("$cacheKey %s %s", $backtrace['function'], serialize($backtrace['args']));
    }

    /**
     * @return string
     */
    protected function getBaseKey(): string
    {
        $prefix = env('CACHE_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_'));

        return sprintf(
            $prefix . ' -locale:%s -entity:%s',
            $this->locale,
            $this->entityName
        );
    }
}
