<?php

namespace Modules\Core\Eloquent;

use Illuminate\Container\Container;
use Illuminate\Pagination\Paginator;
use Modules\Core\Helpers\BackendPaginator;
use Modules\Core\Helpers\FrontendPaginator;
use Kalnoy\Nestedset\QueryBuilder as KalnoyBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

/**
 * @mixin \Illuminate\Database\Query\Builder
 */
class NestedBuilder extends KalnoyBuilder
{
    use Queryable;
    /**
     * The methods that should be returned from query builder.
     *
     * @var array
     */
    protected $passthru = [
        'insert', 'insertGetId', 'getBindings', 'toSql', 'toSqlWithBindings',
        'exists', 'doesntExist', 'count', 'min', 'max', 'avg', 'average', 'sum', 'getConnection',
    ];

    private $currentPaginator;

    public function __construct(QueryBuilder $query)
    {
        parent::__construct($query);

        $isBackend = str_contains(request()->getPathInfo(), config('application.core.core.admin-prefix', 'backend'));
        $this->currentPaginator = $isBackend ? BackendPaginator::class : FrontendPaginator::class;
    }

    /**
     * Paginate the given query.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     *
     * @return \Illuminate\Pagination\FrontendPaginator
     *
     * @throws \InvalidArgumentException
     */
    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        list($page, $perPage, $total, $results) = $this->currentPaginator::parseRoute($perPage, $columns, $pageName, $page, $this);

        return $this->paginator($results, $total, $perPage, $page, [
            'path' => $this->currentPaginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }

    /**
     * Create a new length-aware paginator instance.
     *
     * @param  \Illuminate\Support\Collection  $items
     * @param  int  $total
     * @param  int  $perPage
     * @param  int  $currentPage
     * @param  array  $options
     *
     * @return \Illuminate\Pagination\FrontendPaginator
     */
    protected function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith($this->currentPaginator, compact(
            'items',
            'total',
            'perPage',
            'currentPage',
            'options'
        ));
    }

    /**
     * Get dump current sql query with bindings
     *
     * @return string
     */
    public function toSqlWithBindings()
    {
        return dd(str_replace_array('?', $this->getBindings(), $this->toSql()));
    }

    /**
     * Update a record in the database.
     *
     * @param  array  $values
     * @return int
     */
    public function update(array $values)
    {
        unset($values['translations']);

        return $this->toBase()->update($this->addUpdatedAtColumn($values));
    }
}
