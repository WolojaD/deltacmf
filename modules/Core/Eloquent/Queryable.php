<?php

namespace Modules\Core\Eloquent;

use Illuminate\Pagination\Paginator;
use Modules\Buldozer\Managers\ModelConfigurationManagerInterface;

trait Queryable
{
    /**
     * Add any query sub select
     */
    public function addSubSelect($column, $query)
    {
        if (is_null($this->getQuery()->columns)) {
            $this->select($this->getQuery()->from . '.*');
        }

        return $this->selectSub($query->limit(1)->getQuery(), $column);
    }

    /**
     * Add any query sub select
     */
    public function addSubCount($query, $count_name = 'count')
    {
        if (is_null($this->getQuery()->columns)) {
            $this->select($this->getQuery()->from . '.*');
        }

        return $this->selectSub($query->selectRaw('count(*)')->getQuery(), $count_name);
    }

    /**
     * Paginate the given query in backend.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function backendPaginate($perPage = null, $template = 'default', $current_parent_id = false, $columns = ['*'], $pageName = 'page', $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $perPage = $perPage ?: $this->model->getPerPage();

        $results = ($total = $this->toBase()->getCountForPagination())
                                    ? $this->forPage($page, $perPage)->get($columns)
                                    : $this->model->newCollection();

        $options['path'] = Paginator::resolveCurrentPath();
        $options['pageName'] = $pageName;

        $options = app()->make(ModelConfigurationManagerInterface::class, [$this->model->getConfiguratorPath() ?? ''])->paginateOptionsGenerate($options, $this->model, $template, $current_parent_id);

        return $this->paginator($results, $total, $perPage, $page, $options);
    }
}
