<?php

namespace Modules\Core\Eloquent;

use Modules\Core\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class TranslationModel extends EloquentModel
{
    use NamespacedEntityTrait;

    protected $fillable = [];

    protected $guarded = ['id'];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $file = base_path($this->file_path);

        if (!is_file($file)) {
            return;
        }

        $model = json_decode(file_get_contents($file));
        $fields = collect($model->templates->default)->filter(function ($field, $id) {
            if ($id == 'standard') {
                return false;
            }

            return $field->fillable ?? true;
        });

        $this->fillable = $fields->filter(function ($field) {
            return $field->is_translated ?? false;
        })->map(function ($field, $field_name) {
            return $field_name;
        })->toArray();
    }
}
