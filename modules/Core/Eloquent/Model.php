<?php

namespace Modules\Core\Eloquent;

use Modules\Core\Traits\NestedTrait;
use Modules\Core\Traits\DraftableTrait;
use Modules\Logs\Traits\TrackableTrait;
use Modules\Core\Traits\FileProcessTrait;
use Modules\Core\Traits\FileUploaderTrait;
use Modules\Core\Traits\ImageProcessTrait;
use Modules\Core\Traits\NamespacedEntityTrait;
use Modules\Core\Traits\SearchableSortableTrait;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Modules\Buldozer\Managers\ModelConfigurationManagerInterface;

class Model extends EloquentModel
{
    use SearchableSortableTrait,
        NamespacedEntityTrait,
        FileUploaderTrait,
        ImageProcessTrait,
        FileProcessTrait,
        TrackableTrait,
        DraftableTrait,
        NestedTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $fillable = [];

    protected $with = [];

    protected $searchable = [];

    protected $to_save = [];

    protected $guarded = ['id'];

    protected $model_configuration_manager;

    /*
    |--------------------------------------------------------------------------
    | Boot and __construct
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        parent::boot();
        // FOR IMAGE CROP UPLOAD
        static::saving(function ($model) {
            foreach ($model->attributes as $field => $data) {
                if (strpos($field, 'image_') === 0) {
                    unset($model->attributes[$field]);
                }
            }
        });
    }

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $parameters = app()->make(ModelConfigurationManagerInterface::class, [$this->file_path ?? ''])->modelConstruct();

        if (method_exists($this, 'translations')) {
            $this->translatedAttributes = array_values($parameters['translatedAttributes']);
        }

        $this->with = array_values($parameters['with']);
        $this->searchable = array_values($parameters['searchable']);
        $this->appends = array_values($parameters['appends']);
        $this->to_save = array_values($parameters['to_save']);

        parent::__construct($attributes);
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getJsonList()
    {
        return app()->make(ModelConfigurationManagerInterface::class, [$this->file_path ?? ''])->getList();
    }

    public function getColumns($template = 'default')
    {
        return app()->make(ModelConfigurationManagerInterface::class, [$this->file_path ?? ''])->getColumns($this, $template);
    }

    /**
     * Get all headers user can view in this template
     *
     * @param $columns
     *
     * @return mixed
     */
    public function getHeadersOfCollectionFor($template = 'default')
    {
        $columns = $this->getColumns($template)->filter(function ($item) {
            return $item['column_type'];
        });

        $headers = $columns->mapWithKeys(function ($item, $id) {
            return [$id => $item['title']];
        })->toArray();

        return isset($this->headers) ? array_merge($this->headers, $headers) : $headers;
    }

    public function hasStatus()
    {
        return $this->getJsonList()->templates->default->standard->status ?? false;
    }

    /**
     * Checks if the current url matches the configured backend uri.
     *
     * @return bool
     */
    private function onBackend()
    {
        $url = request()->getPathInfo();

        if (str_contains($url, config('application.core.core.admin-prefix', 'backend'))) {
            return true;
        }

        return false;
    }

    public function getClass()
    {
        return get_class($this);
    }

    public function isNested()
    {
        return $this->getJsonList()->templates->default->standard->nested ?? false;
    }

    /**
     * Changes builder to custom
     *
     * @param [type] $query
     *
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    public function getConfiguratorPath()
    {
        return $this->file_path;
    }
}
