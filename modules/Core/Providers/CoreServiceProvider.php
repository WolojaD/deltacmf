<?php

namespace Modules\Core\Providers;

use Nwidart\Modules\Module;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Blade;
use Modules\Core\Facades\LaravelLocalization;
use Modules\Core\Foundation\Theme\ThemeManager;
use Modules\Core\Console\Commands\SeedMakeCommand;
use Modules\Core\Console\Commands\ModelMakeCommand;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Logs\Console\Commands\LogsClearCommand;
use Modules\Core\Console\Commands\CacheClearCommand;
use Modules\Core\Console\Commands\FieldsInfoCommand;
use Modules\Core\Console\Commands\ModuleMakeCommand;
use Modules\Core\Console\Commands\RequestMakeCommand;
use Modules\Core\Console\Commands\MigrateFreshCommand;
use Modules\Core\Console\Commands\MigrationMakeCommand;
use Modules\Core\Console\Commands\ControllerMakeCommand;
use Modules\Core\Console\Commands\ModuleGenerateCommand;
use Modules\Core\Console\Commands\RepositoryMakeCommand;
use Modules\Core\Console\Commands\MigrateFreshSeedCommand;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * The filters base class name.
     *
     * @var array
     */
    protected $middleware = [
        'Core' => [
            'permissions' => 'PermissionMiddleware',
            'auth.admin' => 'AdminMiddleware',
            'localizationRedirect' => 'LocalizationMiddleware',
            'can' => 'Authorization',
        ],
    ];

    /**
     * Boot the application events.
     */
    public function boot()
    {
        $this->publishConfig('core', 'available-locales');
        $this->publishConfig('core', 'available-locales-backend');
        $this->publishConfig('core', 'config');
        $this->publishConfig('core', 'core');
        $this->publishConfig('core', 'settings');
        $this->publishConfig('core', 'permissions');

        $this->registerMiddleware($this->app['router']);

        $this->registerModuleResourceNamespaces();

        $this->bladeDirectives();
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        $this->registerAvailableLocales();

        $this->registerCommands();
        $this->registerServices();
        $this->setLocalesConfigurations();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('core_core', array_dot(trans('core::core')));
            $event->load('core_fields', array_dot(trans('core::fields')));
            $event->load('core_form', array_dot(trans('core::form')));
            $event->load('core_navigation', array_dot(trans('core::navigation')));
            $event->load('core_permissions', array_dot(trans('core::permissions')));
            $event->load('core_table', array_dot(trans('core::table')));
            $event->load('core_alerts', array_dot(trans('core::alerts')));
            $event->load('core_breadcrumbs', array_dot(trans('core::breadcrumbs')));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->singleton('application.onBackend', function () {
            return $this->onBackend();
        });

        $this->app->singleton(LaravelLocalization::class, function () {
            return new LaravelLocalization();
        });

        $this->app->alias(LaravelLocalization::class, 'laravellocalization');
    }

    /**
     * Register the filters.
     *
     * @param Router $router
     */
    public function registerMiddleware(Router $router)
    {
        foreach ($this->middleware as $module => $middlewares) {
            foreach ($middlewares as $name => $middleware) {
                $class = "Modules\\{$module}\\Http\\Middleware\\{$middleware}";

                $router->aliasMiddleware($name, $class);
            }
        }
    }

    /**
     * Register the console commands.
     */
    private function registerCommands()
    {
        $this->commands([
            MigrateFreshCommand::class,
            MigrateFreshSeedCommand::class,
            ModuleGenerateCommand::class,
            RepositoryMakeCommand::class,
            ControllerMakeCommand::class,
            MigrationMakeCommand::class,
            SeedMakeCommand::class,
            ModuleMakeCommand::class,
            ModelMakeCommand::class,
            RequestMakeCommand::class,
            FieldsInfoCommand::class,
            CacheClearCommand::class,
            LogsClearCommand::class,
        ]);
    }

    private function registerServices()
    {
        $this->app->singleton(ThemeManager::class, function ($app) {
            $path = base_path() . '/themes';

            return new ThemeManager($app, $path);
        });

        $this->app->singleton('application.ModulesList', function () {
            return array_keys($this->app['modules']->toCollection()->toArray());
        });
    }

    /**
     * Register the modules aliases.
     */
    private function registerModuleResourceNamespaces()
    {
        $themes = [];

        if (true === $this->app['config']->get('application.core.core.enable-theme-overrides')) {
            $themeManager = app(ThemeManager::class);

            if (\Schema::hasTable('settings')) {
                $themes = [
                    'backend' => $themeManager->find(settings('core::backend_theme', null, 'artisan'))->getPath(),
                    'frontend' => $themeManager->find(settings('core::frontend_theme', null, 'simplest'))->getPath(),
                ];
            } else {
                $themes = [
                    'backend' => $themeManager->find(config('application.core.core.backend-theme', 'artisan'))->getPath(),
                    'frontend' => $themeManager->find(config('application.core.core.frontend-theme', 'simplest'))->getPath(),
                ];
            }
        }

        foreach ($this->app['modules']->getOrdered() as $module) {
            $this->registerViewNamespace($module, $themes);
            $this->registerLanguageNamespace($module);
        }

        $this->loadTranslationsFrom($themes['frontend'] . '/lang', 'frontend');
    }

    /**
     * Register the view namespaces for the modules.
     *
     * @param Module $module
     * @param array  $themes
     */
    protected function registerViewNamespace(Module $module, array $themes)
    {
        $hints = [];
        $moduleName = $module->getLowerName();

        if (is_core_module($moduleName)) {
            $configFile = 'config';
            $configKey = 'application.' . $moduleName . '.' . $configFile;

            if ($this->checkFileExists($moduleName, $configFile)) {
                $this->mergeConfigFrom($module->getExtraPath('Config' . DIRECTORY_SEPARATOR . $configFile . '.php'), $configKey);
            } else {
                $this->customMergeConfigFrom(\Yaml::parseFile($module->getExtraPath('Config' . DIRECTORY_SEPARATOR . $configFile . '.yml')), $configKey);
            }

            $moduleConfig = $this->app['config']->get($configKey . '.useViewNamespaces');

            if (count($themes) > 0) {
                if (null !== $themes['backend'] && true === array_get($moduleConfig, 'backend-theme')) {
                    $hints[] = $themes['backend'] . '/views/' . $moduleName;
                    $hints[] = $themes['backend'] . '/views';
                }

                if (null !== $themes['frontend'] && true === array_get($moduleConfig, 'frontend-theme')) {
                    $hints[] = $themes['frontend'] . '/views/' . $moduleName;
                    $hints[] = $themes['frontend'] . '/views';
                }
            }

            if (true === array_get($moduleConfig, 'resources')) {
                $hints[] = base_path('modules/' . $module->getName() . '/Resources/views/');
            }
        }

        $this->app['view']->addNamespace($moduleName, $hints);
    }

    /**
     * Register the language namespaces for the modules.
     *
     * @param Module $module
     */
    protected function registerLanguageNamespace(Module $module)
    {
        $moduleName = $module->getLowerName();

        $langPath = base_path("resources/lang/$moduleName");
        $secondPath = base_path("resources/lang/translation/$moduleName");

        if ('translation' !== $moduleName && $this->hasPublishedTranslations($langPath)) {
            return $this->loadTranslationsFrom($langPath, $moduleName);
        }

        if ($this->hasPublishedTranslations($secondPath)) {
            return $this->loadTranslationsFrom($secondPath, $moduleName);
        }

        if ($this->moduleHasCentralisedTranslations($module)) {
            return $this->loadTranslationsFrom($this->getCentralisedTranslationPath($module), $moduleName);
        }

        return $this->loadTranslationsFrom($module->getPath() . '/Resources/lang', $moduleName);
    }

    /**
     * @param string $file
     *
     * @return string
     */
    private function getConfigFilename(string $file)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }

    /**
     * Set the locale configuration for
     * - laravel localization
     * - laravel translatable.
     */
    private function setLocalesConfigurations()
    {
        if (app()->environment() === 'testing' || !\Schema::hasTable('settings')) {
            return;
        }

        $localeConfig = $this->app['cache']
            ->tags(['settings', 'global'])
            ->remember('application.locales', 300, function () {
                return \Modules\Settings\Entities\Settings::whereName('core::locales')->first();
            });

        if ($localeConfig) {
            $locales = json_decode($localeConfig->plainValue);

            config(['translatable.locales' => $locales]);

            if (!$this->onBackend()) {
                $availableLocales = [];

                foreach ($locales as $locale) {
                    $availableLocales = array_merge($availableLocales, [$locale => config("application.core.available-locales.$locale")]);
                }

                $laravelDefaultLocale = config('app.locale');

                config(['app.locale' => array_keys($availableLocales)[0]]);
                config(['laravellocalization.supportedLocales' => $availableLocales]);
            } elseif ($this->onBackend()) {
                if (!in_array(config('app.locale'), ['ru', 'en'])) {
                    config(['app.locale' => 'ru']);
                }

                config(['laravellocalization.supportedLocales' => config('application.core.available-locales-backend')]);
            }
        }
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    private function hasPublishedTranslations($path)
    {
        return is_dir($path);
    }

    /**
     * Does a Module have it's Translations centralised in the Translation module?
     *
     * @param Module $module
     *
     * @return bool
     */
    private function moduleHasCentralisedTranslations(Module $module)
    {
        return is_dir($this->getCentralisedTranslationPath($module));
    }

    /**
     * Get the absolute path to the Centralised Translations for a Module (via the Translations module).
     *
     * @param Module $module
     *
     * @return string
     */
    private function getCentralisedTranslationPath(Module $module)
    {
        $path = config('modules.paths.modules') . '/Translation';

        return $path . "/Resources/lang/{$module->getLowerName()}";
    }

    /**
     * Checks if the current url matches the configured backend uri.
     *
     * @return bool
     */
    private function onBackend()
    {
        $url = request()->getPathInfo();

        if (str_contains($url, config('application.core.core.admin-prefix', 'backend'))) {
            return true;
        }

        return false;
    }

    /**
     * Get argument array from argument string.
     *
     * @param string $argumentString
     *
     * @return array
     */
    private function getArguments(string $argumentString)
    {
        return str_getcsv($argumentString, ',', "'");
    }

    /**
     * List of Custom Blade Directives.
     */
    private function bladeDirectives()
    {
        if (app()->environment() === 'testing') {
            return;
        }

        // Access by given role slug
        Blade::directive('accessByRoleSlug', function ($expression) {
            if (json_decode(str_replace('\'', '"', $expression)) && count(json_decode(str_replace('\'', '"', $expression))) > 1) {
                return "<?php if (\Auth::user() && \Auth::user()->roles->first() && in_array(\Auth::user()->roles->first()->slug, $expression)) { ?>";
            }

            return "<?php if (\Auth::user() && \Auth::user()->roles->first() && \Auth::user()->roles->first()->slug == strval($expression)) { ?>";
        });
        Blade::directive('elseAccessByRoleSlug', function () {
            return '<?php } else { ?>';
        });
        Blade::directive('endAccessByRoleSlug', function () {
            return '<?php } ?>';
        });

        // Access by role type admin
        Blade::directive('accessByRoleTypeAdmin', function () {
            return "<?php if (\Auth::user() && \Auth::user()->roles->first() && (boolval(\Auth::user()->roles->first()->is_admin) === boolval(1))) { ?>";
        });
        Blade::directive('elseAccessByRoleTypeAdmin', function () {
            return '<?php } else { ?>';
        });
        Blade::directive('endAccessByRoleTypeAdmin', function () {
            return '<?php } ?>';
        });

        // Check if file exists
        Blade::directive('isFileExists', function ($expression) {
            return "<?php if ($expression && null !== $expression && is_file(public_path('storage/origin/' . $expression))) { ?>";
        });
        Blade::directive('elseIsFileExists', function () {
            return '<?php } else { ?>';
        });
        Blade::directive('endIsFileExists', function () {
            return '<?php } ?>';
        });
    }
}
