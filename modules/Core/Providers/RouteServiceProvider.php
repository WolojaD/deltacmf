<?php

namespace Modules\Core\Providers;

use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Modules\Core\Macros\Routing\ApiRoutes;
use Modules\Core\Macros\Routing\BackendRoutes;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

abstract class RouteServiceProvider extends ServiceProvider
{
    /**
     * The root namespace to assume when generating URLs to actions.
     *
     * @var string
     */
    protected $namespace = '';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot()
    {
        ApiRoutes::registerMacros();
        BackendRoutes::registerMacros();

        parent::boot();
    }

    /**
     * @return string
     */
    abstract protected function getFrontendRoute();

    /**
     * @return string
     */
    abstract protected function getBackendRoute();

    /**
     * @return string
     */
    abstract protected function getApiRoute();

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {
            $this->loadApiRoutes($router);
        });

        $router->group([
            'namespace' => $this->namespace,
            'prefix' => app('laravellocalization')->setLocale(),
            'middleware' => ['localizationRedirect', 'backend'],
        ], function (Router $router) {
            $this->loadBackendRoutes($router);
        });

        $router->group([
            'namespace' => $this->namespace,
            'prefix' => app('laravellocalization')->setLocale(),
            'middleware' => ['localizationRedirect', 'web'],
        ], function (Router $router) {
            $this->loadFrontendRoutes($router);
        });
    }

    /**
     * @param Router $router
     */
    private function loadFrontendRoutes(Router $router)
    {
        $frontend = $this->getFrontendRoute();

        if ($frontend && file_exists($frontend)) {
            $router->group([
                'middleware' => config('application.core.core.middleware.frontend', []),
            ], function (Router $router) use ($frontend) {
                require $frontend;
            });
        }
    }

    /**
     * @param Router $router
     */
    private function loadBackendRoutes(Router $router)
    {
        $backend = $this->getBackendRoute();

        if ($backend && file_exists($backend)) {
            $router->group([
                'namespace' => 'Admin',
                'prefix' => config('application.core.core.admin-prefix', 'backend'),
                'middleware' => config('application.core.core.middleware.backend', ['auth.admin']),
            ], function (Router $router) use ($backend) {
                require $backend;
            });
        }
    }

    /**
     * @param Router $router
     */
    private function loadApiRoutes(Router $router)
    {
        $api = $this->getApiRoute();

        if ($api && file_exists($api)) {
            $router->group([
                'namespace' => 'Api',
                'prefix' => app('laravellocalization')->setLocale() . '/api/' . config('application.core.core.admin-prefix', 'backend'),
                'middleware' => config('application.core.core.middleware.api', []),
            ], function (Router $router) use ($api) {
                require $api;
            });
        }
    }
}
