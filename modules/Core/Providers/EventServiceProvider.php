<?php

namespace Modules\Core\Providers;

use Illuminate\Mail\Events\MessageSent;
use Modules\Logs\Loggers\LogSentMessage;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        MessageSent::class => [
            LogSentMessage::class,
        ],
    ];
}
