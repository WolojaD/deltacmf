<?php

namespace Modules\Core\Providers;

use Modules\Core\Traits\CanPublishConfigurationTrait;
use Illuminate\Support\ServiceProvider as SupportServiceProvider;

class ServiceProvider extends SupportServiceProvider
{
    use CanPublishConfigurationTrait;
}
