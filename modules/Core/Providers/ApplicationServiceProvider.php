<?php

namespace Modules\Core\Providers;

use Nwidart\Modules\Facades\Module;
use Illuminate\Foundation\AliasLoader;
use Nwidart\Modules\LaravelModulesServiceProvider;
use Modules\Translation\Providers\TranslationServiceProvider;

class ApplicationServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Use symfony yaml via https://github.com/antonioribeiro/yaml
        \Yaml::loadToConfig(config_path('application'), 'application');

        if (class_exists(TranslationServiceProvider::class)) {
            $this->app->register(TranslationServiceProvider::class);
        }

        $this->app->register(LaravelModulesServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Module', Module::class);

        Module::macro('activePaths', function () {
            $active = Module::getByStatus(1);
            $activePaths = [];

            foreach ($active as $activeModule) {
                $module_path = $activeModule->getPath();
                $path = $module_path . '/Routes/router.js';

                if (is_file($path)) {
                    $explodedPath = explode('/', $module_path);

                    $activePaths[] = array_pop($explodedPath);
                }
            }

            return $activePaths;
        });
    }
}
