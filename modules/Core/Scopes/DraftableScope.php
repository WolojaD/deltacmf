<?php

namespace Modules\Core\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Illuminate\Database\Query\Builder as QueryBuilder;

class DraftableScope implements Scope
{
    use ValidateUserByRoleAccess;

    /**
     * Apply scope on the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $has_status = method_exists($model, 'hasStatus') ?
            $model->hasStatus() :
            \Schema::hasColumn($model->getTable(), $model->getStatusColumn());

        if ($has_status) {
            $column = $model->getQualifiedStatusColumn();

            if (app()->make('application.onBackend')) {
                return $builder;
            } else {
                if ($this->userDeveloper() || $this->userAdmin()) {
                    $builder->whereIn($column, [1, 2]);
                } else {
                    $builder->where($column, '=', 1);
                }

                $this->addWithDrafts($builder);
            }
        }

        return $builder;
    }

    /**
     * Remove scope from the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
        $has_status = method_exists($model, 'hasStatus') ?
            $model->hasStatus() :
            \Schema::hasColumn($model->getTable(), $model->getStatusColumn());

        if ($has_status) {
            $query = $builder->getQuery();

            $column = $model->getQualifiedStatusColumn();

            $bindingKey = 0;

            foreach ((array) $query->wheres as $key => $where) {
                if ($this->isPublishedConstraint($where, $column)) {
                    $this->removeWhere($query, $key);

                    $this->removeBinding($query, $bindingKey);
                }

                if (!in_array($where['type'], ['Null', 'NotNull'])) {
                    $bindingKey++;
                }
            }
        }
    }

    /**
     * Remove scope constraint from the query.
     *
     * @param  \Illuminate\Database\Query\Builder  $builder
     * @param  int  $key
     *
     * @return void
     */
    protected function removeWhere(QueryBuilder $query, $key)
    {
        unset($query->wheres[$key]);

        $query->wheres = array_values($query->wheres);
    }

    /**
     * Remove scope constraint from the query.
     *
     * @param  \Illuminate\Database\Query\Builder  $builder
     * @param  int  $key
     *
     * @return void
     */
    protected function removeBinding(QueryBuilder $query, $key)
    {
        $bindings = $query->getRawBindings()['where'];

        unset($bindings[$key]);

        $query->setBindings($bindings);
    }

    /**
     * Check if given where is the scope constraint.
     *
     * @param  array   $where
     * @param  string  $column
     *
     * @return boolean
     */
    protected function isPublishedConstraint(array $where, $column)
    {
        return ($where['type'] == 'Basic' && $where['column'] == $column && $where['value'] == 1);
    }

    /**
     * Extend Builder with custom method.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     */
    protected function addWithDrafts(Builder $builder)
    {
        $builder->macro('withDrafts', function (Builder $builder) {
            $this->remove($builder, $builder->getModel());

            return $builder;
        });
    }
}
