<?php

namespace Modules\Core\Tests\Unit;

use Modules\Core\Tests\CoreTestCase;

class HelpersTest extends CoreTestCase
{
    /** @test */
    public function is_sorted_array_returns_true_if_array_is_sorted_alphabetically()
    {
        $array = [
            'beliver',
            'bronte',
            'carpitulatis'
        ];

        $this->assertTrue(is_sorted_array($array));
    }

    /** @test */
    public function is_sorted_array_returns_false_if_array_is_not_sorted_alphabetically()
    {
        $array = [
            'bronte',
            'beliver',
            'carpitulatis'
        ];

        $this->assertFalse(is_sorted_array($array));
    }
}
