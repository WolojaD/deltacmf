<?php

namespace Modules\Core\Tests;

use Tests\TestCase;
use Modules\Core\Eloquent\Model;
use Modules\Core\Exceptions\Handler;
use FloatingPoint\Stylist\Theme\Theme;
use FloatingPoint\Stylist\Theme\Loader;
use FloatingPoint\Stylist\Theme\Stylist;
use Illuminate\Contracts\Console\Kernel;
use Modules\Users\Repositories\UserInterface;
use Modules\Core\Providers\CoreServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Nwidart\Modules\LaravelModulesServiceProvider;
use Modules\Settings\Providers\SettingsServiceProvider;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider;

abstract class CoreTestCase extends TestCase
{
    use RefreshDatabase;

    protected $app;

    protected $user;

    public function createApplication()
    {
        $app = require __DIR__ . '/../../../bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();
        $this->getEnvironmentSetUp($app);

        return $app;
    }

    public function setUp()
    {
        parent::setUp();

        $this->resetDatabase();
        $this->getEnvironmentSetUp($this->app);

        $this->user = app(UserInterface::class);
        $this->loginWithFakeUser();

        $stylist = new Stylist(new Loader, $this->app);
        $theme = new Theme('builder', 'Testing', $this->getPath());
        $stylist->register($theme, true);

        $this->disableExceptionHandling();
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelModulesServiceProvider::class,
            CoreServiceProvider::class,
            LaravelLocalizationServiceProvider::class,
            SettingsServiceProvider::class,
            'Collective\Html\HtmlServiceProvider',
            'FloatingPoint\Stylist\StylistServiceProvider',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Eloquent' => Model::class,
            'LaravelLocalization' => LaravelLocalization::class,
            'Stylist' => 'FloatingPoint\Stylist\Facades\StylistFacade',
            'Theme' => 'FloatingPoint\Stylist\Facades\ThemeFacade',
        ];
    }

    protected function getApplicationAliases($app)
    {
        $aliases = parent::getApplicationAliases($app);

        $aliases['Stylist'] = 'FloatingPoint\Stylist\Facades\StylistFacade';

        return $aliases;
    }

    protected function getPath()
    {
        return base_path() . '/themes/builder';
    }

    public function loginWithFakeUser()
    {
        $user = $this->user->create([
            'email' => 'qwerty@qwerty.com',
            'password' => 'qwerty',
        ]);

        $this->actingAs($user);
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.locale', 'ru');
        $app['config']->set('translatable.locales', ['ru', 'en']);
        $app['config']->set('laravellocalization.supportedLocales', [
            'ru' => ['name' => 'Russian', 'script' => 'Cyrl', 'native' => 'Русский'],
            'en' => ['name' => 'English', 'script' => 'Latn', 'native' => 'English'],
        ]);
        $app['config']->set('laravellocalization.defaultApplicationLocales', [
            'ru' => ['name' => 'Russian', 'script' => 'Cyrl', 'native' => 'Русский'],
            'en' => ['name' => 'English', 'script' => 'Latn', 'native' => 'English'],
        ]);
    }

    private function resetDatabase()
    {
        // $migrationsPath = 'Database/Migrations';
        // $artisan = $this->app->make(Kernel::class);
        // Makes sure the migrations table is created
        $this->artisan('migrate', [
            '--database' => 'sqlite',
        ]);
        // We empty all tables
        $this->artisan('migrate:reset', [
            '--database' => 'sqlite',
        ]);
        // Migrate
        $this->artisan('migrate', [
            '--database' => 'sqlite',
        ]);
        // $this->artisan('application:migrate:fresh');
    }

    // Hat tip, @adamwathan.
    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct()
            {
            }

            public function report(\Exception $e)
            {
            }

            public function render($request, \Exception $e)
            {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);

        return $this;
    }
}
