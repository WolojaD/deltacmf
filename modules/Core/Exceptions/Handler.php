<?php

namespace Modules\Core\Exceptions;

use Exception;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                case '401':
                    return \Response::view('core::errors.401', [], 401);

                    break;
                case '403':
                    return \Response::view('core::errors.403', [], 403);

                    break;
                case '404':
                    return \Response::view('core::errors.404', [], 404);

                    break;
                case '500':
                    return \Response::view('core::errors.500', [], 500);

                    break;
                case '503':
                    return \Response::view('core::errors.503', [], 503);

                    break;
                default:
                    return $this->renderHttpException($exception);

                    break;
            }
        }

        if ($exception instanceof TokenMismatchException) {
            return redirect()->route('login')
                        ->withInput($request->except('_token', 'password'))
                        ->withError(trans('users::auth.errors.token mismatch exception'));
        }

        if ($exception instanceof ValidationException) {
            return parent::render($request, $exception);
        }

        if (config('app.debug') === false) {
            return $this->handleExceptions($exception);
        }

        return parent::render($request, $exception);
    }

    private function handleExceptions($exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return response()->view('page::errors.404', [], 404);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->view('page::errors.404', [], 404);
        }

        if ($exception instanceof TokenMismatchException) {
            return redirect()->route('login')
                        ->withInput($request->except('_token', 'password'))
                        ->withError(trans('users::auth.errors.token mismatch exception'));
        }

        return response()->view('page::errors.500', [], 500);
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpException $exception)
    {
        $status = $exception->getStatusCode();

        view()->replaceNamespace('errors', [
            resource_path('views/errors'),
            __DIR__ . '/views',
        ]);

        if (view()->exists($view = "page::errors.{$status}")) {
            return response()->view($view, ['exception' => $exception], $status, $exception->getHeaders());
        }

        return $this->convertExceptionToResponse($exception);
    }
}
