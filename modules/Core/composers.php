<?php

use Jenssegers\Agent\Agent;

view()->composer('core::layouts.application', \Modules\Core\Composers\LocaleComposer::class);
view()->composer('core::layouts.application', \Modules\Core\Composers\TranslationsComposer::class);
view()->composer('core::layouts.application', \Modules\Core\Composers\ModulesComposer::class);
view()->composer('core::router_view', \Modules\Core\Composers\ThemeComposer::class);
view()->composer('*', \Modules\Core\Composers\CurrentUserComposer::class);

view()->composer('*', function ($view) {
    $view->withShortcodes();
});

view()->share('agent', new Agent());
