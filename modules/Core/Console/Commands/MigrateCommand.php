<?php

namespace  Modules\Core\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Illuminate\Console\ConfirmableTrait;
use Modules\FieldGenerator\Entities\Field;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Database\Console\Migrations\BaseCommand;

class MigrateCommand extends BaseCommand
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:migrate {--database= : The database connection to use.}
                {--force : Force the operation to run when in production.}
                {--path= : The path to the migrations files to be executed.}
                {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths.}
                {--pretend : Dump the SQL queries that would be run.}
                {--seed : Indicates if the seed task should be re-run.}
                {--step : Force the migrations to be run so they can be rolled back individually.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the database migrations';

    /**
     * The migrator instance.
     *
     * @var \Illuminate\Database\Migrations\Migrator
     */
    protected $migrator;

    /**
     * Create a new migration command instance.
     *
     * @param \Illuminate\Database\Migrations\Migrator $migrator
     */
    public function __construct(Migrator $migrator)
    {
        parent::__construct();

        $this->migrator = $migrator;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (!$this->confirmToProceed()) {
            return;
        }

        $this->prepareDatabase();

        // Next, we will check to see if a path option has been defined. If it has
        // we will use the path relative to the root of this installation folder
        // so that migrations may be run for any path within the applications.
        $this->migrator->run($this->getMigrationPaths(), [
            'pretend' => $this->option('pretend'),
            'step' => $this->option('step'),
        ]);

        // Once the migrator has run we will grab the note output and send it out to
        // the console screen, since the migrator itself functions without having
        // any instances of the OutputInterface contract passed into the class.
        foreach ($this->migrator->getNotes() as $note) {
            $this->output->writeln($note);
        }

        $tables = \DB::connection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tables as $table) {
            $this->generateFieldsInfo($table);
        }

        // Finally, if the "seed" option has been given, we will re-run the database
        // seed task to re-populate the database, which is convenient when adding
        // a migration and a seed at the same time, as it is only this command.
        if ($this->option('seed')) {
            $this->call('db:seed', ['--force' => true]);
        }
    }

    protected function generateFieldsInfo($table)
    {
        if ('migrations' == $table || 'fields' == $table) {
            return;
        }

        $columns = Schema::getColumnListing($table);

        foreach ($columns as $column) {
            $this->generateFieldInfo($table, $column);
        }
    }

    protected function generateFieldInfo($table, $column)
    {
        if (in_array($column, ['id', 'created_at', 'updated_at', 'order', 'depth'])) {
            return;
        }

        $names = [
            'table_name' => $table,
            'table_field' => $column,
        ];
        $params = [
            'status' => [
                'type' => 'select_status',
                'label' => '{"en":"Status","ru":"Статус"}',
                'settings' => '',
                'validation' => '',
            ],
            'parent_id' => [
                'type' => 'select_from_relation',
                'settings' => '',
                'validation' => '',
                'is_parent' => 1,
            ],
            'email' => [
                'type' => 'select_status',
                'label' => '{"en":"E-mail","ru":"E-mail"}',
                'settings' => '',
                'validation' => '',
            ],
            'password' => [
                'type' => 'password',
                'label' => '{"en":"Password","ru":"Пароль"}',
                'settings' => '',
                'validation' => '',
            ],
        ];

        if (isset($params[$column])) {
            Field::updateOrCreate(array_merge($names, $params[$column]));
        }

        $filtered = $this->filter($column);

        if ($filtered) {
            Field::updateOrCreate(array_merge($names, $filtered));
        }
    }

    public function filter($column)
    {
        preg_match('/(.*)_id$/', $column, $relation);

        if (isset($relation)) {
            return [
                'type' => 'select_from_relation',
                'label' => '{"en":"' . Str::ucfirst($relation[1]) . '","ru":"' . Str::ucfirst($relation[1]) . '"}',
                'settings' => '',
                'validation' => '',
                'relation' => $relation[1],
            ];
        }

        preg_match('/^is_(.*)/', $column, $checkbox);

        if (isset($checkbox)) {
            return [
                'type' => 'checkbox',
                'label' => '{"en":"' . Str::ucfirst($checkbox[1]) . '","ru":"' . Str::ucfirst($checkbox[1]) . '"}',
                'settings' => '',
                'validation' => '',
                'relation' => $checkbox[1],
            ];
        }

        preg_match('/(.*)_color$/', $column, $color);

        if (isset($color)) {
            return [
                'type' => 'color_picker',
                'label' => '{"en":"' . Str::ucfirst($color[1]) . ' color","ru":"' . Str::ucfirst($color[1]) . ' color"}',
                'settings' => '',
                'validation' => '',
                'relation' => $color[1],
            ];
        }

        preg_match('/(.*)_image$/', $column, $image);

        if (isset($image)) {
            return [
                'type' => 'upload_image',
                'label' => '{"en":"' . Str::ucfirst($image[1]) . ' image","ru":"' . Str::ucfirst($image[1]) . ' image"}',
                'settings' => '',
                'validation' => '',
                'relation' => $image[1],
            ];
        }

        return false;
    }

    /**
     * Prepare the migration database for running.
     */
    protected function prepareDatabase()
    {
        $this->migrator->setConnection($this->option('database'));

        if (!$this->migrator->repositoryExists()) {
            $this->call(
                'migrate:install',
                ['--database' => $this->option('database')]
            );
        }
    }
}
