<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;

class CacheClearCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'application:cache-clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear cache and other stuff';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->call('route:clear');
        $this->call('view:clear');
        $this->call('config:clear');
        $this->call('cache:clear');
        $this->call('debugbar:clear');

        $files = scandir(base_path('bootstrap/cache'));

        foreach ($files as $key => $file) {
            if ('.gitignore' != $file && is_file(base_path('bootstrap/cache') . '/' . $file)) {
                unlink(base_path('bootstrap/cache') . '/' . $file);
            }
        }
    }
}
