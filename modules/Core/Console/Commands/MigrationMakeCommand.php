<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Support\Str;
use Nwidart\Modules\Support\Stub;
use Nwidart\Modules\Commands\GeneratorCommand;
use Nwidart\Modules\Traits\ModuleCommandTrait;
use Symfony\Component\Console\Input\InputOption;
use Nwidart\Modules\Support\Migrations\NameParser;
use Symfony\Component\Console\Input\InputArgument;
use Nwidart\Modules\Support\Migrations\SchemaParser;
use Nwidart\Modules\Support\Config\GenerateConfigReader;

class MigrationMakeCommand extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'application:module:make-migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new migration for the specified module.';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The migration name will be created.'],
            ['module', InputArgument::OPTIONAL, 'The name of module will be created.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['fields', null, InputOption::VALUE_OPTIONAL, 'The specified fields table.', null],
            ['plain', null, InputOption::VALUE_NONE, 'Create plain migration.'],
            ['asker', 'a', InputOption::VALUE_NONE, 'Ask all fields'],
        ];
    }

    /**
     * Get schema parser.
     *
     * @return SchemaParser
     */
    public function getSchemaParser()
    {
        return new SchemaParser($this->option('fields'));
    }

    /**
     * @throws \InvalidArgumentException
     *
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $parser = new NameParser($this->argument('name'));

        if ($parser->isCreate()) {
            $text = '';

            if ($this->option('asker')) {
                $fields = $this->askForFields();
                $text = $fields['result'];

                $model = Str::ucfirst(Str::singular($parser->getTableName()));

                $this->call('application:module:make-seed', [
                    'name' => $model . 'Fields',
                    '--data' => $fields['info'],
                    'module' => $this->getModuleName(),
                ]);

                return Stub::create('/migration/create.stub', [
                    'class' => $this->getClass(),
                    'table' => $parser->getTableName(),
                    'fields' => $this->getSchemaParser()->render(),
                    'text' => $text,
                ]);
            }

            return Stub::create('/migration/createModule.stub', [
                'class' => studly_case($this->argument('module'))
            ]);
        } elseif ($parser->isAdd()) {
            return Stub::create('/migration/add.stub', [
                'class' => $this->getClass(),
                'table' => $parser->getTableName(),
                'fields_up' => $this->getSchemaParser()->up(),
                'fields_down' => $this->getSchemaParser()->down(),
            ]);
        } elseif ($parser->isDelete()) {
            return Stub::create('/migration/delete.stub', [
                'class' => $this->getClass(),
                'table' => $parser->getTableName(),
                'fields_down' => $this->getSchemaParser()->up(),
                'fields_up' => $this->getSchemaParser()->down(),
            ]);
        } elseif ($parser->isDrop()) {
            return Stub::create('/migration/drop.stub', [
                'class' => $this->getClass(),
                'table' => $parser->getTableName(),
                'fields' => $this->getSchemaParser()->render(),
            ]);
        }

        return Stub::create('/migration/plain.stub', [
            'class' => $this->getClass(),
        ]);
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $generatorPath = GenerateConfigReader::read('migration');

        return $path . $generatorPath->getPath() . '/' . $this->getFileName() . '.php';
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return date('Y_m_d_His_') . $this->getSchemaName();
    }

    /**
     * @return array|string
     */
    private function getSchemaName()
    {
        return $this->argument('name');
    }

    /**
     * @return string
     */
    private function getClassName()
    {
        return Str::studly($this->argument('name'));
    }

    public function getClass()
    {
        return $this->getClassName();
    }

    /**
     * Run the command.
     */
    public function handle()
    {
        parent::handle();

        if (!app()->environment('production')) {
            return;
        }
    }

    private function askForFields()
    {
        $result = '';
        $info = [];
        $i = 0;
        $this->comment('fields for ' . $this->argument('name'));
        $do = $this->confirm('Do you need fields?');

        while ($do) {
            $do = false;

            ++$i;

            $info[$i]['table'] = (new NameParser($this->argument('name')))->getTableName();
            $info[$i]['name'] = $this->ask('Write field name (required)');

            while (!$info[$i]['name']) {
                $info[$i]['name'] = $this->ask('Write field name (required)');
            }

            $info[$i]['type'] = $this->anticipate('Write field type (required)', $this->getFieldTypes());

            while (!$info[$i]['type']) {
                $info[$i]['type'] = $this->ask('Write field type (required)');
            }

            $info[$i]['default'] = $this->ask('Write default value if needed');
            $info[$i]['unique'] = $this->confirm('Is it unique?');
            $info[$i]['nullable'] = $this->confirm('Is it nullable?');
            $info[$i]['unsigned'] = $this->isUnsigneble($info[$i]['type']) && $this->confirm('Is it unsigned?');

            $result .= "\n" . '$table->' . $info[$i]['type'] . '("' . $info[$i]['name'] . '")';

            if ($info[$i]['default']) {
                $result .= '->default("' . $info[$i]['default'] . '")';
            }

            if ($info[$i]['unique']) {
                $result .= '->unique()';
            }

            if ($info[$i]['nullable']) {
                $result .= '->nullable()';
            }

            if ($info[$i]['unsigned']) {
                $result .= '->unsigned()';
            }

            $result .= ';';
            $do = $this->confirm('Add another field?');
        }

        return compact('result', 'info');
    }

    private function getFieldTypes()
    {
        return [
            0 => 'bigIncrements',
            1 => 'bigInteger',
            2 => 'binary',
            3 => 'boolean',
            4 => 'char',
            5 => 'date',
            6 => 'dateTime',
            7 => 'dateTimeTz',
            8 => 'decimal',
            9 => 'double',
            10 => 'enum',
            11 => 'float',
            12 => 'geometry',
            13 => 'geometryCollection',
            14 => 'increments',
            15 => 'integer',
            16 => 'ipAddress',
            17 => 'json',
            18 => 'jsonb',
            19 => 'lineString',
            20 => 'longText',
            21 => 'macAddress',
            22 => 'mediumIncrements',
            23 => 'mediumInteger',
            24 => 'mediumText',
            25 => 'morphs',
            26 => 'multiLineString',
            27 => 'multiPoint',
            28 => 'multiPolygon',
            29 => 'nullableMorphs',
            30 => 'nullableTimestamps',
            31 => 'point',
            32 => 'polygon',
            33 => 'rememberToken',
            34 => 'smallIncrements',
            35 => 'smallInteger',
            36 => 'softDeletes',
            37 => 'softDeletesTz',
            38 => 'string',
            39 => 'text',
            40 => 'time',
            41 => 'timeTz',
            42 => 'timestamp',
            43 => 'timestampTz',
            44 => 'timestamps',
            45 => 'timestampsTz',
            46 => 'tinyIncrements',
            47 => 'tinyInteger',
            48 => 'unsignedBigInteger',
            49 => 'unsignedDecimal',
            50 => 'unsignedInteger',
            51 => 'unsignedMediumInteger',
            52 => 'unsignedSmallInteger',
            53 => 'unsignedTinyInteger',
            54 => 'uuid',
            55 => 'year',
        ];
    }

    private function isUnsigneble($type)
    {
        return in_array($type, ['bigInteger', 'decimal', 'integer', 'mediumInteger', 'smallInteger', 'tinyInteger']);
    }
}
