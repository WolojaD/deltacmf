<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;

class ModuleGenerateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:module:generate {module_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate module with all needed';

    /**
     * model name when needed.
     *
     * @var string
     */
    protected $model = '';

    /**
     * table name from model and module.
     *
     * @var string
     */
    protected $table_name = '';

    /**
     * module name.
     *
     * @var string
     */
    protected $module_name = '';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->module_name = $this->argument('module_name');

        $this->call('application:module:make', ['name' => [$this->module_name]]);

        $folder = base_path('modules/' . $this->module_name . '/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }

        $this->call('application:module:make-controller', ['controller' => 'Admin\\' . $this->module_name . 'Controller', '--admin' => true, 'module' => $this->module_name]);

        foreach ($models as $model_name => $model) {
            $this->model_name = studly_case($model_name);
            $this->model = $model;

            $this->call('application:module:make-controller', ['controller' => 'Api\\' . $this->model_name . 'Controller', '--api' => true, 'module' => $this->module_name]);

            $this->call('application:module:make-repository', ['repository' => $this->model_name . 'Interface', 'module' => $this->module_name]);
            $this->call('application:module:make-repository', ['repository' => 'Eloquent\\Eloquent' . $this->model_name . 'Repository', '--eloquent' => true, 'module' => $this->module_name]);
            $this->call('application:module:make-repository', ['repository' => 'Cache\\Cache' . $this->model_name . 'Decorator', '--cache' => true, 'module' => $this->module_name]);
            $folder = base_path('modules/' . $this->module_name . '/Database/Migrations/');

            if (count(scandir($folder)) == 2) {
                $this->call('application:module:make-migration', ['name' => 'create_' . strtolower($this->module_name) . '_table', 'module' => $this->module_name]);
            }

            $this->call('application:module:make-model', [
                'model' => $this->model_name,
                'module' => $this->module_name,
                'table' => $this->model->table_name,
            ]);

            $this->translations();

            $this->call('application:module:make-request', ['name' => 'Create' . $this->model_name . 'Request', 'module' => $this->module_name]);
            $this->call('application:module:make-request', ['name' => 'Update' . $this->model_name . 'Request', 'module' => $this->module_name, '--update' => true]);

            $this->call('module:make-resource', ['name' => 'Full' . $this->model_name . 'Transformer', 'module' => $this->module_name]);
            $this->call('module:make-resource', ['name' => $this->model_name . 'Transformer', 'module' => $this->module_name, '--collection' => true]);
        }
    }

    private function translations()
    {
        if (isset($this->model->need_translations) && $this->model->need_translations) {
            $this->call('application:module:make-model', [
                'model' => $this->model_name . 'Translation',
                'module' => $this->module_name,
                'table' => Str::singular($this->model->table_name) . '_translations',
                '--translation' => true
            ]);
        }
    }
}
