<?php

namespace Modules\Core\Console\Commands;

use Nwidart\Modules\Support\Stub;
use Nwidart\Modules\Commands\GeneratorCommand;
use Nwidart\Modules\Traits\ModuleCommandTrait;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Nwidart\Modules\Support\Config\GenerateConfigReader;

class ControllerMakeCommand extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The name of argument being used.
     *
     * @var string
     */
    protected $argumentName = 'controller';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'application:module:make-controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new restful controller for the specified module.';

    /**
     * Get controller name.
     *
     * @return string
     */
    public function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $controllerPath = GenerateConfigReader::read('controller');

        return $path . $controllerPath->getPath() . '/' . $this->getPlainControllerName() . '.php';
    }

    /**
     * @return string
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());
        $controller = $this->getControllerName();
        $position = strpos($controller, 'Controller');

        $model_name = false !== $position ? substr_replace($controller, '', $position, strlen('Controller')) : $controller;
        $lower_model_name = strtolower($model_name);

        return (new Stub($this->getStubName(), [
            'MODULENAME' => $module->getStudlyName(),
            'LOWER_MODULENAME' => $module->getLowerName(),
            'CONTROLLERNAME' => $this->getControllerName(),
            'NAMESPACE' => $module->getStudlyName(),
            'CLASS_NAMESPACE' => $this->getClassNamespace($module),
            'CLASS' => $this->getControllerName(),
            'LOWER_NAME' => $module->getLowerName(),
            'MODULE' => $this->getModuleName(),
            'MODEL_NAME' => $model_name,
            'LOWER_MODEL_NAME' => $lower_model_name,
            'NAME' => $this->getModuleName(),
            'STUDLY_NAME' => $module->getStudlyName(),
            'MODULE_NAMESPACE' => $this->laravel['modules']->config('namespace'),
        ]))->render();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['controller', InputArgument::REQUIRED, 'The name of the controller class.'],
            ['module', InputArgument::OPTIONAL, 'The name of module will be used.'],
        ];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['plain', 'p', InputOption::VALUE_NONE, 'Generate a plain controller', null],
            ['api', 'i', InputOption::VALUE_NONE, 'Generate api controller', null],
            ['admin', 'd', InputOption::VALUE_NONE, 'Generate admin controller', null],
            ['nested', null, InputOption::VALUE_OPTIONAL, 'Nested model in controller', null],
        ];
    }

    /**
     * @return array|string
     */
    protected function getControllerName()
    {
        $controller = studly_case($this->argument('controller'));
        $controller = explode('\\', $controller);
        $controller = array_pop($controller);

        if (false === str_contains(strtolower($controller), 'controller')) {
            $controller .= 'Controller';
        }

        return $controller;
    }

    /**
     * @return array|string
     */
    protected function getPlainControllerName()
    {
        $controller = studly_case($this->argument('controller'));

        if (false === str_contains(strtolower($controller), 'controller')) {
            $controller .= 'Controller';
        }

        return $controller;
    }

    public function getDefaultNamespace(): string
    {
        return $this->laravel['modules']->config('paths.generator.controller.path', 'Http/Controllers');
    }

    /**
     * Get the stub file name based on the plain option.
     *
     * @return string
     */
    private function getStubName()
    {
        if (true === $this->option('plain')) {
            return '/controller-plain.stub';
        }

        if (true === $this->option('api')) {
            return $this->option('nested') ? '/controller-nested-api.stub' : '/controller-api.stub';
        }

        if (true === $this->option('admin')) {
            return '/controller-admin.stub';
        }

        return '/controller.stub';
    }

    private function getModelName()
    {
        $controller = $this->getControllerName();
        $position = strpos('Controller', $controller);

        if (false !== $position) {
            return substr_replace('Controller', '', $position, strlen($controller));
        }

        return '';
    }

    private function getModelLowerName()
    {
        $controller = $this->getControllerName();
        $position = strpos('Controller', $controller);

        if (false !== $position) {
            return strtolower(substr_replace('Controller', '', $position, strlen($controller)));
        }

        return '';
    }
}
