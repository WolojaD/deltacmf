<?php

namespace Modules\Core\Console\Commands;

use Nwidart\Modules\Support\Stub;
use Nwidart\Modules\Commands\GeneratorCommand;
use Nwidart\Modules\Traits\ModuleCommandTrait;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Nwidart\Modules\Support\Config\GenerateConfigReader;

class RepositoryMakeCommand extends GeneratorCommand
{
    use ModuleCommandTrait;

    /**
     * The name of argument being used.
     *
     * @var string
     */
    protected $argumentName = 'repository';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'application:module:make-repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new restful repository for the specified module.';

    /**
     * Get repository name.
     *
     * @return string
     */
    public function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $repositoryPath = GenerateConfigReader::read('repository');

        return $path . $repositoryPath->getPath() . '/' . $this->getPlainRepositoryName() . '.php';
    }

    /**
     * @return string
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());
        $repository = $this->getRepositoryName();

        $model_name = str_replace('Repository', '', $repository);
        $model_name = str_replace('Decorator', '', $model_name);
        $model_name = str_replace('Eloquent', '', $model_name);
        $model_name = str_replace('Cache', '', $model_name);

        $lower_model_name = strtolower($model_name);

        return (new Stub($this->getStubName(), [
            'MODULENAME' => $module->getStudlyName(),
            'CONTROLLERNAME' => $this->getRepositoryName(),
            'NAMESPACE' => $module->getStudlyName(),
            'CLASS_NAMESPACE' => $this->getClassNamespace($module),
            'CLASS' => $this->getRepositoryName(),
            'LOWER_NAME' => $module->getLowerName(),
            'MODULE' => $this->getModuleName(),
            'MODEL_NAME' => $model_name,
            'LOWER_MODEL_NAME' => $lower_model_name,
            'NAME' => $this->getModuleName(),
            'STUDLY_NAME' => $module->getStudlyName(),
            'MODULE_NAMESPACE' => $this->laravel['modules']->config('namespace'),
        ]))->render();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['repository', InputArgument::REQUIRED, 'The name of the repository class.'],
            ['module', InputArgument::OPTIONAL, 'The name of module will be used.'],
        ];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['plain', 'p', InputOption::VALUE_NONE, 'Generate a plain repository', null],
            ['eloquent', 'e', InputOption::VALUE_NONE, 'Generate eloquent repository', null],
            ['cache', 'c', InputOption::VALUE_NONE, 'Generate cache repository', null],
        ];
    }

    /**
     * @return array|string
     */
    protected function getRepositoryName()
    {
        $repository = studly_case($this->argument('repository'));
        $repository = explode('\\', $repository);
        $repository = array_pop($repository);

        return $repository;
    }

    /**
     * @return array|string
     */
    protected function getPlainRepositoryName()
    {
        $repository = studly_case($this->argument('repository'));

        return $repository;
    }

    public function getDefaultNamespace(): string
    {
        return $this->laravel['modules']->config('paths.generator.repository.path', 'Http/Repositorys');
    }

    /**
     * Get the stub file name based on the plain option.
     *
     * @return string
     */
    private function getStubName()
    {
        if (true === $this->option('plain')) {
            return '/repository-plain.stub';
        }

        if (true === $this->option('eloquent')) {
            return '/repository-eloquent.stub';
        }

        if (true === $this->option('cache')) {
            return '/repository-cache.stub';
        }

        return '/repository.stub';
    }
}
