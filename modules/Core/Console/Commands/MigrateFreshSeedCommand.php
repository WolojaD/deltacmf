<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;

class MigrateFreshSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:migrate:fresh-seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate all modules databases with parameter fresh --seed';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('application:migrate:fresh');
        $this->call('module:seed');
    }
}
