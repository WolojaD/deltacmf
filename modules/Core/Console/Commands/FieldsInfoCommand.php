<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class FieldsInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:fields-info {tablename} {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate module with all needed';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['tablename', InputArgument::REQUIRED, 'The name of the seeder class.'],
            ['module', InputArgument::REQUIRED, 'The name of module will be used.'],
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tables = \DB::connection()->getDoctrineSchemaManager()->listTableNames();
        $table = $this->argument('tablename');
        $module = $this->argument('module');

        while (!in_array($table, $tables)) {
            $table = $this->ask('Write existed table');
        }

        while (!$this->laravel['modules']->has($module)) {
            $module = $this->ask('Write existed module');
        }

        $this->call('application:module:make-seed', [
            'name' => $table . 'Fields',
            '--data' => $this->generateFieldsInfo($table),
            'module' => $module,
        ]);

        if (in_array(Str::singular($table) . '_translations', $tables)) {
            $this->call('application:module:make-seed', [
                'name' => Str::singular($table) . '_translationsFields',
                '--data' => $this->generateFieldsInfo(Str::singular($table) . '_translations'),
                'module' => $module,
            ]);
        }
    }

    protected function generateFieldsInfo($table)
    {
        if ('migrations' == $table || 'fields' == $table) {
            return;
        }

        $columns = Schema::getColumnListing($table);
        $result = [];

        foreach ($columns as $key => $column) {
            $result[$key]['table'] = $table;
            $result[$key]['name'] = $column;
            $result[$key]['nullable'] = true;
            $result[$key]['type'] = Schema::getColumnType($table, $column);
        }

        return $result;
    }
}
