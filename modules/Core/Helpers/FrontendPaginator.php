<?php

namespace Modules\Core\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Pagination\LengthAwarePaginator;

class FrontendPaginator extends LengthAwarePaginator
{
    /**
     * The default pagination view.
     *
     * @var string
     */
    public static $defaultView = 'pagination.default';

    /**
     * The default "simple" pagination view.
     *
     * @var string
     */
    public static $defaultSimpleView = 'pagination.simple-default';

    /**
      * Get the URL for a given page number.
      *
      * @param  int  $page
      *
      * @return string
      */
    public function url($page)
    {
        if ($page <= 0) {
            $page = 1;
        }

        // If we have any extra query string key / value pairs that need to be added
        // onto the URL, we will put them in query string form and then attach it
        // to the URL. This allows for extra information like sortings storage.
        $parameters = [];

        if (count($this->query) > 0) {
            $parameters = array_merge($this->query, $parameters);
        }

        $path = $this->path;

        if ($page > 1) {
            $path = preg_replace('/' . $this->pageName . '-(\d)$/', $this->pageName . '-' . $page, $path);

            $path = strpos($path, '/' . $this->pageName . '-' . $page) !== false ? $path : str_finish($path, '/') . $this->pageName . '-' . $page;
        } else {
            $path = preg_replace('/' . $this->pageName . '-(\d)$/', '', $path);
        }

        return $path . (count($parameters) ? '?' : '')
                    . Arr::query($parameters)
                    . $this->buildFragment();
    }

    public function paginates()
    {
        return [
            'next' => $this->hasMorePages() ? $this->nextPageUrl() : false,
            'prev' => !$this->onFirstPage() ? $this->previousPageUrl() : false,
        ];
    }

    /**
     * Resolve the current page or return the default value.
     *
     * @param  string  $pageName
     * @param  int  $default
     *
     * @return int
     */
    public static function resolveCurrentPage($pageName = 'page', $default = 1)
    {
        if (isset(static::$currentPageResolver)) {
            return call_user_func(static::$currentPageResolver, $pageName);
        }

        return $default;
    }

    /**
     * @param $perPage
     * @param $columns
     * @param $pageName
     * @param $page
     *
     * @return array
     */
    public static function parseRoute($perPage, $columns, $pageName, $page, $builder): array
    {
        self::currentPageResolver(function ($pageName = 'page') {
            preg_match('/\/' . $pageName . '-(\d+)$/', request()->path(), $page);

            if (!isset($page[1])) {
                return 1;
            }

            if (filter_var($page[1], FILTER_VALIDATE_INT) !== false && (int)$page[1] >= 1) {
                return (int)$page[1];
            }

            abort(404);
        });

        $page = $page ?: self::resolveCurrentPage($pageName);

        $perPage = $perPage ?: $builder->model->getPerPage();

        $results = ($total = $builder->toBase()->getCountForPagination())
            ? $builder->forPage($page, $perPage)->get($columns)
            : $builder->getModel()->newCollection();

        if ($total > 0 && $total < $page) {
            abort(404);
        }

        return [$page, $perPage, $total, $results];
    }
}
