<?php

namespace Modules\Core\Helpers;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class BackendPaginator extends LengthAwarePaginator
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'current_page' => $this->currentPage(),
            'data' => $this->items->toArray(),
            'first_page_url' => $this->url(1),
            'from' => $this->firstItem(),
            'last_page' => $this->lastPage(),
            'last_page_url' => $this->url($this->lastPage()),
            'next_page_url' => $this->nextPageUrl(),
            'path' => $this->path,
            'per_page' => $this->perPage(),
            'prev_page_url' => $this->previousPageUrl(),
            'to' => $this->lastItem(),
            'total' => $this->total(),
            'fields' => $this->fields ?? [],
            'sortOrder' => $this->sortOrder ?? [],
            'backend_path_name' => $this->backend_path_name ?? '',
            'has_tables' => $this->has_tables ?? [],
            'nested' => $this->nested ?? false,
            'buttons' => $this->buttons ?? false,
            'title' => $this->title ?? false,
            'no_create' => $this->no_create ?? false,
            'breadcrumbs' => $this->breadcrumbs ?? false,
            'additions' => $this->additions ?? false
        ];
    }

    /**
     * Add some data to additions
     *
     * @param mixed $additions
     *
     * @return BackendPaginator
     */
    public function addAdditions($additions)
    {
        $this->additions = $additions;

        return $this;
    }

    /**
     * Cange any data of any paginator item
     *
     * @param string $key
     * @param mixed $data
     *
     * @return BackendPaginator
     */
    public function changeDataByKey($key, $data)
    {
        $this->$key = $data;

        return $this;
    }
}
