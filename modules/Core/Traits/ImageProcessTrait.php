<?php

namespace Modules\Core\Traits;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\File;
use Modules\Settings\Entities\Image;

trait ImageProcessTrait
{
    /**
     * @return type
     */
    public static function bootImageProcessTrait()
    {
        static::deleting(function ($entity) {
            $folders = Image::select('title', 'id')->get();

            foreach ($folders as $folder) {
                $pre_path = 'storage/' . $folder->rootPath . '/' . $folder->title . '/';

                if ($entity->desktop_image) {
                    File::delete(public_path($pre_path . ltrim($entity->desktop_image, '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('desktop_image')));
                }

                if ($entity->mobile_image) {
                    File::delete(public_path($pre_path . ltrim($entity->mobile_image, '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('mobile_image')));
                }

                if ($entity->translatable_desktop_image) {
                    File::delete(public_path($pre_path . ltrim($entity->getOriginal('translatable_desktop_image'), '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('translatable_desktop_image')));
                }

                if ($entity->translatable_mobile_image) {
                    File::delete(public_path($pre_path . ltrim($entity->translatable_mobile_image, '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('translatable_mobile_image')));
                }

                if ($entity->preview_image) {
                    File::delete(public_path($pre_path . ltrim($entity->preview_image, '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('preview_image')));
                }

                if ($entity->og_image) {
                    File::delete(public_path($pre_path . ltrim($entity->og_image, '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('og_image')));
                }

                if ($entity->image) {
                    File::delete(public_path($pre_path . ltrim($entity->image, '/origin')));
                    File::delete(public_path('storage/origin' . $entity->getOriginal('image')));
                }
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getAdaptiveImageAttribute()
    {
        if ((new Agent())->isMobile()) {
            return $this->mobile_image ?? $this->desktop_image;
        }

        return $this->desktop_image;
    }

    public function getAdaptiveTranslatableImageAttribute()
    {
        if ((new Agent())->isMobile()) {
            return $this->translatable_mobile_image ?? $this->translatable_desktop_image;
        }

        return $this->translatable_desktop_image;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setDesktopImageAttribute($value)
    {
        $this->processImageFile($value, 'desktop_image', 'origin');
    }

    public function setMobileImageAttribute($value)
    {
        $this->processImageFile($value, 'mobile_image', 'origin');
    }

    public function setTranslatableDesktopImageAttribute($value)
    {
        $this->processImageFile($value, 'translatable_desktop_image', 'origin');
    }

    public function setTranslatableMobileImageAttribute($value)
    {
        $this->processImageFile($value, 'translatable_mobile_image', 'origin');
    }

    public function setPreviewImageAttribute($value)
    {
        $this->processImageFile($value, 'preview_image', 'origin');
    }

    public function setOgImageAttribute($value)
    {
        $this->processImageFile($value, 'og_image', 'origin');
    }

    public function setImageAttribute($value)
    {
        $this->processImageFile($value, 'image', 'origin');
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Upload image to server, save it by parametes and type and return path of file
     *
     * @param array $value
     * @param string $attribute_name
     * @param string $folder
     *
     * @return string
     */
    public function processImageFile($value, $attribute_name, $folder = 'origin')
    {
        if (is_array($value) && $value['coordinates']) {
            $file = request()->{'image_' . $attribute_name};

            if (!$file) {
                if (isset($value['cropped'])) {
                    $file = $cropped;
                } else {
                    $file = file_get_contents(public_path('storage/origin' . $this->getOriginal($attribute_name)));
                }
            }

            $this->uploadBase64Image($file, $attribute_name, 'origin', $value['coordinates']);
        } elseif (starts_with($value, 'data:image')) {
            $this->uploadBase64Image($value, $attribute_name, 'origin');
        } else {
            $this->uploadFile($value, $attribute_name, $folder);
        }
    }

    /**
     * Upload Base64 Image file
     *
     * @param [type]  $value
     * @param stirng  $attribute_name
     * @param string  $folder
     * @param boolean $coordinates
     *
     * @return string
     */
    protected function uploadBase64Image($value, $attribute_name, $folder = 'origin', $coordinates = false)
    {
        if (is_null($value) && $this->{$attribute_name} != null) {
            \Storage::disk('public')->delete('/' . $folder . $this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // TODO what to delete?
        \Storage::disk('public')->delete('/' . $folder . $this->{$attribute_name});
        $this->attributes[$attribute_name] = null;

        $image = \Intervention\Image\ImageManagerStatic::make($value)->interlace();

        if ($coordinates) {
            $image = $image->crop(
                (int) $coordinates['width'],
                (int) $coordinates['height'],
                (int) $coordinates['x'],
                (int) $coordinates['y']
            );
        }

        $filename = md5($value . time());

        switch ($image->mime()) {
            case 'image/x-icon':
            case 'image/png':
                $extension = 'png';
                break;
            default:
                $extension = 'jpg';
                break;
        }

        $new_file_name = $filename . '.' . $extension;

        $entity = self::latest()->first();
        $id = $this->id ?? ($entity ? $entity->id + 1 : 1);

        $folder = $folder ? $folder . '/' : '';
        $path = $folder . $this->table . '/' . $id;

        \Storage::disk('public')->put($path . '/' . $new_file_name, $image->stream($extension, 90));
        $this->attributes[$attribute_name] = '/' . $this->table . '/' . $id . '/' . $new_file_name;
    }
}
