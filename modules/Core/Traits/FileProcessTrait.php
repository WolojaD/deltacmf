<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\File;

trait FileProcessTrait
{
    /**
     * @return type
     */
    public static function bootFileProcessTrait()
    {
        static::deleting(function ($entity) {
            if ($entity->file) {
                // File::delete(public_path('storage' . $entity->getOriginal('file')));
                File::delete(public_path('storage/origin' . $entity->getOriginal('file')));
            }

            if ($entity->video) {
                // File::delete(public_path('storage' . $entity->getOriginal('video')));
                File::delete(public_path('storage/origin' . $entity->getOriginal('video')));
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setFileAttribute($value)
    {
        $this->uploadFile($value, 'file', 'origin');
    }

    public function setVideoAttribute($value)
    {
        if (gettype($value) == 'object') {
            $this->uploadFile($value, 'video', 'origin');
        } elseif (gettype($value) == 'string') {
            $this->attributes['video'] = $value;
        }
    }
}
