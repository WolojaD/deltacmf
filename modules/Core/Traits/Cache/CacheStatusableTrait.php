<?php

namespace Modules\Core\Traits\Cache;

trait CacheStatusableTrait
{
    /**
     * Setting status for all selected ids.
     *
     * @param array $selected_ids
     * @param int   $status
     *
     * @return \Illuminate\Support\Collection
     */
    public function markSelectedAsStatus(array $selected_ids, $status)
    {
        return $this->repository->markSelectedAsStatus($selected_ids, $status);
    }

    /**
     * Get the available statuses.
     *
     * @return array
     */
    public function statusList()
    {
        return $this->repository->statusList();
    }

    public function getFormatedStatusAttribute($status)
    {
        return $this->repository->getFormatedStatusAttribute($status);
    }
}
