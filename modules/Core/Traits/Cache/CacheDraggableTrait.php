<?php

namespace Modules\Core\Traits\Cache;

trait CacheDraggableTrait
{
    public function changeOrder($request)
    {
        return $this->repository->changeOrder($request);
    }
}
