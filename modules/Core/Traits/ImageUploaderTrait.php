<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * Image Upload Trait.
 *
 * Example how to use:
 *      Inside model -
 *          public function set{name_of_attribute}Attribute($value)
 *          {
 *              $this->uploadImageToDisk($value, '{name_of_attribute}');
 *          }
 *
 *          This will prossed attribute with default parameters such as: only one image with origin resolution
 *
 *      Additional parameters -
 *          protected $image_properties = [
 *              '{name_of_attribute}' => [
 *                  'resolutions' => 'auto',
 *              ]
 *          ];
 *
 *          This will prossed image with $default_resolutions parameters below
 */
trait ImageUploaderTrait
{
    protected $extension;
    protected $attribute_name;
    protected $params;
    protected $default_resolutions = [
        // '10p' => ['width' => 15],
        // '480p' => ['width' => 858],
        // '720p' => ['width' => 1280],
        // '1080p' => ['width' => 1920],
        'origin' => null,
    ];

    // /**
    //  * @return type
    //  */
    // public static function bootImageUploadTrait()
    // {
    //     static::deleting(function ($obj) {
    //         foreach ($obj->image_properties as $image_type => $property) {
    //             $extension = $property->extension ?? 'jpg';

    //             if (false !== strpos($obj->{$image_type}, '.' . $extension)) {
    //                 //     if ($property['resolutions'] == 'auto') {
    //                 //         foreach ($obj->default_resolutions as $resolution_type => $value) {
    //                 //             Storage::disk($property['disk'] ?? 'public')->delete('/' . $resolution_type . '/' . $obj->{$image_type});
    //                 //         }
    //                 //     } else {
    //                 //         foreach ($property['resolutions'] as $resolution_type => $value) {
    //                 //             Storage::disk($property['disk'] ?? 'public')->delete('/' . $resolution_type . '/' . $obj->{$image_type});
    //                 //         }
    //                 //     }

    //                 Storage::disk($property['disk'] ?? 'public')->delete('/origin/' . $obj->{$image_type});
    //             }
    //         }
    //     });
    // }

    /**
     * Get image destination path.
     *
     * @param type        $attribute_name
     * @param type|string $folder
     *
     * @return type
     */
    public function getImageDestination($attribute_name, $folder = 'origin')
    {
        $disk = $this->image_properties[$attribute_name]['disk'] ?? 'public';

        return '/' . $disk . '/' . $folder . $this->{$attribute_name};
    }

    /**
     * Get original image.
     *
     * @param type $attribute_name
     *
     * @return type
     */
    public function getOriginImage($attribute_name)
    {
        return $this->getImageDestination($attribute_name);
    }

    /**
     * Method for uploading image to destionation disk.
     *
     * @param type $value
     * @param type $attribute_name
     *
     * @return type
     */
    public function uploadImageToDisk($value, $attribute_name)
    {
        $this->createParams($value, $attribute_name);

        $this->deleteImage($value);

        if ($this->imageUploaded($value)) {
            return $this->processImage();
        }
    }

    /**
     * Create disk and destitation params for image path.
     *
     * @param type $value
     * @param type $attribute_name
     *
     * @return type
     */
    protected function createParams($value, $attribute_name)
    {
        $previos_model = self::latest()->first();

        $this->attribute_name = $attribute_name;
        $this->params = $this->image_properties[$attribute_name];
        $this->params['value'] = $value;

        $entity = self::latest()->first();

        $this->params['id'] = $this->id ?? ($entity ? $entity->id + 1 : 1);
        $this->params['disk'] = isset($this->params['disk']) ? $this->params['disk'] : 'public';

        $this->params['destination_path'] = isset($this->params['destination_path']) ?
                                                $this->params['destination_path'] :
                                                $this->table;
        $this->params['destination_path'] .= '/' . $this->params['id'];

        $this->extension = $this->params['extension'] ?? 'jpg';
    }

    /**
     * Delete current image.
     *
     * @return type
     */
    protected function deleteImage($value)
    {
        if (!$this->imageDeleted() and !$this->imageUploaded($value)) {
            return false;
        }

        $disk = $property['disk'] ?? 'public';
        $property = $this->image_properties[$this->attribute_name];
        $extension = $property->extension ?? 'jpg';

        if (false !== strpos($this->{$this->attribute_name}, '.' . $extension) && isset($property['resolutions'])) {
            if ('auto' == $property['resolutions']) {
                $property['resolutions'] = $this->default_resolutions;
            }

            foreach ($property['resolutions'] as $resolution_type => $value) {
                Storage::disk($disk)->delete('/' . $resolution_type . '/' . $this->{$this->attribute_name});
            }
        } elseif (false !== strpos($this->{$this->attribute_name}, '.' . $extension)) {
            Storage::disk($disk)->delete('/origin/' . $this->{$this->attribute_name});
        }

        $this->attributes[$this->attribute_name] = null;
    }

    /**
     * @return type
     */
    protected function imageUploaded($value)
    {
        return (request()->hasFile($this->attribute_name) and
            request()->file($this->attribute_name)->isValid()) ||
            starts_with($this->params['value'], 'data:image') ||
            (is_object($value) && 'Illuminate\Http\UploadedFile' === get_class($value) && $value->isValid());
    }

    /**
     * Generate filename and resize, if needed.
     *
     * @return type
     */
    protected function processImage()
    {
        $this->params['filename'] = md5($this->params['value'] . time()) . '.' . $this->extension;

        $this->makeResolutions();

        return $this->attributes[$this->attribute_name] = '/' . $this->params['destination_path'] . '/' . $this->params['filename'];
    }

    protected function makeResolutions()
    {
        $this->params['quality'] = $this->params['quality'] ?? 80;

        $this->params['folder'] = 'origin';
        $this->params['resolution'] = null;

        $this->saveImage();
    }

    /**
     * Save image to the given filepath.
     *
     * @return type
     */
    protected function saveImage()
    {
        $this->params['path'] = config('filesystems.disks.' . ($this->params['disk']) . '.root') . '/' . $this->params['folder'] . '/' . $this->params['destination_path'];

        $this->createDirectory($this->params['path']);

        $this->createImage()
            ->encode($this->extension)
            ->save($this->params['path'] . '/' . $this->params['filename'], $this->params['quality']);
    }

    /**
     * Create new directory for save image if not exists.
     *
     * @param type $path
     *
     * @return type
     */
    protected function createDirectory($path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    }

    /**
     * Make image file from given data.
     *
     * @return type
     */
    protected function createImage()
    {
        $image = Image::make($this->params['value'])->interlace();

        // $image = $this->addWatermark($image);

        return $this->longestSide($image);
    }

    /**
     * Get image aspect ratio if set, or make it auto.
     *
     * @param type      $image
     * @param type|bool $ratio
     *
     * @return type
     */
    protected function longestSide($image, $ratio = false)
    {
        $resolution['width'] = $this->params['resolution']['width'] ?? 0;
        $resolution['height'] = $this->params['resolution']['height'] ?? 0;
        $resolution['width'] = $ratio ? intval($ratio * $resolution['width']) : $resolution['width'];
        $resolution['height'] = $ratio ? intval($ratio * $resolution['height']) : $resolution['height'];

        if ($resolution['width'] > 0 && $resolution['height'] > 0) {
            return $this->autoResolution($image, $resolution);
        } elseif ($resolution['width']) {
            return $image->resize($resolution['width'], null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } elseif ($resolution['height']) {
            return $image->resize(null, $resolution['height'], function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        return $image;
    }

    /**
     * Auto resize image.
     *
     * @param type $image
     * @param type $resolution
     *
     * @return type
     */
    protected function autoResolution($image, $resolution)
    {
        return $image->width() > $resolution['width'] ?
            $image->resize($resolution['width'], null, function ($constraint) {
                $constraint->aspectRatio();
            }) :
            $image->resize(null, $resolution['height'], function ($constraint) {
                $constraint->aspectRatio();
            });
    }

    /**
     * @return type
     */
    protected function imageDeleted()
    {
        return (request()->hasFile($this->attribute_name) and
            $this->{$this->attribute_name} and
            null != $this->{$this->attribute_name}) || (is_null($this->params['value']) && null != $this->{$this->attribute_name});
    }

    // protected function addWatermark($image)
    // {
    //     if ($this->params['folder'] != 'main' && isset($this->params['watermark'])) {
    //         $watermark = Image::make(public_path('watermark/watermark.png'));
    //         $watermark = $this->longestSide($watermark, .20);

    //         return $image->insert($watermark, 'bottom-right');
    //     }

    //     return $image;
    // }

    public function resizedImage($attribute_name)
    {
        $path = $this->attributes[$attribute_name];

        if ($path && is_file(public_path('storage/origin' . $path))) {
            return is_file(public_path('storage/i' . $path)) ? '/storage/i' . $path : $this->makeMiniImage($path, $attribute_name);
        }

        return $path;
    }

    /**
     * Make mini image.
     *
     * @return type
     */
    protected function makeMiniImage($path, $attribute_name)
    {
        $this->params['disk'] = 'public';

        $path_items = explode('/', trim($path, '/'));

        $this->params['filename'] = array_pop($path_items);
        $this->params['destination_path'] = implode('/', $path_items);
        $this->params['folder'] = 'i';

        $this->params['resolution'] = $this->{$attribute_name . '_size'} ?? ['width' => 200];
        $this->params['value'] = public_path('storage/origin' . $path);
        $this->params['quality'] = 100;

        $this->saveImage();

        return '/storage/i' . $path;
    }
}
