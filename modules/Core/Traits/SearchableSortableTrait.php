<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\Schema;

trait SearchableSortableTrait
{
    /**
     * @return type
     */
    protected static function bootSearchableSortableTrait()
    {
        static::creating(function ($entity) {
            if (!Schema::hasColumn($entity->getTable(), 'order')) {
                return;
            }

            $groupOrder = $entity->groupOrder ?? 'parent_id';

            if (Schema::hasColumn($entity->getTable(), $groupOrder)) {
                $order = static::where($groupOrder, $entity->{$groupOrder} ?? 0)->max('order');
            } else {
                $order = static::max('order');
            }

            $entity->order = !is_null($order) ? $order + 1 : 0;
        });

        static::deleting(function ($entity) {
            if (!Schema::hasColumn($entity->getTable(), 'order')) {
                return;
            }

            $groupOrder = $entity->groupOrder ?? 'parent_id';

            if (Schema::hasColumn($entity->getTable(), $groupOrder)) {
                static::where($groupOrder, $entity->{$groupOrder} ?? 0)->where('order', '>', $entity->order)->decrement('order');
            } else {
                static::where('order', '>', $entity->order)->decrement('order');
            }
        });
    }

    /**
     * @param type $query
     * @param type $sort
     * @return type
     */
    public function scopeSort($query, $sort)
    {
        $field_items = explode('.', $sort['field']);
        $table_name = $this->getTable();

        if (1 == count($field_items) && Schema::hasColumn($table_name, $field_items[0])) {
            return $query->orderBy($sort['field'], $sort['order']);
        } elseif ('translation' == $field_items[0]) {
            return $query->orderByTranslation($field_items[1], $sort['order']);
        }
    }

    /**
     * @param type $query
     * @param type $search
     * @return type
     */
    public function scopeSearch($query, $search)
    {
        if (!$search) {
            return $query;
        }

        $query = $query->where($this->getTable() . '.id', $search);
        $translated = $this->translatedAttributes ?? [];

        foreach ($this->searchable as $field) {
            $query = in_array($field, $translated) ?
                    $query->orWhereTranslationLike($field, "%{$search}%") :
                    $query->orWhere($field, 'LIKE', "%{$search}%");
        }

        return $query;
    }
}
