<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\Schema;

trait DraggableTrait
{
    private $dragParams;

    /**
     * Change order by request fields.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function changeOrder($request)
    {
        $this->parseOrderRequest($request);

        if (false === $this->dragParams['old'] || false === $this->dragParams['new'] || $this->dragParams['old'] === $this->dragParams['new']) {
            return response()->json([
                'message' => 'no changes',
                'errors' => true,
                'message' => 'Old or New',
            ]);
        }

        $this->dragParams['entity'] = $this->getEntity();

        if (!$this->dragParams['entity']) {
            return response()->json([
                'message' => 'entity',
                'errors' => true,
                'message' => 'No entity',
            ]);
        }

        $this->processOrder();

        if (!$this->dragParams['entity']->save()) {
            return response()->json([
                'message' => 'didnt save',
                'errors' => true,
                'message' => 'No entity save',
            ]);
        }

        return response()->json([
            'errors' => false,
            'message' => trans('reordered'),
        ]);
    }

    /**
     * Find entity by given data.
     */
    private function getEntity()
    {
        $self = $this;

        return $this->model->where('order', $this->dragParams['old'])
             ->when($this->dragParams['hasGroupOrder'], function ($query) use ($self) {
                 return $query->where($self->dragParams['groupOrder'], $self->dragParams['groupOrderValue']);
             })
             ->first();
    }

    /**
     * Find all items that needs to change order and change them order.
     */
    private function processOrder()
    {
        $this->dragParams['entity']->order = $this->dragParams['new'];
        $reorder = $this->dragParams['new'] < $this->dragParams['old'] ? 'increment' : 'decrement';
        $between = $this->dragParams['new'] < $this->dragParams['old'] ? [$this->dragParams['new'], $this->dragParams['old']] : [$this->dragParams['old'], $this->dragParams['new']];

        $self = $this;

        $this->model
            ->where('id', '!=', $this->dragParams['entity']->id)
            ->whereBetween('order', $between)
            ->when($this->dragParams['hasGroupOrder'], function ($query) use ($self) {
                return $query->where($self->dragParams['groupOrder'], $self->dragParams['entity']->{$self->dragParams['groupOrder']});
            })
            ->$reorder('order');
    }

    /**
     * Parse given data in request.
     *
     * @param \Illuminate\Http\Request $request
     */
    private function parseOrderRequest($request)
    {
        $this->dragParams['old'] = $request->get('oldOrder', false);
        $this->dragParams['new'] = $request->get('newOrder', false);
        $this->dragParams['groupOrder'] = $this->model->groupOrder ?? 'parent_id';
        $this->dragParams['groupOrderValue'] = $request->get($this->dragParams['groupOrder'], $request->get('ident', false));
        $this->dragParams['hasGroupOrder'] = Schema::hasColumn($this->model->getTable(), $this->dragParams['groupOrder']);
    }
}
