<?php

namespace Modules\Core\Traits;

use Symfony\Component\Yaml\Yaml;

trait CanPublishConfigurationTrait
{
    /**
     * Publish the given configuration file name (without extension) and the given module.
     *
     * @param string $module
     * @param string $fileName
     */
    public function publishConfig($module, $fileName)
    {
        if (app()->environment() === 'testing') {
            return;
        }

        if ($this->checkFileExists($module, $fileName)) {
            $this->mergeConfigFrom($this->getModuleConfigFilePath($module, $fileName), strtolower("application.$module.$fileName"));
        } else {
            $this->customMergeConfigFrom(Yaml::parseFile($this->getModuleConfigFilePath($module, $fileName, 'yml')), strtolower("application.$module.$fileName"));
        }

        $this->publishes([
            $this->getModuleConfigFilePath($module, $fileName) => config_path(strtolower("application/$module/$fileName") . '.php'),
        ], 'config');
    }

    /**
     * Check if config file exists in module path.
     *
     * @param string $module
     * @param string $fileName
     *
     * @return type
     */
    protected function checkFileExists(string $module, string $fileName)
    {
        return file_exists($this->getModuleConfigFilePath($module, $fileName));
    }

    protected function customMergeConfigFrom($path, $key)
    {
        $config = $this->app['config']->get($key, []);

        $this->app['config']->set($key, array_merge($path, $config));
    }

    /**
     * Get path of the give file name in the given module.
     *
     * @param string $module
     * @param string $fileName
     *
     * @return string
     */
    private function getModuleConfigFilePath($module, $fileName, $fileExtension = 'php')
    {
        return $this->getModulePath($module) . "/Config/$fileName.$fileExtension";
    }

    /**
     * @param $module
     *
     * @return string
     */
    private function getModulePath($module)
    {
        $moduleFolders = preg_grep('/^([^.])/', scandir(base_path('modules')));
        $moduleFolder = array_search($module, array_map('strtolower', $moduleFolders));

        return base_path('modules' . DIRECTORY_SEPARATOR . $moduleFolders[$moduleFolder]);
    }

    /**
     * Config arrays for CoreServiceProvider->register method.
     *
     * @return type
     */
    protected function registerAvailableLocales()
    {
        $registerConfigArray = [
            'available-locales-backend',
            'available-locales',
            'core',
        ];

        foreach ($registerConfigArray as $fileName) {
            $this->customMergeConfigFrom(Yaml::parseFile($this->getModuleConfigFilePath('core', $fileName, 'yml')), strtolower("application.core.$fileName"));
        }
    }
}
