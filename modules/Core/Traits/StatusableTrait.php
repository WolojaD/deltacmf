<?php

namespace Modules\Core\Traits;

trait StatusableTrait
{
    public $draft = 2;
    public $published = 1;
    public $unpublished = 0;

    /**
     * Setting status for all selected ids.
     *
     * @param array $selected_ids
     * @param int   $status
     *
     * @return \Illuminate\Support\Collection
     */
    public function markSelectedAsStatus(array $selected_ids, $status)
    {
        if (!count($selected_ids)) {
            return collect([]);
        }

        $entities = $this->model->whereIn('id', $selected_ids)->get();
        $status = $status ?? $this->published;

        $entities->each(
            function ($item) use ($status) {
                $item->update(['status' => $status]);
            }
        );

        return $entities->pluck('status', 'id');
    }

    /**
     * Get the available statuses.
     *
     * @return array
     */
    public function statusList()
    {
        $module = strtolower(explode('\\', static::class)[1]);

        return [
            $this->draft => trans($module . '::status.draft'),
            $this->published => trans($module . '::status.published'),
            $this->unpublished => trans($module . '::status.unpublished'),
        ];
    }

    public function getFormatedStatusAttribute($status)
    {
        return $this->statusList()[$status ?? $this->draft];
    }
}
