<?php

namespace Modules\Core\Traits;

trait NestedTrait
{
    /**
     * @return type
     */
    protected static function bootNestedTrait()
    {
        static::creating(function ($entity) {
            if ($entity->parent_id ?? false) {
                $entity->depth = ($entity->parent()->select('depth')->first()->depth ?? 0) + 1;
            }
        });

        static::updating(function ($entity) {
            if ($entity->parent_id ?? false) {
                $entity->depth = ($entity->parent()->select('depth')->first()->depth ?? 0) + 1;
            }
        });

        static::deleting(function ($entity) {
            $entity->children->each(function ($child) {
                $child->delete();
            });
        });
    }

    /**
     * @return type
     */
    protected function getPrePath()
    {
        $depth = 'pages' == $this->table ? 2 : 1;

        if ($this->depth > $depth && $this->parent) {
            return  $this->parent->getPrePath() . '/' . $this->parent->slug;
        }

        return '';
    }

    public function getBackendBreadcrumbs($carry = [])
    {
        $carry = $this->getChildrenBread($carry, $this);

        //TODO remove this (removing link for current path) and add in js, or keep it here
        unset($carry[0]['link']);

        return array_reverse($carry);
    }

    /**
     * @param $carry
     * @param $parent
     *
     * @return array
     */
    protected function getChildrenBread($carry, $model): array
    {
        $modelConfig = $model->getJsonList();

        if ($model->id) {
            $carry[] = [
                'name' => method_exists($model, 'defaultTranslate') ? ($model->defaultTranslate()->title ?? '-') : ($model->title ?? '-'),
                'link' => [
                    'name' => 'api.backend.' . $modelConfig->backend_path_name . '.show',
                    'params' => [
                        'ident' => $model->id
                    ]
                ]
            ];
        }

        if ($model->parent) {
            $carry = $this->getChildrenBread($carry, $model->parent);
        } elseif ($model->groupOrder && $model->{$model->groupOrder} > 0) {
            $relation = str_replace('_id', '', $model->groupOrder);
            $carry = $this->getChildrenBread($carry, $model->$relation);
        } else {
            $carry[] = [
                'name' => trans('core::breadcrumbs.' . str_replace('_', ' ', $modelConfig->backend_path_name)),
                'link' => [
                    'name' => 'api.backend.' . $modelConfig->backend_path_name,
                ]
            ];
        }

        return $carry;
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id')->with('children', 'translations');
    }

    public function getIsHomeAttribute(): bool
    {
        return 0 === $this->parent_id;
    }

    public function getPathAttribute()
    {
        return $this->deleteWrongSlashes($this->getPrePath()) . '/' . $this->slug;
    }

    protected function deleteWrongSlashes(string $string)
    {
        return str_replace('//', '/', $string);
    }

    // public function setParentIdAttribute(int $value)
    // {
    //     $parent = static::where('id', $value)->select('depth')->first();

    //     if ($value && $parent) {
    //         $this->attributes['depth'] = $parent->depth + 1;
    //     }

    //     $this->attributes['parent_id'] = $value;
    // }

    // public function scopeFindByPath($query, $path)
    // {
    //     $slugs = explode('/', $path);

    //     $subselect = 'pages' == static::getTable() ?
    //         'case when (count(`depth`) + 1 = max(`depth`) and min(`depth`) = 2) then max(`depth`) else -1 end' :
    //         'case when (count(`depth`) = max(`depth`)) then max(`depth`) else -1 end';

    //     return $query->where('depth', function ($query) use ($slugs, $subselect) {
    //         return $query->selectRaw($subselect)
    //             ->from(static::getTable())
    //             ->whereIn('slug', $slugs)
    //             ->orderBy('depth');
    //     })
    //     ->whereIn('slug', $slugs);
    // }
}
