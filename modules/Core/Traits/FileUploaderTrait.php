<?php

namespace Modules\Core\Traits;

use Illuminate\Http\UploadedFile;

trait FileUploaderTrait
{
    protected function uploadFile($value, $attribute_name, $folder = false)
    {
        $this->deleteFileForEmptyValue($value, $attribute_name, $folder);

        $this->saveUploadedFile($value, $attribute_name, $folder);
    }

    private function deleteFileForEmptyValue($value, $attribute_name, $folder)
    {
        if (is_null($value) && $this->{$attribute_name} != null) {
            $folder = $folder ? $folder . '/' : '';

            \Storage::disk('public')->delete($folder . $this->{$attribute_name});

            $this->attributes[$attribute_name] = null;
        }
    }

    private function saveUploadedFile($value, $attribute_name, $folder, $coordinates = false)
    {
        $isValid = $value instanceof UploadedFile && $value->isValid();

        $folder = $folder ? $folder . '/' : '';

        if ($isValid) {
            \Storage::disk('public')->delete($folder . $this->{$attribute_name});

            $this->attributes[$attribute_name] = null;

            $new_file_name = md5($value->getClientOriginalName() . time()) . '.' . $value->getClientOriginalExtension();

            $entity = self::latest()->first();
            $id = $this->id ?? ($entity ? $entity->id + 1 : 1);

            $path = $folder . $this->table . '/' . $id;
            $value->storeAs($path, $new_file_name, 'public');

            $this->attributes[$attribute_name] = '/' . $this->table . '/' . $id . '/' . $new_file_name;
        } else {
            $this->attributes[$attribute_name] = $value;
        }
    }

    public function uploadTextEditorFile($value, $folder = 'origin')
    {
        $new_file_name = md5($value->getClientOriginalName() . time()) . '.' . $value->getClientOriginalExtension();

        $entity = self::latest()->first();
        $id = $this->model->id ?? ($entity ? $entity->id + 1 : 1);

        $folder = $folder ? $folder . '/' : '';
        $path = $folder . $this->table . '/' . $id . '/text_editor';

        if ($value->storeAs($path, $new_file_name, 'public')) {
            $file_path = '/' . $this->table . '/' . $id . '/text_editor/' . $new_file_name;

            return response()->json([
                'file_path' => '/storage/origin' . $file_path,
                'file_name' => $value->getClientOriginalName(),
            ]);
        }

        return response()->json([
            'errors' => true,
            'filepath' => 'Error!',
            'file_name' => $value->getClientOriginalName(),
        ]);
    }
}
