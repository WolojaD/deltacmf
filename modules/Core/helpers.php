<?php

use Illuminate\Support\Facades\Cache;

if (!function_exists('locale')) {
    function locale($locale = null)
    {
        if (is_null($locale)) {
            return app()->getLocale();
        }

        app()->setLocale($locale);

        return app()->getLocale();
    }
}

if (!function_exists('locale_url')) {
    function locale_url($path)
    {
        $prepend = locale() == request()->segment(1) ? '/' . locale() : '';

        return url($prepend . str_start($path, '/'));
    }
}

if (!function_exists('session_unique_push')) {
    function session_unique_push(string $name, $item)
    {
        $array = session()->pull($name) ?? [];

        $array[] = $item;

        session()->put($name, array_unique($array));
    }
}

if (!function_exists('session_unique_substract')) {
    function session_unique_substract(string $name, $item)
    {
        $array = session()->pull($name) ?? [];

        unset($array[array_serach($array, $item)]);

        session()->put($name, array_unique($array));
    }
}

if (!function_exists('is_module_enabled')) {
    function is_module_enabled($module)
    {
        return array_key_exists($module, app('modules')->allEnabled());
    }
}

if (!function_exists('is_core_module')) {
    function is_core_module($module)
    {
        return in_array(strtolower($module), array_map('strtolower', app('application.ModulesList')));
    }
}

if (!function_exists('is_core_theme')) {
    function is_core_theme(string $theme)
    {
        return in_array($theme, ['artisan', 'simplest'], false);
    }
}

if (!function_exists('get_application_frontend_locales')) {
    function get_application_frontend_locales()
    {
        $availableFrontendLocales = [];

        if (null !== cache()->tags(['settings', 'global'])->get('application.locales')) {
            foreach (json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue) as $locale) {
                $availableFrontendLocales = array_merge($availableFrontendLocales, [$locale => config("application.core.available-locales.$locale")]);
            }
        } else {
            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $locale) {
                $availableFrontendLocales = array_merge($availableFrontendLocales, [$key => config("application.core.available-locales.$key")]);
            }
        }

        return $availableFrontendLocales;
    }
}

if (!function_exists('get_application_frontend_theme_path')) {
    function get_application_frontend_theme_path()
    {
        $themes_path = \Storage::disk('themes')->getAdapter()->getPathPrefix();

        return $themes_path . app('application.settings')->get('core::frontend_theme', null, 'simplest');
    }
}

if (!function_exists('substract_percents')) {
    /**
     * Get discount from number
     *
     * @param int|float $number
     * @param int|float $discount
     *
     * @return float
     */
    function substract_percents($number, $discount = .01)
    {
        return round((100 - $discount) * $number / 100, 2);
    }
}

if (!function_exists('app_cache')) {
    function app_cache($name, $callback)
    {
        return Cache::has($name) ? Cache::get($name) : Cache::rememberForever($name, $callback);
    }
}

if (!function_exists('convert_to_object')) {
    /**
     * Converts array to object
     *
     * @param array $array
     *
     * @return stdClass
     */
    function convert_to_object($array)
    {
        $object = new stdClass();

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convert_to_object($value);
            }

            $object->$key = $value;
        }

        return $object;
    }
}

if (!function_exists('clear_email')) {
    /**
     * Clear email
     *
     * @param $email
     *
     * @return string
     */
    function clear_email($email)
    {
        $trimed_email = trim($email);
        $lower_email = strtolower($trimed_email);
        $filtered_email = filter_var($lower_email, FILTER_SANITIZE_EMAIL);
        $iconv_email = iconv('ISO-8859-1', 'UTF-8//IGNORE', $filtered_email);

        return $iconv_email;
    }
}

if (!function_exists('is_sorted_array')) {
    /**
     * returns true if array sorted alphabetically
     *
     * @param array $array
     *
     * @return bool
     */
    function is_sorted_array(array $array)
    {
        $array = array_values($array);
        $prev_value = '';
        foreach ($array as $key => $value) {
            if ($key !== 0 && strcasecmp(strval($prev_value), strval($value)) > 0) {
                return false;
            }

            $prev_value = $value;
        }

        return true;
    }
}

if (!function_exists('array_drop')) {
    /**
     * search in array and delete item
     *
     * @param array $array
     *
     * @return bool
     */
    function array_drop(array &$array, $search)
    {
        if (($key = array_search($search, $array)) !== false) {
            unset($array[$key]);
        }

        return $array;
    }
}
