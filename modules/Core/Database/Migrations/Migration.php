<?php

namespace Modules\Core\Database\Migrations;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration as LaravelMigration;

class Migration extends LaravelMigration
{
    /**
     * Tables list
     *
     * @var array
     */
    private $tables;

    /**
     * Model list
     *
     * @var array
     */
    private $models;

    public function __construct()
    {
        $files = scandir($this->folder);
        $this->models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $model = json_decode(file_get_contents($this->folder . '/' . $file));
                ($model->not_table ?? false) ?: $this->models[explode('.', $file)[0]] = $model;
            }
        }

        foreach ($this->models as $id => $model) {
            $this->tables[] = $model->table_name;

            if ($model->need_translations) {
                $this->tables[] = str_singular($model->table_name) . '_translations';
            }

            if (isset($model->pivots)) {
                foreach ($model->pivots as $table => $items) {
                    $this->tables[] = $table;
                }
            }
        }
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        foreach ($this->models as $model_name => $model) {
            $this->generateTables($model, $model_name);
        }

        foreach ($this->models as $model_name => $model) {
            $this->generatePivots($model, $model_name);
        }

        foreach ($this->models as $model_name => $model) {
            $this->generateForeigns($model, $model_name);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Generate main table and its translation for up
     *
     * @param object $model
     * @param string $model_name
     * @return void
     */
    protected function generateTables($model, $model_name)
    {
        Schema::create(
            $model->table_name,
            function (Blueprint $table) use ($model) {
                $table->increments('id');

                $this->getFields($model, $table, true);

                $table->timestamps();
            }
        );

        if ($model->need_translations) {
            Schema::create(
                str_singular($model->table_name) . '_translations',
                function (Blueprint $table) use ($model_name, $model) {
                    $table->increments('id');
                    $table->unsignedInteger($model_name . '_id');
                    $table->string('locale');

                    $this->getFields($model, $table, false);

                    $table->timestamps();
                    $unique_key = str_singular($model->table_name) . '_translations_' . $model_name . '_id_locale_unique';

                    if (strlen($unique_key) > 50) {
                        $unique_key = str_singular($model->table_name) . '_t_' . $model_name . '_ilocun';
                    }

                    $table->unique([$model_name . '_id', 'locale'], $unique_key);

                    $foreign_key = str_singular($model->table_name) . '_translations_' . $model_name . '_id_foreign';

                    if (strlen($foreign_key) > 50) {
                        $foreign_key = str_singular($model->table_name) . '_t_' . $model_name . '_if';
                    }

                    $table->foreign($model_name . '_id', $foreign_key)
                        ->references('id')
                        ->on($model->table_name)
                        ->onDelete('cascade');
                }
            );
        }
    }

    /**
     * Generate all foreign keys for up
     *
     * @param object $model
     * @param string $model_name
     * @return void
     */
    protected function generateForeigns($model, $model_name)
    {
        Schema::table(
            $model->table_name,
            function (Blueprint $table) use ($model) {
                $this->getForeigns($model, $table);
            }
        );
    }

    protected function getFields($model, $table, $condition_to_skip = true)
    {
        if ($condition_to_skip && ($model->templates->default->standard->status ?? false)) {
            $table->tinyInteger('status')->default(1);
        }

        if ($condition_to_skip && ($model->templates->default->standard->order ?? false)) {
            $table->unsignedInteger('order')->defaul(1);
        }

        if ($condition_to_skip && ($model->templates->default->standard->nested ?? false)) {
            $table->nestedSet();
            $table->tinyInteger('depth')->default(1);
        }

        if ($condition_to_skip && ($model->templates->default->standard->slugable ?? false)) {
            $table->string('slug')->unique()->index();
        }

        if ($condition_to_skip && ($model->templates->default->standard->desktop_mobile_images ?? false)) {
            if (!isset($model->templates->default->desktop_image)) {
                $table->string('desktop_image')->nullable();
            }

            if (!isset($model->templates->default->mobile_image)) {
                $table->string('mobile_image')->nullable();
            }
        }

        foreach ($model->templates->default as $field_name => $field) {
            if (!($field->field_type ?? false)) {
                continue;
            }

            if (!!($field->is_translated ?? false) == $condition_to_skip ||
                !($field->fillable ?? true) ||
                $field_name == 'standard' ||
                ($field->type ?? false) == 'tab'
            ) {
                continue;
            }

            if ($field->field_type == 'string') {
                $newTable = $table->{$field->field_type}($field_name, $field->field_symbols_count ?? 191);
            } else {
                $newTable = $table->{$field->field_type}($field_name);
            }

            if (!(!($field->is_translated ?? false) && ($field->required ?? false))) {
                $newTable->nullable();
            }

            if ($field->unique ?? false) {
                $newTable->unique();
            }

            if ($field->index ?? false) {
                $newTable->index();
            }

            if (($field->default ?? false) !== false) {
                $newTable->default($field->default);
            }
        }

        if (($model->uniques ?? false) && $condition_to_skip) {
            foreach ($model->uniques as $unique) {
                $table->unique($unique);
            }
        }
    }

    protected function generatePivots($model, $model_name)
    {
        if (isset($model->pivots)) {
            foreach ($model->pivots as $table_name => $items) {
                Schema::create(
                    $table_name,
                    function (Blueprint $table) use ($table_name, $items) {
                        $this->getPivotIds($items, $table);

                        foreach ($items->items ?? [] as $field_name => $field) {
                            if ($field->field_type == 'string') {
                                $newTable = $table->{$field->field_type}($field_name, $field->field_symbols_count ?? 191);
                            } else {
                                $newTable = $table->{$field->field_type}($field_name);
                            }

                            if (!($field->required ?? false)) {
                                $newTable->nullable();
                            }

                            if ($field->unique ?? false) {
                                $newTable->unique();
                            }

                            if ($field->unique ?? false) {
                                $newTable->index();
                            }

                            if ($field->default ?? false) {
                                $newTable->default($field->default);
                            }
                        }

                        foreach ($items->foreigns ?? [] as $index => $foreign) {
                            if ($foreign->name ?? false) {
                                $table->foreign($index, $foreign->name)
                                    ->references($foreign->references)
                                    ->on($foreign->table)
                                    ->onDelete($foreign->onDelete);
                            } else {
                                $table->foreign($index)
                                    ->references($foreign->references)
                                    ->on($foreign->table)
                                    ->onDelete($foreign->onDelete);
                            }
                        }

                        $table->timestamps();
                    }
                );
            }
        }
    }

    protected function getForeigns($model, $table)
    {
        foreach ($model->foreigns ?? [] as $index => $foreign) {
            if ($foreign->name ?? false) {
                $table->foreign($index, $foreign->name)
                    ->references($foreign->references)
                    ->on($foreign->table)
                    ->onDelete($foreign->onDelete);
            } else {
                $table->foreign($index)
                    ->references($foreign->references)
                    ->on($foreign->table)
                    ->onDelete($foreign->onDelete);
            }
        }
    }

    public function getPivotIds($items, $table)
    {
        if (!isset($items->ids)) {
            return;
        }

        $index_name = '';

        foreach ($items->ids as $id => $params) {
            $table->integer($id)->unsigned();
            $idents[] = $id;
            $index_name .= $id;

            if ($params->no_foreign ?? false) {
                continue;
            }

            $foreign_key = $table->getTable() . '_' . $id . '_foreign';

            if (strlen($foreign_key) > 50) {
                $foreign_key = explode('_', $foreign_key);

                foreach ($foreign_key as $key => $item) {
                    if ($key) {
                        $foreign_key[$key] = substr($item, 0, 2);
                    }
                }

                $foreign_key = implode('_', $foreign_key);
            }

            if ($params->name ?? false) {
                $foreign_key = $params->name;
            }

            $table->foreign($id, $foreign_key)
                ->references($params->references ?? 'id')
                ->on($params->table)
                ->onDelete($params->onDelete);
        }

        if ($idents ?? false) {
            $table->index($idents, $index_name);
        }
    }
}
