<?php

namespace Modules\Core\Internationalisation;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Buldozer\Managers\ModelConfigurationManagerInterface;

abstract class BaseFormRequest extends FormRequest
{
    /**
     * Set the translation key prefix for attributes.
     *
     * @var string
     */
    protected $translationsAttributesKey = 'validation.attributes.';

    /**
     * Current processed locale.
     *
     * @var string
     */
    protected $localeKey;
    protected $configurator = false;

    public function configurator()
    {
        if (!$this->configurator && is_file($this->file)) {
            $this->configurator = app()->make(ModelConfigurationManagerInterface::class, [$this->file]);
        }

        return $this->configurator;
    }

    public function rules()
    {
        return $this->configurator() ? $this->configurator->rules() : [];
    }

    public function translationRules()
    {
        return $this->configurator() ? $this->configurator->translationRules() : [];
    }

    /**
     * Return an array of messages for translatable fields.
     *
     * @return array
     */
    public function translationMessages()
    {
        return [];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $factory = $this->container->make('Illuminate\Validation\Factory');

        if (method_exists($this, 'validator')) {
            return $this->container->call([$this, 'validator'], compact('factory'));
        }

        $rules = $this->container->call([$this, 'rules']);
        $attributes = $this->attributes();
        $messages = [];

        $translationsAttributesKey = $this->getTranslationsAttributesKey();

        $this->locale = $this->requiredLocales();

        foreach ($this->container->call([$this, 'translationRules']) as $attribute => $rule) {
            $key = $this->requiredLocales() . '.' . $attribute;
            $rules[$key] = $rule;
            $attributes[$key] = trans($translationsAttributesKey . $attribute);
        }

        foreach ($this->container->call([$this, 'translationMessages']) as $attributeAndRule => $message) {
            $messages[$this->requiredLocales() . '.' . $attributeAndRule] = $message;
        }

        return $factory->make(
            $this->all(),
            $rules,
            array_merge($this->messages(), $messages),
            $attributes
        );
    }

    /**
     * @return array
     */
    public function withTranslations()
    {
        $results = $this->all();
        $translations = [];

        foreach ($this->requiredLocales() as $key => $locale) {
            $locales[] = $key;
            $translations[$key] = $this->get($key);
        }

        $results['translations'] = $translations;

        array_forget($results, $locales);

        return $results;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function requiredLocales()
    {
        return json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue)[0];
    }

    /**
     * Get the validation for attributes key from the implementing class
     * or use a sensible default.
     *
     * @return string
     */
    private function getTranslationsAttributesKey()
    {
        return rtrim($this->translationsAttributesKey, '.') . '.';
    }
}
