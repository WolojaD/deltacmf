<?php

namespace Modules\Core\Internationalisation;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Dimsav\Translatable\Translatable as OriginTranslatable;
use Dimsav\Translatable\Exception\LocalesNotDefinedException;

trait Translatable
{
    use OriginTranslatable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->defaultLocale = $this->validateLocale();
    }

    /**
     * Get corect locale for backend or frontend
     *
     * @return type
     */
    private function validateLocale()
    {
        if (app()->environment('testing')) {
            return 'ru';
        }

        if ($this->onBackend()) {
            return $this->defaultLanguage();
        }

        return locale();
    }

    /**
     * Checks if the current url matches the configured backend uri.
     *
     * @return bool
     */
    private function onBackend()
    {
        $url = request()->getPathInfo();

        if (str_contains($url, config('application.core.core.admin-prefix', 'backend'))) {
            return true;
        }

        return false;
    }

    /**
     * Get default frontend locale (main language)
     *
     * @return type
     */
    public function defaultTranslate()
    {
        return $this->translate($this->defaultLanguage(), true);
    }

    /**
     * Get given locale or it get current locale
     *
     * @param  boolean|string $locale
     *
     * @return type
     */
    public function givenTranslate($locale = false)
    {
        if (!$locale) {
            $locale = locale();
        }

        return $this->translate($this->givenLanguage($locale), true);
    }

    /**
     * @return string
     */
    protected function defaultLanguage()
    {
        if (cache()->tags(['settings', 'global'])->get('application.locales') !== null) {
            return json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue)[0];
        }

        return key(config('laravellocalization.defaultApplicationLocales'));
    }

    /**
     * @param string $locale
     *
     * @return string
     */
    protected function givenLanguage($locale)
    {
        if (cache()->tags(['settings', 'global'])->get('application.locales') !== null) {
            $languages = json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue);
        } else {
            $languages = config('laravellocalization.defaultApplicationLocales');
        }

        return in_array($locale, $languages) ? $locale : config('app.fallback_locale');
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        $tempTranslations = $this->translations;

        if ($this->exists) {
            if (count($this->getDirty()) > 0) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    $this->translations = $tempTranslations;

                    return $this->saveTranslations();
                }

                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                $this->translations = $tempTranslations;

                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('updated', false);
                }

                return $saved;
            }
        } elseif (parent::save($options)) {
            // We save the translations only if the instance is saved in the database.
            $this->translations = $tempTranslations;

            return $this->saveTranslations();
        }

        return false;
    }

    /**
     * @throws \Dimsav\Translatable\Exception\LocalesNotDefinedException
     *
     * @return array
     */
    protected function getLocales()
    {
        $localesConfig = (array) config('translatable.locales');

        if (app()->environment('testing')) {
            $localesConfig = ['ru', 'uk', 'en'];
        }

        if (empty($localesConfig)) {
            throw new LocalesNotDefinedException('Please make sure you have run "php artisan config:publish dimsav/laravel-translatable" ' .
                ' and that the locales configuration is defined.');
        }

        $locales = [];

        foreach ($localesConfig as $key => $locale) {
            if (is_array($locale)) {
                $locales[] = $key;

                foreach ($locale as $countryLocale) {
                    $locales[] = $key . $this->getLocaleSeparator() . $countryLocale;
                }
            } else {
                $locales[] = $locale;
            }
        }

        return $locales;
    }

    /**
    * @param string $key
    *
    * @return mixed
    */
    public function getAttribute($key)
    {
        list($attribute, $locale) = $this->getAttributeAndLocale($key);

        if ($this->{$attribute} ?? false) {
            return $this->{$attribute};
        }

        if ($this->isTranslationAttribute($attribute)) {
            if ($this->getTranslation($locale) === null) {
                if ($this->getTranslation($this->getFallbackLocale()) === null) {
                    return $this->getAttributeValue($attribute);
                }

                $locale = $this->getFallbackLocale();
            }

            // If the given $attribute has a mutator, we push it to $attributes and then call getAttributeValue
            // on it. This way, we can use Eloquent's checking for Mutation, type casting, and
            // Date fields.
            if ($this->hasGetMutator($attribute)) {
                $this->attributes[$attribute] = $this->getAttributeOrFallback($locale, $attribute);

                return $this->getAttributeValue($attribute);
            }

            return $this->getAttributeOrFallback($locale, $attribute);
        }

        return parent::getAttribute($key);
    }

    /**
     * This scope sorts results by the given translation field.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $key
     * @param string                                $sortmethod
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function scopeOrderByTranslation(Builder $query, $key, $sortmethod = 'asc')
    {
        $translationTable = $this->getTranslationsTable();
        $localeKey = $this->getLocaleKey();
        $table = $this->getTable();
        $keyName = $this->getKeyName();

        return $query->join($translationTable, function ($join) use ($translationTable, $localeKey, $table, $keyName) {
            $join->on($translationTable . '.' . $this->getRelationKey(), '=', $table . '.' . $keyName)
                ->where($translationTable . '.' . $localeKey, $this->defaultLanguage());
        })
            ->orderBy($translationTable . '.' . $key, $sortmethod)
            ->select($translationTable . '.*', $table . '.*')
            ->with('translations');
    }

    /**
     * Adds scope to get a list of translated attributes, using the default locale.
     * Example usage: Country::scopeListsDefaultTranslations('name')->get()->toArray()
     * Will return an array with items:
     *  [
     *      'id' => '1',                // The id of country
     *      'name' => 'Griechenland'    // The translated name
     *  ].
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $translationField
     */
    public function scopeListsDefaultTranslations(Builder $query, $translationField)
    {
        $withFallback = $this->useFallback();
        $translationTable = $this->getTranslationsTable();
        $localeKey = $this->getLocaleKey();

        $query
            ->select($this->getTable() . '.' . $this->getKeyName(), $translationTable . '.' . $translationField)
            ->leftJoin($translationTable, $translationTable . '.' . $this->getRelationKey(), '=', $this->getTable() . '.' . $this->getKeyName())
            ->where($translationTable . '.' . $localeKey, $this->defaultLanguage());

        if ($withFallback) {
            $query->orWhere(function (Builder $q) use ($translationTable, $localeKey) {
                $q->where($translationTable . '.' . $localeKey, $this->getFallbackLocale())
                  ->whereNotIn($translationTable . '.' . $this->getRelationKey(), function (QueryBuilder $q) use (
                      $translationTable,
                      $localeKey
                  ) {
                      $q->select($translationTable . '.' . $this->getRelationKey())
                        ->from($translationTable)
                        ->where($translationTable . '.' . $localeKey, $this->defaultLanguage());
                  });
            });
        }
    }

    /**
     * Adds scope to get a list of all translated attributes, using the current locale.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeListsAllTranslations(Builder $query)
    {
        $withFallback = $this->useFallback();
        $translationTable = $this->getTranslationsTable();
        $localeKey = $this->getLocaleKey();

        $translationFields = [];

        foreach ($this->translatedAttributes as $key => $item) {
            $translationFields[$key] = $translationTable . '.' . $item;
        }

        $query
            ->select('*', ...$translationFields)
            ->leftJoin($translationTable, $translationTable . '.' . $this->getRelationKey(), '=', $this->getTable() . '.' . $this->getKeyName())
            ->where($translationTable . '.' . $localeKey, $this->locale());
        if ($withFallback) {
            $query->orWhere(function (Builder $q) use ($translationTable, $localeKey) {
                $q->where($translationTable . '.' . $localeKey, $this->getFallbackLocale())
                  ->whereNotIn($translationTable . '.' . $this->getRelationKey(), function (QueryBuilder $q) use (
                      $translationTable,
                      $localeKey
                  ) {
                      $q->select($translationTable . '.' . $this->getRelationKey())
                        ->from($translationTable)
                        ->where($translationTable . '.' . $localeKey, $this->locale());
                  });
            });
        }
    }
}
