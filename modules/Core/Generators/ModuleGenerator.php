<?php

namespace Modules\Core\Generators;

use Nwidart\Modules\Generators\ModuleGenerator as OriginModuleGenerator;

class ModuleGenerator extends OriginModuleGenerator
{
    /**
     * Generate some resources.
     */
    public function generateResources()
    {
        $this->console->call('application:module:make-seed', [
            'name' => $this->getName(),
            'module' => $this->getName(),
            '--master' => true,
        ]);

        $this->console->call('module:make-provider', [
            'name' => $this->getName() . 'ServiceProvider',
            'module' => $this->getName(),
            '--master' => true,
        ]);

        $this->console->call('module:make-controller', [
            'controller' => $this->getName() . 'Controller',
            'module' => $this->getName(),
        ]);
    }
}
