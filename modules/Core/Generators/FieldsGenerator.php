<?php

namespace Modules\Core\Generators;

use Illuminate\Support\Str;

class FieldsGenerator
{
    /**
     * Generate text for seeder.
     *
     * @param array $fields
     *
     * @return string
     */
    public function generateText($fields)
    {
        if (!$fields || !count($fields)) {
            return '';
        }

        $result = [];

        foreach ($fields as $field) {
            if ('migrations' == $field['table'] || 'fields' == $field['table']) {
                return;
            }

            $result[] = $this->generateFieldInfo($field);
        }

        return '$fields = ' . $this->arrayToString($result) . ";\n" . 'foreach($fields as $field) {' . "\n" . '\Modules\FieldGenerator\Entities\Field::updateOrCreate ($field);' . "\n}";
    }

    /**
     * convert array to string view.
     *
     * @param array $array
     *
     * @return string
     */
    private function arrayToString(array $array)
    {
        $result = "[\n";

        foreach ($array as $key => $item) {
            if (null == $item) {
                continue;
            }

            if (is_array($item)) {
                $result .= "'{$key}' => {$this->arrayToString($item)},\n";
            } else {
                $result .= "'{$key}' => '{$item}',\n";
            }
        }

        $result .= ']';

        return $result;
    }

    /**
     * generate array of description of field.
     *
     * @param array $field
     *
     * @return array
     */
    private function generateFieldInfo(array $field)
    {
        if (in_array($field['name'], ['id', 'created_at', 'updated_at', 'order', 'depth', 'locale'])) {
            return;
        }

        preg_match('/(.*)_translations$/', $field['table'], $translated);

        $field['is_translated'] = 0;

        if (count($translated)) {
            $field['table'] = Str::plural($translated[1]);
            $field['is_translated'] = 1;
        }

        $names = [
            'table_name' => $field['table'],
            'table_field' => $field['name'],
            'required' => !$field['nullable'],
            'hint' => '',
            'is_translated' => $field['is_translated'],
        ];

        $params = [
            'status' => [
                'type' => 'select',
                'label' => ['en' => 'Status', 'ru' => 'Статус'],
                'is_parent' => 0,
                'relation' => '',
            ],
            'parent_id' => [
                'type' => 'select-parent',
                'is_parent' => 1,
                'relation' => '',
            ],
            'email' => [
                'type' => 'email',
                'label' => ['en' => 'E-mail', 'ru' => 'E-mail'],
                'is_parent' => 0,
                'relation' => '',
            ],
            'password' => [
                'type' => 'password',
                'label' => ['en' => 'Password', 'ru' => 'Пароль'],
                'is_parent' => 0,
                'relation' => '',
            ],
        ];

        if (isset($params[$field['name']])) {
            return array_merge($names, $params[$field['name']]);
        }

        $filtered = $this->filter($field['name']);

        if ($filtered) {
            return array_merge($names, $filtered);
        }

        return array_merge($names, [
                'type' => $this->generateFieldByType($field['type']),
                'label' => ['en' => Str::ucfirst($field['name']), 'ru' => Str::ucfirst($field['name'])],
                'is_parent' => 0,
                'relation' => '',
            ]);
    }

    /**
     * parse field name and return description of field.
     *
     * @param string $field
     *
     * @return array
     */
    private function filter(string $field)
    {
        preg_match('/(.*)_id$/', $field, $relation);

        if (count($relation)) {
            return [
                'type' => 'select',
                'label' => ['en' => Str::ucfirst($relation[1]), 'ru' => Str::ucfirst($relation[1])],
                'is_parent' => 0,
                'relation' => $relation[1],
            ];
        }

        preg_match('/^is_(.*)/', $field, $checkbox);

        if (count($checkbox)) {
            return [
                'type' => 'checkbox',
                'label' => ['en' => Str::ucfirst($checkbox[1]), 'ru' => Str::ucfirst($checkbox[1])],
                'is_parent' => 0,
                'relation' => '',
            ];
        }

        preg_match('/(.*)_color$/', $field, $color);

        if (count($color)) {
            return [
                'type' => 'color-picker',
                'label' => ['en' => Str::ucfirst($color[1]) . ' color', 'ru' => Str::ucfirst($color[1]) . ' color'],
                'is_parent' => 0,
                'relation' => '',
            ];
        }

        preg_match('/(.*)_image$/', $field, $image);

        if (count($image)) {
            return [
                'type' => 'upload-image',
                'label' => ['en' => Str::ucfirst($image[1]) . ' image', 'ru' => Str::ucfirst($image[1]) . ' image'],
                'is_parent' => 0,
                'relation' => '',
            ];
        }

        return false;
    }

    /**
     * get field type by type of field 8-).
     *
     * @param string $field
     *
     * @return string
     */
    private function generateFieldByType($field)
    {
        $array = [
            'bigInteger' => 'number',
            'bigint' => 'number',
            'binary' => 'binary',
            'boolean' => 'checkbox',
            'char' => 'text',
            'date' => 'date',
            'dateTime' => 'datetime',
            'datetime' => 'datetime',
            'dateTimeTz' => 'datetime',
            'decimal' => 'number',
            'double' => 'number',
            'float' => 'number',
            'geometry' => 'geometry',
            'geometryCollection' => 'geometryCollection',
            'integer' => 'number',
            'ipAddress' => 'ipAddress',
            'json' => 'json',
            'jsonb' => 'json',
            'lineString' => 'text',
            'longText' => 'ckeditor',
            'macAddress' => 'macAddress',
            'mediumInteger' => 'number',
            'mediumint' => 'number',
            'mediumText' => 'number',
            'multiLineString' => 'number',
            'multiPoint' => 'multiPoint',
            'multiPolygon' => 'multiPolygon',
            'point' => 'point',
            'polygon' => 'polygon',
            'smallInteger' => 'number',
            'smallint' => 'number',
            'string' => 'text',
            'text' => 'textarea',
            'time' => 'time',
            'timeTz' => 'time',
            'timestamp' => 'datetime',
            'timestampTz' => 'datetime',
            'tinyInteger' => 'number',
            'tinyint' => 'number',
            'unsignedBigInteger' => 'number',
            'unsignedDecimal' => 'number',
            'unsignedInteger' => 'number',
            'unsignedMediumInteger' => 'number',
            'unsignedSmallInteger' => 'number',
            'unsignedTinyInteger' => 'number',
            'year' => 'year',
        ];

        return $array[$field] ?? 'text';
    }
}
