<?php

namespace Modules\Core\Macros\Routing;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

class BackendRoutes
{
    public static function registerMacros()
    {
        Router::macro('backendRouteList', function ($module) {
            $folder = base_path('modules/' . $module . '/Config/models');
            $files = scandir($folder);

            $models = [];

            foreach ($files as $key => $file) {
                if ('.' !== $file[0]) {
                    $models[explode('.', $file)[0]] = json_decode(file_get_contents($folder . '/' . $file));
                }
            }

            foreach ($models as $model_name => $model) {
                Route::group(['prefix' => $model->backend_route_prefix], function () use ($model, $module) {
                    $name = $model->backend_path_name;

                    $permission = strtolower(str_replace_array('_', ['.', '.'], $name));

                    $name_without_module = str_replace_first(str_before($name, '_') . '_', '', $name);
                    $controller = $module . 'Controller';

                    Route::get('/', ['as' => 'backend.' . $name . '.index', 'uses' => $controller . '@index', 'middleware' => 'can:' . $permission . '.index']);

                    Route::get('create', ['as' => 'backend.' . $name . '.create', 'uses' => $controller . '@create', 'middleware' => 'can:' . $permission . '.create']);

                    Route::get('{ident}', ['as' => 'backend.' . $name . '.show', 'uses' => $controller . '@show', 'middleware' => 'can:' . $permission . '.index']);

                    Route::get('{ident}/create', ['as' => 'backend.' . $name . '.create', 'uses' => $controller . '@create', 'middleware' => 'can:' . $permission . '.create']);

                    Route::get('{ident}/edit', ['as' => 'backend.' . $name . '.edit', 'uses' => $controller . '@edit', 'middleware' => 'can:' . $permission . '.index']);
                });
            }
        });
    }
}
