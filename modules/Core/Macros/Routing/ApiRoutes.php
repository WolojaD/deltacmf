<?php

namespace Modules\Core\Macros\Routing;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

class ApiRoutes
{
    public static function registerMacros()
    {
        Router::macro('apiRouteList', function ($module) {
            $folder = base_path('modules/' . $module . '/Config/models');
            $files = scandir($folder);

            $models = [];

            foreach ($files as $key => $file) {
                if ('.' !== $file[0]) {
                    $models[explode('.', $file)[0]] = json_decode(file_get_contents($folder . '/' . $file));
                }
            }

            foreach ($models as $model_name => $model) {
                Route::group(['prefix' => $model->backend_route_prefix, 'middleware' => ['api.token', 'auth.admin']], function () use ($model) {
                    $name = $model->backend_path_name;

                    $permission = strtolower(str_replace('_', '.', $name));

                    $name_without_module = str_replace_first(str_before($name, '_') . '_', '', $name);
                    $controller = studly_case($name_without_module) . 'Controller';

                    Route::get('/', ['as' => 'api.backend.' . $name . '.index', 'uses' => $controller . '@index', 'middleware' => 'token-can:' . $permission . '.index']);

                    Route::get('find-new', ['as' => 'api.backend.' . $name . '.findNew', 'uses' => $controller . '@findNew', 'middleware' => 'token-can:' . $permission . '.create']);

                    Route::get('find-edit', ['as' => 'api.backend.' . $name . '.findEdit', 'uses' => $controller . '@findEdit', 'middleware' => 'token-can:' . $permission . '.index']);

                    Route::get('find/{ident}', ['as' => 'api.backend.' . $name . '.find', 'uses' => $controller . '@find', 'middleware' => 'token-can:' . $permission . '.index']);

                    foreach ($model->templates as $template) {
                        if (!($template->standard->has_tables ?? false)) {
                            continue;
                        }

                        foreach ($template->standard->has_tables->inner_tables ?? [] as $table) {
                            Route::get('inner-' . $table . '/{ident}', ['as' => 'api.backend.' . $name . '.find-' . $table, 'uses' => $controller . '@findTable', 'middleware' => 'token-can:' . $permission . '.index']);

                            Route::get('show-inner-' . $table . '/{ident}', ['as' => 'api.backend.' . $name . '.show.find-' . $table, 'uses' => $controller . '@findTable', 'middleware' => 'token-can:' . $permission . '.index']);
                        }

                        foreach ($template->standard->has_tables->outer_tables ?? [] as $table) {
                            Route::get('outer-' . $table . '/{ident}', ['as' => 'api.backend.' . $name . '.find-' . $table, 'uses' => $controller . '@findTable', 'middleware' => 'token-can:' . $permission . '.index']);

                            Route::get('show-outer-' . $table . '/{ident}', ['as' => 'api.backend.' . $name . '.show.find-' . $table, 'uses' => $controller . '@findTable', 'middleware' => 'token-can:' . $permission . '.index']);
                        }
                    }

                    Route::get('show/{ident}', ['as' => 'api.backend.' . $name . '.show', 'uses' => $controller . '@index', 'middleware' => 'token-can:' . $permission . '.index']);

                    Route::get('export-excel', ['as' => 'api.backend.' . $name . '.exportExcel', 'uses' => $controller . '@exportExcel', 'middleware' => 'token-can:' . $permission . '.index']);

                    Route::post('import-excel', ['as' => 'api.backend.' . $name . '.importExcel', 'uses' => $controller . '@importExcel', 'middleware' => 'token-can:' . $permission . '.index']);

                    Route::put('toggle-field', ['as' => 'api.backend.' . $name . '.toggleField', 'uses' => $controller . '@toggleField', 'middleware' => 'token-can:' . $permission . '.edit']);

                    Route::put('change-order', ['as' => 'api.backend.' . $name . '.changeOrder', 'uses' => $controller . '@changeOrder', 'middleware' => 'token-can:' . $permission . '.edit']);

                    Route::post('{ident}/edit', ['as' => 'api.backend.' . $name . '.update', 'uses' => $controller . '@update', 'middleware' => 'token-can:' . $permission . '.edit']);

                    Route::post('/', ['as' => 'api.backend.' . $name . '.store', 'uses' => $controller . '@store', 'middleware' => 'token-can:' . $permission . '.create']);

                    Route::delete('destroy-multiple', ['as' => 'api.backend.' . $name . '.destroy_multiple', 'uses' => $controller . '@destroyMultiple', 'middleware' => 'token-can:' . $permission . '.destroy']);

                    Route::delete('{ident}', ['as' => 'api.backend.' . $name . '.destroy', 'uses' => $controller . '@destroy', 'middleware' => 'token-can:' . $permission . '.destroy']);
                });
            }
        });
    }
}
