<?php

return [
    'default' => [
        'edit_path' => 'backend.page_page.edit',
        'model' => \Modules\Page\Entities\Page::class,
        'repository' => 'Modules\Page\Repositories\Eloquent\EloquentPageRepository',
        'needs_index' => false,
        'weight' => .5,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'brands' => [
        'edit_path' => 'backend.ecommerce_brand.edit',
        'model' => \Modules\Ecommerce\Entities\Brand::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentBrandRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'brand' => [
        'edit_path' => 'backend.ecommerce_brand.edit',
        'model' => \Modules\Ecommerce\Entities\Brand::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentBrandRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'sales' => [
        'edit_path' => 'backend.ecommerce_sale.edit',
        'model' => \Modules\Ecommerce\Entities\Sale::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentSaleRepository',
        'needs_index' => false,
        'weight' => .5,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'sale' => [
        'edit_path' => 'backend.ecommerce_sale.edit',
        'model' => \Modules\Ecommerce\Entities\Sale::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentSaleRepository',
        'needs_index' => false,
        'weight' => .5,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'news&articles' => [
        'combo' => ['news', 'articles']
    ],
    'news' => [
        'edit_path' => 'backend.articles_category.edit',
        'model' => \Modules\Articles\Entities\Category::class,
        'repository' => 'Modules\Articles\Repositories\Eloquent\EloquentCategoryRepository',
        'needs_index' => true,
        'weight' => .5,
        'frequency' => 'weekly',
        'to_page' => true,
        'filterable' => true,
    ],
    'articles' => [
        'edit_path' => 'backend.articles_category.edit',
        'model' => \Modules\Articles\Entities\Category::class,
        'repository' => 'Modules\Articles\Repositories\Eloquent\EloquentCategoryRepository',
        'needs_index' => true,
        'weight' => .5,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'new&article' => [
        'combo' => ['new', 'article']
    ],
    'article' => [
        'edit_path' => 'backend.articles_post.edit',
        'model' => \Modules\Articles\Entities\Post::class,
        'repository' => 'Modules\Articles\Repositories\Eloquent\EloquentPostRepository',
        'weight' => .5,
        'frequency' => 'weekly',
        'belongs_to' => 'articles',
        'og_type' => 'article'
    ],
    'new' => [
        'edit_path' => 'backend.articles_post.edit',
        'model' => \Modules\Articles\Entities\Post::class,
        'repository' => 'Modules\Articles\Repositories\Eloquent\EloquentPostRepository',
        'weight' => .5,
        'frequency' => 'weekly',
        'belongs_to' => 'news',
        'og_type' => 'article'
    ],
    'products' => [
        'edit_path' => 'backend.ecommerce_category.edit',
        'model' => \Modules\Ecommerce\Entities\Category::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentCategoryRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'self_page' => true,
        'filterable' => true,
        'type' => 'store',
    ],
    'product' => [
        'edit_path' => 'backend.ecommerce_product.edit',
        'model' => \Modules\Ecommerce\Entities\Product::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentProductRepository',
        'weight' => .5,
        'frequency' => 'weekly',
        'belongs_to' => 'products',
        'self_page' => true,
        'og_type' => 'product',
        'type' => 'store'
    ],
    'sales' => [
        'edit_path' => 'backend.ecommerce_sale.edit',
        'model' => \Modules\Ecommerce\Entities\Sale::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentSaleRepository',
        'needs_index' => false,
        'weight' => .5,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'top_products' => [
        'edit_path' => 'backend.ecommerce_category.edit',
        'model' => \Modules\Ecommerce\Entities\Category::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentCategoryRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'to_page' => true,
        'filterable' => true,
    ],
    'new_products' => [
        'edit_path' => 'backend.ecommerce_category.edit',
        'model' => \Modules\Ecommerce\Entities\Category::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentCategoryRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'to_page' => true,
        'filterable' => true,
    ],
    'discounted_products' => [
        'edit_path' => 'backend.ecommerce_category.edit',
        'model' => \Modules\Ecommerce\Entities\Category::class,
        'repository' => 'Modules\Ecommerce\Repositories\Eloquent\EloquentCategoryRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'stores' => [
        'edit_path' => 'backend.stores_city.edit',
        'model' => \Modules\Stores\Entities\City::class,
        'repository' => 'Modules\Stores\Repositories\Eloquent\EloquentCityRepository',
        'needs_index' => false,
        'weight' => .8,
        'frequency' => 'weekly',
        'to_page' => true
    ],
    'filters' => [
        'weight' => .5,
        'frequency' => 'weekly',
        'no_structure' => true
    ],
    'manual' => [
        'weight' => .5,
        'frequency' => 'weekly',
        'no_structure' => true,
        'type' => 'manual'
    ],
];
