<?php

namespace Modules\Seo\Providers;

use Illuminate\Support\Facades\Config;
use Modules\Seo\Console\GenerateSitemap;
use Illuminate\Database\Eloquent\Factory;
use Modules\Core\Providers\ServiceProvider;
use Modules\Core\Events\LoadingBackendTranslations;

class SeoServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig('seo', 'config');
        $this->publishConfig('seo', 'permissions');
        $this->publishConfig('seo', 'settings');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();

        $this->registerCommands();

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('seo_sidebar', array_dot(trans('seo::sidebar')));
            $event->load('seo_table', array_dot(trans('seo::table')));
            $event->load('logs_permissions', array_dot(trans('seo::permissions.logs')));
            $event->load('seo_permissions', array_dot(trans('seo::permissions.seo')));
        });
    }

    /**
    * Register the console commands.
    */
    private function registerCommands()
    {
        $this->commands([
            GenerateSitemap::class,
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $folder = base_path('modules/Seo/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }

        foreach ($models as $model_name => $model) {
            $model_name = studly_case($model_name);

            $this->app->bind("Modules\Seo\Repositories\\{$model_name}Interface", function () use ($model_name, $model) {
                $eloquent_repository = "\Modules\Seo\Repositories\Eloquent\Eloquent{$model_name}Repository";
                $entity = "\Modules\Seo\Entities\\{$model_name}";
                $cache_decorator = "\Modules\Seo\Repositories\Cache\Cache{$model_name}Decorator";

                $repository = new $eloquent_repository(new $entity());

                if (!Config::get('app.cache') && !($model->no_cache ?? false)) {
                    return $repository;
                }

                return new $cache_decorator($repository);
            });
        }
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
