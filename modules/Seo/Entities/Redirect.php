<?php

namespace Modules\Seo\Entities;

use Laravel\Scout\Searchable;
use Modules\Core\Eloquent\Model;
use Modules\Users\Entities\Sentinel\User;

class Redirect extends Model
{
    use Searchable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $asYouType = true;

    protected $table = 'seo_redirect';

    protected static $entityNamespace = 'application/seo';

    protected $file_path = 'modules/Seo/Config/models/redirect.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['translations']);

        if ($this->translatedAttributes) {
            foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                foreach ($this->translatedAttributes as $translatedAttribute) {
                    $array[$translatedAttribute . '_' . $locale] = $this->translateOrNew($locale)->$translatedAttribute;
                }
            }
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    public function customUpdateOrCreate(array $data, $action_type = 'structure')
    {
        if ($this->getUrlAttributes($data['old_url']) == '/') {
            return;
        }

        if ($this->getUrlAttributes($data['old_url']) == $this->getUrlAttributes($data['new_url'])) {
            return;
        }

        // Find given new url in db in field old_url
        $new_url_is_old_url = $this->findByUrl($data['new_url'], 'old_url');
        // Find given old url in db in field new_url
        $old_url_is_new_url = $this->findByUrl($data['old_url'], 'new_url');

        // Check if given new url exist in db in field old_url and it the same as given new url and all same for old url
        // Delete it if true
        if ($new_url_is_old_url && $this->getUrlAttributes($new_url_is_old_url->old_url) == $this->getUrlAttributes($data['new_url']) && $this->getUrlAttributes($old_url_is_new_url->new_url) == $this->getUrlAttributes($data['old_url'])) {
            return $new_url_is_old_url->delete();
        }

        if (array_key_exists('action', $data)) {
            $action = $data['action'];
        } else {
            if ($action_type == 'structure') {
                $action = 1;
            } elseif ($action_type == 'file') {
                $action = 0;
            }
        }

        $user_id = \Sentinel::getUser()->id;
        $updated_at = \Carbon\Carbon::now()->toDateTimeString();
        $can_be_deleted = array_key_exists('can_be_deleted', $data) ? $data['can_be_deleted'] : 0;

        if ($model = $new_url_is_old_url) {
            $data_array = [
                'old_url' => $this->getUrlAttributes($data['old_url']),
                'new_url' => $model->new_url,
                'user_id' => $user_id,
                'action' => $action,
                'can_be_deleted' => $can_be_deleted,
                'updated_at' => $updated_at,
            ];

            self::create($data_array);
        }

        if ($old_url_is_new_url) {
            $old_url_is_new_url_all = $this->findAllByUrl($data['old_url'], 'new_url');

            foreach ($old_url_is_new_url_all as $model) {
                $model->new_url = $this->getUrlAttributes($data['new_url']);
                $model->user_id = $user_id;
                $model->can_be_deleted = $can_be_deleted;
                $model->updated_at = $updated_at;
                $model->save();
            }
        }

        if ($model = $this->findByUrl($data['old_url'], 'old_url')) {
            $model->new_url = $this->getUrlAttributes($data['new_url']);
            $model->user_id = $user_id;
            $model->can_be_deleted = $can_be_deleted;
            $model->updated_at = $updated_at;
            $model->save();
        } else {
            $data['user_id'] = $user_id;
            $data['action'] = $action;
            $data['can_be_deleted'] = $can_be_deleted;

            self::create($data);
        }
    }

    public function deleteIfExist(string $value)
    {
        if ($model = $this->findByUrl($value, 'old_url')) {
            return $model->delete();
        }
    }

    private function findByUrl(string $url, string $param)
    {
        return self::where($param, $this->getUrlAttributes($url))->first();
    }

    private function findAllByUrl(string $url, string $param)
    {
        return self::where($param, $this->getUrlAttributes($url))->get();
    }

    /**
     * Parse url by preg_replace and return valid url
     *
     * @param string $value
     *
     * @return string
     */
    private function getUrlAttributes(string $value)
    {
        $value = \LaravelLocalization::getNonLocalizedURL($value);
        $pattern = '/(http:\/\/|https:\/\/)(www\.)?(' . parse_url($value, PHP_URL_HOST) . '\/)([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/';
        $url = preg_replace($pattern, '$4', $value);

        if (ends_with($url, '/')) {
            $url = substr($url, 0, -1);
        }

        return str_start($url, '/');
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    // TODO Так сказала Аня
    // public function getOldUrlAttribute()
    // {
    //     if (starts_with($this->attributes['old_url'], '/')) {
    //         if (ends_with($this->attributes['old_url'], '/')) {
    //             return url('/') . substr($this->attributes['old_url'], 0, -1);
    //         }

    //         return url('/') . $this->attributes['old_url'];
    //     }

    //     return $this->attributes['old_url'];
    // }

    // TODO Так сказала Аня
    // public function getNewUrlAttribute()
    // {
    //     if (starts_with($this->attributes['new_url'], '/')) {
    //         if (ends_with($this->attributes['new_url'], '/')) {
    //             return url('/') . substr($this->attributes['new_url'], 0, -1);
    //         }

    //         return url('/') . $this->attributes['new_url'];
    //     }

    //     return $this->attributes['new_url'];
    // }

    public function getHeadersOfCollectionFor($template = 'default')
    {
        if ($template == 'default') {
            return [
                'old_url' => 'Старый УРЛ',
                'new_url' => 'Новый УРЛ',
            ];
        }

        if ($template == 'import_excel') {
            return [
                'old_url' => [
                    'title' => 'Старый УРЛ',
                    'validation' => 'required|url',
                ],
                'new_url' => [
                    'title' => 'Новый УРЛ',
                    'validation' => 'nullable|url',
                ],
            ];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setOldUrlAttribute($value)
    {
        $value = \LaravelLocalization::getNonLocalizedURL($value);
        $pattern = '/(http:\/\/|https:\/\/)(www\.)?(' . parse_url($value, PHP_URL_HOST) . '\/)([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/';
        $url = preg_replace($pattern, '$4', $value);

        if (ends_with($url, '/')) {
            $url = substr($url, 0, -1);
        }

        $this->attributes['old_url'] = str_start($url, '/');
    }

    public function setNewUrlAttribute($value)
    {
        $value = \LaravelLocalization::getNonLocalizedURL($value);
        $pattern = '/(http:\/\/|https:\/\/)(www\.)?(' . parse_url($value, PHP_URL_HOST) . '\/)([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/';
        $url = preg_replace($pattern, '$4', $value);

        if (ends_with($url, '/')) {
            $url = substr($url, 0, -1);
        }

        if ($url === $value) {
            $this->attributes['new_url'] = '/';
        } else {
            $this->attributes['new_url'] = str_start($url, '/');
        }
    }
}
