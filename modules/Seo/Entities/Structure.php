<?php

namespace Modules\Seo\Entities;

use Laravel\Scout\Searchable;
use Kalnoy\Nestedset\NodeTrait;
use Modules\Page\Entities\Page;
use Modules\Core\Eloquent\Model;
use Modules\Seo\Helpers\Templates;
use Modules\Core\Scopes\DraftableScope;
use Modules\Ecommerce\Entities\Product;
use Modules\Core\Eloquent\NestedBuilder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Users\Traits\ValidateUserByRoleAccess;
use Modules\Core\Internationalisation\Translatable;

class Structure extends Model
{
    use Translatable,
        Searchable,
        ValidateUserByRoleAccess,
        NodeTrait {
            NodeTrait::usesSoftDelete insteadof Searchable;
        }
    // use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $asYouType = true;

    protected $table = 'seo_structure';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/seo';

    protected $file_path = 'modules/Seo/Config/models/structure.json';

    protected $types = ['structure', 'store', 'manual'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        static::created(function ($obj) {
            (new Redirect)->deleteIfExist(url('/') . $obj->url);
        });

        static::deleting(function ($obj) {
            $obj->children->each(function ($child) {
                $child->delete();
            });
        });

        parent::boot();
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['translations']);

        if ($this->translatedAttributes) {
            foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                foreach ($this->translatedAttributes as $translatedAttribute) {
                    $array[$translatedAttribute . '_' . $locale] = $this->translateOrNew($locale)->$translatedAttribute;
                }
            }
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    public function getMetaTemplate(string $page_template, string $type, array $ids)
    {
        $tags = Metatag::where('type', $type)
            ->where('page_template', $page_template)
            ->where('status', 1)
            ->where(function ($q) use ($ids) {
                $q->when(count($ids), function ($q) use ($ids) {
                    $q->whereIn('structure_id', $ids);
                })
                ->orWhereNull('structure_id');
            })->get();

        if (!$tags->count()) {
            return false;
        }

        foreach ($ids as $id) {
            $tag = $tags->first(function ($item) use ($id) {
                return $item->structure_id == $id;
            });

            if ($tag) {
                return $tag;
            }
        }

        $tag = $tags->first(function ($item) {
            return $item->structure_id == null;
        });

        return $tag ? $tag : false;
    }

    /**
     * return all parent ids with current template
     *
     * @return array
     */
    public function getBranchIdsByType(string $page_template, int $parent_id)
    {
        $template = (new Templates)->getTemplate($page_template);
        $result = [];

        //template can have needed parents
        // if ($template['needs_index'] ?? false || $page_template == 'default') {
        $parent = $this->where('id', $parent_id)->withoutGlobalScope(DraftableScope::class)->first();

        while ($parent && $parent->page_template == $page_template) {
            $result[] = $parent->id;
            $parent = $parent->parent()->withoutGlobalScope(DraftableScope::class)->first();
        }
        // }
        return $result;
    }

    /**
     * Update only url in this model
     */
    public function updateUrl($url = false)
    {
        $url = $url ?: $this->parent->url;

        $this->update(['url' => $url . '/' . $this->slug]);
    }

    public function updateChildrenStatuses()
    {
        if ($this->status == 0 || $this->published == 0) {
            $published = 0;
        } elseif ($this->status == 1 && $this->published == 1) {
            $published = 1;
        } else {
            $published = 2;
        }

        $this->children->each(function ($child) use ($published) {
            $child->update(['published' => $published]);
        });
    }

    /**
     * For admin bar in view get attached item to structure
     */
    public function getEditUrl(): string
    {
        $template = (new Templates())->getTemplate($this->page_template);

        return route($template['edit_path'], $this->object_id ?? $this->page_id);
    }

    /**
     * For admin bar in view
     */
    public function getSelfEditUrl($lang = null): string
    {
        return \LaravelLocalization::getLocalizedURL($lang, route('backend.seo_structure.edit', $this->id));
    }

    /**
     * Gets all variables from string and tries to replace them with data
     *
     * @param string $string
     * @return string
     */
    public function parseTemplate($string)
    {
        preg_match_all('/{{([a-zA-Z_]+)}}/', $string, $variables);

        foreach ($variables[1] as $variable) {
            $string = str_replace('{{' . $variable . '}}', $this->$variable ?? '', $string);
        }

        return $string;
    }

    /**
     * find children models and update structure of it
     *
     * @return void
     */
    public function updateChildren()
    {
        if ($children = $this->children) {
            $children->each(function ($child) {
                $model = $child->object_id ? (new Templates())->getModel($child->page_template) : Page::class;

                $model::when($model == Page::class, function ($query) use ($child) {
                    return $query->where('id', $child->page_id);
                }, function ($query) use ($child) {
                    return $query->where('id', $child->object_id);
                })->first()->updateSeoStructure();
            });
        }
    }

    public function dropChildrenWithRedirect($url = false)
    {
        $url = $url ?: ($this->parent->url ?? '');

        foreach ($this->children as $child) {
            $child->dropChildrenWithRedirect($url);
        }

        (new Redirect)->customUpdateOrCreate([
            'old_url' => url('/') . $this->url,
            'new_url' => url('/') . $url,
            'action' => 2,
        ]);

        return $this;
    }

    public static function getForSitemap($parent_id = null)
    {
        $structures = self::where('to_index', 1)
            ->where('parent_id', $parent_id)
            ->where('to_sitemap', 1)
            ->where('status', 1)
            ->select('id', 'weight', 'url', 'frequency', 'parent_id')
            ->get();

        foreach ($structures as $structure) {
            $structures = $structures->merge(self::getForSitemap($structure->id));
        }

        return $structures;
    }

    protected function canBeUpdated($metatag)
    {
        if ($this->individual_tags > 0) {
            return false;
        }

        if ($this->page_template != $metatag->page_template) {
            $sub_tempaltes = (new Templates)->isCombo($metatag->page_template);

            return is_array($sub_tempaltes) ? in_array($this->page_template, $sub_tempaltes) : false;
        }

        return true;
    }

    public function updateMetatag($metatag)
    {
        if ($this->canBeUpdated($metatag)) {
            $data = $metatag->translations
            ->filter(function ($item) {
                return $item->template;
            })->mapWithKeys(function ($item) use ($metatag) {
                return [
                    $item->locale => [
                        $metatag->type => $item->template
                    ]
                ];
            })->toArray();

            $this->update($data);
        }

        if ($metatag->structure_id) {
            $this->updateChildrenMetatag($metatag);
        }
    }

    public function updateChildrenMetatag($metatag)
    {
        $this->children()
            ->withoutGlobalScope(DraftableScope::class)
            ->get()
            ->each(function ($item) use ($metatag) {
                $item->updateMetatag($metatag);
            });
    }

    public function getTitleWithChildren($carry = false)
    {
        if ($this->slug != 'fantom_storepage_u_cant_see') {
            $carry = $carry ? ($this->title . ' / ' . $carry) : $this->title;
        }

        if ($this->parent_id > 0) {
            $carry = $this->parent->getTitleWithChildren($carry);
        }

        return $carry;
    }

    public function getProduct()
    {
        return $this->product()->whereHas('versions')->with('translations')->firstOrFail();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function parent()
    {
        return $this->belongsTo(Structure::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Structure::class, 'parent_id')->with('children');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'object_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopePublished($query)
    {
        $query->when($this->userDeveloper() || $this->userAdmin(), function ($query) {
            $query->whereIn('published', [1, 2]);
        }, function ($query) {
            $query->where('published', 1);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getPriceAttribute()
    {
        if ($this->object_id && $this->model == Product::class) {
            return $this->product->discounted_price ?? '';
        }

        return '';
    }

    public function getParentTitleAttribute()
    {
        return $this->parent->title ?? '';
    }

    public function getUltimateH1TitleAttribute()
    {
        return $this->parseTemplate($this->h1_title ?: $this->title);
    }

    public function getUltimateMetaTitleAttribute()
    {
        $addition = $this->paginationPage ? trans('frontend::seo.pagination.page') . '-' . $this->paginationPage : '';

        return $this->parseTemplate($this->meta_title ?: $this->ultimate_h1_title) . ' ' . $addition;
    }

    public function getUltimateMetaDescriptionAttribute()
    {
        $addition = $this->paginationPage ? trans('frontend::seo.pagination.page') . '-' . $this->paginationPage : '';

        return $this->parseTemplate($this->meta_description ? $this->meta_description . ' ' . $addition : $this->ultimate_meta_title);
    }

    public function getUltimateOgTitleAttribute()
    {
        return $this->parseTemplate($this->og_title ?: $this->ultimate_meta_title);
    }

    public function getUltimateOgDescriptionAttribute()
    {
        return $this->parseTemplate($this->og_description ?: ($this->og_title ?: $this->ultimate_meta_description));
    }

    public function getIndexFollowTagAttribute()
    {
        return '<meta name="robots" content="' . ($this->to_index ? '' : 'no') . 'index, ' . ($this->follow ? '' : 'no') . 'follow" />';
    }

    public function getUltimateSeoTextAttribute()
    {
        return !count(request()->all()) && $this->seo_text ? $this->seo_text : '';
    }

    public function getUltimateCanonicalAttribute()
    {
        $path = $this->canonical ?? url()->current();

        if ($this->paginationPage) {
            $path = str_replace('page-' . $this->paginationPage, '', trim($path, '/'));
        }

        return $path;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setCanonicalAttribute($value)
    {
        $this->attributes['canonical'] = str_start(parse_url($value)['path'] ?? '', '/');
    }

    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = str_start(parse_url(ltrim($value, '/'))['path'] ?? '', '/');
    }

    /**
     * Changes builder to custom
     *
     * @param [type] $query
     *
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new NestedBuilder($query);
    }
}
