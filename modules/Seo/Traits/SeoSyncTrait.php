<?php

namespace Modules\Seo\Traits;

use Modules\Seo\Helpers\StructureSync;

trait SeoSyncTrait
{
    protected static function bootSeoSyncTrait()
    {
        static::created(function ($obj) {
            (new StructureSync($obj))->create();
        });

        static::updated(function ($obj) {
            (new StructureSync($obj))->update();
        });

        static::deleting(function ($obj) {
            (new StructureSync($obj))->delete();
        });
    }

    public static function seoAddChildrenToStructure()
    {
        return;
    }

    public function parentStructure()
    {
        return;
    }
}
