<?php

namespace Modules\Seo\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface StructureInterface extends CoreInterface
{
    public function findByPath($path);

    public function updateWithModel($model);
}
