<?php

namespace Modules\Seo\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface RedirectInterface extends CoreInterface
{
    /**
     * Validate request data and save it or update, if given data exist in db, or through notify error to frontend
     *
     * @param array $data
     *
     * @return Response
     */
    public function createOrUpdate(array $data);

    /**
     * @param string $value
     *
     * @return mixed
     */
    public function findByUrlForPublicMiddleware(string $data);
}
