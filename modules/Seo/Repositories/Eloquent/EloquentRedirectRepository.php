<?php

namespace Modules\Seo\Repositories\Eloquent;

use Modules\Seo\Entities\Structure;
use Nwidart\Modules\Facades\Module;
use Modules\Seo\Repositories\RedirectInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentRedirectRepository extends EloquentCoreRepository implements RedirectInterface
{
    public $model_config = 'modules/Seo/Config/models/redirect.json';

    /**
     * @var \Modules\Seo\Entities\Redirect
     */
    protected $model;

    /**
     * @param \Modules\Seo\Entities\Redirect $model
     */
    public function __construct($model)
    {
        parent::__construct($model);
    }

    /**
     * Validate request data and save it or update, if given data exist in db, or through notify error to frontend
     *
     * @param array $data
     *
     * @return Response
     */
    public function createOrUpdate(array $data)
    {
        if ($this->getUrlAttributes($data['old_url']) == '/') {
            return response()->json([
                'errors' => true,
                'message' => trans('seo::messages.repository.url is domain'),
            ], 422);
        }

        if ($this->getUrlAttributes($data['old_url']) == $this->getUrlAttributes($data['new_url'])) {
            return response()->json([
                'errors' => true,
                'message' => trans('seo::messages.repository.url the same'),
            ], 422);
        }

        $new_url_is_old_url = $this->findByUrl($data['new_url'], 'old_url');
        $old_url_is_new_url = $this->findByUrl($data['old_url'], 'new_url');

        if ($new_url_is_old_url && $new_url_is_old_url->old_url == $this->getUrlAttributes($data['new_url']) && $old_url_is_new_url && $old_url_is_new_url->new_url == $this->getUrlAttributes($data['old_url'])) {
            return $new_url_is_old_url->delete();
        }

        $action = 0;
        $user_id = \Sentinel::getUser()->id;
        $updated_at = \Carbon\Carbon::now()->toDateTimeString();
        $can_be_deleted = array_key_exists('can_be_deleted', $data) ? $data['can_be_deleted'] : 0;

        if ($model = $new_url_is_old_url) {
            $data_array = [
                'old_url' => $this->getUrlAttributes($data['old_url']),
                'new_url' => $model->new_url,
                'user_id' => $user_id,
                'action' => $action,
                'can_be_deleted' => $can_be_deleted,
                'updated_at' => $updated_at,
            ];

            $redirect = $this->model->create($data_array);
        }

        if ($old_url_is_new_url) {
            $old_url_is_new_url_all = $this->findAllByUrl($data['old_url'], 'new_url');

            foreach ($old_url_is_new_url_all as $model) {
                $model->new_url = $this->getUrlAttributes($data['new_url']);
                $model->user_id = $user_id;
                $model->can_be_deleted = $can_be_deleted;
                $model->updated_at = $updated_at;
                $model->save();
            }
        }

        if ($model = $this->findByUrl($data['old_url'], 'old_url')) {
            $model->new_url = $this->getUrlAttributes($data['new_url']);
            $model->user_id = $user_id;
            $model->can_be_deleted = $can_be_deleted;
            $model->updated_at = $updated_at;
            $model->save();

            return response()->json([
                'errors' => false,
                'message' => trans('core::messages.api.fields.updated', ['field' => trans('seo::models.of.redirect')]),
                'id' => $model->id
            ]);
        } else {
            $data['action'] = $action;

            $redirect = $this->model->create($data);

            if ((new Structure)->select('url')->where('url', $this->getUrlAttributes($data['old_url']))->first()) {
                return response()->json([
                    'errors' => false,
                    'type' => 'warning',
                    'message' => trans('core::messages.api.fields.created', ['field' => trans('seo::models.of.redirect')]) . ' ' . trans('seo::messages.repository.url in the structure'),
                    'id' => $redirect->id
                ]);
            }

            return response()->json([
                'errors' => false,
                'message' => trans('core::messages.api.fields.created', ['field' => trans('seo::models.of.redirect')]),
                'id' => $redirect->id
            ]);
        }
    }

    private function findByUrl(string $value, string $param)
    {
        return $this->model->where($param, $this->getUrlAttributes($value))->first();
    }

    private function findAllByUrl(string $url, string $param)
    {
        return $this->model->where($param, $this->getUrlAttributes($url))->get();
    }

    /**
     * @param string $value
     *
     * @return mixed
     */
    public function findByUrlForPublicMiddleware(string $value)
    {
        if (Module::find('seo')->active === 0) {
            return;
        }

        return $this->model->select('new_url')->where('old_url', $this->getUrlAttributes($value))->first();
    }

    /**
     * Parse url by preg_replace and return valid url
     *
     * @param string $value
     *
     * @return string
     */
    private function getUrlAttributes(string $value)
    {
        $value = \LaravelLocalization::getNonLocalizedURL($value);
        $pattern = '/(http:\/\/|https:\/\/)(www\.)?(' . parse_url($value, PHP_URL_HOST) . '\/)([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/';
        $url = preg_replace($pattern, '$4', $value);

        if ($url === $value) {
            return '/';
        }

        if (ends_with($url, '/')) {
            $url = substr($url, 0, -1);
        }

        return str_start($url, '/');
    }

    /**
     * @param $columns
     *
     * @return mixed
     */
    public function selectListForExportExcel($columns)
    {
        return [
            'old_url' => 'old_url',
            'new_url' => 'new_url',
        ];
    }
}
