<?php

namespace Modules\Seo\Repositories\Eloquent;

use Modules\Page\Entities\Page;
use Modules\Seo\Entities\Metatag;
use Modules\Seo\Helpers\Templates;
use Modules\Seo\Entities\Structure;
use Modules\Core\Scopes\DraftableScope;
use Modules\Seo\Repositories\StructureInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentStructureRepository extends EloquentCoreRepository implements StructureInterface
{
    public $model_config = 'modules/Seo/Config/models/structure.json';

    protected $templates;

    public function __construct(Structure $model)
    {
        parent::__construct($model);

        $this->templates = new Templates();
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $path
     *
     * @return \Modules\Page\Entities\Page
     */
    public function findByPath($path)
    {
        $exploded = explode('/', $path);
        $filter = false;
        $to_match = array_pop($exploded);

        if (!preg_match('/([^pbc\_\d])|\d([pbc])|_{2,}|_$|^_/', $to_match)) {
            $filterParsed = explode('_', $to_match);

            if (is_sorted_array($filterParsed) && count($filterParsed) > 1) {
                $path = implode('/', $exploded);
                request()->merge(['filterParsed' => $filterParsed]);
                request()->merge(['to_match' => $to_match]);
            }
        } elseif (preg_match('/^([-a-z0-9]+)_([-a-z0-9]+)$/', $to_match)) {
            $path = implode('/', $exploded);
            request()->merge(['filterParsed' => $to_match]);
            request()->merge(['to_match' => $to_match]);
        }

        request()->merge(['noFilterPath' => $path]);

        $path = str_start($path, '/');

        $structure = $this->model
            ->where('url', $path)
            ->published()
            ->listsAllTranslations()
            ->orderByDesc('depth')
            ->first();

        if ($structure) {
            return [$structure, ''];
        }

        $templates = (new Templates)->getSemiPhantomProductsTemplates();

        $sub_structure = $this->model
            ->whereIn('page_template', $templates)
            ->select('url', 'page_template')
            ->published()
            ->get()
            ->filter(function ($item) use ($path) {
                return strpos($path, $item->url) === 0;
            })
            ->first();

        if (!optional($sub_structure)->url) {
            abort(404);
        }

        return [$this->model
            ->where('url', $sub_structure->url)
            ->with('parent', 'translations', 'page.translations')
            ->first(),
            str_replace($sub_structure->url, '', $path),
        ];
    }

    public function drop($model)
    {
        $class = get_class($model);

        $structure = $model->structure;

        if ($structure) {
            $structure->dropChildrenWithRedirect()->delete();
        }
    }

    public function getSelectList()
    {
        return $this->model->whereIsRoot()->with('children', 'translations')->translated()->get()->flatten()->toArray();
    }

    /**
     * Create structure with data
     *
     * @param array $data
     *
     * @return Structure
     */
    public function create($data)
    {
        try {
            if (!isset($data['page_template'])) {
                $data['type'] = 'manual';
                $data['url'] = str_start($data['url'], '/');
                $data['page_template'] = 'manual';

                $referer = explode('/', $data['url']);
                array_pop($referer);

                $data['parent_id'] = $this->model->where('url', implode('/', $referer))->select('id')->first()->id ?? null;
            } elseif (in_array($data['page_template'], ['products', 'product'])) {
                $data['type'] = 'store';
            }

            if ($data['individual_tags'] ?? false) {
                return $this->model->create($data); //bad?
            }

            return $this->tryCreateWithMetatag($data);
        } catch (\Exception $e) {
            dump($e);
        }
    }

    /**
     * Find metatag and try to apply it to this creating structure
     *
     * @param array $data
     *
     * @return Structure
     */
    public function tryCreateWithMetatag(array $data)
    {
        $template = (new Templates)->getTemplate($data['page_template']);
        $ids = $this->model->getBranchIdsByType($data['page_template'], $data['parent_id'] ?? null);

        if ($template['og_type'] ?? false) {
            $data['og_type'] = $template['og_type'];
        }

        $h1_title = $this->model->getMetaTemplate($data['page_template'], 'h1_title', $ids);
        $meta_description = $this->model->getMetaTemplate($data['page_template'], 'meta_description', $ids);
        $meta_title = $this->model->getMetaTemplate($data['page_template'], 'meta_title', $ids);

        $locales = array_keys(get_application_frontend_locales());

        foreach ($locales as $locale) {
            $data[$locale]['h1_title'] = isset($data[$locale]['h1_title']) ? $data[$locale]['h1_title'] : ($h1_title ? $h1_title->givenTranslate($locale)->template : '');
            $data[$locale]['meta_description'] = isset($data[$locale]['meta_description']) ? $data[$locale]['meta_description'] : ($meta_description ? $meta_description->givenTranslate($locale)->template : '');
            $data[$locale]['meta_title'] = isset($data[$locale]['meta_title']) ? $data[$locale]['meta_title'] : ($meta_title ? $meta_title->givenTranslate($locale)->template : '');

            if (!isset($data[$locale]['title']) && !isset($data[$locale]['h1_title']) && !isset($data[$locale]['meta_description']) && !isset($data[$locale]['meta_description'])) {
                unset($data[$locale]);
            }
        }

        return $this->model->create($data); //bad?
    }

    /**
     * {@inheritdoc}
     */
    public function createWithModel($model)
    {
        list($structure, $url, $ids, $template) = $this->saveDataInitParts($model);

        if ($model->getClass() !== Page::class) {
            if (!$structure) {
                return false;
            }
        }

        $data = [
            'model' => $model->getClass(),
            'parent_id' => $structure->id ?? null,
            'slug' => $model->slug,
            'page_template' => $template['name'],
            'url' => $url,
            'weight' => $template['weight'],
            'frequency' => $template['frequency'],
            'type' => $template['type'] ?? 'structure'
        ];

        if (isset($ids['page_id']) && isset($ids['object_id']) && $ids['page_id'] && $ids['object_id']) {
            $object_model_status = $template['model']::where('id', $ids['object_id'])->value('status') ?? false;

            if ($object_model_status !== false) {
                if ($object_model_status == 0 || $model->status == 0) {
                    $data['status'] = 0;
                } elseif ($object_model_status == 2 || $model->status == 2) {
                    $data['status'] = 2;
                } else {
                    $data['status'] = 1;
                }
            } else {
                $data['status'] = $model->status;
            }
        } else {
            $data['status'] = $model->status;
        }

        foreach ($model->translations as $translation) {
            $data[$translation->locale] = [
                'title' => $translation->title
            ];
        }
        $data = array_merge($ids, $data);

        $data = $this->getDataPublished($model, $data);

        $structure = $this->model->create($data);
        $templates = new Templates($structure->page_template);

        if (!$templates->isSimplePage() && $structure->model) {
            $model = $structure->object_id ? $templates->getModel() : Page::class;

            if ($model) {
                $model::seoAddChildrenToStructure($structure);
            }
        }

        return $structure;
    }

    /**
     * {@inheritdoc}
     */
    public function updateWithModel($model)
    {
        if ($model->structure && $model->getClass() !== $model->structure->model) {
            $object_model = $model;
            $model = $model->structure->page;
        }

        list($structure, $url, $ids, $template) = $this->saveDataInitParts($model);

        if ($model->getClass() !== Page::class) {
            if (!$structure) {
                return $this->drop($model);
            }
        }

        $data = [
            'parent_id' => $structure->id ?? null,
            'page_template' => $template['name'],
            'type' => $template['type'] ?? 'structure'
        ];

        if ($model->getClass() === $model->structure->model) {
            $data['slug'] = $model->slug;
            $data['url'] = $url;
        }

        if (isset($object_model) && $object_model->status && $model->status) {
            if ($object_model->status == 2 || $model->status == 2) {
                $data['status'] = 2;
            } else {
                $data['status'] = 1;
            }
        } elseif (isset($object_model)) {
            $data['status'] = 0;
        } else {
            $data['status'] = $model->status;
        }

        $data = array_merge($ids, $data);

        $translations = $model->translations->mapWithKeys(function ($translation) {
            return [
                $translation->locale => [
                        'title' => $translation->title
                    ]
                ];
        })
         ->toArray();

        $current_structure = $model->structure;

        $page_template_updated = $current_structure->page_template !== $data['page_template'];

        $this->update($current_structure, array_merge($translations, $data));
        $templates = new Templates($current_structure->page_template);

        if (!$templates->isSimplePage() && $structure->model && $page_template_updated) {
            $model = $current_structure->object_id ? $templates->getModel() : Page::class;

            if ($model) {
                $model::seoAddChildrenToStructure($current_structure);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function update($model, $data)
    {
        $data = $this->getDataPublished($model, $data);
        $post_update_needs = $this->postUpdateNeeds($model, $data);

        parent::update($model, $data);

        $children = $model->children;

        foreach ($children as $key => $child) {
            if (
                    $post_update_needs['children']['deleteNotDefault'] &&
                    $child->page_template !== 'default'
                ) {
                $child->delete();
                $children->forget($key);

                continue;
            }

            if ($post_update_needs['children']['updateUrl']) {
                $children_data[$child->id]['url'] = $model->url . '/' . $child->slug;
            }

            if ($post_update_needs['children']['updateStatus']) {
                $children_data[$child->id]['published'] = $this->getPublishedByStatus($model);
            }

            if (isset($children_data)) {
                $this->update($child, $children_data[$child->id]);
            }
        }

        return $model;
    }

    /**
     * FInd all structures fitted to metatag template and update
     *
     * @param Metatag $metatag
     *
     * @return bool
     */
    public function updateMetatags(Metatag $metatag)
    {
        try {
            $this->model
                ->withoutGlobalScope(DraftableScope::class)
                ->when($metatag->structure_id, function ($query) use ($metatag) {
                    return $query->where('id', $metatag->structure_id);
                }, function ($query) use ($metatag) {
                    return $query->when(
                        $sub_tempaltes = (new Templates)->isCombo($metatag->page_template),
                        function ($query) use ($sub_tempaltes) {
                            return $query->whereIn('page_template', $sub_tempaltes);
                        },
                        function ($query) use ($sub_tempaltes, $metatag) {
                            return $query->where('page_template', $metatag->page_template);
                        }
                    )->where('individual_tags', 0);
                })
                ->get()
                ->each(function ($structure) use ($metatag) {
                    $structure->updateMetatag($metatag);
                });

            return true;
        } catch (\Exception $e) {
            dd($e);

            return false;
        }
    }

    /**
     * Tab name for id
     *
     * @param integer $id
     *
     * @return string
     */
    public function getTab(int $id)
    {
        return $this->model->where('id', $id)->select('type')->first()->type;
    }

    public function frequencies()
    {
        return [
            'always' => 'Always',
            'hourly' => 'Hourly',
            'daily' => 'Daily',
            'weekly' => 'Weekly',
            'monthly' => 'Monthly',
            'yearly' => 'Yearly',
            'never' => 'Never'
        ];
    }

    /**
     * @param $model
     * @return array
     */
    public function saveDataInitParts($model): array
    {
        $structure = $model->parentStructure();
        $page_template = $model->page_template;

        if ($model->getClass() !== Page::class && !$structure) {
            return [false, false, false, false];
        }

        if (is_array($page_template)) {
            $page_template = $page_template[array_search($structure->page_template, $page_template)] ?? (
                $page_template[array_search(str_singular($structure->page_template), $page_template)] ?? 'default'
            );
        }

        $templates = new Templates($page_template);
        $template = $templates->getTemplate();

        $url = $templates->getUrl($structure, $model->slug);
        if ($model->getClass() == Page::class) {
            $object_id = ($template['needs_index'] ?? false) ? ($model->relation ?? null) : null;

            $ids = [
                'page_id' => $model->id,
                'object_id' => $object_id,
            ];
        } else {
            $ids = [
                'object_id' => $model->id
            ];
        }

        return [$structure, $url, $ids, $template];
    }

    /**
     * @param $model
     * @param $data
     * @return array
     */
    public function getDataPublished($model, $data): array
    {
        if (isset($data['parent_id']) && ($data['parent_id'] ?? 0) > 0) {
            $new_parent = $this->model->where('id', $data['parent_id'])->select('status', 'published')->first();

            $data['published'] = $this->getPublishedByStatus($new_parent);
        } elseif (array_key_exists('parent_id', $data) && is_null($data['parent_id'])) {
            $data['published'] = $data['status'];
        }

        return $data;
    }

    /**
     * @param $model
     * @param $data
     *
     * @return array
     */
    public function childrenNeedUpdateUrl($model, $data)
    {
        return (isset($data['parent_id']) && $data['parent_id'] !== $model->parent_id) ||
            (isset($data['slug']) && $data['slug'] !== $model->slug) ||
            (isset($data['url']) && $data['url'] !== $model->url);
    }

    /**
     * @param $model
     * @param $data
     *
     * @return array
     */
    public function notDefaultChildrenNeedToBeDeleted($model, $data)
    {
        return isset($data['page_template']) && $data['page_template'] !== $model->page_template;
    }

    /**
     * @param $model
     *
     * @param $data
     * @return bool
     */
    public function childrenNeedUpdateOfStatus($model, $data)
    {
        return (isset($data['slug']) && ((int)$data['status'] !== (int)$model->status)) ||
        (isset($data['published']) && ((int)$data['published'] !== (int)$model->published));
    }

    /**
     * @param $model
     * @param $data
     *
     * @return array|bool
     */
    public function childrenTemplatesNeedProtectionFromDelete($model, $data)
    {
        switch ($model->page_template) {
            case 'articles':
            case 'article':
            case 'news':
            case 'new':
                return ['articles', 'article', 'news', 'new'];
            default:
                return false;
        }
    }

    /**
     * @param $model
     * @return int
     */
    public function getPublishedByStatus($model): int
    {
        if ($model->status == 0 || $model->published == 0) {
            $published = 0;
        } elseif ($model->status == 1 && $model->published == 1) {
            $published = 1;
        } else {
            $published = 2;
        }

        return $published;
    }

    /**
     * @param $model
     * @param $data
     * @return array
     */
    public function postUpdateNeeds($model, $data): array
    {
        $deleteNotDefault = $this->notDefaultChildrenNeedToBeDeleted($model, $data);

        return [
            'children' => [
                'updateUrl' => $this->childrenNeedUpdateUrl($model, $data),
                'updateStatus' => $this->childrenNeedUpdateOfStatus($model, $data),
                'deleteNotDefault' => $deleteNotDefault,
                // 'protectedFromDelete' => $deleteNotDefault ? $this->childrenTemplatesNeedProtectionFromDelete($model, $data) : false,
            ]
        ];
    }
}
