<?php

namespace Modules\Seo\Repositories\Eloquent;

use Modules\Seo\Entities\Metatag;
use Modules\Seo\Helpers\Templates;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Repositories\MetatagInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentMetatagRepository extends EloquentCoreRepository implements MetatagInterface
{
    public $model_config = 'modules/Seo/Config/models/metatag.json';

    public $structure;
    public $templates;

    public function __construct(Metatag $model)
    {
        parent::__construct($model);

        $this->structure = new EloquentStructureRepository(new Structure);
        $this->templates = new Templates();
    }

    public function structures()
    {
        return $this->structure->getSelectList();
    }

    public function templates()
    {
        return $this->templates->getAllList();
    }

    public function types()
    {
        return [
            'h1_title' => 'h1',
            'meta_description' => 'meta description',
            'meta_title' => 'meta title'
        ];
    }

    /**
     * apply metatag to all fitted structures
     *
     * @param int $metatag
     * @return bool
     */
    public function apply(int $metatag)
    {
        $metatag = $this->model->where('id', $metatag)->where('status', 1)->where('page_template', '!=', 'filters')->first();

        return $metatag ? $this->structure->updateMetatags($metatag) : false;
    }
}
