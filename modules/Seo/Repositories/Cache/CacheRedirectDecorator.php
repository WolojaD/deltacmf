<?php

namespace Modules\Seo\Repositories\Cache;

use Modules\Seo\Repositories\RedirectInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheRedirectDecorator extends CoreCacheDecorator implements RedirectInterface
{
    /**
     * @var RedirectInterface
     */
    protected $repository;

    public function __construct(RedirectInterface $redirect)
    {
        parent::__construct();

        $this->entityName = 'seo_redirect';
        $this->repository = $redirect;
    }

    /**
     * Validate request data and save it or update, if given data exist in db, or through notify error to frontend
     *
     * @param array $data
     *
     * @return Response
     */
    public function createOrUpdate($data)
    {
        return $this->remember(function () use ($data) {
            return $this->repository->createOrUpdate($data);
        });
    }

    /**
     * @param string $value
     *
     * @return mixed
     */
    public function findByUrlForPublicMiddleware(string $data)
    {
        return $this->remember(function () use ($data) {
            return $this->repository->findByUrlForPublicMiddleware($data);
        });
    }
}
