<?php

namespace Modules\Seo\Repositories\Cache;

use Modules\Seo\Repositories\StructureInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheStructureDecorator extends CoreCacheDecorator implements StructureInterface
{
    /**
     * @var StructureInterface
     */
    protected $repository;

    public function __construct(StructureInterface $seo)
    {
        parent::__construct();

        $this->entityName = 'seo_structure';
        $this->repository = $seo;
    }

    public function findByPath($path)
    {
        return $this->repository->findByPath($path);
    }

    public function updateWithModel($model)
    {
        return $this->repository->updateWithModel($model);
    }
}
