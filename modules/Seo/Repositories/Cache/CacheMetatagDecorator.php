<?php

namespace Modules\Seo\Repositories\Cache;

use Modules\Seo\Repositories\MetatagInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheMetatagDecorator extends CoreCacheDecorator implements MetatagInterface
{
    /**
     * @var MetatagInterface
     */
    protected $repository;

    public function __construct(MetatagInterface $seo)
    {
        parent::__construct();

        $this->entityName = 'seo_metatags';
        $this->repository = $seo;
    }
}
