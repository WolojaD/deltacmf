<?php

namespace Modules\Seo\Database\Seeders;

// use Modules\Seo\Entities\Seo;
use Illuminate\Database\Seeder;
use Modules\Seo\Entities\Metatag;
use Modules\Seo\Entities\Redirect;

class SeoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            // factory(Seo::class, 5)->create();
            // factory(Redirect::class, 50)->create();

            Metatag::create([
                'page_template' => 'default',
                'name' => 'test',
                'type' => 'h1_title',
                'template' => 'parent title - "{{parent_title}}" title - "{{title}}" price - "{{price}}"'
            ]);

            Metatag::create([
                'page_template' => 'product',
                'name' => 'product test',
                'type' => 'h1_title',
                'template' => 'parent title - "{{parent_title}}" title - "{{title}}" price - "{{price}}"'
            ]);
        }
    }
}
