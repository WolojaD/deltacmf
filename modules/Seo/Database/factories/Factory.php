<?php

use Faker\Generator as Faker;
// use Modules\Seo\Entities\Seo;
use Modules\Seo\Entities\Redirect;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $factory->define(Seo::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });

$factory->define(Redirect::class, function (Faker $faker) {
    return [
        'old_url' => $faker->url,
        'new_url' => $faker->url,
        'user_id' => $faker->numberBetween(1, 5),
    ];
});
