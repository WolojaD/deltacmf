<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::backendRouteList('Seo');

Route::get('go-to-sitemap', ['as' => 'backend.seo_structure.index', 'uses' => 'SeoController@goToSitemap', 'middleware' => 'can:seo.structure.index']);

Route::group(['prefix' => 'seo/robots'], function () {
    Route::get('/', ['as' => 'backend.seo_robots.index', 'uses' => 'SeoController@index', 'middleware' => 'can:seo.robots.index']);
});
