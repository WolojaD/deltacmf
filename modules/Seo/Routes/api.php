<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Seo');

Route::group(['prefix' => '/seo/structure', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::post('generate-sitemap', ['as' => 'api.backend.seo_structure.generateSitemap', 'uses' => 'StructureController@generateSitemap', 'middleware' => 'token-can:seo.structure.edit']);
});

Route::group(['prefix' => '/seo/metatags', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::post('apply/{ident}', ['as' => 'api.backend.seo_metatag.apply', 'uses' => 'MetatagController@apply', 'middleware' => 'token-can:seo.metatag.edit']);
});

Route::group(['prefix' => '/seo/robots', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.seo_robots.getRobots', 'uses' => 'RobotsController@index', 'middleware' => 'token-can:seo.robots.index']);

    Route::post('/', ['as' => 'api.backend.seo_robots.postRobots', 'uses' => 'RobotsController@store', 'middleware' => 'token-can:seo.robots.edit']);
});
