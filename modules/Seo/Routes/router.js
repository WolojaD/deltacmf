import helpers from '@services/helpers'

const tabs = [
    {
        name: 'structure',
        label: 'seo_sidebar.submenu.structure',
        permission: 'seo.structure.index',
        routeName: 'api.backend.seo_structure',
    },
    {
        name: 'store',
        label: 'seo_sidebar.submenu.store',
        permission: 'seo.structure.index',
        routeName: 'api.backend.seo_structure',
        params: {
            tab: "store"
        }
    },
    {
        name: 'manual',
        label: 'seo_sidebar.submenu.manual',
        permission: 'seo.structure.index',
        routeName: 'api.backend.seo_structure',
        params: {
            tab: "manual"
        }
    }
]

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/seo/structure',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':tab([a-z]+)?',
                name: 'api.backend.seo_structure',
                component: require('@/views/crud/index').default,
                props: {
                    tabs
                },
                meta: {
                    pageTitle: 'Structure',
                    breadcrumb: [
                        { name: 'Structure' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.seo_structure.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Structure Edit',
                    breadcrumb: [
                        { name: 'Structure', link: 'api.backend.seo_structure' },
                        { name: 'Structure Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.seo_structure.show',
                component: require('@/views/crud/index').default,
                props: {
                    tabs
                },
                meta: {
                    pageTitle: 'Structure',
                    breadcrumb: [
                        { name: 'Structure' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.seo_structure.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Structure Create',
                    breadcrumb: [
                        { name: 'Structure', link: 'api.backend.seo_structure' },
                        { name: 'Structure Create' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/seo/metatags',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.seo_metatag',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Metatag',
                    breadcrumb: [
                        { name: 'Metatag' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.seo_metatag.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Metatag Edit',
                    breadcrumb: [
                        { name: 'Metatag', link: 'api.backend.seo_metatag' },
                        { name: 'Metatag Edit' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.seo_metatag.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Metatag Create',
                    breadcrumb: [
                        { name: 'Metatag', link: 'api.backend.seo_metatag' },
                        { name: 'Metatag Create' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/seo/redirect',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.seo_redirect',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Redirects',
                    breadcrumb: [
                        { name: 'Redirects' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.seo_redirect.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Redirect Edit',
                    breadcrumb: [
                        { name: 'Redirects', link: 'api.backend.seo_redirect' },
                        { name: 'Redirect Edit' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.seo_redirect.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Redirect Create',
                    breadcrumb: [
                        { name: 'Redirects', link: 'api.backend.seo_redirect' },
                        { name: 'Redirect Create' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/seo/robots',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.seo_robots',
                component: require('@/views/seo/robots/index').default,
                meta: {
                    pageTitle: 'Robots txt',
                    breadcrumb: [
                        { name: 'Robots txt' }
                    ]
                }
            }
        ]
    }
]
