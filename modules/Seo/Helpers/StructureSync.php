<?php

namespace Modules\Seo\Helpers;

use Modules\Seo\Entities\Structure;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Seo\Repositories\Eloquent\EloquentStructureRepository;

class StructureSync
{
    protected $model;

    public function __construct(SeoSyncInterface $model)
    {
        $this->model = $model;
        $this->templates = new Templates();
        $this->structure = new EloquentStructureRepository(new Structure);
    }

    public function create()
    {
        return $this->structure->createWithModel($this->model);
    }

    public function update()
    {
        return $this->structure->updateWithModel($this->model);
    }

    public function delete()
    {
        return $this->structure->drop($this->model);
    }
}
