<?php

namespace Modules\Seo\Helpers;

use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Sitemap as SpatieSitemap;

class Sitemap extends SpatieSitemap
{
    public function chunckWriteToFile(string $path, $count): self
    {
        sort($this->tags);

        $chunked_tags = collect($this->tags)->chunk($count, true);
        $path_info = pathinfo($path);
        $path = $path_info['dirname'] . DIRECTORY_SEPARATOR . $path_info['filename'];

        if ($chunked_tags->count() > 1) {
            $index = SitemapIndex::create();

            foreach ($chunked_tags as $id => $tags) {
                $view = view('laravel-sitemap::sitemap')
                    ->with(compact('tags'))
                    ->render();

                file_put_contents($path . '_' . $id . '.' . $path_info['extension'], $view);

                $index->add($path_info['filename'] . '_' . $id . '.' . $path_info['extension']);
            };

            $index->writeToFile($path . '_index.' . $path_info['extension']);
            unlink(public_path('sitemap.xml'));
        } else {
            $view = view('laravel-sitemap::sitemap')
                ->with(['tags' => $chunked_tags->first()])
                ->render();

            file_put_contents($path . '.' . $path_info['extension'], $view);
            $files = scandir(public_path());

            foreach ($files as $file) {
                if (strpos($file, 'sitemap_') !== false && strpos($file, '.xml') !== false) {
                    unlink(public_path($file));
                }
            }
        }

        return $this;
    }
}
