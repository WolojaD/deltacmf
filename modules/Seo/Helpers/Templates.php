<?php

namespace Modules\Seo\Helpers;

use Illuminate\Support\Facades\Lang;

class Templates
{
    private $templates;
    private $template;

    public function __construct($template = false)
    {
        $this->templates = include base_path('modules/Seo/Config/templates.php');
        $this->template = $template;
    }

    /**
     * get list of all templates filtered for page
     *
     * @return void
     */
    public function getPageList()
    {
        $result = [];

        foreach ($this->templates as $template_name => $template) {
            if (!($template['to_page'] ?? false)) {
                continue;
            }

            $temp = [
                'label' => Lang::get('seo::templates.' . $template_name),
                'value' => $template_name,
            ];

            if ($template['needs_index']) {
                $temp['children'] = $template['model']::whereIsRoot()->get()->map(function ($item) {
                    return [
                        'label' => $item->title,
                        'value' => $item->id
                    ];
                });
            }

            $result[] = $temp;
        }

        return $result;
    }

    /**
     * get list of all templates filtered for structure
     *
     * @return array
     */
    public function getStructureList()
    {
        $result = $this->getAllList();
        foreach ($result as $name => $template) {
            if ($template['no_structure'] ?? false) {
                unset($template[$name]);
            }
        }

        return $result;
    }

    /**
     * get list of all templates template_name => translation
     *
     * @return array
     */
    public function getAllList()
    {
        foreach ($this->templates as $template_name => $template) {
            $result[$template_name] = Lang::get('seo::templates.' . $template_name);
        }

        return $result ?? [];
    }

    /**
     * get template data by name
     *
     * @param string $name
     * @return array
     */
    public function getTemplate($name = false)
    {
        $data = $this->templates[$name ?: $this->template] ?? $this->templates['default'];
        $data['name'] = $name ?: ($this->template ?? 'default');

        return $data;
    }

    /**
     * get model with namespace by name of template
     *
     * @param string $name
     * @return string
     */
    public function getModel($name = null)
    {
        $name = $this->template ?? $name;

        return $this->templates[$name]['model'] ?? false;
    }

    /**
     * get product templates can be added to page
     *
     * @return array
     */
    public function getSemiPhantomProductsTemplates()
    {
        $result = [];

        foreach ($this->templates as $template_name => $template) {
            if ($template_name !== 'products' && isset($template['model']) && ($template['model'] == \Modules\Ecommerce\Entities\Category::class)) {
                $result[] = $template_name;
            }
        }

        return $result;
    }

    /**
     * is combo template by name
     *
     * @param string $name
     * @return boolean
     */
    public function isCombo($name)
    {
        return $this->templates[$name]['combo'] ?? false;
    }

    public function isFilterable($page_template)
    {
        return $this->getTemplate($page_template)['filterable'] ?? false;
    }

    public function getUrl($structure, $slug)
    {
        if ($structure->url ?? false) {
            $url = ($structure->url == '/' || $structure->url == '/fantom_storepage_u_cant_see' || $this->template == 'product') ? '/' . $slug : $structure->url . '/' . $slug;
        }

        return $url ?? '/';
    }

    /**
     * is page is simple
     *
     * @return boolean
     */
    public function isSimplePage()
    {
        return $this->getModel($this->template) == \Modules\Page\Entities\Page::class;
    }
}
