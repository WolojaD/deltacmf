<?php

namespace Modules\Seo\Tests\Unit;

use Carbon\Carbon;
use Modules\Page\Entities\Page;
use Modules\Seo\Helpers\Templates;
use Modules\Seo\Entities\Structure;
use Modules\Core\Tests\CoreTestCase;
use Modules\Ecommerce\Entities\Sale;
use Modules\Ecommerce\Entities\Brand;
use Modules\Page\Repositories\Eloquent\EloquentPageRepository;

class StructureTest extends CoreTestCase
{
    /**
     * ------------------------------------------------
     * CREATE
     * ------------------------------------------------
    **/

    /** @test */
    public function created_page_adds_to_structure()
    {
        $page = factory(Page::class)->create([
            'ru' => [
                'title' => 'test title',
            ],
            'slug' => 'test-slug',
            'status' => 2,
            'page_template' => 'default',
            'parent_id' => null
        ]);
        $structure = Structure::first();
        $templates = (new Templates($page->page_template))->getTemplate();

        $this->assertEquals($page->id, $structure->id);
        $this->assertEquals($page->title, $structure->title);
        $this->assertEquals($page->slug, $structure->slug);
        $this->assertEquals($page->status, $structure->status);
        $this->assertEquals($page->page_template, $structure->page_template);
        $this->assertEquals($templates['weight'], $structure->weight);
        $this->assertEquals($templates['frequency'], $structure->frequency);
    }

    /** @test */
    public function if_no_parent_published_is_status_when_created()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 0]);
        $this->assertEquals(0, Structure::where('page_id', $page->id)->value('published'));
        $page->delete();
        $page = factory(Page::class)->states('is_home')->create(['status' => 1, 'slug' => 'test1']);
        $this->assertEquals(1, Structure::where('page_id', $page->id)->value('published'));
        $page->delete();
        $page = factory(Page::class)->states('is_home')->create(['status' => 2, 'slug' => 'test2']);
        $this->assertEquals(2, Structure::where('page_id', $page->id)->value('published'));
    }

    /** @test */
    public function if_have_parent_published_is_published_when_created()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 0]);
        $page_2 = factory(Page::class)->create([
            'slug' => 'test',
            'page_template' => 'default',
            'status' => 1,
            'parent_id' => $page->id,
        ]);

        $this->assertNotNull(Structure::where('page_id', $page_2->id)->value('published'));
        $this->assertEquals(0, Structure::where('page_id', $page_2->id)->value('published'));
    }

    /** @test */
    public function products_and_product_have_store_type()
    {
        $page = factory(Page::class)->states('is_home')->create();

        $category = \Modules\Ecommerce\Entities\Category::create([
             'parent_id' => null,
             'status' => 1,
             'slug' => 'slug',
             'ru' => [
                 'title' => 'title',
             ]
         ]);

        $category_1 = \Modules\Ecommerce\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $product = \Modules\Ecommerce\Entities\Product::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'produt',
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $created_products_page = factory(Page::class)->create([
            'slug' => 'fantom_storepage_u_cant_see',
            'page_template' => 'products',
            // 'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $structures = Structure::where('id', '!=', 1)->get();
        foreach ($structures as $structure) {
            $this->assertEquals('store', $structure->type);
        }
    }

    /** @test */
    public function created_default_template_adds_to_parent()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_default_page = factory(Page::class)->create([
            'page_template' => 'default',
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_default_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_default_page->id)->value('parent_id')
        );
    }

    /** @test */
    public function created_brands_template_adds_to_parent_and_add_brands()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $brand = Brand::create([
                'slug' => 'slug',
                'ru' => [
                    'title' => 'title'
                ]
            ]);
        $created_brands_page = factory(Page::class)->create([
            'slug' => 'brands',
            'page_template' => 'brands',
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_brands_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_brands_page->id)->value('parent_id')
        );
        $this->assertEquals(
            Structure::where('page_id', $created_brands_page->id)->value('id'),
            Structure::where('object_id', $brand->id)->where('page_template', 'brand')->value('parent_id')
        );
    }

    /** @test */
    public function created_sales_template_adds_to_parent_and_add_sales()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $sale = Sale::create([
            'date_from' => Carbon::now()->subDays(3),
            'date_to' => Carbon::now()->addDays(3),
            'discount' => 33,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title'
            ]
        ]);
        $created_sales_page = factory(Page::class)->create([
            'slug' => 'sales',
            'page_template' => 'sales',
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_sales_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_sales_page->id)->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('page_id', $created_sales_page->id)->value('id'),
            Structure::where('object_id', $sale->id)->where('page_template', 'sale')->value('parent_id')
        );
    }

    /** @test */
    public function created_article_adds_to_articles()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
        ]);
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'status' => 1,
            'relation' => $category->id,
            'parent_id' => $page->id,
        ]);

        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => $category->id,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $this->assertEquals(
            Structure::where('object_id', $category->id)->where('page_template', 'articles')->value('id'),
            Structure::where('object_id', $post->id)->where('page_template', 'article')->value('parent_id')
        );
    }

    /** @test */
    public function created_and_binded_to_page_articles_or_news_status_depends_on_both_category_and_page()
    {
        $page = factory(Page::class)->states('is_home')->create();
        \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
        ]);
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'status' => 1,
            'relation' => 1,
            'parent_id' => $page->id,
        ]);

        \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 0,
            'slug' => 'slug2',
        ]);
        $created_articles_page_2 = factory(Page::class)->create([
            'slug' => 'articles2',
            'page_template' => 'articles',
            'status' => 1,
            'relation' => 2,
            'parent_id' => $page->id,
        ]);

        \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 2,
            'slug' => 'slug3',
        ]);
        $created_articles_page_3 = factory(Page::class)->create([
            'slug' => 'articles3',
            'page_template' => 'articles',
            'status' => 1,
            'relation' => 3,
            'parent_id' => $page->id,
        ]);

        $this->assertEquals(1, Structure::where('page_id', $created_articles_page->id)->value('status'));
        $this->assertEquals(0, Structure::where('page_id', $created_articles_page_2->id)->value('status'));
        $this->assertEquals(2, Structure::where('page_id', $created_articles_page_3->id)->value('status'));
    }

    /** @test */
    public function created_articles_and_news_adds_only_if_have_binding()
    {
        \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        \Modules\Articles\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        \Modules\Articles\Entities\Post::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $this->assertEquals(0, Structure::count());
    }

    /** @test */
    public function created_articles_template_adds_to_parent_and_add_categories_and_articles()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $this->assertEquals($created_articles_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_articles_page->id)->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('page_id', $created_articles_page->id)->value('id'),
            Structure::where('object_id', $category_1->id)->where('page_template', 'articles')->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('object_id', $category_1->id)->where('page_template', 'articles')->value('id'),
            Structure::where('object_id', $post->id)->where('page_template', 'article')->value('parent_id')
        );
    }

    /** @test */
    public function created_news_template_adds_to_parent_and_add_categories_and_news()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $created_news_page = factory(Page::class)->create([
            'slug' => 'news',
            'page_template' => 'news',
            'relation' => 1,
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_news_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_news_page->id)->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('page_id', $created_news_page->id)->value('id'),
            Structure::where('object_id', $category_1->id)->where('page_template', 'news')->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('object_id', $category_1->id)->where('page_template', 'news')->value('id'),
            Structure::where('object_id', $post->id)->where('page_template', 'article')->value('parent_id')
        );
    }

    /** @test */
    public function created_products_before_binded_page_binds_to_it()
    {
        $page = factory(Page::class)->states('is_home')->create();

        $category = \Modules\Ecommerce\Entities\Category::create([
             'parent_id' => null,
             'status' => 1,
             'slug' => 'slug',
             'ru' => [
                 'title' => 'title',
             ]
         ]);

        $category_1 = \Modules\Ecommerce\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $product = \Modules\Ecommerce\Entities\Product::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'produt',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $this->assertEquals(1, Structure::count());

        $created_products_page = factory(Page::class)->create([
            'slug' => 'fantom_storepage_u_cant_see',
            'page_template' => 'products',
            // 'relation' => 1,
            'parent_id' => $page->id,
        ]);

        $this->assertEquals(5, Structure::count());
    }

    /** @test */
    public function created_products_template_adds_to_parent_and_add_categories_and_products()
    {
        $page = factory(Page::class)->states('is_home')->create();

        $created_products_page = factory(Page::class)->create([
            'slug' => 'fantom_storepage_u_cant_see',
            'page_template' => 'products',
            // 'relation' => 1,
            'parent_id' => $page->id,
        ]);

        $category = \Modules\Ecommerce\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Ecommerce\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $product = \Modules\Ecommerce\Entities\Product::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'produt',
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $this->assertEquals($created_products_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_products_page->id)->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('page_id', $created_products_page->id)->value('id'),
            Structure::where('object_id', $category->id)->where('page_template', 'products')->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('object_id', $category->id)->where('page_template', 'products')->value('id'),
            Structure::where('object_id', $category_1->id)->where('page_template', 'products')->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('object_id', $category_1->id)->where('page_template', 'products')->value('id'),
            Structure::where('object_id', $product->id)->where('page_template', 'product')->value('parent_id')
        );

        $this->assertEquals(
            '/' . $category->slug . '/' . $category_1->slug,
            Structure::where('object_id', $category_1->id)->where('page_template', 'products')->value('url')
        );

        $this->assertEquals(
            '/' . $product->slug,
            Structure::where('object_id', $product->id)->where('page_template', 'product')->value('url')
        );
    }

    /** @test */
    public function created_top_products_template_adds_like_default()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_top_products_page = factory(Page::class)->create([
            'page_template' => 'top_products',
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_top_products_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_top_products_page->id)->value('parent_id')
        );
    }

    /** @test */
    public function created_new_products_template_adds_like_default()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_new_products_page = factory(Page::class)->create([
            'page_template' => 'new_products',
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_new_products_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_new_products_page->id)->value('parent_id')
        );
    }

    /** @test */
    public function created_discounted_products_template_adds_like_default()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_discounted_products_page = factory(Page::class)->create([
            'page_template' => 'discounted_products',
            'parent_id' => $page->id,
        ]);

        $this->assertEquals($created_discounted_products_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_discounted_products_page->id)->value('parent_id')
        );
    }

    /**
     * ------------------------------------------------
     * UPDATE
     * ------------------------------------------------
    **/

    /** @test */
    public function updating_default_templates_parent_changing_children_urls()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id]);
        $sub_page_2 = factory(Page::class)->create(['parent_id' => $page->id]);
        $sub_page_2_1 = factory(Page::class)->create(['parent_id' => $sub_page_2->id]);

        $this->assertEquals(
            '/' . $sub_page_2->slug . '/' . $sub_page_2_1->slug,
            Structure::where('page_id', $sub_page_2_1->id)->value('url')
        );
        (new EloquentPageRepository(new Page))->update($sub_page_2, ['parent_id' => $sub_page_1->id]);

        $this->assertEquals(
            '/' . $sub_page_1->slug . '/' . $sub_page_2->slug . '/' . $sub_page_2_1->slug,
            Structure::where('page_id', $sub_page_2_1->id)->value('url')
        );
    }

    /** @test */
    public function updating_default_templates_if_new_parent_status_differs_of_previous_status_update_triggers()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1]);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 0]);
        $sub_page_2 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 1]);
        $sub_page_2_1 = factory(Page::class)->create(['parent_id' => $sub_page_2->id, 'status' => 1]);
        (new EloquentPageRepository(new Page))->update($sub_page_2, ['parent_id' => $sub_page_1->id]);

        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_2->id)->value('status')
        );
        $this->assertEquals(
            0,
            Structure::where('page_id', $sub_page_2->id)->value('published')
        );
        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_2_1->id)->value('status')
        );
        $this->assertEquals(
            0,
            Structure::where('page_id', $sub_page_2_1->id)->value('published')
        );
    }

    /** @test */
    public function updating_default_slug_triggers_update_urls_for_children()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'slug' => 'old_slug']);
        $sub_page_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id]);

        $this->assertEquals(
            '/' . $sub_page_1->slug . '/' . $sub_page_1_1->slug,
            Structure::where('page_id', $sub_page_1_1->id)->value('url')
        );

        (new EloquentPageRepository(new Page))->update($sub_page_1, ['slug' => 'new_slug']);
        // dd('stop');
        $this->assertEquals(
            '/' . Page::where('id', $sub_page_1->id)->value('slug') . '/' . $sub_page_1_1->slug,
            Structure::where('page_id', $sub_page_1_1->id)->value('url')
        );
    }

    /** @test */
    public function updating_status_published_or_status_is_0_children_published_is_0()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1]);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 1]);
        $sub_page_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1]);

        (new EloquentPageRepository(new Page))->update($page, ['status' => 0]);

        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_1->id)->value('status')
        );
        $this->assertEquals(
            0,
            Structure::where('page_id', $sub_page_1->id)->value('published')
        );
        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_1_1->id)->value('status')
        );
        $this->assertEquals(
            0,
            Structure::where('page_id', $sub_page_1_1->id)->value('published')
        );
    }

    /** @test */
    public function updating_status_published_and_status_is_1_children_published_is_1()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 0]);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 0]);
        $sub_page_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1]);

        (new EloquentPageRepository(new Page))->update($page, ['status' => 1]);

        $this->assertEquals(
            0,
            Structure::where('page_id', $sub_page_1->id)->value('status')
        );
        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_1->id)->value('published')
        );
        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_1_1->id)->value('status')
        );
        $this->assertEquals(
            0,
            Structure::where('page_id', $sub_page_1_1->id)->value('published')
        );
    }

    /** @test */
    public function updating_status_published_and_status_is_1_and_2_children_published_is_2()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1]);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 1]);
        $sub_page_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1]);

        (new EloquentPageRepository(new Page))->update($page, ['status' => 2]);

        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_1->id)->value('status')
        );
        $this->assertEquals(
            2,
            Structure::where('page_id', $sub_page_1->id)->value('published')
        );
        $this->assertEquals(
            1,
            Structure::where('page_id', $sub_page_1_1->id)->value('status')
        );
        $this->assertEquals(
            2,
            Structure::where('page_id', $sub_page_1_1->id)->value('published')
        );
    }

    /** @test */
    public function updating_to_default_template_delete_all_not_default_children()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1,  'page_template' => 'default']);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 1, 'page_template' => 'brands']);
        $sub_page_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1, 'page_template' => 'default']);
        $sub_page_1_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1, 'page_template' => 'default']);
        $sub_page_1_2 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1, 'page_template' => 'brand']);

        $this->assertTrue(Structure::where('page_template', '!=', 'default')->count() == 2);

        (new EloquentPageRepository(new Page))->update($sub_page_1, ['page_template' => 'default']);

        $this->assertTrue(Structure::where('page_template', '!=', 'default')->count() == 0);
        $this->assertTrue(Structure::where('page_template', 'default')->count() == 4);
    }

    /**
     * articles
     */

    /** @test */
    public function updating_not_binded_articles_or_news_does_nothing()
    {
        $category = \Modules\Articles\Entities\Category::create([
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category->update([
            'status' => 1,
            'slug' => 'update',
            'ru' => [
                'title' => 'update',
            ]
        ]);

        $this->assertEquals(0, Structure::count());
    }

    /** @test */
    public function updated_to_articles_templates_adds_all_children()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'default',
            'parent_id' => $page->id,
        ]);
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $created_articles_page->update(['relation' => 1, 'page_template' => 'articles']);

        $this->assertEquals($created_articles_page->id, Structure::where('parent_id', $page->id)->value('id'));
        $this->assertEquals(
            Structure::where('page_id', $page->id)->value('id'),
            Structure::where('page_id', $created_articles_page->id)->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('page_id', $created_articles_page->id)->value('id'),
            Structure::where('object_id', $category_1->id)->where('page_template', 'articles')->value('parent_id')
        );

        $this->assertEquals(
            Structure::where('object_id', $category_1->id)->where('page_template', 'articles')->value('id'),
            Structure::where('object_id', $post->id)->where('page_template', 'article')->value('parent_id')
        );
    }

    /** @test */
    public function updated_articles_templates_deletes_all_children_if_no_new_binding()
    {
        $page = factory(Page::class)->states('is_home')->create();

        $category_binded = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_not_binded = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => $category_binded->id,
            'status' => 1,
            'slug' => 'cat1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => $category_1->id,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $this->assertEquals(2, Structure::where('page_template', 'articles')->count());
        $this->assertEquals(1, Structure::where('page_template', 'article')->count());

        $category_1->update(['parent_id' => $category_not_binded->id]);

        $this->assertEquals(1, Structure::where('page_template', 'articles')->count());
        $this->assertEquals(0, Structure::where('page_template', 'article')->count());
    }

    /**
     * Common
     */

    /** @test */
    public function updating_title_updates_title()
    {
        $page = factory(Page::class)->states('is_home')->create([
            'ru' => ['title' => 'old title']
        ]);

        $this->assertEquals(
            $page->translate('ru')->title,
            Structure::where('page_id', $page->id)->first()->translate('ru')->title
        );

        (new EloquentPageRepository(new Page))->update($page, ['ru' => ['title' => 'old title']]);
        $this->assertEquals(
            Page::where('id', $page->id)->first()->translate('ru')->title,
            Structure::where('page_id', $page->id)->first()->translate('ru')->title
        );
    }

    /** @test */
    public function updating_attached_page_item_title_not_updates_title()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'relation' => 1,
            'parent_id' => $page->id,
            'ru' => [
                'title' => 'old_title',
            ]
        ]);
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'wrong_title',
            ]
        ]);

        $category->update(['ru' => [
            'title' => 'new_title',
        ]]);
        $structure = Structure::where('object_id', $category->id)->where('page_template', 'articles')->first();
        $this->assertEquals($created_articles_page->title, $structure->title);
        $this->assertNotEquals($category->title, $structure->title);
    }

    /** @test */
    public function if_item_not_attached_to_page_children_urls_changing_when_slug_changed()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $created_articles_page->update(['slug' => 'new_slug']);
        $this->assertEquals('new_slug', Structure::where('object_id', $category->id)->where('page_template', 'articles')->value('slug'));
        $this->assertEquals('/' . $created_articles_page->slug . '/' . $category_1->slug, Structure::where('object_id', $category_1->id)->where('page_template', 'articles')->value('url'));
        $this->assertEquals('/' . $created_articles_page->slug . '/' . $category_1->slug . '/' . $post->slug, Structure::where('object_id', $post->id)->where('page_template', 'article')->value('url'));
    }

    /** @test */
    public function if_item_attached_to_page_nothing_changes_when_slug_changed()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => 1,
            'status' => 1,
            'slug' => 'slg1',
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => 2,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);

        $category->update(['slug' => 'new_slug']);
        $this->assertEquals('articles', Structure::where('object_id', $category->id)->where('page_template', 'articles')->value('slug'));
    }

    /** @test */
    public function if_item_attached_to_page_updating_status_depends_on_both()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'status' => 1,
            'relation' => 1,
            'parent_id' => $page->id,
        ]);
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
        ]);

        $query = Structure::where('object_id', $category->id)->where('page_template', 'articles');
        $this->assertEquals(1, $query->first()->status);
        $category->update(['status' => 0]);
        $this->assertEquals(0, $query->first()->status);
        $category->update(['status' => 2]);
        $this->assertEquals(2, $query->first()->status);
    }

    /** @test */
    public function updating_articles_template_to_news_deleting_all_except_default_and_articles()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $category = \Modules\Articles\Entities\Category::create([
            'parent_id' => null,
            'status' => 1,
            'slug' => 'slug',
        ]);
        $created_articles_page = factory(Page::class)->create([
            'slug' => 'articles',
            'page_template' => 'articles',
            'status' => 1,
            'relation' => $category->id,
            'parent_id' => $page->id,
        ]);
        $created_brands_page = factory(Page::class)->create([
            'slug' => 'brands',
            'page_template' => 'brands',
            'status' => 1,
            'parent_id' => $created_articles_page->id,
        ]);
        $category_1 = \Modules\Articles\Entities\Category::create([
            'parent_id' => $category->id,
            'status' => 1,
            'slug' => 'cat1',
        ]);
        $post = \Modules\Articles\Entities\Post::create([
            'category_id' => $category->id,
            'status' => 1,
            'slug' => 'post',
            'date_from' => Carbon::now(),
            'ru' => [
                'title' => 'title',
            ]
        ]);
        $this->assertEquals(3, Structure::where('page_id', $created_articles_page->id)->first()->children->count());

        $created_articles_page->update([
            'page_template' => 'news'
        ]);

        $this->assertEquals(2, Structure::where('page_id', $created_articles_page->id)->first()->children->count());
    }

    /** @test */
    public function updated_top_products_template_deletes_not_default_children()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $created_top_products_page = factory(Page::class)->create([
            'page_template' => 'top_products',
            'parent_id' => $page->id,
        ]);
        $sub_page = factory(Page::class)->create([
            'page_template' => 'brands',
            'parent_id' => $created_top_products_page->id,
        ]);
        $sub_page_default = factory(Page::class)->create([
            'page_template' => 'default',
            'parent_id' => $created_top_products_page->id,
        ]);

        $this->assertEquals(2, Structure::where('page_id', $created_top_products_page->id)->first()->children->count());

        $created_top_products_page->update(['page_template' => 'default']);

        $this->assertEquals(1, Structure::where('page_id', $created_top_products_page->id)->first()->children->count());
    }

    /**
     * ------------------------------------------------
     * DELETE
     * ------------------------------------------------
    **/

    /** @test */
    public function deleted_deletes_children()
    {
        $page = factory(Page::class)->states('is_home')->create(['status' => 1,  'page_template' => 'default']);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 1, 'page_template' => 'brands']);
        $sub_page_1_1 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1, 'page_template' => 'default']);

        $this->assertEquals(3, Structure::count());
        (new EloquentPageRepository(new Page))->destroy($page);

        $this->assertEquals(0, Structure::count());
    }

    /** @test */
    public function sales_to_brands()
    {
        $page = factory(Page::class)->states('is_home')->create();
        $brand = Brand::create([
            'slug' => 'slug',
            'ru' => [
                'title' => 'title'
            ]
        ]);
        $brand2 = Brand::create([
            'slug' => 'slug2',
            'ru' => [
                'title' => 'title'
            ]
        ]);
        $sale = Sale::create([
            'slug' => 'test',
            'ru' => [
                'title' => 'title'
            ]
        ]);
        $sub_page_1 = factory(Page::class)->create(['parent_id' => $page->id, 'status' => 1, 'page_template' => 'brands']);
        $this->assertEquals(4, Structure::count());
        $sub_page_2 = factory(Page::class)->create(['parent_id' => $sub_page_1->id, 'status' => 1, 'page_template' => 'sales']);
        $this->assertEquals(6, Structure::count());
        $this->assertEquals(3, Structure::where('page_id', $sub_page_1->id)->first()->children->count());

        $sub_page_2->update(['parent_id' => 1]);
        $this->assertEquals(2, Structure::where('page_id', $sub_page_1->id)->first()->children->count());

        $sub_page_2->update(['parent_id' => $sub_page_1->id]);
        $this->assertEquals(3, Structure::where('page_id', $sub_page_1->id)->first()->children->count());
    }
}
