<?php

namespace Modules\Seo\Tests\Feature;

use Modules\Page\Entities\Page;
use Modules\Core\Tests\CoreTestCase;

class StructureTest extends CoreTestCase
{
    /** @test */
    public function main_page_is_200_if_exists_and_status_1()
    {
        factory(Page::class)->states('is_home')->create(['status' => 1]);

        $this->call('get', '/')->assertStatus(200);
    }

    /** @test */
    public function main_page_is_404_if_exists_and_status_0()
    {
        $this->withExceptionHandling();
        factory(Page::class)->states('is_home')->create(['status' => 0]);

        $this->get('/')->assertStatus(404);
    }
}
