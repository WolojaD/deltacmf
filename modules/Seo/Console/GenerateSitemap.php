<?php

namespace Modules\Seo\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Seo\Entities\Structure;
use Watson\Sitemap\Facades\Sitemap;
use Illuminate\Support\Facades\Storage;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Sitemap::clear();

        // modify this to your own needs
        $locales = collect(json_decode(settings('core::locales')));

        $structure_chunks = Structure::getForSitemap()->where('slug', '!=', 'fantom_storepage_u_cant_see')->chunk(config('sitemap.limit'));

        if ($structure_chunks->count()) {
            $files = scandir(public_path());
            foreach ($files as $file) {
                if (strpos($file, 'sitemap') !== false && strpos($file, '.xml') !== false) {
                    unlink(public_path($file));
                }
            }
        }

        foreach ($structure_chunks as $id => $structure_chunk) {
            $structure_chunk->each(function ($structure, $id) use ($locales) {
                if (!$structure) {
                    return;
                }

                $locales->each(function ($locale, $index) use ($structure, $locales) {
                    $prefix = ($index ? '/' . $locale : '');

                    if ($locales->count()) {
                        Sitemap::addTag(new \Watson\Sitemap\Tags\MultilingualTag(
                            url($prefix . $structure->url), //TODO bug with subdomains
                            $structure->updated_at ?? Carbon::now(),
                            $structure->frequency ?? 'weekly',
                            $structure->weight,
                            $locales->mapWithKeys(function ($locale, $index) use ($structure) {
                                return [$locale . '-ua' => ($index ? '/' . $locale : '') . $structure->url];//TODO -ua может быть не юа
                            })->toArray()
                        ));
                    } else {
                        Sitemap::addTag(
                            url($prefix . $structure->url), //TODO bug with subdomains
                            $structure->updated_at ?? Carbon::now(),
                            $structure->frequency ?? 'weekly',
                            $structure->weight
                        );
                    }
                });
            });

            Storage::disk('public_folder')->put('sitemap' . ($structure_chunks->count() > 1 ? ('_' . $id) : '') . '.xml', Sitemap::xml());
            Sitemap::clear();
        }
    }
}
