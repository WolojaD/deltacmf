<?php

namespace Modules\Seo\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Seo\Entities\Structure;
use Illuminate\Support\Facades\Artisan;
use Modules\Seo\Repositories\StructureInterface;
use Modules\Seo\Transformers\StructureTransformer;
use Modules\Seo\Http\Requests\CreateStructureRequest;
use Modules\Seo\Http\Requests\UpdateStructureRequest;
use Modules\Seo\Transformers\FullStructureTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class StructureController extends CoreApiController
{
    /**
     * @var StructureInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(StructureInterface $structure)
    {
        $this->repository = $structure;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return StructureTransformer
     */
    public function index(Request $request, $id = false)
    {
        $referer = explode('/', request()->headers->get('referer'));
        $type = array_pop($referer);
        $type = !is_numeric($type) ? $type : 'structure';

        if ($id) {
            $request->merge(['pageId' => $id ?: null]);
            $request->merge([
                'where' => [
                    ['url', '!=', '/fantom_storepage_u_cant_see']
                ]
            ]);
            // $request->merge(['where' => ['type' => $type]]);

            return StructureTransformer::collection(
                $this->repository
                     ->serverPaginationFilteringFor($request)
                     ->addAdditions(['tab' => $this->repository->getTab($id)])
            );
        }

        if ($type == 'structure') {
            $request->merge(['where' => ['type' => 'structure', 'parent_id' => null]]);
        } elseif ($type == 'store') {
            $request->merge([
                'where' => [
                    'parent_id' => Structure::where('url', '/fantom_storepage_u_cant_see')->select('id')->first()->id,
                    'type' => 'store'
                ]
            ]);
        } elseif ($type == 'manual') {
            $request->merge(['where' => ['type' => 'manual']]);
        }

        $table = $type == 'manual' ? $type : 'default';

        return StructureTransformer::collection($this->repository->serverPaginationFilteringFor($request, $table));
    }

    /**
     * @param int $id
     *
     * @return FullStructureTransformer
     */
    public function find($id)
    {
        $structure = $this->repository->find($id);

        return new FullStructureTransformer($structure);
    }

    /**
     * @param int $id
     * @param UpdateStructureRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateStructureRequest $request)
    {
        $structure = $this->repository->find($id);

        $this->repository->update($structure, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('seo::models.of.structure')]),
            'id' => $structure->id
        ]);
    }

    /**
     * @param CreateStructureRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStructureRequest $request)
    {
        $structure = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('seo::models.of.structure')]),
            'type' => $structure->parent_id ? 'success' : 'warning',
            'title' => $structure->parent_id ? 'Success' : 'Warning',
            'id' => $structure->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $structure = $this->repository->find($id);

        if ($this->repository->destroy($structure->dropChildrenWithRedirect())) {
            return response()->json([
                'errors' => false,
                'message' => trans('core::messages.api.fields.deleted', ['field' => trans('seo::models.of.structure')]),
                'id' => $structure->id,
            ]);
        }

        return response()->json([
            'errors' => true,
            'message' => trans('core::messages.api.fields.not_deleted', ['field' => trans('seo::models.of.structure')]),
        ]);
    }

    public function generateSitemap()
    {
        shell_exec('/usr/local/php71/bin/php ' . base_path() . '/artisan sitemap:generate');
        shell_exec('php artisan sitemap:generate');
        Artisan::call('sitemap:generate');

        return response()->json([
            'errors' => false,
            'message' => trans('seo::messages.api.sitemap generated'),
        ]);
    }
}
