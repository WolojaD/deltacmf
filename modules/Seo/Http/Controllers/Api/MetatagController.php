<?php

namespace Modules\Seo\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Seo\Repositories\MetatagInterface;
use Modules\Seo\Transformers\MetatagTransformer;
use Modules\Seo\Http\Requests\CreateMetatagRequest;
use Modules\Seo\Http\Requests\UpdateMetatagRequest;
use Modules\Seo\Transformers\FullMetatagTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class MetatagController extends CoreApiController
{
    /**
     * @var MetatagInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(MetatagInterface $metatag)
    {
        $this->repository = $metatag;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     *
     * @return MetatagTransformer
     */
    public function index(Request $request)
    {
        return MetatagTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullMetatagTransformer
     */
    public function find($id)
    {
        $metatag = $this->repository->find($id);

        return new FullMetatagTransformer($metatag);
    }

    /**
     * @param int $id
     * @param UpdateMetatagRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateMetatagRequest $request)
    {
        $metatag = $this->repository->find($id);
        $this->repository->update($metatag, $request->except('ident', 'compendium'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('seo::models.of.metatag')]),
            'id' => $metatag->id
        ]);
    }

    /**
     * @param CreateMetatagRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMetatagRequest $request)
    {
        $metatag = $this->repository->create($request->except('ident', 'compendium'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('seo::models.of.metatag')]),
            'id' => $metatag->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $metatag = $this->repository->find($id);

        $this->repository->destroy($metatag);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('seo::models.of.metatag')]),
        ]);
    }

    public function apply($id)
    {
        if ($this->repository->apply($id)) {
            return response()->json([
                'errors' => false,
                'message' => trans('core::messages.api.fields.applied', ['field' => trans('seo::models.of.metatag')]),
            ]);
        };

        return response()->json([
            'errors' => true,
            'message' => trans('core::messages.api.fields.not_applied', ['field' => trans('seo::models.of.metatag')]),
        ]);
    }
}
