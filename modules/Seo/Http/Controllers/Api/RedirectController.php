<?php

namespace Modules\Seo\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Seo\Repositories\RedirectInterface;
use Modules\Seo\Transformers\RedirectTransformer;
use Modules\Seo\Http\Requests\CreateRedirectRequest;
use Modules\Seo\Http\Requests\UpdateRedirectRequest;
use Modules\Seo\Transformers\FullRedirectTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class RedirectController extends CoreApiController
{
    /**
     * @var RedirectInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(RedirectInterface $redirect)
    {
        $this->repository = $redirect;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return RedirectTransformer
     */
    public function index(Request $request, $id = false)
    {
        return RedirectTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullRedirectTransformer
     */
    public function find($id)
    {
        $redirect = $this->repository->find($id);

        return new FullRedirectTransformer($redirect);
    }

    /**
     * @param int $id
     * @param UpdateRedirectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateRedirectRequest $request)
    {
        $locales = array_keys(get_application_frontend_locales());
        $locales[] = 'ident';

        return $this->repository->createOrUpdate($request->except($locales));
    }

    /**
     * @param CreateRedirectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRedirectRequest $request)
    {
        $request->merge(['user_id' => \Sentinel::getUser()->id]);

        $locales = array_keys(get_application_frontend_locales());
        $locales[] = 'ident';

        return $this->repository->createOrUpdate($request->except($locales));
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $redirect = $this->repository->find($id);

        $this->repository->destroy($redirect);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('seo::models.of.redirect')]),
        ]);
    }
}
