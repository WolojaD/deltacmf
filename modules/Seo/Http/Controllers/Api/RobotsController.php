<?php

namespace Modules\Seo\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class RobotsController extends CoreApiController
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        $file = file_get_contents(base_path() . '/public/robots.txt');

        return response(['file' => $file]);
    }

    public function store(Request $request)
    {
        file_put_contents(base_path() . '/public/robots.txt', $request->file);

        return response()->json([
            'errors' => false,
            'message' => trans('seo::messages.api.robots updated'),
        ]);
    }
}
