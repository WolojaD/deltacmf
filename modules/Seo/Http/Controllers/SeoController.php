<?php

namespace Modules\Seo\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Watson\Sitemap\Facades\Sitemap;
use Modules\Ecommerce\Entities\Product;
use Modules\Core\Http\Controllers\CorePublicController;

class SeoController extends CorePublicController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        dd('Module seo successfully created!\\n Middleware is missed for this module! Config folder should be created with configuration config\application\module_name');

        return view('seo::index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('seo::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('seo::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('seo::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
    }

    public function sitemap()
    {
        $files = scandir(public_path());
        foreach ($files as $file) {
            if (strpos($file, 'sitemap_') !== false && strpos($file, '.xml') !== false) {
                $sitemaps[] = $file;
            }
        }

        if (isset($sitemaps) && count($sitemaps)) {
            foreach ($sitemaps as $sitemap) {
                Sitemap::addSitemap(url($sitemap));//TODO bug with subdomains
            }

            return Sitemap::index();
        }

        return response()->file(public_path('sitemap.xml'));
    }

    public function sitemap_image()
    {
        $products = Product::listsTranslations('title')->select('title', 'gallery_id', 'ecommerce_products.id')->with('gallery', 'gallery.images', 'structure')->get();
        foreach ($products as $product) {
            if (!$product->gallery->images->count()) {
                continue;
            }

            $tag = Sitemap::addTag(
                url($product->structure->url)//TODO bug with subdomains
                // $product->structure->updated_at ?? Carbon::now(),
                // $product->structure->frequency ?? 'weekly',
                // $product->structure->weight
            );

            // foreach ($product->gallery->images as $image) {
            $image = $product->gallery->images->first();
            $tag->addImage(url('storage/origin' . $image->path), $image->title ?? $product->title);
            // }
        }

        return Sitemap::render();
    }
}
