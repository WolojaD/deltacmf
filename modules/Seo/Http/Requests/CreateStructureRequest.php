<?php

namespace Modules\Seo\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateStructureRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Seo/Config/models/structure.json');
    }

    public function authorize()
    {
        return true;
    }
}
