<?php

namespace Modules\Seo\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateMetatagRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Seo/Config/models/metatag.json');
    }

    public function authorize()
    {
        return true;
    }
}
