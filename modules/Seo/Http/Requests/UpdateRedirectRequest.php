<?php

namespace Modules\Seo\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateRedirectRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Seo/Config/models/redirect.json');
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'old_url' => 'required|url',
            'new_url' => 'required|url',
        ];
    }
}
