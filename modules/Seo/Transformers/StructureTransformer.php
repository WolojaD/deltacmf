<?php

namespace Modules\Seo\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class StructureTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => ($this->status ?? '') && ($this->published ?? '')];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['created_at'] = $this->created_at ? $this->created_at->toRfc2822String() : '';
        $result['path'] = $this->url ?? '';
        $result['children_count'] = $this->children()->count() ?? '';

        return $result;
    }
}
