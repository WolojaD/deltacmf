<?php

namespace Modules\Seo\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullMetatagTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'structure_id' => $this->structure_id,
            'type' => $this->type,
            'name' => $this->name,
            'page_template' => $this->page_template,
            'status' => $this->status
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
