<?php

namespace Modules\Seo\Transformers;

use Modules\Seo\Entities\Metatag;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Seo\Repositories\Eloquent\EloquentMetatagRepository;

class MetatagTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }
        $repository = new EloquentMetatagRepository(new Metatag);

        $result['page_template'] = trans('seo::templates.' . $this->page_template);
        $result['type'] = $repository->types()[$this->type] ?? '';
        $result['structure_id'] = $this->structure_title ?? '';
        $result['show'] = $this->page_template != 'filters';

        return $result;
    }
}
