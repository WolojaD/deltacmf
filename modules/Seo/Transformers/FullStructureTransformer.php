<?php

namespace Modules\Seo\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullStructureTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'weight' => $this->weight,
            'to_index' => $this->to_index,
            'to_sitemap' => $this->to_sitemap,
            'follow' => $this->follow,
            'og_type' => $this->og_type ?? 'website',
            'individual_tags' => $this->individual_tags,
            'frequency' => $this->frequency,
            'canonical' => $this->canonical,
            'og_image' => $this->og_image,
            'url' => $this->url
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }
        unset($data['translations']);

        return $data;
    }
}
