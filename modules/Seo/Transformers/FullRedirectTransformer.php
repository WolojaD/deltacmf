<?php

namespace Modules\Seo\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullRedirectTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'old_url' => url('/') . $this->old_url,
            'new_url' => url('/') . $this->new_url,
            'can_be_deleted' => $this->can_be_deleted,
        ];

        return $data;
    }
}
