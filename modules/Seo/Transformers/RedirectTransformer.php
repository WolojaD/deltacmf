<?php

namespace Modules\Seo\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class RedirectTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $first_name = $this->user->first_name ?? '';
        $last_name = $this->user->last_name ?? '';

        $data = [
            'id' => $this->id,
            'old_url' => $this->old_url,
            'new_url' => $this->new_url,
            'action' => trans('seo::table.redirect.status.' . $this->action),
            'user' => $first_name . ' ' . $last_name,
            'can_be_deleted' => boolval($this->can_be_deleted) ? '+' : '-',
            'updated_at' => $this->updated_at->toRfc2822String(),
            'path' => $this->old_url,
        ];

        return $data;
    }
}
