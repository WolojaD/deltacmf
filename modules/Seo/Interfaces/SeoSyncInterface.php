<?php

namespace Modules\Seo\Interfaces;

interface SeoSyncInterface
{
    public function parentStructure();
}
