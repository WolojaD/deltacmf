<?php

namespace Modules\Logs\Repositories\Cache;

use Modules\Logs\Repositories\FolderInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheFolderDecorator extends CoreCacheDecorator implements FolderInterface
{
    /**
    * @var FolderInterface
    */
    protected $repository;

    public function __construct(FolderInterface $logs)
    {
        parent::__construct();

        $this->repository = $logs;
    }
}
