<?php

namespace Modules\Logs\Repositories\Cache;

use Modules\Logs\Repositories\JobInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheJobDecorator extends CoreCacheDecorator implements JobInterface
{
    /**
     * @var JobInterface
     */
    protected $repository;

    public function __construct(JobInterface $logs)
    {
        parent::__construct();

        $this->repository = $logs;
    }
}
