<?php

namespace Modules\Logs\Repositories\Cache;

use Modules\Logs\Repositories\EmailInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheEmailDecorator extends CoreCacheDecorator implements EmailInterface
{
    /**
     * @var EmailInterface
     */
    protected $repository;

    public function __construct(EmailInterface $logs)
    {
        parent::__construct();

        $this->repository = $logs;
    }
}
