<?php

namespace Modules\Logs\Repositories\Cache;

use Modules\Logs\Repositories\JobFailInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheJobFailDecorator extends CoreCacheDecorator implements JobFailInterface
{
    /**
     * @var JobFailInterface
     */
    protected $repository;

    public function __construct(JobFailInterface $logs)
    {
        parent::__construct();

        $this->repository = $logs;
    }
}
