<?php

namespace Modules\Logs\Repositories\Eloquent;

use Modules\Logs\Repositories\JobInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentJobRepository extends EloquentCoreRepository implements JobInterface
{
    public $model_config = 'modules/Logs/Config/models/job.json';
}
