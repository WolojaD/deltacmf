<?php

namespace Modules\Logs\Repositories\Eloquent;

use Modules\Logs\Repositories\JobFailInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentJobFailRepository extends EloquentCoreRepository implements JobFailInterface
{
    public $model_config = 'modules/Logs/Config/models/jobfail.json';
}
