<?php

namespace Modules\Logs\Repositories\Eloquent;

use Modules\Logs\Repositories\FolderInterface;
use Modules\Logs\Transformers\FileTransformer;
use Modules\Logs\Transformers\FolderTransformer;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentFolderRepository extends EloquentCoreRepository implements FolderInterface
{
    public $model_config = 'modules/Logs/Config/models/folder.json';

    protected $filter;

    const MAXFILESIZE = 52428800;

    /**
    * Log levels with bootstrap classes
    *
    * @var array
    */
    private static $levels = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'danger',
        'critical' => 'danger',
        'alert' => 'danger',
        'emergency' => 'danger',
        'processed' => 'info',
        'failed' => 'warning',
    ];

    public function getFolder($request, $id)
    {
        $this->filter = $request->filter;

        try {
            $path = str_replace('--', DIRECTORY_SEPARATOR, $this->model->getPathFromApiRequest());
            $folder = storage_path('logs' . DIRECTORY_SEPARATOR . 'actions' . $path);

            if (is_file($folder)) {
                return $this->getFile($path, $request);
            }

            $files = scandir($folder);

            $this->folders = [];

            foreach ($files as $key => $file) {
                $filter = $request->filled('filter') ? strpos($file, $request->filter) !== false : true;

                if ('.' !== $file[0] && $filter) {
                    $id = str_replace(DIRECTORY_SEPARATOR, '--', ltrim($path, DIRECTORY_SEPARATOR));

                    $this->folders[] = [
                        'folder' => $file,
                        'id' => str_finish($id, '--') . $file,
                    ];
                }
            }

            if (!$id) {
                $this->folders = array_reverse($this->folders);
            }
        } catch (\Exception $e) {
            dd($e);

            $this->folders = [];
        }

        return FolderTransformer::collection($this->paginator(
            collect($this->folders),
            $request->get('per_page', 100)
        )->changeDataByKey('grand_parent_id', $path));
    }

    public function getFile($path, $request)
    {
        $file = storage_path('logs' . DIRECTORY_SEPARATOR . 'actions' . $path);

        $log = $this->parseLog($file)->filter(function ($item) use ($request) {
            $array = [
                'email' => $item['user_data']->email,
                'ip' => $item['user_data']->ip,
                'method' => $item['user_data']->method,
                'text' => $item['text']
            ];

            return count(preg_grep('~' . $request->filter . '~', $array)) > 0;
        });

        return FileTransformer::collection($this->paginator(
            $log,
            $request->get('per_page', 100),
            false,
            'file'
        )->changeDataByKey('grand_parent_id', $path));
    }

    /**
    * Get log levels
    *
    * @return array
    */
    protected function getLevels()
    {
        return array_keys(self::$levels);
    }

    protected function parseLog($filePath = false)
    {
        if (!$filePath) {
            return collect([]);
        }

        $pattern = '/\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}([\+-]\d{4})?\].*/';

        if (app('files')->size($filePath) > self::MAXFILESIZE) {
            throw new \Exception(trans('good-log-viewer::errors.big-file', [
                'maxSize' => (int)(self::MAXFILESIZE / 1024),
            ]));
        }

        $file = app('files')->get($filePath);
        preg_match_all($pattern, $file, $headings);

        if (!is_array($headings)) {
            return [];
        }

        $fileData = preg_split($pattern, $file);

        if ($fileData[0] < 1) {
            array_shift($fileData);
        }

        $data = [];

        foreach ($headings as $h) {
            for ($i = 0, $j = count($h); $i < $j; $i++) {
                foreach ($this->getLevels() as $level) {
                    if (strpos(strtolower($h[$i]), '.' . $level) || strpos(strtolower($h[$i]), $level . ':')) {
                        preg_match('/^\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}([\+-]\d{4})?)\](?:.*?(\w+)\.|.*?)' . $level . ': (.*?)( in .*?:[0-9]+)?$/i', $h[$i], $current);

                        if (!isset($current[4])) {
                            continue;
                        }

                        preg_match('/([a-zA-Z0-9\s]+) ({.*}|\[\]) ({.*})/', $current[4], $dataArray);

                        $data[] = [
                            'date' => $current[1],
                            'level' => $level,
                            'level_class' => self::$levels[$level] ?? 'default',
                            'context' => $current[3],
                            'text' => $dataArray[1],
                            'file' => isset($current[5]) ? $current[5] : null,
                            'extra' => json_decode($dataArray[2]),
                            'user_data' => json_decode(trim($dataArray[3] ?? '{}'))
                        ];
                    }
                }
            }
        }

        return collect(array_reverse($data));
    }
}
