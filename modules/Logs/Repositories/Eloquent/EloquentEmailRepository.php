<?php

namespace Modules\Logs\Repositories\Eloquent;

use Modules\Logs\Repositories\EmailInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentEmailRepository extends EloquentCoreRepository implements EmailInterface
{
    public $model_config = 'modules/Logs/Config/models/email.json';
}
