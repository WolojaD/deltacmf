<?php

namespace Modules\Logs\Entities;

use Modules\Core\Eloquent\Model;

class Folder extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'logs_folders';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/logs';

    protected $file_path = 'modules/Logs/Config/models/folder.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getBackendBreadcrumbs($carry = [])
    {
        $path = ltrim($this->getPathFromApiRequest(), '/');
        $items = explode('--', $path);
        $ident = '';

        $carry[] = [
            'name' => trans('core::breadcrumbs.folders'),
            'link' => [
                'name' => 'api.backend.logs_folder'
            ]
        ];

        foreach ($items as $name) {
            if (!$name) {
                continue;
            }

            $ident .= $ident ? '--' . $name : $name;

            $carry[] = [
                'name' => $name,
                'link' => [
                    'name' => 'api.backend.logs_folder.show',
                    'params' => [
                        'ident' => $ident
                    ]
                ]
            ];
        }

        unset($carry[count($carry) - 1]['link']);

        return $carry;
    }

    public function getPathFromApiRequest()
    {
        $string = str_replace('api/backend/logs/folders/show', '', urldecode(request()->path()));

        if (strpos($string, locale() . '/') === 0) {
            $string = str_replace(locale() . '/', '', $string);
        }

        return str_replace('api/backend/logs/folders', '', $string);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
