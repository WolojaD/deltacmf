<?php

namespace Modules\Logs\Traits;

use Modules\Logs\Loggers\ActionsLogger;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

trait TrackableTrait
{
    use PivotEventTrait;

    public static function bootTrackableTrait()
    {
        static::created(function ($model) {
            (new ActionsLogger($model, $model->setLogMessage('created')))->write();
        });

        static::updated(function ($model) {
            (new ActionsLogger($model, $model->setLogMessage('updated')))->write();
        });

        static::deleted(function ($model) {
            (new ActionsLogger($model, $model->setLogMessage('deleted')))->write();
        });

        static::pivotAttached(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            $log = new ActionsLogger(
                $model,
                $model->setLogMessage('attached'),
                compact('relationName', 'pivotIds', 'pivotIdsAttributes')
            );

            $log->write();
        });

        static::pivotDetached(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            $log = new ActionsLogger(
                $model,
                $model->setLogMessage('detached'),
                compact('relationName', 'pivotIds', 'pivotIdsAttributes')
            );

            $log->write();
        });

        static::pivotUpdated(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            $log = new ActionsLogger(
                $model,
                $model->setLogMessage('pivot updated'),
                compact('relationName', 'pivotIds', 'pivotIdsAttributes')
            );

            $log->write();
        });
    }

    public function setLogMessage($message)
    {
        return isset($this->title) ? $this->title . ' ' . $message : 'record ' . $message;
    }
}
