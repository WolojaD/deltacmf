<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'logs/folders'], function () {
    Route::get('/', ['as' => 'backend.logs_folder.index', 'uses' => 'LogsController@index', 'middleware' => 'can:logs.folder.index']);

    Route::get('{ident}', ['as' => 'backend.logs_folder.show', 'uses' => 'LogsController@show', 'middleware' => 'can:logs.folder.index'])->where(['ident' => '.*']);
});

Route::group(['prefix' => 'logs/mails'], function () {
    Route::get('/', ['as' => 'backend.logs_mail.index', 'uses' => 'LogsController@index', 'middleware' => 'can:logs.mail.index']);
});

Route::group(['prefix' => 'logs/jobs'], function () {
    Route::get('/', ['as' => 'backend.logs_jobs.index', 'uses' => 'LogsController@index', 'middleware' => 'can:logs.job.index']);
});

Route::group(['prefix' => 'logs/failed_jobs'], function () {
    Route::get('/', ['as' => 'backend.logs_jobs_fail.index', 'uses' => 'LogsController@index', 'middleware' => 'can:logs.job.fail.index']);
});
