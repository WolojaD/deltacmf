import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/logs/folders',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.logs_folder',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Folders',
                    breadcrumb: [
                        { name: 'Folders' }
                    ]
                }
            },
            {
                path: ':ident(.*)',
                name: 'api.backend.logs_folder.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Folders',
                    breadcrumb: [
                        { name: 'Folders' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/logs/mails',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.logs_mail',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Emails',
                    breadcrumb: [
                        { name: 'Emails' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/logs/jobs',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.logs_job',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Jobs',
                    breadcrumb: [
                        { name: 'Jobs' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/logs/failed_jobs',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.logs_job_fail',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Failed jobs',
                    breadcrumb: [
                        { name: 'Failed jobs' }
                    ]
                }
            }
        ]
    }
]
