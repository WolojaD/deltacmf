<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'logs/folders', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.logs_folder.index', 'uses' => 'FolderController@index', 'middleware' => 'token-can:logs.folder.index']);

    Route::get('show/{ident}', ['as' => 'api.backend.logs_folder.show', 'uses' => 'FolderController@index', 'middleware' => 'token-can:logs.folder.index'])->where(['ident' => '.*']);
});

Route::group(['prefix' => 'logs/mails', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.logs_mail.index', 'uses' => 'EmailController@index', 'middleware' => 'token-can:logs.mail.index']);
    Route::delete('destroy-multiple', ['as' => 'api.backend.logs_mail.destroy_multiple', 'uses' => 'EmailController@destroyMultiple', 'middleware' => 'token-can:logs.mail.destroy']);
    Route::delete('{ident}', ['as' => 'api.backend.logs_mail.destroy', 'uses' => 'EmailController@destroy', 'middleware' => 'token-can:logs.mail.destroy']);
});

Route::group(['prefix' => 'logs/jobs', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.logs_job.index', 'uses' => 'JobController@index', 'middleware' => 'token-can:logs.job.index']);
    Route::post('start', ['as' => 'api.backend.logs_job.start', 'uses' => 'JobController@start', 'middleware' => 'token-can:logs.job.edit']);
});

Route::group(['prefix' => 'logs/failed_jobs', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.logs_job_fail.index', 'uses' => 'JobFailController@index', 'middleware' => 'token-can:logs.job.fail.index']);
});
