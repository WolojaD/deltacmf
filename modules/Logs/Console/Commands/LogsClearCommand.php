<?php

namespace Modules\Logs\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class LogsClearCommand extends Command
{
    /** Time when all logs became old and must be deleted */
    protected $time;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'application:logs-clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear logs for specified days';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['days', InputArgument::REQUIRED, 'Days in past when all logs became old and must be deleted']
        ];
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $folder = storage_path('logs/actions/');
        $days = $this->argument('days');

        if (!is_numeric($days) && $days) {
            $this->error('wrong days variable: must be integer');

            return;
        }

        $this->time = \Carbon\Carbon::now()->subDays($days);
        $this->clean($folder);
    }

    /**
     * recursive find files and deletes it
     */
    public function clean($folder)
    {
        $folders = scandir($folder);

        foreach ($folders as $sub_folder_name) {
            $sub_folder = $folder . DIRECTORY_SEPARATOR . $sub_folder_name;

            if ((!is_dir($sub_folder) && !is_file($sub_folder)) || in_array($sub_folder_name, ['.', '..', '.gitignore'])) {
                continue;
            } elseif (is_file($sub_folder)) {
                $this->deleteOldFile($sub_folder, $sub_folder_name);
            } else {
                $this->clean($sub_folder);
            }
        }
    }

    /**
     * delete file if days gone
     */
    public function deleteOldFile($sub_folder, $sub_folder_name)
    {
        if ($this->time->gt(str_replace('.log', '', $sub_folder_name))) {
            unlink($sub_folder);
        }
    }
}
