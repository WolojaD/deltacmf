<?php

namespace Modules\Logs\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FileTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);

        $result['email'] = $result['user_data']->email ?? '';
        $result['impersonator_email'] = $result['user_data']->impersonator_email ?? '';
        $result['ip'] = $result['user_data']->ip ?? '';

        return $result;
    }
}
