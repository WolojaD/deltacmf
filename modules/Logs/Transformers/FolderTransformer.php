<?php

namespace Modules\Logs\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FolderTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
