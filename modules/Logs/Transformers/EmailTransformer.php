<?php

namespace Modules\Logs\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class EmailTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }
        $result['created_at'] = $this->created_at->toRfc2822String();
        $result['status'] = $this->status;

        return $result;
    }
}
