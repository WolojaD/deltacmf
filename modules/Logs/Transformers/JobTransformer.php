<?php

namespace Modules\Logs\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class JobTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }
        $result['created_at'] = $this->created_at ? $this->created_at->toRfc2822String() : '';
        $result['available_at'] = $this->available_at ? $this->available_at->toRfc2822String() : '';
        $result['reserved_at'] = $this->reserved_at ? $this->reserved_at->toRfc2822String() : '';

        return $result;
    }
}
