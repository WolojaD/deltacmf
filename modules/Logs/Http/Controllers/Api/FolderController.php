<?php

namespace Modules\Logs\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Logs\Repositories\FolderInterface;
use Modules\Logs\Http\Requests\CreateFolderRequest;
use Modules\Logs\Http\Requests\UpdateFolderRequest;
use Modules\Logs\Transformers\FullFolderTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class FolderController extends CoreApiController
{
    /**
     * @var FolderInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(FolderInterface $folder)
    {
        $this->repository = $folder;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        return $this->repository->getFolder($request, $id);
    }

    public function find($id)
    {
        $folder = $this->repository->find($id);

        return new FullFolderTransformer($folder);
    }

    public function update($id, UpdateFolderRequest $request)
    {
        $folder = $this->repository->find($id);

        $this->repository->update($folder, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('logs::models.of.folder')]),
            'id' => $folder->id
        ]);
    }

    public function store(CreateFolderRequest $request)
    {
        $folder = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('logs::models.of.folder')]),
            'id' => $folder->id
        ]);
    }

    public function destroy($id)
    {
        $folder = $this->repository->find($id);

        $this->repository->destroy($folder);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('logs::models.of.folder')]),
        ]);
    }
}
