<?php

namespace Modules\Logs\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Modules\Logs\Repositories\JobInterface;
use Modules\Logs\Transformers\JobTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class JobController extends CoreApiController
{
    /**
     * @var JobInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(JobInterface $job)
    {
        $this->repository = $job;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return JobTransformer
     */
    public function index(Request $request)
    {
        return JobTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function start()
    {
        Artisan::call('queue:work');
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = $this->repository->find($id);

        $this->repository->destroy($job);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('logs::models.of.job')]),
        ]);
    }
}
