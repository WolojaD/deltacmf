<?php

namespace Modules\Logs\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Logs\Repositories\JobFailInterface;
use Modules\Logs\Transformers\JobFailTransformer;
use Modules\Logs\Http\Requests\CreateJobFailRequest;
use Modules\Logs\Http\Requests\UpdateJobFailRequest;
use Modules\Logs\Transformers\FullJobFailTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class JobFailController extends CoreApiController
{
    /**
     * @var JobFailInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(JobFailInterface $jobfail)
    {
        $this->repository = $jobfail;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return JobFailTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return JobFailTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return JobFailTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return JobFailTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullJobFailTransformer
     */
    public function find($id)
    {
        $jobfail = $this->repository->find($id);

        return new FullJobFailTransformer($jobfail);
    }

    /**
     * @param int $id
     * @param UpdateJobFailRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateJobFailRequest $request)
    {
        $jobfail = $this->repository->find($id);

        $this->repository->update($jobfail, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('logs::models.of.jobfail')]),
            'id' => $jobfail->id
        ]);
    }

    /**
     * @param CreateJobFailRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateJobFailRequest $request)
    {
        $jobfail = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('logs::models.of.jobfail')]),
            'id' => $jobfail->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobfail = $this->repository->find($id);

        $this->repository->destroy($jobfail);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('logs::models.of.jobfail')]),
        ]);
    }
}
