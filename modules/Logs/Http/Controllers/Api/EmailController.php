<?php

namespace Modules\Logs\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Logs\Repositories\EmailInterface;
use Modules\Logs\Transformers\EmailTransformer;
use Modules\Logs\Http\Requests\CreateEmailRequest;
use Modules\Logs\Http\Requests\UpdateEmailRequest;
use Modules\Logs\Transformers\FullEmailTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class EmailController extends CoreApiController
{
    /**
     * @var EmailInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(EmailInterface $email)
    {
        $this->repository = $email;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return EmailTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return EmailTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return EmailTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return EmailTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullEmailTransformer
     */
    public function find($id)
    {
        $email = $this->repository->find($id);

        return new FullEmailTransformer($email);
    }

    /**
     * @param int $id
     * @param UpdateEmailRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateEmailRequest $request)
    {
        $email = $this->repository->find($id);

        $this->repository->update($email, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('logs::models.of.email')]),
            'id' => $email->id
        ]);
    }

    /**
     * @param CreateEmailRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmailRequest $request)
    {
        $email = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('logs::models.of.email')]),
            'id' => $email->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $email = $this->repository->find($id);

        $this->repository->destroy($email);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('logs::models.of.email')]),
        ]);
    }
}
