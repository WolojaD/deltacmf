<?php

namespace Modules\Logs\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateEmailRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Logs/Config/models/email.json');
    }

    public function authorize()
    {
        return true;
    }
}
