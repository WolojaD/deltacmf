<?php

namespace Modules\Logs\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateFolderRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Logs/Config/models/folder.json');
    }

    public function authorize()
    {
        return true;
    }
}
