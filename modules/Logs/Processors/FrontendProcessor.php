<?php

namespace Modules\Logs\Processors;

class FrontendProcessor
{
    public function __invoke(array $record)
    {
        $impersonator = \Cookie::get('impersonator_email');

        if ($impersonator !== null) {
            $impersonator_email = \Crypt::decryptString($impersonator);
        }
        $email = auth()->user() ? auth()->user()->email : null;

        $record['extra'] = [
            'email' => $email,
            'impersonator_email' => $impersonator_email ?? null,
            'origin' => request()->headers->get('referer'),
            'path' => request()->path(),
            'ip' => request()->server('REMOTE_ADDR'),
            'user_agent' => request()->server('HTTP_USER_AGENT'),
            'session' => session()->all(),
            'request' => request()->all(),
            'method' => request()->method(),
        ];

        return $record;
    }
}
