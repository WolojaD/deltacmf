<?php

namespace Modules\Logs\Loggers;

use Modules\Logs\Entities\Email;
use Illuminate\Mail\Events\MessageSent;

class LogSentMessage
{
    /**
     * Handle the event.
     *
     * @param MessageSent $event
     */
    public function handle(MessageSent $event)
    {
        $message = $event->message;
        $body = str_replace("\r", '', str_replace("\n", '', $message->getBody()));
        preg_match('/<body.*?>([\s\S]*)<\/body>/', $body, $parsed);

        if (isset($parsed[1])) {
            $body = $parsed[1];
        }

        Email::create([
            'to' => $this->formatAddressField($message, 'To'),
            'subject' => $message->getSubject(),
            'body' => $body,
            'status' => 2
        ]);
    }

    /**
     * Format address strings for sender, to, cc, bcc.
     *
     * @param $message
     * @param $field
     * @return null|string
     */
    public function formatAddressField($message, $field)
    {
        $headers = $message->getHeaders();
        if (!$headers->has($field)) {
            return null;
        }
        $mailboxes = $headers->get($field)->getFieldBodyModel();
        $strings = [];
        foreach ($mailboxes as $email => $name) {
            $mailboxStr = $email;
            if (null !== $name) {
                $mailboxStr = $name . ' <' . $mailboxStr . '>';
            }
            $strings[] = $mailboxStr;
        }

        return implode(', ', $strings);
    }
}
