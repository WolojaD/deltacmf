<?php

namespace Modules\Logs\Loggers;

use Carbon\Carbon;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Modules\Logs\Processors\BackendProcessor;
use Modules\Logs\Processors\FrontendProcessor;

class ActionsLogger
{
    protected $model;

    protected $message;

    protected $extra;

    protected $isBackend;

    public function __construct($model = '', $message = '', $extra = [])
    {
        $this->model = $model;
        $this->message = $message;
        $this->extra = $extra;

        $this->isBackend = str_contains(request()->getPathInfo(), config('application.core.core.admin-prefix', 'backend'));

        if ($model && !is_string($model)) {
            $this->extra['model_id'] = $model->id;
        }
    }

    /**
     * @return bool|\Exception
     */
    public function write()
    {
        if (app()->runningInConsole()) {
            return;
        }

        $processor = $this->isBackend ? new BackendProcessor() : new FrontendProcessor();

        try {
            return (new Logger('admins_log'))
                ->pushProcessor($processor)
                ->pushHandler(new StreamHandler($this->getModelActionPath()))
                ->info($this->message, $this->extra);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return string
     */
    public function getModelActionPath(): string
    {
        $model_name = is_string($this->model) ? $this->model : str_replace(['Modules\\', '\Entities\\'], '', get_class($this->model));
        $folder = $this->isBackend ? 'backend' : 'frontend';

        return storage_path('logs' . DIRECTORY_SEPARATOR . 'actions' . DIRECTORY_SEPARATOR . Carbon::today()->format('Y-m-d') . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $model_name . '.log');
    }
}
