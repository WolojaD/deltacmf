<?php

namespace Modules\Translation\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class TranslationController extends CoreAdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * @param Request $request
     * @param type $lang
     *
     * @return type
     */
    public function changeLocalization(Request $request, $lang)
    {
        if (session()->get('backend_locale')) {
            session()->forget('backend_locale');
        }

        session()->put('backend_locale', $lang);

        return \LaravelLocalization::getLocalizedURL($lang, $request->url);
    }
}
