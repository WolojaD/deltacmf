<?php

namespace Modules\Translation\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Modules\Users\Traits\CanFindUserWithBearerToken;
use Modules\Translation\Services\TranslationsService;
use Modules\Translation\Services\TranslationRevisions;
use Modules\Translation\Repositories\TranslationRepository;

class TranslationController extends Controller
{
    use CanFindUserWithBearerToken;

    /**
     * @var TranslationsService
     */
    private $translationsService;

    /**
     * @var TranslationRepository
     */
    private $translation;

    public function __construct(TranslationsService $translationsService, TranslationRepository $translation)
    {
        $this->translationsService = $translationsService;
        $this->translation = $translation;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $translations = $this->translationsService->getFileAndDatabaseMergedTranslations($request->type)->allRaw();

        foreach ($translations as $lang_name => $languages) {
            $languages = array_filter($languages, function ($item) use ($request) {
                return 'backend' == $request->type ? false === strpos($item, 'frontend::') : 0 === strpos($item, 'frontend::');
            }, ARRAY_FILTER_USE_KEY);

            foreach ($languages as $name => $translation) {
                $name = explode('::', $name);
                $elements = explode('.', $name[1]);
                $subname = array_shift($elements);

                $result[$name[0]][$subname][implode('.', $elements)][$lang_name] = $translation;
            }
        }

        $data = [
            'translations' => $result ?? [],
        ];

        return $data;
    }

    /**
     * @param Request $request
     *
     * @return type
     */
    public function update(Request $request)
    {
        Sentinel::login($this->findUserWithBearerToken($request->header('Authorization')));

        foreach ($request->values as $locale => $word) {
            $this->translation->saveTranslationForLocaleAndKey(
                $locale,
                $request->key,
                $word
            );
        }
    }

    public function clearCache()
    {
        $this->translation->clearCache();
    }

    public function revisions(TranslationRevisions $revisions, Request $request)
    {
        return $revisions->get(
            $request->get('key'),
            $request->get('locale')
        );
    }
}
