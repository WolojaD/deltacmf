<?php

namespace Modules\Translation\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class AllTranslationController extends Controller
{
    public function __invoke()
    {
        return response()->json([
            'page' => trans('page::page'),
            'core' => trans('core::core'),
        ]);
    }
}
