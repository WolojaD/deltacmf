<?php

return [
    'api' => [
        'socialnetwork created' => 'Социальная сеть создана',
        'socialnetwork updated' => 'Социальная сеть обновлена',
        'socialnetwork deleted' => 'Социальная сеть удалена',
    ],
];
