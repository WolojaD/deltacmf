<?php

return [
    'status' => 'Статус',
    'title' => 'Название',
    'count' => 'Articles',
    'link' => 'Cсылка',
    'icon' => 'Иконка',
    'name' => 'Класс кнопки',
    'created_at' => 'Созданно',
    'order' => 'Порядок',
    'actions' => 'Действия',
];
