<?php

return [
    'status' => 'Status',
    'title' => 'Title',
    'count' => 'Articles',
    'link' => 'Link',
    'icon' => 'Icon',
    'name' => 'Button class',
    'created_at' => 'Created',
    'order' => 'Order',
    'actions' => 'Actions',
];
