<?php

return [
    'api' => [
        'socialnetwork created' => 'Social network created',
        'socialnetwork updated' => 'Social network updated',
        'socialnetwork deleted' => 'Social network deleted',
    ],
];
