<?php

return [
    'view telescope' => 'Telescope',
    'view website' => 'На сайт',
    'profile' => 'Профиль',
    'sign out' => 'Выйти',
    'language' => [
        'English' => 'English',
        'Russian' => 'Русский',
    ],
];
