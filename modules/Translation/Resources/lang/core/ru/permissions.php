<?php

return [
    'all buttons' => [
        'allow all' => 'Разрешить все права',
        'inherit all' => 'Наследовать все',
        'deny all' => 'Запретить все права',
    ],
    'group buttons' => [
        'allow all' => 'Разрешить все права',
        'inherit all' => 'Наследовать все',
        'deny all' => 'Запретить все права',
    ],
    'buttons' => [
        'allow' => 'Разрешить',
        'inherit' => 'Наследовать',
        'deny' => 'Запретить',
    ],
    'names' => [
        'index' => 'Список',
        'create' => 'Создать',
        'show' => 'Показать',
        'edit' => 'Редактировать',
        'destroy' => 'Удалить',
    ],
];
