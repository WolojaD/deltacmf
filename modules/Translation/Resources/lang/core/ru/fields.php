<?php

return [
    'slug' => [
        'button' => [
            'generate' => 'Сгенерировать',
        ],
    ],
    'upload-image' => [
        'chose image' => 'Выбрать изображение',
    ],
    'status-element' => 'draft',
    'phone' => [
        'button' => 'Добавить телефон'
    ],
    'upload-file' => [
        'chose file' => 'Выбрать файл',
    ],
    'status' => [
        'not_published' => 'Не опубликовано',
        'published' => 'Опубликовано',
        'draft' => 'Черновик'
    ]
];
