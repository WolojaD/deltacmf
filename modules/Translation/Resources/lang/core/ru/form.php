<?php

return [
    'buttons' => [
        'save' => 'Сохранить',
        'save_and_exit' => 'Сохранить и выйти',
        'cancel' => 'Отменить',
        'delete' => 'Удалить',
        'apply' => 'Применить',
        'duplicate' => 'Копировать',
        'show' => 'Показать',
        'create' => 'Создать',
        'edit' => 'Редактировать',
        'close' => 'Закрыть',
        'suggestions' => 'Предложения',
        'attach-users' => 'Присоединенные пользоавтели',
        'choose' => 'Выбрать',
    ],
    'tabs' => [
        'parameter' => 'Параметр',
        'value' => 'Значение',
    ],
];
