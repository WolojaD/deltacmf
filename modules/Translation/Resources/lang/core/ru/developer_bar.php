<?php

return [
    'edit' => [
        'structure' => 'Редактировать seo структуру',
        'products' => 'Редактировать категорию товаров',
        'product' => 'Редактировать товар',
        'articles' => 'Редактировать категорию статей',
        'news' => 'Редактировать категорию новостей',
        'article' => 'Редактировать статью',
        'new' => 'Редактировать новость'
    ]
];
