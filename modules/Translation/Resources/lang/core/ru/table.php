<?php

return [
    'buttons' => [
        'create' => 'создать',
        'delete' => 'удалить',
        'detach' => 'убрать',
        'go back' => 'вернуться',
        'exportExcel' => 'экспортировать Excel',
        'importExcel' => 'импортировать Excel',
        'generateSitemap' => 'генерировать Sitemap',
        'showSitemap' => 'Посмотреть Sitemap',
        'showSitemapImage' => 'Посмотреть Sitemap Image',
        'queueStart' => 'стартовать очередь',
        'save_items' => 'Сохранить',
        'get_products' => 'Найти Товары',
        'find_add_product' => 'Найти и добавить Товары',
        'attach_user' => 'Прикрепить пользователя',
    ],
    'filter' => [
        'search for' => 'поиск',
        'clear' => 'очистить',
    ],
    'vuetable' => [
        'no data available' => 'Данные недоступны',
    ],
    'pagination info' => [
        'of' => 'из',
        'no relevant data' => 'Нет соответствующих данных',
    ],
];
