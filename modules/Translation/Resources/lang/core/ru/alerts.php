<?php

return [
    'swal' => [
        'title' => 'Вы уверены?',
        'success title' => 'Успешно!',
        'text' => 'Удалить',
        'apply' => 'Применить',
        'delete all text' => 'Удалить выбранные елементы?',
        'gallery text' => 'Галерея будет удаленна',
        'image text' => 'Удалить изображение?',
    ],
    'notify' => [
        'no create permission' => 'Нет разрешения на создание',
        'no edit permission' => 'Нет разрешения на редактирование',
    ],
    'something_wrong' => 'Oops something went wrong',
    'authorization' => [
        'permission denied' => 'Нет разрешения на доступ к модулю: ',
    ],
];
