<?php

return [
    'api' => [
        'fields' => [
            'created' => 'Запись :field добавлена',
            'updated' => 'Запись :field обновлена',
            'deleted' => 'Запись :field удалена',
            'applied' => 'Запись :field применена',
            'attached' => 'Запись :field прикреплена',
            'detached' => 'Запись :field откреплена',
            'multi_attached' => 'Записи :field прикреплены',
            'multi_detached' => 'Записи :field откреплены',
            'not_created' => 'Запись :field не добавлена',
            'not_updated' => 'Запись :field не обновлена',
            'not_deleted' => 'Запись :field не удалена',
            'not_applied' => 'Запись :field не применена',
            'not_attached' => 'Запись :field не прикреплена',
            'not_detached' => 'Запись :field не откреплена',
            'not_multi_attached' => 'Записи :field не прикреплены',
            'not_multi_detached' => 'Записи :field не откреплены',
        ]
    ]
];
