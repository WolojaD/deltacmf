<?php

return [
    'buttons' => [
        'save' => 'Save',
        'save_and_exit' => 'Save and exit',
        'cancel' => 'Cancel',
        'delete' => 'Delete',
        'apply' => 'Apply',
        'duplicate' => 'Duplicate',
        'show' => 'Show',
        'create' => 'Create',
        'edit' => 'Edit',
        'close' => 'Close',
        'suggestions' => 'Suggestions',
        'attach-users' => 'Attached Users',
        'choose' => 'Choose',
    ],
    'tabs' => [
        'parameter' => 'Parameter',
        'value' => 'Value',
    ],
];
