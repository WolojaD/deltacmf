<?php

return [
    'buttons' => [
        'create' => 'create',
        'delete' => 'delete',
        'detach' => 'detach',
        'go back' => 'return',
        'exportExcel' => 'export Excel',
        'importExcel' => 'import Excel',
        'generateSitemap' => 'generate Sitemap',
        'showSitemap' => 'Show Sitemap',
        'showSitemapImage' => 'Show Sitemap Image',
        'queueStart' => 'start queue',
        'save_items' => 'Save',
        'get_products' => 'Get products',
        'find_add_product' => 'Find and add products',
        'attach_user' => 'Attach User',
    ],
    'filter' => [
        'search for' => 'search',
        'clear' => 'clear',
    ],
    'vuetable' => [
        'no data available' => 'Data not available',
    ],
    'pagination info' => [
        'of' => 'of',
        'no relevant data' => 'No relevant data',
    ],
];
