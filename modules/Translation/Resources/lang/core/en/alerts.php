<?php

return [
    'swal' => [
        'title' => 'Are you sure?',
        'success title' => 'Successful!',
        'text' => 'Delete',
        'apply' => 'Apply',
        'delete all text' => 'Are you sure you want to delete the selected items?',
        'gallery text' => 'the gallery will be deleted',
        'image text' => 'Delete image?',
    ],
    'notify' => [
        'no create permission' => 'No create permission',
        'no edit permission' => 'No edit permission',
    ],
    'something_wrong' => 'Oops something went wrong',
    'authorization' => [
        'permission denied' => 'Permission denied for module: ',
    ],
];
