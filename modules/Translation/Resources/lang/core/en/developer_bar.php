<?php

return [
    'edit' => [
        'structure' => 'Edit seo structure',
        'products' => 'Edit products category',
        'product' => 'Edit product',
        'articles' => 'Edit articles category',
        'news' => 'Edit news category',
        'article' => 'Edit article',
        'new' => 'Edit new'
    ]
];
