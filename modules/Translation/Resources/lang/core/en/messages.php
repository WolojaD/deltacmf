<?php

return [
    'api' => [
        'fields' => [
            'created' => ':field record was created',
            'updated' => ':field record was updated',
            'deleted' => ':field record was deleted',
            'applied' => ':field record was applied',
            'attached' => ':field record was attached',
            'detached' => ':field record was detached',
            'multi_detached' => ':field records was detached',
            'multi_attached' => ':field records was attached',
            'not_created' => ':field record was not created',
            'not_updated' => ':field record was not updated',
            'not_deleted' => ':field record was not deleted',
            'not_applied' => ':field record was not applied',
            'not_attached' => ':field record was not attached',
            'not_detached' => ':field record was not detached',
            'not_multi_detached' => ':field records was not detached',
            'not_multi_attached' => ':field records was not attached',
        ]
    ]
];
