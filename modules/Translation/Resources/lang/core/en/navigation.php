<?php

return [
    'view telescope' => 'Telescope',
    'view website' => 'View website',
    'profile' => 'Profile',
    'sign out' => 'Exit',
    'language' => [
        'English' => 'English',
        'Russian' => 'Russian',
    ],
];
