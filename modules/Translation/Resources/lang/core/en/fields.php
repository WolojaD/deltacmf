<?php

return [
    'slug' => [
        'button' => [
            'generate' => 'Generate',
        ],
    ],
    'upload-image' => [
        'chose image' => 'Select image',
    ],
    'status-element' => 'draft',
    'phone' => [
        'button' => 'New phone'
    ],
    'upload-file' => [
        'chose file' => 'Select file',
    ],
    'status' => [
        'not_published' => 'Not published',
        'published' => 'Published',
        'draft' => 'Draft'
    ]
];
