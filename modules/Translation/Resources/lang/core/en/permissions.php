<?php

return [
    'all buttons' => [
        'allow all' => 'Allow all rights',
        'inherit all' => 'Inherit all',
        'deny all' => 'Deny all rights',
    ],
    'group buttons' => [
        'allow all' => 'Allow all rights',
        'inherit all' => 'Inherit all',
        'deny all' => 'Deny all rights',
    ],
    'buttons' => [
        'allow' => 'Allow',
        'inherit' => 'Inherit',
        'deny' => 'Deny',
    ],
    'names' => [
        'index' => 'List',
        'create' => 'Create',
        'show' => 'Show',
        'edit' => 'Edit',
        'destroy' => 'Delete',
    ],
];
