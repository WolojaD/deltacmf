<?php

return [
    'translation' => 'Translations Front-end',
    'frontend' => 'Translations Front-end',
    'backend' => 'Translations Back-end',
    'permissions' => [
        'translation' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
