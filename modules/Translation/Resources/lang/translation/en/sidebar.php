<?php

return [
    'title' => 'Translations',
    'site' => 'Site',
    'newsletters' => 'Newsletters',
    'submenu' => [
        'backend' => 'Admin panel',
        'frontend' => 'Client side',
    ],
];
