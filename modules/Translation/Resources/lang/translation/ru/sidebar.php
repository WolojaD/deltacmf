<?php

return [
    'title' => 'Переводы',
    'site' => 'Сайт',
    'newsletters' => 'Рассылка',
    'submenu' => [
        'backend' => 'Админ панель',
        'frontend' => 'Клиентская часть',
    ],
];
