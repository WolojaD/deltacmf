<?php

return [
    'translation' => 'Переводы Front-end',
    'frontend' => 'Переводы Front-end',
    'backend' => 'Переводы Back-end',
    'permissions' => [
        'translation' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
