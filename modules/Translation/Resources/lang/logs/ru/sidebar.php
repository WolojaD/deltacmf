<?php

return [
    'title' => 'Логи',
    'folders' => 'Логи действий',
    'emails' => 'Логи сообщений',
    'jobs' => 'Очереди',
    'failed_jobs' => 'Ошибки очередей',
];
