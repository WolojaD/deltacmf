<?php

return [
    'sliders' => [
        'status' => 'Status',
        'title' => 'Title',
        'comment' => 'Title',
        'created_at' => 'Created',
        'short_code' => 'Shortcode',
        'actions' => 'Actions',
    ],
    'slides' => [
        'status' => 'Status',
        'title' => 'Title',
        'count' => 'Articles',
        'url' => 'URL',
        'desktop_image' => 'Desktop image',
        'mobile_image' => 'Mobile Image',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
    ],
];
