<?php

return [
    'slider' => 'Slider',
    'slide' => 'Slide',
    'permissions' => [
        'slider' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
        'slide' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
