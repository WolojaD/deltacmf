<?php

return [
    'slider' => 'Слайдер',
    'slide' => 'Слайд',
    'permissions' => [
        'slider' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
        'slide' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
