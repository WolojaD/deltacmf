<?php

return [
    'api' => [
        'forms created' => 'Form created',
        'forms updated' => 'Form updated',
        'forms deleted' => 'Form deleted',
        'fields created' => 'Field created',
        'fields updated' => 'Field updated',
        'fields deleted' => 'Field deleted',
        'regex created' => 'RegExp created',
        'regex updated' => 'RegExp updated',
        'regex deleted' => 'RegExp deleted',
    ],
];
