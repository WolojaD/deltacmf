<?php

return [
    'form' => 'Forms',
    'field' => 'Form fields',
    'field.data' => 'Fields data',
    'regex' => 'RegExp',
    'response' => 'Form response'
];
