<?php

return [
    'forms' => [
        'status' => 'Status',
        'title' => 'Title',
        'short_code' => 'Shortcode',
        'emails' => 'E-mail',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
    ],
    'fields' => [
        'status' => 'Status',
        'title' => 'Title',
        'field_type' => 'Field type',
        'unique_code' => 'Unique code',
        'required' => 'Required',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
    ],
    'regex' => [
        'status' => 'Status',
        'title' => 'Title',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
    ],
    'response' => [
        'tab all' => 'All',
        'title' => 'Froms name',
        'name' => 'Name',
        'email' => 'E-mail',
        'phone' => 'Phone',
        'response_data' => 'Data',
        'created_at' => 'Created',
        'actions' => 'Actions',
        'boolean true' => 'Yes',
        'boolean false' => 'No',
    ],
];
