<?php

return [
    'small_text' => 'String',
    'middle_text' => 'Large text',
    'large_text' => 'Large text',
    'phone' => 'Phone',
    'email' => 'E-mail',
    'integer' => 'Integer',
    'file' => 'File',
    'boolean' => 'Checkbox',
    'separator' => 'Separator',
    'select' => 'Select value',
    'multi_select' => 'Multi select value',
    'hidden' => 'Hidden',
];
