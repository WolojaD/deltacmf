<?php

return [
    'title' => 'Forms',
    'submenu' => [
        'forms' => 'Forms',
        'regex' => 'RegExp',
        'response' => 'Response',
    ],
];
