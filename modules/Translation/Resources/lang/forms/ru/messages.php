<?php

return [
    'api' => [
        'forms created' => 'Форма создана',
        'forms updated' => 'Форма обновлена',
        'forms deleted' => 'Форма удалена',
        'fields created' => 'Поле создано',
        'fields updated' => 'Поле обновлено',
        'fields deleted' => 'Поле удалено',
        'regex created' => 'RegExp создан',
        'regex updated' => 'RegExp обновлен',
        'regex deleted' => 'RegExp удален',
    ],
];
