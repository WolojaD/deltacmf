<?php

return [
    'small_text' => 'Строка',
    'middle_text' => 'Большой текст',
    'large_text' => 'Большой текст',
    'phone' => 'Телефон',
    'email' => 'E-mail',
    'integer' => 'Цифровое значение',
    'file' => 'Файл',
    'boolean' => 'Чекбокс',
    'separator' => 'Разделитель',
    'select' => 'Выбор значения',
    'multi_select' => 'Выбор нескольких значений',
    'hidden' => 'Спрятанное поле',
];
