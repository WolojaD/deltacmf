<?php

return [
    'forms' => [
        'status' => 'Статус',
        'title' => 'Название',
        'short_code' => 'Код вставки',
        'emails' => 'Е-мейлы',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
    'fields' => [
        'status' => 'Статус',
        'title' => 'Название',
        'field_type' => 'Тип поля',
        'unique_code' => 'Уникальный код',
        'required' => 'Обязательное',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
    'regex' => [
        'status' => 'Статус',
        'title' => 'Название',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
    'response' => [
        'tab all' => 'Все',
        'title' => 'Название формы',
        'name' => 'ФИО',
        'email' => 'Е-мейл',
        'phone' => 'Телефон',
        'response_data' => 'Данные',
        'created_at' => 'Созданно',
        'actions' => 'Действия',
        'boolean true' => 'Да',
        'boolean false' => 'Нет',
    ],
];
