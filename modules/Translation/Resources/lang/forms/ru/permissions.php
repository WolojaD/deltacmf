<?php

return [
    'form' => 'Формы',
    'field' => 'Поля форм',
    'field.data' => 'Данные полей',
    'regex' => 'RegExp',
    'response' => 'Заявки форм',
];
