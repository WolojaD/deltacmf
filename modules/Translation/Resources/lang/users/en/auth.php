<?php

return [
    'go-home' => 'Back to site view',
    'form head' => [
        'title' => 'Authorization',
        'description' => 'Do not forget, login and password are case sensitive.',
    ],
    'email' => 'E-mail',
    'password' => 'Password',
    'remember me' => 'Remember me',
    'forgot password' => 'I forgot my password?',
    'login' => 'Login',
    'errors' => [
        'email required' => 'E-mail field is required!',
        'password required' => 'Password field is required!',
        'token mismatch exception' => 'Error! You page session expired! Seems you couldn\'t submit form for a long time. Please try again',
    ],
    'successfully logged in' => 'Successfully authorized',
    'account created check email' => 'The account is created! Please check your email to activate',
    'account created' => 'The account is created!',
    'leave impersonation' => 'Leave impersonation',
];
