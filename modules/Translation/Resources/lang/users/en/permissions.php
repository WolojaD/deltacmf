<?php

return [
    'users' => 'Users',
    'roles' => 'Roles',
    'permissions' => [
        'users' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
            'impersonate' => 'Login like',
        ],
        'roles' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
