<?php

return [
    'title' => 'Users',
    'submenu' => [
        'admins' => 'Admins',
        'users' => 'Users',
        'roles' => 'Roles',
    ],
];
