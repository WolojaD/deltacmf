<?php

return [
    'users' => [
        'user' => [
            'tab_name' => 'User',
            'first_name' => 'Name',
            'last_name' => 'Lastname',
            'email' => 'E-mail',
            'is_activated' => 'Is active ?',
            'administrator' => 'Administrator',
            'user' => 'User',
            'is_admin hint' => 'Only site administrators can enter the admin panel',
        ],
        'role' => [
            'tab_name' => 'Role',
            'roles' => 'Chose role',
        ],
        'permissions' => [
            'tab_name' => 'Permissions',
        ],
        'password' => [
            'tab_name' => 'New password',
            'password' => 'Password',
            'password_confirmation' => 'Password confirmation',
            'send_reset' => 'Send reset password email',
            'send_email' => 'Send on email'
        ],
    ],
    'roles' => [
        'role' => [
            'tab_name' => 'Role',
            'name' => 'Name',
            'is_admin' => 'Type',
        ],
        'permissions' => [
            'tab_name' => 'Permissions',
        ],
        'users' => [
            'tab_name' => 'Users',
            'users with role' => 'Users with role',
        ],
    ],
];
