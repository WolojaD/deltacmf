<?php

return [
    'login' => [
        'invalid login or password' => 'Login or password are invalid',
        'account not validated' => 'Account is not validated',
    ],
    'api' => [
        'user created' => 'User created',
        'user updated' => 'User updated',
        'user deleted' => 'User deleted',
        'user disabled' => 'Cannot change the status of this user',
        'reset password email was sent' => 'Reset password email was sent',
        'reset password email disabled' => 'Password reset is not available at the moment',
        'role created' => 'Role created',
        'role updated' => 'Role updated',
        'role deleted' => 'Role deleted',
        'role disable' => 'The "Developer" role cannot be deleted',
        'role has users' => 'You cannot delete this role, as attached users are',
    ],
    'form users' => [
        'errors' => [
            'first_name required' => 'name is required ',
            'last_name required' => 'lastname is required',
            'email required' => 'email is required',
            'email unique' => 'email must be unique',
            'password required' => 'password is required',
            'roles required' => 'role is required',
        ],
    ],
    'form roles' => [
        'errors' => [
            'name required' => 'name is required',
            'name unique' => 'name must be unique',
            'slug required' => 'slug is required',
            'slug unique' => 'slug must be unique',
        ],
    ],
];
