<?php

return [
    'users' => [
        'status' => 'Status',
        'email' => 'E-mail',
        'fullname' => 'Full name',
        'role' => 'Role',
        'online_status' => 'Online',
        'last_login' => 'Latest login',
        'created_at' => 'Created',
        'impersonate' => 'Login like',
        'impersonate true' => 'Enter',
        'impersonate false' => 'Not enough rights',
        'actions' => 'Actions',
    ],
    'roles' => [
        'name' => 'Name',
        'slug' => 'Unique name',
        'is_admin' => 'Type',
        'created_at' => 'Created',
        'actions' => 'Actions',
        'admin type' => 'Site administrators',
        'user type' => 'Site users',
        'changed rights' => 'User rights changed from standard role',
    ],
    'modal' => [
        'search' => 'Search Users',
        'users' => 'Users',
        'email' => 'Email',
        'first_name' => 'Name',
        'last_name' => 'Second Name',
    ],
];
