<?php

return [
    'user_info' => 'User information',
    'first_name' => 'Name',
    'last_name' => 'Lastname',
];
