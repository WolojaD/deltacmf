<?php

return [
    'users' => [
        'user' => [
            'tab_name' => 'Пользователь',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'email' => 'E-mail',
            'is_activated' => 'Активирован ?',
            'administrator' => 'Администратор',
            'user' => 'Пользователь',
            'is_admin hint' => 'Только администраторы сайта могут заходить в админ панель',
        ],
        'role' => [
            'tab_name' => 'Роль',
            'roles' => 'Выберите роль',
        ],
        'permissions' => [
            'tab_name' => 'Разрешения',
        ],
        'password' => [
            'tab_name' => 'Новый пароль',
            'password' => 'Пароль',
            'password_confirmation' => 'Подтверждение пароля',
            'send_reset' => 'Отправить email восстановления пароля',
            'send_email' => 'Отправить на E-mail'
        ],
    ],
    'roles' => [
        'role' => [
            'tab_name' => 'Роль',
            'name' => 'Название',
            'is_admin' => 'Тип',
        ],
        'permissions' => [
            'tab_name' => 'Разрешения',
        ],
        'users' => [
            'tab_name' => 'Пользователи',
            'users with role' => 'Пользователи с данной ролью',
        ],
    ],
];
