<?php

return [
    'users' => 'Пользователи',
    'roles' => 'Роли',
    'permissions' => [
        'users' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
            'impersonate' => 'Войти как',
        ],
        'roles' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
