<?php

return [
    'title' => 'Пользователи',
    'submenu' => [
        'admins' => 'Администраторы',
        'users' => 'Пользователи',
        'roles' => 'Роли',
    ],
];
