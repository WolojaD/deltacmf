<?php

return [
    'users' => [
        'status' => 'Статус',
        'email' => 'E-mail',
        'fullname' => 'Полное имя',
        'role' => 'Роль',
        'online_status' => 'Онлайн',
        'last_login' => 'Последний вход',
        'created_at' => 'Созданно',
        'impersonate' => 'Войти как',
        'impersonate true' => 'Войти',
        'impersonate false' => 'Недостаточно прав',
        'actions' => 'Действия',
    ],
    'roles' => [
        'name' => 'Название',
        'slug' => 'Уникальное имя',
        'is_admin' => 'Тип',
        'created_at' => 'Созданно',
        'actions' => 'Действия',
        'admin type' => 'Администраторы сайта',
        'user type' => 'Пользователи сайта',
        'changed rights' => 'Права пользователя изменены от стандартной роли',
    ],
    'modal' => [
        'search' => 'Поск пользователей',
        'users' => 'Пользователи',
        'email' => 'Email',
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
    ],
];
