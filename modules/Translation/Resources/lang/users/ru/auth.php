<?php

return [
    'go-home' => 'Вернуться к просмотру сайта',
    'form head' => [
        'title' => 'Авторизация',
        'description' => 'Не забудьте, логин и пароль чувствительны к регистру букв.',
    ],
    'email' => 'E-mail',
    'password' => 'Пароль',
    'remember me' => 'Запомнить меня',
    'forgot password' => 'Я забыл пароль?',
    'login' => 'Войти',
    'errors' => [
        'email required' => 'Поле электронной почты обьязательно!',
        'password required' => 'Поле пароля обьязательно!',
        'token mismatch exception' => 'Ошибка! Ваш сеанс истек! Кажется, вы не отправляли форму в течение длительного времени. Пожалуйста, попробуйте еще раз',
    ],
    'successfully logged in' => 'Успешно авторизирован',
    'account created check email' => 'Аккаунт создан! Пожалуйста, проверьте электронную почту для активации',
    'account created' => 'Аккаунт создан!',
    'leave impersonation' => 'Выйти из пользователя',
];
