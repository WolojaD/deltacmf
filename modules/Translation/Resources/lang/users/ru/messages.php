<?php

return [
    'login' => [
        'invalid login or password' => 'Неправильный логин или пароль',
        'account not validated' => 'Учетная запись не подтверждена',
    ],
    'api' => [
        'user created' => 'Пользователь создан',
        'user updated' => 'Пользователь обновлен',
        'user deleted' => 'Пользователь удален',
        'user disabled' => 'Нельзя изменить статус данного пользователя',
        'reset password email was sent' => 'Сброс пароля отправлено по электронной почте',
        'reset password email disabled' => 'Сброс пароля в данный момент не доступен',
        'role created' => 'Роль создана',
        'role updated' => 'Роль обновлена',
        'role deleted' => 'Роль удалена',
        'role disable' => 'Роль "Разработчика" нельзя удалить',
        'role has users' => 'Вы не можете удалить данную роль, так как к ней прикрепленные пользователи',
    ],
    'form users' => [
        'errors' => [
            'first_name required' => 'Необходимо указать поле имени',
            'last_name required' => 'Необходимо указать поле фамилии',
            'email required' => 'Необходимо указать поле email',
            'email unique' => 'Поле email должно быть уникальным',
            'password required' => 'Необходимо указать поле пароль',
            'roles required' => 'Необходимо указать поле роль',
        ],
    ],
    'form roles' => [
        'errors' => [
            'name required' => 'Необходимо указать поле название',
            'name unique' => 'Поле название должно быть уникальным',
            'slug required' => 'Необходимо указать поле slug',
            'slug unique' => 'Поле slug должно быть уникальным',
        ],
    ],
];
