<?php

return [
    'logs' => [
        'mail' => 'Логи писем',
        'folder' => 'Логи действий',
        'job' => 'Очереди',
        'job.fail' => 'Ошибки очередей',
    ],
    'seo' => [
        'metatag' => 'Метатеги',
        'redirect' => 'Редиректы',
        'structure' => 'Структура',
        'robots' => 'Robots.txt',
    ],
];
