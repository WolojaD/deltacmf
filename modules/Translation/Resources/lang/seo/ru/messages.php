<?php

return [
    'api' => [
        'sitemap generated' => 'Sitemap сгенерирован',
        'import excel success' => 'Файл успешно загружен',
        'import excel error' => 'Произошла ошибка при загрузке файла',
        'robots updated' => 'Файл robots.txt успешно сохранен',
    ],
    'repository' => [
        'url is domain' => 'Старая ссылка не может быть доменом сайта',
        'url the same' => 'Ссылки не могут быть одинаковые',
        'url in the structure' => 'Такая ссылка уже существует в SEO структуре',
    ],
];
