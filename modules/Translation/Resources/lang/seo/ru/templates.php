<?php

return [
    'default' => 'По умолчанию',
    'brands' => 'Бренды',
    'brand' => 'Бренд',
    'sales' => 'Распродажи',
    'sale' => 'Распродажа',
    'news&articles' => 'Новости и статьи',
    'news' => 'Новости',
    'articles' => 'Статьи',
    'new&article' => 'Новость и статья',
    'article' => 'Статья',
    'new' => 'Новость',
    'products' => 'Товары',
    'product' => 'Товар',
    'stores' => 'Магазины',
    'filters' => 'Фильтры',
    'manual' => 'Ручной'
];
