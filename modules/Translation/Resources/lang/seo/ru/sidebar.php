<?php

return [
    'title' => 'SEO',
    'submenu' => [
        'structure' => 'Структура',
        'store' => 'Магазин',
        'manual' => 'Ручные',
        'metatag' => 'Метатеги',
        'redirect' => 'Редирект',
        'robots' => 'Robots.txt',
    ],
];
