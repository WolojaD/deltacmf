<?php

return [
    'redirect' => [
        'status' => [
            '0' => 'Insert',
            '1' => 'Update',
            '2' => 'Deletion',
        ],
    ],
    'robots' => 'Robots.txt file contents'
];
