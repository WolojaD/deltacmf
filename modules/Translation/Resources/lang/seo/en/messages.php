<?php

return [
    'api' => [
        'sitemap generated' => 'Sitemap generated',
        'import excel success' => 'File uploaded successfully',
        'import excel error' => 'An error occurred while loading the file',
        'robots updated' => 'Robots.txt file saved successfully',
    ],
    'repository' => [
        'url is domain' => 'Old link cannot be the site domain',
        'url the same' => 'Links may not be the same',
        'url in the structure' => 'This link already exists in the SEO structure',
    ],
];
