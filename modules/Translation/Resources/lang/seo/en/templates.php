<?php

return [
    'default' => 'Default',
    'brands' => 'Brands',
    'brand' => 'Brand',
    'sales' => 'Sales',
    'sale' => 'Sale',
    'news&articles' => 'News & articles',
    'news' => 'News',
    'articles' => 'Articles',
    'new&article' => 'New & article',
    'article' => 'Article',
    'new' => 'New',
    'products' => 'Products',
    'product' => 'Product',
    'stores' => 'Stores',
    'filters' => 'Filters',
    'manual' => 'Manual'
];
