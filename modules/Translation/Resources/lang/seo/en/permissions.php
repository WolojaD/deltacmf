<?php

return [
    'logs' => [
        'mail' => 'Mail logs',
        'folder' => 'Action logs',
        'job' => 'Jobs',
        'job.fail' => 'Jobs errors',
    ],
    'seo' => [
        'metatag' => 'Metatags',
        'redirect' => 'Redirects',
        'structure' => 'Structure',
        'robots' => 'Robots.txt',
    ],
];
