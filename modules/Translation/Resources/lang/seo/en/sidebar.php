<?php

return [
    'title' => 'SEO',
    'submenu' => [
        'structure' => 'Structure',
        'store' => 'Store',
        'manual' => 'Manual',
        'metatag' => 'Metatag',
        'redirect' => 'Redirect',
        'robots' => 'Robots.txt',
    ],
];
