<?php

return [
    'page' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'slug' => 'Cсылка',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
];
