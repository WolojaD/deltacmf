<?php

return [
    'page' => 'Структура сайта',
    'permissions' => [
        'page' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
