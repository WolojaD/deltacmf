<?php

return [
    'api' => [
        'page created' => 'Страница создана',
        'page updated' => 'Страница обновлена',
        'page deleted' => 'Страница удалена',
        'page disable' => 'Страница не может быть удалена',
    ],
];
