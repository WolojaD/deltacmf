<?php

return [
    'page' => [
        'status' => 'Status',
        'title' => 'Title',
        'slug' => 'Slug',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
    ],
];
