<?php

return [
    'api' => [
        'page created' => 'Page created',
        'page updated' => 'Page updated',
        'page deleted' => 'Page deleted',
        'page disable' => 'Page can not be deleted',
    ],
];
