<?php

return [
    'page' => 'Site structure',
    'permissions' => [
        'page' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
