<?php

return [
    'number of images' => 'Количество изображений:',
    'shortcode' => 'Код вставки:',
    'buttons' => [
        'add gallery' => 'Добавить галерею',
        'go back' => 'Вернутся к галереям',
        'add images' => 'Добавить изображение',
        'hide progress' => 'Спрятать прогресс',
        'show progress' => 'Показать прогресс',
    ],
    'modal' => [
        'modal add title' => 'Создать новую галерею',
        'modal edit title' => 'редактирование галереи',
        'title' => 'Название:',
        'title placeholder' => 'Название:',
        'show_title' => 'Показать заголовок:',
        'display_style' => 'Стиль отображения:',
        'description' => 'Описание:',
        'description placeholder' => 'Описание:',
    ],
    'dropzone' => [
        'title' => 'Бросьте файлы здесь, чтобы загрузить',
        'notify title' => 'Файл загружен',
    ],
    'image parameters' => [
        'file name' => 'Файл:',
        'show file' => 'Посмотреть файл',
        'width' => 'Ширина:',
        'height' => 'Высота:',
        'size' => 'Размер:',
        'uploaded' => 'Загружено:',
        'modified' => 'Обновлено:',
        'modal edit image' => 'Редактирование изображения'
    ],
    'updated message' => [
        'changes saved successfully' => 'Успешно сохранены!',
        'close' => 'Закрыть',
    ],
];
