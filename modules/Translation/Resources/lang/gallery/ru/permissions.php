<?php

return [
    'gallery' => 'Галерея',
    'permissions' => [
        'gallery' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
