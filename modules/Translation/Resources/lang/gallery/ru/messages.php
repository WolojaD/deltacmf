<?php

return [
    'api' => [
        'gallery created' => 'Галерея создана',
        'gallery updated' => 'Галерея обновлена',
        'gallery deleted' => 'Галерея удалена',
        'image updated' => 'Изображение обновлена',
        'image deleted' => 'Изображение удалена',
    ],
];
