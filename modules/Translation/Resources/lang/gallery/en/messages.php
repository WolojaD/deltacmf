<?php

return [
    'api' => [
        'gallery created' => 'Gallert created',
        'gallery updated' => 'Gallert updated',
        'gallery deleted' => 'Gallert deleted',
        'image updated' => 'Image updated',
        'image deleted' => 'Image deleted',
    ],
];
