<?php

return [
    'gallery' => 'Gallery',
    'permissions' => [
        'gallery' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
