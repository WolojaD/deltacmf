<?php

return [
    'number of images' => 'Number of images:',
    'shortcode' => 'Insertion code:',
    'buttons' => [
        'add gallery' => 'Add gallery',
        'go back' => 'Back to galleries',
        'add images' => 'Add an image',
        'hide progress' => 'Hide progress',
        'show progress' => 'Show progress',
    ],
    'modal' => [
        'modal add title' => 'Create a new gallery',
        'modal edit title' => 'edit gallery',
        'title' => 'Title:',
        'title placeholder' => 'Title:',
        'show_title' => 'Show title:',
        'display_style' => 'Display style:',
        'description' => 'Description:',
        'description placeholder' => 'Description:',
    ],
    'dropzone' => [
        'title' => 'Drop the files here to download',
        'notify title' => 'File uploaded',
    ],
    'image parameters' => [
        'file name' => 'File:',
        'show file' => 'Open file',
        'width' => 'Width:',
        'height' => 'Height:',
        'size' => 'Size:',
        'uploaded' => 'Uploaded:',
        'modified' => 'Updated:',
        'modal edit image' => 'Edit image'
    ],
    'updated message' => [
        'changes saved successfully' => 'Successfully saved!',
        'close' => 'Close',
    ],
];
