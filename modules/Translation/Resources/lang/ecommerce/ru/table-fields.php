<?php

return [
    'product_versions_label' => 'Варианты товаров',
    'product_versions' => [
        'sale_discount' => 'Скидка распродажи',
        'status' => 'Статус',
        'article' => 'Артикул',
        'parameters' => 'Параметры',
        'price' => 'Стоимость',
        'discount' => 'Скидка',
        'discount_price' => 'Стоимость со скидкой',
        'quantity' => 'Количество',
        'actions' => 'Действия',
        'add' => 'Добавить',
        'choose' => 'Выберите'
    ],
    'stock' => [
        'quantity' => 'Количество',
        'actions' => 'Действия',
        'add_to_stock' => 'Добавить в магазин',
    ],
    'stock_log' => [
        'to_logs' => 'К логам',
        'export-excel' => 'Экспортировать Excel'
    ],
    'products' => [
        'finded' => 'Найденные товары',
        'search' => 'Поиск товаров',
        'categories' => 'Категории',
        'brands' => 'Бренды',
        'article' => 'Артикул',
        'title' => 'Товар',
        'category' => 'Категория',
        'price' => 'Стоимость'
    ],
    'orders_show' => [
        'title' => 'Данные заказа',
        'name' => 'Имя',
        'show_all_orders' => 'Показать все заказы',
        'phone' => 'Телефон',
        'delivery_type' => 'Способ доставки',
        'payment_type' => 'Способ оплаты',
        'address' => 'Адресс доставки',
        'comment' => 'Комментарий заказчика',
        'total' => 'Общая сумма',
        'uah' => 'грн',
        'delivery' => 'Доставка',
        'total_with_delivery' => 'Общаяя сумма с доставкой',
        'user_data' => 'Данные пользователя',
        'to_orders' => 'К заказам',
        'product_name' => 'Наименование товара',
        'parameters' => 'Параметры',
        'product_discount' => 'Скидка товара',
        'quantity' => 'Количество',
        'price_with_discount' => 'Цена со скидкой',
        'hide' => 'Скрыть',
        'show' => 'Показать',
        'send_order' => 'Отправить заказ',
    ]
];
