<?php

return [
    'brand' => 'Ecommerce Бренды',
    'category' => 'Ecommerce Категории',
    'delivery.pay' => 'Ecommerce Типы оплаты',
    'delivery.type' => 'Ecommerce Типы доставки',
    'order' => 'Ecommerce Заказы',
    'parameter' => 'Ecommerce Параметры',
    'parameter.data' => 'Ecommerce Данные параметров',
    'product' => 'Ecommerce Товары',
    'product.value' => 'Ecommerce Значения товаров',
    'product.version' => 'Ecommerce Версии товаров',
    'sale' => 'Ecommerce Распродажи',
];
