<?php

return [
    'ecommerce' => 'Магазин',
    'products' => 'Товары',
    'brands' => 'Бренды',
    'infos' => 'Параметры',
    'delivery_pays' => 'Типы оплаты',
    'delivery_types' => 'Типы доставки',
    'orders' => 'Заказы',
    'stocks' => 'Остатки',
    'stock_log' => 'Лог остатков',
    'sales' => 'Распродажи',
    'submenu' => [
        'products' => 'Товары',
        'categories' => 'Категории',
        'log' => [
            'full' => 'Остатки по датам с указанием складов',
            'current_day' => 'Остатки по складам на текущую дату',
            'total_by_dates' => 'Остатки по датам по всем складам'
        ]
    ]
];
