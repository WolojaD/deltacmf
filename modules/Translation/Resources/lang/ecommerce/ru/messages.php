<?php

return [
    'order.content' => 'Содержимое заказа',
    'discount_changed' => 'Скидка изменена',
    'sale.ended' => 'завершена',
    'sale.active' => 'активна',
    'errors' => [
        'no_versions' => 'Одна версия товара обязательна'
    ]
];
