<?php

return [
    'of' => [
        'brand' => 'бренда',
        'category' => 'категории',
        'delivery_pay' => 'типа оплаты',
        'delivery_type' => 'типа доставки',
        'order' => 'заказа',
        'parameter' => 'параметра',
        'parameter_data' => 'данных параметра',
        'product' => 'товара',
        'product_value' => 'характеристики товара',
        'product_version' => 'версии товара',
        'sale' => 'распродажи',
    ]
];
