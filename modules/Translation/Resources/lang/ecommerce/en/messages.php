<?php

return [
    'order.content' => 'Order content',
    'sale.ended' => 'ended',
    'sale.active' => 'active',
    'discount_changed' => 'Discount changed',
    'errors' => [
        'no_versions' => 'One version of product is required'
    ]
];
