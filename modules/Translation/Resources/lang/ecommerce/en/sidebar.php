<?php

return [
    'ecommerce' => 'Ecommerce',
    'products' => 'Products',
    'brands' => 'Brands',
    'infos' => 'Parameters',
    'delivery_pays' => 'Pay types',
    'delivery_types' => 'Delivery types',
    'orders' => 'Orders',
    'stocks' => 'Stocks',
    'stock_log' => 'Stock Log',
    'sales' => 'Sales',
    'submenu' => [
        'products' => 'Products',
        'categories' => 'Categories',
        'log' => [
            'full' => 'Remains on dates with the indication of warehouses',
            'current_day' => 'Remains of warehouses on the current date',
            'total_by_dates' => 'Remains of dates for all warehouses'
        ]
    ]
];
