<?php

return [
    'brand' => 'Ecommerce Brand',
    'category' => 'Ecommerce Category',
    'delivery.pay' => 'Ecommerce Delivery Pay',
    'delivery.type' => 'Ecommerce Delivery Type',
    'order' => 'Ecommerce Order',
    'parameter' => 'Ecommerce Parameter',
    'parameter.data' => 'Ecommerce Parameter data',
    'product' => 'Ecommerce Product',
    'product.value' => 'Ecommerce Product value',
    'product.version' => 'Ecommerce Product version',
    'sale' => 'Ecommerce Sale',
];
