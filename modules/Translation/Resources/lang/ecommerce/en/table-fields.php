<?php

return [
    'product_versions_label' => 'Product versions',
    'product_versions' => [
        'sale_discount' => 'Sale discount',
        'status' => 'Status',
        'article' => 'Article',
        'parameters' => 'Parameters',
        'price' => 'Price',
        'discount' => 'Discount',
        'discount_price' => 'Discount price',
        'quantity' => 'Quantity',
        'actions' => 'Actions',
        'add' => 'Add',
        'choose' => 'Choose'
    ],
    'stock' => [
        'quantity' => 'Quantity',
        'actions' => 'Actions',
        'add_to_stock' => 'Add to stock',
    ],
    'stock_log' => [
        'to_logs' => 'Back to log',
        'export-excel' => 'Export Excel'
    ],
    'products' => [
        'finded' => 'Finded products',
        'search' => 'Search products',
        'categories' => 'Categories',
        'brands' => 'Brands',
        'article' => 'Article',
        'title' => 'Product',
        'category' => 'Category',
        'price' => 'Price'
    ],
    'orders_show' => [
        'title' => 'Order data',
        'name' => 'Name',
        'show_all_orders' => 'Show all orders',
        'phone' => 'Phone',
        'delivery_type' => 'Delivery type',
        'payment_type' => 'Payment type',
        'address' => 'Delivery address',
        'comment' => 'Order comment',
        'total' => 'Total price',
        'uah' => 'uah',
        'delivery' => 'Delivery',
        'total_with_delivery' => 'Total price with discount',
        'user_data' => 'User data',
        'to_orders' => 'To orders',
        'product_name' => 'Product name',
        'parameters' => 'Prameters',
        'product_discount' => 'Product discount',
        'quantity' => 'Quantity',
        'price_with_discount' => 'Price with discount',
        'hide' => 'Hide',
        'show' => 'Show',
        'send_order' => 'Send order',
    ]
];
