<?php

return [
    'of' => [
        'brand' => 'of brand',
        'category' => 'of category',
        'delivery_pay' => 'of payment type',
        'delivery_type' => 'of delivery type',
        'order' => 'of order',
        'parameter' => 'of parameter',
        'parameter_data' => 'of parameter data',
        'product' => 'of product',
        'product_value' => 'of product value',
        'product_version' => 'of product version',
        'sale' => 'of sale',
    ]
];
