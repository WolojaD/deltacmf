<?php

return [
    'settings' => [
        'fields' => [
            'locales' => [
                'primary locale' => 'Primary locale',
                'secondary locales' => 'Secondary locales',
            ],
        ],
    ],
    'image' => [
        'title' => 'Title',
        'width' => 'Width',
        'height' => 'Height',
        'status' => 'Crop',
        'actions' => 'Actions',
    ],
];
