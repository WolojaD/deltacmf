<?php

return [
    'settings' => 'Settings',
    'image' => 'Image dimensions',
    'permissions' => [
        'settings' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
