<?php

return [
    'title' => 'Settings',
    'submenu' => [
        'core' => 'Parameters',
        'dashboard' => 'Dashboard',
        'image' => 'Image dimensions',
        'fields' => 'Fields',
        'logs' => 'Logs',
        'cache_clear' => 'Clear cache',
        'storage_link' => 'Link \'storage\' folder',
        'cache_config' => 'Create \'config cache\'',
    ],
];
