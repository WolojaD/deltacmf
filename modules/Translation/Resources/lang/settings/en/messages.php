<?php

return [
    'settings saved' => 'Settings saved',
    'settings updated' => 'Settings updated',
    'сache cleared' => 'Cache cleared',
    'storage link' => [
        'unlink error' => 'Error deleting \'storage\' directory',
        'try error' => 'Error attempting to delete the \'storage\' directory',
        'storage linked' => 'Directory \'storage\' successfully linked',
    ],
    'сonfig cached' => 'Application settings successfully cached',
];
