<?php

return [
    'title' => [
        'settings' => 'Settings',
        'general settings' => 'General settings',
        'module settings' => 'Module settings',
        'module name settings' => 'Module :module settings',
    ],
    'breadcrumb' => [
        'settings' => 'Settings',
        'module settings' => 'Module :module settings',
    ],
    'tabs' => [
        'articles' => 'Articles',
        'core' => 'Settings',
        'dashboard' => 'Dashboard',
        'emailtemplates' => 'Mail',
        'users' => 'Users',
        'page' => 'Pages',
        'ecommerce' => 'Ecommerce',
        'newsletters' => 'Newsletters',
        'seo' => 'Seo',
    ],
];
