<?php

return [
    'settings' => [
        'fields' => [
            'locales' => [
                'primary locale' => 'Основной язык',
                'secondary locales' => 'Второстепенные языки',
            ],
        ],
    ],
    'image' => [
        'title' => 'Название',
        'width' => 'Ширина',
        'height' => 'Высота',
        'status' => 'Обрезать',
        'actions' => 'Действия',
    ],
];
