<?php

return [
    'title' => 'Настройки',
    'submenu' => [
        'core' => 'Параметры',
        'dashboard' => 'Панель',
        'image' => 'Размеры изображений',
        'logs' => 'Логи',
        'fields' => 'Поля',
        'cache_clear' => 'Очистить кэш',
        'storage_link' => 'Cоздать линк папки \'storage\'',
        'cache_config' => 'Создать \'config cache\'',
    ],
];
