<?php

return [
    'settings' => 'Настройки',
    'image' => 'Размеры изображений',
    'permissions' => [
        'settings' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
