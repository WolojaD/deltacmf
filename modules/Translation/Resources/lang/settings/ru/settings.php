<?php

return [
    'title' => [
        'settings' => 'Настройки',
        'general settings' => 'Общие настройки',
        'module settings' => 'Настройки модулей',
        'module name settings' => 'Настройки модуля :module',
    ],
    'breadcrumb' => [
        'settings' => 'Настройки',
        'module settings' => 'Настройки модуля :module',
    ],
    'tabs' => [
        'articles' => 'Статьи',
        'core' => 'Настройки',
        'dashboard' => 'Панель',
        'emailtemplates' => 'Почта',
        'users' => 'Пользователи',
        'page' => 'Структура',
        'ecommerce' => 'Магазин',
        'newsletters' => 'Рассылка',
        'seo' => 'Seo',
    ],
];
