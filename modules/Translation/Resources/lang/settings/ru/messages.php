<?php

return [
    'settings saved' => 'Настройки сохранены',
    'settings updated' => 'Настройки обновлены',
    'сache cleared' => 'Кеш очищен',
    'storage link' => [
        'unlink error' => 'Ошибка в удалении директории \'storage\'',
        'try error' => 'Ошибка при попытке удаления директории \'storage\'',
        'storage linked' => 'Директории \'storage\' успешно привязана',
    ],
    'сonfig cached' => 'Настройки приложения успешно кешированы',
];
