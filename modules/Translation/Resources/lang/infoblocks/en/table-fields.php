<?php

return [
    'infoblocks' => [
        'status' => 'Status',
        'title' => 'Title',
        'description' => 'Description',
        'infoblock_template' => 'Template',
        'shortcode' => 'Shortcode',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
    ],
];
