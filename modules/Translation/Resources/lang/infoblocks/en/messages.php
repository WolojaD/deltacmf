<?php

return [
    'api' => [
        'infoblock created' => 'Infoblock created',
        'infoblock updated' => 'Infoblock updated',
        'infoblock deleted' => 'Infoblock deleted',
        'template has groups' => 'You cannot delete a template, as information blocks are attached to it',
    ],
];
