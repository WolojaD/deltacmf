<?php

return [
    'infoblock' => 'Infoblocks',
    'permissions' => [
        'infoblocks' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
