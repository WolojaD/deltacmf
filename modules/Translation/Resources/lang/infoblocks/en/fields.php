<?php

return [
    'title' => 'Title',
    'description' => 'Description',
    'content' => 'Content',
    'image' => 'Image',
    'desktop_image' => 'Desktop image',
    'mobile_image' => 'Mobile image',
    'file' => 'File or icon',
    'translatable_desktop_image' => 'Translatable desktop image',
    'translatable_mobile_image' => 'Translatable mobile image',
    'url' => 'URL',
    'url_title' => 'URL title',
    'video' => 'Video',
];
