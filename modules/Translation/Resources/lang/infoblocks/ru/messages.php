<?php

return [
    'api' => [
        'infoblock created' => 'Инфоблок создан',
        'infoblock updated' => 'Инфоблок обновлен',
        'infoblock deleted' => 'Инфоблок удален',
        'template has groups' => 'Вы не можете удалить шаблон, так как к нему прикрепленны инфоблоки',
    ],
];
