<?php

return [
    'infoblock' => 'Инфоблоки',
    'permissions' => [
        'infoblocks' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
