<?php

return [
    'title' => 'Название',
    'description' => 'Описание',
    'content' => 'Контент',
    'image' => 'Изображение',
    'desktop_image' => 'Десктопное изображение',
    'mobile_image' => 'Мобильное изображение',
    'file' => 'Файл или иконка',
    'translatable_desktop_image' => 'Мультиязычное десктопное изображение',
    'translatable_mobile_image' => 'Мультиязычное мобильное изображение',
    'url' => 'URL',
    'url_title' => 'Название URL',
    'video' => 'Видео или ссылка на видео',
];
