<?php

return [
    'infoblocks' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'description' => 'Описание',
        'infoblock_template' => 'Шаблон',
        'shortcode' => 'Код вставки',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
];
