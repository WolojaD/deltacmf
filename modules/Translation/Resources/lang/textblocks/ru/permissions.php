<?php

return [
    'text.block' => 'Текстовые блоки',
    'permissions' => [
        'textblock' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
