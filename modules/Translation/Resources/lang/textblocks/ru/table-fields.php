<?php

return [
    'textblock' => [
        'short_code' => 'Код вставки',
        'comment' => 'Коментарий',
        'body' => 'Содержание',
        'created_at' => 'Создано',
        'actions' => 'Действия',
    ],
];
