<?php

return [
    'textblock' => [
        'short_code' => 'Shortcode',
        'comment' => 'Comment',
        'body' => 'Body',
        'created_at' => 'Created',
        'actions' => 'Actions',
    ],
];
