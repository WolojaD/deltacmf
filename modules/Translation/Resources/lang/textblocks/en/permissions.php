<?php

return [
    'text.block' => 'Text blocks',
    'permissions' => [
        'textblock' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
