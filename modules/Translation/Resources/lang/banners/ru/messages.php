<?php

return [
    'api' => [
        'banners created' => 'Группа баннеров создана',
        'banners updated' => 'Группа баннеров обновлена',
        'banners deleted' => 'Группа баннеров удалена',
        'banner created' => 'Баннер создан',
        'banner updated' => 'Баннер обновлен',
        'banner deleted' => 'Баннер удален',
    ],
];
