<?php

return [
    'device kind' => 'Устройство',
    'device model' => 'Система',
    'device platform' => 'Платформа',
    'device platform version' => 'Версия платформы',
    'agent name' => 'Агент',
    'agent browser' => 'Браузер',
    'agent browser version' => 'Версия браузера',
    'referer_domain' => 'Need to test',
    'referer_url' => 'Need to test',
    'referer_host' => 'Need to test',
    'referer_medium' => 'Need to test',
    'referer_source' => 'Need to test',
    'referer_search_terms_hash' => 'Need to test',
    'no' => 'Нет',
    'yes' => 'Да',
];
