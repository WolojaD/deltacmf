<?php

return [
    'banners' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'short_code' => 'Код вставки',
        'banners_active' => 'Активные баннеры',
        'created_at' => 'Созданно',
        'actions' => 'Действия',
        'weight' => 'Вес',
        'translatable_desktop_image' => 'Превью изображения',
        'statistics' => 'Статистика',
    ],
    'statistics_by_days' => [
        'date' => 'Дата',
        'clicked' => 'Переходы',
        'not_clicked' => 'Просмотры',
        'actions' => 'Действия',
    ],
    'statistics_full' => [
        'created_at' => 'Созданно',
        'ip' => 'IP',
        'device_is_mobile' => 'Телефон',
        'is_robot' => 'Робот',
        'is_click' => 'Нажато',
        'device_data' => 'Данные устройства',
        'agent_data' => 'Данные браузера',
        'referer_data' => 'Данные Referer',
        'actions' => 'Действия',
    ],
];
