<?php

return [
    'api' => [
        'banners created' => 'Banners group created',
        'banners updated' => 'Banners group updated',
        'banners deleted' => 'Banners group deleted',
        'banner created' => 'Banner created',
        'banner updated' => 'Banner updated',
        'banner deleted' => 'Banner deleted',
    ],
];
