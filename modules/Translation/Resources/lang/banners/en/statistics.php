<?php

return [
    'device kind' => 'Device',
    'device model' => 'OS',
    'device platform' => 'Platform',
    'device platform version' => 'Platform version',
    'agent name' => 'Agent',
    'agent browser' => 'Browser',
    'agent browser version' => 'Browser version',
    'referer_domain' => 'Need to test',
    'referer_url' => 'Need to test',
    'referer_host' => 'Need to test',
    'referer_medium' => 'Need to test',
    'referer_source' => 'Need to test',
    'referer_search_terms_hash' => 'Need to test',
    'no' => 'No',
    'yes' => 'Yes',
];
