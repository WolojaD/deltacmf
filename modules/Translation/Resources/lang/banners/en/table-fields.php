<?php

return [
    'banners' => [
        'status' => 'Status',
        'title' => 'Title',
        'short_code' => 'Short code',
        'banners_active' => 'Active banners',
        'created_at' => 'Created',
        'actions' => 'Actions',
        'weight' => 'Weight',
        'translatable_desktop_image' => 'Preview image',
        'statistics' => 'Statistics',
    ],
    'statistics_by_days' => [
        'date' => 'Date',
        'clicked' => 'Clicks',
        'not_clicked' => 'Views',
        'actions' => 'Actions',
    ],
    'statistics_full' => [
        'created_at' => 'Created',
        'ip' => 'IP',
        'device_is_mobile' => 'Phone',
        'is_robot' => 'Robot',
        'is_click' => 'Click',
        'device_data' => 'Device data',
        'agent_data' => 'Agent data',
        'referer_data' => 'Referer data',
        'actions' => 'Actions',
    ],
];
