<?php

return [
    'api' => [
        'post created' => 'Post created',
        'post updated' => 'Post updated',
        'post deleted' => 'Post deleted',
        'category created' => 'Category created',
        'category updated' => 'Category updated',
        'category deleted' => 'Category deleted',
    ],
];
