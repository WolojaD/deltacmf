<?php

return [
    'title' => 'Stores',
    'submenu' => [
        'stores' => 'Stores',
        'cities' => 'Cities',
    ],
];
