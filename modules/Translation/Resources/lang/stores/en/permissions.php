<?php

return [
    'stores' => 'Stores',
    'store' => 'Stores',
    'cities' => 'Cities',
    'city' => 'Cities',
    'permissions' => [
        'stores' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
        'cities' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
