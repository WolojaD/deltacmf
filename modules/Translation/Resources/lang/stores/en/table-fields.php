<?php

return [
    'articles' => [
        'status' => 'Status',
        'title' => 'Title',
        'description' => 'Description',
        'slug' => 'Slug',
        'mobile_image' => 'Mobile Image',
        'desktop_image' => 'Desktop image',
        'preview_image' => 'Preview image',
        'category.title' => 'Category',
        'date_from' => 'Date from',
        'date_to' => 'Date to',
        'actions' => 'Actions',
    ],
    'categories' => [
        'status' => 'Status',
        'title' => 'Title',
        'posts_count' => 'Articles',
        'slug' => 'Slug',
        'created_at' => 'Created',
        'order' => 'Order',
        'actions' => 'Actions',
        'category news' => 'Новости категории',
    ],
    'button' => [
        'add' => 'Add',
        'add_user' => 'Add user',
    ]
];
