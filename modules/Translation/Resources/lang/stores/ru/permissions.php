<?php

return [
    'stores' => 'Магазины',
    'store' => 'Магазины',
    'cities' => 'Города',
    'city' => 'Города',
    'permissions' => [
        'stores' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
        'cities' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
