<?php

return [
    'title' => 'Магазины',
    'submenu' => [
        'stores' => 'Магазины',
        'cities' => 'Города',
    ],
];
