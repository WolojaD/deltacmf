<?php

return [
    'stores' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'category.title' => 'Категория',
        'date_from' => 'Дата c',
        'date_to' => 'Дата по',
        'actions' => 'Действия',
    ],
    'cities' => [
        'status' => 'Статус',
        'title' => 'Название',
        'posts_count' => 'Статьи',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
    'button' => [
        'add' => 'Добавить',
        'add_user' => 'Добавить пользователя',
    ],
    'store_users' => [
        'status' => 'Статус',
        'id' => 'ID',
        'name' => 'Имя',
        'actions' => 'Действия',
    ]
];
