<?php

return [
    'dashboard' => 'Dashboard',
    'permissions' => [
        'dashboard' => [
            'index' => 'View',
        ],
    ],
];
