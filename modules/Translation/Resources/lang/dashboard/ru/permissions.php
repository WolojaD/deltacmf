<?php

return [
    'dashboard' => 'Панель',
    'permissions' => [
        'dashboard' => [
            'index' => 'Просмотр',
        ],
    ],
];
