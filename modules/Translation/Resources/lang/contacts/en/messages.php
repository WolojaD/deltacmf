<?php

return [
    'api' => [
        'slider created' => 'Slider created',
        'slider updated' => 'Slider updated',
        'slider deleted' => 'Slider deleted',
        'slide created' => 'Slide created',
        'slide updated' => 'Slide updated',
        'slide deleted' => 'Slide deleted',
    ],
];
