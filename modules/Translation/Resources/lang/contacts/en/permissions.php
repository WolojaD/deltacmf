<?php

return [
    'group' => 'Group of contacts',
    'contact' => 'Contact',
    'permissions' => [
        'group' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
        'contact' => [
            'index' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'destroy' => 'Delete',
        ],
    ],
];
