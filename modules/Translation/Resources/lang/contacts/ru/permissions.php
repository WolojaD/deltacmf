<?php

return [
    'group' => 'Группы контактов',
    'contact' => 'Контакт',
    'permissions' => [
        'group' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
        'contact' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
