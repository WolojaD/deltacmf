<?php

return [
    'phone' => 'Телефон',
    'email' => 'E-mail',
    'link' => 'Ссылка',
    'other' => 'Произвольное поле'
];
