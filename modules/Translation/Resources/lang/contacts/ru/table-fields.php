<?php

return [
    'sliders' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'comment' => 'Название',
        'created_at' => 'Созданно',
        'short_code' => 'Код вставки',
        'actions' => 'Действия',
    ],
    'slides' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'count' => 'Articles',
        'url' => 'Cсылка',
        'desktop_image' => 'Десктопное изображение',
        'mobile_image' => 'Мобильное изображение',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
    ],
];
