<?php

return [
    'api' => [
        'slider created' => 'Слайдер создан',
        'slider updated' => 'Слайдер обновлен',
        'slider deleted' => 'Слайдер удален',
        'slide created' => 'Слайд создан',
        'slide updated' => 'Слайд обновлен',
        'slide deleted' => 'Слайд удален',
    ],
];
