<?php

return [
    'api' => [
        'post created' => 'Запись создана',
        'post updated' => 'Запись обновлена',
        'post deleted' => 'Запись удалена',
        'category created' => 'Категория создана',
        'category updated' => 'Категория обновлена',
        'category deleted' => 'Категория удалена',
    ],
];
