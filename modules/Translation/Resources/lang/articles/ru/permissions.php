<?php

return [
    'post' => 'Статьи',
    'category' => 'Категории статей',
    'permissions' => [
        'post' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
        'category' => [
            'index' => 'Просмотр',
            'create' => 'Создать',
            'edit' => 'Редактировать',
            'destroy' => 'Удалить',
        ],
    ],
];
