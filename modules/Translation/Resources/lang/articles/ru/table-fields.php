<?php

return [
    'articles' => [
        'status' => 'Статус',
        'title' => 'Заголовок',
        'description' => 'Анонс',
        'slug' => 'Cсылка',
        'mobile_image' => 'Мобильное изображение',
        'desktop_image' => 'Десктопное изображение',
        'preview_image' => 'Изображение предпросмотра',
        'category.title' => 'Категория',
        'date_from' => 'Дата c',
        'date_to' => 'Дата по',
        'actions' => 'Действия',
    ],
    'categories' => [
        'status' => 'Статус',
        'title' => 'Название',
        'posts_count' => 'Статьи',
        'slug' => 'Cсылка',
        'created_at' => 'Созданно',
        'order' => 'Порядок',
        'actions' => 'Действия',
        'category news' => 'Новости категории',
    ],
];
