<?php

return [
    'title' => 'Articles',
    'submenu' => [
        'posts' => 'Posts',
        'categories' => 'Categories',
    ],
];
