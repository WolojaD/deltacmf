<?php

namespace Modules\Translation\Services;

use Modules\Translation\ValueObjects\TranslationGroup;
use Modules\Translation\Repositories\TranslationRepository;
use Modules\Translation\Repositories\FileTranslationRepository;

class TranslationsService
{
    /**
     * @var FileTranslationRepository
     */
    private $fileTranslations;

    /**
     * @var TranslationRepository
     */
    private $databaseTranslations;

    public function __construct()
    {
        $this->fileTranslations = app(FileTranslationRepository::class);
        $this->databaseTranslations = app(TranslationRepository::class);
    }

    /**
     * Get the file translations & the database translations, overwrite the file translations by db translations.
     *
     * @param string $request_type
     *
     * @return TranslationGroup
     */
    public function getFileAndDatabaseMergedTranslations(string $request_type)
    {
        $allFileTranslations = $this->fileTranslations->all($request_type);
        $allDatabaseTranslations = $this->databaseTranslations->allFormatted($request_type);

        foreach ($allFileTranslations as $locale => $fileTranslation) {
            foreach ($fileTranslation as $key => $translation) {
                if (false === is_string($translation)) {
                    unset($allFileTranslations[$locale][$key]);
                }

                if (isset($allDatabaseTranslations[$locale][$key])) {
                    $allFileTranslations[$locale][$key] = $allDatabaseTranslations[$locale][$key];

                    unset($allDatabaseTranslations[$locale][$key]);
                }
            }
        }

        $this->addDatabaseOnlyTranslations($allFileTranslations, $allDatabaseTranslations);
        $this->filterOnlyActiveLocales($allFileTranslations, $request_type);

        return new TranslationGroup($allFileTranslations);
    }

    /**
     * Filter out the non-active locales.
     *
     * @param array &$allFileTranslations
     * @param string $request_type
     *
     * @return type
     */
    private function filterOnlyActiveLocales(array &$allFileTranslations, string $request_type)
    {
        $activeLocales = $this->getActiveLocales($request_type);

        foreach ($allFileTranslations as $locale => $value) {
            if (!in_array($locale, $activeLocales)) {
                unset($allFileTranslations[$locale]);
            }
        }
    }

    /**
     * Get the currently active locales.
     *
     * @param string $request_type
     *
     * @return type
     */
    private function getActiveLocales(string $request_type)
    {
        $locales = [];

        if ('frontend' == $request_type) {
            foreach (get_application_frontend_locales() as $locale => $translation) {
                $locales[] = $locale;
            }
        } else {
            foreach (config('application.core.available-locales-backend') as $locale => $translation) {
                $locales[] = $locale;
            }
        }

        return $locales;
    }

    /**
     * @param array $allFileTranslations
     * @param array $allDatabaseTranslations
     */
    private function addDatabaseOnlyTranslations(array &$allFileTranslations, array $allDatabaseTranslations)
    {
        foreach ($allDatabaseTranslations as $locale => $group) {
            foreach ($group as $key => $value) {
                $allFileTranslations[$locale][$key] = $value;
            }
        }
    }
}
