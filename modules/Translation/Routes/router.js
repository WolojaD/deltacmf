import helpers from '@services/helpers'

const allBackendLocales = helpers.appStorage().allBackendLocales
const allFrontendLocales = helpers.appStorage().allFrontendLocales

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/translation',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: 'backend',
                name: 'api.backend.translation.backend',
                component: require('@/views/translation/index').default,
                props: {
                    allLocales: allBackendLocales,
                    type: 'backend'
                },
                meta: {
                    pageTitle: 'Translation backend',
                    breadcrumb: [
                        { name: 'Translation backend' }
                    ]
                }
            },
            {
                path: 'frontend',
                name: 'api.backend.translation.frontend',
                component: require('@/views/translation/index').default,
                props: {
                    allLocales: allFrontendLocales,
                    type: 'frontend'
                },
                meta: {
                    pageTitle: 'Translation frontend',
                    breadcrumb: [
                        { name: 'Translation frontend' }
                    ]
                }
            }
        ]
    }
]
