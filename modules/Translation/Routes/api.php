<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'translation', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('/', ['as' => 'api.backend.translation.index', 'uses' => 'TranslationController@index']);

    Route::put('front-update', ['as' => 'api.backend.translation.frontend.update', 'uses' => 'TranslationController@update', 'middleware' => 'token-can:translation.frontend.edit']);

    Route::put('back-update', ['as' => 'api.backend.translation.backend.update', 'uses' => 'TranslationController@update', 'middleware' => 'token-can:translation.backend.edit']);

    Route::post('clear-cache', ['as' => 'api.backend.translation.clearCache', 'uses' => 'TranslationController@clearCache']);

    Route::post('revisions', ['as' => 'api.backend.translation.revisions', 'uses' => 'TranslationController@revisions']);
});
