<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'translation'], function () {
    Route::bind('translations', function ($id) {
        return \Modules\Translation\Entities\TranslationTranslation::find($id);
    });

    Route::get('backend', ['as' => 'backend.translation.backend', 'uses' => 'TranslationController@index', 'middleware' => 'can:translation.backend.index']);
    Route::get('frontend', ['as' => 'backend.translation.frontend', 'uses' => 'TranslationController@index', 'middleware' => 'can:translation.frontend.index']);

    Route::get('change-localization/{lang}', ['as' => 'backend.translation.changeLocalization', 'uses' => 'TranslationController@changeLocalization']);
});
