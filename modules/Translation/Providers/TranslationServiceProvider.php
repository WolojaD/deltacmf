<?php

namespace Modules\Translation\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Modules\Core\Providers\ServiceProvider;
use Modules\Translation\Entities\Translation;
use Modules\Core\Composers\CurrentUserViewComposer;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Translation\Services\TranslationLoader;
use Modules\Translation\Repositories\TranslationRepository;
use Modules\Translation\Repositories\FileTranslationRepository;
use Modules\Translation\Repositories\Cache\CacheTranslationDecorator;
use Modules\Translation\Repositories\Eloquent\EloquentTranslationRepository;
use Modules\Translation\Repositories\File\FileTranslationRepository as FileDiskTranslationRepository;

class TranslationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        view()->composer('translation::backend.translation.index', CurrentUserViewComposer::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            // $event->load('translations', array_dot(trans('translation::translations')));
            $event->load('translation_sidebar', array_dot(trans('translation::sidebar')));
            $event->load('translation_permissions', array_dot(trans('translation::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('translation', 'config');
        $this->publishConfig('translation', 'permissions');

        $this->registerValidators();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        if ($this->shouldRegisterCustomTranslator()) {
            $this->registerCustomTranslator();
        }
    }

    /**
     * Should we register the Custom Translator?
     *
     * @return bool
     */
    protected function shouldRegisterCustomTranslator()
    {
        if (false === config('app.translations-gui', true)) {
            return false;
        }

        if (false === Schema::hasTable((new Translation())->getTable())) {
            return false;
        }

        return true;
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->bind(TranslationRepository::class, function () {
            $repository = new EloquentTranslationRepository(new Translation());

            return new CacheTranslationDecorator($repository);
        });

        $this->app->bind(FileTranslationRepository::class, function ($app) {
            return new FileDiskTranslationRepository($app['files'], $app['translation.loader']);
        });
    }

    protected function registerCustomTranslator()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new TranslationLoader($app['files'], $app['path.lang']);
        });

        $this->app->singleton('translator', function ($app) {
            $loader = $app['translation.loader'];

            $locale = $app['config']['app.locale'];

            $trans = new \Illuminate\Translation\Translator($loader, $locale);

            $trans->setFallback($app['config']['app.fallback_locale']);

            return $trans;
        });
    }

    private function registerValidators()
    {
        Validator::extend('extensions', function ($attribute, $value, $parameters) {
            return in_array($value->getClientOriginalExtension(), $parameters);
        });

        Validator::replacer('extensions', function ($message, $attribute, $rule, $parameters) {
            return str_replace([':attribute', ':values'], [$attribute, implode(',', $parameters)], $message);
        });
    }
}
