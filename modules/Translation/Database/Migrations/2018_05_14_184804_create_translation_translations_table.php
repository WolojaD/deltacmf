<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('translation_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('value');
            $table->integer('translation_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['translation_id', 'locale'], 'translations_trans_id_locale_unique');
            $table->foreign('translation_id')->references('id')->on('translations')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('translation_translations');
    }
}
