<?php

namespace Modules\Translation\Repositories;

interface FileTranslationRepository
{
    /**
     * Get all the translations for all modules on disk.
     * @param string $request_type
     *
     * @return array
     */
    public function all(string $request_type);
}
