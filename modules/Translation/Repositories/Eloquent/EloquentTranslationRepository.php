<?php

namespace Modules\Translation\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Modules\Translation\Entities\TranslationTranslation;
use Modules\Translation\Repositories\TranslationRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentTranslationRepository extends EloquentCoreRepository implements TranslationRepository
{
    /**
     * @param string $key
     * @param string $locale
     *
     * @return string
     */
    public function findByKeyAndLocale($key, $locale = null)
    {
        $locale = $locale ?: app()->getLocale();
        $translation = $this->model->where('key', $key)->with('translations')->first();

        if ($translation && $translation->hasTranslation($locale)) {
            return $translation->translate($locale)->value;
        }

        return '';
    }

    /**
     * @param type $locale
     * @param type $group
     * @param type $namespace
     * @return type
     */
    public function getTranslationsForGroupAndNamespace($locale, $group, $namespace)
    {
        $start = $namespace . '::' . $group;

        $test = $this->model->where('key', 'LIKE', "{$start}%")->whereHas('translations', function (Builder $query) use ($locale) {
            $query->where('locale', $locale);
        })->get();

        $translations = [];

        foreach ($test as $item) {
            $key = str_replace($start . '.', '', $item->key);
            $translations[$key] = $item->translate($locale)->value;
        }

        return $translations;
    }

    /**
     * @param string $request_type
     * @return type
     */
    public function allFormatted(string $request_type)
    {
        $allRows = $this->all();
        $allDatabaseTranslations = [];

        if ('frontend' == $request_type) {
            $locales = get_application_frontend_locales();
        } else {
            $locales = config('application.core.available-locales-backend');
        }

        foreach ($allRows as $translation) {
            foreach ($locales as $locale => $language) {
                if ($translation->hasTranslation($locale)) {
                    $allDatabaseTranslations[$locale][$translation->key] = $translation->translate($locale)->value;
                }
            }
        }

        return $allDatabaseTranslations;
    }

    /**
     * @param type $locale
     * @param type $key
     * @param type $value
     * @return type
     */
    public function saveTranslationForLocaleAndKey($locale, $key, $value)
    {
        $translation = $this->findTranslationByKey($key);
        $translation->translateOrNew($locale)->value = $value;
        $translation->save();
    }

    /**
     * @param type $key
     * @return type
     */
    public function findTranslationByKey($key)
    {
        return $this->model->firstOrCreate(['key' => $key]);
    }

    /**
     * Update the given translation key with the given data.
     *
     * @param string $key
     * @param array  $data
     *
     * @return mixed
     */
    public function updateFromImport($key, array $data)
    {
        $translation = $this->findTranslationByKey($key);
        $translation->update($data);
    }

    /**
     * Set the given value on the given TranslationTranslation.
     *
     * @param TranslationTranslation $translationTranslation
     * @param string                 $value
     */
    public function updateTranslationToValue(TranslationTranslation $translationTranslation, $value)
    {
        $translationTranslation->value = $value;
        $translationTranslation->save();
    }
}
