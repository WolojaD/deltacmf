<?php

namespace Modules\Emailtemplates\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullEmailTemplateTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'status' => $this->status,
            'editor' => $this->editor,
            'comment' => $this->comment,
            'module_title' => [$this->module_title, $this->module_method],
            'variables' => $this->variables,
            'emails' => $this->emails,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
