<?php

namespace Modules\Emailtemplates\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class EmailTemplateTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'email_topic' => $this->defaultTranslate()->email_topic,
            'comment' => $this->comment,
            'module_title' => ucfirst($this->module_title),
            'module_method' => ucfirst($this->module_method),
            'emails' => $this->emails,
            'updated_at' => $this->updated_at->toRfc2822String(),
        ];
    }
}
