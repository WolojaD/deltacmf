<?php

namespace Modules\Emailtemplates\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Emailtemplates\Repositories\EmailTemplateInterface;
use Modules\Emailtemplates\Transformers\EmailTemplateTransformer;
use Modules\Emailtemplates\Http\Requests\CreateEmailTemplateRequest;
use Modules\Emailtemplates\Http\Requests\UpdateEmailTemplateRequest;
use Modules\Emailtemplates\Transformers\FullEmailTemplateTransformer;

class EmailTemplateController extends CoreApiController
{
    /**
     * @var EmailTemplateInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(EmailTemplateInterface $emailtemplate)
    {
        $this->repository = $emailtemplate;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|string $id
     *
     * @return EmailTemplateTransformer
     */
    public function index(Request $request, $ident = false)
    {
        if ($ident && in_array($ident, $this->repository->modulesWithEmailTemplates())) {
            $request->merge(['where' => ['module_title' => $ident]]);
        }

        return EmailTemplateTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function findEmailtemplates()
    {
        return $this->repository->modulesWithEmailTemplates();
    }

    /**
     * @param int $id
     *
     * @return FullEmailTemplateTransformer
     */
    public function find($id)
    {
        $emailtemplate = $this->repository->find($id);

        return new FullEmailTemplateTransformer($emailtemplate);
    }

    /**
     * @param int $id
     * @param UpdateEmailTemplateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($ident, UpdateEmailTemplateRequest $request)
    {
        if ($request->exists('module_title') && is_array($request->module_title)) {
            $emailtemplate = $this->repository->find($ident);

            $request->merge(['module_method' => $request->module_title[1]]);
            $request->merge(['module_title' => $request->module_title[0]]);

            $request->validate(['module_title' => 'unique:emailtemplates,module_title,' . $request->id . ',id,module_method,' . $request->module_method]);

            $this->repository->update($emailtemplate, $request->except('ident'));

            return response()->json([
                'errors' => false,
                'message' => trans('core::messages.api.fields.updated', ['field' => trans('emailtemplates::models.of.emailtemplate')]),
                'id' => $emailtemplate->id
            ]);
        }
    }

    /**
     * @param CreateEmailTemplateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmailTemplateRequest $request)
    {
        if ($request->exists('module_title') && is_array($request->module_title)) {
            $request->merge(['module_method' => $request->module_title[1]]);
            $request->merge(['module_title' => $request->module_title[0]]);

            $request->validate(['module_title' => 'unique:emailtemplates,module_title,NULL,id,module_method,' . $request->module_method]);

            $emailtemplate = $this->repository->create($request->except('ident'));

            return response()->json([
                'errors' => false,
                'message' => trans('core::messages.api.fields.created', ['field' => trans('emailtemplates::models.of.emailtemplate')]),
                'id' => $emailtemplate->id
            ]);
        }
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emailtemplate = $this->repository->find($id);

        $this->repository->destroy($emailtemplate);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('emailtemplates::models.of.emailtemplate')]),
        ]);
    }
}
