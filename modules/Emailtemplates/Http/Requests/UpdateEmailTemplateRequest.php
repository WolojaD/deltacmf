<?php

namespace Modules\Emailtemplates\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateEmailTemplateRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Emailtemplates/Config/models/email_template.json');
    }

    public function authorize()
    {
        return true;
    }
}
