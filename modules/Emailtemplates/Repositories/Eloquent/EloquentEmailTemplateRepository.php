<?php

namespace Modules\Emailtemplates\Repositories\Eloquent;

use Nwidart\Modules\Facades\Module;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\Emailtemplates\Repositories\EmailTemplateInterface;

class EloquentEmailTemplateRepository extends EloquentCoreRepository implements EmailTemplateInterface
{
    public $model_config = 'modules/Emailtemplates/Config/models/email_template.json';

    /**
     * {@inheritdoc}
     */
    public function create($data)
    {
        if (!method_exists($this->model, 'getJsonList') || !request()->get('ident', false)) {
            return $this->model->create($data);
        }

        $config = $this->model->getJsonList();

        foreach ($config->templates->default ?? [] as $field_name => $field) {
            if (!request()->filled($field_name) && ($field->from_ident ?? false)) {
                $data[$field_name] = (int) request()->ident;
            }
        }

        foreach (get_application_frontend_locales() as $locale => $translation) {
            if (!file_exists(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($data['module_title']) . '/' . $locale)) {
                mkdir(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($data['module_title']) . '/' . $locale, 0777, true);
            }

            $file = fopen(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($data['module_title']) . '/' . $locale . '/' . strtolower($data['module_method']) . '.blade.php', 'w');

            fwrite($file, $this->emailBodyParser($data[$locale]['email_body']));

            fclose($file);
        }

        return $this->model->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update($model, $data)
    {
        $model->update($data);

        foreach (get_application_frontend_locales() as $locale => $translation) {
            if (!file_exists(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($data['module_title']) . '/' . $locale)) {
                mkdir(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($data['module_title']) . '/' . $locale, 0777, true);
            }

            $file = fopen(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($data['module_title']) . '/' . $locale . '/' . strtolower($data['module_method']) . '.blade.php', 'w');

            fwrite($file, $this->emailBodyParser($data[$locale]['email_body']));

            fclose($file);
        }

        return $model;
    }

    /**
     * Parse variables in email_body data for put them like php variables in blade file
     *
     * @param string $email_body
     *
     * @return string
     */
    private function emailBodyParser($email_body)
    {
        // $email_body = $this->if_parser($email_body);
        $email_body = $this->foreach_parser($email_body);
        $email_body = $this->double_curly_braces_parser($email_body);
        // $email_body = $this->curly_braces_parser($email_body);
        $email_body = $this->url_parser($email_body);

        return $email_body;
    }

    /**
     * If parser in email_body data
     *
     * @param string $email_body
     *
     * @return string
     */
    private function if_parser($email_body)
    {
        $email_body = preg_replace('/.*(@if\()([a-z._]+)(\)).*/', '@if($$2)', $email_body);

        return preg_replace('/.*(@endif).*/', '$1', $email_body);
    }

    /**
     * Foreach parser in email_body data
     *
     * @param string $email_body
     *
     * @return string
     */
    private function foreach_parser($email_body)
    {
        $email_body = preg_replace('/.*(@foreach\()([a-z._]+)(\)).*/', '@foreach($$2 as $value)', $email_body);

        return preg_replace('/.*(@endforeach).*/', '$1', $email_body);
    }

    /**
     * Curly braces ( { and } ) parser in email_body data
     *
     * @param string $email_body
     *
     * @return string
     */
    private function curly_braces_parser($email_body)
    {
        return preg_replace('/{([a-z\._]+)}/', '{{$$1 ?? \'\'}}', $email_body);
    }

    /**
     * Double curly braces ( {{ and }} ) parser in email_body data
     *
     * @param string $email_body
     *
     * @return string
     */
    private function double_curly_braces_parser($email_body)
    {
        // Old version
        // return preg_replace('/{{(value)(.*)}}/', '{{$$1$2 ?? \'\'}}', $email_body);

        // New version
        return preg_replace('/{{([a-z\._(->)?]+)}}/', '{{$$1 ?? \'\'}}', $email_body);
    }

    /**
     * @url parser in email_body data
     *
     * @param string $email_body
     *
     * @return string
     */
    private function url_parser($email_body)
    {
        return preg_replace('/@url/', '{{url(\'/\')}}', $email_body);
    }

    public function getModulesWithEmailTemplates()
    {
        $childrens = [];
        $modulesWithEmailTemplatesTrait = [];

        foreach (Module::allEnabled() as $moduleName => $moduleParams):
            if (file_exists($moduleParams->getPath() . DIRECTORY_SEPARATOR . 'Traits' . DIRECTORY_SEPARATOR . 'EmailTemplatesTrait.php')) {
                foreach (get_class_methods('Modules\\' . $moduleName . '\\Traits\\EmailTemplatesTrait') as $method):
                    $childrens[] = [
                        'label' => $method,
                        'value' => $method,
                    ];
                endforeach;

                $modulesWithEmailTemplatesTrait[] = [
                    'label' => $moduleName,
                    'value' => $moduleName,
                    'children' => $childrens,
                ];

                $childrens = [];
            }
        endforeach;

        if (count($modulesWithEmailTemplatesTrait) > 0):
            return $modulesWithEmailTemplatesTrait;
        endif;

        return false;
    }

    public function modulesWithEmailTemplates()
    {
        $modulesWithEmailTemplatesTrait = [];

        foreach (Module::allEnabled() as $moduleName => $moduleParams):
            if (file_exists($moduleParams->getPath() . DIRECTORY_SEPARATOR . 'Traits' . DIRECTORY_SEPARATOR . 'EmailTemplatesTrait.php')) {
                $modulesWithEmailTemplatesTrait[] = $moduleName;
            }
        endforeach;

        if (count($modulesWithEmailTemplatesTrait) > 0):
            return $modulesWithEmailTemplatesTrait;
        endif;

        return false;
    }

    /**
     * @param string $module_title
     * @param string $module_method
     *
     * @return [type]
     */
    public function getTemplate($module_title, $module_method)
    {
        return $this->model->where('module_title', $module_title)->where('module_method', $module_method)->with('translations')->first();
    }
}
