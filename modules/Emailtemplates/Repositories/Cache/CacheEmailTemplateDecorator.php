<?php

namespace Modules\Emailtemplates\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Emailtemplates\Repositories\EmailTemplateInterface;

class CacheEmailTemplateDecorator extends CoreCacheDecorator implements EmailTemplateInterface
{
    /**
     * @var EmailTemplateInterface
     */
    protected $repository;

    public function __construct(EmailTemplateInterface $emailtemplates)
    {
        parent::__construct();

        $this->entityName = 'emailtemplates';
        $this->repository = $emailtemplates;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($module_title, $module_method)
    {
        return $this->repository->getTemplate($module_title, $module_method);
    }
}
