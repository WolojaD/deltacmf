<?php

namespace Modules\Emailtemplates\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface EmailTemplateInterface extends CoreInterface
{
    /**
     * @param string $module_title
     * @param string $module_method
     *
     * @return [type]
     */
    public function getTemplate($module_title, $module_method);
}
