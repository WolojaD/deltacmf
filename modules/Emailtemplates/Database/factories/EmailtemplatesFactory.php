<?php

use Faker\Generator as Faker;
use Modules\Emailtemplates\Entities\EmailTemplate;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(EmailTemplate::class, function (Faker $faker) {
    $data = [
        'comment' => $faker->sentence,
        'module_title' => $faker->name,
    ];

    return $data;
});

/*
|--------------------------------------------------------------------------
| Users -> sendResetCodeEmail
|--------------------------------------------------------------------------
*/

$factory->state(EmailTemplate::class, 'sendResetCodeEmail', function (Faker $faker) {
    $data = [
        'editor' => 1,
        'comment' => 'Отправить email восстановления пароля',
        'variables' => '@url - текущая ссылка сайта в формате - http//example.com
{{user_id}} - ID пользователя
{{code}} - уникальный код для доступа к востановлению пароля',
        'module_title' => 'Users',
        'module_method' => 'sendResetCodeEmail',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'email_topic' => 'Восстановления пароля',
            'email_body' => '<p>Reset password</p>

<p>To reset password complete this form.</p>

<p><a href="@url/auth/reset/{{user_id}}/{{code}}">Reset password</a></p>',
        ];
    }

    return $data;
});

/*
|--------------------------------------------------------------------------
| Ecommerce -> sendOrderDataForUser
|--------------------------------------------------------------------------
*/

$factory->state(EmailTemplate::class, 'sendOrderDataForUser', function (Faker $faker) {
    $data = [
        'editor' => 1,
        'comment' => 'Отправить email c данными о заказе',
        'variables' => '{{user_fullname}} - полное имя пользователя
{{user_phone}} - телефон пользователя
{{user_address}} - адресс пользователя
{{user_delivery}} - доставка
{{user_delivery_price}} - стоимость доставки
{{user_comment}} - комментарий пользователя
{{order_product_versions}} - спецыальная переменная где хранятся все товары данного заказа',
        'module_title' => 'Ecommerce',
        'module_method' => 'sendOrderDataForUser',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'email_topic' => 'Данные о заказе',
            'email_body' => '<p>Привет {{user_fullname}}(fullname)</p>

<p>Мы  {{user_delivery}}(delivery)  на  {{user_address}}(address)  и позвоним перед этим на  {{user_phone}}(phone).<br />
Так что приготовь за доставку  {{user_delivery_price}}(delivery_price)</p>

<p>Ты чето хотел этим сказать -  {{user_comment}}(comment)</p>

<p>@if($user_comment != 1)</p>

<p>Comment not 1</p>

<p>@endif</p>

<p>Доставим тебе:</p>

<p>@foreach(order_product_versions)</p>

<p><strong>Название товара - {{value->product_title}}</strong></p>

<p>Цена - {{value->price}}</p>

<p>Количество - {{value->quantity}}</p>

<p>@endforeach</p>',
        ];
    }

    return $data;
});

/*
|--------------------------------------------------------------------------
| Ecommerce -> sendOrderDataForAdmin
|--------------------------------------------------------------------------
*/

$factory->state(EmailTemplate::class, 'sendOrderDataForAdmin', function (Faker $faker) {
    $data = [
        'editor' => 1,
        'comment' => 'Отправить email c данными о заказе',
        'variables' => '{{order_product_versions}} - спецыальная переменная где хранятся все товары данного заказа',
        'module_title' => 'Ecommerce',
        'module_method' => 'sendOrderDataForAdmin',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'email_topic' => 'Данные о заказе',
            'email_body' => '<p>@foreach(order_product_versions)</p>

<p><strong>Название товара - {{value->product_title}}</strong></p>

<p>Цена - {{value->price}}</p>

<p>Количество - {{value->quantity}}</p>

<p>@endforeach</p>',
        ];
    }

    return $data;
});

/*
|--------------------------------------------------------------------------
| Newsletters -> SendSubscribeDataForAdmin
|--------------------------------------------------------------------------
*/

$factory->state(EmailTemplate::class, 'sendSubscribeDataForAdmin', function (Faker $faker) {
    $data = [
        'editor' => 1,
        'comment' => 'Отправить email о новом подписчике',
        'variables' => '{{email}} - email пользователя',
        'module_title' => 'Newsletters',
        'module_method' => 'sendSubscribeDataForAdmin',
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'email_topic' => 'Новый подписавшийся на сайте',
            'email_body' => '<p>Новый пользователь, с ящиком {{email}} подписался на рассылку на сайте</p>',
        ];
    }

    return $data;
});
