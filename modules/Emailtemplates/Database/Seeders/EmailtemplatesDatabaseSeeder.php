<?php

namespace Modules\Emailtemplates\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Emailtemplates\Entities\EmailTemplate;

class EmailtemplatesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EmailTemplate::class)->states('sendResetCodeEmail')->create();
        factory(EmailTemplate::class)->states('sendOrderDataForUser')->create();
        factory(EmailTemplate::class)->states('sendOrderDataForAdmin')->create();
        factory(EmailTemplate::class)->states('sendSubscribeDataForAdmin')->create();
    }
}
