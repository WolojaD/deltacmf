<?php

namespace Modules\Emailtemplates\Providers;

use App;
use Illuminate\Support\Facades\Mail;
use Modules\Emailtemplates\Mail\EmailUserAutoMailable;
use Modules\Emailtemplates\Mail\EmailAdminAutoMailable;
use Modules\Emailtemplates\Repositories\EmailTemplateInterface;

class EmailtemplatesTraitsProvider
{
    /**
     * @var EmailTemplateInterface
     */
    protected $repository;

    public function __construct()
    {
        $this->repository = App::make(EmailTemplateInterface::class);
    }

    /**
     * Handle email sending
     *
     * @param array $data_array
     *
     * @return [type]
     */
    public function handle($data_array = [])
    {
        if (array_key_exists('user_email', $data_array)) {
            $this->handleWithUserEmail($data_array);
        } else {
            $this->handleWithAdminEmail($data_array);
        }
    }

    /**
     * Handle email sending for users emails
     *
     * @param array $data_array
     *
     * @return mixes
     */
    private function handleWithUserEmail($data_array)
    {
        $email_template = $this->getRepository($data_array);

        Mail::to(clear_email($data_array['user_email']))->send(new EmailUserAutoMailable($data_array, $email_template));

        if (null !== $email_template->emails) {
            foreach ($this->multiexplode([', ', ','], $email_template->emails) as $email) {
                Mail::to(clear_email($email))->send(new EmailAdminAutoMailable($data_array, $email_template));
            }
        }
    }

    /**
     * Handle email sending for admins emails
     *
     * @param array $data_array
     *
     * @return mixes
     */
    private function handleWithAdminEmail($data_array)
    {
        $email_template = $this->getRepository($data_array);

        if (null !== $email_template->emails) {
            foreach ($this->multiexplode([', ', ','], $email_template->emails) as $email) {
                Mail::to(clear_email($email))->send(new EmailAdminAutoMailable($data_array, $email_template));
            }
        } else {
            $administrator_emails = cache()->tags(['settings', 'global'])->get('application.administrator_emails')->first();

            if (null !== $administrator_emails->plainValue) {
                foreach ($this->multiexplode([', ', ','], $administrator_emails->plainValue) as $email) {
                    Mail::to(clear_email($email))->send(new EmailAdminAutoMailable($data_array, $email_template));
                }
            }
        }
    }

    /**
     * Get template data by module and method name
     *
     * @param array $data_array
     *
     * @return mixed
     */
    private function getRepository($data_array)
    {
        return $this->repository->getTemplate($data_array['module'], $data_array['method']);
    }

    /**
     * @param array $delimiters
     * @param string $string
     *
     * @return array
     */
    private function multiexplode($delimiters, $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);

        return $launch;
    }
}
