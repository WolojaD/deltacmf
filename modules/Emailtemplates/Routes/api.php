<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Emailtemplates');

Route::group(['prefix' => '/emailtemplates'], function () {
    Route::get('find-emailtemplates', ['as' => 'api.backend.emailtemplates_email_template.findEmailtemplates', 'uses' => 'EmailTemplateController@findEmailtemplates', 'middleware' => 'token-can:emailtemplates.email.template.index']);
});
