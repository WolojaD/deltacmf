import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/emailtemplates',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.emailtemplates_email_template',
                component: require('@/views/emailtemplates/index').default,
                meta: {
                    pageTitle: 'Email templates',
                    breadcrumb: [
                        { name: 'Email templates' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.emailtemplates_email_template.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Email template Create',
                    breadcrumb: [
                        { name: 'Email templates', link: 'api.backend.emailtemplates_email_template' },
                        { name: 'Email template Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.emailtemplates_email_template.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Email template Edit',
                    breadcrumb: [
                        { name: 'Email templates', link: 'api.backend.emailtemplates_email_template' },
                        { name: 'Email template Edit' }
                    ]
                }
            },
            {
                path: ':ident',
                name: 'api.backend.emailtemplates_email_template.show',
                component: require('@/views/emailtemplates/index').default,
                meta: {
                    pageTitle: 'Email templates',
                    breadcrumb: [
                        { name: 'Email templates' }
                    ]
                }
            }
        ]
    }
]
