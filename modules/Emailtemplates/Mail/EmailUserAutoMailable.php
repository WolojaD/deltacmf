<?php

namespace Modules\Emailtemplates\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUserAutoMailable extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var type
     */
    private $data_array;
    private $email_template;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data_array, $email_template)
    {
        $this->data_array = $data_array;
        $this->email_template = $email_template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_body_data = $this->emailBodyParser();

        if (count(cache()->tags(['settings', 'global'])->get('application.mail')) > 0) {
            foreach (cache()->tags(['settings', 'global'])->get('application.mail') as $key => $value) {
                if ($value->name == 'emailtemplates::mail_from_address') {
                    $mail_from_address = $value->plainValue;
                }

                if ($value->name == 'emailtemplates::mail_from_name') {
                    $mail_from_name = $value->plainValue;
                }
            }
        }

        return $this->subject($this->email_template->givenTranslate()->email_topic)
            ->from($mail_from_address ?? strtolower(config('app.name') . '@mail.com'), $mail_from_name ?? config('app.name'))
            ->view('emailtemplates::' . strtolower($this->email_template->module_title) . '/' . $this->getTemplateLocale() . '/' . strtolower($this->email_template->module_method), $email_body_data);
    }

    /**
     * Get values for all variables in email body
     *
     * @return array
     */
    private function emailBodyParser()
    {
        $result = [];

        foreach ($this->data_array['variables'] as $variable => $value) {
            $result[$variable] = $value;
        }

        if (array_key_exists('variables_for_control_structures', $this->data_array)) {
            foreach ($this->data_array['variables_for_control_structures'] as $variable => $value) {
                $result[$variable] = $value;
            }
        }

        return $result;
    }

    /**
     * Get current locale by some mini logic
     * If exist template by current locale return it or return template in default main frontend language
     *
     * @return string
     */
    private function getTemplateLocale()
    {
        if (file_exists(get_application_frontend_theme_path() . '/views/emailtemplates/' . strtolower($this->email_template->module_title) . '/' . locale() . '/' . strtolower($this->email_template->module_method) . '.blade.php')) {
            return locale();
        }

        return json_decode(cache()->tags(['settings', 'global'])->get('application.locales')->plainValue)[0];
    }
}
