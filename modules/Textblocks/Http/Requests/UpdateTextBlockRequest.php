<?php

namespace Modules\Textblocks\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateTextBlockRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Textblocks/Config/models/text_block.json');
    }

    public function authorize()
    {
        return true;
    }
}
