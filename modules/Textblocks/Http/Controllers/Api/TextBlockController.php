<?php

namespace Modules\Textblocks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Textblocks\Repositories\TextBlockInterface;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Textblocks\Transformers\TextBlockTransformer;
use Modules\Textblocks\Http\Requests\CreateTextBlockRequest;
use Modules\Textblocks\Http\Requests\UpdateTextBlockRequest;
use Modules\Textblocks\Transformers\FullTextBlockTransformer;

class TextBlockController extends CoreApiController
{
    /**
     * @var TextBlockInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(TextBlockInterface $textblock)
    {
        $this->repository = $textblock;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return TextBlockTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return TextBlockTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return TextBlockTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return TextBlockTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullTextBlockTransformer
     */
    public function find($id)
    {
        $textblock = $this->repository->find($id);

        return new FullTextBlockTransformer($textblock);
    }

    /**
     * @param int $id
     * @param UpdateTextBlockRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateTextBlockRequest $request)
    {
        $textblock = $this->repository->find($id);

        $this->repository->update($textblock, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('textblocks::models.of.textblock')]),
            'id' => $textblock->id
        ]);
    }

    /**
     * @param CreateTextBlockRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTextBlockRequest $request)
    {
        $textblock = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('textblocks::models.of.textblock')]),
            'id' => $textblock->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $textblock = $this->repository->find($id);

        $this->repository->destroy($textblock);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('textblocks::models.of.textblock')]),
        ]);
    }
}
