import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/textblocks',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.textblocks_text_block',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Textblocks',
                    breadcrumb: [
                        { name: 'Textblocks' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.textblocks_text_block.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Textblock Create',
                    breadcrumb: [
                        { name: 'Textblocks', link: 'api.backend.textblocks_text_block' },
                        { name: 'Textblock Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.textblocks_text_block.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Textblock Edit',
                    breadcrumb: [
                        { name: 'Textblocks', link: 'api.backend.textblocks_text_block' },
                        { name: 'Textblock Edit' }
                    ]
                }
            }
        ]
    }
]
