<?php

namespace Modules\Textblocks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullTextBlockTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'comment' => $this->comment,
            'short_code' => $this->short_code,
            'editor' => $this->editor,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $data[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
