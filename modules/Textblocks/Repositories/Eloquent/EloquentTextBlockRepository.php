<?php

namespace Modules\Textblocks\Repositories\Eloquent;

use Modules\Textblocks\Repositories\TextBlockInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentTextBlockRepository extends EloquentCoreRepository implements TextBlockInterface
{
    public $model_config = 'modules/Textblocks/Config/models/text_block.json';
}
