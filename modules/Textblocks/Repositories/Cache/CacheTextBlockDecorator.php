<?php

namespace Modules\Textblocks\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Textblocks\Repositories\TextBlockInterface;

class CacheTextBlockDecorator extends CoreCacheDecorator implements TextBlockInterface
{
    /**
     * @var TextBlockInterface
     */
    protected $repository;

    public function __construct(TextBlockInterface $textblocks)
    {
        parent::__construct();

        $this->entityName = 'textblocks';
        $this->repository = $textblocks;
    }
}
