<?php

use Faker\Generator as Faker;
use Modules\Textblocks\Entities\Textblock;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Textblock::class, function (Faker $faker) {
    $data = [
        'editor' => $faker->numberBetween(0, 1),
        'short_code' => $faker->lexify('?????????'),
        'comment' => $faker->name,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'body' => $faker->sentence,
        ];
    }

    return $data;
});
