<?php

namespace Modules\Textblocks\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Textblocks\Entities\Textblock;

class TextblocksDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Textblock::class, 5)->create();
        }
    }
}
