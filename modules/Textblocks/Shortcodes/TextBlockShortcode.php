<?php

namespace Modules\Textblocks\Shortcodes;

use Modules\Textblocks\Entities\TextBlock;

class TextBlockShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        $textblock = TextBlock::when(!is_numeric($shortcode->code), function ($q) use ($shortcode) {
            $q->where('short_code', $shortcode->code);
        }, function ($q) use ($shortcode) {
            $q->where('id', $shortcode->code);
        })->first();

        if (null === $textblock) {
            return '';
        }

        return isset($textblock->body) ? view('textblocks::index', compact('textblock')) : '';
    }
}
