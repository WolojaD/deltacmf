import helpers from '@services/helpers'

export default [{
    path: '/' + helpers.makeBaseUrl() + '/socialnetworks',
    component: require('@components/layouts/default-page').default,
    meta: {
        requiresAuth: true
    },
    children: [
        {
            path: '/',
            name: 'api.backend.socialnetworks_social_network',
            component: require('@/views/crud/index').default,
            meta: {
                pageTitle: 'Social Networks',
                breadcrumb: [{
                    name: 'Social Networks'
                }]
            }
        },
        {
            path: 'create',
            name: 'api.backend.socialnetworks_social_network.create',
            component: require('@/views/crud/create').default,
            meta: {
                pageTitle: 'Social Network Create',
                breadcrumb: [{
                    name: 'Social Networks',
                    link: 'api.backend.socialnetworks_social_network'
                },
                {
                    name: 'Social Network Create'
                }
                ]
            }
        },
        {
            path: ':ident(\\d+)/edit',
            name: 'api.backend.socialnetworks_social_network.edit',
            component: require('@/views/crud/edit').default,
            meta: {
                pageTitle: 'Social Network Edit',
                breadcrumb: [{
                    name: 'Social Networks',
                    link: 'api.backend.socialnetworks_social_network'
                },
                {
                    name: 'Social Network Edit'
                }
                ]
            }
        }
    ]
}]
