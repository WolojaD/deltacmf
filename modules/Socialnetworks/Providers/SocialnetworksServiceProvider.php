<?php

namespace Modules\Socialnetworks\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Modules\Core\Providers\ServiceProvider;
use Modules\Core\Events\LoadingBackendTranslations;

class SocialnetworksServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('socialnetworks_sidebar', array_dot(trans('socialnetworks::sidebar')));
            $event->load('socialnetworks_table-fields', array_dot(trans('socialnetworks::table-fields')));
            $event->load('socialnetworks_permissions', array_dot(trans('socialnetworks::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('socialnetworks', 'config');
        $this->publishConfig('socialnetworks', 'permissions');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $folder = base_path('modules/Socialnetworks/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }

        foreach ($models as $model_name => $model) {
            $model_name = studly_case($model_name);

            $this->app->bind("Modules\Socialnetworks\Repositories\\{$model_name}Interface", function () use ($model_name, $model) {
                $eloquent_repository = "\Modules\Socialnetworks\Repositories\Eloquent\Eloquent{$model_name}Repository";
                $entity = "\Modules\Socialnetworks\Entities\\{$model_name}";
                $cache_decorator = "\Modules\Socialnetworks\Repositories\Cache\Cache{$model_name}Decorator";

                $repository = new $eloquent_repository(new $entity());

                if (!Config::get('app.cache') && !($model->no_cache ?? false)) {
                    return $repository;
                }

                return new $cache_decorator($repository);
            });
        }
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
