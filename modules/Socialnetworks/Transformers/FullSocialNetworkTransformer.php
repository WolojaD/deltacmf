<?php

namespace Modules\Socialnetworks\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullSocialNetworkTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'image' => $this->image,
            'link' => $this->link,
            'class' => $this->class,
            'status' => $this->status,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
