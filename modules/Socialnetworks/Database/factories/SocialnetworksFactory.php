<?php

use Faker\Generator as Faker;
use Modules\Socialnetworks\Entities\Socialnetwork;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Socialnetwork::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'image' => $faker->name,
        'link' => $faker->url,
        'class' => $faker->text(20),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->word,
        ];
    }

    return $data;
});
