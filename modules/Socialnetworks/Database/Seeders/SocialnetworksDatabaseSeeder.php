<?php

namespace Modules\Socialnetworks\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Socialnetworks\Entities\Socialnetwork;

class SocialnetworksDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Socialnetwork::class, 55)->create();
        }
    }
}
