<?php

namespace Modules\Socialnetworks\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Cache;
use Modules\Socialnetworks\Repositories\SocialNetworkInterface;

class SocialnetworkComposer
{
    /**
     * @var SocialNetworkInterface
     */
    protected $socialnetwork;

    /**
     * SocialnetworkComposer constructor.
     *
     * @param SocialNetworkInterface $socialnetwork
     */
    public function __construct(SocialNetworkInterface $socialnetwork)
    {
        $this->socialnetwork = $socialnetwork;
    }

    public function compose(View $view)
    {
        $view->with('socialnetworks', $this->getSocialnetworks());
    }

    /**
     * @return array
     */
    private function getSocialnetworks()
    {
        if (app()->isLocal()) {
            return $this->socialnetwork->getViewData();
        } else {
            if (!Cache::has('socialnetworks')) {
                return $socialnetworks = Cache::rememberForever('socialnetworks', function () {
                    return $this->socialnetwork->getViewData();
                });
            } else {
                return Cache::get('socialnetworks');
            }
        }
    }
}
