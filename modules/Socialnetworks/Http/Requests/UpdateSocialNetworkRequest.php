<?php

namespace Modules\Socialnetworks\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateSocialNetworkRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Socialnetworks/Config/models/social_network.json');
    }

    public function authorize()
    {
        return true;
    }
}
