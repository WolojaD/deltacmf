<?php

namespace Modules\Socialnetworks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Socialnetworks\Repositories\SocialNetworkInterface;
use Modules\Socialnetworks\Transformers\SocialNetworkTransformer;
use Modules\Socialnetworks\Http\Requests\CreateSocialNetworkRequest;
use Modules\Socialnetworks\Http\Requests\UpdateSocialNetworkRequest;
use Modules\Socialnetworks\Transformers\FullSocialNetworkTransformer;

class SocialNetworkController extends CoreApiController
{
    /**
     * @var SocialNetworkInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(SocialNetworkInterface $socialnetwork)
    {
        $this->repository = $socialnetwork;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return SocialNetworkTransformer
     */
    public function index(Request $request, $id = false)
    {
        return SocialNetworkTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullSocialNetworkTransformer
     */
    public function find($id)
    {
        $socialnetwork = $this->repository->find($id);

        return new FullSocialNetworkTransformer($socialnetwork);
    }

    /**
     * @param int $id
     * @param UpdateSocialNetworkRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateSocialNetworkRequest $request)
    {
        $socialnetwork = $this->repository->find($id);

        $this->repository->update($socialnetwork, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('socialnetworks::models.of.socialnetwork')]),
            'id' => $socialnetwork->id
        ]);
    }

    /**
     * @param CreateSocialNetworkRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSocialNetworkRequest $request)
    {
        $socialnetwork = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('socialnetworks::models.of.socialnetwork')]),
            'id' => $socialnetwork->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $socialnetwork = $this->repository->find($id);

        $this->repository->destroy($socialnetwork);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('socialnetworks::models.of.socialnetwork')]),
        ]);
    }
}
