<?php

namespace Modules\Socialnetworks\Repositories\Eloquent;

use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\Socialnetworks\Repositories\SocialNetworkInterface;

class EloquentSocialNetworkRepository extends EloquentCoreRepository implements SocialNetworkInterface
{
    public $model_config = 'modules/Socialnetworks/Config/models/social_network.json';

    /**
     * @return mixed
     */
    public function getViewData()
    {
        return $this->model
            ->where('link', '!=', '')
            ->with('translations')
            ->listsAllTranslations()
            ->orderBy('order')
            ->get();
    }
}
