<?php

namespace Modules\Socialnetworks\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\Socialnetworks\Repositories\SocialNetworkInterface;

class CacheSocialNetworkDecorator extends CoreCacheDecorator implements SocialNetworkInterface
{
    /**
     * @var SocialNetworkInterface
     */
    protected $repository;

    public function __construct(SocialNetworkInterface $socialnetworks)
    {
        parent::__construct();

        $this->entityName = 'socialnetworks';
        $this->repository = $socialnetworks;
    }

    /**
     * {@inheritdoc}
     */
    public function getViewData()
    {
        return $this->remember(function () {
            return $this->repository->getViewData();
        });
    }
}
