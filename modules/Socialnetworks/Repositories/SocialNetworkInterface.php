<?php

namespace Modules\Socialnetworks\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface SocialNetworkInterface extends CoreInterface
{
    /**
     * {@inheritdoc}
     */
    public function getViewData();
}
