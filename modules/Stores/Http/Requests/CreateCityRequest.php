<?php

namespace Modules\Stores\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateCityRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Stores/Config/models/city.json');
    }

    public function authorize()
    {
        return true;
    }
}
