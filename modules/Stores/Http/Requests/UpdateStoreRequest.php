<?php

namespace Modules\Stores\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateStoreRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Stores/Config/models/store.json');
    }

    public function authorize()
    {
        return true;
    }
}
