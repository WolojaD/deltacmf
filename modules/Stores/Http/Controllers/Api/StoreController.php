<?php

namespace Modules\Stores\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Users\Repositories\UserInterface;
use Modules\Stores\Repositories\StoreInterface;
use Modules\Stores\Transformers\StoreTransformer;
use Modules\Stores\Http\Requests\CreateStoreRequest;
use Modules\Stores\Http\Requests\UpdateStoreRequest;
use Modules\Stores\Transformers\FullStoreTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class StoreController extends CoreApiController
{
    /**
     * @var StoreInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $city;
    protected $user;

    public function __construct(StoreInterface $store, UserInterface $user)
    {
        $this->repository = $store;
        $this->modelInfo = $this->repository->getJsonList();
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return StoreTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($id) {
            return StoreTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return StoreTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function users(Request $request)
    {
        return StoreTransformer::collection($this->repository->getUsers($request, 'attached'));
    }

    public function getUsersList(Request $request)
    {
        $data = json_decode($request->form);

        return $this->repository->sellerUsersListWithoutAttached($data->ident);
    }

    public function addUsers(Request $request, $id)
    {
        $translation = count($request->users) > 1 ? 'multi_attach' : 'attach';
        $this->repository->attachUsers($request->users, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.' . $translation, ['field' => trans('ecommerce::models.of.product')]),
        ]);
    }

    public function detachUser(Request $request, $id)
    {
        $this->repository->detachUsers($request->item_id, $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.detached', ['field' => trans('stores::models.of.user')]),
        ]);
    }

    public function detachUsers(Request $request, $id)
    {
        $this->repository->detachUsers(json_decode($request->checked), $id);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.multi_detached', ['field' => trans('stores::models.of.user')]),
        ]);
    }

    /**
     * @param int $id
     *
     * @return FullStoreTransformer
     */
    public function find($id)
    {
        $store = $this->repository->find($id);

        return new FullStoreTransformer($store);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function findEdit(Request $request)
    {
        $carry = [
            [
                'name' => trans('core::breadcrumbs.store edit')
            ]
        ];
        $store = $this->repository->find($request->ident);

        return [
            'fields' => $this->repository->getFieldsArray(),
            'buttons' => [
                'attach-users'
            ],
            'breadcrumbs' => $store ? $store->city->getBackendBreadcrumbs($carry) : []
        ];
    }

    /**
     * @param int $id
     * @param UpdateStoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateStoreRequest $request)
    {
        $store = $this->repository->find($id);

        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        if (isset($data['days'])) {
            $data['days'] = array_filter($data['days'], function ($item) {
                return count($item['days'] ?? []) > 0;
            });
        } else {
            $data['days'] = [];
        }

        $this->repository->update($store, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('stores::models.of.store')]),
            'id' => $store->id
        ]);
    }

    /**
     * @param CreateStoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStoreRequest $request)
    {
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        if (isset($data['days'])) {
            $data['days'] = array_filter($data['days'], function ($item) {
                return count($item['days'] ?? []) > 0;
            });
        } else {
            $data['days'] = [];
        }

        $store = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('stores::models.of.store')]),
            'id' => $store->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = $this->repository->find($id);

        $this->repository->destroy($store);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('stores::models.of.store')]),
        ]);
    }
}
