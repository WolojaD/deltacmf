<?php

namespace Modules\Stores\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Stores\Repositories\CityInterface;
use Modules\Stores\Transformers\CityTransformer;
use Modules\Stores\Http\Requests\CreateCityRequest;
use Modules\Stores\Http\Requests\UpdateCityRequest;
use Modules\Stores\Transformers\FullCityTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class CityController extends CoreApiController
{
    /**
     * @var CityInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(CityInterface $city)
    {
        $this->repository = $city;
        $this->modelInfo = $this->repository->getJsonList();
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return CityTransformer
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return CityTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return CityTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return CityTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullCityTransformer
     */
    public function find($id)
    {
        $city = $this->repository->find($id);

        return new FullCityTransformer($city);
    }

    /**
     * @param int $id
     * @param UpdateCityRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateCityRequest $request)
    {
        $city = $this->repository->find($id);

        $this->repository->update($city, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.updated', ['field' => trans('stores::models.of.city')]),
            'id' => $city->id
        ]);
    }

    /**
     * @param CreateCityRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCityRequest $request)
    {
        $city = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.created', ['field' => trans('stores::models.of.city')]),
            'id' => $city->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = $this->repository->find($id);

        $this->repository->destroy($city);

        return response()->json([
            'errors' => false,
            'message' => trans('core::messages.api.fields.deleted', ['field' => trans('stores::models.of.city')]),
        ]);
    }
}
