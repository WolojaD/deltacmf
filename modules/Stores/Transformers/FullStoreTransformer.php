<?php

namespace Modules\Stores\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullStoreTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->getAttributes();

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        $days = array_filter($this->days, function ($item) {
            return count($item['days']) > 0;
        });

        $data['days'] = $days;

        return $data;
    }
}
