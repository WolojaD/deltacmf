<?php

namespace Modules\Stores\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class StoreTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? ''];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['city_title'] = $this->city ? $this->city->defaultTranslate()->title : '';

        return $result;
    }
}
