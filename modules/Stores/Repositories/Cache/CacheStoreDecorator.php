<?php

namespace Modules\Stores\Repositories\Cache;

use Modules\Stores\Repositories\StoreInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheStoreDecorator extends CoreCacheDecorator implements StoreInterface
{
    /**
     * @var StoreInterface
     */
    protected $repository;

    public function __construct(StoreInterface $stores)
    {
        parent::__construct();

        $this->entityName = 'stores';
        $this->repository = $stores;
    }
}
