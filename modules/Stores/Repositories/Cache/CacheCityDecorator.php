<?php

namespace Modules\Stores\Repositories\Cache;

use Modules\Stores\Repositories\CityInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheCityDecorator extends CoreCacheDecorator implements CityInterface
{
    /**
     * @var CityInterface
     */
    protected $repository;

    public function __construct(CityInterface $stores)
    {
        parent::__construct();

        $this->entityName = 'stores_cities';
        $this->repository = $stores;
    }
}
