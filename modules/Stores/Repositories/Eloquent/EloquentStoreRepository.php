<?php

namespace Modules\Stores\Repositories\Eloquent;

use Modules\Stores\Entities\City;
use Modules\Users\Entities\Sentinel\User;
use Modules\Stores\Repositories\StoreInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentStoreRepository extends EloquentCoreRepository implements StoreInterface
{
    public $model_config = 'modules/Stores/Config/models/store.json';

    public function cities()
    {
        return City::listsDefaultTranslations('title')->orderBy('id', 'asc')->pluck('title', 'id')->toArray();
    }

    public function getDays()
    {
        $days = [];

        for ($d = 1; $d < 8; $d++) {
            $days[$d] = trans('stores::days.full.' . $d);
        }

        return $days;
    }

    public function getUsers($request, $template = 'attached')
    {
        $store = $this->model->where('id', $request->ident)->first();
        $users = $store->users;

        $carry = [
            [
                'name' => $store->defaultTranslate()->title,
                'link' => [
                    'name' => 'api.backend.stores_store.edit',
                    'params' => [
                        'ident' => $request->ident
                    ]
                ]
            ],
            [
                'name' => trans('core::breadcrumbs.store users'),
            ]
        ];
        $breadcrumbs = $store->getBackendBreadcrumbs();
        array_pop($breadcrumbs);
        $breadcrumbs = array_merge($breadcrumbs, $carry);

        $data = $this->paginator(
            $users,
            $request->get('per_page', 10),
            false,
            $template
        )
            ->changeDataByKey('breadcrumbs', $breadcrumbs);

        return $data;
    }

    public function attachUsers($users, $id)
    {
        return $this->model->find($id)->users()->attach(array_column($users, 'id'));
    }

    /**
     * Remove users from users
     *
     * @param array $detach_list
     * @param integer $product_id
     *
     * @return void
     */
    public function detachUsers($detach_list, $user_id)
    {
        if ($detach_list = $this->validForPivotMaking($detach_list)) {
            $this->model->find($user_id)->users()->detach($detach_list);
        }
    }

    public function sellerUsersListWithoutAttached($id = false)
    {
        $users = User::whereHas('roles', function ($query) {
            $query->where('slug', 'seller');
        })
        ->when($id, function ($query) use ($id) {
            $store = $this->model->where('id', $id)->first();
            $attached = $store->users->pluck('id');
            if (count($attached)) {
                return $query->whereNotIn('id', $attached);
            }
        })
        ->get()
        ->map(function ($user) {
            return [
                    'id' => $user->id,
                    'email' => $user->email,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                ];
        });

        return $this->paginator($users, 50);
    }
}
