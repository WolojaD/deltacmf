<?php

namespace Modules\Stores\Repositories\Eloquent;

use Modules\Stores\Repositories\CityInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentCityRepository extends EloquentCoreRepository implements CityInterface
{
    public $model_config = 'modules/Stores/Config/models/city.json';

    /**
     * @return mixed
     */
    public function getViewData()
    {
        $cities = $this->model
            ->whereHas('stores')
            ->with(['translations',
                'stores' => function ($query) {
                    $query->orderBy('order');
                },
                'stores.translations'])
            ->orderBy('order')
            ->get();

        return [
            'stores' => $cities
        ];
    }
}
