<?php

use Modules\Core\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    protected $folder;

    public function __construct()
    {
        $this->folder = realpath(__DIR__ . '/../../Config/models/');

        parent::__construct();
    }
}
