<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Stores');

Route::group(['prefix' => '/stores/store', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::get('{ident}/users', ['as' => 'api.backend.stores_store.users', 'uses' => 'StoreController@users', 'middleware' => 'token-can:stores.store.edit']);

    Route::delete('detach-user/{id}', ['as' => 'api.backend.stores_store.user.detach.stores_store', 'uses' => 'StoreController@detachUser', 'middleware' => 'token-can:stores.store.edit']);

    Route::delete('detach-users/{id}', ['as' => 'api.backend.stores_store.users.detach_multiple.stores_store', 'uses' => 'StoreController@detachUsers', 'middleware' => 'token-can:stores.store.edit']);

    Route::get('get-users-list', ['as' => 'api.backend.stores_store.users.getUsersList', 'uses' => 'StoreController@getUsersList', 'middleware' => 'token-can:stores.store.edit']);

    Route::post('add-users/{id}', ['as' => 'api.backend.stores_store.users.addUsers', 'uses' => 'StoreController@addUsers', 'middleware' => 'token-can:stores.store.edit']);
});
