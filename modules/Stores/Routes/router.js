import helpers from '@services/helpers'

const tabs = [
    {
        name: 'stores',
        label: 'stores_sidebar.submenu.stores',
        permission: 'stores.store.index',
        routeName: 'api.backend.stores_store',
    },
    {
        name: 'cities',
        label: 'stores_sidebar.submenu.cities',
        permission: 'stores.city.index',
        routeName: 'api.backend.stores_city'
    }
]

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/stores',
        component: require('@components/layouts/default-page').default,
        redirect: {name: 'api.backend.stores_store'},
        meta: { requiresAuth: true },
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/stores/store',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.stores_store',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'stores'
                },
                meta: {
                    pageTitle: 'Stores',
                    breadcrumb: [
                        { name: 'Stores' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.stores_store.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Stores Create',
                    breadcrumb: [
                        { name: 'Stores', link: 'api.backend.stores_store' },
                        { name: 'Store Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.stores_store.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Stores Edit',
                    breadcrumb: [
                        { name: 'Stores', link: 'api.backend.stores_store' },
                        { name: 'Store Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/users',
                name: 'api.backend.stores_store.users',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Store users',
                    breadcrumb: [
                        { name: 'Store', link: 'api.backend.stores_store' },
                        { name: 'Store users' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/stores/city',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.stores_city',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'cities'
                },
                meta: {
                    pageTitle: 'Cities show',
                    breadcrumb: [
                        { name: 'City show' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.stores_city.show',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'cities'
                },
                meta: {
                    pageTitle: 'City show',
                    breadcrumb: [
                        { name: 'City show' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.stores_city.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'City Create',
                    breadcrumb: [
                        { name: 'Cities', link: 'api.backend.stores_city' },
                        { name: 'City Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.stores_city.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'City Edit',
                    breadcrumb: [
                        { name: 'Cities', link: 'api.backend.stores_city' },
                        { name: 'City Edit' }
                    ]
                }
            }
        ]
    }
]
