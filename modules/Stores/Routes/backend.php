<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

Route::backendRouteList('Stores');

Route::group(['prefix' => '/stores/store'], function () {
    Route::get('{id}/users', ['as' => 'backend.stores_store.users', 'uses' => 'StoresController@index', 'middleware' => 'can:stores.store.edit']);
});
