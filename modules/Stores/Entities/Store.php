<?php

namespace Modules\Stores\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Ecommerce\Entities\Stock;
use Modules\Users\Entities\Sentinel\User;
use Modules\Core\Internationalisation\Translatable;

class Store extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'stores';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/stores';

    protected $casts = [
        'days' => 'array',
    ];

    protected $file_path = 'modules/Stores/Config/models/store.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'store_users', 'store_id', 'user_id');
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
