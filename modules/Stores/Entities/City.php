<?php

namespace Modules\Stores\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class City extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'stores_cities';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/stores';

    protected $file_path = 'modules/Stores/Config/models/city.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getAllStoresCountAttribute()
    {
        return $this->stores()->count();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
