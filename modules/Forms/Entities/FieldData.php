<?php

namespace Modules\Forms\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class FieldData extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'forms_field_datas';
    protected $fillable = [
        'field_id',
        'order',
        'status',
        'title',
    ];
    public $translatedAttributes = [
        'title',
    ];
    protected $searchable = [
        'title',
    ];
    protected $casts = [
        'status' => 'integer',
    ];
    public $groupOrder = 'field_id';

    protected $file_path = 'modules/Forms/Config/models/field_data.json';

    public $breadcrumbs = true;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function field()
    {
        return $this->belongsTo(Field::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
