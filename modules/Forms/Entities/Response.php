<?php

namespace Modules\Forms\Entities;

use Modules\Core\Eloquent\Model;

class Response extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'forms_responses';
    protected $fillable = [
        'form_id',
        'locale',
        'ip',
        'name',
        'email',
        'phone',
        'response',
    ];
    protected $searchable = [
        'form_id',
        'name',
        'email',
        'phone',
        'response',
    ];
    protected $casts = [
        'response' => 'array',
    ];
    protected static $entityNamespace = 'application/forms';

    protected $file_path = 'modules/Forms/Config/models/response.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getResponseDataByFormFields()
    {
        $response = [];

        foreach ($this->response as $title => $value) {
            $response[$value['title']] = $value['value'];
        }

        $response['Язык'] = $this->locale;
        $response['IP'] = $this->ip;

        return $response;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function form()
    {
        return $this->belongsTo(Form::class)->with('translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getHeadersOfCollectionFor($template = 'default')
    {
        if ($template == 'default') {
            return [
                'form_id' => 'Название формы',
                'name' => 'ФИО',
                'email' => 'E-mail',
                'phone' => 'Телефон',
                'response' => 'Данные',
                'created_at' => 'Создано',
            ];
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
