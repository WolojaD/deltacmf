<?php

namespace Modules\Forms\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Field extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'forms_fields';
    protected $fillable = [
        'order',
        'status',
        'form_id',
        'regex_id',
        'unique_code',
        'field_type',
        'required',
        'class',
        'accept',
        'title',
        'placeholder',
        'comment',
        'default',
    ];
    public $translatedAttributes = [
        'title',
        'placeholder',
        'comment',
        'default',
    ];
    protected $searchable = [
        'title',
    ];
    protected $casts = [
        'status' => 'integer',
    ];
    public $groupOrder = 'form_id';
    protected static $entityNamespace = 'application/forms';

    protected $file_path = 'modules/Forms/Config/models/field.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return type
     */
    public static function boot()
    {
        static::deleting(function ($obj) {
            $obj->field_data->each(function ($data) {
                $data->delete();
            });
        });

        parent::boot();
    }

    /**
     * @return array
     */
    public function toArrayForForm()
    {
        return [
            'title' => $this->defaultTranslate()->title,
            'field_type' => $this->field_type,
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function form()
    {
        return $this->belongsTo(Form::class)->with('translations');
    }

    public function regex()
    {
        return $this->belongsTo(Regex::class);
    }

    public function field_data()
    {
        return $this->hasMany(FieldData::class, 'field_id', 'id')->with('translations')->orderBy('order');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getRegexTitleAttribute()
    {
        if ($this->regex_id) {
            return $this->regex->defaultTranslate()->title;
        }

        return '';
    }

    public function getFormTitleAttribute()
    {
        if ($this->form_id) {
            return $this->form->defaultTranslate()->title;
        }

        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setAcceptAttribute($value)
    {
        if (null !== $value) {
            $this->attributes['accept'] = trim(strtolower(str_replace('.', '', $value)));
        } else {
            $this->attributes['accept'] = null;
        }
    }
}
