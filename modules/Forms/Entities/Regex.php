<?php

namespace Modules\Forms\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Regex extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'forms_regexes';
    protected $fillable = [
        'title',
        'regex_frontend',
        'regex_backend',
        'error_message',
    ];
    public $translatedAttributes = [
        'error_message',
    ];
    protected $searchable = [
    ];
    protected static $entityNamespace = 'application/forms';

    protected $file_path = 'modules/Forms/Config/models/regex.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function fields()
    {
        return $this->hasMany(Field::class, 'form_id', 'id')->with('translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
