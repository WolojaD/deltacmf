<?php

namespace Modules\Forms\Entities;

use Modules\Core\Eloquent\Model;
use Modules\Core\Internationalisation\Translatable;

class Form extends Model
{
    use Translatable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'forms';
    protected $fillable = [
        'status',
        'custom_module',
        'custom_action',
        'forms_template',
        'short_code',
        'comment',
        'emails',
        'show_personal_data_message',
        'show_title',
        'allow_files',
        'redirect',
        'title',
        'description',
        'message',
        'auto_response',
        'button_title',
    ];
    public $translatedAttributes = [
        'title',
        'description',
        'message',
        'auto_response',
        'button_title',
    ];
    protected $searchable = [
    ];
    protected $casts = [
        'status' => 'int',
        'show_personal_data_message' => 'int',
        'show_title' => 'int',
        'allow_files' => 'int',
    ];
    protected static $entityNamespace = 'application/forms';

    protected $file_path = 'modules/Forms/Config/models/form.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * @return type
     */
    public static function boot()
    {
        static::deleting(function ($obj) {
            $obj->fields->each(function ($field) {
                $field->delete();
            });
        });

        parent::boot();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function fields()
    {
        return $this->hasMany(Field::class, 'form_id', 'id')->with('translations', 'regex')->orderBy('order');
    }

    public function responses()
    {
        return $this->hasMany(Response::class, 'form_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
