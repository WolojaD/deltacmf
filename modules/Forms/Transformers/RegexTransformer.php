<?php

namespace Modules\Forms\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class RegexTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'regex_frontend' => 'Frontend: ' . $this->regex_frontend,
            'regex_backend' => 'Backend: ' . $this->regex_backend,
            'created_at' => $this->created_at->toRfc2822String(),
        ];
    }
}
