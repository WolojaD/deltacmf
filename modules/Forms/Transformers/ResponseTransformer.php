<?php

namespace Modules\Forms\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ResponseTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'form_title' => $this->form->defaultTranslate()->title,
            'name' => $this->name ?? '',
            'email' => str_replace(';', ' ', $this->email) ?? '',
            'phone' => str_replace(';', ' ', $this->phone) ?? '',
            'response' => $this->getResponseDataByFormFields(),
            'created_at' => $this->created_at->toRfc2822String(),
        ];
    }
}
