<?php

namespace Modules\Forms\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullFormTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'status' => $this->status,
            'comment' => $this->comment,
            'short_code' => $this->short_code,
            'emails' => $this->emails,
            'show_personal_data_message' => $this->show_personal_data_message,
            'show_title' => $this->show_title,
            'allow_files' => $this->allow_files,
            'redirect' => $this->redirect,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $data[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
