<?php

namespace Modules\Forms\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FormTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at->toRfc2822String(),
            'title' => $this->defaultTranslate()->title,
            'emails' => $this->emails,
            'short_code' => $this->short_code,
            'comment' => $this->comment,
            'children_count' => $this->fields->count() ?? null,
        ];
    }
}
