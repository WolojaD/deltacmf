<?php

namespace Modules\Forms\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullFieldTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'regex_id' => $this->regex->id ?? '',
            'unique_code' => $this->unique_code ?? '',
            'field_type' => $this->field_type ?? '',
            'required' => $this->required ?? '',
            'status' => $this->status ?? '',
            'class' => $this->class ?? '',
            'accept' => $this->accept ?? '',
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            $data[$locale] = [];

            foreach ($this->translatedAttributes as $translatedAttribute) {
                $data[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $data;
    }
}
