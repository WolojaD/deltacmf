<?php

namespace Modules\Forms\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FieldTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $title = $this->required ? '* ' . $this->defaultTranslate()->title : $this->defaultTranslate()->title;

        return [
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at->toRfc2822String(),
            'title' => $title,
            'comment' => $this->defaultTranslate()->comment,
            'order' => $this->order,
            'field_type' => ['table_title' => trans('forms::field-types.' . $this->field_type), 'original_title' => $this->field_type],
            'unique_code' => $this->unique_code,
            'required' => $this->required,
            'children_count' => $this->field_data()->count() ?? null,
        ];
    }
}
