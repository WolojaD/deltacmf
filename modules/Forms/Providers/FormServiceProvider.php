<?php

namespace Modules\Forms\Providers;

use Modules\Forms\Entities\Form;
use Modules\Forms\Entities\Field;
use Modules\Forms\Entities\Regex;
use Modules\Forms\Entities\Response;
use Modules\Forms\Entities\FieldData;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Webwizo\Shortcodes\Facades\Shortcode;
use Modules\Core\Providers\ServiceProvider;
use Modules\Forms\Shortcodes\FormsShortcode;
use Modules\Forms\Repositories\FormRepository;
use Modules\Forms\Repositories\FieldRepository;
use Modules\Forms\Repositories\RegexRepository;
use Modules\Forms\Repositories\ResponseRepository;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Forms\Repositories\FieldDataRepository;
use Modules\Forms\Repositories\Cache\CacheFormDecorator;
use Modules\Forms\Repositories\Cache\CacheFieldDecorator;
use Modules\Forms\Repositories\Cache\CacheRegexDecorator;
use Modules\Forms\Repositories\Eloquent\EloquentFormRepository;
use Modules\Forms\Repositories\Eloquent\EloquentFieldRepository;
use Modules\Forms\Repositories\Eloquent\EloquentRegexRepository;
use Modules\Forms\Repositories\Eloquent\EloquentResponseRepository;
use Modules\Forms\Repositories\Eloquent\EloquentFieldDataRepository;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        Shortcode::register('forms', FormsShortcode::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('forms_sidebar', array_dot(trans('forms::sidebar')));
            $event->load('forms_table-fields', array_dot(trans('forms::table-fields')));
            $event->load('forms_permissions', array_dot(trans('forms::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('forms', 'config');
        $this->publishConfig('forms', 'permissions');
        $this->publishConfig('forms', 'fields-type');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        // Form
        $this->app->bind(FormRepository::class, function () {
            $repository = new EloquentFormRepository(new Form());

            if (!Config::get('app.cache')) {
                return $repository;
            }

            return new CacheFormDecorator($repository);
        });

        // Field
        $this->app->bind(FieldRepository::class, function () {
            $repository = new EloquentFieldRepository(new Field());

            if (!Config::get('app.cache')) {
                return $repository;
            }

            return new CacheFieldDecorator($repository);
        });

        // Field Data
        $this->app->bind(FieldDataRepository::class, function () {
            $repository = new EloquentFieldDataRepository(new FieldData());

            return $repository;
        });

        // Regex
        $this->app->bind(RegexRepository::class, function () {
            $repository = new EloquentRegexRepository(new Regex());

            if (!Config::get('app.cache')) {
                return $repository;
            }

            return new CacheRegexDecorator($repository);
        });

        // Response
        $this->app->bind(ResponseRepository::class, function () {
            $repository = new EloquentResponseRepository(new Response());

            return $repository;
        });
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
