<?php

namespace Modules\Forms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Modules\Forms\Mail\FormMailable;
use Modules\Forms\Mail\FormAutoMailable;
use Modules\Forms\Repositories\FormRepository;
use Modules\Forms\Repositories\ResponseRepository;
use Modules\Core\Http\Controllers\CorePublicController;

class FormController extends CorePublicController
{
    /**
     * @var FormRepository
     */
    private $form;

    /**
     * @var ResponseRepository
     */
    private $form_response;

    public function __construct(FormRepository $form, ResponseRepository $form_response)
    {
        parent::__construct();

        $this->form = $form;
        $this->form_response = $form_response;
    }

    /**
     * @param int $form_id
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index($form_id, Request $request)
    {
        if ($request->ajax()) {
            $form_request = $request->except(['_method', '_token']);

            $form = $this->form->find($form_id);
            $fields = $form->fields;

            $validate_required_fields_array_rules = [];
            $validate_required_fields_array_niceNames = [];

            foreach ($fields as $field) {
                $fields_code_title_array[$field->unique_code] = $field->toArrayForForm();

                if ($field->field_type == 'separator') {
                    continue;
                }

                $validate_required_fields_array_niceNames[$field->unique_code] = $field->defaultTranslate()->title;

                if (boolval($field->required)) {
                    $validate_required_fields_array_rules[$field->unique_code] = 'required';
                }

                if ($field->field_type == 'small_text') {
                    if (array_key_exists($field->unique_code, $validate_required_fields_array_rules)) {
                        $validate_required_fields_array_rules[$field->unique_code] .= '|max:255';
                    } else {
                        $validate_required_fields_array_rules[$field->unique_code] = 'nullable|max:255';
                    }
                }

                if ($field->field_type == 'file' && null !== $field->accept) {
                    if (array_key_exists($field->unique_code, $validate_required_fields_array_rules)) {
                        $validate_required_fields_array_rules[$field->unique_code] .= '|mimetypes:' . $this->getMimeTypes($field->accept) . '|max:20480';
                    } else {
                        $validate_required_fields_array_rules[$field->unique_code] = 'mimetypes:' . $this->getMimeTypes($field->accept) . '|max:20480';
                    }
                } elseif ($field->field_type == 'file') {
                    $validate_required_fields_array_rules[$field->unique_code] = 'max:20480';
                }

                if ($field->field_type == 'email' && null === $field->default) {
                    $form_field_email = $form_request[$field->unique_code];

                    if (array_key_exists($field->unique_code, $validate_required_fields_array_rules)) {
                        $validate_required_fields_array_rules[$field->unique_code] .= '|email';
                    } elseif (null !== $form_request[$field->unique_code]) {
                        $validate_required_fields_array_rules[$field->unique_code] = 'nullable|email';
                    }
                }

                if ($field->field_type == 'multi_select') {
                    if (array_key_exists($field->unique_code, $validate_required_fields_array_rules)) {
                        $validate_required_fields_array_rules[$field->unique_code] .= '|array';
                    } else {
                        $validate_required_fields_array_rules[$field->unique_code] = 'nullable|array';
                    }
                }

                if ($field->regex && null !== $field->regex->regex_backend) {
                    if (array_key_exists($field->unique_code, $validate_required_fields_array_rules)) {
                        $validate_required_fields_array_rules[$field->unique_code] = explode('|', $validate_required_fields_array_rules[$field->unique_code]);
                        $validate_required_fields_array_rules[$field->unique_code][] = 'regex:' . $field->regex->regex_backend;
                    } else {
                        $validate_required_fields_array_rules[$field->unique_code] = ['nullable', 'regex:' . $field->regex->regex_backend];
                    }
                }
            }

            if (count($validate_required_fields_array_rules)) {
                $validator = \Validator::make($form_request, $validate_required_fields_array_rules);
                $validator->setAttributeNames($validate_required_fields_array_niceNames);

                if ($validator->fails()) {
                    return response()->json(['error' => true, 'msg' => $validator->messages()], 422);
                }
            }

            $form_request['form_id'] = $form->id;
            $form_request['locale'] = locale();
            $form_request['ip'] = ($request->ip() == '127.0.0.1') ? 'localhost' : $request->ip();
            $form_request['fields_code_title_array'] = $fields_code_title_array;

            $form_response = $this->form_response->create($form_request);

            if ($form_response) {
                $this->sendMail($form, $form_request, $validate_required_fields_array_niceNames, $form_field_email ?? null, $form->auto_response);
            } else {
                return response()->json(['error' => true, 'msg' => \Exception::getMessage()], 422);
            }
        }
    }

    /**
     * @param array $form
     * @param array $form_request
     * @param array $validate_required_fields_array_niceNames
     * @param string|null $form_field_email
     * @param string|null $form_auto_response
     *
     * @return \Illuminate\Http\Response
     */
    private function sendMail($form, $form_request, $validate_required_fields_array_niceNames, $form_field_email = null, $form_auto_response = null)
    {
        // Send email to user's email from emails field in form
        if (isset($form_field_email) && null !== $form_field_email && null !== $form_auto_response) {
            try {
                Mail::to(clear_email($form_field_email))->send(new FormAutoMailable($form_auto_response));
            } catch (\Exception $e) {
                dd(Mail::failures());
            }
        }

        $emails = $this->multiexplode([', ', ','], $form->emails);

        // Send email to admin emails set in form
        if (isset($emails[0]) && !empty($emails[0])) {
            if (count($emails) > 1) {
                try {
                    foreach ($emails as $email) {
                        Mail::to(clear_email($email))->send(new FormMailable($form, array_except($form_request, ['form_id', 'locale', 'ip', 'fields_code_title_array']), $validate_required_fields_array_niceNames));
                    }

                    return response()->json(200);
                } catch (\Exception $error) {
                    return response()->json(['error' => true, 'msg' => $error->getMessage()], 422);
                }
            } else {
                try {
                    Mail::to(clear_email($emails[0]))->send(new FormMailable($form, array_except($form_request, ['form_id', 'locale', 'ip', 'fields_code_title_array']), $validate_required_fields_array_niceNames));

                    return response()->json(200);
                } catch (\Exception $error) {
                    return response()->json(['error' => true, 'msg' => $error->getMessage()], 422);
                }
            }
        } else {
            return response()->json(200);
        }
    }

    /**
     * @param array $delimiters
     * @param string $string
     *
     * @return array
     */
    private function multiexplode($delimiters, $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);

        return $launch;
    }

    private function getMimeTypes($field_accept)
    {
        $mimes = config('mimes');

        $field_accept = str_replace(', ', ',', $field_accept);
        $field_accept_array = explode(',', $field_accept);

        foreach ($field_accept_array as $field_mime) {
            $validation_array[] = $mimes[$field_mime];
        }

        return implode(',', array_flatten($validation_array));
    }
}
