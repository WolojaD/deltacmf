<?php

namespace Modules\Forms\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class FormsController extends CoreAdminController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $id = 0)
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('core::router_view');
    }
}
