<?php

namespace Modules\Forms\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Forms\Repositories\FieldRepository;
use Modules\Forms\Transformers\FieldTransformer;
use Modules\Core\Internationalisation\Translatable;
use Modules\Forms\Http\Requests\CreateFieldRequest;
use Modules\Forms\Http\Requests\UpdateFieldRequest;
use Modules\Forms\Repositories\FieldDataRepository;
use Modules\Forms\Transformers\FieldDataTransformer;
use Modules\Forms\Transformers\FullFieldTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class FieldController extends CoreApiController
{
    use Translatable;

    /**
     * @var FieldRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $field_data;

    public function __construct(FieldRepository $field, FieldDataRepository $field_data)
    {
        $this->repository = $field;
        $this->modelInfo = $this->repository->getJsonList();
        $this->field_data = $field_data;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(
                [
                    'grand_parent_id' => $this->find($id)->form_id ?? 0,
                    'back_path_name' => 'api.backend.forms_form.show',
                    'where' => ['field_id' => $id]
                ]
            );

            $title = $this->repository->getTitleByid($id);
            $field_datas = $this->field_data->serverPaginationFilteringFor($request)
                ->changeDataByKey('title', $title);

            return FieldDataTransformer::collection($field_datas);
        }

        return FieldTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $field = $this->repository->find($id);

        return new FullFieldTransformer($field);
    }

    public function update($id, UpdateFieldRequest $request)
    {
        $field = $this->repository->find($id);

        $fieldRequest = $request->all();

        $request->merge(['form_id' => $field->form_id]);

        if (!$request->filled('unique_code')) {
            $request->merge(['unique_code' => str_slug($fieldRequest[$this->defaultLanguage()]['title'])]);
        }

        $request->validate(['unique_code' => 'unique:forms_fields,unique_code,' . $request->id . ',id,form_id,' . $field->form_id . '|max:191']);

        $this->repository->update($field, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.fields updated'),
            'id' => $field->id
        ]);
    }

    public function store(CreateFieldRequest $request)
    {
        $fieldRequest = $request->all();

        $request->merge(['form_id' => $request->ident]);

        if (!$request->filled('unique_code')) {
            $request->merge(['unique_code' => str_slug($fieldRequest[$this->defaultLanguage()]['title'])]);
        }

        $request->validate(['unique_code' => 'unique:forms_fields,unique_code,NULL,id,form_id,' . $request->ident . '|max:191']);

        $field = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.fields created'),
            'id' => $field->id
        ]);
    }

    public function destroy($id)
    {
        $field = $this->repository->find($id);

        $this->repository->destroy($field);

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.fields deleted'),
        ]);
    }
}
