<?php

namespace Modules\Forms\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Forms\Repositories\RegexRepository;
use Modules\Forms\Transformers\RegexTransformer;
use Modules\Forms\Http\Requests\CreateRegexRequest;
use Modules\Forms\Http\Requests\UpdateRegexRequest;
use Modules\Forms\Transformers\FullRegexTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class RegexController extends CoreApiController
{
    /**
     * @var RegexRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(RegexRepository $regex)
    {
        $this->repository = $regex;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return RegexTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return RegexTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return RegexTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $regex = $this->repository->find($id);

        return new FullRegexTransformer($regex);
    }

    public function update($id, UpdateRegexRequest $request)
    {
        $regex = $this->repository->find($id);

        $this->repository->update($regex, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.regex updated'),
            'id' => $regex->id
        ]);
    }

    public function store(CreateRegexRequest $request)
    {
        $regex = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.regex created'),
            'id' => $regex->id
        ]);
    }

    public function destroy($id)
    {
        $regex = $this->repository->find($id);

        $this->repository->destroy($regex);

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.regex deleted'),
        ]);
    }
}
