<?php

namespace Modules\Forms\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Forms\Entities\Form;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Forms\Entities\Response;
use Modules\Logs\Loggers\ActionsLogger;
use Modules\Forms\Repositories\ResponseRepository;
use Modules\Forms\Transformers\ResponseTransformer;
use Modules\Forms\Http\Requests\CreateResponseRequest;
use Modules\Forms\Http\Requests\UpdateResponseRequest;
use Modules\Forms\Transformers\FullResponseTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class ResponseController extends CoreApiController
{
    /**
     * @var ResponseRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(ResponseRepository $response)
    {
        $this->repository = $response;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['form_id' => $id]]);
        }

        return ResponseTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $response = $this->repository->find($id);

        return new FullResponseTransformer($response);
    }

    public function update($id, UpdateResponseRequest $request)
    {
        $response = $this->repository->find($id);

        $this->repository->update($response, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('Response updated'),
            'id' => $response->id
        ]);
    }

    public function store(CreateResponseRequest $request)
    {
        $response = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('Response created'),
            'id' => $response->id
        ]);
    }

    public function findForms()
    {
        return Form::has('responses')->get();
    }

    /**
     * @param  Request $request
     *
     * @return Excel
     */
    public function exportExcel(Request $request)
    {
        preg_match('/api.backend.([a-z]+)_([a-z]+).exportExcel$/', $request->route()->getName(), $model_match);
        $main_model = $model_match[2];
        $file_name = str_plural($main_model) . ' ' . \Carbon\Carbon::now()->format('d-m-Y H:i:s') . '.xlsx';

        $collection = $this->repository->collectionForExport($request);

        (new ActionsLogger($main_model, 'excel exported'))->write();

        $temp_collection = $collection->collection()->map(function ($item) {
            $string = '';

            $item['form_id'] = Form::find($item['form_id'])->title;
            $item['email'] = str_replace(';', "\n", $item['email']);
            $item['phone'] = str_replace(';', "\n", $item['phone']);

            foreach ($item['response'] as $response) {
                if (strpos($response['value'], '/forms/') !== false) {
                    $string .= $response['title'] . ': ' . url('/') . $response['value'] . "\n";
                } else {
                    $string .= $response['title'] . ': ' . $response['value'] . "\n";
                }
            }

            $item['response'] = $string;

            return $item;
        });

        $collection->setCollection($temp_collection);

        return Excel::download($collection, $file_name);
    }

    public function destroy($id)
    {
        $response = $this->repository->find($id);

        $this->repository->destroy($response);

        return response()->json([
            'errors' => false,
            'message' => trans('Response deleted'),
        ]);
    }
}
