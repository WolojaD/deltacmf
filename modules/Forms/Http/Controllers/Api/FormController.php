<?php

namespace Modules\Forms\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Forms\Repositories\FormRepository;
use Modules\Forms\Repositories\FieldRepository;
use Modules\Forms\Transformers\FormTransformer;
use Modules\Forms\Transformers\FieldTransformer;
use Modules\Forms\Http\Requests\CreateFormRequest;
use Modules\Forms\Http\Requests\UpdateFormRequest;
use Modules\Forms\Transformers\FullFormTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class FormController extends CoreApiController
{
    /**
     * @var FormRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    /**
     * @var FieldRepository
     */
    protected $field;

    public function __construct(FormRepository $form, FieldRepository $field)
    {
        $this->repository = $form;
        $this->modelInfo = $this->repository->getJsonList();
        $this->field = $field;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['form_id' => $id]]);

            return FieldTransformer::collection($this->field->serverPaginationFilteringFor($request));
        }

        return FormTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $form = $this->repository->find($id);

        return new FullFormTransformer($form);
    }

    public function update($id, UpdateFormRequest $request)
    {
        if ($request->filled('redirect')) {
            if (starts_with($request->redirect, request()->root())) {
                $request->merge(['redirect' => str_replace(request()->root(), '', $request->redirect)]);
            }

            $request->merge(['redirect' => $request->redirect]);
        }

        $form = $this->repository->find($id);

        $this->repository->update($form, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.forms updated'),
            'id' => $form->id
        ]);
    }

    public function store(CreateFormRequest $request)
    {
        if ($request->filled('redirect')) {
            if (starts_with($request->redirect, request()->root())) {
                $request->merge(['redirect' => str_replace(request()->root(), '', $request->redirect)]);
            }

            $request->merge(['redirect' => $request->redirect]);
        }

        $form = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.forms created'),
            'id' => $form->id
        ]);
    }

    public function destroy($id)
    {
        $form = $this->repository->find($id);

        $this->repository->destroy($form);

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.forms deleted'),
        ]);
    }
}
