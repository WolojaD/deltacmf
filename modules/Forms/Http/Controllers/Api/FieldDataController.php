<?php

namespace Modules\Forms\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Forms\Repositories\FieldDataRepository;
use Modules\Forms\Transformers\FieldDataTransformer;
use Modules\Forms\Http\Requests\CreateFieldDataRequest;
use Modules\Forms\Http\Requests\UpdateFieldDataRequest;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Forms\Transformers\FullFieldDataTransformer;

class FieldDataController extends CoreApiController
{
    /**
     * @var FieldDataRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(FieldDataRepository $fielddata)
    {
        $this->repository = $fielddata;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return FieldDataTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return FieldDataTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return FieldDataTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $fielddata = $this->repository->find($id);

        return new FullFieldDataTransformer($fielddata);
    }

    public function update($id, UpdateFieldDataRequest $request)
    {
        $fielddata = $this->repository->find($id);

        $this->repository->update($fielddata, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.fields updated'),
            'id' => $fielddata->id
        ]);
    }

    public function store(CreateFieldDataRequest $request)
    {
        $fielddata = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.fields created'),
            'id' => $fielddata->id
        ]);
    }

    public function destroy($id)
    {
        $fielddata = $this->repository->find($id);

        $this->repository->destroy($fielddata);

        return response()->json([
            'errors' => false,
            'message' => trans('forms::messages.api.fields deleted'),
        ]);
    }
}
