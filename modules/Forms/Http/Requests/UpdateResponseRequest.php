<?php

namespace Modules\Forms\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateResponseRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
           // 'template' => 'required',
        ];
    }

    public function translationRules()
    {
        return [
            // 'title' => 'required',
        ];
    }

    public function messages()
    {
        return [
            // 'template.required' => trans('page::messages.template is required'),
        ];
    }

    public function translationMessages()
    {
        return [
            // 'title.required' => trans('page::messages.title is required'),
        ];
    }
}
