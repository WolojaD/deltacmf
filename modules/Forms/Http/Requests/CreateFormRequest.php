<?php

namespace Modules\Forms\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateFormRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'comment' => 'required|max:191',
            'short_code' => 'required|max:191|regex:/^[A-Za-z\s-_]+$/',
            'emails' => 'required|regex:/^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/u',
            'redirect' => 'nullable|url',
        ];
    }

    public function translationRules()
    {
        return [
            'title' => 'required|max:191',
            'description' => 'max:500',
            'button_title' => 'max:191',
        ];
    }

    public function messages()
    {
        return [
            // 'template.required' => trans('page::messages.template is required'),
        ];
    }

    public function translationMessages()
    {
        return [
            // 'title.required' => trans('page::messages.title is required'),
        ];
    }
}
