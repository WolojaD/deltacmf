<?php

namespace Modules\Forms\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateFieldDataRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function translationRules()
    {
        return [
            'title' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            // 'template.required' => trans('page::messages.template is required'),
        ];
    }

    public function translationMessages()
    {
        return [
            // 'title.required' => trans('page::messages.title is required'),
        ];
    }
}
