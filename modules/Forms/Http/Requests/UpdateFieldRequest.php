<?php

namespace Modules\Forms\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateFieldRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'field_type' => 'required',
            'unique_code' => 'required|max:191',
            'accept' => 'nullable|regex:/^[a-zA-Z]+(,\s?[a-zA-Z]+)*$/u',
            'class' => 'max:191',
            'accept' => 'max:500',
        ];
    }

    public function translationRules()
    {
        return [
            'title' => 'required|max:255',
            'placeholder' => 'max:255',
            'comment' => 'max:255',
            'default' => 'max:255',
        ];
    }

    public function messages()
    {
        return [
            // 'template.required' => trans('page::messages.template is required'),
        ];
    }

    public function translationMessages()
    {
        return [
            // 'title.required' => trans('page::messages.title is required'),
        ];
    }
}
