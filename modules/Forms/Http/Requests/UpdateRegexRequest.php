<?php

namespace Modules\Forms\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateRegexRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'regex_frontend' => 'required|max:500',
            'regex_backend' => 'required|max:500',
        ];
    }

    public function translationRules()
    {
        return [
            // 'title' => 'required',
        ];
    }

    public function messages()
    {
        return [
            // 'template.required' => trans('page::messages.template is required'),
        ];
    }

    public function translationMessages()
    {
        return [
            // 'title.required' => trans('page::messages.title is required'),
        ];
    }

    protected function prepareForValidation()
    {
        if ($this->has('regex_backend')) {
            try {
                preg_match($this->regex_backend, 'qwertyuiop');
            } catch (\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                   'regex_backend' => $e->getMessage(),
                ]);

                throw $error;
            }
        }
    }
}
