<?php

return [
    'field_type' => [
        'small_text' => trans('forms::field-types.small_text'),
        'middle_text' => trans('forms::field-types.middle_text'),
        'phone' => trans('forms::field-types.phone'),
        'email' => trans('forms::field-types.email'),
        'integer' => trans('forms::field-types.integer'),
        'file' => trans('forms::field-types.file'),
        'boolean' => trans('forms::field-types.boolean'),
        'separator' => trans('forms::field-types.separator'),
        'select' => trans('forms::field-types.select'),
        'multi_select' => trans('forms::field-types.multi_select'),
        'hidden' => trans('forms::field-types.hidden'),
    ],
];
