<?php

namespace Modules\Forms\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FormAutoMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var type
     */
    private $form_auto_response;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form_auto_response)
    {
        $this->form_auto_response = $form_auto_response;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(
                config('mail')['from']['address'] ?? strtolower(config('app.name') . '@mail.com'),
                config('mail')['from']['name'] ?? config('app.name')
            )
            ->subject(trans('forms::mailable.auto mailable.subject') ?? config('mail')['from']['name'])
            ->view('forms::mail.auto_response')
            ->with(['data' => $this->form_auto_response]);
    }
}
