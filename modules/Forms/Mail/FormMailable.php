<?php

namespace Modules\Forms\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FormMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var type
     */
    private $form;

    /**
     * @var type
     */
    private $form_data;

    /**
     * @var type
     */
    private $fields_niceNames;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form, $form_data, $fields_niceNames)
    {
        $this->form = $form;
        $this->form_data = $form_data;
        $this->fields_niceNames = $fields_niceNames;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(
                config('mail')['from']['address'] ?? strtolower(config('app.name') . '@mail.com'),
                config('mail')['from']['name'] ?? config('app.name')
            )
            ->subject($this->form->defaultTranslate()->title ?? config('mail')['from']['name'])
            ->view('forms::mail.index')
            ->with(['data' => $this->form_data, 'fieldsNiceNames' => $this->fields_niceNames]);
    }
}
