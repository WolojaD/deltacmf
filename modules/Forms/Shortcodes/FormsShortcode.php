<?php

namespace Modules\Forms\Shortcodes;

use Modules\Forms\Entities\Form;

class FormsShortcode
{
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        $form = Form::when(!is_numeric($shortcode->code), function ($q) use ($shortcode) {
            $q->where('short_code', $shortcode->code);
        }, function ($q) use ($shortcode) {
            $q->where('id', $shortcode->code);
        })->with('translations', 'fields')->first();

        if (null === $form) {
            return '';
        }

        if ($form) {
            return $form->fields && $form->fields->count() ? view('forms::' . $form->forms_template, compact('form')) : '';
        } else {
            return '';
        }
    }
}
