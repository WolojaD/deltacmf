<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Forms');

Route::group(['prefix' => '/response'], function () {
    Route::get('find-froms', ['as' => 'api.backend.forms_response.findForms', 'uses' => 'ResponseController@findForms', 'middleware' => 'token-can:forms.response.index']);
});
