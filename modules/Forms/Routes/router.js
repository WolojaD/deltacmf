import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/forms',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.forms_form',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Forms',
                    breadcrumb: [
                        { name: 'Forms' }
                    ]
                }
            },
            // {
            //     path: 'fld/:ident(\\d+)',
            //     name: 'api.backend.forms_field',
            //     redirect: ':ident(\\d+)'
            // },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.forms_form.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Fields',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Fields' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.forms_form.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Form Create',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Form Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.forms_form.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Form Edit',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Form Edit' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/forms/field',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':ident(\\d+)',
                name: 'api.backend.forms_field.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Field Data',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Fields', link: 'api.backend.forms_field' },
                        { name: 'Field Data' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.forms_field.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Field Create',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Fields', link: 'api.backend.forms_field' },
                        { name: 'Field Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.forms_field.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Field Edit',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Fields', link: 'api.backend.forms_field' },
                        { name: 'Field Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/forms/field_data',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.forms_field_data.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Field Data Create',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Fields', link: 'api.backend.forms_field' },
                        { name: 'Field Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.forms_field_data.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Field Data Edit',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Fields', link: 'api.backend.forms_field' },
                        { name: 'Field Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/forms/regex',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.forms_regex',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Regex',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Regex' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.forms_regex.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Regex Create',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Regex', link: 'api.backend.forms_regex' },
                        { name: 'Regex Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.forms_regex.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Regex Edit',
                    breadcrumb: [
                        { name: 'Forms', link: 'api.backend.forms_form' },
                        { name: 'Regex', link: 'api.backend.forms_regex' },
                        { name: 'Regex Edit' }
                    ]
                }
            },
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/forms/response',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.forms_response',
                component: require('@/views/forms/response/index').default,
                meta: {
                    pageTitle: 'Forms response',
                    breadcrumb: [
                        { name: 'Forms response' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.forms_response.show',
                component: require('@/views/forms/response/index').default,
                meta: {
                    pageTitle: 'Forms response',
                    breadcrumb: [
                        { name: 'Forms response' }
                    ]
                }
            }
        ]
    }
]
