<?php

namespace Modules\Forms\Repositories\Eloquent;

use Modules\Core\Traits\DraggableTrait;
use Modules\Core\Traits\StatusableTrait;
use Modules\Forms\Repositories\FieldDataRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentFieldDataRepository extends EloquentCoreRepository implements FieldDataRepository
{
    use StatusableTrait, DraggableTrait;

    public $model_config = 'modules/Forms/Config/models/field_data.json';
}
