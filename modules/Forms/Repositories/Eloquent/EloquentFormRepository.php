<?php

namespace Modules\Forms\Repositories\Eloquent;

use Modules\Core\Traits\StatusableTrait;
use Modules\Forms\Entities\FormTranslation;
use Modules\Forms\Repositories\FormRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentFormRepository extends EloquentCoreRepository implements FormRepository
{
    use StatusableTrait;

    public $model_config = 'modules/Forms/Config/models/form.json';

    /**
     * Returns title value of form by id
     *
     * @param integer $id
     *
     * @return string
     */
    public function getTitleById($id)
    {
        return FormTranslation::where('form_id', $id)
            ->where('locale', locale())
            ->select('title')
            ->first()
            ->title ?? '';
    }
}
