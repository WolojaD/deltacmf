<?php

namespace Modules\Forms\Repositories\Eloquent;

use Modules\Forms\Entities\Regex;
use Modules\Core\Traits\DraggableTrait;
use Modules\Core\Traits\StatusableTrait;
use Modules\Forms\Entities\FieldTranslation;
use Modules\Forms\Repositories\FieldRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentFieldRepository extends EloquentCoreRepository implements FieldRepository
{
    use StatusableTrait, DraggableTrait;

    public $model_config = 'modules/Forms/Config/models/field.json';

    protected function getFieldTypeValues()
    {
        return config('application.forms.fields-type.field_type');
    }

    protected function tableForRegex()
    {
        $regex = Regex::all();

        if ($regex->count()) {
            return $regex->toArray();
        }

        return null;
    }

    /**
     * Returns title value of field by id
     *
     * @param integer $id
     *
     * @return string
     */
    public function getTitleById($id)
    {
        return FieldTranslation::where('field_id', $id)
            ->where('locale', locale())
            ->select('title')
            ->first()
            ->title ?? '';
    }
}
