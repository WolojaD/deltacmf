<?php

namespace Modules\Forms\Repositories\Eloquent;

use Modules\Core\Traits\DraggableTrait;
use Modules\Core\Traits\StatusableTrait;
use Modules\Forms\Repositories\RegexRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentRegexRepository extends EloquentCoreRepository implements RegexRepository
{
    use StatusableTrait, DraggableTrait;

    public $model_config = 'modules/Forms/Config/models/regex.json';
}
