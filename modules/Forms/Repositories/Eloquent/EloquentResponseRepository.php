<?php

namespace Modules\Forms\Repositories\Eloquent;

use Illuminate\Http\UploadedFile;
use Modules\Forms\Repositories\ResponseRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentResponseRepository extends EloquentCoreRepository implements ResponseRepository
{
    public $model_config = 'modules/Forms/Config/models/response.json';

    /**
     * @param array $data
     *
     * @return type
     */
    public function create($data)
    {
        $attributes['name'] = '';
        $attributes['email'] = '';
        $attributes['phone'] = '';
        $attributes['response'] = [];

        foreach ($data['fields_code_title_array'] as $unique_code => $field_data) {
            if (isset($data[$unique_code]) && null !== $data[$unique_code] or $field_data['field_type'] == 'boolean') {
                if ($field_data['field_type'] == 'small_text' && preg_match("/\b(name|title)\b/", $unique_code) === 1) {
                    $attributes['name'] .= $data[$unique_code];

                    continue;
                }

                if ($field_data['field_type'] == 'email') {
                    $attributes['email'] .= $data[$unique_code] . ';';

                    continue;
                }

                if ($field_data['field_type'] == 'phone') {
                    $attributes['phone'] .= $data[$unique_code] . ';';

                    continue;
                }

                if ($field_data['field_type'] == 'boolean') {
                    if (isset($data[$unique_code]) && !empty($data[$unique_code])) {
                        if (locale() == 'en') {
                            $data[$unique_code] = trans('forms::table-fields.response.boolean true', [], 'en');
                        } else {
                            $data[$unique_code] = trans('forms::table-fields.response.boolean true', [], 'ru');
                        }
                    } else {
                        if (locale() == 'en') {
                            $data[$unique_code] = trans('forms::table-fields.response.boolean false', [], 'en');
                        } else {
                            $data[$unique_code] = trans('forms::table-fields.response.boolean false', [], 'ru');
                        }
                    }
                }

                if ($field_data['field_type'] == 'file') {
                    $data[$unique_code] = $this->uploadForm($data[$unique_code], $data['form_id']);
                }

                if ($field_data['field_type'] == 'multi_select') {
                    if (count($data[$unique_code])) {
                        $multi_select_value = '';

                        foreach ($data[$unique_code] as $title => $value) {
                            $multi_select_value .= $title . '; ';
                        }

                        $data[$unique_code] = $multi_select_value;
                    }
                }

                $attributes['response'][] = [
                    'title' => $field_data['title'],
                    'value' => $data[$unique_code]
                ];
            }
        }

        $attributes['form_id'] = $data['form_id'];
        $attributes['locale'] = $data['locale'];
        $attributes['ip'] = $data['ip'];

        $response = $this->model->create($attributes);

        return $response;
    }

    /**
     * @param type $value
     * @param int $form_id
     *
     * @return string
     */
    private function uploadForm($value, $form_id)
    {
        $isValid = $value instanceof UploadedFile && $value->isValid();

        if ($isValid) {
            $new_file_name = md5($value->getClientOriginalName() . time()) . '.' . $value->getClientOriginalExtension();
            $entity = $this->model->latest()->first();
            $id = $entity ? $entity->id + 1 : 1;
            $path = 'forms/' . $form_id . '/' . $id;
            $value->storeAs($path, $new_file_name, 'public');

            return '/' . $path . '/' . $new_file_name;
        }
    }

    /**
     * @param $columns
     *
     * @return mixed
     */
    public function selectListForExportExcel($columns)
    {
        return [
            'form_id' => 'form_id',
            'name' => 'name',
            'email' => 'email',
            'phone' => 'phone',
            'response' => 'response',
            'created_at' => 'created_at',
        ];
    }
}
