<?php

namespace Modules\Forms\Repositories\Cache;

use Modules\Forms\Repositories\FormRepository;
use Modules\Core\Traits\Cache\CacheDraggableTrait;
use Modules\Core\Traits\Cache\CacheStatusableTrait;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheFormDecorator extends CoreCacheDecorator implements FormRepository
{
    use CacheStatusableTrait, CacheDraggableTrait;

    /**
    * @var FormRepository
    */
    protected $repository;

    public function __construct(FormRepository $form)
    {
        parent::__construct();

        $this->entityName = 'forms';
        $this->repository = $form;
    }
}
