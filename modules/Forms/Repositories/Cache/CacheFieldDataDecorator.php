<?php

namespace Modules\Forms\Repositories\Cache;

use Modules\Forms\Repositories\FieldDataRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheFieldDataDecorator extends CoreCacheDecorator implements FieldDataRepository
{
    /**
    * @var FieldDataRepository
    */
    protected $repository;

    public function __construct(FieldDataRepository $forms)
    {
        parent::__construct();

        $this->repository = $forms;
    }
}
