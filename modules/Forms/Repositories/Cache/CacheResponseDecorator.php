<?php

namespace Modules\Forms\Repositories\Cache;

use Modules\Core\Traits\Cache\CacheDraggableTrait;
use Modules\Forms\Repositories\ResponseRepository;
use Modules\Core\Traits\Cache\CacheStatusableTrait;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheResponseDecorator extends CoreCacheDecorator implements ResponseRepository
{
    use CacheStatusableTrait, CacheDraggableTrait;

    /**
    * @var ResponseRepository
    */
    protected $repository;

    public function __construct(ResponseRepository $response)
    {
        parent::__construct();

        $this->repository = $response;
    }
}
