<?php

namespace Modules\Forms\Repositories\Cache;

use Modules\Forms\Repositories\FieldRepository;
use Modules\Core\Traits\Cache\CacheDraggableTrait;
use Modules\Core\Traits\Cache\CacheStatusableTrait;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheFieldDecorator extends CoreCacheDecorator implements FieldRepository
{
    use CacheStatusableTrait, CacheDraggableTrait;

    /**
    * @var FieldRepository
    */
    protected $repository;

    public function __construct(FieldRepository $field)
    {
        parent::__construct();

        $this->entityName = 'forms_fields';
        $this->repository = $field;
    }
}
