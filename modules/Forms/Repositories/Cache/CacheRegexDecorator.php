<?php

namespace Modules\Forms\Repositories\Cache;

use Modules\Forms\Repositories\RegexRepository;
use Modules\Core\Traits\Cache\CacheDraggableTrait;
use Modules\Core\Traits\Cache\CacheStatusableTrait;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheRegexDecorator extends CoreCacheDecorator implements RegexRepository
{
    use CacheStatusableTrait, CacheDraggableTrait;

    /**
    * @var RegexRepository
    */
    protected $repository;

    public function __construct(RegexRepository $regex)
    {
        parent::__construct();

        $this->entityName = 'forms_regexes';
        $this->repository = $regex;
    }
}
