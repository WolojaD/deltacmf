<?php

use Faker\Generator as Faker;
use Modules\Forms\Entities\Form;
use Modules\Forms\Entities\Field;
use Modules\Forms\Entities\Regex;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Form::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'short_code' => $faker->lexify('?????????'),
        'comment' => $faker->sentence,
        'emails' => $faker->email,
        'show_personal_data_message' => $faker->numberBetween(0, 1),
        'show_title' => $faker->numberBetween(0, 1),
        'redirect' => $faker->numberBetween(0, 1) == 1 ? $faker->url : null,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'message' => $faker->sentence,
            'auto_response' => $faker->numberBetween(0, 1) == 1 ? $faker->sentence : null,
            'button_title' => $faker->numberBetween(0, 1) == 1 ? $faker->sentence : null,
        ];
    }

    return $data;
});

$factory->define(Regex::class, function (Faker $faker) {
    $data = [
        'regex_frontend' => $faker->uuid . ' ' . $faker->uuid,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'error_message' => $faker->text($maxNbChars = 350),
        ];
    }

    return $data;
});
$factory->state(Regex::class, 'phone', [
    'title' => 'Телефон',
    'regex_backend' => '/^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/',
]);
$factory->state(Regex::class, 'fio', [
    'title' => 'ФИО',
    'regex_backend' => '/^[\wа-яА-Я`ґєҐЄ´ІіЇї\'.,-№\/\s]{3,}$/',
]);
$factory->state(Regex::class, 'email', [
    'title' => 'E-mail',
    'regex_backend' => '/^[a-z0-9\-\_\.\+]+@[a-z0-9\-\_\.]+\.[a-z]+$/',
]);

$factory->define(Field::class, function (Faker $faker) {
    $data = [
        'status' => $faker->numberBetween(0, 1),
        'form_id' => $faker->numberBetween(1, 4),
        'regex_id' => $faker->numberBetween(1, 3),
        'unique_code' => $faker->word . $faker->unique($reset = true)->randomDigitNotNull . $faker->unique($reset = true)->randomDigitNotNull . $faker->unique($reset = true)->randomDigitNotNull,
        'required' => $faker->numberBetween(0, 1),
        'class' => $faker->numberBetween(0, 1) == 1 ? $faker->word : null,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'comment' => $faker->sentence,
            'default' => $faker->numberBetween(0, 1) == 1 ? $faker->sentence : null,
        ];
    }

    return $data;
});
$factory->state(Field::class, 'small_text', [
    'field_type' => 'small_text',
]);
$factory->state(Field::class, 'middle_text', [
    'field_type' => 'middle_text',
]);
$factory->state(Field::class, 'phone', [
    'field_type' => 'phone',
]);
$factory->state(Field::class, 'email', [
    'field_type' => 'email',
]);
$factory->state(Field::class, 'boolean', [
    'field_type' => 'boolean',
]);
$factory->state(Field::class, 'hidden', [
    'field_type' => 'hidden',
]);
$factory->state(Field::class, 'file', [
    'field_type' => 'file',
]);
