<?php

namespace Modules\Forms\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Forms\Entities\Form;
use Modules\Forms\Entities\Field;
use Modules\Forms\Entities\Regex;

class FormsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Form::class, 4)->create();
            factory(Regex::class, 1)->states('phone')->create();
            factory(Regex::class, 1)->states('fio')->create();
            factory(Regex::class, 1)->states('email')->create();
            factory(Field::class, random_int(1, 5))->states('small_text')->create();
            factory(Field::class, random_int(1, 5))->states('middle_text')->create();
            factory(Field::class, random_int(1, 5))->states('phone')->create();
            factory(Field::class, random_int(1, 5))->states('email')->create();
            factory(Field::class, random_int(1, 5))->states('boolean')->create();
            factory(Field::class, random_int(1, 5))->states('hidden')->create();
            factory(Field::class, random_int(1, 5))->states('file')->create();
        }
    }
}
