<?php

namespace Modules\Banners\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullGroupTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->getAttributes();

        return $data;
    }
}
