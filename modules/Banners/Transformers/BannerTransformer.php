<?php

namespace Modules\Banners\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class BannerTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = ['id' => $this->id, 'status' => $this->status ?? '', 'description' => $this->description];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['statistics'] = trans('banners::table-fields.banners.statistics');

        return $result;
    }
}
