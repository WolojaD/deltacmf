<?php

namespace Modules\Banners\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class GroupTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $headers = $this->resource->getHeadersOfCollectionFor('default');
        $result = [
            'id' => $this->id, 'status' => $this->status ?? '',
            'children_count' => $this->banners->count(),
            'comment' => $this->comment
        ];

        foreach ($headers as $field => $header) {
            $result[$field] = $this->$field;
        }

        $result['banners_active'] = $this->banners->where('status', 1)->count();

        return $result;
    }
}
