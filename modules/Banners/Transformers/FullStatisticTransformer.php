<?php

namespace Modules\Banners\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullStatisticTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'ip' => $this->ip,
            'device_is_mobile' => !!$this->device_is_mobile ? trans('banners::statistics.yes') : trans('banners::statistics.no'),
            'is_robot' => !!$this->is_robot ? trans('banners::statistics.yes') : trans('banners::statistics.no'),
            'is_click' => !!$this->is_click ? trans('banners::statistics.yes') : trans('banners::statistics.no'),
            'device_data' => '<div>' .
                '<p>' . '<b>' . trans('banners::statistics.device kind') . ': </b>' . $this->device_kind . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.device model') . ': </b>' . $this->device_model . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.device platform') . ': </b>' . $this->device_platform . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.device platform version') . ': </b>' . $this->device_platform_version . '</p>' .
                '</div>',
            'agent_data' => '<div>' .
                '<p>' . '<b>' . trans('banners::statistics.agent browser') . ': </b>' . $this->agent_browser . ' (' . $this->agent_browser_version . ')</p>' .
                '</div>',
            'referer_data' => '<div>' .
                '<p>' . '<b>' . trans('banners::statistics.referer_domain') . ': </b>' . $this->referer_domain . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.referer_url') . ': </b>' . $this->referer_url . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.referer_host') . ': </b>' . $this->referer_host . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.referer_medium') . ': </b>' . $this->referer_medium . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.referer_source') . ': </b>' . $this->referer_source . '</p>' .
                '<p>' . '<b>' . trans('banners::statistics.referer_search_terms_hash') . ': </b>' . $this->referer_search_terms_hash . '</p>' .
                '</div>',
            'created_at' => $this->created_at->toRfc2822String(),
        ];

        return $data;
    }
}
