<?php

namespace Modules\Banners\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class StatisticTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => 1,
            'date' => $this['day'],
            'clicked' => $this['clicked'],
            'not_clicked' => $this['not_clicked'],
        ];
    }
}
