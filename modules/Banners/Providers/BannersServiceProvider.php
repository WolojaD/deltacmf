<?php

namespace Modules\Banners\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Factory;
use Modules\Banners\Support\MobileDetect;
use Webwizo\Shortcodes\Facades\Shortcode;
use Modules\Banners\Support\RefererParser;
use Modules\Core\Providers\ServiceProvider;
use Modules\Banners\Support\UserAgentParser;
use Modules\Banners\Entities\BannerStatistic;
use Modules\Banners\Shortcodes\BannersShortcode;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Banners\Repositories\StatisticCrawlerInterface;
use Modules\Banners\Repositories\Eloquent\EloquentStatisticCrawlerRepository;

class BannersServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        Shortcode::register('banners', BannersShortcode::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('banners_sidebar', array_dot(trans('banners::sidebar')));
            $event->load('banners_table-fields', array_dot(trans('banners::table-fields')));
            $event->load('banners_permissions', array_dot(trans('banners::permissions')));
        });
    }

    public function boot()
    {
        $this->publishConfig('banners', 'config');
        $this->publishConfig('banners', 'permissions');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $folder = base_path('modules/Banners/Config/models/');

        $files = scandir($folder);

        $models = [];

        foreach ($files as $key => $file) {
            if ('.' !== $file[0]) {
                $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
            }
        }

        foreach ($models as $model_name => $model) {
            $model_name = studly_case($model_name);
            $this->model = $model;

            $this->app->bind("Modules\Banners\Repositories\\{$model_name}Interface", function () use ($model_name, $model) {
                $eloquent_repository = "\Modules\Banners\Repositories\Eloquent\Eloquent{$model_name}Repository";
                $entity = "\Modules\Banners\Entities\\{$model_name}";
                $cache_decorator = "\Modules\Banners\Repositories\Cache\Cache{$model_name}Decorator";

                $repository = new $eloquent_repository(new $entity());

                if (!Config::get('app.cache') && !($model->no_cache ?? false)) {
                    return $repository;
                }

                return new $cache_decorator($repository);
            });
        }

        $this->app->bind(StatisticCrawlerInterface::class, function () {
            $repository = new EloquentStatisticCrawlerRepository(
                new BannerStatistic(),
                new MobileDetect(),
                new UserAgentParser(),
                new RefererParser()
            );

            return $repository;
        });
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
