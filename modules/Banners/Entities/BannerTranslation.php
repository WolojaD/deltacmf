<?php

namespace Modules\Banners\Entities;

use Modules\Core\Traits\FileUploaderTrait;
use Modules\Core\Traits\ImageProcessTrait;
use Modules\Core\Eloquent\TranslationModel;

class BannerTranslation extends TranslationModel
{
    use ImageProcessTrait, FileUploaderTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'banner_translations';

    protected $file_path = 'modules/Banners/Config/models/banner.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
