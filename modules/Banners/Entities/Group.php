<?php

namespace Modules\Banners\Entities;

use Modules\Core\Eloquent\Model;

class Group extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'banners_groups';

    protected static $entityNamespace = 'application/banners';

    protected $file_path = 'modules/Banners/Config/models/group.json';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        static::deleting(function ($obj) {
            $obj->banners->each(function ($banner) {
                $banner->delete();
            });
        });

        parent::boot();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function banners()
    {
        return $this->hasMany(Banner::class);
    }

    public function active_banners()
    {
        return $this->hasMany(Banner::class)->where('status', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
