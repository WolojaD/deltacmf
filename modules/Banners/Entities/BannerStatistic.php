<?php

namespace Modules\Banners\Entities;

use Illuminate\Database\Eloquent\Model;

class BannerStatistic extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'banner_statistics';
    protected $fillable = [
        'banner_id',
        'ip',
        'device_kind',
        'device_model',
        'device_platform',
        'device_platform_version',
        'device_is_mobile',
        'agent_name',
        'agent_browser',
        'agent_browser_version',
        'referer_domain',
        'referer_url',
        'referer_host',
        'referer_medium',
        'referer_source',
        'referer_search_terms_hash',
        'is_robot',
        'is_click',
    ];
    protected $casts = [
        'banner_id' => 'int',
        'device_is_mobile' => 'boolean',
        'is_robot' => 'boolean',
        'is_click' => 'boolean',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function banner()
    {
        return $this->belongsTo(Banner::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
