<?php

namespace Modules\Banners\Repositories\Cache;

use Modules\Banners\Repositories\BannerInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheBannerDecorator extends CoreCacheDecorator implements BannerInterface
{
    /**
    * @var BannerInterface
    */
    protected $repository;

    public function __construct(BannerInterface $banners)
    {
        parent::__construct();

        $this->entityName = 'banners';
        $this->repository = $banners;
    }
}
