<?php

namespace Modules\Banners\Repositories\Cache;

use Modules\Banners\Repositories\GroupInterface;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheGroupDecorator extends CoreCacheDecorator implements GroupInterface
{
    /**
    * @var GroupInterface
    */
    protected $repository;

    public function __construct(GroupInterface $banners)
    {
        parent::__construct();

        $this->entityName = 'banners_groups';
        $this->repository = $banners;
    }
}
