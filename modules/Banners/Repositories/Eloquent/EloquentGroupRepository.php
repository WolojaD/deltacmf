<?php

namespace Modules\Banners\Repositories\Eloquent;

use Modules\Banners\Repositories\GroupInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentGroupRepository extends EloquentCoreRepository implements GroupInterface
{
    public $model_config = 'modules/Banners/Config/models/group.json';
}
