<?php

namespace Modules\Banners\Repositories\Eloquent;

use Illuminate\Pagination\Paginator;
use Modules\Banners\Entities\Banner;
use Modules\Core\Helpers\BackendPaginator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Banners\Repositories\StatisticCrawlerInterface;

class EloquentStatisticCrawlerRepository implements StatisticCrawlerInterface
{
    protected $model;

    /**
     * @var Modules\Banners\Support\UserAgentParser
     */
    private $userAgentParser;

    /**
     * @var Modules\Banners\Support\MobileDetect
     */
    private $mobileDetect;

    /**
     * @var Modules\Banners\Support\RefererParser
     */
    private $refererParser;

    public function __construct(
        $model,
        $mobileDetect,
        $userAgentParser,
        $refererParser
    ) {
        $this->model = $model;
        $this->mobileDetect = $mobileDetect;
        $this->userAgentParser = $userAgentParser;
        $this->refererParser = $refererParser;
        $this->banner = (new EloquentBannerRepository(new Banner));
    }

    /**
     * @return array
     */
    public function getCurrentDeviceProperties()
    {
        if ($properties = $this->getDevice()) {
            $properties['device_platform'] = $this->getOperatingSystemFamily();
            $properties['device_platform_version'] = $this->getOperatingSystemVersion();
        }

        return $properties;
    }

    /**
     * @return array
     */
    public function getCurrentAgentArray()
    {
        return [
            'agent_name' => $this->getCurrentUserAgent() ?: 'Other',
            'agent_browser' => $this->userAgentParser->userAgent,
            'agent_browser_version' => $this->userAgentParser->getUserAgentVersion(),
        ];
    }

    /**
     * @param string $referer
     * @param tystringpe $currentUrl
     *
     * @return array
     */
    public function getRefererArray($referer, $currentUrl)
    {
        if ($referer) {
            $url = parse_url($referer);

            if (!isset($url['host'])) {
                return;
            }

            $parts = explode('.', $url['host']);

            $domain = array_pop($parts);

            if (count($parts) > 0) {
                $domain = array_pop($parts) . '.' . $domain;
            }

            return $this->refererParser->getDataArray($referer, $url['host'], $domain, $currentUrl);
        }
    }

    /**
     * @return array
     */
    private function getDevice()
    {
        try {
            return $this->mobileDetect->detectDevice();
        } catch (\Exception $exeption) {
            return $exeption;
        }
    }

    /**
     * @return mixed
     */
    private function getOperatingSystemFamily()
    {
        try {
            return $this->userAgentParser->operatingSystem;
        } catch (\Exception $exeption) {
            return $exeption;
        }
    }

    /**
     * @return mixed
     */
    private function getOperatingSystemVersion()
    {
        try {
            return $this->userAgentParser->getOperatingSystemVersion();
        } catch (\Exception $exeption) {
            return $exeption;
        }
    }

    public function getCurrentUserAgent()
    {
        return $this->userAgentParser->originalUserAgent;
    }

    /**
     * @param  [type] $request
     * @param  int $ident
     *
     * @return [type]
     */
    public function serverPaginationFilteringFor($request, $ident): LengthAwarePaginator
    {
        $data = $this->allWithBuilder();

        $data = $data->where('banner_id', (int) $ident)
            ->selectRaw('DATE(created_at) as day, count(case when is_click = true then 1 end) as "clicked", count(case when is_click = false then 0 end) as "not_clicked"')
            ->groupBy(\DB::raw('DATE(created_at)'))
            ->orderBy('day', 'desc')
            ->get();

        $banner = $this->banner->getParentAndTitleById($ident);

        return new BackendPaginator(
            $data->forPage(Paginator::resolveCurrentPage(), $request->get('per_page', 10)),
            $data->count(),
            $request->get('per_page', 10),
            Paginator::resolveCurrentPage(),
            [
                'breadcrumbs' => [
                    [
                        'name' => trans('core::breadcrumbs.banners group'),
                        'link' => [
                            'name' => 'api.backend.banners_group.index',
                        ]
                    ],
                    [
                        'name' => $banner->title,
                        'link' => [
                            'name' => 'api.backend.banners_group.show',
                            'params' => [
                                'ident' => $banner->group_id
                            ]
                        ]
                    ],
                    [
                        'name' => trans('core::breadcrumbs.banner statistic'),
                    ]
                ]
            ]
        );
    }

    /**
     * @param  [type] $request
     * @param  int $ident
     *
     * @return [type]
     */
    public function serverPaginationFilteringForAll($request, $ident): LengthAwarePaginator
    {
        $data = $this->allWithBuilder();

        $data = $data->where('banner_id', (int) $ident)->where('is_click', 1)->orderBy('created_at', 'desc')->get();
        $banner = $this->banner->getParentAndTitleById($ident);

        return new BackendPaginator(
            $data->forPage(Paginator::resolveCurrentPage(), $request->get('per_page', 10)),
            $data->count(),
            $request->get('per_page', 10),
            Paginator::resolveCurrentPage(),
            [
                'breadcrumbs' => [
                    [
                        'name' => trans('core::breadcrumbs.banners group'),
                        'link' => [
                            'name' => 'api.backend.banners_group.index',
                        ]
                    ],
                    [
                        'name' => $banner->title,
                        'link' => [
                            'name' => 'api.backend.banners_group.show',
                            'params' => [
                                'ident' => $banner->group_id
                            ]
                        ]
                    ],
                    [
                        'name' => trans('core::breadcrumbs.banner statistic'),
                    ]
                ]
            ]
        );
    }

    public function allWithBuilder()
    {
        return $this->model->newQuery();
    }

    /**
     * {@inheritdoc}
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($model)
    {
        return $model->delete();
    }
}
