<?php

namespace Modules\Banners\Repositories\Eloquent;

use Modules\Banners\Entities\Group;
use Modules\Banners\Repositories\BannerInterface;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentBannerRepository extends EloquentCoreRepository implements BannerInterface
{
    public $model_config = 'modules/Banners/Config/models/banner.json';

    protected $groups;

    /**
    * @param \Modules\Ecommerce\Entities\Banner $model
    */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->groups = new EloquentGroupRepository(new Group);
    }

    /**
     * Flatten array by relation.
     *
     * @param array  $entities
     * @param string $relation
     *
     * @return array
     */
    protected function groups()
    {
        return $this->groups->all()->pluck('title', 'id')->toArray();
    }

    public function getParentAndTitleById($ident)
    {
        return $this->model->where('banners.id', $ident)->listsTranslations('title')->select('title', 'group_id')->first();
    }
}
