<?php

namespace Modules\Banners\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Banners\Repositories\StatisticCrawlerInterface;

class BannersController extends Controller
{
    /**
     * @var StatisticCrawlerInterface
     */
    protected $statisticInterface;

    public function __construct(StatisticCrawlerInterface $statisticInterface)
    {
        $this->statisticInterface = $statisticInterface;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function getStatisticRequest(Request $request)
    {
        if (null !== $request->all() && $request->ajax()) {
            $this->statisticInterface->create($this->makeStatisticData($request));
        }
    }

    /**
     * @return array
     */
    protected function makeStatisticData(Request $request)
    {
        $statisticData = [
            'banner_id' => (int) $request->id ?? 0,
            'is_click' => (int) $request->click ?? 0,
            'ip' => ($request->ip() == '127.0.0.1') ? 'localhost' : $request->ip(),
        ];

        $statisticData = array_merge($statisticData, $this->statisticInterface->getCurrentDeviceProperties());
        $statisticData = array_merge($statisticData, $this->statisticInterface->getCurrentAgentArray());

        // Referer
        // $statisticData = array_merge($statisticData, $this->statisticInterface->getRefererArray($request->headers->get('referer'), url()->current()));

        return $statisticData;
    }
}
