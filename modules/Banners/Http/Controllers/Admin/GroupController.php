<?php

namespace Modules\Banners\Http\Controllers\Admin;

use Modules\Banners\Repositories\GroupInterface;
use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class GroupController extends CoreAdminController
{
    /**
     * @var GroupInterface
     */
    private $group;

    public function __construct(GroupInterface $group)
    {
        $this->group = $group;
    }

    /**
     * Display a listing of the resource.
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     * @return Illuminate\View\View
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Illuminate\View\View
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function destroy($id)
    {
        $this->group->delete($id);

        return redirect()->route('backend.group.index')
            ->withSuccess(trans('Banners deleted'));
    }
}
