<?php

namespace Modules\Banners\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\CoreAdminController;

class BannersController extends CoreAdminController
{
    /**
     * Display a listing of the resource.
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     * @return Illuminate\View\View
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Illuminate\View\View
     */
    public function edit()
    {
        return view('core::router_view');
    }
}
