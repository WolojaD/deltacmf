<?php

namespace Modules\Banners\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Banners\Transformers\StatisticTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Banners\Transformers\FullStatisticTransformer;
use Modules\Banners\Repositories\StatisticCrawlerInterface;

class StatisticController extends CoreApiController
{
    /**
     * @var StatisticCrawlerInterface
     */
    protected $statistics;

    public function __construct(StatisticCrawlerInterface $statistics)
    {
        $this->repository = $statistics;
    }

    /**
     * @param Request $request
     * @param int $ident
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $ident)
    {
        if ($request->exists('table') && $request->table == 'full') {
            return FullStatisticTransformer::collection($this->repository->serverPaginationFilteringForAll($request, $ident));
        }

        return StatisticTransformer::collection($this->repository->serverPaginationFilteringFor($request, $ident));
    }

    /**
     * @param  int $ident
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($ident)
    {
        $statistics = $this->repository->find($ident);

        $this->repository->destroy($statistics);

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banners deleted'),
        ]);
    }
}
