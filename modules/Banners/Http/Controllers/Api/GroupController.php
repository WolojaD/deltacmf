<?php

namespace Modules\Banners\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Banners\Repositories\GroupInterface;
use Modules\Banners\Repositories\BannerInterface;
use Modules\Banners\Transformers\GroupTransformer;
use Modules\Banners\Transformers\BannerTransformer;
use Modules\Banners\Http\Requests\CreateGroupRequest;
use Modules\Banners\Http\Requests\UpdateGroupRequest;
use Modules\Banners\Transformers\FullGroupTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class GroupController extends CoreApiController
{
    /**
     * @var GroupInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    /**
     * @var BannerInterface
     */
    protected $banners;

    public function __construct(GroupInterface $group, BannerInterface $banners)
    {
        $this->repository = $group;
        $this->modelInfo = $this->repository->getJsonList();
        $this->banners = $banners;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = false)
    {
        if ($id) {
            $request->merge(['where' => ['group_id' => $id]]);

            return BannerTransformer::collection($this->banners->serverPaginationFilteringFor($request));
        }

        return GroupTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullGroupTransformer
     */
    public function find($id)
    {
        $group = $this->repository->find($id);

        return new FullGroupTransformer($group);
    }

    /**
     * @param int $id
     * @param UpdateGroupRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateGroupRequest $request)
    {
        $group = $this->repository->find($id);
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $this->repository->update($group, $data);

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banners updated'),
            'id' => $group->id
        ]);
    }

    /**
     * @param CreateGroupRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroupRequest $request)
    {
        $data = $request->except('ident');

        foreach ($data as $key => $item) {
            if (is_array($item) && !count($item)) {
                unset($data[$key]);
            }
        }

        $group = $this->repository->create($data);

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banners created'),
            'id' => $group->id
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = $this->repository->find($id);

        $this->repository->destroy($group);

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banners deleted'),
        ]);
    }

    /**
     * @param  Request $request
     * @param  int $id
     *
     * @return [type]
     */
    public function findTable(Request $request, $id)
    {
        if (strpos($request->route()->getName(), 'find-banners')) {
            $request->merge(['where' => ['group_id' => $id]]);

            return BannerTransformer::collection($this->banners->serverPaginationFilteringFor($request));
        }

        return parent::findTable($request, $id);
    }
}
