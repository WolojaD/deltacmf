<?php

namespace Modules\Banners\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Banners\Repositories\GroupInterface;
use Modules\Banners\Repositories\BannerInterface;
use Modules\Banners\Transformers\BannerTransformer;
use Modules\Banners\Http\Requests\CreateBannerRequest;
use Modules\Banners\Http\Requests\UpdateBannerRequest;
use Modules\Banners\Transformers\FullBannerTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class BannerController extends CoreApiController
{
    /**
     * @var BannerInterface
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    public function __construct(BannerInterface $banner, GroupInterface $group)
    {
        $this->repository = $banner;
        $this->modelInfo = $this->repository->getJsonList();
        $this->group = $group;
    }

    /**
     * @param Request $request
     * @param boolean|int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return BannerTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return BannerTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return BannerTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    /**
     * @param int $id
     *
     * @return FullBannerTransformer
     */
    public function find($id)
    {
        $banner = $this->repository->find($id);

        return new FullBannerTransformer($banner);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function findNew(Request $request)
    {
        $carry = [
            [
                'name' => trans('core::breadcrumbs.banners create')
            ]
        ];

        return [
            'fields' => $this->repository->getFieldsArray(),
            'breadcrumbs' => $this->group->find($request->ident)->getBackendBreadcrumbs($carry)
        ];
    }

    /**
     * @param int $id
     * @param UpdateBannerRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateBannerRequest $request)
    {
        $banner = $this->repository->find($id);

        $this->repository->update($banner, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banner updated'),
            'goto' => ['name' => 'api.backend.banners_group.show', 'params' => ['ident' => $banner->group_id]],
            'id' => $banner->id
        ]);
    }

    /**
     * @param CreateBannerRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBannerRequest $request)
    {
        $banner = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banner created'),
            'goto' => ['name' => 'api.backend.banners_group.show', 'params' => ['ident' => $banner->group_id]],
            'id' => $banner->id
        ]);
    }

    /**
     * @param  Request $request
     * @param  int $id
     *
     * @return [type]
     */
    public function findTable(Request $request, $id)
    {
        if (strpos($request->route()->getName(), 'find-banner_statistics')) {
            $request->merge(['where' => ['banner_id' => $id]]);

            return BannerTransformer::collection($this->banners->serverPaginationFilteringFor($request));
        }

        return parent::findTable($request, $id);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = $this->repository->find($id);

        $this->repository->destroy($banner);

        return response()->json([
            'errors' => false,
            'message' => trans('banners::messages.api.banner deleted'),
        ]);
    }
}
