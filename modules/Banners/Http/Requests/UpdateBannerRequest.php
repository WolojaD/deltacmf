<?php

namespace Modules\Banners\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateBannerRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Banners/Config/models/banner.json');
    }

    public function authorize()
    {
        return true;
    }
}
