<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Banners');

Route::group(['prefix' => '/banners/banner/statistics'], function () {
    Route::get('{ident}', ['as' => 'api.backend.banners_statistics', 'uses' => 'StatisticController@index', 'middleware' => 'token-can:banners.banner.statistic.index']);

    Route::delete('{ident}', ['as' => 'api.backend.banners_group.show.destroy', 'uses' => 'StatisticController@destroy', 'middleware' => 'token-can:banners.banner.statistic.index']);
});
