import helpers from '@services/helpers'

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/banners/group',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.banners_group',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Banners',
                    breadcrumb: [
                        { name: 'Banners' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.banners_group.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Banners Create',
                    breadcrumb: [
                        { name: 'Banners', link: 'api.backend.banners_group' },
                        { name: 'Banners Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.banners_group.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Banners Edit',
                    breadcrumb: [
                        { name: 'Banners', link: 'api.backend.banners_group' },
                        { name: 'Banners Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.banners_group.show',
                component: require('@/views/crud/index').default,
                meta: {
                    pageTitle: 'Banners',
                    breadcrumb: [
                        { name: 'Banners', link: 'api.backend.banners_group' },
                        { name: 'Banner' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/banners/banner',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.banners_banner.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Banner Create',
                    breadcrumb: [
                        { name: 'Banners', link: 'api.backend.banners_group' },
                        { name: 'Banner Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.banners_banner.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Banner Edit',
                    breadcrumb: [
                        { name: 'Banners', link: 'api.backend.banners_group' },
                        { name: 'Banner Edit' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.banners_banner.show',
                component: require('@/views/banners/statistics').default,
                meta: {
                    pageTitle: 'Banner Statistic',
                    breadcrumb: [
                        { name: 'Banners', link: 'api.backend.banners_group' },
                        { name: 'Banner Statistic' }
                    ]
                }
            }
        ]
    }
]
