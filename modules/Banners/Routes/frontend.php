<?php

/*
|--------------------------------------------------------------------------
| FrontEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register FrontEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "frontend" middleware group.
|
*/

Route::get('/banners-statistics-request', ['middleware' => config('application.banners.config.middleware'), 'as' => 'getStatisticRequest', 'uses' => 'BannersController@getStatisticRequest']);
