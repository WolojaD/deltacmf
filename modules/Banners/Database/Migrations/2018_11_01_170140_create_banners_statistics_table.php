<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('banner_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_id')->unsigned()->index();
            // IP
            $table->string('ip')->index();
            // Device
            $table->string('device_kind', 16)->index();
            $table->string('device_model', 64)->index();
            $table->string('device_platform', 64)->index();
            $table->string('device_platform_version', 16)->index();
            $table->boolean('device_is_mobile');
            // Agent
            $table->string('agent_name', 255)->nullable();
            $table->string('agent_browser')->index();
            $table->string('agent_browser_version');
            // Referer
            $table->string('referer_domain', 500)->nullable();
            $table->string('referer_url', 255)->nullable();
            $table->string('referer_host', 255)->nullable();
            $table->string('referer_medium', 255)->nullable();
            $table->string('referer_source', 255)->nullable();
            $table->string('referer_search_terms_hash', 255)->nullable();
            // Robot
            $table->boolean('is_robot');
            // Click
            $table->boolean('is_click');
            // Foreign
            $table->foreign('banner_id')->references('id')->on('banners')->onDelete('cascade');
            // Timestamps
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('banner_statistics');
    }
}
