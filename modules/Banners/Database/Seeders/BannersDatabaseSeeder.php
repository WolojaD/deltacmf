<?php

namespace Modules\Banners\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Banners\Entities\Group;
use Modules\Banners\Entities\Banner;
use Modules\Banners\Entities\BannerStatistic;

class BannersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        if (!app()->environment('production')) {
            factory(Group::class, 3)->create();
            factory(Banner::class, 15)->create();
            factory(BannerStatistic::class, 1000)->create();
        }
    }
}
