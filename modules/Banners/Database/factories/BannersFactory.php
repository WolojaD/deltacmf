<?php

use Faker\Generator as Faker;
use Modules\Banners\Entities\Group;
use Modules\Banners\Entities\Banner;
use Modules\Banners\Entities\BannerStatistic;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Group::class, function (Faker $faker) {
    $data = [
        'title' => $faker->name,
        'short_code' => $faker->lexify('?????????'),
        'status' => $faker->numberBetween(0, 1),
        'active' => $faker->numberBetween(1, 3),
        'comment' => $faker->numberBetween(0, 1) == 1 ? $faker->sentence : null,
    ];

    return $data;
});

$factory->define(Banner::class, function (Faker $faker) {
    $data = [
        'group_id' => $faker->numberBetween(1, 3),
        'status' => $faker->numberBetween(0, 1),
        'url' => $faker->url,
        'blank' => $faker->numberBetween(0, 1),
        'weight' => $faker->numberBetween(0, 100),
        'title_color' => $faker->hexcolor(),
        'description_color' => $faker->hexcolor(),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
            'description' => $faker->sentence,
            'translatable_desktop_image_alt' => $faker->name,
            'button_title' => $faker->name,
        ];
    }

    return $data;
});

$factory->define(BannerStatistic::class, function (Faker $faker) {
    $data = [
        'banner_id' => $faker->numberBetween(1, 15),
        'ip' => $faker->ipv4(),
        'device_kind' => $faker->numberBetween(1, 2) == 1 ? 'Computer' : 'Phone',
        'device_model' => $faker->numberBetween(1, 2) == 1 ? 'Macintosh' : 'Windows',
        'device_platform' => $faker->numberBetween(1, 2) == 1 ? 'OS X' : 'Windows NT',
        'device_platform_version' => $faker->numberBetween(1, 2) == 1 ? '10_14_1' : '10.1',
        'device_is_mobile' => $faker->numberBetween(0, 1),
        'agent_name' => $faker->numberBetween(1, 2) == 1 ? $faker->chrome() : $faker->safari(),
        'agent_browser' => $faker->numberBetween(1, 2) == 1 ? 'Chrome' : 'Safari',
        'agent_browser_version' => $faker->ean8(),
        'referer_domain' => 'delta.test',
        'referer_url' => $faker->url(),
        'referer_host' => 'delta.test',
        'is_robot' => $faker->numberBetween(0, 1),
        'is_click' => $faker->numberBetween(0, 1),
        'created_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now', $timezone = 'Europe/Kiev'),
    ];

    return $data;
});
