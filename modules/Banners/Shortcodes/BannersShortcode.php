<?php

namespace Modules\Banners\Shortcodes;

use Modules\Banners\Entities\Group;
use Illuminate\Support\Facades\View;

class BannersShortcode
{
    /**
     * @param string $shortcode
     * @param string $content
     * @param string $compiler
     * @param string $name
     * @param string $viewData
     *
     * @return type
     */
    public function register($shortcode, $content, $compiler, $name, $viewData)
    {
        // Get Group with active banners by given short code
        $banner_group = Group::when(!is_numeric($shortcode->code), function ($q) use ($shortcode) {
            $q->where('short_code', $shortcode->code);
        }, function ($q) use ($shortcode) {
            $q->where('id', $shortcode->code);
        })->with('active_banners')->first();

        if (null === $banner_group) {
            return '';
        }

        // Check if number of acvite banners in group data lower than active banners in group
        if ($banner_group->active < $banner_group->active_banners->count()) {
            $interval = 100000;
            $range = 0;

            // Create range array by formula for each banner in group
            foreach ($banner_group->active_banners as $key => $banner) {
                $range += intval($banner->weight) / intval($banner_group->active_banners->sum('weight')) * $interval;
                $range_array[$banner->id] = intval($range);
            }

            $result = [];

            // Find first banner in range array that more than rand and set it to the new result array and unset in from range array
            while (count($result) < $banner_group->active) {
                $banner_id = $this->array_find($range_array, function ($value) {
                    return $value > rand(0, 100000);
                });

                unset($range_array[$banner_id]);

                $result[] = $banner_id;
            }

            // Check if View exist by group template and render it
            if (View::exists('banners::' . $banner_group->banners_template)) {
                return !empty($result) ? view('banners::' . $banner_group->banners_template)->with('banners', $banner_group->active_banners->whereIn('id', $result)) : '';
            } else {
                return '';
            }
        }

        if ($banner_group && View::exists('banners::' . $banner_group->banners_template)) {
            return $banner_group->active_banners->count() ? view('banners::' . $banner_group->banners_template)->with('banners', $banner_group->active_banners) : '';
        } else {
            return '';
        }
    }

    /**
     * @param array $array
     * @param callable $function
     *
     * @return type
     */
    private function array_find(array $array, callable $function)
    {
        foreach ($array as $key => $value) {
            if ($function($value, $key, $array)) {
                return $key;
            }
        }

        return false;
    }
}
