<?php

namespace Modules\Banners\Support;

use Snowplow\RefererParser\Parser;

class RefererParser
{
    /**
     * Referer parser instance.
     *
     * @var Parser
     */
    private $parser;

    /**
     * Referer parser instance.
     *
     * @var \Snowplow\RefererParser\Referer
     */
    private $referer;

    /**
     * Create a referer parser instance.
     *
     * @return mixed
     */
    public function __construct()
    {
        $this->parser = new Parser();
    }

    /**
     * @param string $refererUrl
     * @param string $host
     * @param string $domain
     * @param string $currentUrl
     *
     * @return array
     */
    public function getDataArray($refererUrl, $host, $domain, $currentUrl)
    {
        $attributes = [
            'referer_url' => $refererUrl,
            'referer_host' => $host,
            'referer_domain' => $domain,
            'referer_medium' => null,
            'referer_source' => null,
            'referer_search_terms_hash' => null,
        ];

        $parsed = $this->parse($refererUrl, $currentUrl);

        if ($parsed->isKnown()) {
            $attributes['referer_medium'] = $parsed->getMedium();
            $attributes['referer_source'] = $parsed->getSource();
            $attributes['referer_search_terms_hash'] = sha1($parsed->getSearchTerm());
        }

        return $attributes;
    }

    /**
     * Parse a referer.
     *
     * @return RefererParser
     */
    private function parse($refererUrl, $pageUrl)
    {
        $this->setReferer($this->parser->parse($refererUrl, $pageUrl));

        return $this;
    }

    /**
     * Get the search medium.
     *
     * @return string|null
     */
    public function getMedium()
    {
        if ($this->isKnown()) {
            return $this->referer->getMedium();
        }
    }

    /**
     * Get the search source.
     *
     * @return string|null
     */
    public function getSource()
    {
        if ($this->isKnown()) {
            return $this->referer->getSource();
        }
    }

    /**
     * Get the search term.
     *
     * @return string|null
     */
    public function getSearchTerm()
    {
        if ($this->isKnown()) {
            return $this->referer->getSearchTerm();
        }
    }

    /**
     * Check if the referer is knwon.
     *
     * @return bool
     */
    public function isKnown()
    {
        return $this->referer->isKnown();
    }

    /**
     * Set the referer.
     *
     * @param \Snowplow\RefererParser\Referer $referer
     *
     * @return RefererParser
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }
}
