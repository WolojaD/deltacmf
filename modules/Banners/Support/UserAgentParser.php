<?php

namespace Modules\Banners\Support;

use Jenssegers\Agent\Agent;

class UserAgentParser
{
    public $agent;

    public $operatingSystem;

    public $userAgent;

    public $originalUserAgent;

    public function __construct()
    {
        $this->agent = new Agent();
        $this->operatingSystem = $this->agent->platform();
        $this->userAgent = $this->agent->browser();
        $this->originalUserAgent = $this->agent->getUserAgent();
    }

    public function getOperatingSystemVersion()
    {
        return $this->agent->version($this->operatingSystem);
    }

    public function getUserAgentVersion()
    {
        return $this->agent->version($this->userAgent);
    }
}
