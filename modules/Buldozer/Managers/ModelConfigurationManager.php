<?php

namespace Modules\Buldozer\Managers;

use Illuminate\Support\Manager;
use Modules\Core\Eloquent\Model;
use Illuminate\Foundation\Application;
use Modules\Buldozer\Configurators\Implementors\JsonConfigurator;
use Modules\Buldozer\Configurators\Implementors\DatabaseConfigurator;

class ModelConfigurationManager extends Manager implements ModelConfigurationManagerInterface
{
    protected $path;

    public function __construct(Application $app, string $path = '')
    {
        parent::__construct($app);

        $this->path = $path;
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return pathinfo($this->path)['extension'] ?? 'database';
    }

    public function createJsonDriver()
    {
        return new JsonConfigurator($this->path);
    }

    public function createArrayDriver()
    {
        return new ArrayConfigurator($this->path);
    }

    public function createDatabaseDriver()
    {
        return new DatabaseConfigurator();
    }

    public function getColumns(Model $model, $template = 'default')
    {
        return $this->driver()->getColumns($model, $template);
    }

    public function rules()
    {
        return $this->driver()->rules();
    }

    public function translationRules()
    {
        return $this->driver()->translationRules();
    }

    public function paginateOptionsGenerate($options, $model, $template, $current_parent_id)
    {
        return $this->driver()->paginateOptionsGenerate($options, $model, $template, $current_parent_id);
    }
}
