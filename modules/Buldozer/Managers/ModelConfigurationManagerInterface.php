<?php

namespace Modules\Buldozer\Managers;

use Modules\Core\Eloquent\Model;

interface ModelConfigurationManagerInterface
{
    public function getDefaultDriver();

    public function createJsonDriver();

    public function createArrayDriver();

    public function getColumns(Model $model, $template = 'default');

    public function rules();

    public function translationRules();

    public function paginateOptionsGenerate($options, $model, $template, $current_parent_id);
}
