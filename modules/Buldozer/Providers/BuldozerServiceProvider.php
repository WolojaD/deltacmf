<?php

namespace Modules\Buldozer\Providers;

use Illuminate\Foundation\Application;
use Modules\Core\Providers\ServiceProvider;
use Modules\Buldozer\Managers\ModelConfigurationManager;
use Modules\Buldozer\Managers\ModelConfigurationManagerInterface;

class BuldozerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig('buldozer', 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->bind(ModelConfigurationManagerInterface::class, function ($app, $params) {
            return new ModelConfigurationManager($app, ...$params);
        });

        // TODO If you want custom name
        // $this->app->alias(ModelConfigurationManagerInterface::class, 'some-shit');
    }
}
