<?php

namespace Modules\Buldozer\Configurators\Implementors;

use Modules\Core\Eloquent\Model;
use Modules\Buldozer\Configurators\ConfiguratorInterface;

class JsonConfigurator implements ConfiguratorInterface
{
    protected $path;
    protected $list;

    public function __construct($path)
    {
        if (is_file(base_path($path))) {
            $this->list = json_decode(file_get_contents(base_path($path)));
        } elseif (is_file($path)) {
            $this->list = json_decode(file_get_contents($path));
        }
    }

    public function getList()
    {
        return $this->list;
    }

    /**
     * model properies generator
     *
     * @return array
     */
    public function modelConstruct()
    {
        $fields = collect($this->list->templates->default ?? [])->filter(function ($field) {
            return $field->fillable ?? true;
        });

        $result = [
            'translatedAttributes' => [],
            'searchable' => [],
            'appends' => [],
            'to_save' => [],
            'with' => [],
        ];

        foreach ($fields as $field_name => $field) {
            if ($field->is_translated ?? false && ($this->list->need_translations ?? true)) {
                $result['translatedAttributes'][] = $field_name;
            }

            if ($field->searchable ?? false) {
                $result['searchable'][] = $field_name;
            }

            if ($field->appends ?? false) {
                $result['appends'][] = $field->name ?? false;
            }

            if ($field->fillable ?? true) {
                $result['to_save'][] = $field_name ?? false;
            }

            // TODO NEED TO CHECK IF SLOW WORK
            // if (preg_match('/[a-z]+_id$/', $field_name) && $this->onBackend() && !($field->no_relation ?? false)) {
            //     $this->with[] = str_replace('_id', '', $field_name);
            // }
        }

        return $result;
    }

    /**
     * -----------------------------
     * form fields methods
     * -----------------------------
     */
    public function getFields($repository)
    {
        $result = [];
        $fields = $this->list->templates->default;

        $general_tab = '{"en":"General","ru":"Общие"}';
        $key = $general_tab;

        if (($fields->standard->nested ?? false) && !($fields->parent_id ?? false)) {
            $label = is_bool($fields->standard->nested) ? ['en' => 'Parent', 'ru' => 'Принадлежит'] : $fields->standard->nested;

            $result[$key]['parent_id'] = [
                'field_type' => 'select-tree',
                'parameters' => [
                    'field_label' => $label,
                    'required' => false,
                    'array_values' => $repository->getArrayValues('tableForFlattenByRelation'),
                ],
            ];
        }

        if (($fields->standard->slugable ?? false) && !($fields->slug ?? false)) {
            $result[$key]['slug'] = [
                'field_type' => 'slug',
                'parameters' => [
                    'field_label' => ['en' => 'Slug', 'ru' => 'Уникальное имя'],
                    'required' => true,
                ],
            ];
        }

        if (($fields->standard->desktop_mobile_images ?? false) && !($fields->desktop_image ?? false) && !($fields->mobile_images ?? false)) {
            $result[$key]['desktop_image'] = [
                'field_type' => 'upload-image',
                'parameters' => [
                    'field_label' => ['en' => 'Desktop image', 'ru' => 'Десктопное изображение'],
                ],
            ];

            $result[$key]['mobile_image'] = [
                'field_type' => 'upload-image',
                'parameters' => [
                    'field_label' => ['en' => 'Mobile image', 'ru' => 'Мобильное изображение'],
                ],
            ];
        }

        $role = auth()->user() ? auth()->user()->roles()->first()->slug : '';

        foreach ($fields as $field_name => $field) {
            $roles = $field->field_roles ?? false;
            $canSee = $roles ? (is_array($roles) ? in_array($role, $roles) : $role == $roles) : true;

            if (strpos(request()->path(), 'find-edit') && ($field->field_except ?? false) && in_array(request()->ident, $field->field_except)) {
                continue;
            }

            if (strpos(request()->path(), 'find-edit') && !($field->edit ?? true)) {
                continue;
            }

            if ($field_name == 'standard' || !($field->type ?? false) || !$canSee) {
                continue;
            }

            if ($field->type === 'tab') {
                $key = json_encode($field->label);

                continue;
            }

            if (($field->visible ?? false) && !$repository->{$field->visible}()) {
                continue;
            }

            $field->table_field = $field_name;
            $field_data = [
                'field_type' => $field->type,
                'parameters' => [
                    'field_label' => $field->label ?? '',
                    'field_attributes' => $field->attributes ?? null,
                    'field_placeholder' => $field->placeholder ?? '',
                    'hint' => $field->hint ?? null,
                    'required' => $field->required ?? false,
                    'validation' => $field->validation ?? null,
                    'array_values' => $repository->getArrayValues($field),
                    'default' => $field->default ?? null,
                    'mainRelation' => $field->mainRelation ?? null,
                    'from_ident' => $field->from_ident ?? null,
                    'settings' => $field->settings ?? null
                ],
            ];

            if ($field->is_translated ?? false) {
                $result[$key]['translatable'][$field->table_field] = $field_data;
            } else {
                $result[$key][$field->table_field] = $field_data;
            }
        }

        if (($fields->standard->status ?? false) && !($fields->status ?? false)) {
            $result[$general_tab]['status'] = [
                'field_type' => 'select',
                'parameters' => [
                    'field_label' => ['en' => 'Status', 'ru' => 'Статус'],
                    'array_values' => $repository->getArrayValues('status'),
                    'default' => 1
                ],
            ];
        }

        return $result;
    }

    /**
     * -----------------------------
     *  table columns methods
     * -----------------------------
     **/

    /**
    * @param $parent
    * @param $options
    * @param string $template
    *
    * @return array
    */
    public function paginateOptionsGenerate($options, $model, $template = 'default', $current_parent_id = false)
    {
        $templateModel = $this->list->templates->$template ?? false;

        if ($templateModel) {
            $options['fields'] = $this->getColumns($model, $template);
            $options['sortOrder'] = ['current' => $this->sort ?? false, 'default' => ($templateModel->standard->sortOrder ?? false)];
            $options['backend_path_name'] = $this->list->backend_path_name ?? false;
            $options['has_tables'] = $templateModel->standard->has_tables ?? [];
            $options['nested'] = $templateModel->standard->nested ?? false;
            $options['buttons'] = $templateModel->standard->buttons ?? false;
            $options['no_create'] = $templateModel->standard->no_create ?? false;
            $title_field = ($templateModel->standard->nested ?? false) ? ($templateModel->standard->title_field ?? false) : false;
        }

        if ($current_parent_id) {
            $title_field = isset($title_field) && $title_field ? $title_field : 'title';
            $options['title'] = $model->find($current_parent_id)->{$title_field};
        }

        if ((request()->ident ?? false) && is_numeric(request()->ident) && request()->filled('where')) {
            $relation = str_replace('_id', '', key(request()->where));

            if (method_exists($model, $relation)) {
                $relation = $model->$relation()->getRelated()->find(request()->ident);

                if (method_exists($relation, 'getBackendBreadcrumbs')) {
                    $options['breadcrumbs'] = $relation->getBackendBreadcrumbs();
                }
            } else {
                $options['breadcrumbs'] = $model->getBackendBreadcrumbs();
            }
        } elseif (method_exists($model, 'getBackendBreadcrumbs')) {
            $options['breadcrumbs'] = $model->getBackendBreadcrumbs();
        }

        return $options;
    }

    /**
     * @param string $template
     * @return \Illuminate\Support\Collection
     */
    public function getColumns(Model $model, $template = 'default')
    {
        $role = auth()->user()->roles->first()->slug;

        return $this->getConfigColumnns($template)->mapWithKeys(function ($field, $id) use ($role, $model) {
            $field = (object) $field;
            if (!$this->canSee($field, $role) && $this->doesNotHaveCondition($field, $model)) {
                return [];
            }

            return [
                $field->name ?? $id => [
                    'originName' => $field->originName ?? '',
                    'title' => isset($field->label) && is_array($field->label) ? $field->label[locale()] : ($field->label->{locale()} ?? ''),
                    'sortable' => ($field->sortable ?? false) ? ($field->sortField ?? $field->name) : false,
                    'options' => $field->options ?? false,
                    'relationMethod' => $field->relationMethod ?? false,
                    'column_type' => $field->column_type ?? false,
                    'column_class' => $field->column_class ?? false,
                    'column_attributes' => $field->column_attributes ?? false
                ]
            ];
        })->filter();
    }

    private function getConfigColumnns($template = 'default')
    {
        $template = $this->getList()->templates->{$template};

        $table = [];

        if ($template->standard->multicheck ?? false) {
            $table[] = $this->getDefault('checkbox');
        }

        if ($template->standard->status ?? false) {
            $table[] = $this->getDefault('status');
        }

        $table = $this->tableListGenerate($template, $table);

        if (($template->standard->order ?? false) && !($template->order ?? false)) {
            $table[] = $this->getDefault('order');
        }

        if ($template->standard->actions ?? false) {
            $table[] = $this->getDefault('actions');
            $table[count($table) - 1]['options'] = [
                'buttonParams' => $template->standard->actions
            ];
        }

        return collect($table);
    }

    private function getDefault($field)
    {
        return [
            'checkbox' => [
                'name' => '__checkbox',
            ],
            'status' => [
                'name' => '__status',
                'sortField' => 'status',
                'label' => ['en' => 'Status', 'ru' => 'Статус'],
                'sortable' => 'status'
            ],
            'order' => [
                'name' => '__order',
                'label' => ['en' => 'Order', 'ru' => 'Порядок'],
            ],
            'actions' => [
                'name' => '__actions',
                'label' => ['en' => 'Actions', 'ru' => 'Действия'],
                'options' => [
                    'buttonParams' => [
                        'edit' => true,
                        'delete' => true
                    ]
                ],
            ]
        ][$field] ?? false;
    }

    /**
     * @param $custom
     * @param $table
     *
     * @return array
     */
    private function tableListGenerate($custom, $table): array
    {
        foreach ($custom as $field_name => $field) {
            if ($field_name == 'standrad' || !isset($custom->{$field_name}->column_type) || !$custom->{$field_name}->column_type) {
                continue;
            }

            $objField = $custom->{$field_name};

            $objField->originName = $field_name;
            $objField->name = $objField->name ?? $field_name;

            preg_match('/([a-zA-Z]+)_id$/', $field_name, $preg_result);
            $relation = $preg_result[1] ?? '';
            $objField->relationMethod = $objField->relationMethod ?? $relation;

            if ($objField->sortable ?? false) {
                $objField->sortField = ($objField->is_translated ?? false) ? 'translation.' . $objField->name : $objField->name;
            }

            $table[] = $objField;
        }

        return $table;
    }

    /**
     * @param $field
     * @param $role
     * @return bool
     */
    private function canSee($field, $role): bool
    {
        $roles = $field->column_roles ?? false;

        return $roles ? (is_array($roles) ? in_array($role, $roles) : $role == $roles) : true;
    }

    /**
     * @param $field
     * @param Model $model
     * @return bool
     */
    private function doesNotHaveCondition($field, Model $model): bool
    {
        return !isset($field->column_conditional) || (method_exists($model, $field->column_conditional) && !$model->{$field->column_conditional}());
    }

    /**
     * ------------------------------
     * for validation request
     * ------------------------------
     */
    public function rules()
    {
        $default_template = $this->list->templates->default;

        $result = collect($default_template)
        ->filter(function ($field, $id) {
            return ($field->fillable ?? true) && $id !== 'standard' && !($field->is_translated ?? false);
        })
        ->map(function ($field, $field_name) {
            $result = '';

            if ($field->required ?? false) {
                $result .= 'required';
            }

            if (isset($field->field_type) ? ($field->field_type == 'string' && ($field->type ?? '') !== 'upload-image-in-base64' && ($field->type ?? '') !== 'upload-image') : false) {
                $result .= $result ? '|' : '';
                $result .= 'max:' . ($field->field_symbols_count ?? 191);
            }

            if ($field->unique ?? false) {
                $result .= $result ? '|' : '';
                $result .= 'unique:' . $this->list->table_name . ',' . $field_name;
                $result .= request()->get('id', false) ? ',' . request()->id : '';
            }

            if (($field->validation ?? '') && request()->filled($field_name)) {
                $result .= $result ? '|' : '';
                $result .= $field->validation;
            }

            return $result;
        })
        ->filter(function ($field) {
            return $field;
        })
        ->toArray();

        if ($default_template->standard->slugable ?? false) {
            $add_id = request()->get('id', false) ? ',' . request()->id : '';
            $result['slug'] = $result['slug'] ?? 'required|min:1|max:191|unique:' . $this->list->table_name . ',slug' . $add_id . '|regex:/^[-a-z0-9]+$/';
        }

        if ($default_template->standard->status ?? false) {
            $result['status'] = 'numeric';
        }

        return $result;
    }

    public function translationRules()
    {
        return collect($this->list->templates->default)
            ->filter(function ($field, $id) {
                return ($field->fillable ?? true) && $id !== 'standard' && ($field->is_translated ?? false);
            })
            ->map(function ($field) {
                $result = '';

                if ($field->required ?? false) {
                    $result .= 'required';
                }

                if ($field->field_type == 'string' && $field->type !== 'upload-image-in-base64') {
                    $result .= $result ? '|' : '';
                    $result .= 'max:' . ($field->field_symbols_count ?? 191);
                }

                if ($field->unique ?? false) {
                    $result .= $result ? '|' : '';
                    $result .= 'unique:' . $field->table_name . ',' . $field_name;
                    $result .= isset($this->id) ? ',' . $this->id : '';
                }

                if ($field->validation ?? '') {
                    $result .= $result ? '|' : '';
                    $result .= $field->validation;
                }

                return $result;
            })
            ->filter(function ($field) {
                return $field;
            })
            ->toArray();
    }
}
