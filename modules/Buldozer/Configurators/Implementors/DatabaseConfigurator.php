<?php

namespace Modules\Buldozer\Configurators\Implementors;

use Modules\Core\Eloquent\Model;
use Modules\FieldGenerator\Entities\Field;
use Modules\Buldozer\Configurators\ConfiguratorInterface;

class DatabaseConfigurator implements ConfiguratorInterface
{
    protected $list;

    public function getList()
    {
        return [];
    }

    /**
     * model properies generator
     *
     * @return array
     */
    public function modelConstruct()
    {
        return [
            'translatedAttributes' => [],
            'searchable' => [],
            'appends' => [],
            'to_save' => [],
            'with' => [],
        ];
    }

    /**
     * -----------------------------
     * form fields methods
     * -----------------------------
     */
    public function getFields($repository)
    {
        $result = [];
        $fields = Field::where('table_name', $repository->getTable())
            ->where('is_published', true)
            ->where('type', '!=', '_tab')
            ->orderBy('order', 'asc')
            ->get();

        foreach ($fields as $field) {
            $key = !$field->tab ? '{"en":"General","ru":"Общие"}' : json_encode($field->tab);

            $field_data = [
                'field_type' => $field->type,
                'parameters' => [
                    'field_label' => $field->label ?? '',
                    'model' => str_singular($field->table_name ?? ''),
                    'field_placeholder' => $field->placeholder ?? '',
                    'hint' => $field->hint ?? '',
                    'required' => $field->required ?? false,
                    'validation' => $field->validation ?? '',
                    'array_values' => $repository->getArrayValues($field),
                    'settings' => $field->settings ?? null,
                ],
            ];

            if ('select' == $field->type && 'status' == $field->table_field) {
                $field_data['parameters']['default'] = 1;
            }

            if ($field->is_translated) {
                $result[$key]['translatable'][$field->table_field] = $field_data;
            } else {
                $result[$key][$field->table_field] = $field_data;
            }
        }

        return $result;
    }

    /**
     * -----------------------------
     *  table columns methods
     * -----------------------------
     **/

    /**
         * @param $parent
         * @param $options
         * @param string $template
         *
         * @return array
         */
    public function paginateOptionsGenerate($options, $model, $template = 'default', $current_parent_id = false)
    {
        return $options;
    }

    /**
     * @param string $template
     * @return \Illuminate\Support\Collection
     */
    public function getColumns(Model $model, $template = 'default')
    {
        return [];
    }

    /**
     * ------------------------------
     * for validation request
     * ------------------------------
     */
    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [];
    }
}
