<?php

namespace Modules\Buldozer\Configurators;

use Modules\Core\Eloquent\Model;

interface ConfiguratorInterface
{
    public function getList();

    public function modelConstruct();

    public function getColumns(Model $model, $template = 'default');

    public function rules();

    public function translationRules();
}
