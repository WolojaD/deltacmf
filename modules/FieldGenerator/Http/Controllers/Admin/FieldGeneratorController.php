<?php

namespace Modules\FieldGenerator\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\CoreAdminController;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;

class FieldGeneratorController extends CoreAdminController
{
    /**
     * @var FieldGeneratorRepository
     */
    private $fieldgenerator;

    public function __construct(FieldGeneratorRepository $fieldgenerator)
    {
        $this->fieldgenerator = $fieldgenerator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('core::router_view');
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('core::router_view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('core::router_view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->fieldgenerator->delete($id);

        return redirect()->route('backend.fieldgenerator.index')
            ->withSuccess(trans('FieldGenerator deleted'));
    }
}
