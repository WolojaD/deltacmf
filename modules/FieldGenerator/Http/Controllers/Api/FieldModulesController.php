<?php

namespace Modules\FieldGenerator\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;

class FieldModulesController extends CoreApiController
{
    /**
     * @var FieldGeneratorRepository
     */
    protected $repository;

    /**
     * @var Status
     */
    protected $status;

    public function __construct(FieldGeneratorRepository $fieldgenerator)
    {
        $this->repository = $fieldgenerator;
    }

    public function index()
    {
        return $this->repository->getModules();
    }

    public function show(Request $request, $id)
    {
        return $this->repository->getModels($id);
    }

    public function toggle(Request $request)
    {
        return $this->repository->toggleModule($request->module);
    }
}
