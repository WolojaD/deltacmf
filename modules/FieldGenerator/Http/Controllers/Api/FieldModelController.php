<?php

namespace Modules\FieldGenerator\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;

class FieldModelController extends CoreApiController
{
    /**
     * @var FieldGeneratorRepository
     */
    protected $repository;

    /**
     * @var Status
     */
    protected $status;

    public function __construct(FieldGeneratorRepository $fieldgenerator)
    {
        $this->repository = $fieldgenerator;
    }

    public function show(Request $request, $parent, $index)
    {
        return $this->repository->getModel($parent, $index);
    }

    public function update(Request $request, $parent, $index)
    {
        return $this->repository->setModel($request->modelConfig, $parent, $index);
    }
}
