<?php

namespace Modules\FieldGenerator\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\FieldGenerator\Entities\Field;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;
use Modules\FieldGenerator\Transformers\FieldGeneratorTransformer;
use Modules\FieldGenerator\Http\Requests\CreateFieldGeneratorRequest;
use Modules\FieldGenerator\Http\Requests\UpdateFieldGeneratorRequest;
use Modules\FieldGenerator\Transformers\FullFieldGeneratorTransformer;

class FieldGeneratorController extends Controller
{
    /**
     * @var FieldGeneratorRepository
     */
    protected $repository;

    /**
     * @var Status
     */
    protected $status;

    public function __construct(FieldGeneratorRepository $fieldgenerator)
    {
        $this->repository = $fieldgenerator;
    }

    public function index(Request $request, $table_name)
    {
        return FieldGeneratorTransformer::collection($this->repository->getTableFields($table_name));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function findNew()
    {
        return [];
    }

    public function changeOrder(Request $request, $table_name)
    {
        return $this->repository->changeOrder(
            $request->get('oldOrder', false),
            $request->get('newOrder', false),
            $table_name
        );
    }

    public function find(Field $fieldgenerator)
    {
        return new FullFieldGeneratorTransformer($fieldgenerator);
    }

    public function update(Field $fieldgenerator, UpdateFieldGeneratorRequest $request)
    {
        if ('_tab' == $fieldgenerator->type) {
            return response()->json([
                'errors' => true,
            ]);
        }

        $this->repository->update($fieldgenerator, $request->request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('FieldGenerator updated'),
            'field' => new FullFieldGeneratorTransformer($fieldgenerator),
        ]);
    }

    public function store(CreateFieldGeneratorRequest $request, $table_name)
    {
        $request->merge([
            'type' => '_tab',
            'table_name' => $table_name,
            'table_field' => $request->label['en'] . rand(1, 900) . rand(1, 900),
        ]);

        $fieldgenerator = $this->repository->create($request->request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('FieldGenerator created'),
            'field' => new FullFieldGeneratorTransformer($fieldgenerator),
        ]);
    }

    public function destroy(Field $fieldgenerator)
    {
        if ('_tab' != $fieldgenerator->type) {
            return response()->json([
                'errors' => true,
            ]);
        }

        $this->repository->destroy($fieldgenerator);

        return response()->json([
            'errors' => false,
            'message' => trans('FieldGenerator deleted'),
        ]);
    }

    public function toggleField(Request $request)
    {
        return $this->repository->markSelectedAs($request->get('items', []), $request->position, $request->field);
    }
}
