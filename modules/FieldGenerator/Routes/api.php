<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| Additional Settings Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'additional-settings', 'middleware' => ['api.token', 'auth.admin']], function () {
    /*
    |--------------------------------------------------------------------------
    | Fields Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'fields'], function () {
        Route::get('modules', ['as' => 'api.backend.fieldgenerator.modules', 'uses' => 'FieldModulesController@index', 'middleware' => 'token-can:fieldgenerator.index']);

        Route::get('modules/{ident}', ['as' => 'api.backend.fieldgenerator.modules.show', 'uses' => 'FieldModulesController@show', 'middleware' => 'token-can:fieldgenerator.show']);

        Route::get('model/{parent}/{ident}', ['as' => 'api.backend.fieldgenerator.model.show', 'uses' => 'FieldModelController@show', 'middleware' => 'token-can:fieldgenerator.show']);

        Route::put('modules', ['as' => 'api.backend.fieldgenerator.modules.toggle', 'uses' => 'FieldModulesController@toggle', 'middleware' => 'token-can:fieldgenerator.edit']);

        Route::put('model/{parent}/{ident}', ['as' => 'api.backend.fieldgenerator.models.update', 'uses' => 'FieldModelController@update', 'middleware' => 'token-can:fieldgenerator.edit']);
    });
});
