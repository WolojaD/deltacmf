import helpers from '@services/helpers'

const allFrontendLocales = helpers.appStorage().allFrontendLocales

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/additional-settings/field-generator',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.fieldgenerator',
                component: require('@/views/fieldgenerator/module/index').default,
                meta: {
                    pageTitle: 'Fields',
                    breadcrumb: [
                        { name: 'Fields' }
                    ]
                }
            },
            {
                path: ':ident',
                name: 'api.backend.fieldgenerator.module.show',
                component: require('@/views/fieldgenerator/module/show').default,
                meta: {
                    pageTitle: 'Models',
                    breadcrumb: [
                        { name: 'Modules', link: 'api.backend.fieldgenerator' },
                        { name: 'Models' }
                    ]
                }
            },
            {
                path: ':parent/:ident',
                name: 'api.backend.fieldgenerator.model.show',
                component: require('@/views/fieldgenerator/model/show').default,
                meta: {
                    pageTitle: 'Model',
                    breadcrumb: [
                        { name: 'Modules', link: 'api.backend.fieldgenerator' },
                        { name: 'Models', link: 'api.backend.fieldgenerator.module.show|ident:parent'},
                        { name: 'Model' }
                    ]
                }
            },
            {
                path: ':table_name/create',
                name: 'api.backend.fieldgenerator.create',
                component: require('@/views/fieldgenerator/create').default,
                meta: {
                    pageTitle: 'Fields',
                    breadcrumb: [
                        { name: 'Fields', link: 'api.backend.fieldgenerator' },
                        { name: 'Tab Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.fieldgenerator.edit',
                component: require('@/views/fieldgenerator/edit').default,
                props: {
                    allFrontendLocales
                },
                meta: {
                    pageTitle: 'Field Edit',
                    breadcrumb: [
                        { name: 'Fields', link: 'api.backend.fieldgenerator' },
                        { name: 'Field Edit' }
                    ]
                }
            }
        ]
    }
]
