<?php

/*
|--------------------------------------------------------------------------
| BackEnd Routes
|--------------------------------------------------------------------------
|
| Here is where you can register BackEnd routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "backend" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Additional Settings Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'additional-settings'], function () {
    /*
    |--------------------------------------------------------------------------
    | Fields Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'field-generator'], function () {
        Route::get('/', ['as' => 'backend.fieldgenerator.index', 'uses' => 'FieldGeneratorController@index', 'middleware' => 'can:fieldgenerator.index'])->where('any', '.*');
        Route::any('{any}', ['as' => 'backend.fieldgenerator.index', 'uses' => 'FieldGeneratorController@index', 'middleware' => 'can:fieldgenerator.index'])->where('any', '.*');
    });
});
