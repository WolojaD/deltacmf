<?php

namespace Modules\FieldGenerator\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FieldGeneratorTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        $results = parent::toArray($request);

        $results['deletable'] = '_tab' == $this->type;
        $results['editable'] = '_tab' != $this->type;

        return $results;
    }
}
