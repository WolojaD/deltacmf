<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table_name');
            $table->string('table_field');

            // $table->string('colum_type')->nullable(); // if null not show
            // $table->string('colum_label')->nullable(); // if null field name
            // $table->string('column_order')->default('order|asc'); // text field|order because back need this.
            // $table->string('column_per_page')->default('order|asc'); // text field|order because back need this.

            $table->string('type')->nullable(); // if null not show
            $table->text('label')->nullable(); // if null field name
            $table->text('placeholder')->nullable(); // if null doesnt show
            $table->text('hint')->nullable(); // if null doesnt show
            $table->unsignedInteger('order')->default(1); // number of order like in modules

            $table->text('tab')->nullable(); // if null shows in main tab or not in tab idk
            // $table->boolean('is_tab')->default(0); // if null shows in main tab or not in tab idk
            // $table->string('translatable'); // maybe model will be used

            // $table->string('comment')->nullable(); //comment for edit/create field
            $table->string('relation')->nullable(); // example - category (for selects)
            // $table->string('relation_model_field')->nullable();
            $table->boolean('is_parent')->default(0);
            $table->boolean('is_published')->default(1);
            $table->boolean('is_translated')->default(0);
            $table->boolean('required')->default(0);

            $table->text('settings')->nullable(); // array of needed settings for fields;
            $table->string('validation')->nullable(); // array of validation rules;

            $table->unique(['table_name', 'table_field']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
