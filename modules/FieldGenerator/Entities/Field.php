<?php

namespace Modules\FieldGenerator\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntityTrait;

class Field extends Model
{
    use NamespacedEntityTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'fields';
    protected $guarded = [];
    protected $casts = [
        'is_parent' => 'boolean',
        'is_published' => 'boolean',
        'is_translated' => 'boolean',
        'required' => 'boolean',
        'placeholder' => 'array',
        'label' => 'array',
        'tab' => 'array',
        'hint' => 'array',
        'settings' => 'array',
    ];
    public $timestamps = false;
    protected static $entityNamespace = 'application/fieldgenerator';

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($entity) {
            $order = self::where('table_name', $entity->table_name)->max('order');
            $entity->order = !is_null($order) ? $order + 1 : 0;

            $entity->tab = static::generateRightArray($entity->tab);
            $entity->label = static::generateRightArray($entity->label);
            $entity->placeholder = static::generateRightArray($entity->placeholder);
            $entity->hint = static::generateRightArray($entity->hint);

            if ('_tab' != $entity->type) {
                $tab = self::where('table_name', $entity->table_name)->where('type', '_tab')->orderBy('order', 'desc')->first();

                $tab_name = $tab->label ?? ['en' => 'General', 'ru' => 'Общие'];

                $entity->tab = $tab_name;
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    protected static function generateRightArray($value)
    {
        if (!$value) {
            return ['en' => null, 'ru' => null];
        }

        return $value;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
