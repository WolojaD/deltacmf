<?php

namespace Modules\FieldGenerator\Providers;

use Illuminate\Database\Eloquent\Factory;
use Modules\FieldGenerator\Entities\Field;
use Modules\Core\Providers\ServiceProvider;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;
use Modules\FieldGenerator\Repositories\Eloquent\EloquentFieldGeneratorRepository;

class FieldGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerBindings();

        // $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
        //     $event->load('field_sidebar', array_dot(trans('field::sidebar')));
        //     $event->load('field_table-fields', array_dot(trans('field::table-fields')));
        // });
    }

    public function boot()
    {
        $this->publishConfig('fieldgenerator', 'config');
        $this->publishConfig('fieldgenerator', 'fields-config');
        $this->publishConfig('fieldgenerator', 'permissions');

        $this->registerFactories();

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function registerBindings()
    {
        $this->app->bind(FieldGeneratorRepository::class, function () {
            $repository = new EloquentFieldGeneratorRepository(new Field());

            return $repository;
        });
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories/');
        }
    }
}
