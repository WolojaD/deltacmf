<?php

namespace Modules\FieldGenerator\Repositories\Cache;

use Modules\Core\Repositories\Cache\CoreCacheDecorator;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;

class CacheFieldGeneratorDecorator extends CoreCacheDecorator implements FieldGeneratorRepository
{
    /**
     * @var FieldGeneratorRepository
     */
    protected $repository;

    public function __construct(FieldGeneratorRepository $field)
    {
        parent::__construct();

        $this->entityName = 'fields';
        $this->repository = $field;
    }
}
