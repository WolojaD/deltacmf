<?php

namespace Modules\FieldGenerator\Repositories;

use Modules\Core\Repositories\CoreInterface;

interface FieldGeneratorRepository extends CoreInterface
{
}
