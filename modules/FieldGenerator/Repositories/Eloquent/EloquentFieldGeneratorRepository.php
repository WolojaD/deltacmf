<?php

namespace Modules\FieldGenerator\Repositories\Eloquent;

use Nwidart\Modules\Facades\Module;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;
use Modules\FieldGenerator\Repositories\FieldGeneratorRepository;

class EloquentFieldGeneratorRepository extends EloquentCoreRepository implements FieldGeneratorRepository
{
    /**
     * Get modules list
     *
     * @return object
     */
    public function getModules()
    {
        $modules = Module::all();

        foreach ($modules as $module) {
            $module = json_decode(file_get_contents($module->getPath() . '/module.json'));

            if (in_array($module->order, [0, 1])) {
                continue;
            }

            $result[] = [
                'folder' => $module->name,
                'active' => (bool)$module->active
            ];
        }

        return $result;
    }

    /**
     * Save updated modules list
     *
     * @param object $modules
     *
     * @return void
     */
    public function toggleModule($module)
    {
        $module = Module::find($module);
        $file = $module->getPath() . '/module.json';

        $module = json_decode(file_get_contents($file));
        $module->active = $module->active ? 0 : 1;

        return file_put_contents(
            $file,
            json_encode($module)
        );
    }

    /**
     * Get folder name from modules list by id
     *
     * @param string $id
     *
     * @return void
     */
    public function getModuleFolder($id)
    {
        return $this->getModules()[$id]['folder'];
    }

    /**
     * Get all models from module by id of module
     *
     * @param string $id
     *
     * @return array
     */
    public function getModels($id)
    {
        $folder = $this->getModuleFolder($id);

        $files = collect(scandir(base_path("/modules/{$folder}/Config/models/")))
            ->filter(
                function ($file) {
                    return strpos($file, '.json');
                }
            )
            ->map(
                function ($file) {
                    return str_replace('.json', '', $file);
                }
            );

        return $files;
    }

    /**
     * Get path of model by module name and model name
     *
     * @param string $module
     * @param string $model
     *
     * @return void
     */
    public function getModelPath($module, $model)
    {
        $folder = $this->getModuleFolder($module);

        $table = $this->getModels($module)->first(
            function ($table, $key) use ($model) {
                return $table == $model;
            }
        );

        return base_path("/modules/{$folder}/Config/models/{$table}.json");
    }

    /**
     * Get model by module name and model name
     *
     * @param string $module
     * @param string $model
     *
     * @return object
     */
    public function getModel($module, $model)
    {
        return file_get_contents($this->getModelPath($module, $model));
    }

    /**
     * Save all changed model data and make backup
     *
     * @param array $model
     * @param string $module
     * @param string $model_index
     *
     * @return void
     */
    public function setModel($model, $module, $model_index)
    {
        $model_path = $this->getModelPath($module, $model_index);

        $this->backupModel($model_path, $module, $model_index);

        $model = $this->proccessTemplates($model);

        $this->saveModel($model_path, $model);
    }

    /**
     * Create backup file for model config file
     *
     * @param string $model_path
     * @param string $module
     * @param string $model_index
     *
     * @return void
     */
    public function backupModel($model_path, $module, $model_index)
    {
        $model_backup_name = $model_index . '.' . time() . '.json';
        $backup_folder = base_path('modules/' . $this->getModuleFolder($module) . '/Config/backup/');

        if (!file_exists($backup_folder)) {
            mkdir($backup_folder, 0777, true);
        }

        $model_backup_path = $backup_folder . $model_backup_name;

        copy($model_path, $model_backup_path);
    }

    /**
     * Proccessing all templates of model to change order and delete not needed
     *
     * @param array $model
     *
     * @return array
     */
    public function proccessTemplates($model)
    {
        foreach ($model['templates'] as $template_name => $template) {
            $model['templates'][$template_name] = $this->updatePositionsOfFields($template);
        }

        return $model;
    }

    /**
     * Updates list of fields in template
     *
     * @param array $template
     *
     * @return array
     */
    public function updatePositionsOfFields($template)
    {
        $new_ordered_template = ['standard' => $template['standard'] ?? []];

        foreach ($template['draggable'] as $field) {
            $new_ordered_template[$field] = $template[$field];
        }

        return $new_ordered_template;
    }

    /**
     * Save model to config
     *
     * @param string $model_path
     * @param array $model
     *
     * @return void
     */
    public function saveModel($model_path, $model)
    {
        return file_put_contents(
            $model_path,
            json_encode($model, JSON_UNESCAPED_UNICODE)
        );
    }
}
