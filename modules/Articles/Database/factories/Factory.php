<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Modules\Articles\Entities\Post;
use Modules\Articles\Entities\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Category::class, function (Faker $faker) {
    $data = [
        'parent_id' => 1,
        'status' => $faker->numberBetween(0, 1),
        'slug' => $faker->slug,
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->name,
        ];
    }

    return $data;
});

$factory->define(Post::class, function (Faker $faker) {
    $data = [
        'category_id' => $faker->numberBetween(1, 5),
        'status' => $faker->numberBetween(0, 2),
        'slug' => $faker->slug,
        'date_from' => Carbon::now(),
    ];

    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
        $data[$key] = [
            'title' => $faker->sentence,
            'content' => $faker->randomHtml(2, 3),
        ];
    }

    return $data;
});

$factory->state(Category::class, 'is_home', [
    'parent_id' => null,
    'depth' => 1,
]);

$factory->state(Category::class, 'online', [
    'status' => 1,
]);

$factory->state(Category::class, 'offline', [
    'status' => 0,
]);

$factory->state(Category::class, 'testing', [
    'status' => 2,
]);

$factory->state(Post::class, 'online', [
    'status' => 1,
]);

$factory->state(Post::class, 'offline', [
    'status' => 0,
]);

$factory->state(Post::class, 'testing', [
    'status' => 2,
]);
