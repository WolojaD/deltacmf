<?php

namespace Modules\Articles\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Articles\Entities\Post;
use Modules\Articles\Entities\Category;

class ArticlesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('production')) {
            $this->generateArticles();
        }
    }

    public function generateArticles()
    {
        $data = [
            'parent_id' => null,
            'status' => 1,
            'slug' => 'main_category',
        ];

        foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
            $data[$key] = [
                'title' => 'Главная категория'
            ];
        }

        $category = factory(Category::class)->create($data);

        for ($i = 1; $i < 7; $i++) {
            $data = [
                'parent_id' => 1,
                'status' => 1,
                'slug' => 'kategoria-' . $i,
            ];

            foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                $data[$key] = [
                    'title' => 'категория ' . $i
                ];
            }

            $category = factory(Category::class)->create($data);

            unset($data);

            $j_max = rand(3, 6);

            for ($j = 1; $j < $j_max; $j++) {
                $data = [
                    'parent_id' => $category->id,
                    'status' => 1,
                    'slug' => 'podkategoria-' . $j . '-kategorii-' . $i,
                ];

                foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                    $data[$key] = [
                        'title' => 'подкатегория ' . $j . ' категории ' . $i,
                    ];
                }
                $sub_category = factory(Category::class)->create($data);
                unset($data);

                if (rand(0, 1)) {
                    continue;
                }

                $k_max = rand(2, 4);

                for ($k = 1; $k < $k_max; $k++) {
                    $data = [
                        'category_id' => $sub_category->id,
                        'status' => 1,
                        'slug' => 'statya-' . $k . '-podkategorii-' . $j . '-kategorii-' . $i,
                    ];

                    foreach (config('laravellocalization.defaultApplicationLocales') as $key => $value) {
                        $data[$key] = [
                            'title' => 'статья ' . $k . ' подкатегории ' . $j . ' категории ' . $i
                        ];
                    }

                    $product = factory(Post::class)->create($data);

                    unset($data);
                }
            }
        }
    }
}
