<?php

namespace Modules\Articles\Transformers;

use Modules\Articles\Entities\Post;
use Modules\Seo\Entities\Structure;
use Illuminate\Http\Resources\Json\Resource;

class PostTransformer extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'preview_image' => $this->preview_image,
            'status' => $this->status,
            'description' => $this->defaultTranslate()->description ?? '',
            'date_from' => $this->date_from ? $this->date_from->toRfc2822String() : '',
            'date_to' => $this->date_to ? $this->date_to->toRfc2822String() : '',
            'title' => $this->defaultTranslate()->title ?? '',
            'path' => Structure::where('object_id', $this->id)->where('model', Post::class)->select('url')->first()->url ?? '',
            'category_title' => $this->category ? $this->category->defaultTranslate()->title : '',
        ];
    }
}
