<?php

namespace Modules\Articles\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullCategoryTransformer extends Resource
{
    public function toArray($request)
    {
        $categoryData = [
            'id' => $this->id,
            'slug' => $this->slug,
            'status' => $this->status,
            'parent_id' => $this->parent_id,
            'mobile_image' => $this->mobile_image,
            'desktop_image' => $this->desktop_image,
            'sort_by_date' => $this->sort_by_date,
            'show_date' => $this->show_date,
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $categoryData[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $categoryData;
    }
}
