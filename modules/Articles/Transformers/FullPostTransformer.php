<?php

namespace Modules\Articles\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class FullPostTransformer extends Resource
{
    public function toArray($request)
    {
        $postData = [
            'id' => $this->id,
            'slug' => $this->slug,
            'status' => $this->status,
            'category_id' => $this->category->id,
            'mobile_image' => $this->mobile_image,
            'desktop_image' => $this->desktop_image,
            'preview_image' => $this->preview_image,
            'date_from' => $this->date_from ? $this->date_from->format('Y-m-d H:i:s') : '',
            'date_to' => $this->date_to ? $this->date_to->format('Y-m-d H:i:s') : '',
        ];

        foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $postData[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
            }
        }

        return $postData;
    }
}
