<?php

namespace Modules\Articles\Transformers;

use Modules\Seo\Entities\Structure;
use Modules\Articles\Entities\Category;
use Illuminate\Http\Resources\Json\Resource;

class CategoryTransformer extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'status' => $this->status,
            'created_at' => $this->created_at->toRfc2822String(),
            'parent_id' => $this->parent_id,
            'path' => Structure::where('object_id', $this->id)->where('model', Category::class)->select('url')->first()->url ?? '',
            'title' => $this->defaultTranslate()->title ?? '',
            'children_count' => $this->children_count ?? null,
        ];
    }
}
