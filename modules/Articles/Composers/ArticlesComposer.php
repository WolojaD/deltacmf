<?php

namespace Modules\Articles\Composers\Frontend;

use Illuminate\Contracts\View\View;
use Modules\Articles\Repositories\PostRepository;

class ArticlesComposer
{
    /**
     * @var PostRepository
     */
    private $post;

    public function __construct(PostRepository $post)
    {
        $this->post = $post;
    }

    public function compose(View $view)
    {
        $view->with('posts', $this->post->all());
    }
}
