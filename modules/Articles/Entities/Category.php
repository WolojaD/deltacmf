<?php

namespace Modules\Articles\Entities;

use Laravel\Scout\Searchable;
use Kalnoy\Nestedset\NodeTrait;
use Modules\Page\Entities\Page;
use Modules\Core\Eloquent\Model;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Core\Eloquent\NestedBuilder;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;
use Modules\Seo\Repositories\Eloquent\EloquentStructureRepository;

class Category extends Model implements SeoSyncInterface
{
    use Translatable,
        SeoSyncTrait,
        Searchable,
        NodeTrait {
            NodeTrait::usesSoftDelete insteadof Searchable;
        }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $asYouType = true;

    protected $table = 'articles_categories';

    public $translatedAttributes = [];

    protected static $entityNamespace = 'application/articles';

    protected $file_path = 'modules/Articles/Config/models/category.json';

    public $page_template = ['articles', 'news'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        static::deleting(function ($obj) {
            $obj->children->each(function ($child) {
                $child->delete();
            });
        });

        parent::boot();
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['translations']);

        if ($this->translatedAttributes) {
            foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                foreach ($this->translatedAttributes as $translatedAttribute) {
                    $array[$translatedAttribute . '_' . $locale] = $this->translateOrNew($locale)->$translatedAttribute;
                }
            }
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * @return type
     */
    protected function getPrePath()
    {
        if ($this->parent && !$this->parent->page) {
            return $this->parent->getPrePath() . '/' . $this->parent->slug;
        }

        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function posts()
    {
        return $this->hasMany(Post::class)->with('translations');
    }

    public function active_posts()
    {
        return $this->hasMany(Post::class)->where('status', 1)->with('translations');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('children', 'translations')->withCount('posts');
    }

    public function first_children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('translations');
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'relation', 'id')->whereIn('page_template', ['articles', 'news']);
    }

    public function structure()
    {
        return $this->hasOne(Structure::class, 'object_id')->whereIn('page_template', ['articles', 'news'])->with('translations');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getAllPostsCountAttribute()
    {
        if ($this->children->count()) {
            return $this->children->reduce(function ($carier, $child) {
                return $carier + $child->all_posts_count;
            }, 0);
        }

        return $this->posts_count ?? $this->posts->count();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */
    public function parentStructure()
    {
        return Structure::when($this->parent_id, function ($query) {
            $query->whereIn('page_template', ['articles', 'news'])
                    ->where('object_id', $this->parent_id);
        }, function ($query) {
            $query->where('id', function ($q) {
                $q->select('parent_id')
                    ->from('seo_structure')
                    ->whereIn('page_template', ['articles', 'news'])
                    ->where('object_id', $this->id)
                    ->first();
            });
        })
        ->first();
    }

    public static function seoAddChildrenToStructure($structure)
    {
        $category = self::where('id', $structure->object_id)->first();

        if (!$category) {
            return;
        }

        $structure_repository = (new EloquentStructureRepository(new Structure));
        $children = $category->children;

        foreach ($children as $sub_category) {
            $structure_repository->createWithModel($sub_category);
        }

        $posts = $category->posts;

        foreach ($posts as $post) {
            $structure_repository->createWithModel($post);
        }
    }

    /**
     * Changes builder to custom
     *
     * @param [type] $query
     *
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new NestedBuilder($query);
    }
}
