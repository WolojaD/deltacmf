<?php

namespace Modules\Articles\Entities;

use Carbon\Carbon;
use Laravel\Scout\Searchable;
use Modules\Page\Entities\Page;
use Modules\Core\Eloquent\Model;
use Modules\Seo\Entities\Structure;
use Modules\Seo\Traits\SeoSyncTrait;
use Modules\Seo\Interfaces\SeoSyncInterface;
use Modules\Core\Internationalisation\Translatable;

class Post extends Model implements SeoSyncInterface
{
    use Translatable,
        SeoSyncTrait,
        Searchable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $asYouType = true;

    protected $table = 'articles_posts';

    public $translatedAttributes = [];

    protected $casts = [
        'status' => 'int',
        'date_from' => 'datetime',
        'date_to' => 'datetime',
    ];

    protected static $entityNamespace = 'application/articles';

    public $groupOrder = 'category_id';

    protected $file_path = 'modules/Articles/Config/models/post.json';

    public $page_template = ['article', 'new'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        self::creating(function ($entity) {
            self::where('category_id', $entity->category_id ?? 0)->increment('order');

            $entity->order = 0;
        });

        self::deleting(function ($entity) {
            self::where('category_id', $entity->category_id ?? 0)->where('order', '>', $entity->order)->decrement('order');
        });

        parent::boot();
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['translations']);

        if ($this->translatedAttributes) {
            foreach (app('laravellocalization')->getSupportedFrontendLocales() as $locale => $supportedLocale) {
                foreach ($this->translatedAttributes as $translatedAttribute) {
                    $array[$translatedAttribute . '_' . $locale] = $this->translateOrNew($locale)->$translatedAttribute;
                }
            }
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * Method allows to show order column in articles
     *
     * @return void
     */
    public function showOrder()
    {
        $exploded = array_reverse(explode('/', trim(request()->headers->get('referer'), '/')));

        return $exploded[1] == 'category' && (Category::where('id', $exploded[0])->select('sort_by_date')->first()->sort_by_date ?? false);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function category()
    {
        return $this->belongsTo(Category::class)->withCount('posts');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopePublished($query)
    {
        $current_time = Carbon::now();

        return $query->where('date_from', '<=', $current_time)->where(function ($query) use ($current_time) {
            return $query->where('date_to', '>=', $current_time)->orWhereNull('date_to');
        });
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getCategoryTitleAttribute()
    {
        return $this->category->title ?? '';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SEO SYNC IMPLEMENTATION
    |--------------------------------------------------------------------------
    */

    public function parentStructure()
    {
        return Structure::where('object_id', $this->category_id)
            ->whereIn('page_template', ['articles', 'news'])
            ->whereIn('model', [Category::class, Page::class])
            ->first();
    }
}
