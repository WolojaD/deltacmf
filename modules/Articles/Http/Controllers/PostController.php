<?php

namespace Modules\Articles\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Articles\Repositories\PostRepository;

class PostController extends Controller
{
    protected $category;

    public function __construct(PostRepository $post)
    {
        $this->post = $post;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($page, $category, $slug)
    {
        $post = $this->post->findBySlug($slug);
        $path_without_page = ltrim(explode($post->slug, request()->path())[1], '/');

        if (!$path_without_page) {
            return view('articles::' . $page->getViewTemplate() . '.show', compact('post', 'page'));
        }
    }
}
