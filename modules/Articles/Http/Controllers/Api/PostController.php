<?php

namespace Modules\Articles\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Articles\Repositories\PostRepository;
use Modules\Articles\Transformers\PostTransformer;
use Modules\Articles\Http\Requests\CreatePostRequest;
use Modules\Articles\Http\Requests\UpdatePostRequest;
use Modules\Articles\Repositories\CategoryRepository;
use Modules\Articles\Transformers\FullPostTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;

class PostController extends CoreApiController
{
    /**
     * @var PostRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    protected $category;

    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        $this->repository = $post;
        $this->modelInfo = $this->repository->getJsonList();
        $this->category = $category;
    }

    public function index(Request $request, $id = false)
    {
        if ($id) {
            return PostTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return PostTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $post = $this->repository->find($id);

        return new FullPostTransformer($post);
    }

    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->repository->find($id);

        $this->repository->update($post, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('articles::messages.api.post updated'),
            'id' => $post->id
        ]);
    }

    public function store(CreatePostRequest $request)
    {
        $post = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('articles::messages.api.post created'),
            'id' => $post->id
        ]);
    }

    public function destroy($id)
    {
        $post = $this->repository->find($id);

        $this->repository->destroy($post);

        return response()->json([
            'errors' => false,
            'message' => trans('articles::messages.api.post deleted'),
        ]);
    }
}
