<?php

namespace Modules\Articles\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Articles\Repositories\CategoryRepository;
use Modules\Articles\Transformers\CategoryTransformer;
use Modules\Core\Http\Controllers\Api\CoreApiController;
use Modules\Articles\Http\Requests\CreateCategoryRequest;
use Modules\Articles\Http\Requests\UpdateCategoryRequest;
use Modules\Articles\Transformers\FullCategoryTransformer;

class CategoryController extends CoreApiController
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * @var Object
     */
    protected $modelInfo;

    /**
     * @var Status
     */
    protected $status;

    public function __construct(CategoryRepository $category)
    {
        $this->repository = $category;
        $this->modelInfo = $this->repository->getJsonList();
    }

    public function index(Request $request, $id = false)
    {
        if ($this->modelInfo->templates->default->standard->nested ?? false) {
            $request->merge(['pageId' => $id ?: null]);

            return CategoryTransformer::collection($this->repository->serverPaginationFilteringFor($request));
        }

        if ($id) {
            return CategoryTransformer::collection($this->repository->paginator(collect([]), 10));
        }

        return CategoryTransformer::collection($this->repository->serverPaginationFilteringFor($request));
    }

    public function find($id)
    {
        $category = $this->repository->find($id);

        return new FullCategoryTransformer($category);
    }

    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->repository->find($id);

        $this->repository->update($category, $request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('articles::messages.api.category updated'),
            'id' => $category->id
        ]);
    }

    public function store(CreateCategoryRequest $request)
    {
        $category = $this->repository->create($request->except('ident'));

        return response()->json([
            'errors' => false,
            'message' => trans('articles::messages.api.category created'),
            'id' => $category->id
        ]);
    }

    public function destroy($id)
    {
        $category = $this->repository->find($id);

        $this->repository->destroy($category);

        return response()->json([
            'errors' => false,
            'message' => trans('articles::messages.api.category deleted'),
        ]);
    }
}
