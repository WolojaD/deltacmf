<?php

namespace Modules\Articles\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Articles\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * Show the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show($page, $slug)
    {
        $category = $this->category->findByPath($slug);

        if ($category) {
            $path_without_page = ltrim(explode($category->slug, request()->path())[1], '/');
        }

        if (isset($path_without_page) && !$path_without_page) {
            $method = $this->getMethodForPage($page);

            return $this->$method($page, $category);
        }

        return app('\Modules\Articles\Http\Controllers\PostController')->show($page, $category, $path_without_page ?? $slug);
    }

    /**
    *
    * Methods based on page template, needed to take all needed variables to needed views for template
    *
    */
    protected function articles($page, $category)
    {
        $sub_categories = $this->category->findByParentId($category->id);

        return view('page::' . $page->getViewTemplate(), compact('category', 'page', 'sub_categories'));
    }

    protected function news($page, $category)
    {
        $sub_categories = $this->category->findByParentId($category->id);

        return view('page::' . $page->getViewTemplate(), compact('page', 'category', 'sub_categories'));
    }

    /**
     * Return the method for the given page
     * or the default method if none found.
     *
     * @param $page
     *
     * @return string
     */
    private function getMethodForPage($page)
    {
        return method_exists($this, $page->page_template) ? $page->page_template : 'news';
    }
}
