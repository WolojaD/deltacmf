<?php

namespace Modules\Articles\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePostRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Articles/Config/models/post.json');
    }

    public function authorize()
    {
        return true;
    }
}
