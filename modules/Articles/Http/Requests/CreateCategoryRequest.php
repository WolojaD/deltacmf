<?php

namespace Modules\Articles\Http\Requests;

use Modules\Seo\Entities\Structure;
use Modules\Core\Internationalisation\BaseFormRequest;

class CreateCategoryRequest extends BaseFormRequest
{
    protected $file;

    public function __construct()
    {
        $this->file = base_path('modules/Articles/Config/models/category.json');
    }

    public function rules()
    {
        return parent::rules();
    }

    protected function prepareForValidation()
    {
        if ($this->has('slug')) {
            $max = Structure::where('slug', 'like', $this->slug . '%')
                ->pluck('slug')
                ->map(function ($slug) {
                    return (int) trim(str_replace(request()->slug, '', $slug), '-');
                })->max();

            if (is_numeric($max)) {
                $this->merge(['slug' => $this->slug . '-' . ($max + 1)]);
            }
        }
    }

    public function authorize()
    {
        return true;
    }
}
