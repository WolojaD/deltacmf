<?php

$folder = base_path('modules/Articles/Config/models');
$files = scandir($folder);
$models = [];

foreach ($files as $key => $file) {
    if ('.' !== $file[0]) {
        $models[explode('.', $file)[0]] = (json_decode(file_get_contents($folder . '/' . $file)));
    }
}

foreach ($models as $model) {
    $permission_index = str_replace_array('_', ['.', '.'], $model->backend_path_name);
    $permission = str_replace_first('articles.', '', $permission_index);
    $result[$permission_index] = [
        'index' => 'core::permissions.names.index',
        'create' => 'core::permissions.names.create',
        // 'show' => 'core::permissions.names.show',
        'edit' => 'core::permissions.names.edit',
        'destroy' => 'core::permissions.names.destroy',
    ];
}

return $result ?? [];
