<?php

namespace Modules\Articles\Repositories\Eloquent;

use Illuminate\Database\Query\Builder;
use Modules\Articles\Entities\Category;
use Modules\Articles\Repositories\PostRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentPostRepository extends EloquentCoreRepository implements PostRepository
{
    public $model_config = 'modules/Articles/Config/models/post.json';

    /**
     * @param int $id
     *
     * @return object
     */
    public function find($id)
    {
        return $this->model->where('id', $id)->with('category', 'translations', 'category.translations')->first();
    }

    /**
     * Create a article post.
     *
     * @param array $data
     *
     * @return Post
     */
    public function create($data)
    {
        if (isset($data['date_to']) && is_null($data['date_to'])) {
            unset($data['date_to']);
        }

        return $this->model->create($data);
    }

    /**
     * Update a resource.
     *
     * @param $post
     * @param array $data
     *
     * @return mixed
     */
    public function update($post, $data)
    {
        if (is_null($data['date_to'])) {
            unset($data['date_to']);
        }

        $post->update($data);

        return $post;
    }

    public function destroy($model)
    {
        return $model->delete();
    }

    /**
     * Return all resources in the given language.
     *
     * @param string $lang
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allTranslatedIn($lang)
    {
        return $this->model->whereHas('translations', function (Builder $q) use ($lang) {
            $q->where('locale', "$lang");
            $q->where('title', '!=', '');
        })->with('translations')->whereStatus($this->published)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Return the latest x article posts.
     *
     * @param int $amount
     *
     * @return Collection
     */
    public function latest($amount = 5)
    {
        return $this->model->whereStatus($this->published)->orderBy('created_at', 'desc')->take($amount)->get();
    }

    /**
     * Get the previous post of the given post.
     *
     * @param object $post
     *
     * @return object
     */
    public function getPreviousOf($post)
    {
        return $this->model->where('created_at', '<', $post->created_at)
            ->whereStatus($this->published)->orderBy('created_at', 'desc')->first();
    }

    /**
     * Get the next post of the given post.
     *
     * @param object $post
     *
     * @return object
     */
    public function getNextOf($post)
    {
        return $this->model->where('created_at', '>', $post->created_at)
            ->whereStatus($this->published)->first();
    }

    /**
     * Find a resource by the given slug.
     *
     * @param string $slug
     *
     * @return object
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->with('translations')->firstOrFail();
    }

    public function categories()
    {
        return Category::listsDefaultTranslations('title')->orderBy('id', 'asc')->pluck('title', 'id')->toArray();
    }

    /**
    * Data generation to show in view blade
    */
    public function getViewData($structure)
    {
        if ($structure->object_id) {
            return [
                'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
                'structure' => $structure,
                'post' => $this->model->where('id', $structure->object_id)->firstOrFail()
            ];
        }

        return [];
    }
}
