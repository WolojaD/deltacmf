<?php

namespace Modules\Articles\Repositories\Eloquent;

use Modules\Settings\Entities\Settings;
use Modules\Articles\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentCoreRepository;

class EloquentCategoryRepository extends EloquentCoreRepository implements CategoryRepository
{
    public $model_config = 'modules/Articles/Config/models/category.json';

    /**
     * {@inheritdoc}
     */
    public function mainTranslatedIn($lang)
    {
        return $this->model->whereHas('translations', function (Builder $query) use ($lang) {
            $query->where('locale', "$lang");
        })
            ->whereIsRoot()
            ->with('translations', 'posts', 'children')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * @param string $path
     *
     * @return \Modules\Articles\Entities\Category
     */
    public function findByPath($path)
    {
        return $this->model->findByPath($path)->first();
    }

    public function findByParentId($parent_id)
    {
        return $this->model->where('parent_id', $parent_id)->with('translations')->select(['id', 'parent_id', 'slug'])->get();
    }

    /**
    * Data generation to show in view blade
    */
    public function getViewData($structure)
    {
        if ($structure->object_id) {
            $category = $this->model->where('id', $structure->object_id)->with('first_children')->firstOrFail();

            $per_page = (int) settings('articles::' . $structure->page_template . '_per_page');

            $posts = $category->posts()
                ->published()
                ->listsTranslations('title', 'description')
                ->select('slug', 'title', 'description', 'date_from', 'preview_image')
                ->paginate($per_page ?: 1);

            return [
                'breadcrumbs' => $this->getFrontBreadcrumbs($structure),
                'structure' => $structure,
                'category' => $category,
                'posts' => $posts,
                'paginates' => $posts->paginates()
            ];
        }

        return [];
    }
}
