<?php

namespace Modules\Articles\Repositories\Cache;

use Modules\Articles\Repositories\PostRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CachePostDecorator extends CoreCacheDecorator implements PostRepository
{
    /**
    * @var PostRepository
    */
    protected $repository;

    public function __construct(PostRepository $articles)
    {
        parent::__construct();

        $this->entityName = 'articles_posts';
        $this->repository = $articles;
    }
}
