<?php

namespace Modules\Articles\Repositories\Cache;

use Modules\Articles\Repositories\CategoryRepository;
use Modules\Core\Repositories\Cache\CoreCacheDecorator;

class CacheCategoryDecorator extends CoreCacheDecorator implements CategoryRepository
{
    /**
    * @var CategoryRepository
    */
    protected $repository;

    public function __construct(CategoryRepository $articles)
    {
        parent::__construct();

        $this->entityName = 'articles_categories';
        $this->repository = $articles;
    }
}
