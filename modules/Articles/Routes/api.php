<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiRouteList('Articles');

Route::group(['prefix' => '/articles/post', 'middleware' => ['api.token', 'auth.admin']], function () {
    Route::post('text-editor-upload', ['as' => 'api.backend.articles_post.textEditorUpload', 'uses' => 'PostController@textEditorUpload', 'middleware' => 'token-can:articles.post.edit']);
});
