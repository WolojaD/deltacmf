import helpers from '@services/helpers'

const tabs = [
    {
        name: 'posts',
        label: 'articles_sidebar.submenu.posts',
        permission: 'articles.post.index',
        routeName: 'api.backend.articles_post',
    },
    {
        name: 'categories',
        label: 'articles_sidebar.submenu.categories',
        permission: 'articles.category.index',
        routeName: 'api.backend.articles_category'
    }
]

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/articles',
        component: require('@components/layouts/default-page').default,
        redirect: {name: 'api.backend.articles_post'},
        meta: { requiresAuth: true },
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/articles/post',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.articles_post',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'posts'
                },
                meta: {
                    pageTitle: 'Articles',
                    breadcrumb: [
                        { name: 'Articles' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.articles_post.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Article Create',
                    breadcrumb: [
                        { name: 'Articles', link: 'api.backend.articles_post' },
                        { name: 'Article Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.articles_post.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Article Edit',
                    breadcrumb: [
                        { name: 'Articles', link: 'api.backend.articles_post' },
                        { name: 'Article Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/articles/category',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.articles_category',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'categories'
                },
                meta: {
                    pageTitle: 'Category show',
                    breadcrumb: [
                        { name: 'Category show' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)',
                name: 'api.backend.articles_category.show',
                component: require('@/views/crud/index').default,
                props: {
                    tabs,
                    activeName: 'categories'
                },
                meta: {
                    pageTitle: 'Category show',
                    breadcrumb: [
                        { name: 'Category show' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/create',
                name: 'api.backend.articles_category.create',
                component: require('@/views/crud/create').default,
                meta: {
                    pageTitle: 'Category Create',
                    breadcrumb: [
                        { name: 'Categories', link: 'api.backend.articles_category' },
                        { name: 'Category Create' }
                    ]
                }
            },
            {
                path: ':ident(\\d+)/edit',
                name: 'api.backend.articles_category.edit',
                component: require('@/views/crud/edit').default,
                meta: {
                    pageTitle: 'Category Edit',
                    breadcrumb: [
                        { name: 'Categories', link: 'api.backend.articles_category' },
                        { name: 'Category Edit' }
                    ]
                }
            }
        ]
    }
]
