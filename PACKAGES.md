# Tech list

List of main packages used in this application with doc urls:

* [axios](https://github.com/axios/axios) - for routing inside backend
* [babel](https://babeljs.io) - for Vue
* [laravel-mix](https://laravel.com/docs/5.6/mix) - compilation
* [lodash](https://lodash.com/docs/4.17.10) - datetime, pagination, etc.
* [moment](https://momentjs.com/docs/) - default
* [popper.js](https://popper.js.org/popper-documentation.html) - mimimi helper
* [tailwindcss](https://tailwindcss.com/docs/what-is-tailwind/) - all css frontend
* [vue](https://vuejs.org/v2/guide/) - all frontend logic, routing, etc.
* [element](http://element.eleme.io/#/en-US) - vue frontend components
* [velocity](https://github.com/julianshapiro/velocity) - js animation

List of helpers or extentions used to help main packages:

* vue-events
* vue-router
* vuex
* v-mask
* [sweetalert2](https://sweetalert2.github.io) - all modals and alerts
* [tailwindcss-tables](https://github.com/drehimself/tailwindcss-tables) - bootstrap styled tables for Tailwind CSS
* [form-backend-validation](https://github.com/spatie/form-backend-validation) - online form validation for vue
* [vue-i18n](https://kazupon.github.io/vue-i18n/introduction.html) - internationalization plugin
* [vue2-dropzone](https://rowanwins.github.io/vue-dropzone/docs/dist/#/installation) - vue dropzone
* [vue-cropperjs](https://github.com/Agontuk/vue-cropperjs) - cropper js
* [vuedraggable](https://github.com/SortableJS/Vue.Draggable) - drag-and-drop

List of laravel packages:

* [sentinel](https://cartalyst.com/manual/sentinel/2.0#introduction) - security
* [modules](https://nwidart.com/laravel-modules/v3/introduction) - module system
* [ziggy](https://github.com/tightenco/ziggy) - route() inside js
* [uuid](https://github.com/webpatser/laravel-uuid) - aditional security
* [debugbar](https://github.com/barryvdh/laravel-debugbar) - debugbar
* [yaml](http://symfony.com/doc/current/components/yaml.html) - symfony yaml
* [yaml extension](https://github.com/antonioribeiro/yaml) - symfony yaml extension for laravel
* [dbal](https://github.com/doctrine/dbal) - doctrine database abstraction layer
* [larastan](https://github.com/nunomaduro/larastan) - laravel errors finder in PHP
* [impersonate](https://github.com/404labfr/laravel-impersonate) - authenticate as another users
* [agent](https://github.com/jenssegers/agent) - user agent parser
* [pivot](https://github.com/fico7489/laravel-pivot) - new events for sync, attach, detach or updateExistingPivot
* [secureheaders](https://github.com/mikefrancis/laravel-secureheaders) - secureheaders wrapper
* [excel](https://github.com/Maatwebsite/Laravel-Excel) - excel
* [nestedset](https://github.com/lazychaser/laravel-nestedset) - nested

