<?php

return [
    // Toggles error reporting, see https://github.com/aidantwoods/SecureHeaders/wiki/errorReporting
    'errorReporting' => false,

    // Safe mode
    'safeMode' => false,

    // HSTS Strict-Transport-Security
    'hsts' => [
        'enabled' => true,
    ],

    // Content Security Policy
    'csp' => [
        'default' => [
            'self',
            'https://*',
        ],
        'default-src' => [
            'self',
        ],
        'object-src' => [
            'self',
        ],
        'connect-src' => [
            'self',
            'blob:',
            'https://*',
        ],
        'media-src' => [
            '*',
            'blob:',
        ],
        'child-src' => [
            'self',
            'https://*',
        ],
        'img-src' => [
            '*',
            'self',
            'unsafe-inline',
            'unsafe-eval',
            'data:',
            'blob:',
            'https://*',
        ],
        'style-src' => [
            'self',
            'unsafe-inline',
            'https://*',
        ],
        'script-src' => [
            'self',
            'unsafe-inline',
            'unsafe-eval',
            'https://*',
        ],
        'worker-src' => [
            'self',
            'unsafe-inline',
            'unsafe-eval',
            'https://*',
        ],
        'frame-src' => [
            'self',
            'https://*',
        ],
        'frame-ancestors' => [
            'self',
        ],
        'font-src' => [
            'self',
            'data:',
            'https://*',
        ],
        // 'form-action' => [
        //     'self',
        //     'unsafe-inline',
        //     'unsafe-eval',
        // ],
    ],
];
