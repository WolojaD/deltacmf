<?php

return [
    // IMPORTANT - Don't change this array if you don't now what it doing!".
    'supportedLocales' => [
        'ru' => ['name' => 'Russian', 'script' => 'Cyrl', 'native' => 'Русский'],
        'en' => ['name' => 'English', 'script' => 'Latn', 'native' => 'English'],
    ],

    // This languages need for db seed's
    // Uncomment the languages that your site supports and reorder whem (if you need this) in order to main language of your application goes first
    'defaultApplicationLocales' => [
        'uk' => ['name' => 'Ukrainian', 'script' => 'Cyrl', 'native' => 'Українська'],
        'ru' => ['name' => 'Russian', 'script' => 'Cyrl', 'native' => 'Русский'],
        'en' => ['name' => 'English', 'script' => 'Latn', 'native' => 'English'],
        // 'de' => ['name' => 'German', 'script' => 'Latn', 'native' => 'Deutsch'],
        // 'fr' => ['name' => 'French', 'script' => 'Latn', 'native' => 'Français'],
        // 'es' => ['name' => 'Spanish', 'script' => 'Latn', 'native' => 'Español'],
        // 'ja' => ['name' => 'Japanese', 'script' => 'Jpan', 'native' => '日本語'],
        // 'zh' => ['name' => 'Chinese (Simplified)', 'script' => 'Hans', 'native' => '简体中文'],
    ],

    // Negotiate for the user locale using the Accept-Language header if it's not defined in the URL?
    // If false, system will take app.php locale attribute
    'useAcceptLanguageHeader' => true,

    // Should application use the locale stored in a session
    // if the locale is not defined in the url?
    // If false and locale not defined in the url, the system will
    // take the default locale (defined in config/app.php) or
    // the browser preferred language (if useAcceptLanguageHeader is true) or
    // the cookie locale (if useCookieLocale is true)
    'useSessionLocale' => true,

    // Should application use and store the locale stored in a cookie
    // if the locale is not defined in the url or the session?
    // If true and locale not defined in the url or the session,
    // system will take the default locale (defined in config/app.php)
    // or the browser preferred locale (if useAcceptLanguageHeader is true)
    'useCookieLocale' => true,

    // If LaravelLocalizationRedirectFilter is active and hideDefaultLocaleInURL
    // is true, the url would not have the default application language
    //
    // IMPORTANT - When hideDefaultLocaleInURL is set to true, the unlocalized root is treated as the applications default locale "app.locale".
    // Because of this language negotiation using the Accept-Language header will NEVER occur when hideDefaultLocaleInURL is true.
    //
    'hideDefaultLocaleInURL' => true,

    // If you want to display the locales in particular order in the language selector you should write the order here.
    //CAUTION: Please consider using the appropriate locale code otherwise it will not work
    //Example: 'localesOrder' => ['es','en'],
    'localesOrder' => [],

    //  If you want to use custom lang url segments like 'at' instead of 'de-AT', you can use the mapping to tallow the LanguageNegotiator to assign the descired locales based on HTTP Accept Language Header. For example you want ot use 'at', so map HTTP Accept Language Header 'de-AT' to 'at' (['de-AT' => 'at']).
    'localesMapping' => [],

    // Locale suffix for LC_TIME and LC_MONETARY
    // Defaults to most common ".UTF-8". Set to blank on Windows systems, change to ".utf8" on CentOS and similar.
    'utf8suffix' => env('LARAVELLOCALIZATION_UTF8SUFFIX', '.UTF-8'),

    // URLs which should not be processed, e.g. '/nova', '/nova/*', '/nova-api/*' or specific application URLs
    // Defaults to []
    'urlsIgnored' => ['/api', '/api/*'],
];
