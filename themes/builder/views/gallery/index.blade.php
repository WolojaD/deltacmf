@if($gallery)

    @accessByRoleTypeAdmin
        <a href="{{ route('backend.gallery.show', $gallery->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Gallery</a>
    @endAccessByRoleTypeAdmin

    <div class="container">
        <div id="main_area">

            @if(count($gallery->images) > 0 && $gallery->show_title)
                <h2>{{ $gallery->title ?? '' }}</h2>
            @endif
            <p>{{ $gallery->description ?? '' }}</p>

            <div class="row">
                <div class="col-sm-6" id="slider-thumbs">
                    <ul class="hide-bullets">

                        @foreach($gallery->images as $image)
                            <li class="col-sm-3">
                                <a class="thumbnail" id="carousel-selector-{{ $loop->index }}">
                                    <img src="/storage/i/gallery-image{{ $image->path }}">
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="col-xs-12" id="slider">
                        <div class="row">
                            <div class="col-sm-12" id="carousel-bounding-box">
                                <div class="carousel slide" id="myCarousel">
                                    <div class="carousel-inner">

                                        @foreach($gallery->images as $image)
                                            <div class="@if($loop->index == 0) active @endif item" data-slide-number="{{ $loop->index }}">
                                                <img src="/storage/origin{{ $image->path }}">

                                                <p>{{ $image->title ?? '' }}</p>
                                                <p>{{ $image->description ?? '' }}</p>
                                            </div>
                                        @endforeach

                                    </div>
                                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
