@accessByRoleTypeAdmin
    <a href="{{ route('backend.textblocks_text_block.edit', $textblock->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Textblock</a>
@endAccessByRoleTypeAdmin

<div class="container">
    <div class="jumbotron">
        <p>{!! $textblock->body !!}</p>
    </div>
</div>
