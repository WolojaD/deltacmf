@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<div class="text-center">
    <h1>{{ $structure->ultimate_h1_title }}</h1>

    @if($post->adaptive_image)
        <img src="{{ $post->adaptive_image }}" alt="{{ $post->title }}" title="{{ $post->title }}">
    @endif

    {!! $post->content !!}
    {!! $structure->ultimate_seo_text !!}
</div>



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
