@extends('layouts.app')

@section('before_styles')
<style>
a.disabled {
   pointer-events: none;
   cursor: default;
   color: #333333
}
a.checked {
   color: #abccba
}
</style>
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')
filter
products total - {{$products->total()}}
<ul>
<li>
    from {{floatval($filters['prices']['min'])}}
    <input type="text" name="price_min" value="{{floatval($filters['filtered_prices']['min_price'] ?? $filters['prices']['min'])}}">
    <input type="text" name="price_max" value="{{floatval($filters['filtered_prices']['max_price'] ?? $filters['prices']['max'])}}">
    to {{floatval($filters['prices']['max'])}}
</li>
<li>-------------</li>
@foreach($filters['brands'] as $brand)
<li><a href="{{$brand->getFullFilterPath($filters)}}" class="{{$brand->disabled .' '. $brand->checked}}">{{$brand->title}}</a>({{$brand->count}})</li>
@endforeach
@foreach($filters['parameters'] as $parameter) 
<li>-------------</li>
@foreach($parameter as $parameter_data) 
@if($loop->first)
<h3>{{$parameter_data->parameter_title}}</h3>
@endif
<li><a href="{{$parameter_data->getFullFilterPath($filters)}}" class="{{$parameter_data->disabled .' '. $parameter_data->checked}}">{{$parameter_data->title}}</a>({{$parameter_data->count}})</li>
@endforeach
@endforeach
</ul>
endfilter

<div class="text-center">
    <h1>{{ $structure->ultimate_h1_title }}</h1>
    <h1>{{ $category->title ?? ''}}</h1>
    <h1>{{ $structure->model }}</h1>
    <h1>{{ $structure->status }}</h1>
    categories

    @foreach($sub_categories as $sub_category)
        <p><a href="{{ $structure->url . '/' . $sub_category->slug }}">{{ $sub_category->title }}</a></p>
    @endforeach

    
    @if(optional($products))
    products
        @foreach($products as $product)
            <p><a href="{{ '/' . $product->slug }}">{{ $product->title }}</a></p>
        @endforeach
        {{$products->links()}}
    @endif
</div>


{!! $structure->ultimate_seo_text !!}
@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
