@extends('layouts.app')

@section('title')
Delta CMF
@stop

@section('description')
@stop

@section('before_styles')
@stop

@section('after_styles')
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<!-- Styles -->
<style>
.full-height,body,html{height:100vh}body,html{background-color:#fff;color:#636b6f;font-family:Raleway,sans-serif;font-weight:100;margin:0}.flex-center{align-items:center;display:flex;justify-content:center}.position-ref{position:relative}.top-right{position:absolute;right:10px;top:18px}.content{text-align:center}.title{font-size:84px}.dropbtn,.links>a{color:#636b6f;padding:0 25px;font-size:12px;font-weight:600;letter-spacing:.1rem;text-transform:uppercase;text-decoration:none}.m-b-md{margin-bottom:30px}.top-right-dropdown-pages{position:absolute;right:17%;top:18px}.top-right-dropdown-langs{position:absolute;right:9%;top:18px}.dropdown-content{display:none;position:absolute;background-color:#f1f1f1;min-width:160px;box-shadow:0 8px 16px 0 rgba(0,0,0,.2);z-index:1}.dropdown-content a{color:#000;padding:12px 16px;text-decoration:none;display:block}.dropdown-content a:hover{background-color:#ddd}.dropdown:hover .dropdown-content{display:block}.progress{background-color:#fff!important}.progress-bar{background-color:grey!important;color:white!important}
</style>
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
flex-center position-ref full-height
@stop

@section('before_content')
@stop

@section('content')



<div class="dropdown top-right-dropdown-pages">
    <a class="dropbtn">Pages</a>
    <div class="dropdown-content">
        @foreach($pages as $page)
            <a href="{{ url(LaravelLocalization::getCurrentLocale().'/'.$page->path) }}">{{ $page->title }}</a>
        @endforeach
    </div>
</div>
<div class="dropdown top-right-dropdown-langs">
    <a class="dropbtn">{{ LaravelLocalization::getCurrentLocaleName() }}</a>
    <div class="dropdown-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            <a rel="alternate" lang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}">{!! $properties['native'] !!}</a>
        @endforeach
    </div>
</div>
<div class="top-right links">
    <a href="{{ route('backend.dashboard.index') }}">Backend</a>
</div>
<div class="content">
    <div class="title m-b-md">
        Delta CMF
    </div>
    <div class="links">
        <a href="https://laravel.com/docs">Laravel</a>
        <a href="https://vuejs.org/v2/guide/">Vue</a>
        <a href="https://element.eleme.io/#/en-US/component/quickstart">Element</a>
        <a href="https://tailwindcss.com/docs/what-is-tailwind/">Tailwind</a>
        <a href="https://bitbucket.org/cfdigital/deltacmf/src/master/">Bitbucket</a>
    </div>
</div>



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
@stop
