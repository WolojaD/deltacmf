@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<div class="text-center">
    <h1>{{ $structure->ultimate_h1_title }}</h1>

    @foreach($category->children as $child)
        <p><a href="{{ $structure->url . '/' . $child->slug }}">{{ $child->title }}</a></p>
    @endforeach


    @foreach($posts as $post)
        <div>
            <a href="{{ $structure->url . '/' . $post->slug }}" title="{{ $post->title }}">
                <img alt="{{ $post->title }}" и title="{{ $post->title }}" src="/storage/i/news-image{{ $structure->preview_image }}">
            </a>
            <div>
                <a href="{{ $structure->url . '/' . $post->slug }}" title="{{ $post->title }}"><h3>{{ $post->title }}</h3></a>
                <p>{{ $category->title }}</p>
                <p><a href="{{ $structure->url . '/' . $post->slug }}" title="{{ $post->title }}">{{ $post->description }}</a></p>

                @if($category->show_date)
                    <p><a href="{{ $structure->url . '/' . $post->slug }}" title="{{ $post->title }}">{{ $post->date_from }}</a></p>
                @endif
            </div>
        </div>
    @endforeach

    {{ $posts->links() }}

    @if($structure->page)
        {!! $structure->page->body !!}
    @endif

    @if($structure->id ?? false)
        {!! $structure->ultimate_seo_text !!}
    @endif

</div>



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
