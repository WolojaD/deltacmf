@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')


@include('ecommerce::includes.comparison_button')

<div class="text-center">
    <h1>{{ $structure->ultimate_h1_title }}</h1>
    <h1>{{ $product->title }}</h1>
    <h1>{{ $structure->model }}</h1>
    <h1>{{ $structure->status }}</h1>
</div>
@include('ecommerce::suggestions')
@include('ecommerce::recent')

@if($structure->id ?? false)
    {!! $structure->ultimate_seo_text !!}
@endif



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
