@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<div class="text-center">
    <h1>{{ $structure->ultimate_h1_title }}</h1>
    <h1>{{ $structure->model }}</h1>
    <h1>{{ $structure->status }}</h1>

    @if($sales)
        @foreach($sales as $sale)
            <h2 style="margin: 50;"><a href="{{ $structure->path . '/' . $sale->slug }}">{{ $sale->title }}</a></h2>
        @endforeach
    @endif

    {!! $structure->page->body !!}
    {!! $structure->ultimate_seo_text !!}
</div>



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop