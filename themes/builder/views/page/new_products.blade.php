@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<div class="text-center">
    <h1>{{ $structure->ultimate_h1_title }}</h1>
    <h1>{{ $category->title ?? ''}}</h1>
    <h1>{{ $structure->model }}</h1>
    <h1>{{ $structure->status }}</h1>
    categories

    @foreach($categories as $category)
        <p><a href="{{ $structure->url . '/' . $filter_structure->url . '/' . $category->slug }}">{{ $category->title }}</a></p>
    @endforeach

    products

    @if($category)
        @foreach($category->new_products as $product)
            <p><a href="{{ '/' . $product->slug }}">{{ $product->title }}</a></p>
        @endforeach
    @endif

</div>


{!! $structure->ultimate_seo_text !!}
@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
