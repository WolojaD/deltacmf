@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<div class="text-center">
    <h1>Города</h1>
    @if($stores ?? false)
        @foreach($stores as $city)
            <h2>{{ $city->title }}</h2>
            @foreach($city->stores as $store)
                <p>-- name: {{ $store->title }}</p>
                <p>-- address: {{ $store->address }}</p>
                <p>-- latlng: {{ $store->latlng }}</p>
                <p>-- outlet : {{ $store->outlet }}</p>
                @if(count($store['days'] ?? []) > 0)
                    <p>-- рабочие часы</p>
                    @foreach($store['days'] as $day)

                        <p>{{ trans('frontend::stores.days.short.' . $day['days'][0]) . '--' . trans('frontend::stores.days.short.' . end($day['days'] )) }} ------ {{ $day['workhours'] ?? '' }}</p>
                    @endforeach
                @endif
                <br>

            @endforeach
        @endforeach
    @endif
</div>



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
