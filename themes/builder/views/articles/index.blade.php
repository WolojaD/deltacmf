@extends('layouts.app')

@section('title')
@stop

@section('description')
@stop

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



{!! $page->body !!}

@foreach($categories as $category)
    *<a href="{{ Request::url() . $category->path }}">Cat:{{ $category->title }}</a> <br>

    {{-- @foreach($category->subCategories as $sub_category)
        -<a href="{{ Request::url() . $sub_category->path }}">Cat:{{ $sub_category->title }}</a> <br>

        @foreach($sub_category->activePosts as $post)
            --<a href="{{ Request::url() . $sub_category->path . '/' . $post->slug }}">{{ $post->title }}</a> <br>
        @endforeach

    @endforeach --}}

    @foreach($category->activePosts as $post)
        <a href="{{ Request::url() . $category->path . '/' . $post->slug }}">{{ $post->title }}</a> <br>
    @endforeach

@endforeach



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
