@extends('layouts.app')

@section('title')
@stop

@section('description')
@stop

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<a href="/{{ preg_replace('/\/([-a-zA-Z0-9]+)$/', '', request()->path()) }}">back</a>

{!! $post->content !!}



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
