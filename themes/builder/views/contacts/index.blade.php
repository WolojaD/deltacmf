<div class="container">

    @accessByRoleTypeAdmin
        <a href="{{ route('backend.contacts_group.show', $contacts_group->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Contacts</a>
    @endAccessByRoleTypeAdmin

    @if($contacts_group->show_title)
        <h2>{{ $contacts_group->title ?? '' }}</h2>
    @endif

    @if($contacts_group->contacts)
        @if(!is_null($contacts_group->frame) && $contacts_group->show_before_text == 1)
            {!! $contacts_group->frame !!}
        @endif

        @foreach($contacts_group->contacts as $contact)
            <div class="{{ $contact->class ?? '' }}">

                @isFileExists($contact->image)
                    <div>
                        <img src="{{ asset('storage/origin' . $contact->image) }}" alt="">
                    </div>
                @endIsFileExists

                <div>
                    @if($contact->show_title)
                        <h3>{{ $contact->title ?? '' }}</h3>
                    @endif

                    @include('contacts::fields.' . $contact->field_type, $contact)
                </div>
            </div>
        @endforeach

        @if(!is_null($contacts_group->frame) && $contacts_group->show_before_text == 0)
            {!! $contacts_group->frame !!}
        @endif
    @endif

</div>
