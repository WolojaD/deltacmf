@php($emails = explode(',', $contact->body))

@foreach ($emails as $email)
    @if(filter_var($email, FILTER_VALIDATE_EMAIL))
        <a href="mailto:{{ $email }}">{{ $email }}</a>
    @endif
@endforeach
