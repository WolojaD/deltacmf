@if($form && $form->fields->count())
    <div class="container">

        @accessByRoleTypeAdmin
            <a href="{{ route('backend.forms_form.edit', $form->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Form</a>
            <a href="{{ route('backend.forms_form.show', $form->id) }}" target="_blank" class="admin-edit-shortcode-block-button">View Form Fields</a>
        @endAccessByRoleTypeAdmin

        {{-- Название формы --}}
        @if($form->show_title)
            <h2>{{ $form->title }}</h2>
        @endif

        {{-- Краткое описание формы --}}
        @if(null !== $form->description)
            <h4>{{ $form->description }}</h4>
        @endif

        {{-- Проверка можно ли загружать файлы в этой форме --}}
        @if($form->allow_files)
            {!! Form::open(['route' => ['frontend.form.post', $form->id], 'method' => 'POST', 'id' => 'form_' . $form->id, 'class' => 'form', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
        @else
            {!! Form::open(['route' => ['frontend.form.post', $form->id], 'method' => 'POST', 'id' => 'form_' . $form->id, 'class' => 'form', 'files' => false]) !!}
        @endif

            @if($form->redirect)
                <input name="_redirect" type="hidden" value="{{ $form->redirect }}">
            @endif

                {{-- Вывод полей формы по их типу и передача в них параметров поля --}}
                @foreach($form->fields as $field)
                    @include('forms::fields.' . $field->field_type, $field)
                @endforeach

                {{-- Кнопка отправки формы --}}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" onclick="sendForm(this)" class="btn btn-default">{{ $form->button_title ?? trans('frontend::forms.default button send title') }}</button>
                    </div>
                </div>

            {!! Form::close() !!}

            {{-- Вывод сообщения после успешной отправки формы (если оно есть) --}}
            @if(null !== $form->message && isset($form->message))
                <p class="form_sucsess_message">{!! $form->message !!}</p>
            @endif

            {{-- Вывод сообщения о персональных данных (если разрешен показ) --}}
            @if($form->show_personal_data_message)
                <input type="checkbox" id="show_personal_data_message" name="show_personal_data_message" />
                <p class="form_personal_data_message">Вывод сообщения о персональных данных</p>
            @endif

    </div>
@endif

<style type="text/css">
    .form_sucsess_message {
        display: none;
    }
    .active {
        display: block !important;
    }
</style>
