@component('forms::mail.html.message')
    Контактная форма<br><br><br>

    @foreach($data as $field_key => $field_value)
        {{ $fieldsNiceNames[$field_key] }} - {{ $field_value }}
    @endforeach

@endcomponent
