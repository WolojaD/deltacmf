<div class="form-group">
    <label class="control-label col-sm-2" for="number">{{ $field->title }}:</label>

    <div class="col-sm-10">
        <input type="number"
                step="any"
                class="form-control {{ $field->class ?? '' }}"
                id="{{ $field->unique_code . '_' . $field->id }}"
                placeholder="{{ $field->placeholder }}"
                name="{{ $field->unique_code }}"
                @if(null !== $field->regex)
                data-regex="{{ $field->regex->regex_frontend }}"
                @endif
                @if($field->default)
                value="{{ $field->default }}"
                @endif
                {{ $field->required ? 'required' : '' }}
                onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 || event.charCode == 0"/>

        @if(null !== $field->comment)
            <small class="text-muted">{{ $field->comment }}</small>
        @endif

    </div>

</div>
