<div class="form-group">
    <label class="control-label col-sm-2" for="checkbox">{{ $field->title }}:</label>

    <div class="col-sm-10">
        <input type="checkbox"
                class="form-control {{ $field->class ?? '' }}"
                id="{{ $field->unique_code . '_' . $field->id }}"
                name="{{ $field->unique_code }}"
                @if($field->default)
                value="{{ $field->default }}"
                @else
                value="true"
                @endif
                {{ $field->required ? 'required' : '' }}/>

        @if(null !== $field->comment)
            <small class="text-muted">{{ $field->comment }}</small>
        @endif

    </div>

</div>
