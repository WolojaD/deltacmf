<div class="form-group">
    <label class="control-label col-sm-2" for="textarea">{{ $field->title }}:</label>

    <div class="col-sm-10">
        <textarea rows="4"
                class="form-control {{ $field->class ?? '' }}"
                id="{{ $field->unique_code . '_' . $field->id }}"
                placeholder="{{ $field->placeholder ?? '' }}"
                name="{{ $field->unique_code }}"
                @if(null !== $field->regex)
                data-regex="{{ $field->regex->regex_frontend }}"
                @endif
                {{ $field->required ? 'required' : '' }}>@if($field->default){{ $field->default }}@endif</textarea>

        @if(null !== $field->comment)
            <small class="text-muted">{{ $field->comment }}</small>
        @endif

    </div>

</div>
