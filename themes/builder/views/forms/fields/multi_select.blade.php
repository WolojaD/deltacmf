@if($field->field_data->count())
    <div class="form-group">
        <label class="control-label col-sm-2" for="checkbox">{{ $field->title }}:</label>

        <div class="col-sm-10 {{ $field->class ?? '' }}">

            @foreach($field->field_data as $data)
                <div class="checkbox">

                    <label>
                        <input id="{{ $field->unique_code . '_' . $data->id }}"
                                name="{{ $field->unique_code }}[{{ $data->title }}]"
                                type="checkbox"
                                value="true">
                        {{ $data->title }}
                    </label>

                </div>
            @endforeach

            @if(null !== $field->comment)
                <small class="text-muted">{{ $field->comment }}</small>
            @endif

        </div>

    </div>
@endif
