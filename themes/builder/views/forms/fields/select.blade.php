@if($field->field_data->count())
    <div class="form-group">
        <label class="control-label col-sm-2" for="text">{{ $field->title }}:</label>

        <div class="col-sm-10">

            <select class="form-control {{ $field->class ?? '' }}"
                    id="{{ $field->unique_code . '_' . $field->id }}"
                    name="{{ $field->unique_code }}"
                    {{ $field->required ? 'required' : '' }}>

                @foreach($field->field_data as $data)
                    <option value="{{ $data->title }}">{{ $data->title }}</option>
                @endforeach

            </select>

            @if(null !== $field->comment)
                <small class="text-muted">{{ $field->comment }}</small>
            @endif

        </div>

    </div>
@endif
