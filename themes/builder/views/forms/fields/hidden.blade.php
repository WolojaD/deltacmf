<div class="form-group">

    <input type="hidden"
            name="{{ $field->unique_code }}"
            id="{{ $field->unique_code . '_' . $field->id }}"
            @if($field->default)
            value="{{ $field->default }}"
            @endif
            />

</div>
