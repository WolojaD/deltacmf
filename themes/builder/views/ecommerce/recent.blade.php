@if(isset($recent) && $recent->count()) 
<h2>Просмотренные</h2>
<div class="product-carousel__wrap">
    @foreach($recent as $item)
    <div class="product" >

        <a href="/{{$item->slug}}" class="product__img" >
            {{-- <img src="/storage/origin{{$item->gallery->images->nth(2)->first()->path ?? ''}}" alt="alt" style="max-height:250px;"> --}}
            <img src="/storage/origin{{$item->gallery->images->first()->path ?? ''}}" alt="alt" style="max-height:250px;">
        </a>

        <div class="product__content">
            <a href="/{{$item->slug}}" class="product__title">{{$item->title}}</a>
            
            <div class="product__cost">
                <p class="product__old-price">{{$item->minimum_price}} uah</p>
                <p class="product__price">{{$item->discounted_price}} uah</p>
            </div>
        </div>
    </div>
@endforeach
</div>
@endif