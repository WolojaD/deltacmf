@extends('layouts.app')

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@include('includes.open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
@stop

@section('before_content')
@stop

@section('content')



<div class="text-center">
    @if(optional($products)->count())
    products
    @foreach($products as $product)
        {{$product->title}}
    @endforeach
    {{$products->appends(request()->only('search'))->links()}}
    @endif
</div>
@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
