<div class="search__form">
    <div class="container">
        <div class="search__field">
            <input type="text" placeholder="Search" name="search" id="products_search">
            <button class="search__submit" onclick="makeSearch('products_search')">search</button>
        </div>
    </div>
</div>