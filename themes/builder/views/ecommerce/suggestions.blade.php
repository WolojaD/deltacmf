@if(isset($suggestions) && $suggestions->count()) 
<h2>Предложения</h2>
<div class="product-carousel__wrap">
    @foreach($suggestions as $suggestion)
    <div class="product" >

        <a href="/{{$suggestion->slug}}" class="product__img" >
            {{-- <img src="/storage/origin{{$suggestion->gallery->images->nth(2)->first()->path ?? ''}}" alt="alt" style="max-height:250px;"> --}}
            <img src="/storage/origin{{$suggestion->gallery->images->first()->path ?? ''}}" alt="alt" style="max-height:250px;">
        </a>

        <div class="product__content">
            <a href="/{{$suggestion->slug}}" class="product__title">{{$suggestion->title}}</a>
            
            <div class="product__cost">
                <p class="product__old-price">{{$suggestion->minimum_price}} uah</p>
                <p class="product__price">{{$suggestion->discounted_price}} uah</p>
            </div>
        </div>
    </div>
@endforeach
</div>
@endif