@if($banners)

    @foreach($banners as $banner)
        @isFileExists($banner->translatable_desktop_image)

        @accessByRoleTypeAdmin
            <a href="{{ route('backend.banners_banner.edit', $banner->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit banner</a>
        @endAccessByRoleTypeAdmin

            <div class="jumbotron">
                <div class="container">
                    <div class="media">

                        <div class="media-left media-middle">
                            <img onload="setTimeout(function() { sendStatistics({{ $banner->id }}) }, 1000);" class="media-object" src="{{ asset('storage/i/banners-image' . $banner->adaptive_translatable_image) }}" alt="{{ $banner->translatable_desktop_image_alt ?? '' }}">
                        </div>

                        <div class="media-body">
                            <h4 class="media-heading" @if($banner->title_color)style="color:{{ $banner->title_color }}"@endif>{{ $banner->title }}</h4>
                            <p @if($banner->description_color)style="color:{{ $banner->description_color }}"@endif>{{ nl2br($banner->description) ?? '' }}</p>
                        </div>

                        <div class="media-right media-middle">
                            <p>
                                <a class="btn btn-lg btn-primary"
                                    @if($banner->blank)target="_blank"@endif
                                    href="{{ $banner->url }}"
                                    id="{{ $banner->id }}"
                                    onclick="sendStatistics({{ $banner->id }}, 1)"
                                    role="button">
                                    {{ $banner->button_title ?? trans('frontend::banners.default button title') }}
                                </a>
                            </p>
                        </div>

                    </div>
                </div>
            </div>

        @endIsFileExists
    @endforeach

@endif
