@php($hasChild = count($menu_item->childs) > 0)
@php($hasProduct = $menu_item->total_product_count > 0)

@if($hasProduct || $menu_item->product_count_in_category > 0)
    <li>
        <a
            @if(trim(LaravelLocalization::getLocalizedURL(locale(), $menu_item->structure->url), '/') != request()->url())
                href="{{ LaravelLocalization::getLocalizedURL(locale(),  $menu_item->structure->url) }}"
            @endif
        >
            {{ $menu_item->menu_title ?? $menu_item->title }}
            @if($hasChild) <span class="caret"></span> @endif
        </a>

        @if ($hasChild)
            <ul class="dropdown-toggle">

                @foreach($menu_item->childs as $menu_item)
                    @include('includes.catalog_navigation', ['menu_item' => $menu_item])
                @endforeach

            </ul>
        @endif

    </li>
@endif
