@php($hasChild = count($menu_item->childs) > 0)

<li class="@if($hasChild) dropdown @endif">
    <a
        class="@if(isset($page) && $menu_item->slug == $page->slug) active @endif dropdown-toggle"

        @if(trim(LaravelLocalization::getLocalizedURL(locale(), $menu_item->url), '/') != request()->url())
            href="{{ LaravelLocalization::getLocalizedURL(locale(), $menu_item->url) }}"
        @endif

        @if($hasChild)
            data-toggle="dropdown"
            role="button"
            aria-haspopup="true"
            aria-expanded="false"
        @endif
    >
        {{ $menu_item->menu_title ?? $menu_item->title }}

        @if($hasChild) <span class="caret"></span> @endif
    </a>

    @if ($hasChild)
        <ul class="dropdown-menu">
            @foreach($menu_item->childs as $menu_item)
                @include('includes.navigation_item', ['menu_item' => $menu_item])
            @endforeach
        </ul>
    @endif
</li>
