<nav class="navbar navbar-default navbar-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" @if (\Route::currentRouteName() == 'homepage') href="{{ url('/' . locale()) }}" @endif >Project Delta</a>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

                @if (count($navigation['main_menu']) > 0)
                    @foreach ($navigation['main_menu'] as $menu_item)
                        @include('includes.navigation_item', $menu_item)
                    @endforeach
                @endif

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ LaravelLocalization::getCurrentLocaleNative() }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a rel="alternate" lang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}">{!! $properties['native'] !!}</a>
                            </li>
                        @endforeach

                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
