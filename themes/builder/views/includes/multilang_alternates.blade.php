@foreach(get_application_frontend_locales() as $loc_index => $locale)
    @if($loc_index !== locale())
        <link rel="alternate" href="{{ LaravelLocalization::getLocalizedURL($loc_index) }}" hreflang="{{ $loc_index }}" />
    @endif
@endforeach
    <link rel="alternate" href="{{ LaravelLocalization::getLocalizedURL() }}" hreflang="{{ locale() }}" />
