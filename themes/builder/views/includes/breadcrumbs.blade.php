@if(isset($breadcrumbs) && $structure->parent_id > 0)
    <ol class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">

        @foreach($breadcrumbs as $breadcrumb)
            <li typeof="v:Breadcrumb">
                <a href="{{ $breadcrumb['url'] == $structure->url ? '#' : $breadcrumb['url'] }}"
                    class="{{ $breadcrumb['url'] == $structure->url ? 'active' : '' }}"
                    title="{{ $breadcrumb['title'] }}"
                    rel="v:url"
                    property="v:title">{{ $breadcrumb['title'] }}</a>
            </li>
        @endforeach

    </ol>
@endif
