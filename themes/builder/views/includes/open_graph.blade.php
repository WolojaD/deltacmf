@if($structure ?? false)
    <meta property="og:title" content="{{ $structure->ultimate_og_title }}" />
    <meta property="og:description" content="{{ $structure->ultimate_og_description }}" />
    <meta property="og:type" content="{{ $structure->og_type ?? 'website' }}" />
    <meta property="og:locale" content="{{ locale() }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    @if($structure->og_image ?? false)
    <meta property="og:image" content="{{ url('/storage/origin/' . $structure->og_image) }}" />
    @endif
@endif
