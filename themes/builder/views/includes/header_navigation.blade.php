@php($hasChild = count($menu_item->childs) > 0)
<li>
    <a
            class="@if(isset($page) && $menu_item->slug == $page->slug) active @endif"
            @if(trim(LaravelLocalization::getLocalizedURL(locale(), $menu_item->structure->url), '/') != request()->url())
            href="{{ LaravelLocalization::getLocalizedURL(locale(),  $menu_item->structure->url) }}"
            @endif
    >
        {{ $menu_item->menu_title ?? $menu_item->title }}
        @if($hasChild) <span class="caret"></span> @endif
    </a>

    @if ($hasChild)
        <ul>
            @foreach($menu_item->childs as $menu_item)
                @include('includes.header_navigation', ['menu_item' => $menu_item])
            @endforeach
        </ul>
    @endif
</li>
