<footer class="footer">

    @if(count($socialnetworks ?? []) > 0)
        <div>

            @foreach($socialnetworks as $socialnetwork)
                <a href="{{ $socialnetwork->link }}" rel="nofollow" target="_blank" class="{{ $socialnetwork->class ?? '' }}">

                    @isFileExists($socialnetwork->image)
                        <img src="{{ asset('storage/origin' . $socialnetwork->image) }}" alt="{{ $socialnetwork->title }}">
                    @elseIsFileExists
                        {{ $socialnetwork->title }}
                    @endIsFileExists

                </a>
            @endforeach

        </div>
    @endif

    @if(count($navigation['footer_menu']) > 0)
        <ul>

            @foreach ($navigation['footer_menu'] as $menu_item)
                @include('includes.navigation_item', ['menu_item' => $menu_item])
            @endforeach

        </ul>
    @endif

    @include('newsletters.subscribe_form')

</footer>
