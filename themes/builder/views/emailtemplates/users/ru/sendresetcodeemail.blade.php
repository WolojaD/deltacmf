<p>Reset password</p>

<p>To reset password complete this form.</p>

<p><a href="{{url('/')}}/auth/reset/{{$user_id ?? ''}}/{{$code ?? ''}}">Reset password</a></p>