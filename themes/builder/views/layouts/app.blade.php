<!DOCTYPE html>
<html lang="{{ locale() }}">
    <head>
        @if(($structure->index_follow_tag ?? false) && (!$paginated || settings('seo::index_pagination')))
            {!! $structure->index_follow_tag !!}
        @else
            <meta name="robots" content="noindex, nofollow" />
        @endif

        <base src="{{ url('/') }}">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1, shrink-to-fit=no">

        <title>@yield('title', $structure->ultimate_meta_title ?? '')</title>
        <meta name="description" content="@yield('description', $structure->ultimate_meta_description ?? '')">

        @if(Route::currentRouteName() != 'homepage')
            <link rel="canonical" href="{{ locale_url($paginated ? ($structure->canonical ?? request()->path()) : request()->path()) }}"/>
        @endif

        @if(isset($paginates) && !settings('seo::canonical_pagination'))
            @foreach($paginates as $type => $paginate)
                @if($paginate)
                    <link rel="{{ $type }}" href="{{ $paginate }}"/>
                @endif
            @endforeach
        @endif

        @include('includes.multilang_alternates')

        <!-- Favicon -->
        {{-- <link rel="icon" type="image/x-icon" href="{{ asset('themes/application_frontend/image/favicon/favicon.ico') }}"> --}}

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Open Graph -->
        @yield('open_graph')

        <!-- CSS -->
        @yield('before_styles')
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        @yield('after_styles')

        <!-- Analytic script head -->
        {!! $analytic_scripts['analytic_script_head'] !!}
    </head>
    <body class="@yield('body_class')">
        <!-- Analytic script body begin -->
        {!! $analytic_scripts['analytic_script_body_begin'] !!}

        @include('includes.developer_bar')

        @include('includes.navigation')
        @include('ecommerce.includes.search_bar')

        <div class="container @yield('wrapper_class')">

            @include('includes.header')

            @if(isset($catalog_menu) && count($catalog_menu) > 0)
                <h3 class="text-center">Catalog navigation</h3>

                <ul class="nav nav-pills">

                    @foreach ($catalog_menu as $menu_item)
                        @include('includes.catalog_navigation', ['menu_item' => $menu_item])
                    @endforeach

                    @if (Route::currentRouteName() != 'homepage' && count($navigation['header_menu'] ?? []) > 0)
                        @foreach ($navigation['header_menu'] as $menu_item)
                            @include('includes.header_navigation',  ['menu_item' => $menu_item])
                        @endforeach
                    @endif

                    @if(Route::currentRouteName() != 'homepage' && isset($header_menu) && count($header_menu) > 0)
                        @foreach ($header_menu as $menu_item)
                            @include('includes.header_navigation', ['menu_item' => $menu_item])
                        @endforeach
                    @endif
                </ul>
            @endif



            @include('includes.breadcrumbs')

            @yield('before_content')

            @yield('content')

            @yield('after_content')

            @include('includes.footer')

        </div>

        @include('includes.svg')

        <!-- JavaScript -->
        @yield('before_scripts')
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        @stack('scripts')

        {{-- FORMS module --}}
        <script type="text/javascript">
            function sendForm(element) {
                $('.error').remove();

                var $Form = $(element.form);
                var formData = $Form.serializeFiles();

                $.ajax({
                    url: $Form.attr('action'),
                    type: 'POST',
                    data: formData,
                    credentials: 'same-origin',
                    mode: 'no-cors',
                    cache: false,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-Token': $Form.find('[name="_token"]').val()
                    },
                    success: function (response) {
                        $('.form_sucsess_message').addClass('active');

                        setTimeout(function () {
                            $Form.trigger("reset").removeClass('active');
                            $('.form_sucsess_message').removeClass('active');
                        }, 4000);

                        if (typeof $Form.find('[name="_redirect"]').val() == 'string') {
                            document.location.assign($Form.find('[name="_redirect"]').val());
                        }
                    },
                    error: function(response) {
                        if (response.responseJSON.error) {
                            $.each(response.responseJSON.msg, function(index, value) {
                                $Form[0].elements[index].insertAdjacentHTML('afterEnd', "<li class=\"error\">" + value + "</li>");
                            });
                        }
                    }
                });
            }
            $.fn.serializeFiles = function() {
                var obj = $(this);
                var formData = new FormData();

                $.each($(obj).find("input[type='file']"), function(i, tag) {
                    $.each($(tag)[0].files, function(i, file) {
                        formData.append(tag.name, file);
                    });
                });

                var params = $(obj).serializeArray();

                $.each(params, function (i, val) {
                    formData.append(val.name, val.value);
                });

                return formData;
            };
        </script>

        {{-- BANNERS module --}}
        <script type="text/javascript">
            function sendStatistics(bannerId, isClick) {
                if (typeof isClick == undefined || typeof isClick == 'undefined') {
                    isClick = null;
                }

                var statisticsUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.hostname + '/banners-statistics-request';

                $.ajax({
                    url: statisticsUrl,
                    type: 'GET',
                    data: {
                        id: bannerId,
                        click: isClick
                    },
                    mode: 'no-cors',
                    cache: false,
                    async: true,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        // console.log(response);
                    },
                    error: function(response) {
                        console.log(response);
                    }
                });
            }
        </script>

        {{-- ECOMMERCE module --}}
        <script type="text/javascript">
            function addToComparison (id) {
                const url = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.hostname + '/comparison';
                $.ajax({
                    url,
                    type: 'POST',
                    data: {id: id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        console.log('success:', response);
                    },
                    error: function(response) {
                        console.log('error:', response);
                    }
                });
            }

            function makeSearch (id) {
                const url = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.hostname + '/search';
                const search = $('#products_search').val();
                console.log('object :',  $('#products_search'));
                if(search.length < 3) {
                    return false;
                }

                $.ajax({
                    url,
                    type: 'GET',
                    data: {
                        search
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        console.log('success:', response);
                    },
                    error: function(response) {
                        console.log('error:', response);
                    }
                });
            }
        </script>

        {{-- NEWSLETTERS module --}}
        <script type="text/javascript">
            $(function() {
                $('#subscribe-btn').on('click', function (event) {
                    event.preventDefault();
                    var $this = $(this);
                    var $form = $this.closest('form');
                    sendSubscribeForm($form);
                })
            });

            function sendSubscribeForm(data) {
                $('.error').remove();

                var email = data.find('[name="email"]').val();

                if (email.length) {
                    $.ajax({
                        url: data.attr('action'),
                        type: 'POST',
                        data: {
                            email : data.find('[name="email"]').val()
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            alert('OK');
                        },
                        error: function(response) {
                            if (response.responseJSON.error) {
                                $.each(response.responseJSON.msg, function(index, value) {
                                    data[0].elements[index].insertAdjacentHTML('afterEnd', "<p class=\"error\">" + value + "</p>");
                                });
                            }
                        }
                    });
                }
            }
        </script>

        @yield('after_scripts')

        <!-- Analytic script body end -->
        {!! $analytic_scripts['analytic_script_body_end'] !!}
    </body>
</html>
