@if($slider)
    <div class="container">

        @accessByRoleTypeAdmin
            <a href="{{ route('backend.slider_slider.show', $slider->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Slider</a>
        @endAccessByRoleTypeAdmin

        <h2>{{ $slider->title }}</h2>

        <div id="slider_{{ $slider->id }}"
            @if($slider->autoscroll)
                data-ride="carousel"
                @if($slider->delay)
                    data-interval="{{ $slider->delay * 1000 }}"
                @endif
            @endif
            class="carousel slide">

            <ol class="carousel-indicators">

                @foreach($slider->slides as $slide)
                    <li data-target="#slider_{{ $slider->id }}" data-slide-to="{{ $loop->index }}" class="@if($loop->index == 0) active @endif"></li>
                @endforeach

            </ol>

            <div class="carousel-inner">

                @foreach($slider->slides as $slide)
                    <div class="item @if($loop->index == 0) active @endif">

                        @if($slide->url)
                            <a href="{{ $slide->url }}" target={{ $slide->target_blank ? '_blank' : '_self' }}>
                        @endif
                        <img src="/storage/i/slider-image{{ $slide->adaptive_image }}" alt="{{ $slide->title ?? '' }}" style="width:100%;">

                        <div class="carousel-caption">
                            <h2>{{ $slide->title ?? '' }}</h2>
                            <h3>{{ $slide->sub_title ?? '' }}</h3>
                            <p>{{ $slide->description ?? '' }}</p>
                        </div>

                        @if($slide->url)
                            </a>
                        @endif

                    </div>
                @endforeach

            </div>

            <a class="left carousel-control" href="#slider_{{ $slider->id }}" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#slider_{{ $slider->id }}" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endif
