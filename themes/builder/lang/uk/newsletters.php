<?php

return [
    'form rules' => [
        'errors' => [
            'email email' => 'Поле E-Mail адрес должно быть действительным электронным адресом.',
            'email unique' => 'Вы уже подписаны',
        ],
    ],
];
