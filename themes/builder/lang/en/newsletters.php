<?php

return [
    'form rules' => [
        'errors' => [
            'email email' => 'The e-mail address must be a valid email address.',
            'email unique' => 'You are already subscribed',
        ],
    ],
];