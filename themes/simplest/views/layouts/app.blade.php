<!DOCTYPE html>
<html lang="{{ locale() }}">
    <head>
        @if($structure->index_follow_tag ?? false)
            {!! $structure->index_follow_tag !!}
        @endif

        <base src="{{ url('/') }}">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1, shrink-to-fit=no">

        <title>@yield('title', $structure->ultimate_meta_title)</title>
        <meta name="description" content="@yield('description', $structure->ultimate_meta_description)">

        @if(Route::currentRouteName() != 'homepage')
            <link rel="canonical" href="{{ url($structure->ultimate_canonical) }}"/>
        @endif

        @if(isset($paginates))
            @foreach($paginates as $type => $paginate)
                @if($paginate)
                    <link rel="{{ $type }}" href="{{ $paginate }}"/>
                @endif
            @endforeach
        @endif

        @include('includes.multilang_alternates')

        <!-- Favicon -->
        {{-- <link rel="icon" type="image/x-icon" href="{{ asset('themes/simplest/image/favicon/favicon.ico') }}"> --}}

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Open Graph -->
        @yield('open_graph')

        <!-- CSS -->
        @yield('before_styles')
        {{-- EXAMPLE --}}
        {{-- <link rel="stylesheet" href="{{ asset(mix('themes/simplest/css/style.min.css')) }}"> --}}
        @yield('after_styles')

        <!-- Analytic script head -->
        {!! $analytic_scripts['analytic_script_head'] !!}
    </head>
    <body class="@yield('body_class')">
        <!-- Analytic script body begin -->
        {!! $analytic_scripts['analytic_script_body_begin'] !!}

        @include('includes.developer_bar')

        <div class="@yield('wrapper_class')">

            @include('includes.header')

            @yield('before_content')

            @yield('content')

            @yield('after_content')

            @include('includes.footer')

        </div>

        @include('includes.svg')

        <!-- JavaScript -->
        @yield('before_scripts')
        {{-- EXAMPLE --}}
        {{-- <script type="text/javascript" src="{{ asset(mix('themes/simplest/js/script.min.js')) }}"></script> --}}
        @yield('after_scripts')

        <!-- Analytic script body end -->
        {!! $analytic_scripts['analytic_script_body_end'] !!}
    </body>
</html>
