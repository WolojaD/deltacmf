@if($slider)

    @accessByRoleTypeAdmin
        <a href="{{ route('backend.slider_slider.show', $slider->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Slider</a>
    @endAccessByRoleTypeAdmin

    <h2>{{ $slider->title }}</h2>

    <div id="slider_{{ $slider->id }}">

        @foreach($slider->slides as $slide)

            @if($slide->url)
                <a href="{{ $slide->url }}" target={{ $slide->target_blank ? '_blank' : '_self' }}>
            @endif

                <img src="/storage/i/slider-image{{ $slide->desktop_image }}" alt="{{ $slide->title ?? '' }}"">

                <h2>{{ $slide->title ?? '' }}</h2>
                <h3>{{ $slide->sub_title ?? '' }}</h3>
                <p>{{ $slide->description ?? '' }}</p>

            @if($slide->url)
                </a>
            @endif

        @endforeach

    </div>
@endif
