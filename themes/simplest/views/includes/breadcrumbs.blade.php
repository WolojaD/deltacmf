@if($breadcrumbs && $structure->parent_id > 0)
    <div class="breacrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">

        @foreach($breadcrumbs as $breadcrumb)
            <span typeof="v:Breadcrumb">
                <a href="{{ $breadcrumb['url'] == $structure->url ? '#' : $breadcrumb['url'] }}" title="{{ $breadcrumb['title'] }}" rel="v:url" property="v:title">{{ $breadcrumb['title'] }}</a>
            </span>
        @endforeach

    </div>
@endif
