<style>
    .admin-burger {
        display: none
    }

    .admin-bar-wrapper {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        background: #231f20;
        z-index: 9999;
        height: 40px;
        display: flex;
        justify-content: space-between;
        padding: 0 20px
    }

    .admin-bar-wrapper + * {
        margin-top: 40px
    }

    .admin-bar {
        display: flex;
        flex-flow: row nowrap;
        justify-content: space-between;
        width: 100%
    }

    .admin-actions, .admin-logo {
        align-items: center;
        display: flex
    }

    .admin-logo {
        font-size: 1.45vw;
        color: #fff
    }

    .admin-logo svg {
        height: 100%;
        display: flex;
        width: 135px
    }

    .admin-actions a {
        font-size: 13px;
        color: #fff;
        display: flex;
        align-items: center
    }

    .admin-actions svg {
        width: 20px;
        height: 18px;
        margin-right: 5px
    }

    .admin-actions svg, .admin-actions svg use {
        color: #fff
    }

    .admin-actions a + a {
        margin-left: 25px
    }

    @media (max-width: 1200px) {
        .admin-logo {
            font-size: 18px
        }
    }

    @media (max-width: 1000px) {
        .admin-burger {
            display: flex;
            width: 36px;
            background: rgba(255, 255, 255, 0);
            border: none;
            height: 40px;
            padding: 10px 0;
        }

        .admin-burger svg {
            width: 100%;
            height: 20px;
        }

        .admin-burger rect {
            fill: #fff
        }

        .admin-actions {
            position: absolute;
            top: 100%;
            left: 0;
            right: 0;
            background: #231f1f;
            padding: 10px 20px;
            justify-content: space-between;
            box-shadow: 0 1px 0 0 #fff inset;
            pointer-events: none;
            transform: translate(0, -20px);
            opacity: 0;
            transition: all .2s linear
        }

        .admin-actions.show-admin-bar {
            pointer-events: all;
            transform: translate(0, 0);
            opacity: 1;
            transition: all .2s linear
        }
    }

    @media (max-width: 800px) {
        .admin-actions {
            flex-flow: row wrap
        }

        .admin-actions a, .admin-actions a + a {
            width: 100%;
            margin: 0;
            padding: 5px 0
        }
    }
</style>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector('.admin-burger').addEventListener('click', function () {
            document.querySelector('.admin-actions').classList.toggle('show-admin-bar');
        });
    });
</script>

@accessByRoleTypeAdmin

    <nav class="admin-bar-wrapper">

        <div class="admin-bar">

            <div class="admin-logo">
                <svg class="svg-logo"><use xlink:href="#svg-logo"></use></svg>
            </div>

            <div class="admin-actions">

                @impersonating
                    <a href="{{ route('impersonate.leave') }}">
                        <svg class="button-icon arrow-back"><use xlink:href="#arrow-back" class="arrow-back"></use></svg>
                        {{ Session::get('backend_locale') == 'ru' ? 'Выйти из пользователя' : 'Leave impersonation' }}
                    </a>
                @endImpersonating

                @if(isset($structure->page))
                    <a href="{{ $structure->page->getEditUrl(Session::get('backend_locale')) }}">
                        <svg class="svg-edit"><use xlink:href="#svg-edit"></use></svg>
                        {{ Session::get('backend_locale') == 'ru' ? 'Отредактировать страницу' : 'Edit page' }}
                    </a>
                @endif

                @if(isset($structure) && $structure->object_id)
                    <a href="{{ $structure->getEditUrl(Session::get('backend_locale')) }}">
                        <svg class="svg-edit"><use xlink:href="#svg-edit"></use></svg>
                        @lang('core::developer_bar.edit.' . $structure->page_template, [], Session::get('backend_locale'))
                    </a>
                @endif

                @if(isset($structure))
                    <a href="{{ $structure->getSelfEditUrl(Session::get('backend_locale')) }}">
                        <svg class="svg-edit"><use xlink:href="#svg-edit"></use></svg>
                        @lang('core::developer_bar.edit.structure', [], Session::get('backend_locale'))
                    </a>
                @endif

                <a href="{{ LaravelLocalization::getLocalizedURL(Session::get('backend_locale'), route('backend.dashboard.redirectIntended')) }}">
                    <svg class="svg-back"><use xlink:href="#svg-back"></use></svg>
                    {{ Session::get('backend_locale') == 'ru' ? 'Вернуться к админ-панели' : 'Back to backend' }}
                </a>

                <a>
                    <svg class="svg-user"><use xlink:href="#svg-user"></use></svg>
                    {{ $user->email }}
                </a>

                <a href="{{ route('logout') }}">
                    <svg class="svg-logout"><use xlink:href="#svg-logout"></use></svg>
                    {{ Session::get('backend_locale') == 'ru' ? 'Выйти' : 'Logout' }}
                </a>

            </div>

            <button class="admin-burger">
                <svg id="mob-menu" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 20">
                    <rect x="0" y="0"  width="3" height="3"></rect>
                    <rect x="0" y="8.5"  width="3" height="3"></rect>
                    <rect x="0" y="17" width="3" height="3"></rect>
                    <rect x="5"         width="19" height="3"></rect>
                    <rect x="5" y="8.5"   width="19" height="3"></rect>
                    <rect x="5" y="17"  width="19" height="3"></rect>
                </svg>
            </button>
        </div>
    </nav>

@elseAccessByRoleTypeAdmin

    @impersonating
        <nav class="admin-bar-wrapper">
            <div class="admin-bar">

                <div class="admin-logo">
                    <svg class="svg-logo"><use xlink:href="#svg-logo"></use></svg>
                </div>

                <div class="admin-actions">
                    <a href="{{ route('impersonate.leave') }}">
                        <svg class="button-icon arrow-back"><use xlink:href="#svg-back" class="svg-back"></use></svg>
                        {{ Session::get('backend_locale') == 'ru' ? 'Выйти из пользователя' : 'Leave impersonation' }}
                    </a>
                </div>

            </div>
        </nav>
    @endImpersonating

@endAccessByRoleTypeAdmin
