@php($phones = explode(',', $contact->body))

@foreach ($phones as $phone)
    <a href="tel:{{ str_replace(['+', '-','(', ')'], '', $phone) }}">{{ $phone }}</a>
@endforeach
