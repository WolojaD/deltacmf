<a @if($contact->url && !is_null($contact->url)) href="{{ $contact->url }}" @endif>{{ $contact->body ?? ''}}</a>
