@if($banners)

    @foreach($banners as $banner)
        @isFileExists($banner->translatable_desktop_image)

        @accessByRoleTypeAdmin
        <a href="{{ route('backend.banners_banner.edit', $banner->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit banner</a>
        @endAccessByRoleTypeAdmin

            {{-- Изображение --}}
            <img onload="setTimeout(function() { sendStatistics({{ $banner->id }}) }, 1000);" src="{{ asset('storage/i/banners-image' . $banner->adaptive_translatable_image) }}" alt="{{ $banner->translatable_desktop_image_alt ?? '' }}">

            {{-- Название и цвет --}}
            <h4 @if($banner->title_color)style="color:{{ $banner->title_color }}"@endif>{{ $banner->title }}</h4>

            {{-- Описание и цвет --}}
            <p @if($banner->description_color)style="color:{{ $banner->description_color }}"@endif>{{ nl2br($banner->description) ?? '' }}</p>

            {{-- Ссылка (кнопка) --}}
            <a @if($banner->blank)target="_blank"@endif
                href="{{ $banner->url }}"
                id="{{ $banner->id }}"
                onclick="sendStatistics({{ $banner->id }}, 1)">
                {{ $banner->button_title ?? trans('frontend::banners.default button title') }}
            </a>

        @endIsFileExists
    @endforeach

@endif
