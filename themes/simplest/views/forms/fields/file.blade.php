<div class="form-group">
    <label class="control-label col-sm-2" for="file">{{ $field->title }}:</label>

    <div class="col-sm-10">
        <input type="file"
                @if(null !== $field->accept)
                accept="{{ '.' . str_replace(',', ',.', str_replace(', ', ',', $field->accept)) }}"
                @endif
                class="form-control {{ $field->class ?? '' }}"
                id="{{ $field->unique_code . '_' . $field->id }}"
                placeholder="{{ $field->placeholder }}"
                name="{{ $field->unique_code }}"
                @if(null !== $field->regex)
                data-regex="{{ $field->regex->regex_frontend }}"
                @endif
                {{ $field->required ? 'required' : '' }}/>

        @if(null !== $field->comment)
            <small class="text-muted">{{ $field->comment }}</small>
        @endif

    </div>

</div>
