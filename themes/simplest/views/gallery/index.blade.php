@accessByRoleTypeAdmin
    <a href="{{ route('backend.gallery.show', $gallery->id) }}" target="_blank" class="admin-edit-shortcode-block-button">Edit Gallery</a>
@endAccessByRoleTypeAdmin

<h2>{{ $gallery->title ?? '' }}</h2>
<p>{{ $gallery->description ?? '' }}</p>

<div style="border-style: inset;" id="gallery{{ $gallery->id }}" class="galley_block">

    @foreach($gallery->images as $image)
        <a href="/storage/origin{{ $image->path }}">
            <img src="/storage/i/gallery-image{{ $image->path }}">
            <p>{{ $image->title ?? '' }}</p>
            <p>{{ $image->description ?? '' }}</p>
        </a>
    @endforeach

</div>
