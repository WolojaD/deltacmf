@extends('layouts.app')

@section('title')
@stop

@section('description')
@stop

@section('before_styles')
@stop

@section('after_styles')
@stop

@section('open_graph')
@stop

@section('body_class')
@stop

@section('wrapper_class')
container
@stop

@section('before_content')
@stop

@section('content')



<div class="jumbotron">
    <div class="text-center">
        Category
        <h1>{{ $category->title }}</h1>
    </div>
</div>



@stop

@section('after_content')
@stop

@section('before_scripts')
@stop

@section('after_scripts')
@stop
