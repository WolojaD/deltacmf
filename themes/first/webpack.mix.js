const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('../../public');

mix
    .styles('resources/vendor/element-custom/index.sass', '../../public/themes/first/css/element.css')
    .postCss(
        'resources/vendor/tailwind/css/tailwind.css',
        'themes/first/css/app.css', [
            tailwindcss('resources/vendor/tailwind/js/tailwind.js'),
        ])
    .js('resources/assets/js/app.js', 'themes/first/js/app.js')
    .version()
    .extract()
    .copyDirectory('resources/vendor/element-custom/fonts', '../../public/themes/first/css/fonts')
    .copyDirectory('resources/vendor/tinymce-vue/tinymce', '../../public/themes/first/vendor/tinymce');
