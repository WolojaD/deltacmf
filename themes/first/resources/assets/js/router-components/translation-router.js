import helpers from '../services/helpers'

const locales = helpers.appStorage().locales;

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/translation',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.translation',
                component: require('../views/translation/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Translation',
                    breadcrumb: [
                        { name: 'Translation' }
                    ]
                }
            }
        ]
    }
];
