import helpers from '../services/helpers'

const locales = helpers.appStorage().locales;

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/articles',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.articles',
                component: require('../views/articles/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Articles',
                    breadcrumb: [
                        { name: 'Articles' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.articles.create',
                component: require('../views/articles/create').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Article Create',
                    breadcrumb: [
                        { name: 'Articles', link: 'api.backend.articles' },
                        { name: 'Article Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.articles.edit',
                component: require('../views/articles/edit').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Article Edit',
                    breadcrumb: [
                        { name: 'Articles', link: 'api.backend.articles' },
                        { name: 'Article Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/articles/categories',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.articles.categories',
                component: require('../views/articles/categories/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Categories',
                    breadcrumb: [
                        { name: 'Categories' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/show',
                name: 'api.backend.articles.categories.show',
                component: require('../views/articles/categories/show').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Category show',
                    breadcrumb: [
                        { name: 'Categories', link: 'api.backend.articles.categories' },
                        { name: 'Category show' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.articles.categories.create',
                component: require('../views/articles/categories/create').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Category Create',
                    breadcrumb: [
                        { name: 'Categories', link: 'api.backend.articles.categories' },
                        { name: 'Category Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.articles.categories.edit',
                component: require('../views/articles/categories/edit').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Category Edit',
                    breadcrumb: [
                        { name: 'Categories', link: 'api.backend.articles.categories' },
                        { name: 'Category Edit' }
                    ]
                }
            }
        ]
    }
];
