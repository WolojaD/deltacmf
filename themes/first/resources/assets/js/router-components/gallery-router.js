import helpers from '../services/helpers'

const locales = helpers.appStorage().locales;

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/gallery',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.gallery',
                component: require('../views/gallery/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Gallery',
                    breadcrumb: [
                        { name: 'Gallery' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/show',
                name: 'api.backend.gallery.show',
                component: require('../views/gallery/show').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Gallery Edit',
                    breadcrumb: [
                        { name: 'Gallery', link: 'api.backend.gallery' },
                        { name: 'Gallery Edit' }
                    ]
                }
            }
        ]
    }
];
