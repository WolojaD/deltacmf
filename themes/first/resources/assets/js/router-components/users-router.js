import helpers from '../services/helpers'

const locales = helpers.appStorage().locales;

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/users',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.users',
                component: require('../views/users/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Users',
                    breadcrumb: [
                        { name: 'Users' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.users.create',
                component: require('../views/users/create').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'User Create',
                    breadcrumb: [
                        { name: 'Users', link: 'api.backend.users' },
                        { name: 'User Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.users.edit',
                component: require('../views/users/edit').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'User Edit',
                    breadcrumb: [
                        { name: 'Users', link: 'api.backend.users' },
                        { name: 'User Edit' }
                    ]
                }
            }
        ]
    },
    {
        path: '/' + helpers.makeBaseUrl() + '/users/roles',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.role',
                component: require('../views/users/roles/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Roles',
                    breadcrumb: [
                        { name: 'Roles' }
                    ]
                }
            },
            {
                path: 'create',
                name: 'api.backend.role.create',
                component: require('../views/users/roles/create').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Role Create',
                    breadcrumb: [
                        { name: 'Roles', link: 'api.backend.role' },
                        { name: 'Role Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.role.edit',
                component: require('../views/users/roles/edit').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Role Edit',
                    breadcrumb: [
                        { name: 'Roles', link: 'api.backend.role' },
                        { name: 'Role Edit' }
                    ]
                }
            }
        ]
    }
];
