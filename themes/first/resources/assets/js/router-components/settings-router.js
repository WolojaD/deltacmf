import helpers from '../services/helpers'

const locales = helpers.appStorage().locales;

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/settings',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: ':module',
                name: 'api.backend.settings.module',
                component: require('../views/settings/index').default,
                props: {
                    locales
                },
                meta: {
                    breadcrumb: [
                        { name: 'Core' }
                    ]
                }
            }
        ]
    }
];
