import helpers from '../services/helpers'

const locales = helpers.appStorage().locales;

export default [
    {
        path: '/' + helpers.makeBaseUrl() + '/pages',
        component: require('../components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.page',
                component: require('../views/page/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Pages',
                    breadcrumb: [
                        { name: 'Pages' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/create',
                name: 'api.backend.page.create',
                component: require('../views/page/create').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Page Create',
                    breadcrumb: [
                        { name: 'Pages', link: 'api.backend.page' },
                        { name: 'Page Create' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.page.edit',
                component: require('../views/page/edit').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Page Edit',
                    breadcrumb: [
                        { name: 'Pages', link: 'api.backend.page' },
                        { name: 'Page Edit' }
                    ]
                }
            },
            {
                path: ':id(\\d+)/edit',
                name: 'api.backend.page.show.edit',
                component: require('../views/page/edit').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Page Edit',
                    breadcrumb: [
                        { name: 'Pages', link: 'api.backend.page' },
                        { name: 'Page Edit' }
                    ]
                }
            },
            {
                path: ':id(\\d+)',
                name: 'api.backend.page.show',
                component: require('../views/page/index').default,
                props: {
                    locales
                },
                meta: {
                    pageTitle: 'Pages',
                    breadcrumb: [
                        { name: 'Pages' }
                    ]
                }
            },
        ]
    }
];
