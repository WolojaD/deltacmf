import VueRouter from 'vue-router';
import helpers from './services/helpers';

/**
 *  Router imports
 */
import articlesRouter from './router-components/articles-router';
import galleryRouter from './router-components/gallery-router';
import pageRouter from './router-components/page-router';
import settingsRouter from './router-components/settings-router';
import translationRouter from './router-components/translation-router';
import usersRouter from './router-components/users-router';

/**
 *  Router compilate
 */
const routes = [
    {
        path: '/' + helpers.makeBaseUrl() + '/',
        component: require('./components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.dashboard',
                component: require('./views/dashboard/index').default,
                meta: {
                    pageTitle: 'Dashboard'
                }
            }
        ]
    },
    ...articlesRouter,
    ...galleryRouter,
    ...pageRouter,
    ...settingsRouter,
    ...translationRouter,
    ...usersRouter,
    {
        path: '*',
        component : require('./components/layouts/default-page').default,
        children: [
            {
                path: '*',
                component: require('./views/errors/page-not-found').default
            }
        ]
    }
];

/**
 *  Router create
 */
const router = new VueRouter({
    mode: 'history',
    routes,
    linkActiveClass: 'active'
});

/**
 *  Router validate
 */
// router.beforeEach((to, from, next) => {
//     if (to.matched.some(match => match.meta.requiresAuth)) {
//         return helpers.checkApiToken().then(response => {
//             if (!response) {
//                 this.$router.go('/auth/login');
//             }

//             return next()
//         })
//     }

//     return next()
// });

export default router;
