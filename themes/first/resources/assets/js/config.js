module.exports = {
    apiUrl: window.apiEndpoint || process.env.API_ENDPOINT,
    url(url) {
        return `${this.apiUrl}/${url}`;
    }
}
