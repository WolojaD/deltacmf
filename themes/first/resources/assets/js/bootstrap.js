/**
 *  Window add-ons
 */
window.axios = require('axios')
window._ = require('lodash')
window.moment = require('moment')

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

/**
 * API token.
 */
var apiToken = document.head.querySelector('meta[name="api-token"]')

if (apiToken) {
    window.axios.defaults.headers.common['Authorization'] = `Bearer ${apiToken.content}`
} else {
    console.error('API token not found in a meta tag.')
}

/**
 * CSRF Token.
 */
var token = document.head.querySelector('meta[name="csrf-token"]')

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token')
}

/**
 * Axios interceptors for validate response from backend.
 */
window.axios.interceptors.response.use(null, (error) => {
    if (error.response === undefined) {
        console.log(error);

        return;
    }

    if (error.response.status === 403) {
        console.log(error);

        window.location = route('backend.dashboard.index');
    }

    if (error.response.status === 401) {
        console.log(error);

        window.location = route('login');
    }

    return Promise.reject(error);
});

document.addEventListener('DOMContentLoaded', function () {
    let userAgent = window.navigator.userAgent;

    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
        document.documentElement.classList.add('ios-device');
    }
});
