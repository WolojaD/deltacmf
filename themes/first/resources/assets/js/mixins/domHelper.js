export default {
    methods: {
        getHtmlElement (query) {
            return this.$root.$el.querySelector(query)
        },
        generateSvg (name, text) {
            return `<svg class="${name}"><use class="${name}" xlink:href="#${name}"></use></svg>${text}`
        }
    }
}
