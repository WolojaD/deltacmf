export default {
    methods: {
        pushRoute(route) {
            this.$router.push({ name: route });
        }
    }
};
