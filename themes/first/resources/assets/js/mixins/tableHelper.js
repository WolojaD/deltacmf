export default {
    methods: {
        formatLabel(value) {
            if (value === true || value === 1) {
                return '<i class="fa fa-circle text-green-lighter"></i>'
            } else {
                return '<i class="fa fa-circle text-red-lighter"></i>'
            }
        },
        onRowClass(tableData, index) {
            if (this.$route.name == 'api.backend.users') {
                return tableData.permissions === null
                    ? ''
                    : 'bg-red-lightest'
            }
        }
    }
};
