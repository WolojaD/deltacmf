import moment from 'moment';

export default {
    methods: {
        ucfirst(string) {
            return string[0].toUpperCase() + string.substr(1);
        },
        ucword(string) {
            return string.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
        },
        formatDateTime(date, fmt = 'DD.MM.YYYY HH:mm') {
            return date ? moment(date).format(fmt) : '';
        }
    },
};
