export default {
    /**
     *  Make backend url with language to form urls
     */
    makeBaseUrl() {
        const currentLocale = window.appStorage.currentBackendLocale;
        const adminPrefix = window.appStorage.adminPrefix;

        if (window.appStorage.hideDefaultLocaleInURL == 1) {
            if (currentLocale == 'ru') {
                return adminPrefix;
            }

            return `${currentLocale}/${adminPrefix}`;
        }

        return `${currentLocale}/${adminPrefix}`;
    },

    /**
     *  Send request for validate if user auth
     */
    checkApiToken() {
        return axios.get(route('api.backend.check')).then(response => {
            return !!response.data.authenticated;
        }).catch(error =>{
            return response.data.authenticated;
        });
    },

    /**
     *  Logout method
     */
    logout() {
        return axios.get(route('logout')).then(response => {
            window.axios.defaults.headers.common['Authorization'] = null;
            axios.defaults.headers.common['Authorization'] = null;
        }).catch(error => {
            console.log(error);
        });
    },

    /**
     *  Get data from appStorage
     */
    appStorage() {
        try {
            return window.appStorage;
        } catch (e) {}
    }
}
