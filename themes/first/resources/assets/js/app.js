/**
 *  Require
 */
require('./bootstrap').default;

/**
 *  Import
 */
import Vue from 'vue';
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';
import VueRouter from 'vue-router';
import VueEvents from 'vue-events';
import ElementUI from 'element-ui';
// For custom CSS you need to copy this folder and put you folder's sourse and include it in webpack mix for compilation
// import 'element-ui/lib/theme-chalk/index.sass'
import locale from 'element-ui/lib/locale/lang/en';

/**
 *  Vue use
 */
window.Vue = Vue;
Vue.use(Vuex);
Vue.use(VueI18n);
Vue.use(VueRouter);
Vue.use(VueEvents);
Vue.use(ElementUI, { locale });

/**
 *  Vue mixin
 */
import translationHelper from './mixins/translationHelper';
import requestHelper from './mixins/requestHelper';
import shortcutHelper from './mixins/shortcutHelper';
import slugifyHelper from './mixins/slugifyHelper';
import stringHelper from './mixins/stringHelper';
import domHelper from './mixins/domHelper'

Vue.mixin(translationHelper);
Vue.mixin(requestHelper);
Vue.mixin(shortcutHelper);
Vue.mixin(slugifyHelper);
Vue.mixin(stringHelper);
Vue.mixin(domHelper);

/**
 *  Custom component
 */
import customActions from './components/elements/custom-actions';
import customStatus from './components/elements/custom-status';

Vue.component('custom-actions', customActions);
Vue.component('custom-status', customStatus);

/**
 * Current language and trans.
 */
const messages = {
    [window.appStorage.currentBackendLocale]: window.appStorage.translations,
};

const i18n = new VueI18n({
    locale: window.appStorage.currentBackendLocale,
    messages,
});

/**
 *  Main config files import for Vue app
 */
import store from './store';
import router from './router-config';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#root',
    store,
    router,
    i18n
});
