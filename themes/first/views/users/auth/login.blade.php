@extends('core::layouts.auth')

@section('title')
Login
@stop

@section('content')

<div class="container mx-auto h-full flex justify-center items-center">
    <div class="w-1/3">
        <h1 class="font-hairline mb-6 text-center">Login</h1>

        @include('core::partials.notifications')

        {!! Form::open(['route' => 'login.post', 'class' => 'border-teal p-8 border-t-12 bg-white mb-6 rounded-lg shadow-lg']) !!}
            <div class="mb-4">
                <label class="font-bold text-grey-darker block mb-2">Email</label>
                <input type="email" class="block appearance-none w-full bg-white border border-grey-light hover:border-grey px-2 py-2 rounded shadow {{ $errors->has('email') ? 'border-red' : '' }}" name="email" value="{{ old('email') }}" placeholder="Your Email">
                {!! $errors->first('email', '<p class="text-red text-xs italic">:message</p>') !!}
            </div>

            <div class="mb-4">
                <label class="font-bold text-grey-darker block mb-2">Password</label>
                <input type="password" class="block appearance-none w-full bg-white border border-grey-light hover:border-grey px-2 py-2 rounded shadow {{ $errors->has('password') ? 'border-red' : '' }}" name="password" value="{{ old('password') }}" placeholder="Your Password">
                {!! $errors->first('password', '<p class="text-red text-xs italic">:message</p>') !!}
            </div>

            <div class="flex items-center justify-between">
                <button class="bg-teal-dark hover:bg-teal text-white font-bold py-2 px-4 rounded" type="submit">
                    Login
                </button>

                {{-- <a class="no-underline inline-block align-baseline font-bold text-sm text-blue hover:text-blue-dark float-right" href="{{ route('reset') }}">
                    Forgot Password?
                </a> --}}
            </div>
        {!! Form::close() !!}

        {{-- <div class="text-center">
            <p class="text-grey-dark text-sm">Don't have an account? <a href="{{ route('register') }}" class="no-underline text-blue font-bold">Create an Account</a>.</p>
        </div> --}}
    </div>
</div>

@stop
