@extends('core::layouts.auth')

@section('title')
Reset password
@stop

@section('content')

<div class="container mx-auto h-full flex justify-center items-center">
    <div class="w-1/3">
        <h1 class="font-hairline mb-6 text-center">Reset password</h1>

        @include('users::partials.notifications')

        {!! Form::open(['class' => 'w-full max-w-xs']) !!}
            <div class="md:flex md:items-center mb-6">
                <div class="md:w-1/3">
                    <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4 {{ $errors->has('password') ? 'text-red' : '' }}" for="inline-password">
                        Password
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter hover:border-purple rounded w-full py-2 px-4 text-grey-darker {{ $errors->has('password') ? 'border-red' : '' }}" id="inline-password" type="password" name="password" placeholder="************">
                    {!! $errors->first('password', '<p class="text-red text-xs italic">:message</p>') !!}
                </div>
            </div>
            <div class="md:flex md:items-center mb-6">
                <div class="md:w-1/3">
                    <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4 {{ $errors->has('password_confirmation') ? 'text-red' : '' }}" for="inline-password_confirmation">
                        Password Confirmation
                    </label>
                </div>
                <div class="md:w-2/3">
                    <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter hover:border-purple rounded w-full py-2 px-4 text-grey-darker {{ $errors->has('password_confirmation') ? 'border-red' : '' }}" id="inline-password_confirmation" type="password" name="password_confirmation" placeholder="************">
                    {!! $errors->first('password_confirmation', '<p class="text-red text-xs italic">:message</p>') !!}
                </div>
            </div>
            <div class="md:flex md:items-center">
                <div class="md:w-1/3"></div>
                <div class="md:w-2/3">
                    <button class="shadow bg-purple hover:bg-purple-light text-white font-bold py-2 px-4 rounded" type="submit">
                        Reset password
                    </button>
                </div>
            </div>
        {!! Form::close() !!}

    </div>
</div>

@stop
