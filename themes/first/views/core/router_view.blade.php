@extends('core::layouts.application')

@section('title')
    DeltaCMF
@stop

@section('body_class')
    bg-grey-lighter h-screen font-sans
@stop



@section('content')

    <router-view></router-view>

@stop
