<el-header>
    <el-menu :default-active="activeIndex" class="el-menu-demo" mode="horizontal">

        @foreach(app()->modules->getOrdered() as $module)

            @if($module->name !== 'Core')

                <el-menu-item index="0"><a href="{{ route('backend.'.$module->alias.'.index') }}">{{ $module->name }}</a></el-menu-item>

            @endif

        @endforeach

        <el-submenu index="2">
            <template slot="title">Workspace</template>
            <el-menu-item index="2-1">item one</el-menu-item>
            <el-menu-item index="2-2">item two</el-menu-item>
            <el-menu-item index="2-3">item three</el-menu-item>
            <el-submenu index="2-4">
                <template slot="title">item four</template>
                <el-menu-item index="2-4-1">item one</el-menu-item>
                <el-menu-item index="2-4-2">item two</el-menu-item>
                <el-menu-item index="2-4-3">item three</el-menu-item>
            </el-submenu>
        </el-submenu>
        <el-menu-item index="3" disabled>Info</el-menu-item>
        <el-menu-item index="4"><a href="{{ route('logout') }}">Logout</a></el-menu-item>
    </el-menu>
</el-header>
