<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <title>@yield('title')</title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset('themes/first/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('themes/first/css/element.css') }}">
    </head>
    <body class="bg-grey-lighter h-screen font-sans">

        @yield('content')

    </body>
</html>
