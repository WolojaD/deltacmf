<!DOCTYPE html>
<html lang="{{ locale() }}">
    <head>
        <base src="{{ url('/') }}">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>@yield('title')</title>

        <!-- CSRF and API Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="api-token" content="{{ $currentUser ? $currentUser->getFirstApiKey() : '1X2x3X4x5X6x7X8x9X0x' }}">

        <!-- CSS -->
        @yield('before_styles')
        <link rel="stylesheet" href="{{ asset('themes/first/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('themes/first/css/element.css') }}">
        @yield('after_styles')

        <!-- tightenco/ziggy -->
        @routes
    </head>
    <body class="@yield('body_class')">

        <div id="root" class="main-wrapper">

            @yield('before_content')

            @yield('content')

            @yield('after_content')

        </div>

        <!-- JavaScript -->
        <script type="text/javascript">
            window.appStorage = {
                translations: {!! $backendTranslations !!},
                currentBackendLocale: '{{ $currentBackendLocale }}',
                allBackendLocales: {!! json_encode($allBackendLocales) !!},
                currentFrontendLocale: '{{ $currentFrontendLocale }}',
                allFrontendLocales: {!! json_encode($allFrontendLocales) !!},
                adminPrefix: '{{ config('application.core.core.admin-prefix', 'backend') }}',
                hideDefaultLocaleInURL: '{{ config('laravellocalization.hideDefaultLocaleInURL') }}',
                currentUser: {!! $currentUser->getCurrentUser() !!}
            };
        </script>
        @yield('before_scripts')
        <script type="text/javascript" src="{{ asset('themes/first/js/manifest.js') }}"></script>
        <script type="text/javascript" src="{{ asset('themes/first/js/vendor.js') }}"></script>
        <script type="text/javascript" src="{{ asset('themes/first/js/app.js') }}"></script>
        @yield('after_scripts')
    </body>
</html>
