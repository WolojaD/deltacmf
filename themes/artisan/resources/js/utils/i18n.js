export function generateTitle (title) {
    return this.trans('core_breadcrumbs.' + title.toLowerCase())
}
