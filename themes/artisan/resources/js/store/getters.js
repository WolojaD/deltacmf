const getters = {
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews,
    formData: state => state.formData.formData,
    formState: state => state.formData.formState,
    stateTrigger: state => state.formData.stateTrigger
}

export default getters
