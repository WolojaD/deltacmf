import helpers from '@services/helpers'

const formData = {
    state: {
        formData: {},
        formState: {},
        formStateTrigger: false
    },
    mutations: {
        ADD_FORM_DATA: (state, payload) => {
            state.formData = payload;
        },
        ADD_FORM_STATE: (state, payload) => {
            _.forEach(payload, (fields, tab) => {
                if (_.has(fields, 'translatable')) {
                    _.forEach(fields.translatable, (fieldParams, fieldName) => {
                        if (_.has(fieldParams, 'parameters.required') && fieldParams.parameters.required === true) {
                            state.formState[fieldName] = true
                        }
                    });
                }

                _.forEach(fields, (fieldParams, fieldName) => {
                    if (_.has(fieldParams, 'parameters.required') && fieldParams.parameters.required === true) {
                        state.formState[fieldName] = true
                    }
                });
            });
        },
        CHECK_FORM_STATE: (state) => {
            _.forEach(state.formState, (fieldValue, fieldName) => {
                if (_.has(state.formData[helpers.appStorage().currentFrontendLocale], fieldName)) {
                    if (state.formData[helpers.appStorage().currentFrontendLocale][fieldName] === "" || state.formData[helpers.appStorage().currentFrontendLocale][fieldName] === null) {
                        state.formState[fieldName] = false;
                    } else {
                        state.formState[fieldName] = true;
                    }
                } else if (_.has(state.formData, fieldName)) {
                    if (state.formData[fieldName] === "" || state.formData[fieldName] === null) {
                        state.formState[fieldName] = false;
                    } else {
                        state.formState[fieldName] = true;
                    }
                } else {
                    state.formState[fieldName] = false;
                }
            })

            if (_.size(_.filter(state.formState, Boolean)) !== 0) {
                if (_.size(_.filter(state.formState, Boolean)) === Object.keys(state.formState).length) {
                    state.formStateTrigger = true;
                } else {
                    state.formStateTrigger = false;
                }
            } else {
                state.formStateTrigger = false;
            }
        },
        DEL_FORM_DATA: (state) => {
            state.formData = {};
            state.formState = {};
        }
    },
    actions: {
        addFormState ({ commit }, payload) {
            commit('ADD_FORM_STATE', payload);
        },
        checkFormState ({ commit }, payload) {
            commit('CHECK_FORM_STATE', payload);
        },
        addFormData ({ commit }, payload) {
            commit('ADD_FORM_DATA', payload);
        },
        delFormData ({ commit, state }) {
            return new Promise((resolve) => {
                commit('DEL_FORM_DATA');
                // TODO Need to test
                // resolve([...state.formData]);
                resolve([state.formData]);
            })
        }
    }
};

export default formData
