import VueRouter from 'vue-router'
import helpers from '@services/helpers'

/**
 *  Router compilate
 */
const routes = [
    {
        path: '/' + helpers.makeBaseUrl() + '/dashboard',
        component: require('@components/layouts/default-page').default,
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                name: 'api.backend.dashboard',
                component: require('@/views/dashboard/index').default,
                meta: {
                    pageTitle: 'Dashboard'
                }
            }
        ]
    },
];

const moduleList = helpers.appStorage().activeModulesPaths;

for (let moduleName in moduleList) {
    routes.push(...require(`@modules/${moduleList[moduleName]}/Routes/router`).default);
}

routes.push({
    path: '*',
    component: require('@components/layouts/default-page').default,
    children: [
        {
            path: '/',
            component: require('@/views/errors/404').default
        }
    ]
});

/**
 *  Router create
 */
const router = new VueRouter({
    mode: 'history',
    routes,
    linkActiveClass: 'active'
})

export default router
