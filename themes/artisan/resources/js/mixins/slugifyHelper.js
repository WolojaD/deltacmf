'use strict'

export default {
    methods: {
        slugify(string = '') {
            if (typeof string !== 'string') {
                return ''
            }

            if (!string) {
                return ''
            }

            let value

            value = string.trim().replace(/^\s+|\s+$/g, '') // Trim
            value = value.toLowerCase()

            value = this.cyrillicToLatnTranslit(value) // convert Cyr to Latn or return Latn

            // Remove accents
            const from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;'
            const to = 'aaaaaeeeeeiiiiooooouuuunc------'

            for (let i = 0, l = from.length; i < l; i++) {
                value = value.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
            }

            value = value.replace(/[^a-z0-9 -]/g, '') // Remove invalid chars
                .replace(/\s+/g, '-') // Collapse whitespace and replace by -
                .replace(/-+/g, '-')
                .replace(/^-/g, '')
                .replace(/-$/g, '')

            return value
        },
        cyrillicToLatnTranslit(string) {
            if (!string) {
                return ''
            }

            const _firstLetterAssociations = {
                'а': 'a',
                'б': 'b',
                'в': 'v',
                'ґ': 'g',
                'г': 'g',
                'д': 'd',
                'е': 'e',
                'ё': 'e',
                'є': 'e',
                'ж': 'zh',
                'з': 'z',
                'и': 'i',
                'і': 'i',
                'ї': 'ji',
                'й': 'y',
                'к': 'k',
                'л': 'l',
                'м': 'm',
                'н': 'n',
                'о': 'o',
                'п': 'p',
                'р': 'r',
                'с': 's',
                'т': 't',
                'у': 'u',
                'ф': 'f',
                'х': 'h',
                'ц': 'c',
                'ч': 'ch',
                'ш': 'sh',
                'щ': 'shch',
                'ъ': '',
                'ы': 'y',
                'ь': '',
                'э': 'e',
                'ю': 'yu',
                'я': 'ya',
                "'": '',
                '’': ''
            }

            const _associations = Object.assign({}, _firstLetterAssociations)

            let newStr = ''

            for (let i = 0; i < string.length; i++) {
                let strLowerCase = string[i].toLowerCase()

                if (strLowerCase === ' ') {
                    newStr += '-'

                    continue
                }

                let newLetter = (i === 0 ? _firstLetterAssociations : _associations)[strLowerCase]

                if (typeof newLetter === 'undefined') {
                    newStr += strLowerCase
                } else {
                    newStr += newLetter
                }
            }

            return newStr
        }
    }
}
