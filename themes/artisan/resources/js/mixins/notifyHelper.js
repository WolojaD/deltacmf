export default {
    methods: {
        notify(response) {
            let headerHeight = this.getHtmlElement('header.el-header').getBoundingClientRect().height;
            
            if (response.errors) {
                this.$notify.error({
                    title: 'Error!',
                    message: response.message,
                    offset: headerHeight
                });
            } else {
                if (_.has(response, 'type') && _.has(response, 'title')) {
                    this.$notify({
                        title: response.title,
                        type: response.type,
                        message: response.message,
                        offset: headerHeight
                    });
                } else if (_.has(response, 'type')) {
                    this.$notify({
                        title: 'Success!',
                        type: response.type,
                        message: response.message,
                        offset: headerHeight
                    });
                } else {
                    this.$notify({
                        title: 'Success!',
                        type: 'success',
                        message: response.message,
                        offset: headerHeight
                    });
                }
            }
        }
    }
}
