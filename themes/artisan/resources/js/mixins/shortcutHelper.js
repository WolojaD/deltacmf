export default {
    methods: {
        pushRoute (route) {
            if (typeof route === 'string') {
                const elements = route.split('|');

                if (elements.length == 1) {
                    this.$router.push({ name: elements[0] })
                } else {
                    const params_list = elements[1].split(',');
                    let params = {};

                    for (let id in params_list) {
                        let parameter_item = params_list[id].split(':');

                        params[parameter_item[0]] = this.$route.params[parameter_item[1]];
                    }

                    this.$router.push({ name: elements[0], params })
                }
            } else if (typeof route === 'object') {
                this.$router.push(route)
            }
        },
        cellClicked (item) {
            if (!this.table.parentId.includes(item.parent_id)) {
                this.table.parentId.push(item.parent_id)
            }

            if (!this.table.parentId.includes(item.id)) {
                this.table.parentId.push(item.id)
            }

            this.$events.fire('filter-bar-reset')
            this.table.apiRoute = route(this.$route.name + '.index', { pageId: item.id })
            this.$router.push({ name: this.$route.name, params: {id: item.id} })
        }
    }
}
