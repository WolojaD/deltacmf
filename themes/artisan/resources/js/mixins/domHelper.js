export default {
    methods: {
        getHtmlElement (query) {
            return this.$root.$el.querySelector(query)
        },
        generateSvg (name, text) {
            return `<svg class="${name}"><use class="${name}" xlink:href="#${name}"></use></svg>${text}`
        },
        isTouchDevice() {
            let prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
            let mq = function(query) {
                return window.matchMedia(query).matches;
            }

            if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
                return true;
            }

            // include the 'heartz' as a way to have a non matching MQ to help terminate the join
            // https://git.io/vznFH
            let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');

            return mq(query);
        },
        enterModal(el, done) {
            let content = el.querySelector('.modal-content');

            Velocity(el, {opacity: [1, 0]}, {duration: 500, begin: function () {
                    let padding = window.innerWidth - document.body.clientWidth+'px';
                    document.body.classList.add('overflow-hidden');
                    document.body.style.width = 'calc(100% - '+ padding +')';
                    document.querySelector('header').style.right = padding;
                }});

            Velocity(content, {translateY: ['0%', '-10%']}, {duration: 500, complete: done});
        },
        leaveModal(el, done) {
            let content = el.querySelector('.modal-content');

            Velocity(content, {translateY: ['10%', '0%']}, {duration: 500});
            Velocity(el, {opacity: [0, 1]}, {duration: 500, complete: function () {
                    document.body.classList.remove('overflow-hidden');
                    document.body.style.width = '';
                    document.querySelector('header').style.right = '';
                    done();
                }});
        },
    }
}
