export default {
    methods: {
        ucfirst(string) {
            if (string) {
                return string[0].toUpperCase() + string.substr(1) || '';
            }

            return '';
        },
        ucword(string) {
            return string.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
        },
        formatDateTime(date, fmt = 'HH:mm:ss DD.MM.YYYY', colored = false) {
            if (!date) {
                return;
            }

            let formatedDate = moment(date).format(fmt);

            if (!colored) {
                return formatedDate;
            }

            let started = moment(date) - moment() <= 0;

            return `<p style="color:${started ? 'green' : 'red'}">${formatedDate}</p>`;

        },
        declOfNum: function(array) {
            let declOfNum = (function() {
                let cases = [2, 0, 1, 1, 1, 2];

                let declOfNumSubFunction = function(titles, number) {
                    number = Math.abs(number);

                    return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
                };

                return function(_titles) {
                    if (arguments.length === 1) {
                        return function(_number) {
                            return declOfNumSubFunction(_titles, _number)
                        }
                    } else {
                        return declOfNumSubFunction.apply(null, arguments)
                    }
                }
            })();

            return declOfNum(array);
        },
        formatBytes(bytes, decimals) {
            if (0 == bytes) return "0 Bytes";

            var c = 1024,
                d = decimals || 2,
                e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                f = Math.floor(Math.log(bytes) / Math.log(c));

            return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f]
        },
        decimalAdjust(type, value, exp) {
            // If the exp is undefined or zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }

            value = +value;
            exp = +exp;

            // If the value is not a number or the exp is not an integer...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }

            // If the value is negative...
            if (value < 0) {
                return -decimalAdjust(type, -value, exp);
            }

            // Shift
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));

            // Shift back
            value = value.toString().split('e');

            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        },
        floor10(value, exp) {
            return this.decimalAdjust('floor', value, exp);
        },
        round10(value, exp) {
            return this.decimalAdjust('round', value, exp);
        },
        ceil10(value, exp) {
            return this.decimalAdjust('ceil', value, exp);
        },
        numberWithSpaces(number) {
            let parts = number.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
            return parts.join(".");
        }
    }
}
