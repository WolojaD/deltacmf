export default {
    methods: {
        pushAxiosGet(url, callback) {
            axios.get(url)
                .then((response) => {
                    if (response.status === 200 && typeof (response.data) === 'object') {
                        callback(response.data)
                    }
                })
                .catch((error) => {
                    console.log('GET - ' + error)
                })
        },
        pushAxiosPost(url, data = null, callback) {
            if (data !== null && data !== 'null') {
                axios.post(url, data)
                    .then((response) => {
                        callback(response.data)
                    })
                    .catch((error) => {
                        console.log('POST - ' + error)
                    })
            }
        },
        pushAxiosPut(url, data = null, callback) {
            if (data !== null && data !== 'null') {
                axios.put(url, data)
                    .then((response) => {
                        callback(response.data)
                    })
                    .catch((error) => {
                        console.log('PUT - ' + error)
                    })
            } else if (data === null) {
                axios.put(url)
                    .then((response) => {})
                    .catch((error) => {
                        console.log('PUT - ' + error)
                    })
            }
        },
        pushAxiosDelete(url, callback = null) {
            axios.delete(url)
                .then((response) => {
                    if (callback !== null) {
                        callback(response.data)
                    }
                })
                .catch((error) => {
                    console.log('DELETE - ' + error)
                })
        },
        getApiToken() {
            let apiToken = document.head.querySelector('meta[name="api-token"]')

            if (apiToken) {
                return `Bearer ${apiToken.content}`
            } else {
                console.error('API token not found in a meta tag.')
            }
        }
    }
}
