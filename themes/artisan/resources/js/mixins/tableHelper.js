export default {
    methods: {
        onRowClass(tableData, index) {
            if (this.$route.name == 'api.backend.users') {
                return tableData.permissions === null
                    ? ''
                    : 'bg-red-lightest'
            }
        }
    }
}
