export default {
    methods: {
        validateUserPermissionIndexByRouteName(data) {
            if (data.includes('_')) {
                return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 2).join('.') + '.index'] ||
                       window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 3).join('.') + '.index'];
            }

            return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3).join('.') + '.index'];
        },
        validateUserPermissionCreateByRouteName(data) {
            if (data.includes('_')) {
                return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 2).join('.') + '.create'] ||
                       window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 3).join('.') + '.create'];
            }

            return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3).join('.') + '.create'];
        },
        validateUserPermissionEditByRouteName(data) {
            if (data.includes('_')) {
                return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 2).join('.') + '.edit'] ||
                       window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 3).join('.') + '.edit'];
            }

            return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3).join('.') + '.edit'];
        },
        validateUserPermissionDeleteByRouteName(data) {
            if (data.includes('_')) {
                return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 2).join('.') + '.destroy'] ||
                       window.appStorage.currentUser.permissions[data.split('.').slice(2, 3)[0].split('_').slice(0, 3).join('.') + '.destroy'];
            }

            return window.appStorage.currentUser.permissions[data.split('.').slice(2, 3).join('.') + '.destroy'];
        },
        canUser(action) {
            return window.appStorage.currentUser.permissions[`${this.meta.backend_path_name.replace(/_/g, '.').toLowerCase()}.${action}`];
        }
    }
}
