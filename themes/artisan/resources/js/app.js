/**
 *  Require
 */
require('@/bootstrap').default

/**
 *  Import
 */
import Vue from 'vue'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
import VueEvents from 'vue-events'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import _ from 'lodash'
import Velocity from 'velocity-animate'
import VueClipboard from 'vue-clipboard2'
import VueVisible from 'vue-visible'

// import VueInputMask from 'vue-inputmask' - this don`t work
const VueInputMask = require('vue-inputmask').default // TODO CUT THIS MADDNESSSS

/**
 *  Vue use
 */
window.Vue = Vue
Vue.use(Vuex)
Vue.use(VueI18n)
Vue.use(VueRouter)
Vue.use(VueEvents)
Vue.use(ElementUI, { locale })
Vue.set(Vue.prototype, '_', _)
Vue.use(VueClipboard)
Vue.use(VueVisible)
Vue.use(VueInputMask)

/**
 *  Vue mixin
 */
import translationHelper from '@/mixins/translationHelper'
import requestHelper from '@/mixins/requestHelper'
import shortcutHelper from '@/mixins/shortcutHelper'
import slugifyHelper from '@/mixins/slugifyHelper'
import stringHelper from '@/mixins/stringHelper'
import domHelper from '@/mixins/domHelper'
import permissionHelper from '@/mixins/permissionHelper'
import breakpointHelper from '@/mixins/breakpointHelper'
import notifyHelper from '@/mixins/notifyHelper'

Vue.mixin(translationHelper)
Vue.mixin(requestHelper)
Vue.mixin(shortcutHelper)
Vue.mixin(slugifyHelper)
Vue.mixin(stringHelper)
Vue.mixin(domHelper)
Vue.mixin(permissionHelper)
Vue.mixin(breakpointHelper)
Vue.mixin(notifyHelper)

/**
 *  Custom component
 */
import actionsElement from '@components/table-origin/elements/actions-element'
import statusElement from '@components/elements/status-element'
import imageElement from '@components/elements/image-element'

Vue.component('actions-element', actionsElement)
Vue.component('status-element', statusElement)
Vue.component('image-element', imageElement)

/**
 * Current language and trans.
 */
var messages = {
    [window.appStorage.currentBackendLocale]: window.appStorage.translations,
}

var i18n = new VueI18n({
    locale: window.appStorage.currentBackendLocale,
    silentTranslationWarn: true,
    messages,
})

/**
 *  Main config files import for Vue app
 */
import store from '@/store'
import router from '@/router-config'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
new Vue({
    store,
    router,
    i18n
}).$mount('#app')
