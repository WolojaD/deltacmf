export default {
    /**
     *  Make backend url with language to form urls
     */
    makeBaseUrl() {
        const currentBackendLocale = window.appStorage.currentBackendLocale
        const currentFrontendLocale = window.appStorage.currentFrontendLocale
        const adminPrefix = window.appStorage.adminPrefix

        if (window.appStorage.hideDefaultLocaleInURL == 1) {
            if (currentBackendLocale == 'ru') {
                return adminPrefix
            }

            return `${currentBackendLocale}/${adminPrefix}`
        }

        return `${currentBackendLocale}/${adminPrefix}`
    },

    /**
     *  Send request for validate if user auth
     */
    checkToken(callback) {
        axios.get(route('api.backend.check'))
            .then(response => {
                callback(!!response.data)
            })
            .catch(error => {
                callback(error)
            })
    },

    /**
     *  Logout method
     */
    logout() {
        return axios.get(route('logout')).then(response => {
            window.axios.defaults.headers.common['Authorization'] = null
            axios.defaults.headers.common['Authorization'] = null
        }).catch(error => {
            console.log('Error - ' + error)
        })
    },

    /**
     *  Get data from appStorage
     */
    appStorage() {
        try {
            return window.appStorage
        } catch (error) {
            console.log('Error - ' + error)
        }
    }
}
