import Vue from "vue"

Vue.filter("capitalize", function (value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1)
});

// Vue.filter("numberWithSpaces", function (value) {
//     let parts = value.toString().split(".");
//     parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
//     return parts.join(".");
// });



// TODO
// import { createMask } from "imask";
// const maskField = (field, maskOptions) => {
//     const mask = createMask(maskOptions);
//
//     return {
//         get: function() {
//             return mask.resolve(this[field]);
//         },
//         set: function(value) {
//             mask.resolve(value);
//             this[field] = mask.unmaskedValue;
//         }
//     };
// };
//
// Vue.filter("parsePrice", function (value) {
//     maskField(value, {
//         mask: Number
//     });
// });
