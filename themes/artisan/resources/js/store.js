import Vue from 'vue'
import Vuex from 'vuex'

// Default
import actions from '@/store/actions'
import getters from '@/store/getters'
import mutations from '@/store/mutations'

// Modules
import tagsView from '@/store/modules/tagsView'
import formData from '@/store/modules/formData'

Vue.use(Vuex)

// Init Vuex
const store = new Vuex.Store({
    state: {},
    actions,
    getters,
    mutations,
    modules: {
        tagsView,
        formData
    }
})

export default store
