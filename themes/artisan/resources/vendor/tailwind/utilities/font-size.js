module.exports = function () {
    return function({e, addUtilities}) {
        const tableWidthUtilities = {};

        for (let i = 0; i < 100; i++) {
            tableWidthUtilities[`.font-${i}`] = { fontSize: `${i}px` };
        }

        addUtilities(tableWidthUtilities);
    }
};
