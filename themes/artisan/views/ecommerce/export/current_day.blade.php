<table cellspacing="0" cellpadding="0" border="0">

    <thead>
        <tr>
            <th></th>
            @foreach($storages as $storage)
                <th>{{ $storage['title'] }}</th>
            @endforeach
        </tr>
    </thead>

    <tbody>
    @foreach($products as $p_key => $product)
        <tr>
            <td>{{ $p_key ?? ''}}</td>
            @foreach($storages as $storage)
                <td>
                    <p>
                        {{$product[$storage['id']]['storage_quantity'] ?? ''}}
                    </p>
                </td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>