<table cellspacing="0" cellpadding="0" border="0">

    <thead>
        <tr>
            <th></th>
            @foreach($dates as $date)
                <th>{{ $date }}</th>
            @endforeach
        </tr>
    </thead>

    <tbody>
    @foreach($storages as $storage)
        <tr>
            <td colspan="8"><b>{{ $storage['title'] ?? '' }}</b></td>
        </tr>

        @foreach($storage['products'] as $p_key => $product)
            <tr>
            <td>{{ $p_key ?? ''}}</td>
            @foreach($dates as $date)
                <td>
                    <p>
                        {{$product['dates'][$date]['count'] ?? ''}}
                    </p>
                </td>
            @endforeach
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>