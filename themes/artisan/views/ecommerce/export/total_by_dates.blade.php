<table cellspacing="0" cellpadding="0" border="0">

    <thead>
    <tr>
        <th></th>
        @foreach($dates as $date)
            <th>{{ $date }}</th>
        @endforeach
    </tr>
    </thead>

    <tbody>
    @foreach($products as $p_key => $product)
        <tr>
            <td>{{ $p_key ?? ''}}</td>
            @foreach($dates as $date)
                <td>
                    <p>
                        {{$product[$date] ?? ''}}
                    </p>
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>