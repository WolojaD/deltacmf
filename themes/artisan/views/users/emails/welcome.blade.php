<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <title>Welcome!</title>
    <style>
    .ExternalClass,.ReadMsgBody,body,html{width:100%;background-color:#fff}.BG1,.BG2,.BG4{background-size:cover;background-position:center center}a,div,li,p,td{-webkit-text-size-adjust:none}*{-moz-osx-font-smoothing:grayscale}body{height:100%;margin:0;padding:0}p{padding:0!important;margin:0!important}.hover:hover{opacity:.85;filter:alpha(opacity=85)}.BG1{background-image:url(images/1_bg.jpg)}.BG2{background-image:url(images/2_bg.jpg)}.BG4{background-image:url(images/4_bg.jpg)}@media only screen and (max-width:479px){table[class=full],table[class=mobile]{clear:both;width:100%!important}body{width:auto!important}table[class=mobile]{padding-left:30px;padding-right:30px}table[class=fullCenter],td[class=fullCenter]{width:100%!important;text-align:center!important;clear:both}[class=hide]{display:none}[class=buttonScale]{float:none!important;text-align:center!important;display:inline-block!important;clear:both}.image400 img{width:100%!important;height:auto}}body{background:0 0!important}
</style>
</head>

<body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">
    <div id="edit_link" class="hidden" style="display: none;">
        <div class="close_link"></div>
        <input type="text" id="edit_link_value" class="createlink" placeholder="Your URL">
        <div id="change_image_wrapper">
            <div id="change_image">
                <p id="change_image_button">Change &nbsp; <span class="pixel_result"></span></p>
            </div>
            <input type="button" value="" id="change_image_link">
            <input type="button" value="" id="remove_image">
        </div>
        <div id="tip"></div>
    </div>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" style="background-color: #ffffff;" data-thumb="" data-module="notification_1">
        <tbody>
            <tr>
                <td align="center">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody>
                            <tr>
                                <td width="100%" height="100" align="center">
                                    <table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                        <tbody>
                                            <tr>
                                                <td width="100%" height="50">

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="500" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" object="drag-module-small">
                                        <tbody>
                                            <tr>
                                                <td width="100%" valign="middle" align="center">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fullCenter" align="center" object="drag-module-small">
                                                        <tbody>
                                                            <tr>
                                                                <td width="100%" style="width:140px; height:auto;">
                                                                    <a href="#" style="text-decoration: none;">
                                                                        <img src="http://via.placeholder.com/250x100" width="250" alt="DeltaCMF" border="0" class="hover">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                        <tbody>
                                                            <tr>
                                                                <td width="100%" height="50" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullcenter">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="middle" width="100%" style="text-align: left; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 60px; color: #79c7ea; line-height: 70px; font-weight: 300;" class="fullcenter">
                                                                    Bring The best Project To Life
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                        <tbody>
                                                            <tr>
                                                                <td width="100%" height="50" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full BG1" style="border-radius: 6px; background-image: url(&quot;http://via.placeholder.com/600x270&quot;);" data-bg="">
                                        <tbody>
                                            <tr>
                                                <td width="100%" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
                                                    <div>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="middle" align="center">
                                                                        <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td width="100%" height="50" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="500" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="middle" align="left">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="middle" width="100%" style="text-align: left; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 16px; color: #575757; line-height: 30px; font-weight: 300;" class="fullCenter">
                                                                                        DeltaCMF mission is to solve the biggest
                                                                                        <br> problem in the world with WordPress. To help people
                                                                                        <br> live healthier for longer.
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" height="30" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="500" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%">
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="fullCenter">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td height="50" bgcolor="#79c7ea" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; padding-left: 50px; padding-right: 50px; font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: rgb(255,255, 255); font-size: 22px; font-weight: 400; line-height: 1px; background-color: #79c7ea;">
                                                                                        <a href="{{ URL::to('auth/activate/' . $user['id'] . '/' . $activationCode) }}" style="color: rgb(255, 255, 255); text-decoration: none; width: 100%;">Confirm email!</a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" style="-webkit-border-bottom-right-radius: 6px; -moz-border-bottom-right-radius: 6px; border-bottom-right-radius: 6px; -webkit-border-bottom-left-radius: 6px; -moz-border-bottom-left-radius: 6px; border-bottom-left-radius: 6px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="middle" align="center">
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td width="100%" height="50" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                        <tbody>
                                            <tr>
                                                <td width="100%" height="60" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: rgb(165, 168, 174); font-size: 12px; font-weight: 400; line-height: 18px;" class="fullCenter">
                                                    <i>By cf.digital</i>
                                                    <br> Copyright 2018
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                        <tbody>
                                            <tr>
                                                <td width="100%" height="60" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
