@extends('core::layouts.auth')

@section('title')
Login
@stop

@section('content')
    <video autoplay loop muted id="auth-video" style="opacity: 0;">
        <source src="{{ asset('themes/artisan/image/login.mp4') }}" type="video/mp4">
    </video>

    <main>
        <a href="{{ url('/') }}" class="go-home">{{ trans('users::auth.go-home') }}</a>

        @include('core::partials.notifications')

        {!! Form::open(['route' => 'login.post', 'class' => 'auth-form']) !!}
            <div class="logo">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.00055 22.725">
                    <polygon points="38.456 6.592 38.456 2.94 21.525 2.94 21.525 19.748 38.456 19.748 38.456 16.119 25.152 16.119 25.152 13.172 36.406 13.172 36.406 9.541 25.152 9.541 25.152 6.592 38.456 6.592" style="fill: #fff"></polygon>
                    <polygon points="71.206 2.94 55.337 2.94 55.337 6.568 61.461 6.568 61.461 19.799 65.087 19.799 65.087 6.568 71.206 6.568 71.206 2.94" style="fill: #fff"></polygon>
                    <polygon points="86.179 19.799 90.213 19.799 81.709 2.94 79.84 2.945 79.698 3.016 79.55 2.94 77.691 2.94 69.187 19.799 73.223 19.799 79.698 6.962 86.179 19.799" style="fill: #fff"></polygon>
                    <path d="M96.729,4.8848a1.7526,1.7526,0,0,0-.2881-.6387,2.03217,2.03217,0,0,0-.522-.5137,2.4336,2.4336,0,0,0-.7197-.3447,2.90512,2.90512,0,0,0-.8725-.126,3.21034,3.21034,0,0,0-1.2041.2149,2.42476,2.42476,0,0,0-.9165.6386,2.87367,2.87367,0,0,0-.5865,1.0508,4.74157,4.74157,0,0,0-.2055,1.4668,4.816,4.816,0,0,0,.2055,1.4727,3.00978,3.00978,0,0,0,.5865,1.0713,2.50551,2.50551,0,0,0,.9116.6523,3.05773,3.05773,0,0,0,1.1855.2217,2.79711,2.79711,0,0,0,1.6299-.4424,1.89829,1.89829,0,0,0,.7959-1.29h.5791a2.69148,2.69148,0,0,1-.3433.9687,2.53318,2.53318,0,0,1-.6479.7237,2.8472,2.8472,0,0,1-.898.4443,3.79574,3.79574,0,0,1-1.1015.1523,3.61365,3.61365,0,0,1-1.4522-.2812,3.0548,3.0548,0,0,1-1.0957-.7969,3.62539,3.62539,0,0,1-.6982-1.251,5.23821,5.23821,0,0,1-.2402-1.6455,5.13416,5.13416,0,0,1,.2402-1.6299,3.55722,3.55722,0,0,1,.7016-1.2373,3.05854,3.05854,0,0,1,1.1001-.7851,3.61032,3.61032,0,0,1,1.4444-.2754,3.45,3.45,0,0,1,1.0493.1601,3.177,3.177,0,0,1,.895.4473,2.78017,2.78017,0,0,1,.668.6914,2.50476,2.50476,0,0,1,.3784.8809Z" style="fill: #fff"></path>
                    <path d="M106.81351,10.4688h-.5572V3.7578l-2.7436,6.711h-.5835l-2.7383-6.711v6.711h-.5566V2.8438h.8076l2.7793,6.9296,2.7964-6.9296h.7959Z" style="fill: #fff"></path>
                    <path d="M112.25441,10.6074a4.95936,4.95936,0,0,1-1.3486-.164,2.85058,2.85058,0,0,1-.97169-.4639,2.081,2.081,0,0,1-.6026-.7314,2.52409,2.52409,0,0,1-.24409-.9639h.5508a2.07182,2.07182,0,0,0,.31441.8545,1.70641,1.70641,0,0,0,.5679.542,2.29133,2.29133,0,0,0,.76711.2861,4.778,4.778,0,0,0,.9194.084,3.565,3.565,0,0,0,.9156-.1074,2.18764,2.18764,0,0,0,.6962-.3125,1.45561,1.45561,0,0,0,.4424-.4942,1.37974,1.37974,0,0,0,.1543-.6523,1.4652,1.4652,0,0,0-.1313-.6445,1.17281,1.17281,0,0,0-.4463-.4551,3.50232,3.50232,0,0,0-.82719-.3487c-.3403-.1025-.76311-.206-1.27049-.3134a7.05172,7.05172,0,0,1-1.07471-.3203,2.83488,2.83488,0,0,1-.7431-.4219,1.50145,1.50145,0,0,1-.43021-.5498A1.74411,1.74411,0,0,1,109.353,4.71a1.76924,1.76924,0,0,1,.187-.8145,1.86411,1.86411,0,0,1,.5323-.6338,2.494,2.494,0,0,1,.8291-.4121,3.685,3.685,0,0,1,1.0683-.1445,4.02633,4.02633,0,0,1,1.0742.1348,2.7555,2.7555,0,0,1,.8521.3935,2.0662,2.0662,0,0,1,.8813,1.4619h-.53709a1.79392,1.79392,0,0,0-.2505-.5703,1.6415,1.6415,0,0,0-.4502-.4551,2.138,2.138,0,0,0-.6523-.3007,3.166,3.166,0,0,0-.8667-.1075,2.67254,2.67254,0,0,0-1.5522.375,1.19992,1.19992,0,0,0-.524,1.0313,1.29227,1.29227,0,0,0,.0977.5234,1.0008,1.0008,0,0,0,.33009.3985,2.22255,2.22255,0,0,0,.6176.3144,7.29267,7.29267,0,0,0,.9537.2549c.2226.0449.4536.0928.6904.1445.23441.0528.4682.1133.69189.1817a4.28217,4.28217,0,0,1,.6416.2568,2.11154,2.11154,0,0,1,.53419.376,1.61884,1.61884,0,0,1,.3613.5322A1.81789,1.81789,0,0,1,115,8.3887a1.93547,1.93547,0,0,1-.2056.8926,2.11549,2.11549,0,0,1-.5703.7031,2.659,2.659,0,0,1-.8716.459A3.52035,3.52035,0,0,1,112.25441,10.6074Z" style="fill: #fff"></path>
                    <polygon points="16.044 8.862 11.253 12.132 5.618 16.094 5.618 21.193 17.04 13.162 17.04 9.562 16.044 8.862" style="fill: #fff"></polygon>
                    <polygon points="14.752 7.955 3.44 0 0 1.789 0 20.938 3.44 22.725 4.174 22.209 4.174 5.615 11.088 10.476 14.752 7.955" style="fill: #fff"></polygon>
                    <polygon points="45.669 16.143 45.669 2.94 42.013 2.94 42.013 2.94 42.013 19.748 42.013 19.799 57.405 19.799 57.405 16.143 45.669 16.143" style="fill: #fff"></polygon>
                </svg>
            </div>

            <div class="form-head">
                <p>{{ trans('users::auth.form head.title') }}</p>
                <span>{{ trans('users::auth.form head.description') }}</span>
            </div>

            <label class="input {{ $errors->has('email') ? 'error' : '' }}">
                <input type="email" class="auth-form-input" name="email" value="{{ old('email') }}" placeholder="show">
                <span>{{ trans('users::auth.email') }}</span>
                {!! $errors->first('email', '<i>:message</i>') !!}
            </label>

            <label class="input {{ $errors->has('password') ? 'error' : '' }}">
                <input type="password" class="auth-form-input" name="password" value="{{ old('password') }}" placeholder="show">
                <span>{{ trans('users::auth.password') }}</span>
                {!! $errors->first('password', '<i>:message</i>') !!}
            </label>

            <div class="user-options">
                <label class="remember">
                    <input type="checkbox" name="remember">
                    <span><i></i>{{ trans('users::auth.remember me') }}</span>
                </label>

                @if (config('application.users.config.allow_user_reset', false))
                    <a class="button" href="{{ route('reset') }}">{{ trans('users::auth.forgot password') }}</a>
                @endif

            </div>

            <div class="btn_wrap">
                <button class="button pink icon-btn-text" type="submit">{{ trans('users::auth.login') }}</button>
            </div>

            @if (config('application.users.config.allow_user_registration', false))
                <div class="text-center register-link">
                    <p class="text-grey-dark text-sm">Don't have an account? <a href="{{ route('register') }}" class="no-underline text-blue font-bold">Create an Account</a>.</p>
                </div>
            @endif

        {!! Form::close() !!}

        <div class="login-footer">
            <span>Copyright Delta CMS, 2018 - {{ date('Y') }}</span>
        </div>

    </main>
@stop

@section('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        let inputs = document.querySelectorAll('.auth-form-input');

        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener("blur", (e) => {
                if (e.target.value === '') {
                    e.target.classList.remove('not-empty');
                } else {
                    e.target.classList.add('not-empty');
                }
            }, true)
        }

        let video = document.querySelector("#auth-video");
            video.playbackRate = 0.45;
            video.onplay = function() {
                video.style.opacity = 1
            };
    });
</script>
@stop

@section('style')
<style>
    .auth-page body{
        background: #0e110e;
        min-width: 100%;
        min-height: 100%;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .auth-page main{
        width: 100%;
        max-width: 400px;
    }
    .alert p.font-16{
        font-size: 16px;
        color: #ff6662;
        text-align: center;
        margin-bottom: 15px;
    }
</style>
@stop
