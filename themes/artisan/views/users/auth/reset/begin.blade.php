@extends('core::layouts.auth')

@section('title')
Reset password
@stop

@section('content')

<div class="container mx-auto h-full flex justify-center items-center">
    <div class="w-2/4">
        <h1 class="font-hairline mb-6 text-center">To reset password complete this form</h1>

        @include('core::partials.notifications')

        {!! Form::open(['route' => 'reset.post', 'class' => 'w-full max-w-sm']) !!}
            <div class="flex items-center border-b border-b-2 py-2 {{ $errors->has('email') ? 'border-red' : 'border-teal' }}">

                <input class="appearance-none bg-transparent w-full border-none text-grey-darker mr-3 py-1 px-2 {{ $errors->has('email') ? 'border-red' : '' }}" type="email" name="email" value="{{ old('email') }}" placeholder="Your Email" aria-label="Email">

                <button class="flex-no-shrink  hover:bg-teal-dark {{ Session::has('error') ? 'border-red bg-red' : 'border-teal bg-teal' }} hover:border-teal-dark text-sm border-4 text-white py-1 px-2 rounded" type="submit">
                    Reset password
                </button>

                <a class="flex-no-shrink border-transparent border-4 text-teal hover:text-teal-darker text-sm py-1 px-2 rounded" href="{{ route('login') }}">
                    Cancel
                </a>

            </div>
        {!! Form::close() !!}

        {!! $errors->first('email', '<p class="text-red text-xs italic">:message</p>') !!}

    </div>
</div>

@stop
