<!DOCTYPE html>
<html lang="{{ locale() }}" class="{{ $agent->isMobile() ? 'mobile' : '' }} {{ mb_strtolower($agent->platform()) }}">
    <head>
        <meta name="robots" content="noindex, nofollow" />

        <base src="{{ url('/') }}">
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1, shrink-to-fit=no" />

        <title>@yield('title')</title>
        <meta name="description" content="DeltaCMF platform" />
        <meta name="copyright" content="CF.Digital" />

        <!-- CSRF and API Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="api-token" content="{{ $currentUser->getFirstApiKey() }}">

        <!-- CSS -->
        @yield('before_styles')
        {{ Html::style('themes/artisan/css/app.css') }}
        {{ Html::style('themes/artisan/css/element.css') }}
        {{ Html::style('themes/artisan/css/custom.css') }}
        @yield('after_styles')

        <!-- tightenco/ziggy -->
        @routes
    </head>
    <body class="@yield('body_class')">

        <div id="app" class="main-wrapper">

            @yield('before_content')

            @yield('content')

            @yield('after_content')

        </div>

        <!-- JavaScript -->
        <script type="text/javascript">
            window.appStorage = {
                translations: {!! $backendTranslations !!},
                currentBackendLocale: '{{ $currentBackendLocale }}',
                allBackendLocales: {!! json_encode($allBackendLocales) !!},
                currentFrontendLocale: '{{ $currentFrontendLocale }}',
                allFrontendLocales: {!! json_encode($allFrontendLocales) !!},
                adminPrefix: '{{ config('application.core.core.admin-prefix', 'backend') }}',
                hideDefaultLocaleInURL: '{{ config('laravellocalization.hideDefaultLocaleInURL') }}',
                currentUser: {!! $currentUser->getCurrentUser() !!},
                activeModulesPaths: {!! json_encode($activeModulesPaths) !!}
            };
        </script>
        @yield('before_scripts')
        {{ Html::script('themes/artisan/vendor/ckeditor/ckeditor.js') }}
        {{ Html::script('themes/artisan/js/manifest.js') }}
        {{ Html::script('themes/artisan/js/vendor.js') }}
        {{ Html::script('themes/artisan/js/app.js') }}
        @yield('after_scripts')

        @include('core::includes.application_svg')
    </body>
</html>
