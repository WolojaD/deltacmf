<!DOCTYPE html>
<html class="auth-page">
    <head>
        <meta name="robots" content="noindex, nofollow" />

        <base src="{{ url('/') }}">
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1, shrink-to-fit=no" />

        <title>DeltaCMS | @yield('title')</title>
        <meta name="description" content="DeltaCMF platform" />
        <meta name="copyright" content="CF.Digital" />
        <meta name="author" content="CF.Digital, Kyryll Kovalenko, Aleksey Kashin, Volodja Jarmolenko" />

        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{ Html::style('themes/artisan/css/app.css') }}
        {{ Html::style('themes/artisan/css/element.css') }}
    </head>
    <body>

        @yield('before_content')

        @yield('content')

        @yield('after_content')

    </body>

    @yield('scripts')
    @yield('style')
</html>
