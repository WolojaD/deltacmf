@extends('core::layouts.application')

@section('title')
DeltaCMS
@stop

@section('before_styles')
@stop

@section('after_styles')
<style>
    .go-to-main-user{
        bottom: 0;
        padding: 20px;
        left: 0;
        color: #ffffff;
        display: flex;
        align-items: center;
        transition: border-color .3s, background-color .3s, color .3s;
    }
    .go-to-main-user:hover{
        color: #303133;
        background: #ffffff;
    }
    .go-to-main-user svg use{
        transition: border-color .3s, background-color .3s, color .3s;
    }
    .go-to-main-user svg{
        padding: 0;
        margin-right: 5px;
    }
    .go-to-main-user:hover svg use{
        color: #303133;
    }
    #user_error{
        position: fixed;
        z-index: 9999;
        right: 0;
        top: 150px;
        padding: 10px;
        margin-right: 20px;
        transform: translateX(130%);
    }
    @media (max-width: 1200px) {
        .go-to-main-user{
            justify-content: center;
            color: #231f20;
            padding: 0;
        }
        .go-to-main-user svg.button-icon use{
            color: #231f20;
        }
    }
</style>
@stop

@section('body_class')
@stop


@section('content')

    @if (Session::has('error'))
        <div role="alert" class="el-notification right" id="user_error">
            <i class="el-notification__icon el-icon-error"></i>
            <div class="el-notification__group is-with-icon">
                <h2 class="el-notification__title">Error!</h2>
                <div class="el-notification__content">
                    <p>{{ Session::get('error') }}</p>
                </div>
                <div class="el-notification__closeBtn el-icon-close" id="user_error-close"></div>
            </div>
        </div>
    @endif

    @impersonating
        <a class="go-to-main-user" href="{{ route('impersonate.leave') }}">
            <svg class="button-icon arrow-back"><use xlink:href="#arrow-back" class="arrow-back"></use></svg>
            @lang('users::auth.leave impersonation', [], locale())
        </a>
    @endImpersonating

    <router-view></router-view>

@stop

@section('before_scripts')
@stop

@section('after_scripts')
@if (Session::has('error'))
    <script>
        setTimeout( () => {
            let el = document.querySelector('#user_error');
            Velocity(el, {translateX: ['0%', '130%']}, {
                duration: 550,
                complete: function () {
                    setTimeout( () => {
                        Velocity(el, {opacity: ['0', '1']}, {
                            duration: 550,
                            complete: function () {
                                el.parentElement.removeChild(el);
                            }
                        });
                    }, 3000)
                }
            });
            document.querySelector('#user_error-close').onclick = function (e) {
                Velocity(el, {opacity: ['0', '1']}, {
                    duration: 550,
                    complete: function () {
                        el.parentElement.removeChild(el);
                    }
                });
            }
        }, 500);
    </script>
@endif

@impersonating
    <script>
        let logOut = document.querySelector('.go-to-main-user');

        document.addEventListener('DOMContentLoaded', function () {
            if (window.innerWidth <= 1200) {
                document.querySelector('aside>.mobile-actions').insertAdjacentElement('afterbegin', logOut);
            } else {
                document.querySelector('#navigation-header').insertAdjacentElement('afterbegin', logOut);
            }

        });
        window.addEventListener('resize', function () {
            setTimeout( () => {
                if (window.innerWidth <= 1200) {
                    if (!document.querySelector('aside>.mobile-actions').querySelector('.go-to-main-user')) {
                        document.querySelector('aside>.mobile-actions').insertAdjacentElement('afterbegin', logOut);
                    }
                } else {
                    if (!document.querySelector('#navigation-header').querySelector('.go-to-main-user')) {
                        document.querySelector('#navigation-header').insertAdjacentElement('afterbegin', logOut);
                    }
                }
            }, 250);
        });
    </script>
@endImpersonating
@stop
