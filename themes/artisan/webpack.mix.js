const mix = require('laravel-mix');
const webpack = require('webpack');
const tailwindcss = require('tailwindcss');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('../../public');

mix
    .styles('resources/vendor/element-custom/index.css', '../../public/themes/artisan/css/element.css')
    .sass('resources/css/index.sass', '../../public/themes/artisan/css/custom.css')
    .postCss(
        'resources/vendor/tailwind/css/tailwind.css',
        'themes/artisan/css/app.css', [
            tailwindcss('resources/vendor/tailwind/js/tailwind.js'),
        ])
    .options({
        processCssUrls: false
    })
    .js('resources/js/app.js', 'themes/artisan/js/app.js')
    .webpackConfig({
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                "@": path.resolve(__dirname, 'resources/js'),
                "@services": path.resolve(__dirname, 'resources/js/services'),
                "@components": path.resolve(__dirname, 'resources/js/components'),
                "@resources": path.resolve(__dirname, 'resources'),
                "@modules": path.resolve(__dirname, '../../modules'),
            }
        }
    })
    .extract()
    .copyDirectory('resources/image', '../../public/themes/artisan/image')
    .copyDirectory('resources/fonts', '../../public/themes/artisan/fonts')
    .copyDirectory('resources/vendor/element-custom/fonts', '../../public/themes/artisan/css/fonts')
    .copyDirectory('resources/vendor/ckeditor-vue/ckeditor', '../../public/themes/artisan/vendor/ckeditor')
    .copyDirectory('resources/vendor/telescope', '../../public/vendor/telescope');

if (mix.inProduction()) {
    mix.version();
} else {
    mix
        // .browserSync({
        //     proxy: 'deltacmf.localhost'
        // })
        .sourceMaps();
}
