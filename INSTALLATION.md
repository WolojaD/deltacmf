# Installation

DeltaCMF requires [Node.js](https://nodejs.org/) v10+ and [npm](https://www.npmjs.com) v6+ to run.
```sh
cp .env.example .env
composer install
php artisan key:generate
```
Then you need to set-up your database config inside .env file.
Next:
```sh
php artisan application:migrate:fresh-seed
```
Okey, you got it installed, but you need a front-end ?
Front-end divided into themes in separate folders
You need to go into folder that you need (for example - admin theme called 'artisan'), so your path would be - `you application root/themes/artisan` and in this folder run:
```sh
npm install
```
For production environment...
```sh
npm run prod
```
Or for developing environment...
```sh
npm run dev
or
npm run watch
```



## Administrator area (backend)
DeltaCMF use http:://domain.com/backend (or https:://domain.com/backend) to route you to the administration area.
| Login | Password |
| ------ | ------ |
| cfdigital@c-format.ua | cfdigital@c-format.ua |



### Update after major commit :flushed:
Major commit - commit that starts like - `[major] name of commit etc.`
First, you need to install new packages for npm
```sh
npm install
```
Then, you need to update project
```sh
composer install
```
And last - refresh database
```sh
php artisan application:migrate:fresh-seed
```
If you find some bugs - please, tell me :smile:



### Update after minor commit
Maybe, need's to refresh database:
```sh
php artisan application:migrate:fresh-seed
```



# For module creating...
This project uses [modules generate](https://nwidart.com/laravel-modules/v3/introduction) package extended by custom generate logic
[Watch this file](MODULEGENERATE.md)



# COMMANDS
[Watch this file](COMMANDS.md)
