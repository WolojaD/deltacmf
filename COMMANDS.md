# Commands

### Remigrate the entire base with seeds
```sh
php artisan application:migrate:fresh-seed
```



### Generate new or add old module [more in this file](MODULEGENERATE.md.md)
```sh
php artisan application:module:generate {Module Name}
```
where {Module Name} - module name



### Removing logs for a certain number of days
```
php artisan application:logs-clear {days}
```
where {days} - the number of days until the date when the files are considered obsolete
