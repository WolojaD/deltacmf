# Генерация модуля

Данный радел находится в стадии разработки и может иметь не точности!

Модуль генерируется командой:
```sh
php artisan application:module:generate {module_name}
```
где {module_name} - название модуля

## Генерация файлов относящихся к модулю
В папке "Config/models" модуля нужно создать json файл, у которого название = название модели (через нижнее подчеркивание для >1 слов)
```json

Для примера ниже показанно содержимое файла -

{
    "table_name": "ecommerce_products",
    "backend_path_name": "ecommerce_product",
    "backend_route_prefix": "/ecommerce/product",
    "need_translations": true,
    "templates": {
        "default": {
            "standard": {
                "title_field": "title",
                "multicheck": true,
                "actions": {
                    "edit": true,
                    "delete": true
                },
                "has_tables": {
                    "inner_tables": ["ecommerce_product_versions"]
                },
                "buttons": {
                    "inner_buttons": ["find-and-add-product"]
                },
                "order": true,
                "status": true,
                "nested": false,
                "slugable": true,
                "no_create": true,
                "desktop_mobile_images": true,
                "sortOrder": {
                    "field": "order",
                    "sortField": "order",
                    "direction": "asc"
                }
            },
            "title": {
                "field_type": "string",
                "type": "text",
                "label": {"en": "Title", "ru": "Заголовок"},
                "hint": {"en": "Title", "ru": "Заголовок"},
                "column_type": "text-column",
                "relation": "",
                "validation": "",
                "sortable": true,
                "required": false,
                "unique": false,
                "searchable": true,
                "is_translated": true,
                "field_roles": ["developer"],
                "column_roles": "developer",
            },
            "category_id": {
                "field_type": "unsignedInteger",
                "type": "select",
                "label": {"en": "Category", "ru": "Категория"},
                "hint": {"en": "Category", "ru": "Категория"},
                "column_type": "text-column",
                "name": "category_title",
                "relation": "categories",
                "validation": "",
                "sortable": true,
                "required": true,
                "unique": false,
                "is_translated": false,
                "from_ident": true
            },
            "gallery_tab": {
                "field_type": "string",
                "type": "tab",
                "label": {"en": "Gallery", "ru": "Галерея"}
            },
            "gallery_id": {
                "field_type": "unsignedInteger",
                "type": "gallery",
                "label": {"en": "Gallery", "ru": "Галерея"},
                "hint": {"en": "Gallery", "ru": "Галерея"},
                "column_type": "text-column",
                "name": "gallery_title"
            }
        },
        "sale": {
            "standard": {
                "multicheck": true,
                "actions": {
                    "edit": true,
                    "detach": true
                },
                "nested": false,
                "sortOrder": {
                    "field": "order",
                    "sortField": "order",
                    "direction": "asc"
                }
            },
            "title": {
                "field_type": "string",
                "field_symbols_count": 250,
                "type": "text",
                "label": {"en": "Title", "ru": "Заголовок"},
                "hint": {"en": "Title", "ru": "Заголовок"},
                "column_type": "text-column",
                "relation": "",
                "default": "test",
                "validation": "",
                "sortable": true,
                "required": false,
                "unique": false,
                "is_translated": true
            },
            "category_id": {
                "field_type": "unsignedInteger",
                "type": "select",
                "label": {"en": "Category", "ru": "Категория"},
                "hint": {"en": "Category", "ru": "Категория"},
                "column_type": "text-column",
                "name": "category_title",
                "relation": "categories",
                "validation": "",
                "sortable": true,
                "required": true,
                "unique": false,
                "is_translated": false,
                "from_ident": true
            }
        }
    },
    "pivots": {
        "ecommerce_order_products": {
            "ids": {
                "order_id": {
                    "table": "ecommerce_orders",
                    "onDelete": "NO ACTION"
                },
                "product_id": {
                    "table": "ecommerce_products",
                    "onDelete": "NO ACTION"
                }
            },
            "items": {
                "variant_id": {
                    "field_type": "unsignedInteger"
                },
                "quantity": {
                    "field_type": "unsignedInteger"
                },
                "price": {
                    "field_type": "decimal"
                },
                "discount": {
                    "field_type": "decimal"
                },
                "discount_price": {
                    "field_type": "decimal"
                },
                "article": {
                    "field_type": "text"
                },
                "parameter": {
                    "field_type": "text"
                }
            }
        }
    }
```

| Индекс                  | Описание
| --- |:---:
| table_name*             | название таблицы в базе
| backend_path_name*      | используется для прав и названий путей до _ - название модуля(обязательно), после - название модели
| backend_route_prefix*   | используется для префикса в путях backend и api
| need_translations       | добавляет все связанное с переводами (модели таблицы поля...)


| templates*              | default используется для генерации всего (наличие обязательно), остальные для вывода кастомных таблиц
| --- |:---:
| standard                | частоиспользуемые практически не меняющиеся характеристики таблиц, могут быть переписаны как обычное поле в темплейте
| --- | :---:
| title_field             | поле модели которое определяет заголовок (по умолчанию title)
| multicheck              | наличие чекбоксов в таблице при выводе
| actions                 | список дейтсвий в каждой строке таблицы вывода
| has_tables              | показывает дополнительные таблицы под таблицей вывода, если она главная
| outer_tables            | таблицы показывается в индексе
| inner_tables            | таблицы показываются внутри (show)
| buttons                 | дополнительные (не стандартные кнопки над таблицей) inner/outer так же как у таблиц выше
| order                   | наличие порядка у модели
| status                  | наличие статуса у модели
| nested                  | наличие ветвистости у модели, можно передать лейбл ({"en": "Category", "ru": "Категория"})
| slugable                | наличие урлов у модели
| no_create               | нет кнопки создать у таблицы вывода
| desktop_mobile_images   | наличие мобильного и десктопного изображения у модели
| sortOrder               | начальный порядок вывода в таблице вывода
| desktop_mobile_images   | наличие мобильного и десктопного изображения у модели
| --- | :---:
| не standard             | описываются поля и колонки модели и все что связано с ними
| --- | :---:
| field_type              | тип данных для миграций
| type                    | тип данных (компонент) для формы редактированя/создания
| label                   | для вывода навания поля формы редактированя/создания или колонки таблицы вывода
| hint                    | для вывода подсказки поля формы редактированя/создания
| column_type             | тип данных (компонент) для таблицы вывода
| name                    | атрибут модели для таблицы вывода
| relation                | метод в репозитории который вернет массив данных для селекта
| validation              | дополнительная валидация поля (в Request)
| sortable                | сортировка по полю в таблице вывода
| field_symbols_count     | в миграции присваивает ограничение по символам (для строк), в валидации проверяет ограничение по симоволам
| default                 | в миграции присваивает стандартное значение поля
| required                | в миграции присваивает not nullable, в валидации проверяет наличие, на форме ставит звездочку
| unique                  | в миграции присваивает unique, в валидации проверяет уникальность
| is_translated           | в миграции создает translatable модель (1 раз) и добавляет туда поле, в валидации проверяет в
                            отдельном методе, на форме подставляется к табам
| searchable              | поле может искатся в таблице вывода
| from_ident              | сохраняет поле равное идентификатору в create (в пути цифра)
| field_roles,column_roles| массив или строка, определяет выводится ли соответсвующая запись в таблице(column) и в форме (field)
| pivots                  | пивот таблицы, которые могут и не отностится к этой модели
| --- |:---:
| индекс                  | название таблицы в базе
| ids*                    | id связи и создание foreign с указанным действием
| items                   | дополнительные поля

После создания файла, еще раз запускается команда:
```sh
php artisan application:module:generate {module_name}
```
будут созданы нужные для работы файлы.



## После генерации файлов
Нужно добавить права пользователю, пути в sidebar, пути в router.js (если его нет, создать), добавить переводы.


TODO БЛА БЛА
в контроллере нужно изменять индекс
для бредкрамбов нужен groupId в модели
